#!/bin/bash

# Install Java 8 if necessary
java_version=$(java -version 2>&1 | awk -F '"' '/version/ {print $2}')
if [[ "$java_version" < 1.8 ]]; then
    sudo add-apt-repository ppa:webupd8team/java
    sudo apt-get update
    sudo apt-get install oracle-java8-installer  
fi

if [ ! -d ./neo4j-community-3.3.1 ]; then
    # Download neo4j
    wget https://neo4j.com/artifact.php?name=neo4j-community-3.3.1-unix.tar.gz -O neo4j-community-3.3.1-unix.tar.gz
    tar -xf neo4j-community-3.3.1-unix.tar.gz
    # Configure neo4j
    wget https://opb.data-lab.si/res/Neo4j/neo4j.conf
    mv neo4j.conf ./neo4j-community-3.3.1/conf/neo4j.conf
    # Remove unneded files
    rm neo4j-community-3.3.1-unix.tar.gz
fi

# Start server
./neo4j-community-3.3.1/bin/neo4j start
echo "Access the web interface via 'https://{DELOVNO_OKOLJE}-{UPORABNISKO_IME}.c9users.io:8081/browser/'"