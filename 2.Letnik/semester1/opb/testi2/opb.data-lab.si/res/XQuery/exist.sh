#!/bin/bash
exist_directory="existdb"

# Set permission
# chmod +x eXist.sh


if [ ! -f ./eXist-db-setup-2.2.jar ]; then
    # Download eXist server
    wget https://bintray.com/existdb/releases/download_file?file_path=eXist-db-setup-2.2.jar -O eXist-db-setup-2.2.jar
fi


if [ ! -d "$exist_directory" ]; then
    # Install eXist server
    
    # Select target path
    # 1. korak: /home/ubuntu/workspace/existdb <ENTER>
    # 2. korak: 1 <ENTER>
    
    # Set Data Directory
    # 3. korak: <ENTER>
    # 4. korak: 1 <ENTER>
    
    # Set Admin Password and Configure Memory
    # 5. korak: <ENTER>
    # 6. korak: <ENTER>
    
    # Maximum memory in mb
    # 7. korak: <ENTER>
    # 8. korak: <ENTER>
    # 9. korak: 1 <ENTER>
    java -jar eXist-db-setup-2.2.jar -console
fi


# Start eXist server
# https://{ime-aplikacije}-{uporabniško.ime}.c9users.io:8443/exists/
./existdb/bin/startup.sh


# Update examples
#wget https://dl.dropboxusercontent.com/u/2855959/Moodle/XQuery/OPB-data.zip
#unzip OPB-data.zip
#rm -rf __MACOSX
#rm -rf OPB-data.zip
#./existdb/bin/client.sh -m /db/OPB -p ./OPB
#rm -rf ./OPB