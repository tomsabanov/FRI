$(document).ready(function() {
	pripraviTestiranjeSQL();
});

function pripraviTestiranjeSQL() {
	if (zeNalozen) {
	} else {
		zeNalozen = true;
		$('div.answer>textarea').each(function(index) {
			$('<p></p>').insertAfter(this);
			$('<p><img src="https://opb.data-lab.si/run.png" alt="Testiraj poizvedbo" title="Testiraj poizvedbo" style="padding-bottom:5px; border:0; vertical-align:middle; cursor:pointer" />&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:10pt"></span></p>').insertAfter(this);
		});
		omogociTestiranjeSQL();
	}
}

var sec;
var timer;
var zakasnitevMs = 15 * 1000;
//var dbProxyURL = "http://sandbox.lavbic.net/OPB/DBProxy";
var dbProxyURL = "https://octonion.data-lab.si/OPB/DBProxy";
var zeNalozen;


function omogociTestiranjeSQL() {
	//console.log("Omogoči testiranje SQL.");
	$('div.answer>textarea').each(function(index) {
		var gumbSlika = $("textarea + p > img");
		gumbSlika.attr("src", "https://opb.data-lab.si/run.png");
		gumbSlika.attr("alt", "Testiraj poizvedbo");
		gumbSlika.attr("title", "Testiraj poizvedbo");
		gumbSlika.css("cursor", "pointer");

		var vnosnoPolje = $(this);
		var guiPrikaz = $(this).next().next();
		var uporabnikImePriimek = $(".logininfo").children().eq(0).text().trim();
		var db = $('span:contains("Privzeta podatkovna baza"):eq(' + index + ')>u').text().trim();
		if (db == '') {
			db = $('span:contains("Privzeta podatkovna baza"):eq(' + index + ')>span').text().trim();
		}
		//var vprasanje = $('span:contains("Privzeta podatkovna baza"):eq(' + index + ')').prev().prev().text();
		var vprasanje = $('span:contains("Privzeta podatkovna baza"):eq(' + index + ')').parent().parent().find("p:eq(1) > span:eq(0)").text().trim();
		if (vprasanje == '') {
			vprasanje = "1";
		}
		
		//console.log("Ime in priimek : |" + uporabnikImePriimek + "|");
		//console.log("DB : |" + db + "|");
		//console.log("Vprašanje : |" + vprasanje + "|");

		$(this).next().children('img').click(function(e) {
			onemogociTestiranjeSQL();
			guiPrikaz.html('<img src="https://opb.data-lab.si/ajax-loader.gif" alt="Izvajam poizvedbo"/>');
			$.get(dbProxyURL,
				{
					db: db,
					query: vnosnoPolje.val(),
					user: uporabnikImePriimek,
					question: vprasanje
				},
				function(odgovor) {
					guiPrikaz.html(odgovor);
				});

			sec = zakasnitevMs/1000 - 1;
			gumbSlika.next().text("(Počakaj " + (sec + 1) + "s za ponovno testiranje poizvedbe)");
			gumbSlika.next().fadeIn('fast');
			timer = setInterval(function() {
				gumbSlika.next().text("(Počakaj " + (sec--) + "s za ponovno testiranje poizvedbe)");
			   if (sec == -1) {
				  gumbSlika.next().fadeOut('fast');
				  clearInterval(timer);
			   }
			}, 1000);

			setTimeout(omogociTestiranjeSQL, zakasnitevMs);
		});
	});
}


function onemogociTestiranjeSQL() {
	//console.log("Onemogoči testiranje SQL za " + (zakasnitevMs/1000) + " s.");
	$('div.answer>textarea').each(function(index) {
		var gumbSlika = $("textarea + p > img");
		gumbSlika.attr("src", "https://opb.data-lab.si/run_disabled.png");
		gumbSlika.attr("alt", "Počakaj " + (zakasnitevMs/1000) + "s za ponovno testiranje poizvedbe");
		gumbSlika.attr("title", "Počakaj " + (zakasnitevMs/1000) + "s za ponovno testiranje poizvedbe");
		gumbSlika.css("cursor", "progress");
		gumbSlika.unbind('click');
	});
}
