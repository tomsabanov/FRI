#!/bin/bash
service_apache2=apache2
service_mysql=mysql

clear
echo
echo Izberite akcijo, ki jo želite izvesti:
echo --------------------------------------
echo 1. START relacijska podatkovna baza in SQL \(mySQL podatkovna baza + Apache spletni strežnik + phpMyAdmin\)
echo 2. STOP relacijska podatkovna baza in SQL \(mySQL podatkovna baza + Apache spletni strežnik + phpMyAdmin\)
echo 3. START XML podatkovna baza in XPath ter XQuery \(existdb\)
echo
read izbira

if (( $izbira == 1 )); then
    echo
    echo "Pripravljanje okolja za poizvedovanje po relacijski podatkovni bazi ..."
    echo
    # Preveri, če je mySQL podatkovna baza pognana
    if (( $(ps -ef | grep -v grep | grep $service_mysql | wc -l) > 0 )); then
        echo "- mySQL :: OK"
    else
        echo "- mySQL :: poganjam ..."
        sudo service $service_mysql start
    fi
    # Preveri, če je Apache spletni strežnik pognan
    if (( $(ps -ef | grep -v grep | grep $service_apache2 | wc -l) > 0 )); then
        echo "- Apache :: OK"
    else
        echo "- Apache :: poganjam ..."
        sudo service $service_apache2 start
    fi
    echo
    echo "Za poizvedovanje uporabite naslov http://{ime-aplikacije}-{uporabniško.ime}.c9users.io/phpmyadmin/"
    echo
    exit
elif (( $izbira == 2 )); then
    echo
    echo "Ugašanje okolja za poizvedovanje po relacijski podatkovni bazi ..."
    echo
    sudo service $service_apache2 stop
    sudo service $service_mysql stop
    exit
elif (( $izbira == 3 )); then
    echo
    echo "Pripravljanje okolja za poizvedovanje po XML podatkovni bazi ..."
    echo
    sudo service $service_apache2 stop
    sudo service $service_mysql stop
    ./existdb/bin/startup.sh
    exit
fi