#!/bin/bash

# Update examples
wget http://opb.data-lab.si/res/XQuery/XQueryData.zip
unzip XQueryData.zip
rm -rf __MACOSX
rm -rf XQueryData.zip
./existdb/bin/client.sh -m /db/OPB -p ./XQueryData
rm -rf ./XQueryData