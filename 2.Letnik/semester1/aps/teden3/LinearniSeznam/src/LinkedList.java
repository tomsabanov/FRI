
public class LinkedList
{
	protected LinkedListElement first;
	protected LinkedListElement last;
	
	LinkedList()
	{
		makenull();
	}
	
	//Funkcija makenull inicializira seznam
	public void makenull()
	{
		//drzimo se implementacije iz knjige:
		//po dogovoru je na zacetku glava seznama (header)
		first = new LinkedListElement(null, null);
		last = null;
	}
	
	//Funkcija addLast doda nov element na konec seznama
	public void addLast(Object obj)
	{
		//najprej naredimo nov element
		//ustrezno ga povezemo s koncem verige obstojecih elementov
		//po potrebi posodobimo kazalca "first" in "last"
		if(last == null){
			last = new LinkedListElement(obj);
			first.next.next = last;
		}
		else{
			LinkedListElement novElement = new LinkedListElement(obj);
			last.next = novElement;
			last = novElement;
		}

	}
	
	//Funkcija write izpise elemente seznama
	public void write()
	{
		//zacnemo pri elementu za glavo seznama
		//sprehodimo se po elementih do konca seznama
		//in izpisemo vrednost vsakega elementa
		//za kontrolo lahko izpisemo tudi vrednosti kazalcev "first" in "last"
		LinkedListElement el = first.next;
		while(el != null && el.element != null){
			System.out.println(el.element);
			el = el.next;
		}	
	}
	
	//Funkcija addFirst doda nov element na prvo mesto v seznamu (takoj za glavo seznama)
	void addFirst(Object obj)
	{
		//najprej naredimo nov element
		//ustrezno ga povezemo z glavo seznama
		//po potrebi posodobimo kazalca "first" in "last"
		if(first.next == null){
			first.next = new LinkedListElement(obj);
		}
		else{
			LinkedListElement novElement = new LinkedListElement(obj,first.next);
			first.next = novElement;
		}
	}
	
	//Funkcija length() vrne dolzino seznama (pri tem ne uposteva glave seznama)
	int length()
	{
		int length = 0;
		LinkedListElement current = first.next;
		while(current != null){
			length++;
			current = current.next;	
		}
		return length;
	}
	
	//Funkcija lengthRek() klice rekurzivno funkcijo za izracun dolzine seznama
	int lengthRek()
	{
		// pomagajte si z dodatno funkcijo int lengthRek(LinkedListElement el), ki izracuna
		// dolzino seznama za opazovanim elementom ter pristeje 1
		return lengthRek(first.next) - 1;	
	}
	int lengthRek(LinkedListElement el){
		if(el == null) return 1;
		return 1 + lengthRek(el.next);
	}
	
	//Funkcija insertNth vstavi element na n-to mesto v seznamu
	//(prvi element seznama, ki se nahaja takoj za glavo seznama, je na indeksu 0)
	boolean insertNth(Object obj, int n)
	{
		//zacnemo pri glavi seznama
		
		//sprehodimo se po elementih dokler ne pridemo do zeljenega mesta
		
		// ce je polozaj veljaven
		//   naredimo nov element
		//   ustrezno ga povezemo v verigo elementov
		//   po potrebi posodobimo kazalec "last"
		//   vrnemo true
		// sicer
		//   vrnemo false
		int i = 0;
		LinkedListElement current = first.next;
		LinkedListElement prev = new LinkedListElement(null);
		while(i<=n){
			if(i==n){
				//smo na pravem mestu
				LinkedListElement el = new LinkedListElement(obj,current);
				prev.next = el;
				if(i == 0) first.next = prev.next;
				return true;
			}
			prev = current;
			current = current.next;
			i++;
			if(current == null) return false;
		}	
		return false;
	}
	
	//Funkcija deleteNth izbrise element na n-tem mestu v seznamu
	//(prvi element seznama, ki se nahaja takoj za glavo seznama, je na indeksu 0)
	boolean deleteNth(int n)
	{
		//zacnemo pri glavi seznama
		
		//sprehodimo se po elementih dokler ne pridemo do zeljenega mesta
		
		// ce je polozaj veljaven
		//   ustrezno prevezemo elemente seznama tako, da ciljni element izlocimo iz verige
		//   po potrebi posodobimo kazalec "last"
		//   vrnemo true
		// sicer
		//   vrnemo false
		int i = 0;
		LinkedListElement current = first.next;
		LinkedListElement prev = new LinkedListElement(null);	
		while(i<=n){
			if(i==n){
				if(i==0){
					first.next = first.next.next;
					return true;
				}
				prev.next = current.next;
				if(current.next == null){
					prev.next = null;
					last = prev;
				}	
				return true;
			}
			prev = current;
			current = current.next;
			i++;
			if(current == null) return false;
		}
		return false;
	}
	
	//Funkcija reverse obrne vrstni red elementov v seznamu (pri tem ignorira glavo seznama)
	void reverse()
	{
		//ne pozabimo na posodobitev kazalca "last"!
		last = first.next;

		LinkedListElement prev = null;
		LinkedListElement curr = first.next;
		LinkedListElement next = null;
		while(curr != null){
			next = curr.next;
			curr.next = prev;
			prev = curr;
			curr = next;

		}
		first.next = prev;

	}
	
	//Funkcija reverseRek klice rekurzivno funkcijo, ki obrne vrstni red elementov v seznamu
	void reverseRek()
	{
		// pomagajte si z dodatno funkcijo void reverseRek(LinkedListElement el), ki
		// obrne del seznama za opazovanim elementom, nato ta element doda na konec (obrnjenega) seznama
		last = first.next;
		LinkedListElement newFirst = reverseRek(first.next);
		first.next = newFirst;
	}
	LinkedListElement reverseRek(LinkedListElement el){
		if(el == null) return null; 
		if(el.next == null) return el;
		LinkedListElement ostalo = reverseRek(el.next);
		el.next.next = el;
		el.next = null;
		return ostalo;
	}
	
	//Funkcija removeDuplicates odstrani ponavljajoce se elemente v seznamu
	void removeDuplicates()
	{
		//ne pozabimo na posodobitev kazalca "last"!
		LinkedListElement current = first.next;
		int length = length();
		for(int i = 0; i<length; i++){
			int value = ((Number)current.element).intValue();
			LinkedListElement currentEl = current.next;
			LinkedListElement prevEl = current;
			for(int j = i+1; j<length;j++){
				int currentValue = ((Number)currentEl.element).intValue();
				if(value == currentValue){
					prevEl.next = currentEl.next;
					currentEl = currentEl.next;
					if(currentEl == null){
					       	last = prevEl;
						break;
					}
				}
				else{
					prevEl = currentEl;
					currentEl = currentEl.next;
				}
			}
			if(current.next == null) break;
			current = current.next;
		}	
	}
}
