class QueueElement
{
	Object element;
	QueueElement next;

	QueueElement()
	{
		element = null;
		next = null;
	}
}

class Queue
{
	//QueueElement -> QueueElement -> QueueElement -> ... -> QueueElement
	//     ^                                                       ^
	//     |                                                       |  
	//    front                                                   rear
	//
	// nove elemente dodajamo na konec vrste (kazalec rear)
	// elemente brisemo na zacetku vrste (kazalec front)
	
	private QueueElement front;
	private QueueElement rear;
	
	public Queue()
	{
		makenull();
	}
	
	public void makenull()
	{
		front = null;
		rear = null;
	}
	
	public boolean empty()
	{
		return (front == null);
	}
	
	public Object front()
	{
		// funkcija vrne zacetni element vrste (nanj kaze kazalec front).
		// Elementa NE ODSTRANIMO iz vrste!
		if(front == null) return null;
		return front.element;
	}
	
	public void enqueue(Object obj)
	{
		// funkcija doda element na konec vrste (nanj kaze kazalec rear)
		QueueElement el = new QueueElement();
		el.element = obj;
		if(front == null){
			front = new QueueElement();
			front.element  = obj;
		}
		else if(rear == null){
			rear = new QueueElement();
			rear.element = obj;
			front.next = rear;
		}
		else{
			rear.next = el;
			rear = el;
		}
	}
	
	public void dequeue()
	{
		// funkcija odstrani zacetni element vrste (nanj kaze kazalec front)
		if(front.next == null){
			front = null;
			return;
		}
		front = front.next;
	}
}
