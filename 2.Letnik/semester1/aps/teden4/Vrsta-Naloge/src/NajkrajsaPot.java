class Konj
{
	int x;
	int y;
	String poteze;
	
	Konj(int x, int y, String poteze)
	{
		// hranimo trenutno pozicijo ('x', 'y') ter pot do nje ('poteze')
		this.x = x;
		this.y = y;
		this.poteze = poteze + " -> [" + x + "," + y + "]";
	}
}

	
public class NajkrajsaPot {
	
	public static void main(String[] args) {
		
		// podana je mapa v obliki dvodimenzionalnega polja 'mapa'
		// 'C' oznacuje ciljno polje
		// 'B' oznacuje polje z bombo
		// ' ' oznacuje varno polje, na katerega lahko stopimo
		
		char[][] mapa = {
				{'B',' ',' ',' ','B',' ',' ','B'},
				{' ','B',' ',' ',' ','B','B',' '},
				{' ','B','B',' ',' ',' ',' ',' '},
				{' ','B',' ',' ','B','B','B',' '},
				{'B','B','B','B','B',' ','B','B'},
				{'B',' ',' ','B',' ','C',' ',' '},
				{' ','B','B','B',' ','B','B','B'},
				{' ','B',' ',' ','B','B',' ',' '}};
		
		
		// zacetni polozaj konja
		int zacetni_x = 0;
		int zacetni_y = 1;
		
		//
		// Zelimo poiskati najkrajso varno pot sahovskega konja iz zacetne pozicije do ciljnega polja.
		// Pri tem si pomagamo z abstraktnim podatkovnim tipom vrsta.
		//
		
		Queue queue = new Queue();
		
		System.out.println("Zacetna pozicija konja je [" + zacetni_x + "," + zacetni_y + "]");
		
		Konj k = new Konj(zacetni_x, zacetni_y, "");
		queue.enqueue(k);
		
		boolean obstajaPot = false;
		while (!queue.empty())
		{
			Konj zacetni = (Konj)queue.front();
			queue.dequeue();
			// - preberemo zacetni element in ga odstranimo iz vrste
			
			if(zacetni.x < 0 || zacetni.y < 0) continue;	
			if(zacetni.x >= mapa[0].length || zacetni.y>=mapa.length) continue;
			// - preverimo ali prebrani element oznacuje veljavno pozicijo na mapi (ali so koordinate znotraj dovoljenih vrednosti)
			//   ce pozicija ni veljavna, izvedemo naslednjo iteracijo zanke
			
			
			if(mapa[zacetni.x][zacetni.y] == 'C')  System.out.println(zacetni.poteze);
			if(mapa[zacetni.x][zacetni.y] == 'B') continue;
			if(mapa[zacetni.x][zacetni.y] == '.') continue;


			mapa[zacetni.x][zacetni.y] = '.';
			
			Konj k1 = generirajKonja(zacetni, 2, 1);
			Konj k2 = generirajKonja(zacetni, 2, -1);
			Konj k3 = generirajKonja(zacetni, -2, 1);
			Konj k4 = generirajKonja(zacetni, -2, -1);
			Konj k5 = generirajKonja(zacetni, 1,2);
			Konj k6 = generirajKonja(zacetni, -1, 2);
			Konj k7 = generirajKonja(zacetni, 1, -2);
			Konj k8 = generirajKonja(zacetni, -1,-2);

			queue.enqueue(k1);
			queue.enqueue(k2);
			queue.enqueue(k3);
			queue.enqueue(k4);
			queue.enqueue(k5);
			queue.enqueue(k6);
			queue.enqueue(k7);
			queue.enqueue(k8);
			
			// - ce je pozicija veljavna, preverimo ali smo na cilju 'C' -  v tem primeru izpisemo resitev in izstopimo iz zanke
			// - ce je pozicija veljavna, preverimo ali smo na bombi 'B' - v tem primeru izvedemo naslednjo iteracijo zanke 
			// - ce je pozicija veljavna in nismo ne na cilju ne na bombi, generiramo naslednje pozicije konja in jih dodamo v vrsto
		}
		
		if (!obstajaPot)
			System.out.println("Varne poti do ciljnega polja ni!");
	}

	public static Konj generirajKonja(Konj k, int x, int y){
		String poteze = vrniPotezo(k.x + x, k.y + y, k.poteze);
		Konj nov = new Konj(k.x + x, k.y + y, poteze);	
		return nov;
	}

	public static String vrniPotezo(int x, int y, String prejsna){
		return prejsna + " -> [" + x +","+y+"]";
	}

}
