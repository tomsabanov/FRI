class StackElement
{
	Object element;
	StackElement next;

	StackElement()
	{
		element = null;
		next = null;
	}
}

class Stack
{
	//StackElement -> StackElement -> StackElement -> ... -> StackElement
	//     ^
	//     |
	//    top                                                   
	//
	// elemente vedno dodajamo in brisemo na zacetku seznama (kazalec top)
	
	
	private StackElement top;
	
	public Stack()
	{
		makenull();
	}
	
	public void makenull()
	{
		top = null;
	}
	
	public boolean empty()
	{
		return (top == null);
	}
	
	public Object top()
	{
		// Funkcija vrne vrhnji element sklada (nanj kaze kazalec top).
		// Elementa NE ODSTRANIMO z vrha sklada!
		if(top == null) return null;
		return top.element;
	}
	
	public void push(Object obj)
	{
		StackElement el = new StackElement(obj);
		if(top == null){
			top = el;
		}
		else{
			el.next = top;
			top = el;
		}
		// Funkcija vstavi nov element na vrh sklada (oznacuje ga kazalec top)
	}
	
	public void pop()
	{
		if(top == null) return;
		top = top.next;
		// Funkcija odstrani element z vrha sklada (oznacuje ga kazalec top)
	}
}
