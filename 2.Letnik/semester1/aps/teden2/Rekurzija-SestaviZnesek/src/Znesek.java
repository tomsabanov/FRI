public class Znesek {

	// funkcija preveri, ce obstaja podmnozica elementov v tabeli 'vrednosti',
	// ki se sesteje v 'znesek'.
	
	public static boolean sestavi(int[] vrednosti, int index, int znesek)
	{
		// robni pogoj: 
		// - znesek je sestavljen
		// - porabili smo vse elemente, zneska nismo sestavili
		
		
		// problem poskusimo resiti tako, da uporabimo trenutni element 
		// (mozno le, ce zahtevani znesek ni manjsi od vrednosti trenutnega elementa)
		// 
		// ce nam ne uspe, preskocimo trenutni element in nadaljujemo iskanje
		if(vrednosti.length == index) return false;
		int z = znesek - vrednosti[index]; 
		int brez = znesek;
		if(z > 0) return sestavi(vrednosti, index+1,z) || sestavi(vrednosti,index+1,brez);
		if(z == 0) return true;
		return sestavi(vrednosti, index+1, brez);	
	}
	
	public static void main(String[] args) {
		int[] vrednosti = {7,8,5,1,3,9,2,5,2,3,5};
		int znesek = 10;
		
		System.out.print("Znesek " + znesek + " dobimo tako, da sestejemo elemente: ");
		
		if (!sestavi(vrednosti, 0, znesek))
			System.out.println("Zneska ni mogoce sestaviti s podanimi elementi");
	}

}
