
public class GlavniProgram {

	// Funkcija prejme stavek ter izpise (brez ponavljanja)
	// - crke, ki nastopajo v stavku
	// - crke, ki nastopajo v vsaki besedi stavka
	public static void crke(String stavek)
	{
		// stavek spremenimo v male crke in ga razdelimo na besede
		String[] besede = stavek.toLowerCase().split(" ");
		
		// mnozica za hranjenje vseh crk v stavku 
		Set vse = new Set();
		
		// mnozico za hranjenje crk, ki nastopajo v vsaki besedi stavka
		Set povsod = new Set();
		
		for (int i = 0; i < besede.length; i++)
		{
			// mnozica crk v trenutni besedi
			Set beseda = new Set();
			for (int j = 0; j < besede[i].length(); j++)
			{
				beseda.insert(besede[i].charAt(j));
			}
			
			if (i==0)
				povsod.union(beseda); // zacnemo z vsemi crkami prve besede
			else
				povsod.intersection(beseda); // ohranimo samo skupne crke
			
			vse.union(beseda); // mnozici crk dodamo se crke iz opazovane besede
		}
		
		System.out.println("V stavku se pojavljajo crke: ");
		vse.print();
		System.out.println("V vsaki besedi se pojavljajo crke: ");
		povsod.print();
	}
	
	public static Set createPowerSet(Set s)
	{
		// rezultat je mnozica mnozic
		Set result = new Set();
		
		// prvi element v rezultatu je prazna mnozica
		result.insert(new Set());
		
		// za vsak element iz izhodiscne mnozice...
		for (SetElement iterS = s.first(); !s.overEnd(iterS); iterS = s.next(iterS))
		{
			// ... pripravimo novo mnozico mnozic
			Set setOfSets = new Set();
			
			// za vsako ustvarjeno mnozico... 
			for (SetElement iterR = result.first(); !result.overEnd(iterR); iterR = result.next(iterR))
			{
				// naredimo kopijo trenutne mnozice
				Set tmp = new Set();
				tmp.union((Set)result.retrieve(iterR));
				
				// vanjo dodamo trenutni element
				tmp.insert(s.retrieve(iterS));
				
				// razsirjeno mnozico dodamo kot nov element v mnozico mnozic
				setOfSets.insert(tmp);
			}
			
			// pripravljeno mnozico mnozic dodamo v rezultat
			result.union(setOfSets);
		}
		
		return result;
	}
	
	public static void printPowerSet(Set p)
	{
		for (SetElement iter = p.first(); !p.overEnd(iter); iter = p.next(iter))
		{
			Set s = (Set)p.retrieve(iter);
			s.print();
		}
	}
	
	public static void main(String[] args) 
	{
		Set a = new Set();
		a.insert(1);
		a.print();
		
		a.delete(a.locate(1));
		a.print();
		
		a.insert(1);
		a.insert(2);
		a.insert(3);
		a.insert(2);
		a.insert(3);
		a.insert(4);
		a.print();
		
		crke("Abstraktni podatkovni tip");
		
		Set b = new Set();
		
		b.insert(10);
		b.insert(5);
		b.insert(3);
		
		System.out.print("Potencna mnozica mnozice ");
		b.print();
		
		Set p = createPowerSet(b);
		printPowerSet(p);
	}

}
