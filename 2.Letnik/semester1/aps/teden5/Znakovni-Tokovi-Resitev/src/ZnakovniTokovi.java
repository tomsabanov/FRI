import java.io.*;

public class ZnakovniTokovi {

	public static Student readStudent (BufferedReader reader) throws Exception
	{
		String line;
		Student student = null;
		
		line = reader.readLine();
		student = new Student(line);
		
		line = reader.readLine();
		int numOfEntries = Integer.parseInt(line);
				
		for (int j = 0; j < numOfEntries; j++)
		{
			line = reader.readLine();
			String[] tokens = line.split(",");
			
			Mark mark = new Mark(tokens[0], Integer.parseInt(tokens[1]));
			student.addMark(mark);
		}
		
		return student;
	}
	
	public static void readFile(String fileName, LinkedList list)
	{
		BufferedReader reader = null;
		
		try
		{
			String line;
			
			// Datoteko odpremo tako, da ustvarimo predmet reader
			reader = new BufferedReader(new FileReader(fileName));
			
			line = reader.readLine();
			int numOfStudents = Integer.parseInt(line);
			
			for (int i = 0; i < numOfStudents; i++)
			{
				Student student = readStudent(reader);
				if (student != null)
					list.add(student);
			}
        }
		catch (FileNotFoundException ex)
		{
			System.err.println("Ne najdem zahtevane datoteke!");
		}
		catch (IOException ex) 
		{
			System.err.println("Napaka pri branju datoteke");
        }
		catch (NumberFormatException ex)
		{
			System.err.println("Napaka pri zapisu stevila");
		}
		catch (Exception ex)
		{
			System.err.println("Napacni podatki");
		}
		finally 
		{
			try { if (reader != null) reader.close();}
			catch (IOException ex) { System.err.println("Napaka pri zapiranju datoteke");}
		}
		
	}
	
	public static void main(String[] args) 
	{
		LinkedList listOfStudents = new LinkedList();
		readFile("ocene.txt", listOfStudents);
		listOfStudents.print();
		
		System.out.println("\n\nSortiran seznam studentov (glede na povprecno oceno):\n");
		listOfStudents.sort();
		listOfStudents.print();
	}

}
