
public class Student extends ListElement implements Comparable
{
	private String name;
	private LinkedList marks;
	
	public Student(String name)
	{
		this.name = name;
		marks = new LinkedList();
	}
	
	public boolean addMark(Mark newMark)
	{
		for (ListNode curNode = marks.first(); !marks.overEnd(curNode); curNode = marks.next(curNode))
		{
			Mark curMark = (Mark)marks.retrieve(curNode);
			
			if (curMark.equals(newMark))
				return false;
		}
		
		marks.add(newMark);
		return true;
	}
	
	public void print()
	{
		System.out.println("Ime studenta: " + name);
		System.out.println("Opravljeni predmeti:");
		marks.print();
		System.out.println("Povprecna ocena: " + getAverageMark());
		System.out.println();
	}
	
	public double getAverageMark()
	{
		int sum = 0;
		int count = 0;
		
		for (ListNode curNode = marks.first(); !marks.overEnd(curNode); curNode = marks.next(curNode))
		{
			Mark curMark = (Mark)marks.retrieve(curNode);
			sum += curMark.getMark();
			count++;
		}
		
		if (count > 0)
			return (double)sum/count;
		else
			return 0;
	}
	
	public int compareTo(Object other)
	{
		Student otherStudent = (Student)other;
		
		double myAvg = getAverageMark();
		double otherAvg = otherStudent.getAverageMark();
		if (myAvg > otherAvg)
			return 1;
		else if (myAvg < otherAvg)
			return -1;
		else
			return 0;
	}
}
