#! /bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color


if [ -z $1 ]
then
	/bin/echo -e "${GREEN}Uporaba${NC}: ./test.sh ImeNaloge potDoTestnihDatotek omejitevCasa"
	/bin/echo -e "${GREEN}ImeNaloge${NC}: lahko daste NalogaN.java ali pa samo NalogaN - naloga se vam avtomaticno compila. Ce so tezave pri compilu, bo program uporabil prejsno delujoco verzijo"
	/bin/echo -e "${GREEN}potDoTestnihDatotek${NC}: kje se datoteke nahajajo recimo . ali ./testi ali testi...."
	/bin/echo -e "${GREEN}omejitevCasa${NC}: edini neobvezen parameter, default je nastavljen na 1s, ce zelite vecjo omejitev casa, nastimate kot Nm ali Ns kjer je N cifra, s so sekunde, m minute"
	/bin/echo -e "vrnjen cas vzemite z kanckom soli....namrec nimam pojma kateri cas oni gledajo..."
	exit
fi



PROGRAM=$1
PROGRAM_NAME=$PROGRAM
PROGRAM_NUM="${PROGRAM//[!0-9]/}"

TESTS=$(readlink -f $2)

CUSTOM=$(echo "$3" | tr '[:upper:]' '[:lower:]')

CUSTOM_INTERVAL_START=1
CUSTOM_INTERVAL_END=-1

CUSTOM_TIME='1s'

if [ ${PROGRAM: -5} == ".java" ]
then
	PROGRAM_NAME="${PROGRAM%.*}"
else
	PROGRAM="$PROGRAM.java"
fi

if [ ! -f $PROGRAM ]
then
	echo "File $PROGRAM does not exist"
	exit
fi

if [ ! -d $TESTS ]
then
	echo "Directory $TESTS does not exist"
	exit
fi

if [[ $CUSTOM == "i"  ]]; then
    #setup interval
    INTERVAL=$(echo $4 | grep -Eo '[0-9]+')
    arr=($INTERVAL)
    CUSTOM_INTERVAL_START=${arr[0]}
    CUSTOM_INTERVAL_END=${arr[1]}

fi

if [[ $CUSTOM == "t"  ]]; then
    #setup time
   CUSTOM_TIME=$4
   echo $CUSTOM_TIME
fi

if [[ $CUSTOM == "it"  ]]; then

    INTERVAL=$(echo $4 | grep -Eo '[0-9]+')
    arr=($INTERVAL)
    CUSTOM_INTERVAL_START=${arr[0]}
    CUSTOM_INTERVAL_END=${arr[1]}

    CUSTOM_TIME=$5
fi

javac $PROGRAM

index=$CUSTOM_INTERVAL_START
INPUT_base="${TESTS}/I" #I${PROGRAM_NUM}
OUTPUT_base="${TESTS}/O"

INPUT="${INPUT_base}_$index.txt"
INPUT_OUT="${TESTS}/out_${index}.txt"
OUTPUT="${OUTPUT_base}_$index.txt"

LOG="${TESTS}/out_${index}.log"


while [ -f $INPUT ]
do
	#first run program with $INPUT and $INPUT_OUT -> input_out is where results will be saved
	touch $INPUT_OUT
	TIME=`(time timeout $CUSTOM_TIME java $PROGRAM_NAME $INPUT $INPUT_OUT &> $LOG) 2>&1 | grep -E "real" | sed s/[a-z]//g`

	RUNTIME=0
	for i in $TIME; do RUNTIME=`echo "$RUNTIME + $i"|bc`; done

	rm $LOG
	RESULTS=$(diff -w -b --strip-trailing-cr $INPUT_OUT $OUTPUT)

	rm $INPUT_OUT

	if [ -z "$RESULTS" ]
	then
		/bin/echo -e "Testni primer: $index ...................... ${GREEN}OK${NC} ${RUNTIME} "
	else
		/bin/echo -e "Testni primer: $index ...................... ${RED}NOPE${NC}  ${RUNTIME} "
	fi

    if [ $index == $CUSTOM_INTERVAL_END ];then
        break
    fi

	index=$((index+1))
	INPUT="${INPUT_base}_${index}.txt"
	INPUT_OUT="out_${index}.txt"
	OUTPUT="${OUTPUT_base}_${index}.txt"
	LOG="out_${index}.log"

done


