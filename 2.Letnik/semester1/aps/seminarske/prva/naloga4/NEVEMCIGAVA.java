import java.io.*;
import java.lang.reflect.Array;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Naloga4 {
    public static void main(String args[]) {
        if (args.length >= 2) {
            try (BufferedReader reader = new BufferedReader(new FileReader(args[0]))) {
                String line;
                ListArray<Integer> listArray = null;
                while ((line = reader.readLine()) != null) {
                    String[] values = line.split(",");
                    switch (values[0]) {
                        case "s":
                            listArray = new ListArray<>(Integer.parseInt(values[1]), Integer.class);
//                            System.out.println(listArray.toString().replaceAll("\n", "|")+"   :  " + line);
                            break;
                        case "i":
//                            System.out.println(listArray.toString().replaceAll("\n", "|")+"   :  " + line);
                            //noinspection ConstantConditions
                            listArray.insert(Integer.parseInt(values[2]), Integer.parseInt(values[1]));
                            break;
                        case "r":
//                            System.out.println(listArray.toString().replaceAll("\n", "|")+"   :  " + line);
                            //noinspection ConstantConditions
                            listArray.remove(Integer.parseInt(values[1]));
                            break;
                        default:
                            break;
                    }
                }

                try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(new File(args[1])))) {
                    //noinspection ConstantConditions
                    outputStreamWriter.write(listArray.toString());
                } catch (IOException e) {
                    System.exit(1);
                }
            } catch (IOException | NullPointerException ex) {
                System.err.println("Cannot read from file. Or file not formatted properly.");
                System.exit(-1);
            }
        }
    }
}

class ListArray<T> implements Iterable<T> {
    private LinkedList<T[]> linkedList = new LinkedList<>();
    private final Integer arraySize;
    private final Class<T> tClass;

    ListArray(int n, Class<T> tClass) {
        arraySize = n;
        this.tClass = tClass;
        //noinspection unchecked
        linkedList.addFirst((T[]) Array.newInstance(tClass, n));
    }

    @SuppressWarnings("WeakerAccess")
    public boolean insert(int position, T element) {
        Iterator<LinkedList.LinkedListElement<T[]>> linkedListElementIterator = linkedList.advancedIterator();
        int currentPosition = 0;
        // Find element
        while (linkedListElementIterator.hasNext()) {
            // Getting array
            LinkedList.LinkedListElement<T[]> linkedListElement = linkedListElementIterator.next();
            T[] array = linkedListElement.element;
            // Inserting element
            for (int i = 0; i < array.length && array[i] != null || position == 0; i++) {
                // If there is a free space in that block
                if (currentPosition == position - 1 && hasFreeSpace(array, i + 1)) {
                    insertAndMove(array, i + 1, element);
                    return true; // Element is inserted and everything is okay
                }
                if (currentPosition == position && hasFreeSpace(array, i + 1)) {
                    insertAndMove(array, i, element);
                    return true;
                }
                if (currentPosition == position && !hasFreeSpace(array, i + 1)) {
                    // Add new array
                    //noinspection unchecked
                    linkedListElement.next = new LinkedList.LinkedListElement<>(linkedListElement.next, linkedListElement,
                            (T[]) Array.newInstance(this.tClass, this.arraySize));
                    // Fix also next element of current new (linkedListElement.next)
                    if (linkedListElement.next.next != null) {
                        linkedListElement.next.next.previous = linkedListElement.next;
                    }

                    cutHalfToNewArray(array, linkedListElement.next.element);
                    // No time for optimization just repeat whole insertion
                    return insert(position, element);
                }
                currentPosition++;
            }
        }
        return false;
    }

    @SuppressWarnings({"WeakerAccess", "UnusedReturnValue"})
    public boolean remove(int position) {
        Iterator<LinkedList.LinkedListElement<T[]>> linkedListElementIterator = linkedList.advancedIterator();
        int currentPosition = 0;
        // Find element
        while (linkedListElementIterator.hasNext()) {
            // Getting array
            LinkedList.LinkedListElement<T[]> linkedListElement = linkedListElementIterator.next();
            T[] array = linkedListElement.element;
            // Inserting element
            for (int i = 0; i < array.length && array[i] != null; i++) {
                if (currentPosition == position) {
                    int newSize = deleteAndMove(array, i);
                    int elementsToCopy = arraySize / 2 - newSize;
                    if (elementsToCopy > 0) {
                        if (linkedListElement.next != null) {
                            newSize = cutElementsFromBeginning(linkedListElement.next.element, array, elementsToCopy, arraySize / 2 - elementsToCopy);
                            // we have to delete this block.
                            if (newSize < arraySize / 2) {
                                cutElementsFromBeginning(linkedListElement.next.element, array, arraySize, arraySize / 2);
                                // Delete element
                                linkedListElement.next = linkedListElement.next.next;
                                if (linkedListElement.next != null) {
                                    linkedListElement.next.previous = linkedListElement;
                                }
                            }
                        }
                    }
                    return true;
                }
                currentPosition++;
            }
        }
        return false;
    }

    /**
     * Cuts <i>elementsToCopy</i> elements from one array beginning and moves them to others's first half.
     *
     * @param arrayFrom      array to copy from.
     * @param arrayTo        array to copy to.
     * @param elementsToCopy number of elements to be copied.
     * @return new size of <i>arrayFrom</i> array.
     */
    private int cutElementsFromBeginning(T[] arrayFrom, T[] arrayTo, int elementsToCopy, int position) {
        // Copy elements
        for (int i = 0; i < elementsToCopy && arrayFrom[i] != null; i++) {
            arrayTo[position + i] = arrayFrom[i];
            arrayFrom[i] = null;
        }
        // Move the rest of elements
        int j;
        for (j = elementsToCopy; j < arrayFrom.length && arrayFrom[j] != null; j++) {
            arrayFrom[j - elementsToCopy] = arrayFrom[j];
        }
        arrayFrom[j-elementsToCopy] = null;
        return j - elementsToCopy;
    }

    /**
     * Deletes elements and shift the rest of them to left.
     *
     * @param array    array from which are we deleting
     * @param position position of element to be deleted
     * @return new size of array
     */
    private int deleteAndMove(T[] array, int position) {
        array[position] = null;
        int i;
        for (i = position; i < array.length - 1 && array[i + 1] != null; i++) {
            array[i] = array[i + 1];
        }
        array[i] = null;
        return i;
    }

    private void cutHalfToNewArray(T[] arrayFrom, T[] arrayTo) {
        for (int i = 0; i < arrayTo.length / 2 + ((arrayTo.length%2==0)?0:1); i++) {
            // Copy
            arrayTo[i] = arrayFrom[arrayFrom.length / 2 + i];
            // Remove element from first array
            arrayFrom[arrayFrom.length / 2 + i] = null;
        }
    }

    private void insertAndMove(T[] array, int index, T element) {
        T nextElement = array[index];
        array[index] = element;
        // Move all elements
        for (int j = index + 1; j < array.length && nextElement != null; j++) {
            T temp = nextElement;
            nextElement = array[j];
            array[j] = temp;
        }

    }

    private boolean hasFreeSpace(T[] array, int startIndex) {
        for (int i = startIndex; i < array.length; i++) {
            if (array[i] == null) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            Iterator<T[]> linkedListIterator = linkedList.iterator();
            T[] currentList = linkedListIterator.hasNext() ? linkedListIterator.next() : null;
            Integer currentIndex = -1;
            T current = currentList != null ? currentList[0] : null;

            @Override
            public boolean hasNext() {
                return currentIndex + 1 < arraySize || linkedListIterator.hasNext();
            }

            @Override
            public void remove() {
                if (currentList != null) {
                    currentList[currentIndex] = null;
                    for (int i = currentIndex; i < currentList.length; i++) {
                        currentList[i] = currentList[i + 1];
                        if (currentList[i] == null) {// End of block
                            break;
                        }
                    }
                }
            }

            @Override
            public T next() {
                currentIndex++;
                if (currentIndex < arraySize) {
                    current = currentList[currentIndex];
                } else if (linkedListIterator.hasNext()) {
                    currentList = linkedListIterator.next();
                    currentIndex = 0;
                    current = currentList[currentIndex];
                } else {
                    current = null;
                }
                return current;
            }
        };
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        int currentIndex = 0;
        for (T t : this) {
            currentIndex++;
            s.append(t).append(currentIndex % arraySize == 0 ? "\n" : ",");
        }
        return currentIndex / arraySize + "\n" + s.toString().replaceAll("null", "NULL");
    }
}

class LinkedList<T> implements Iterable<T> {
    /**
     * Element of first will be always null.
     */
    private LinkedListElement<T> first = new LinkedListElement<>();

    /**
     * Add new element to first position.
     *
     * @param element Element to be added.
     */
    void addFirst(T element) {
        first.next = new LinkedListElement<>(first.next, first, element);
        if (first.previous != null) {
            first.previous.previous = first.next;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            Iterator<LinkedListElement<T>> advancedIterator = advancedIterator();

            @Override
            public boolean hasNext() {
                return advancedIterator.hasNext();
            }

            @Override
            public T next() {
                return advancedIterator.next().element;
            }

            @Override
            public void remove() {
                advancedIterator.remove();
            }
        };
    }

    Iterator<LinkedListElement<T>> advancedIterator() {
        return new Iterator<LinkedListElement<T>>() {
            LinkedListElement<T> current = first;

            @Override
            public boolean hasNext() {
                return current.next != null;
            }

            @Override
            public LinkedListElement<T> next() {
                current = current.next;
                return current;
            }

            @Override
            public void remove() {
                try {
                    current.previous.next = current.next;
                    current.next.previous = current.previous;
                } catch (NullPointerException e) {
                    throw new NoSuchElementException(current.element.toString());
                }
            }
        };
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("{");
        for (T t : this) {
            s.append(t.toString()).append(",");
        }
        s.append("}");
        return s.toString();
    }

    static class LinkedListElement<U> {
        LinkedListElement<U> next;
        LinkedListElement<U> previous;
        U element;

        LinkedListElement() {
        }

        LinkedListElement(LinkedListElement<U> next, LinkedListElement<U> previous, U element) {
            this.next = next;
            this.previous = previous;
            this.element = element;
        }

    }
}
