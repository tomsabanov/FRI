import java.io.File;
import java.util.Scanner;
import java.io.FileWriter;


class Node{
	Node next;
	int numberOfElements;
	int[] array;
	
	public Node(int N){
		this.next = null;
		this.array = new int[N];
	}	

	public Node(int N, Node next){
		this.array = new int[N];
		this.numberOfElements = 0;
		this.next = next;
	}
}

class DataStructure{
	private int maxElements;
	private int numberOfNodes = 0;

	Node first;

	private int numberOfElements=0; // index(id) of last element in DataStructure 


	public  DataStructure(){
		this.init(5);
	}
	public  DataStructure(int N){
		this.init(N);
	}
	public void init(int N){
		this.maxElements = N;
		first = new Node(N,null);
		numberOfNodes = 1;
	}

	public boolean insert(int v, int p){
		//moramo dobiti node, v katerega moramo vstavit element
		int num = 0;
		Node current = first;
		int pos = p;
		while(current != null){
			num+=current.numberOfElements;
			if(p<=num || p == 0){
				//vstavit potrebujemo v ta Node
				int y = pos;
				return	this.insertIntoNode(v,y, current); // we transform the logical position of an element to a physical position  	
			}
			pos=pos - current.numberOfElements;
			current = current.next;
		}
	/*	while(pos > this.maxElements && current!=null){
			pos = pos - current.numberOfElements;
			current = current.next;
		}*/
		return this.insertIntoNode(v,pos,current);
	}
	boolean insertIntoNode(int v,int pos, Node current){
		if(pos == this.maxElements){
			if(current.next == null) return false;
			return this.insertIntoNode(v,0,current.next);
		}
		if(current.numberOfElements == maxElements){
			//create new node
			Node newNode = new Node(this.maxElements, current.next);
			current.next = newNode;

			//splitting
			//newNode.numberOfElements = this.maxElements - this.maxElements/2;
			//current.numberOfElements = this.maxElements/2;
			int numToCopy = this.maxElements - this.maxElements/2;
			int src = current.numberOfElements/2 ;
			//if(this.maxElements%2 == 0) src--;
			System.arraycopy(current.array,src, newNode.array, 0, numToCopy);
			for(int i = src; i<src+numToCopy;i++){
				current.array[i] = 0;
			}
			this.numberOfNodes++;
			
			current.numberOfElements = this.maxElements/2;
			newNode.numberOfElements = this.maxElements - this.maxElements/2;
			if(pos>current.numberOfElements) return this.insertIntoNode(v,pos-current.numberOfElements , newNode);
			return this.insertIntoNode(v,pos,current);

		}
		//insert (and shift) in current node
		if(current.array[pos] == 0){ // potrebno bo spremeniti v -1, ce so elementi lahko tudi enaki 0....
			current.array[pos] = v;
		}
		else{
			//shiftamo od pos naprej (vkljucno z pos)
			System.arraycopy(current.array,pos,current.array,pos+1,current.numberOfElements - pos);
			current.array[pos] = v;
		}
		current.numberOfElements++;
		this.numberOfElements++;
		
		return true;
	}

	public boolean remove(int p){
		int num = 0;
		Node current = first;
		int pos = p;
		while(current != null){
			num+=current.numberOfElements;
			if(p<num || p == 0){
				int y = pos;
				return this.removeFromNode(y, current);
			}
			pos = pos - current.numberOfElements;
			current = current.next;
		}	
		return false;
	}
	public boolean removeFromNode(int pos, Node current){
		current.array[pos] = 0; //removed element;
		current.numberOfElements--;
		
	        this.numberOfElements--;
		if(pos<current.numberOfElements){
			//potrebno shiftat
			this.shiftLeft(current, pos+1, pos, current.numberOfElements - pos);
			//iSystem.arraycopy(current.array, pos+1, current.array, pos,current.numberOfElements - pos);  
		}

		int x = this.maxElements/2 - current.numberOfElements; //stevilo elementov, ki jih bomo prestavil x >= max/2 - num
		if(current.numberOfElements < this.maxElements/2 && current.next != null){
			//vzamemo elemente iz naslednjega clena
			//pogledamo, ce jih je tudi v naslednjem clenu manj kot n/2 -> vzamemo vse
			//drugace vzamemo toliko, da pridemo cez n/2
			if(current.next.numberOfElements-x < this.maxElements/2){
				//kopiramo vse
				System.arraycopy(current.next.array, 0, current.array, current.numberOfElements, current.next.numberOfElements);
				current.numberOfElements+=current.next.numberOfElements;
				current.next = current.next.next;
				this.numberOfNodes--;
			}
			else{
				System.arraycopy(current.next.array, 0, current.array, current.numberOfElements, x);
				//potrebno shiftat levo se naslednji node 
				this.shiftLeft(current.next, x, 0, current.next.numberOfElements - x);
			        current.next.numberOfElements -= x;	
				current.numberOfElements += x;
			}
		
		}

		return true;
	}

	public boolean shiftLeft(Node current, int  src, int dest, int num){
		for(int i = src; i<src+num;i++){
			current.array[dest] = current.array[i];
			current.array[i] = 0;
			dest++;
		}
		return true;
	}

	public boolean dump(String output){
		try{
		FileWriter writer = new FileWriter(output); 
	       	writer.write(this.numberOfNodes+ "\n");
		 Node current = this.first;
		 while(current != null){
			 String s = "";
			for(int j = 0; j<this.maxElements; j++){
				if(current.array[j] != 0){
					if(j!=0){
				        	s += "," + current.array[j];
					}
					else{
				        	s += current.array[j];
					}
				}
				else{
					s += ",NULL";
				}
			}
			writer.append(s+ "\n");
			current = current.next;
		 }
		 writer.close();
		}
		catch(Exception e){
			return false;
		}
		return true;
	}
}



public class Naloga4{

	public static void main(String[] args){
		String input = args[0];
		String output = args[1];
		File in = new File(input);
		
		try{
			Scanner sc = new Scanner(in);
			sc.useDelimiter("\\D");
			int K = Integer.parseInt(sc.nextLine());
			readAndExecuteCommands(sc, K, output);
		}	
		catch(Exception e){
			System.out.println(e);
		}

	}

	public static void readAndExecuteCommands(Scanner sc, int K, String output){
		DataStructure data=new DataStructure();
		for(int i = 0; i<K ;i++){
			String command = sc.nextLine();
			String[] parameters = command.split(",");
			switch(parameters[0]){
				case "s":
					int n = Integer.parseInt(parameters[1]);
					data = new DataStructure(n);
					break;		
				case "i":
					int v = Integer.parseInt(parameters[1]);
					int p = Integer.parseInt(parameters[2]);
					data.insert(v,p);
				//	data.dump("out.txt");
					break;
				case "r":
					int position = Integer.parseInt(parameters[1]);
					data.remove(position);
				//	data.dump("out.txt");
					break;
			}
		}
		data.dump(output);
	}

}
				
