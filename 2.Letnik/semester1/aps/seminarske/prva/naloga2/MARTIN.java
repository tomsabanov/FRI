import java.io.*;
public class Naloga2{
	int kapaciteta;
	ListElement first;
	HmElement[] hm;
	int hmVel;

	public void init(int size){
		this.kapaciteta = size;
		this.first = null;
		this.hmVel = size/4;
		this.hm = new HmElement[hmVel];
	}
	public boolean alloc(int size, int id){
		int index = id%this.hmVel;
		//System.out.print("index: "+index);

		HmElement entry = this.hm[index];
		HmElement temp = entry;
		HmElement prev = temp;
		boolean nes = true;
		if(entry!=null){
			//System.out.print(" "+entry.id);
			while(temp!=null){
				if(temp.id==id){
					return false;
				}
				prev = temp;
				temp = temp.next;
			}
		}else{
			this.hm[index] = new HmElement(id);
			//System.out.print(" "+this.hm[index].id);
			nes = false;
		}
		/*if(obstaja(id))
			return false;*/
		if(first==null){
			first = new ListElement(id,size,0);
			if(nes)
				prev.next=new HmElement(id);
			return true;
		}
		ListElement start = first;
		if(first.start>=size){
			ListElement clen = new ListElement(id,size,0);
			ListElement temp2 = first;
			first=clen;
			first.next=temp2;
			if(nes)
				prev.next=new HmElement(id);
			return true;
		}
		if(start.next==null){
			//prever kapaciteto
			if(start.start+start.size+size>this.kapaciteta)
				return false;
			ListElement clen = new ListElement(id,size,start.start+start.size);
			start.next=clen;
			if(nes)
				prev.next=new HmElement(id);
			return true;
		}
		while(start.next!=null){
			if(start.next.start-(start.start+start.size+1)>=size-1){
				break;
			}
			start = start.next;
		}
		//na koncu preverja če dovolj placa
		if(start.next==null){
			if(start.start+start.size+size>this.kapaciteta){
				return false;
			}
		}
		ListElement clen = new ListElement(id,size,(start.start+start.size));
		clen.next = start.next;
		start.next = clen;
		if(nes)
			prev.next=new HmElement(id);
		return true;
				
	}
	public int free(int id){
		int index = id%this.hmVel;
		HmElement entry = this.hm[index];
		//System.out.print("index: "+index);
		if(entry!=null){
			HmElement temp = entry;
			HmElement prev = temp;
			if(entry.next == null&&entry.id==id){
				this.hm[index] = null;
			}else if(entry.next!=null&&entry.id==id){
				this.hm[index] = entry.next;
			}else{
				while(temp!=null&&temp.id!=id){
					prev = temp;
					temp = temp.next;
				}
				prev.next = temp.next;
			}
		}

		ListElement pred = first;
		if(first.next==null)
			if(pred.id==id){
				int siz = first.size;
				first=null;
				return siz;
			}
		if(first.id==id){
			int siz = first.size;
			first=first.next;
			return siz;
		}
		ListElement naslednik = first.next;
		int size = 0;
		while(naslednik!=null){
			if(naslednik.id==id){
				pred.next = naslednik.next;
				size = naslednik.size;
				break;
			}
			pred = naslednik;
			naslednik = naslednik.next;
		}
		return size;
	}
	public void defrag(int n){
		//optimiziri
		int index = 0;
		if(first.start>0){
			first.start = 0;
			n--;
		}
		ListElement zacetek = first;
		for(int i = 0;i<n;i++){
			ListElement temp = zacetek;
			while(temp.next!=null){
				if(temp.next.start-(temp.start+temp.size)>0){
					temp.next.start=temp.start+temp.size;
					zacetek = temp.next;
					break;
				}
				temp=temp.next;
			}

		}
	}
	public boolean obstaja(int id){
		ListElement temp = first;
		while(temp!=null){
			if(temp.id == id)
				return true;
			temp = temp.next;
		}
		return false;
	}
	public void izpis(){
		ListElement temp = first;
		while(temp!=null){
			System.out.print("(id:"+temp.id+"|"+temp.start+"-"+(temp.start+temp.size-1)+")");
			temp = temp.next;
		}
		System.out.println();
	}
	public static void main(String[] args){
		Naloga2 nal2 = new Naloga2();
		String vhod = args[0];
		String izhod = args[1];
      	BufferedReader reader;
      	try{
      		reader = new BufferedReader(new FileReader(vhod));
      		int n = Integer.parseInt(reader.readLine());
      		for(int i = 0;i<n;i++){
      			String[] ukaz = (reader.readLine()).split(",");
      			//System.out.println(i);
      			switch(ukaz[0].charAt(0)){
      				case 'i': {
      					nal2.init(Integer.parseInt(ukaz[1]));
      				}     				
      					break;
      				case 'a':{
      					//System.out.print("a: "+ukaz[2]+" ");
      					int a = Integer.parseInt(ukaz[1]);
      					int b = Integer.parseInt(ukaz[2]);
      					nal2.alloc(a,b);
      				}
      					break;
      				case 'f':{
      					//System.out.print("f: "+ukaz[1]+" ");
      					nal2.free(Integer.parseInt(ukaz[1]));
      				}
      					break;
      				case 'd':{;
      					//System.out.print("d: ");
      					nal2.defrag(Integer.parseInt(ukaz[1]));
      				}
      			}
      			//nal2.izpis();

      		}
      		
      		//System.out.println(Arrays.toString(array));
      	}catch(Exception e){
      		System.out.println("neki narobe---"+e.getClass());
      	}

      	try{
      		FileWriter fw = new FileWriter(izhod);
      		BufferedWriter bw = new BufferedWriter(fw);
      		//bw.write("what");
      		/*for(int i = 0;i<array.length;){
      			int id = array[i];
      			int start = i;
      			int konc = i;
      			while(i<array.length&&array[i]==id)
      				i++;
      			konc = i;
      			if(id!=0){
	      			String toBe = id+","+start+","+(konc-1);
	      			//System.out.println(toBe);
	      			bw.write(toBe);
	      			bw.newLine();
      			}
      			i=konc;*/
      		
      		ListElement temp = nal2.first;
			while(temp!=null){
				String toPrint = temp.id+","+temp.start+","+(temp.start+temp.size-1);
				bw.write(toPrint);
				bw.newLine();
				temp = temp.next;
			}
	      		bw.flush();
	    }catch(Exception e){

	    }
		
	}
	class ListElement{
		int id;
		int size;
		int start;
		ListElement next;
		ListElement(int id, int size,int start){
			this.id = id;
			this.size = size;
			this.next = null;
			this.start = start;
		}
	}
	class HmElement{
		int id;
		HmElement next;
		HmElement(int id){
			this.id = id;
			this.next = null;
		}
	}
}