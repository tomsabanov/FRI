import java.io.*;
import java.util.Arrays;
import java.util.Iterator;

public class Naloga2 {
    public static void main(String[] args) {
        String outputFileName = args[1];
        Memory memory = new Memory();
        // Generate size structure
        try (BufferedReader reader = new BufferedReader(new FileReader(args[0]))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] values = line.split(",");
                String action = values[0];
                switch (action) {
                    case "i":
                        memory.init(Integer.parseInt(values[1]));
                        break;
                    case "a":
                        memory.alloc(Integer.parseInt(values[1]), Integer.parseInt(values[2]));
                        break;
                    case "f":
                        memory.free(Integer.parseInt(values[1]));
                        break;
                    case "d":
                        memory.defrag(Integer.parseInt(values[1]));
                        break;
                }
            }
            // Find shortest path
            try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(new File(outputFileName)))) {
                outputStreamWriter.write(memory.toString());
            } catch (IOException e) {
                System.exit(1);
            }
        } catch (IOException e) {
            System.exit(1);
        }

    }
}

class Memory {
    private HashMap hashMap;

    @SuppressWarnings("WeakerAccess")
    public void init(int size) {
        hashMap = new HashMap(size);
    }

    @SuppressWarnings("UnusedReturnValue")
    public boolean alloc(int size, int id) {
        Iterator<HashMapElement> iter = hashMap.iterator();
        HashMapElement el = null;
        HashMapElement pel = null;
        if (iter.hasNext()) {
            el = pel = iter.next();
            if (pel.position >= size) {
                hashMap.add(0, id, size);
                return true;
            }
        }
        while (iter.hasNext()) {
            el = iter.next();
            if (el.position - pel.position - pel.size >= size) {
                hashMap.add(pel.position + pel.size, id, size);
                return true;
            }
            pel = el;

        }
        if (el == null && size <= hashMap.size) {
            hashMap.add(0, id, size);
            return true;
        }
        //noinspection ConstantConditions
        if (el.position + el.size + size <= hashMap.size) {
            hashMap.add(el.position + el.size, id, size);
            return true;
        }
        return false;
    }

    @SuppressWarnings("UnusedReturnValue")
    public int free(int id) {
        for (HashMapElement current : hashMap) {
            if (current.index == id) {
                hashMap.remove(current);
                return current.size;
            }
        }
        return 0;
    }

    @SuppressWarnings("WeakerAccess")
    public void defrag(int n) {
        Iterator<HashMapElement> iterator = hashMap.iterator();
        int currentEnd = 0;
        while (iterator.hasNext() && n > 0) {
            HashMapElement element = iterator.next();
            if (element.position - currentEnd > 0) {
                hashMap.remove(element);
                hashMap.add(currentEnd, element.index, element.size);
                currentEnd += element.size;
                n--;
            } else {
                currentEnd = element.position + element.size;
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder r = new StringBuilder();
        for (HashMapElement current : hashMap) {
            r.append(current.index).append(",").append(current.position).append(",").append(current.position + current.size - 1).append("\n");
        }
        return r.toString();
    }
}

class HashMap implements Iterable<HashMapElement> {
    private LinkedList[] linkedLists;
    final Integer size;

    HashMap(Integer size) {
        this.size = size;
        linkedLists = new LinkedList[size / 4 + 1];
    }

    public void add(int i, int id, int size) {
        int uu = i / linkedLists.length;
        if (linkedLists[uu] == null) {
            linkedLists[uu] = new LinkedList();
        }
        linkedLists[uu].add(i, id, size);
    }

    @Override
    public String toString() {
        return "{" + Arrays.toString(linkedLists) +
                '}';
    }

    @Override
    public Iterator<HashMapElement> iterator() {
        return new HashMapIterator();
    }

    class HashMapIterator implements Iterator<HashMapElement> {
        private int currentIndex = 0;
        private LinkedListElement currentElement = linkedLists[0] == null ? null : linkedLists[0].first;

        HashMapIterator() {
            while (currentIndex < linkedLists.length) {
                if (linkedLists[currentIndex] != null && linkedLists[currentIndex].first != null) {
                    currentElement = linkedLists[currentIndex].first;
                    break;
                }
                currentIndex++;
            }
        }

        @Override
        public boolean hasNext() {
            return currentElement != null;
        }

        @Override
        public HashMapElement next() {
            LinkedListElement r = currentElement;
            currentElement = currentElement.next;
            if (currentElement == null) {
                currentIndex++;
                while (currentIndex < linkedLists.length) {
                    if (linkedLists[currentIndex] != null && linkedLists[currentIndex].first != null) {
                        currentElement = linkedLists[currentIndex].first;
                        break;
                    }
                    currentIndex++;
                }

            }
            return new HashMapElement(r);
        }
    }
    @SuppressWarnings("UnusedReturnValue")
    boolean remove(HashMapElement current) {
        int uu = current.position / linkedLists.length;
        return linkedLists[uu].remove(current.position);
    }
}

class HashMapElement {
    int position;
    int index;
    int size;

    @SuppressWarnings("unused")
    public HashMapElement(int position, int index, int size) {
        this.position = position;
        this.index = index;
        this.size = size;
    }

    HashMapElement(LinkedListElement r) {
        this.size = r.size;
        this.index = r.index;
        this.position = r.position;
    }

    @Override
    public String toString() {
        return "{" + index + ":" + position + "," + (position + size) + "}";
    }
}

class LinkedList {

    public void add(int i, int id, int size) {
        // binary search
        LinkedListElement current = this.first;
        while (current != null) {
            if (current.position < i) {
                current = current.next;
            } else if (current.position == i) {
                current.size = size;
                current.index = id;
                return;
            } else {
                LinkedListElement newElement = new LinkedListElement(id, size, current.prev, current, i);
                if (current.prev != null) {
                    current.prev.next = newElement;
                } else {
                    this.first = newElement;
                }
                current.prev = newElement;
                return;
            }

        }
        if (this.first == null) {
            this.last = this.first = new LinkedListElement(id, size, null, null, i);
        } else {
            this.last.next = new LinkedListElement(id, size, this.last, null, i);
            this.last = last.next;
        }
    }

    LinkedListElement first = null;
    private LinkedListElement last = null;

    @Override
    public String toString() {
        StringBuilder r = new StringBuilder("L {");
        LinkedListElement current = this.first;
        while (current != null) {
            r.append(current.toString());
            current = current.next;
        }
        r.append("}");
        return r.toString();
    }

    public LinkedListElement get(int i) {
        LinkedListElement current = this.first;
        while (current != null) {
            if (current.position < i) {
                current = current.next;
            } else if (current.position == i) {
                return current;
            } else {
                return null;
            }
        }
        return null;
    }

    public boolean remove(int i) {
        LinkedListElement current = this.first;
        while (current != null) {
            if (current.position < i) {
                current = current.next;
            } else if (current.position == i) {
                if (current.prev != null) {
                    current.prev.next = current.next;
                } else {
                    this.first = current.next;
                }
                if (current.next != null) {
                    current.next.prev = current.prev;
                } else {
                    this.last = current.prev;
                }
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
}

class LinkedListElement {
    Integer index;
    Integer size;
    LinkedListElement prev;
    LinkedListElement next;
    Integer position;

    LinkedListElement(Integer index, Integer size, LinkedListElement prev, LinkedListElement next, Integer position) {
        this.index = index;
        this.size = size;
        this.prev = prev;
        this.next = next;
        this.position = position;
    }

    @Override
    public String toString() {
        return "(" + index + ": " + position + ", " + (position + size) + ')';
    }
}