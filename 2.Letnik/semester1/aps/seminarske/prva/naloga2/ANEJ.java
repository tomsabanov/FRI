import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.FileWriter;

public class Naloga2 {

	public static void main(String[] args) {

	    long start = System.currentTimeMillis();
	    
		Memmory mem = new Memmory();

		try {
			
			BufferedReader bf = new BufferedReader(new FileReader(args[0]));
			String line;

			line = bf.readLine();
			String line1[] = line.split(",");
			int n = Integer.parseInt(line1[0]);
			
			for (int ii = 0; ii < n; ii++) {
				line = bf.readLine();
				String func[] = line.split(",");

					switch (func[0].charAt(0)) {
					case 'i':
						mem.init(Integer.parseInt(func[1]));
					break;
					case 'a':
						mem.alloc(Integer.parseInt(func[1]), Integer.parseInt(func[2]));
					break;
					case 'f':
						mem.free(Integer.parseInt(func[1]));
					break;
					case 'd':
						mem.defrag(Integer.parseInt(func[1]));
					break;
				}
			}

			bf.close();

			// PrintWriter pw = new PrintWriter(args[1]);
			FileWriter fw = new FileWriter(args[1]);
			// mem.printState(pw);
			mem.printState(fw);
			// pw.close();
			fw.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	    long end = System.currentTimeMillis();;
	    System.out.printf("The program ran for %d ms.\n", end - start);
	    
	}

	private static class Memmory {
		private VList variables;
		private int size;

		public void init (int size) {
			this.size = size;
			this.variables = new VList(size);
		}

		public boolean alloc(int size, int id) {
			if (size > this.size || this.variables.contains(id)) return false;

			if (this.variables.empty() || this.variables.retrieve(this.variables.first()).location >= size) {
				
				this.variables.insert(null, id, 0, size);

			} else {

				VListElement p = this.findSpace(size);
				if (p == null) return false;

				Variable v = this.variables.retrieve(p);
				this.variables.insert(p, id, v.location + v.size, size);
			}

			return true;
		}

		public int free (int id) {
			VListElement el = this.variables.locate(id);
			if (el == null) return 0;

			int removed = el.data.size;

			this.variables.delete(el);

			return removed;
		}

		public void defrag (int n) {
				
			VListElement variableAfterFreeSpace = this.firstVariableAfterFreeSpace(this.variables.first());

			for (int ii = 0; ii < n; ii++) {

				variableAfterFreeSpace = this.firstVariableAfterFreeSpace(variableAfterFreeSpace);
				if (variableAfterFreeSpace == null) return;

				Variable v = this.variables.retrieve(this.variables.previous(variableAfterFreeSpace));

				this.move(variableAfterFreeSpace, variableAfterFreeSpace == this.variables.first() ? 0 : v.location + v.size);

			}
		}

		private void move (VListElement el, int ix) {
			el.data.location = ix;
		}

		public VListElement findSpace (int size) {

			VListElement iter = this.variables.first();
			if (iter == null) return null;

			while (!this.variables.overEnd(this.variables.next(iter))) {

				Variable prev = this.variables.retrieve(iter);
				Variable next = this.variables.retrieve(this.variables.next(iter));

				int spaceAvailable = next.location - (prev.location + prev.size);
				if (spaceAvailable >= size) return iter;

				iter = this.variables.next(iter);
			}

			Variable v = this.variables.retrieve(iter);
			if (this.size - (v.location + v.size) >= size) return iter;
			
			return null;
		}

		private VListElement firstVariableAfterFreeSpace (VListElement from) {

			VListElement iter = from;
			if (iter == null) return null;

			if (iter == this.variables.first()) {
				if (this.variables.retrieve(iter).location > 0) return iter;
				else iter = this.variables.next(iter);
			}

			while (!this.variables.overEnd(iter)) { 
				Variable prev = variables.retrieve(variables.previous(iter));
				Variable curr = variables.retrieve(iter);

				if (prev.location + prev.size != curr.location) return iter;

				iter = this.variables.next(iter);
			}

			return null;
		}
		
		public void printState (PrintWriter pw) {
			for (VListElement tmp = this.variables.first(); !this.variables.overEnd(tmp); tmp = this.variables.next(tmp)) {
				pw.printf("%d,%d,%d\n", tmp.data.id, tmp.data.location, tmp.data.location + tmp.data.size - 1);
			}
		}
		
		public void printState (FileWriter fw) throws IOException {
			for (VListElement tmp = this.variables.first(); !this.variables.overEnd(tmp); tmp = this.variables.next(tmp)) {
				String s = String.format("%d,%d,%d\n", tmp.data.id, tmp.data.location, tmp.data.location + tmp.data.size - 1);
				fw.append(s);
			}
		}
	}

	private static class VList {
		private VListElement first, last;
		private IdHashMap ids;

		public VList (int sz) {
			this.ids = new IdHashMap(sz);
			this.makenull();
		}

		public void makenull () {
			this.first = null;
			this.last = null;
		}

		public VListElement first () {
			return this.first;
		}

		public VListElement last () {
			return this.last;
		}

		public VListElement next (VListElement pos) {
			return pos.next;
		}

		public VListElement previous (VListElement pos) {
			return pos.previous;
		}

		public boolean overEnd (VListElement pos) {
			return pos == null;
		} 

		public boolean empty () {
			return this.first == null;
		}

		public void insert (VListElement prev, int id, int startIx, int size) { // TODO: check
			
			VListElement nxt;
			if (prev != null) nxt = prev.next;
			else nxt = this.first;

			VListElement el = new VListElement (prev, nxt, id, startIx, size);

			if (prev != null) {
				if (prev.next != null) prev.next.previous = el;
				prev.next = el;
			} else if (this.first != null) {
				this.first.previous = el;
			}
			
			if (this.first == null) {
				this.first = el;
				this.last = el;
			} else if (prev == this.last) {
				this.last = el;
			}

			if (prev == null) {
				this.first = el;
			}

			this.ids.insert(id, el);
		}

		public void delete (VListElement pos) {

			if (pos == null) return;

			this.ids.delete(this.retrieve(pos).id);

			if (pos == this.last && pos == this.first) { // deleting the only one
				this.first = null;
				this.last = null;
				return;
			} 

			if (pos == this.last) {
				this.last = pos.previous;
				this.last.next = null;
				return;
			} 

			if (pos == this.first) {
				this.first = pos.next;
				this.first.previous = null;
				return;
			}
			
			pos.previous.next = pos.next;
			pos.next.previous = pos.previous;

		}

		public Variable retrieve (VListElement el) {
			return el != null ? el.data : null;
		}

		public VListElement locate (int id) {
			return this.ids.get(id);
		}

		public boolean contains (int id) {
			return this.ids.get(id) != null;
		}

	}
		
	private static class VListElement {
		private Variable data;
		private VListElement previous, next;

		public VListElement (VListElement prev, VListElement nxt, int id, int loc, int sz) {
			this.previous = prev;
			this.next = nxt;
			this.data = new Variable(id, loc, sz);
		}
	}
		
	private static class Variable {
		int id;
		int location;
		int size;

		public Variable (int id, int loc, int sz) {
			this.id = id;
			this.location = loc;
			this.size = sz;
		}
	}

	private static class IdHashMap {
		private Id ids[];

		public IdHashMap (int sz) {
			this.ids = new Id[sz / 100 + 1]; 
		}

		public void insert (int key, VListElement ptr) {
			int ix = Math.abs(key) % this.ids.length;

			if (this.ids[ix] == null) {
				this.ids[ix] = new Id(key, ptr);
			} else {
				Id iter = this.ids[ix];
				while (iter.next != null) iter = iter.next;
				iter.next = new Id(key, ptr);
				iter.next.previous = iter;
			}
		}

		public void delete (int key) {
			int ix = Math.abs(key) % this.ids.length;

			if (this.ids[ix] == null) return;

			Id iter = this.ids[ix];

			while (iter != null) {

				if (iter.id == key) {
					
					if (iter.previous == null) {
						this.ids[ix] = iter.next;
					} else {
						iter.previous.next = iter.next;
					}
					
					if (iter.next != null) {
						iter.next.previous = iter.previous;
					}

					return;
				}
				
				iter = iter.next;
			}

		}

		public VListElement get (int key) {
			int ix = Math.abs(key) % this.ids.length;

			if (this.ids[ix] == null) return null;

			Id iter = this.ids[ix];
			while (iter != null) {
				if (iter.id == key) return iter.ptr;
				iter = iter.next;
			}
			
			return null;
		}
	}

	private static class Id {
		int id = -1;
		Id previous = null, next = null;
		VListElement ptr;

		public Id (int id, VListElement ptr) {
			this.id = id;
			this.ptr = ptr;
		}
	}
}
