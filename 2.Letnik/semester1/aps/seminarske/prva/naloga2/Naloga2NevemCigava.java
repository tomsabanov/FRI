import java.io.*;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Spliterator;
import java.util.function.Consumer;

public class Naloga2 {
    public static void main(String[] args) {

        String inputFileName = args[0];
        String outputFileName = args[1];
        Memory memory = new Memory();
        try (Scanner s = new Scanner(new File(inputFileName))) {
            s.useDelimiter("[\\, \r\n]+");
            // Generate size structure
            int n = s.nextInt();
            while (n > 0) {
                n--;
                String action = s.next();
                switch (action) {
                    case "i":
                        memory.init(s.nextInt());
                        break;
                    case "a":
                        memory.alloc(s.nextInt(), s.nextInt());
                        break;
                    case "f":
                        memory.free(s.nextInt());
                        break;
                    case "d":
                        memory.defrag(s.nextInt());
                        break;
                }
            }
            // Find shortest path
            try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(new File(outputFileName)))) {
                outputStreamWriter.write(memory.toString());
            } catch (IOException e) {
                System.err.println("Cannot write to output file.");
                System.exit(1);
            }
        } catch (FileNotFoundException e) {
            System.err.println("Input file not found.");
            System.exit(1);
        }

    }
}

class Memory {
    HashMap hashMap;

    public void init(int size) {
        hashMap = new HashMap(size);
    }

    public boolean alloc(int size, int id) {
        Iterator<HashMapElement> iter = hashMap.iterator();
        HashMapElement el = null;
        HashMapElement pel = null;
        if (iter.hasNext()) {
            el = pel = iter.next();
            if (pel.position >= size) {
                hashMap.add(0, id, size);
                return true;
            }
        }
        while (iter.hasNext()) {
            el = iter.next();
            if (el.position - pel.position - pel.size >= size) {
                hashMap.add(pel.position + pel.size, id, size);
                return true;
            }
            pel = el;

        }
        if (el == null && size <= hashMap.size) {
            hashMap.add(0, id, size);
            return true;
        }
        if (el.position + el.size + size <= hashMap.size) {
            hashMap.add(el.position + el.size, id, size);
            return true;
        }
        return false;
    }

    public int free(int id) {
        Iterator<HashMapElement> iterator = hashMap.iterator();
        while (iterator.hasNext()) {
            HashMapElement current = iterator.next();
            if (current.index == id) {
                hashMap.remove(current);
                return current.size;
            }
        }
        return 0;
    }

    public void defrag(int n) {
        Iterator<HashMapElement> iterator = hashMap.iterator();
        int currentEnd = 0;
        while (iterator.hasNext() && n > 0) {
            HashMapElement element = iterator.next();
            if (element.position - currentEnd > 0) {
                hashMap.remove(element);
                hashMap.add(currentEnd, element.index, element.size);
                currentEnd += element.size;
                n--;
            } else {
                currentEnd = element.position + element.size;
            }
        }
    }

    @Override
    public String toString() {
        String r = "";
        Iterator<HashMapElement> iterator = hashMap.iterator();
        while (iterator.hasNext()) {
            HashMapElement currrent = iterator.next();
            r += currrent.index + "," + currrent.position + "," + (currrent.position + currrent.size - 1) + "\n";
        }
        return r;
    }
}

class HashMap implements Iterable<HashMapElement> {
    private LinkedList[] linkedLists;
    public final Integer size;

    public HashMap(Integer size) {
        this.size = size;
        linkedLists = new LinkedList[size / 4 + 1];
    }

    public void add(int i, int id, int size) {
        int uu = i / linkedLists.length;
        if (linkedLists[uu] == null) {
            linkedLists[uu] = new LinkedList();
        }
        linkedLists[uu].add(i, id, size);
    }

    public LinkedListElement get(int i) {
        int uu = i / linkedLists.length;
        if (linkedLists[uu] == null) {
            linkedLists[uu] = new LinkedList();
        }
        return linkedLists[uu].get(i);
    }

    @Override
    public String toString() {
        return "{" + Arrays.toString(linkedLists) +
                '}';
    }

    @Override
    public Iterator<HashMapElement> iterator() {
        return new HashMapIterator();
    }

    class HashMapIterator implements Iterator<HashMapElement> {
        private int currentListIndex = 0;
        private int currentIndex = 0;
        private LinkedListElement currentElement = linkedLists[0] == null ? null : linkedLists[0].first;

        public HashMapIterator() {
            while (currentIndex < linkedLists.length) {
                    if (linkedLists[currentIndex] != null && linkedLists[currentIndex].first != null) {
                        currentElement = linkedLists[currentIndex].first;
                        currentListIndex = 0;
                        break;
                    }
                    currentIndex++;
                }
        }

        @Override
        public boolean hasNext() {
            return currentElement != null;
        }

        @Override
        public HashMapElement next() {
            LinkedListElement r = currentElement;
            currentElement = currentElement.next;
            if (currentElement == null) {
                currentIndex++;
                while (currentIndex < linkedLists.length) {
                    if (linkedLists[currentIndex] != null && linkedLists[currentIndex].first != null) {
                        currentElement = linkedLists[currentIndex].first;
                        currentListIndex = 0;
                        break;
                    }
                    currentIndex++;
                }

            }
            return new HashMapElement(r);
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    @Override
    public void forEach(Consumer<? super HashMapElement> consumer) {

    }

    @Override
    public Spliterator<HashMapElement> spliterator() {
        return null;
    }

    public boolean remove(HashMapElement current) {
        int uu = current.position / linkedLists.length;
        return linkedLists[uu].remove(current.position);
    }
}

class HashMapElement {
    int position;
    int index;
    int size;

    public HashMapElement(int position, int index, int size) {
        this.position = position;
        this.index = index;
        this.size = size;
    }

    public HashMapElement(LinkedListElement r) {
        this.size = r.size;
        this.index = r.index;
        this.position = r.position;
    }

    @Override
    public String toString() {
        return "{" + index + ":" + position + "," + (position + size) + "}";
    }
}

class LinkedList {

    public void add(int i, int id, int size) {
        // binary search
        LinkedListElement current = this.first;
        while (current != null) {
            if (current.position < i) {
                current = current.next;
            } else if (current.position == i) {
                current.size = size;
                current.index = id;
                return;
            } else {
                LinkedListElement newElement = new LinkedListElement(id, size, current.prev, current, i);
                if (current.prev != null) {
                    current.prev.next = newElement;
                } else {
                    this.first = newElement;
                }
                current.prev = newElement;
                return;
            }

        }
        if (this.first == null) {
            this.last = this.first = new LinkedListElement(id, size, null, null, i);
        } else {
            this.last.next = new LinkedListElement(id, size, this.last, null, i);
            this.last = last.next;
        }
    }

    LinkedListElement first = null;
    LinkedListElement last = null;

    @Override
    public String toString() {
        String r = "L {";
        LinkedListElement current = this.first;
        while (current != null) {
            r += current.toString();
            current = current.next;
        }
        r += "}";
        return r;
    }

    public LinkedListElement get(int i) {
        LinkedListElement current = this.first;
        while (current != null) {
            if (current.position < i) {
                current = current.next;
            } else if (current.position == i) {
                return current;
            } else {
                return null;
            }
        }
        return null;
    }

    public boolean remove(int i) {
        LinkedListElement current = this.first;
        while (current != null) {
            if (current.position < i) {
                current = current.next;
            } else if (current.position == i) {
                if (current.prev != null) {
                    current.prev.next = current.next;
                } else {
                    this.first = current.next;
                }
                if (current.next != null) {
                    current.next.prev = current.prev;
                } else {
                    this.last = current.prev;
                }
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
}

class LinkedListElement {
    public Integer index;
    public Integer size;
    public LinkedListElement prev;
    public LinkedListElement next;
    public Integer position;

    public LinkedListElement(Integer index, Integer size, LinkedListElement prev, LinkedListElement next, Integer position) {
        this.index = index;
        this.size = size;
        this.prev = prev;
        this.next = next;
        this.position = position;
    }

    @Override
    public String toString() {
        return "(" + index + ": " + position + ", " + (position + size) + ')';
    }
}