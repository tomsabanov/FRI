import java.io.File;
import java.util.Scanner;
import java.io.FileWriter;

class Node{
	int id;
	int size;
	int x;

	Node next;
	Node prev;
	public Node(int id, int size,int x){
		this.id = id;
		this.size = size;
		this.x = x;
	}
	public Node(int id, int size, int x, Node prev, Node next){
		this(id,size,x);
		this.prev = prev;
		this.next = next;
	}

}

class DataStructure{
	int free;
	// size + free = maxSize
	Node first;

	public DataStructure(int size){
		this.init(size);	
	}

	public void init(int size){
		this.free = size;
	}
	public boolean alloc(int size, int id){
		if(size>this.free) return false;

		//check if this is first element to be inserted
		if(first == null){
			first = new Node(id, size, 0, null,null);
			this.free = this.free - size;
			return true;
		}


		//naprej preveriti ce ze obstaja spremenljivka z oznako id
		//med preverjanjem ze kar poskusamo najti plac
		Node current = first;
		Node prev = null;
		Node chosen  = null;
		
		int index = 0;
		while(current != null){
			if(current.id == id) return false;
			if(chosen == null && current.x - index >= size) {
				chosen = new Node(id, size, index,prev,current);	
			
			}
			if(current.next == null) break;
			index = current.x + current.size;
			prev = current;
			current = current.next;
		}

		if(chosen == null) {
			// appendamo chosen direktno na current
			current.next = new Node(id, size, current.x + current.size, current, null); 
			this.free = this.free - size;
			return true;
		}


		this.free = this.free - size;
		
		if(chosen.prev != null){
		       	chosen.prev.next = chosen;
		}
		else{
			//first element
			chosen.prev = null;
			first = chosen;
		}
		if(chosen.next != null) chosen.next.prev = chosen;
		
		return true;
	}	
	public int free(int id){
		
		Node current = first;
		Node prev = null;
		while(current != null){
			if(current.id == id){

				this.free = this.free + current.size;
				if(prev!=null){
					prev.next = current.next;
				}
				else{
					first = current.next;	
				}
				return current.size;
			}
			prev = current;
			current = current.next;

		}
		
		return 0;
	}
	public void defrag(int n){
		Node start = first;
		int index = 0;
		for(int i = 0; i<n;i++){
			Node current = start;
			while(current != null){
				if(current.x - index != 0){
					//nasli smo prazno mesto	
					current.x = index;

					//set start 
					start = current.next;
					index = index + current.size;
					break;
				}
				index = index + current.size;
				current = current.next;
			}
		}
	}

	public void dump(String output){
		try{
			FileWriter writer = new FileWriter(output);
			Node current = first;
			while(current!=null){
				String s = "" + current.id + "," + current.x + ","  + (current.x + current.size - 1); 
				writer.append(s + "\n");
				current = current.next;
			//	System.out.println(s);
			}
			//System.out.println();
			writer.close();
		}
		catch(Exception e){

		}
	}


}

public class Naloga2{

	public static void main(String[] args){
		String input = args[0];
		String output = args[1];
		File in = new File(input);
		
		try{
			Scanner sc = new Scanner(in);
			sc.useDelimiter("\\D");
			int N = Integer.parseInt(sc.nextLine());
			readAndExecuteCommands(sc, N, output);
		}	
		catch(Exception e){
			System.out.println(e);
		}

	}

	public static void readAndExecuteCommands(Scanner sc, int N, String output){
		DataStructure data = new DataStructure(0);
		for(int i = 0; i<N ;i++){
			String command = sc.nextLine();
			String[] parameters = command.split(",");
		//	System.out.println(command);
			switch(parameters[0]){
				case "i":
					int size  = Integer.parseInt(parameters[1]);
					data = new DataStructure(size);
					break;		
				case "a":
					int alloc_size = Integer.parseInt(parameters[1]);
					int alloc_id = Integer.parseInt(parameters[2]);
					data.alloc(alloc_size,alloc_id);
					break;
				case "f":
					int free_id = Integer.parseInt(parameters[1]);
					data.free(free_id);
					break;
				case "d":
					int n = Integer.parseInt(parameters[1]);
					data.defrag(n);
					break;
			}
			//data.dump(output);
			//System.out.println("****************");
		}
		data.dump(output);
	}

}
				
