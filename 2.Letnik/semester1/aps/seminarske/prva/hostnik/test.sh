#! /bin/bash

PROGRAM=$1
PROGRAM_NAME=$PROGRAM
PROGRAM_NUM="${PROGRAM//[!0-9]/}"

TESTS=$(readlink -f $2)

CUSTOM_TIME=$3


if [ ${PROGRAM: -5} == ".java" ]
then
	PROGRAM_NAME="${PROGRAM%.*}"
else
	PROGRAM="$PROGRAM.java"
fi

if [ ! -f $PROGRAM ]
then
	echo "File $PROGRAM does not exist"
	exit
fi

if [ ! -d $TESTS ] 
then
	echo "Directory $TESTS does not exist"
	exit
fi

if [ -z $CUSTOM_TIME ]
then
	CUSTOM_TIME="1s"
fi



RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color


javac $PROGRAM

index=1
INPUT_base="${TESTS}/I${PROGRAM_NUM}"
OUTPUT_base="${TESTS}/O${PROGRAM_NUM}"

INPUT="${INPUT_base}_$index.txt"
INPUT_OUT="${TESTS}/out_${index}.txt"
OUTPUT="${OUTPUT_base}_$index.txt"

LOG="${TESTS}/out_${index}.log"

while [ -f $INPUT ]
do
	#first run program with $INPUT and $INPUT_OUT -> input_out is where results will be saved
	touch $INPUT_OUT
	
	TIME=`(time timeout $CUSTOM_TIME java $PROGRAM_NAME $INPUT $INPUT_OUT &> $LOG) 2>&1 | grep -E "user|sys" | sed s/[a-z]//g`	
	
	RUNTIME=0
	for i in $TIME; do RUNTIME=`echo "$RUNTIME + $i"|bc`; done

	rm $LOG
	RESULTS=$(diff --strip-trailing-cr -w $INPUT_OUT $OUTPUT)

#	rm $INPUT_OUT

	if [ -z "$RESULTS" ] 
	then
		/bin/echo -e "Testni primer: $index ...................... ${GREEN}OK${NC} ${RUNTIME} "
	else
		/bin/echo -e "Testni primer: $index ...................... ${RED}NOPE${NC}  ${RUNTIME} "
	fi

	index=$((index+1))
	INPUT="${INPUT_base}_${index}.txt"
	INPUT_OUT="out_${index}.txt"
	OUTPUT="${OUTPUT_base}_${index}.txt"
	LOG="out_${index}.log"
done


