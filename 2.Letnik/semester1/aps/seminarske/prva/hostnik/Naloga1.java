import java.io.*;
import java.util.Arrays;
import java.util.Objects;
import java.util.Scanner;

public class Naloga1 {

    public static void main(String[] args) {
        String inputFileName = args[0];
        String outputFileName = args[1];
        try (Scanner s = new Scanner(new File(inputFileName))) {
            s.useDelimiter("[\\, \r\n]+");
            // Generate object structure
            Taxi taxi = new Taxi(s.nextInt(), s.nextInt(), s.nextInt());
            int numberOfPersons = s.nextInt();
            Person[] allPersons = new Person[numberOfPersons];
            while (numberOfPersons > 0) {
                numberOfPersons--;
                allPersons[numberOfPersons] = new Person(s.nextInt(), s.nextInt(), s.nextInt(), s.nextInt(), s.nextInt());
            }
            // Find shortest path
            PathSolution shortestPath = getShortestPath(taxi, allPersons, new PathSolution(allPersons.length, taxi));
            try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(new File(outputFileName)))) {
                outputStreamWriter.write(shortestPath.toString());
                System.out.println(shortestPath.toString());
            } catch (IOException e) {
                System.err.println("Cannot write to output file.");
                System.exit(1);
            }
        } catch (FileNotFoundException e) {
            System.err.println("Input file not found.");
            System.exit(1);
        }

    }

    private static PathSolution shortestPath;// Keeps track of current shortest solution to eliminate those that are bigger at start.

    private static PathSolution getShortestPath(Taxi taxi, Person[] persons, PathSolution solution) {
        if (solution.getMovements().length <= solution.getCurrentSize()) {
            return solution;
        }
        if (Naloga1.shortestPath != null && solution.getCurrentSize() >= Naloga1.shortestPath.getCurrentSize()) {
            return Naloga1.shortestPath;
        }
        // For each seat decide to add one new person or to let them go.
        PathSolution shortestPath = null;

        for (int seatsFilled = taxi.getSeatsFilled(); seatsFilled > 0; seatsFilled--) {
            Person currentPerson = taxi.getFirstPerson();
            // Let person go.
            solution.addMovement(new Movement(MovementType.EXIT, currentPerson));
            PathSolution removedSeatPath = getShortestPath(taxi, persons, solution);
            if (removedSeatPath != null && (shortestPath == null || shortestPath.compareTo(removedSeatPath) > 0)) {
                shortestPath = (PathSolution) removedSeatPath.clone();
                if (shortestPath.getMovements().length == shortestPath.getCurrentSize()) {
                    if (Naloga1.shortestPath == null || Naloga1.shortestPath.compareTo(shortestPath) > 0) {
                        Naloga1.shortestPath = shortestPath;
                    }
                }
            }
            taxi.addLastPerson(currentPerson); // Fix back
            solution.removeLastMovement();
        }
        if (taxi.hasAnySeatsLeft()) {
            // Add new person
            // What person should we add? Try all of them.
            for (Person person : persons) {
                if (!solution.containsPerson(person)) {// Person has already been in a taxi.
                    taxi.addLastPerson(person);
                    solution.addMovement(new Movement(MovementType.ENTER, person));
                    PathSolution personsPath = getShortestPath(taxi, persons, solution);
                    if (personsPath != null && (shortestPath == null || shortestPath.compareTo(personsPath) > 0)) {
                        shortestPath = ((PathSolution) personsPath.clone());
                        if (Naloga1.shortestPath.getMovements().length == shortestPath.getCurrentSize()) {
                            if (Naloga1.shortestPath == null || Naloga1.shortestPath.compareTo(shortestPath) > 0) {
                                Naloga1.shortestPath = shortestPath;
                            }
                        }
                    }
                    taxi.removeLastPerson(); // Fix back
                    solution.removeLastMovement();
                }
            }

        }


        return shortestPath;

    }
}


enum MovementType {
    ENTER, EXIT
}


class PathSolution implements Comparable<PathSolution>, Cloneable {

    Movement[] movements;
    Integer currentSize;
    Taxi taxi;
    private Integer currentPathLength = 0;

    public PathSolution(Integer numberOfPersons, Taxi taxi) {
        this.currentSize = 0;
        movements = new Movement[numberOfPersons * 2];
        this.taxi = taxi;
    }

    @Override
    public int compareTo(PathSolution solution) {
        if (solution == null) {
            return -1;
        }
        return Integer.compare(this.getLength(), solution.getLength());
    }

    public Integer getLength() {
        return currentPathLength;
    }

    public boolean containsPerson(Person person) {
        for (Integer i = 0; i < this.currentSize; i++) {
            if (movements[i].person.equals(person)) {
                return true;
            }
        }
        return false;
    }

    public void addMovement(Movement movement) {
        this.currentSize++;
        movements[this.currentSize - 1] = movement;
        Movement prevMovement;
        ManhattanPoint prevPoint;
        if (this.currentSize >= 2) {
            prevMovement = movements[this.currentSize - 2];
            prevPoint = prevMovement.getType() == MovementType.ENTER ? prevMovement.getPerson().getCurrentDestination() : prevMovement.getPerson().getFinalDestination();
        } else {
            prevPoint = taxi.getCurrentPosition();
        }
        switch (movement.getType()) {
            case ENTER:
                this.currentPathLength += prevPoint.distance(movement.person.getCurrentDestination());
                break;
            case EXIT:
                this.currentPathLength += prevPoint.distance(movement.person.getFinalDestination());
                break;
        }
    }

    public Movement[] getMovements() {
        return movements;
    }

    public void removeLastMovement() {
        this.currentSize--;
        Movement movement = movements[this.currentSize];
        Movement prevMovement;
        ManhattanPoint prevPoint;
        if (this.currentSize >= 1) {
            prevMovement = movements[this.currentSize - 1];
            prevPoint = prevMovement.getType() == MovementType.ENTER ? prevMovement.getPerson().getCurrentDestination() : prevMovement.getPerson().getFinalDestination();
        } else {
            prevPoint = taxi.getCurrentPosition();
        }
        switch (movement.getType()) {
            case ENTER:
                this.currentPathLength -= prevPoint.distance(movement.person.getCurrentDestination());
                break;
            case EXIT:
                this.currentPathLength -= prevPoint.distance(movement.person.getFinalDestination());
                break;
        }
    }

    public void setMovements(Movement[] movements) {
        this.movements = movements;
    }

    public Integer getCurrentSize() {
        return currentSize;
    }

    public void setCurrentSize(Integer currentSize) {
        this.currentSize = currentSize;
    }

    @Override
    protected Object clone() {
        PathSolution solution = new PathSolution(this.movements.length / 2, this.taxi);
        solution.setMovements(this.movements.clone());
        solution.setCurrentSize(this.currentSize);
        solution.currentPathLength = this.currentPathLength;
        return solution;
    }

    @Override
    public String toString() {
        String r = "";
        Integer currentSize = this.currentSize - 1;
        if (currentSize >= 1) {
            for (Integer i = 0; i < currentSize; i++) {
                r += movements[i].person.getId() + ",";
            }
            r += movements[currentSize].person.getId();
        }
        return r;
    }
}

class Movement {
    MovementType type;
    Person person;

    public Movement(MovementType type, Person person) {
        this.type = type;
        this.person = person;
    }

    public MovementType getType() {
        return type;
    }

    public void setType(MovementType type) {
        this.type = type;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Movement)) return false;
        Movement movement = (Movement) o;
        return getType() == movement.getType() &&
                Objects.equals(getPerson(), movement.getPerson());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getType(), getPerson());
    }
}

class Taxi {
    private Person[] seats;
    private int currentFirst = 0;
    private int seatsFilled = 0;
    private ManhattanPoint currentDestination;

    public Taxi(Person[] seats) {
        this.seats = seats;
    }

    public Taxi(int seatsNumber) {
        this.seats = new Person[seatsNumber];
    }

    public Taxi(Person[] seats, ManhattanPoint currentDestination) {
        this.seats = seats;
        this.currentDestination = currentDestination;
    }

    public Taxi(int seatsNumber, int xCurrentDestination, int yCurrentDestination) {
        this.seats = new Person[seatsNumber];
        this.currentDestination = new ManhattanPoint(xCurrentDestination, yCurrentDestination);
    }

    public ManhattanPoint getCurrentPosition() {
        return currentDestination;
    }

    public void setCurrentDestination(ManhattanPoint currentDestination) {
        this.currentDestination = currentDestination;
    }

    public Person getFirstPerson() {
        if (this.seatsFilled <= 0) {
            return null;
        }
        Person r = this.seats[this.currentFirst];
        this.seats[this.currentFirst] = null;
        seatsFilled--;
        this.currentFirst = (this.currentFirst + 1) % this.seats.length;
        return r;
    }

    public void removeLastPerson() {
        seatsFilled--;

        seats[(this.currentFirst + this.seatsFilled) % this.seats.length] = null;
    }

    public void addLastPerson(Person person) {
        if (seatsFilled >= seats.length) {
            return;
        }
        this.seatsFilled++;
        seats[(this.currentFirst + this.seatsFilled - 1) % this.seats.length] = person;
    }

    public Person[] getSeats() {
        return seats;
    }

    public void setSeats(Person[] seats) {
        this.seats = seats;
    }

    public int getSeatsFilled() {
        return seatsFilled;
    }

    public void setSeatsFilled(int seatsFilled) {
        this.seatsFilled = seatsFilled;
    }

    public ManhattanPoint getCurrentDestination() {
        return currentDestination;
    }

    @Override
    public String toString() {
        return "Taxi{" +
                "seats=" + Arrays.toString(seats) +
                ", currentDestination=" + currentDestination +
                '}';
    }

    public boolean hasAnySeatsLeft() {
        return seats.length > seatsFilled;
    }
}

class Person {
    private Integer id;

    private ManhattanPoint currentDestination;

    private ManhattanPoint finalDestination;

    public Person(Integer id, int xCurrentDestination, int yCurrentDestination, int xFinalDestination, int yFinalDestination) {
        this.id = id;
        this.currentDestination = new ManhattanPoint(xCurrentDestination, yCurrentDestination);
        this.finalDestination = new ManhattanPoint(xFinalDestination, yFinalDestination);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ManhattanPoint getCurrentDestination() {
        return currentDestination;
    }

    public void setCurrentDestination(ManhattanPoint currentDestination) {
        this.currentDestination = currentDestination;
    }

    public ManhattanPoint getFinalDestination() {
        return finalDestination;
    }

    public void setFinalDestination(ManhattanPoint finalDestination) {
        this.finalDestination = finalDestination;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return Objects.equals(getId(), person.getId()) &&
                Objects.equals(getCurrentDestination(), person.getCurrentDestination()) &&
                Objects.equals(getFinalDestination(), person.getFinalDestination());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getCurrentDestination(), getFinalDestination());
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", currentDestination=" + currentDestination +
                ", finalDestination=" + finalDestination +
                '}';
    }
}

class ManhattanPoint {
    private int x;
    private int y;

    public ManhattanPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int distance(ManhattanPoint point) {
        return Math.abs(x - point.getX()) + Math.abs(y - point.getY());
    }

    @Override
    public String toString() {
        return "ManhattanPoint{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}