import java.io.*;
import java.lang.reflect.Array;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Naloga5 {
    public static void main(String args[]) {
        if (args.length >= 2) {
            try (BufferedReader reader = new BufferedReader(new FileReader(args[0]))) {
                String line;
                Memory memory = null;
                while ((line = reader.readLine()) != null) {
                    String[] values = line.split(",");
                    switch (values[0]) {
                        case "i":
                            memory = new Memory(Integer.parseInt(values[1]), Integer.parseInt(values[2]));
                            break;
                        case "a":
                            //noinspection ConstantConditions
                            memory.alloc(Integer.parseInt(values[1]), Integer.parseInt(values[2]));
                            break;
                        case "f":
                            //noinspection ConstantConditions
                            memory.free(Integer.parseInt(values[1]));
                            break;
                        default:
                            break;
                    }
                }

                try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(new File(args[1])))) {
                    //noinspection ConstantConditions
                    outputStreamWriter.write(memory.toString());
                } catch (IOException e) {
                    System.exit(1);
                }
            } catch (IOException | NullPointerException ex) {
                System.err.println("Cannot read from file. Or file not formatted properly.");
                System.exit(-1);
            }
        }
    }
}

class Memory {
    private ListArray<MemoryBlock> memory;

    @SuppressWarnings("unused")
    public Memory() {
        init(16, 16);
    }

    Memory(int m, int n) {
        init(m, n);
    }

    @SuppressWarnings("WeakerAccess")
    public void init(int m, int n) {
        memory = new ListArray<>(m, n, MemoryBlock.class);
    }

    public void free(int id) {
        for(Iterator<MemoryBlock> iterator = memory.iterator(); iterator.hasNext();) {
            MemoryBlock next = iterator.next();
            if (next != null && next.id == id) {
                iterator.remove();
                break;
            }
        }
    }

    @SuppressWarnings("UnusedReturnValue")
    public boolean alloc(int size, int id) {
        if (size > memory.arraySize) {
            return false;
        }
        MemoryBlock[] best = null;
        Integer bestLeftBytes = Integer.MAX_VALUE;
        Integer lastBestIndex = memory.arraySize - 1;
        for (MemoryBlock[] list : memory.getLinkedList()) {
            int sizeSum = 0;
            Integer lastIndex = 0;
            for (int i = 0; i < list.length; i++) {
                if (list[i] != null) {
                    sizeSum += list[i].size;
                } else {
                    lastIndex = i;
                    break;
                }
            }
            Integer leftBytes = list.length - sizeSum;
            if (leftBytes >= size && leftBytes < bestLeftBytes) {
                best = list;
                bestLeftBytes = leftBytes;
                lastBestIndex = lastIndex;
            }
        }
        //noinspection ConstantConditions
        best[lastBestIndex] = new MemoryBlock(id, size);
        return true;
    }

    @Override
    public String toString() {
        int[] solution = new int[this.memory.arraySize + 1];
        for (MemoryBlock[] memoryBlock : memory.getLinkedList()) {
            int maxIndex = 0;
            for (MemoryBlock block : memoryBlock) {
                if (block == null) {
                    break;
                }
                maxIndex += block.size;
            }
            solution[maxIndex]++;
        }
        StringBuilder s = new StringBuilder();
        for (Integer integer : solution) {
            s.append(integer).append("\n");
        }
        return s.toString();
    }

    private class MemoryBlock {
        int id;
        int size;

        MemoryBlock(int id, int size) {
            this.id = id;
            this.size = size;
        }

        @Override
        public String toString() {
            return "{" + id + ", " + size + '}';
        }
    }
}

class ListArray<T> implements Iterable<T> {
    private LinkedList<T[]> linkedList = new LinkedList<>();
    final Integer arraySize;

    LinkedList<T[]> getLinkedList() {
        return linkedList;
    }

    /**
     * Creates new instance of ListArray
     *
     * @param m      Size of LinkedList
     * @param n      Size of each array
     * @param tClass Class of Elements in structure.
     */
    ListArray(int m, int n, Class<T> tClass) {
        arraySize = n;
        while (m > 0) {
            m--;
            //noinspection unchecked
            linkedList.addFirst((T[]) Array.newInstance(tClass, n));
        }
    }


    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            Iterator<T[]> linkedListIterator = linkedList.iterator();
            T[] currentList = linkedListIterator.hasNext() ? linkedListIterator.next() : null;
            Integer currentIndex = -1;
            T current = currentList != null ? currentList[0] : null;

            @Override
            public boolean hasNext() {
                return currentIndex + 1 < arraySize || linkedListIterator.hasNext();
            }

            @Override
            public void remove() {
                if (currentList != null) {
                    currentList[currentIndex] = null;
                    for (int i = currentIndex; i < currentList.length; i++) {
                        currentList[i] = currentList[i + 1];
                        if (currentList[i] == null) {// End of block
                            break;
                        }
                    }
                }
            }

            @Override
            public T next() {
                currentIndex++;
                if (currentIndex < arraySize) {
                    current = currentList[currentIndex];
                } else if (linkedListIterator.hasNext()) {
                    currentList = linkedListIterator.next();
                    currentIndex = 0;
                    current = currentList[currentIndex];
                } else {
                    current = null;
                }
                return current;
            }
        };
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("{");
        for (T t : this) {
            s.append(t).append(",");
        }
        s.append("}");
        return s.toString();
    }
}

class LinkedList<T> implements Iterable<T> {
    /**
     * Element of first will be always null.
     */
    private LinkedListElement<T> first = new LinkedListElement<>();

    /**
     * Add new element to first position.
     *
     * @param element Element to be added.
     */
    void addFirst(T element) {
        first.next = new LinkedListElement<>(first.next, first, element);
        if (first.previous != null) {
            first.previous.previous = first.next;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            LinkedListElement<T> current = first;

            @Override
            public boolean hasNext() {
                return current.next != null;
            }

            @Override
            public T next() {
                current = current.next;
                return current.element;
            }

            @Override
            public void remove() {
                try {
                    current.previous.next = current.next;
                    current.next.previous = current.previous;
                } catch (NullPointerException e) {
                    throw new NoSuchElementException(current.element.toString());
                }
            }
        };
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("{");
        for (T t : this) {
            s.append(t.toString()).append(",");
        }
        s.append("}");
        return s.toString();
    }

    class LinkedListElement<U> {
        LinkedListElement<U> next;
        LinkedListElement<U> previous;
        U element;

        LinkedListElement() {
        }

        LinkedListElement(LinkedListElement<U> next, LinkedListElement<U> previous, U element) {
            this.next = next;
            this.previous = previous;
            this.element = element;
        }

    }
}
