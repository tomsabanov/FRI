import java.io.File;
import java.util.Scanner;
import java.io.FileWriter;


class Element{
	int id;
	int size;
	Element next;

	public Element(int size, int id){
		//	System.out.println("Inserting id" + id);
		this.id = id;
		this.size = size;
		this.next = null;
	}

}

class Node{
	int space;
	Element first;
	Element last;

	Node next;

	public Node(int space){
		this.space = space;
		this.next = null;
	}

	public boolean insert(int size, int id){
		this.space = this.space - size;
		if(first == null){
			first = new Element(size, id);
			first.next = last;
			return true;
		}
		if(last == null){
			last = new Element(size, id);
			first.next = last;
			return true;
		}
		last.next = new Element(size,id);
		last = last.next;
		return true;
	}

	public int remove(int id, int maxSize){
		// method returns the new size of the node or -1 if id wasn't present in the node
		Element current = first;
		Element prev = current;
		while(current != null){
			if(current.id == id){
				//delete the element
				//first increase space by the size of the element
			//	this.printElements();
				this.space = this.space + current.size;
				if(current.next == null){
					//this is the last/first Element
					if(last == null){
						//this is first element
						first = null;
					}
					else{
						last = prev;
						//this is last element
					}
				}
				else{
					prev.next = current.next;
				}
			//	System.out.println("MAX SIZE IS " + maxSize + " AND SPACE IS " + this.space);
				return this.space;
			}
			prev = current;
			current=current.next;
		}

		return  -1;
	}

	public void printElements(){
		Element current = first;
		while(current!= null){
			System.out.println("ID " + current.id + " size " + current.size);
			current=current.next;
		}

	}

}


class DataStructure{
	int maxNodeSize;
	int numberOfNodes;
	
	int[] zasedenost;

	Node first;


	public  DataStructure(){
		this.init(16,16);
	}
	public DataStructure(int m, int n){
		this.init(m,n);
	}
	public void init(int m, int n){
		this.numberOfNodes = m;
		this.zasedenost = new int[m+1];
		this.zasedenost[0] = m;
			
		this.maxNodeSize = n;

		first = new Node(this.maxNodeSize);
		Node current = first;
		for(int i = 1; i<this.numberOfNodes;i++){
			current.next = new Node(this.maxNodeSize);
			current = current.next;
		}

	}

	public boolean alloc(int size, int id){
		
		Node current = first;
		Node chosen = first;
		int min = Integer.MAX_VALUE;
		while(current != null){
			int new_min = current.space - size;
			if(new_min>= 0 && new_min<=min){
				min = new_min;
				chosen = current;
			}	
			current = current.next;
		}
		if(min<Integer.MAX_VALUE){
			// SEDAJ VEMO DA BOMO INSERTALI V CHOSEN NODE
			// MORAMO ZMANJSATI VREDNOST NA PREJSNEM SIZU CHOSEN NODA
			
			int index = this.maxNodeSize - chosen.space;
			this.zasedenost[index]--;

			//SEDAJ ZVECAMO VREDNOST NA NOVEM SIZU NODE-A	
		//	index = this.maxNodeSize - chosen.space + size;
			
			index = index + size;

			this.zasedenost[index]++;
		       	return chosen.insert(size, id);
		}
		return false;
	}
	public int free(int id){
		Node current = first;	
		while(current != null){
			int old_space = current.space;
			int new_space = current.remove(id, this.maxNodeSize);
			if(new_space >= 0){
				// ZMANJSAJ VREDNOST NA PREJSNEM SIZU CHOSEN NODE-A 
				int index = this.maxNodeSize - old_space;
			//	System.out.println("OLD INDEX   " + index); 
				this.zasedenost[index]--;
			//	System.out.println("NEW INDEX " + index);
			//	System.out.println("MAX NODE SIZE " + this.maxNodeSize);
				
				//ZVECAMO VREDNOST NA NOVEM SIZU NODE-A
				index = this.maxNodeSize - new_space;
			//	System.out.println("NEW INDEX " + index);
			//	System.out.println("NEW SPACE " + new_space);
			//	System.out.println("OLD SPACE " + old_space);
			//	System.out.println("MAXNODESIZE " + this.maxNodeSize);
			//	System.out.println();
				this.zasedenost[index]++;
				break;
			}	
			current = current.next;
		}
		return 1;
	}

	
	public boolean dump(String output){
		try{
			FileWriter writer = new FileWriter(output); 
			Node current = this.first;
			writer.write(this.zasedenost[0] + "\n");
			int value  = this.numberOfNodes - this.zasedenost[0]+1;
			int sum = this.zasedenost[0];
			String s = "";
			for(int i = 1; i<=value; i++){
				writer.append(this.zasedenost[i] + "\n");
				sum+=this.zasedenost[i];
				s += this.zasedenost[i] + "\n";
				if(sum==this.numberOfNodes) break;
				
			}
		//	System.out.println(s);
						
			writer.close();
		}
		catch(Exception e){
			return false;
		}
		return true;
	}

}



public class Naloga5{

	public static void main(String[] args){
		String input = args[0];
		String output = args[1];
		File in = new File(input);
		
		try{
			Scanner sc = new Scanner(in);
			sc.useDelimiter("\\D");
			int K = Integer.parseInt(sc.nextLine());
			readAndExecuteCommands(sc, K, output);
		}	
		catch(Exception e){
			System.out.println(e);
		}

	}

	public static void readAndExecuteCommands(Scanner sc, int K, String output){
		DataStructure data=new DataStructure();
		for(int i = 0; i<K ;i++){
			String command = sc.nextLine();
			String[] parameters = command.split(",");
			switch(parameters[0]){
				case "i":
					int m = Integer.parseInt(parameters[1]);
					int n = Integer.parseInt(parameters[2]);
					data = new DataStructure(m,n);
					break;		
				case "a":
					int s = Integer.parseInt(parameters[1]);
					int id = Integer.parseInt(parameters[2]);
					data.alloc(s,id);
					break;
				case "f":
					int j = Integer.parseInt(parameters[1]);
					data.free(j);
					break;
			}
		}
		data.dump(output);
	}

}
				
