import java.util.Scanner;
import java.io.File;
import java.io.FileWriter;
import java.io.File;

class Stranka{
	int id;
	int origin_x;
	int origin_y;
	int dest_x;
	int dest_y;

	Boolean dropped = false;
	Boolean pickedUp = false;

	public Stranka(int id, int or_x, int or_y, int de_x, int de_y){
		this.id = id;
		this.origin_x = or_x;
		this.origin_y = or_y;
		this.dest_x = de_x;
		this.dest_y = de_y;
	}


}

class Taxi{
	int steviloVsehStrank;
	Stranka[] stranke;
	int[] bestPath;
	int minPathLength = Integer.MAX_VALUE;
	int maxStrank;//istocasno v avtu le maxStrank
	
	int pos_x;//zacetni poziciji taxija
	int pos_y;

	int numCurrent=0; //stevilo trenutnih strank v taxiju
	int numDropped = 0;//stevilo droppanih strank
	int[] currentPath; //trenutna pot (na koncu se kopira v bestPath)
	int currentPathIndex = 0; // kam vstavit passengerja
	int currentPathLength = 0;

	int[] trenutneStranke; //indexi trenutnih strank v avtu

	public Taxi(int maxStrank,int x, int y){
		this.pos_x = x;
		this.pos_y = y;
		this.maxStrank = maxStrank;
		this.trenutneStranke = new int[maxStrank];
		for(int i = 0;i<maxStrank;i++){
			trenutneStranke[i] = -1;
		}
	}

	public void setSteviloStrank(int st){
		this.steviloVsehStrank = st;
		this.stranke = new Stranka[st];
		this.bestPath = new int[st * 2];
		this.currentPath = new int[st * 2];
	}

	public void dodajStranko(Stranka s, int i){
		this.stranke[i] = s;
	}


	public void printStranke(){
		for(int i = 0; i<this.steviloVsehStrank;i++){
			Stranka s = stranke[i];
			String n = "Stranka " + s.id + " (" + s.origin_x + "," + s.origin_y + " (" + s.dest_x + "," + s.dest_y + ")";
			System.out.println(n);
		}
	}
	public void findPath(){
		if(currentPathLength > minPathLength){
			return;
		}
		if(numDropped == steviloVsehStrank){
			//vse stranke smo droppali
			if(currentPathLength <= minPathLength){
				minPathLength = currentPathLength;
				copyToBest();
			}
			return;
		}
		if(numCurrent < maxStrank){
			//lahko stranke pobiramo
			for(int i = 0; i<steviloVsehStrank; i++){
				if(stranke[i].pickedUp==false){
					Stranka s = stranke[i];
					//poberemo to stranko
					s.pickedUp = true;
					numCurrent++;

					//najdemo frej plac na trenutnih strankah
					int ind = 0;
					for(int j = 0; j<maxStrank;j++){
						if(trenutneStranke[j] == -1){
							trenutneStranke[j] = i;
							ind = j;
							break;
						}
					}

					//setamo pot
					currentPath[currentPathIndex] = s.id;
					currentPathIndex++;

					//set current path length
					int oldLength = currentPathLength;
					currentPathLength +=Math.abs(pos_x - s.origin_x) + Math.abs(pos_y - s.origin_y); 

					//setat potrebno taxi koordinate 
					int oldX = pos_x;
					int oldY = pos_y;
					pos_x = s.origin_x;
					pos_y = s.origin_y;


					//recursion	
					findPath();
				
					//revert
					


					currentPathIndex--;
					s.pickedUp = false;
					numCurrent--;

					pos_x = oldX;
					pos_y = oldY;

					currentPathLength = oldLength;

					trenutneStranke[ind] = -1;

				}

			}

		}
		//stranke odlagamo
		for(int i = 0; i<maxStrank;i++){
			if(trenutneStranke[i] != -1){
				//odlozimo stranko
				Stranka s = stranke[trenutneStranke[i]];
				s.dropped=true;

				int temp_id = trenutneStranke[i];
				trenutneStranke[i] = -1;

				currentPath[currentPathIndex] = s.id;
				currentPathIndex++;

				int oldLength = currentPathLength;
				currentPathLength += Math.abs(pos_x-s.dest_x) + Math.abs(pos_y - s.dest_y);

				int oldX = pos_x;
				int oldY = pos_y;
				pos_x = s.dest_x;
				pos_y = s.dest_y;

				numDropped++;
				numCurrent--;
				
				//recursion
				findPath();

				currentPathIndex--;
				s.dropped=false;

				currentPath[currentPathIndex] = 0;
				pos_x = oldX;
				pos_y = oldY;
				currentPathLength = oldLength;

				numDropped--;
				numCurrent++;


				trenutneStranke[i] = temp_id;
				
			}
		}
		


	}

	public void copyToBest(){
		for(int i = 0; i<steviloVsehStrank*2; i++){
			bestPath[i] = currentPath[i];
		}
	}


}


public class Naloga1{

	public static void main(String[] args){
		File input = new File(args[0]);
		String output = args[1];
		Taxi taxi;
		try{

			Scanner sc = new Scanner(input);
			sc.useDelimiter("\\D");		
			int N = readInt(sc); 
			int xT = readInt(sc); 
			int yT = readInt(sc); 

			taxi = new Taxi(N,xT,yT);

			int  M = readInt(sc);

			taxi.setSteviloStrank(M);
			int id;
			int or_x;
			int or_y;
			int de_x;
			int de_y;
			for(int i = 0; i<M; i++){
				id =readInt(sc); 
				or_x =readInt(sc); 
				or_y =readInt(sc); 
				de_x =readInt(sc); 
				de_y  = readInt(sc);	
				Stranka s = new Stranka(id,or_x,or_y,de_x,de_y);
				taxi.dodajStranko(s,i);
			}
		//	taxi.printStranke();
			sc.close();	
			taxi.findPath();
			dump(taxi.bestPath, output);

		}catch(Exception e){
			System.out.println("There was a problem " + e);
		}	

	}

	public static void dump(int[] data, String output){
		try{
			FileWriter writer= new FileWriter(output);
			String s = "";
			for(int i = 0; i<data.length-1;i++){
				s+=""+data[i] + ",";
			}
			s+="" + data[data.length-1];
			writer.write(s);
			writer.close();
		}
		catch(Exception e){

		}


	}

	public static int readInt(Scanner s){
		while(s.hasNext()){
			if(s.hasNextInt()){
				return s.nextInt();
			}
			s.next();
		}
		return -1;
	}
}
