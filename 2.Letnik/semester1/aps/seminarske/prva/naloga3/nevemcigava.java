import java.io.*;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class Naloga3 {
    public static void main(String[] args) {
        String outputFileName = args[1];
        // Well It is ugly but I do not have enough time to make it cleaner
        // Read last line to get memory size.
        String line;
        int hashMapSize = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader(args[0]))) {
            while ((line = reader.readLine()) != null) {
                hashMapSize = Integer.parseInt(line.split(",")[2]);
            }
        } catch (IOException e) {
            System.exit(2);
        }

        HashMap hashMap = new HashMap(hashMapSize);
        HashMap hashMap1 = new HashMap(hashMapSize);
        //
        try (BufferedReader reader = new BufferedReader(new FileReader(args[0]))) {
            while ((line = reader.readLine()) != null) {
                String[] val = line.split(",");
                int index = Integer.parseInt(val[0]);
                int position = Integer.parseInt(val[1]);
                int size = Integer.parseInt(val[2]) - position + 1;
                hashMap1.add(position, index, size);
                hashMap.add(position, index, size);
            }

            currentGlobalBest = simpleFragmentation(new Solution(), hashMap1);
            Solution solution = findBestFragmentation(new Solution(), hashMap);
            try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(new File(outputFileName)))) {
                outputStreamWriter.write(solution.toString());
            } catch (IOException e) {
                System.exit(2);
            }
        } catch (IOException e) {
            System.exit(2);
        }


    }

    private static Solution simpleFragmentation(Solution solution, HashMap hashMap) {
        Iterator<HashMapElement> iterator = hashMap.iterator();
        int currentEnd = 0;
        while (iterator.hasNext()) {
            HashMapElement element = iterator.next();
            if (element.position - currentEnd > 0) {
                hashMap.add(currentEnd, element.index, element.size);
                solution.addMove(element.index, element.size, currentEnd);
                currentEnd += element.size;
            } else {
                currentEnd = element.position + element.size;
            }
        }

        return solution;
    }

    private static Solution currentGlobalBest;

    private static Solution findBestFragmentation(Solution solution, HashMap currentMemory) {
        if (currentGlobalBest != null && currentGlobalBest.getCost() <= solution.getCost()) {
            return currentGlobalBest;
        }
        if (currentMemory.isDefragmented()) {
            return solution;
        }
        //Try to move every block
        Iterator<HashMapElement> iterator = currentMemory.iterator();
        Solution currentBest = currentGlobalBest;
        while (iterator.hasNext()) {
            HashMapElement elementToMove = iterator.next();
            // Do not move same element twice
            if (solution.moves.last != null && elementToMove.index == solution.moves.last.index) {
                continue;// we should not two times move same element.
            }
            // If all solutions for that element will be too big just skip that element
            if (solution.getCost() + elementToMove.size >= currentGlobalBest.getCost()) {
                continue;
            }
            // If element does not exists for some reason.
            if (!currentMemory.remove(elementToMove)) {
                continue;
            }
            Iterator<Integer> freeSpaceIterator = currentMemory.freeSpaceIndexIterator(elementToMove.size);
            while (freeSpaceIterator.hasNext()) {
                Integer nextFreeSpace = freeSpaceIterator.next();
                // If element has already been on that position.
                if (nextFreeSpace == elementToMove.position || solution.contains(elementToMove.index, nextFreeSpace)) {
                    continue;
                }

                // Do recursion
                currentMemory.add(nextFreeSpace, elementToMove.index, elementToMove.size);
                solution.addMove(elementToMove.index, elementToMove.size, nextFreeSpace);
                Solution solution1 = findBestFragmentation(solution, currentMemory);

                if (currentBest == null || solution1.getCost() < currentBest.getCost()) {
                    currentBest = solution1.clone();
                    if (currentGlobalBest == null || currentBest.getCost() < currentGlobalBest.getCost()) {
                        currentGlobalBest = currentBest;
                    }
                }
                // Fix back;
                solution.removeLastMove(elementToMove.size);
                currentMemory.remove(nextFreeSpace);
                // Done

            }
            // Fix back
            currentMemory.add(elementToMove.position, elementToMove.index, elementToMove.size);
        }
        return currentBest;
    }
}

class Solution {
    LinkedList moves = new LinkedList();
    private int cost = 0;
    int getCost() {
        return cost;
    }

    void addMove(int id, int size, int newPosition) {
        moves.addLast(id, newPosition);
        cost += size;
    }

    void removeLastMove(int size) {
        moves.removeLast();
        cost -= size;
    }

    @SuppressWarnings("MethodDoesntCallSuperMethod")
    @Override
    public Solution clone() {
        Solution clone = new Solution();
        clone.cost = this.cost;
        clone.moves = new LinkedList();
        LinkedListElement element = this.moves.first;
        while (element != null) {
            clone.moves.addLast(element.index, element.size);
            element = element.next;
        }
        return clone;
    }

    @Override
    public String toString() {
        return moves.toString();
    }

    boolean contains(int index, int newLocation) {
        LinkedListElement element = this.moves.first;
        while (element != null) {
            if (element.index == index && element.size == newLocation) {
                return true;
            }
            element = element.next;
        }
        return false;
    }
}

class HashMap implements Iterable<HashMapElement> {
    private LinkedList[] linkedLists;
    private final Integer size;

    HashMap(Integer size) {
        this.size = size;
        linkedLists = new LinkedList[size / 4 + 1];
    }

    public void add(int i, int id, int size) {
        int uu = i / linkedLists.length;
        if (linkedLists[uu] == null) {
            linkedLists[uu] = new LinkedList();
        }
        // If memory became fragmented we let index where it is. else we update it to new.
        // We do that for more efficient checking if memory is fragmented.
        linkedLists[uu].add(i, id, size);
    }

    @SuppressWarnings("unused")
    public LinkedListElement get(int i) {
        int uu = i / linkedLists.length;
        if (linkedLists[uu] == null) {
            linkedLists[uu] = new LinkedList();
        }
        return linkedLists[uu].get(i);
    }

    @Override
    public String toString() {
        StringBuilder r = new StringBuilder("{");
        for (HashMapElement next : this) {
            r.append(next.toString()).append(", ");
        }
        r.append("}");

        return r.toString();
    }

    @Override
    public Iterator<HashMapElement> iterator() {
        return new HashMapIterator();
    }

    Iterator<Integer> freeSpaceIndexIterator(Integer size) {
        return new FreeSpaceIterator(size);
    }

    class HashMapIterator implements Iterator<HashMapElement> {
        private int currentIndex = 0;
        private LinkedListElement currentElement = linkedLists[0] == null ? null : linkedLists[0].first;

        HashMapIterator() {
            while (currentIndex < linkedLists.length) {
                if (linkedLists[currentIndex] != null && linkedLists[currentIndex].first != null) {
                    currentElement = linkedLists[currentIndex].first;
                    break;
                }
                currentIndex++;
            }
        }

        @Override
        public boolean hasNext() {
            return currentElement != null;
        }

        @Override
        public HashMapElement next() {
            LinkedListElement r = currentElement;
            currentElement = currentElement.next;
            if (currentElement == null) {
                currentIndex++;
                while (currentIndex < linkedLists.length) {
                    if (linkedLists[currentIndex] != null && linkedLists[currentIndex].first != null) {
                        currentElement = linkedLists[currentIndex].first;
                        break;
                    }
                    currentIndex++;
                }

            }
            return new HashMapElement(r);
        }

    }

    /**
     * Iterator for all possible start indexes for specified size.
     */
    class FreeSpaceIterator implements Iterator<Integer> {
        final Integer size;
        private Integer currentIndex;
        private Integer maxFreeIndex = 0;
        Iterator<HashMapElement> iterator = HashMap.this.iterator();

        FreeSpaceIterator(Integer size) {
            this.size = size;
            currentIndex = 0;
            while (iterator.hasNext()) {
                HashMapElement next = iterator.next();
                maxFreeIndex = next.position;
                if(maxFreeIndex - currentIndex >= size) {
                    break;
                } else {
                    currentIndex = next.position+next.size;
                }
            }
            if (maxFreeIndex-currentIndex < size){
                currentIndex = null;
            }
        }
        @Override
        public boolean hasNext() {
            return currentIndex != null && currentIndex <= HashMap.this.size;
        }
        private void calculateNext(){
           while (iterator.hasNext()) {
                HashMapElement next = iterator.next();
                maxFreeIndex = next.position;
                if(maxFreeIndex - currentIndex > size) {
                    break;
                } else {
                    currentIndex = next.position+next.size + 1;
                }
            }
            if (!iterator.hasNext() || maxFreeIndex-currentIndex < size){
                currentIndex = null;
            }
        }
        @Override
        public Integer next() {
            Integer r = currentIndex;
            calculateNext();
            return r;
        }
    }

    @Override
    public void forEach(Consumer<? super HashMapElement> consumer) {

    }

    @Override
    public Spliterator<HashMapElement> spliterator() {
        return null;
    }

    boolean remove(HashMapElement current) {
        int uu = current.position / linkedLists.length;
        return linkedLists[uu].remove(current.position);
    }

    @SuppressWarnings("UnusedReturnValue")
    boolean remove(Integer position) {
        int uu = position / linkedLists.length;
        return linkedLists[uu].remove(position);
    }

    boolean isDefragmented() {
        Iterator<HashMapElement> iterator = this.iterator();
        int currentEnd = 0;
        while (iterator.hasNext()) {
            HashMapElement element = iterator.next();
            if (element.position - currentEnd > 0) {
                return false;
            } else {
                currentEnd = element.position + element.size;
            }
        }
        return true;
    }

}

class HashMapElement {
    int position;
    int index;
    int size;

    @SuppressWarnings("unused")
    public HashMapElement(int position, int index, int size) {
        this.position = position;
        this.index = index;
        this.size = size;
    }

    HashMapElement(LinkedListElement r) {
        this.size = r.size;
        this.index = r.index;
        this.position = r.position;
    }

    @Override
    public String toString() {
        return "{" + index + ":" + position + "," + (position + size) + "}";
    }
}

class LinkedList {
    @SuppressWarnings("unused")
    private int size = 0;

    public void add(int i, int id, int size) {
        // binary search
        this.size++;
        LinkedListElement current = this.first;
        while (current != null) {
            if (current.position < i) {
                current = current.next;
            } else if (current.position == i) {
                System.err.println("Rewrite element");
                return;
            } else {
                LinkedListElement newElement = new LinkedListElement(id, size, current.prev, current, i);
                if (current.prev != null) {
                    current.prev.next = newElement;
                } else {
                    this.first = newElement;
                }
                current.prev = newElement;
                return;
            }

        }
        if (this.first == null) {
            this.last = this.first = new LinkedListElement(id, size, null, null, i);
        } else {
            this.last.next = new LinkedListElement(id, size, this.last, null, i);
            this.last = last.next;
        }
    }

    void addLast(int id, int size) {
        this.size++;
        if (this.last == null) {
            this.last = this.first = new LinkedListElement(id, size, null, null, 0);
        } else {
            last.next = new LinkedListElement(id, size, last, null, last.position);
            this.last = last.next;
        }
    }

    LinkedListElement first = null;
    LinkedListElement last = null;

    @Override
    public String toString() {
        StringBuilder r = new StringBuilder();
        LinkedListElement current = this.first;
        while (current != null) {
            r.append(current.toString()).append("\n");
            current = current.next;
        }
        return r.toString();
    }

    LinkedListElement get(int i) {
        LinkedListElement current = this.first;
        while (current != null) {
            if (current.position < i) {
                current = current.next;
            } else if (current.position == i) {
                return current;
            } else {
                return null;
            }
        }
        return null;
    }

    public boolean remove(int i) {
        LinkedListElement current = this.first;
        while (current != null) {
            if (current.position < i) {
                current = current.next;
            } else if (current.position == i) {
                if (current.prev != null) {
                    current.prev.next = current.next;
                } else {
                    this.first = current.next;
                }
                if (current.next != null) {
                    current.next.prev = current.prev;
                } else {
                    this.last = current.prev;
                }
                size--;
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    void removeLast() {
        size--;
        if (this.last == null || this.last == this.first) {
            this.last = this.first = null;
            return;
        }
        this.last.prev.next = null;
        this.last = this.last.prev;
    }
}

class LinkedListElement {
    Integer index;
    Integer size;
    LinkedListElement prev;
    LinkedListElement next;
    Integer position;

    LinkedListElement(Integer index, Integer size, LinkedListElement prev, LinkedListElement next, Integer position) {
        this.index = index;
        this.size = size;
        this.prev = prev;
        this.next = next;
        this.position = position;
    }

    @Override
    public String toString() {
        return index + "," + size;
    }
}
