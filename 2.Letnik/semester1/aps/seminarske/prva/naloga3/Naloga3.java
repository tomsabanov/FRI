import java.io.File;
import java.util.Scanner;
import java.io.FileWriter;

class LinkedList{
	Element first;
	Element last;
	public LinkedList(){
		makenull();
	}
	public void makenull(){
		first = new Element(null,null);
		last = null;
	}
	public void addLast(int id, int start, int end){
		Element el = new Element(id,start,end,null,null);
		if(last==null){
			el.prev = first;
			first.next = el;
			last = el;
		}
		else{
			last.next = el;
			el.prev = last;
			last = el;
		}
	}
}
class Element{
	Element next;
	Element prev;
	int id, start,end;
	Boolean moved = false;
	
	public Element(int id, int start, int end, Element nxt, Element prv){
		this(id,start,end);
		this.next = nxt;
		this.prev = prv;	
	}
	public Element(Element nxt, Element prv){
		this.next = nxt;
		this.prev = prv;
	}
	public Element(int id, int start, int end){
		this.id = id;
		this.start = start;
		this.end = end;
	}
	

}
class Stack{

	StackElement top;
	int sum = 0;
	public Stack(){
		makenull();
	}
	public void makenull(){
		top = null;
	}
	public boolean empty(){
		return (top == null);
	}
	public StackElement top(){
		if(!empty()) return top;
		return null;
	}
	public void push(int id, int start, int size){
		StackElement el = new StackElement(id,start,size);
		this.sum = this.sum + size;
		el.next = top;

		top = el;
	}
	public void pop(){
		if(!empty()){
			sum = sum - top.size;
		      	top = top.next;
		}
	}

}
class StackElement{
	int id, start, size;
	StackElement next;
	StackElement(int id, int start, int size){
		this.id = id;
		this.start = start;
		this.size = size;
		next = null;
	}
}

class DataStructure{
	//data structure bo skrbel za pravilno ustvarjanje in vstavljanje elementov in spacov ter potez
	
	LinkedList elements;
	Stack moves;
	int min = Integer.MAX_VALUE;

	LinkedList chosen_moves;

	public DataStructure(){
		// ustvarimo empty l.l elements, spaces, moves.....
		this.elements = new LinkedList();
		this.moves = new Stack();
	}
	public void insertElement(int id, int start, int end){
		elements.addLast(id,start,end);
	}
	public void defrag(){
		defrag(elements.first.next,0,0);
	}


	public void printElement(String S, Element e, int nivo){
		String n = generateTabs(nivo);
		System.out.println(n + S + " with ID " + e.id + " at " + e.start + "-" + e.end);
	}	

	public void printMove(String s, Element e, int nivo){
		String n = generateTabs(nivo);
		System.out.println(n + "PREMAKNILI ELEMENT " + e.id + " na " + e.start + " - " + e.end);
	}


	public void printPop(String S,Element e, int nivo){
		String n = generateTabs(nivo);
		System.out.println(n + "PREMAKNILI NAZAJ  ELEMENT " + e.id + " na " + e.start + " - " + e.end);
	}


	public void printSpace(String S, Element e, int index, int space_size, int nivo){
		String n = generateTabs(nivo);
		System.out.println(n + "Found an empty space at " + index + "-" + (e.start-1) + " with size " + space_size + " at element with id " + e.id + " at " + e.start + "-" + e.end);
	}

	public String generateTabs(int nivo){
		String s = "";
		for(int i = 0; i<nivo;i++){
			s+="\t";
		}
		return s;
	}
	public String generateDash(int nivo){
		String s = "";
		s+=generateTabs(nivo);
		for(int i = 0; i<=nivo; i++){
			for(int j = 0;j<16;j++){
				s+="-";
			}	
		}
		return s;
	}
	public void printElements(int nivo){
		String n = generateTabs(nivo);
		Element current = elements.first.next;
		String s = n;
		while(current!=null){
			String el = current.id + "(" + current.start + "," + current.end +")";
			if(current.next != null){
				el+=" ----> ";
			}
			s+=el;
			current = current.next;
		}
		System.out.println(s);

	}
	public void printMoves(int nivo){
		String n = generateTabs(nivo);
		StackElement current = moves.top();
		String s = n;
		while(current!=null){
			String el = current.id + "," + current.start; 
			if(current.next != null){
				el+=" ----> ";
			}
			s+=el;
			current = current.next;
		}
		System.out.println(s);
	}

	public void defrag(Element first, int indx, int nivo){
		if(moves.sum > min){
			//the current number of ordered bytes is already higher than the minimum, so we return
			return; 
		}

		//String n = generateTabs(nivo);
		if(first == null){
			//we went through 1 possibility, return 
			//if(filled == false) return;
			if(moves.sum <= this.min){ // TODO ENACAJ LAHKO POBRISEM (ENAK REZULTAT, LE POTEZE DRUGACNE)
			//	System.out.println(n + "FOUND MINIMAL POSSIBILITY WITH SIZE " + moves.sum);
			//	printMoves(nivo);
				//printElement(n + "PREV ELEMENT", first.prev, nivo);
				//printElement(n + "THIS ELEMENT",first, nivo);
				this.min = moves.sum;
				this.copyStackToLinkedList();
			}	
			return;
		}
		//search for first empty space in this sub-l.l
		//we start at the given l.l element ( first ) 
		Element current = first;
		int index = indx; //we will calculate empty space depending on the index

		int space_start = -1;
		int space_size = -1;
		while(current != null){
			if(current.start == index){
				index = current.end + 1;
			}	
			else{
				// we found our empty space
				space_start = index;
				space_size = current.start - index;
				
			//	printSpace("",current, index,space_size, nivo);
				// we will try to fill this empty space now (and we will only try to fill it with elements starting from the elements.last to current element
				break;
			}
			current = current.next;
		}
		if(current == null){
			return;
		}

		Element revCurrent = elements.last;
	
		//System.out.println(n + "STARTING SEARCHING FROM ID " + elements.last.id);
		while(true){

			// get revCurrent size
			int size = revCurrent.end - revCurrent.start + 1;	
			if(revCurrent == current && current.moved == false){
				int oldStart = current.start;
				int oldEnd = current.end;

				current.start = space_start;
				current.end = space_start + size-1;

				moves.push(current.id, space_start, size);
				current.moved = true;
			
				defrag(current.next,current.end + 1, nivo);
				
				moves.pop();
				current.moved = false;


				current.start = oldStart;
				current.end = oldEnd;	
				return;
			}
			else if(size<=space_size && revCurrent.moved == false && space_start < revCurrent.start){
				
				//check if revCurrent is last
				Boolean b = false;
				if(revCurrent == elements.last){
					b = true;
					elements.last = revCurrent.prev;
					elements.last.next = null;
				}

			//	printElement("REVCURRENT ELEMENT ", revCurrent, nivo);
			//	printElement("CURRENT ELEMENT ", current, nivo);
				
				//we must now change pointers of revCurrent
				Element revCurrentPREV = revCurrent.prev;//we store the prev of revCurrent
				int oldStart = revCurrent.start;
				int oldEnd = revCurrent.end;

				revCurrentPREV.next = revCurrent.next; 
				if(!b){
					revCurrentPREV.next.prev = revCurrentPREV;
				}

				current.prev.next = revCurrent;
				revCurrent.prev = current.prev;

				revCurrent.next = current;
				current.prev = revCurrent;

				revCurrent.start = index;
				revCurrent.end = index+size - 1;
				
				revCurrent.moved = true;	
					
			//	printMove("ELEMENT ",revCurrent, nivo);	
			//	printElement("LAST EL ", elements.last, nivo);
			//	printElements(nivo);
			//	printElement(" PREMAKNJENI ELEMENT PREV ", revCurrent.prev, nivo);
			//	printElement(" PREMAKNJENI ELEMENT NEXT EL.PREV ", revCurrent.next.prev, nivo);
			//	System.out.println(generateDash(nivo));
				
				//we create a move and insert it into the stack
				moves.push(revCurrent.id,index, size);
				if(space_size == size){
					defrag(current.next, current.end+1, nivo+1);
				}
				else{
					defrag(current, revCurrent.end+1, nivo+1);
				}

				//now we revert changes
				//we pop the move off the stack
				moves.pop();	
			//	System.out.println(n + "POPPED");

				revCurrent.prev.next = revCurrent.next;
				revCurrent.next.prev = revCurrent.prev;

				revCurrent.prev = revCurrentPREV;
				revCurrent.next = revCurrentPREV.next;

				revCurrentPREV.next = revCurrent;
				if(!b){
					revCurrent.next.prev = revCurrent;
				}


				revCurrent.start = oldStart;
				revCurrent.end = oldEnd;

				revCurrent.moved = false;

			//	printPop("Element", revCurrent,nivo);
			//	printMoves(nivo);
			//	printElements(nivo);

			//	System.out.println(generateDash(nivo));
				if(b){
					elements.last.next = revCurrent;
					elements.last = revCurrent;
					elements.last.prev = revCurrentPREV;
					revCurrent.next = null;
				}


			}
			if(revCurrent.prev == null) return;

			revCurrent = revCurrent.prev;
		}

	}
	public void copyStackToLinkedList(){
		LinkedList list = new LinkedList();
		StackElement top = this.moves.top();
		while(top != null){
			list.addLast(top.id, top.start, top.start + top.size-1);
			top = top.next; 
		}
		chosen_moves = list;
	}	

	public void dump(String output){
	//	System.out.println("Begginin to dump");
		try{
			FileWriter writer = new FileWriter(output);
			Element current = chosen_moves.last;
			int sum = 0;
			//String nov = "";
			while(current != null && current.id != 0){
				String s = "" + current.id + "," + current.start + "\n";
			//	nov+=current.id + "(" + current.start + "," + current.end + ") --->";
				sum+=current.end-current.start + 1;
				writer.write(s);
				current = current.prev;
			}
			writer.close();
		//	System.out.println("SUM IS " + sum);
			//System.out.println(nov);
		}
		catch(Exception e){

		}
	}
	public void printElements(){
		Element current =elements.first.next; 
		while(current.prev != null){
			System.out.println(current.id + "," + current.start + "," + current.end);
			current = current.next;
		}
		System.out.println();
	}

}

public class Naloga3{

	public static void main(String[] args){
		String input = args[0];
		String output = args[1];
		File in = new File(input);
		
		try{
			Scanner sc = new Scanner(in);
			sc.useDelimiter("\\D");
			readAndExecuteCommands(sc,output);
		}	
		catch(Exception e){
			System.out.println(e);
		}

	}

	public static void readAndExecuteCommands(Scanner sc, String output){
		DataStructure data = new DataStructure();
		while(sc.hasNextLine()){
			String command = sc.nextLine();
			String[] parameters = command.split(",");
			int id = Integer.parseInt(parameters[0]);
			int start = Integer.parseInt(parameters[1]);
			int end = Integer.parseInt(parameters[2]);
			data.insertElement(id, start,end);
		}
		data.defrag();
		data.dump(output);
	}

}
				
