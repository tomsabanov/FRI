import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

class Edge{
    int edgeId;
    int destId;
    int weight;
    public Edge(int edgeId,int destId, int weight){
        this.edgeId = edgeId;
        this.destId = destId;
        this.weight = weight;
    }
}

class Graph{
    int numberOfNodes;
    LinkedList<Edge>[] edges; // list of edges for a node
    
    @SuppressWarnings("unchecked")
    public Graph(){
        this.numberOfNodes = 0;
        this.edges = new LinkedList[30000];
        for (int o = 0; o < 30000; o++) 
            this.edges[o] = new LinkedList<>(); 
    }

    public int getEdgeId(int src, int dest){
            LinkedList<Edge> node = this.edges[src];
            //now we find the edge with  destId=dest
            for(Edge e : node){
                if(e.destId == dest){
                    return e.edgeId;
                }
            }
            return -1;
    }
    public void increment(){
        this.numberOfNodes++;
    }

}

class PQElement{
    public int nodeId;
    public int key;

}

class comparator implements Comparator<PQElement> { 
    @Override
    public int compare(PQElement node0, PQElement node1) 
    { 
        return node0.key - node1.key; 
    } 
} 

public class Naloga8{

   static FileWriter writer;

    public static void main(String[] args){
            File input = new File(args[0]);
            String output = args[1];
            Naloga8Prim naloga = new Naloga8Prim();
            Graph g = new Graph();
            
            try{
                writer = new FileWriter(output);
                Scanner sc = new Scanner(input);
                sc.useDelimiter(",|\\s+");

                int M = readInt(sc);
                for(int i = 0; i<M; i++){
                    int roadId = readInt(sc);
                    int cityA = readInt(sc);
                    int cityB = readInt(sc);
                    int length = readInt(sc);

                    naloga.addEdge(g,roadId, cityA,cityB,length);
                    //System.out.println("GRAPGH NODES "+ g.numberOfNodes);

                }
                int carReach = readInt(sc);
                float probability = readFloat(sc);
                int[] result = naloga.findPath(g);

                write(g, result, writer);

                writer.close();

            }
            catch(Exception e){
                System.out.println(e);
            }
    
    }
    static void write(Graph graph, int[] result, FileWriter writer) throws IOException{
        //we need to get the edgeId of every src-dest in result
        int[] edges = new int[graph.numberOfNodes -1];
        for(int i = 1; i<graph.numberOfNodes; i++){
            
            int src = i;
            int dest = result[src];
            int edgeId = graph.getEdgeId(src,dest); 
            edges[i-1] = edgeId;
            //System.out.println("Edge id " + edgeId);
            //now we need to get only the edge id
        }
        Arrays.sort(edges);
        String s = "";
        for(int i = 0; i<edges.length;i++){
            if(edges[i] != -1){
                s+=edges[i] + ",";
            }
        }
        s = s.substring(0, s.length()-1);
        writer.write(s);
    }
    void addEdge(Graph graph, int edgeId,int src, int dest, int weight) 
    { 
        if(graph.edges[src].size() == 0){
            graph.increment();
        }

        if(graph.edges[dest].size() == 0){
            graph.numberOfNodes++;
            graph.increment();
        }
        Edge node0 = new Edge(edgeId,dest, weight); 
        Edge node = new Edge(edgeId,src, weight); 
        graph.edges[src].addLast(node0); 
        graph.edges[dest].addLast(node); 
    } 
    int[] findPath(Graph graph){
        // Whether a vertex is in PriorityQueue or not 
        Boolean[] mstset = new Boolean[graph.numberOfNodes]; 
        PQElement[] e = new PQElement[graph.numberOfNodes]; 

        // Stores the parents of a vertex 
        int[] parent = new int[graph.numberOfNodes]; 
        //System.out.println("PARENT LENGTH " + graph.numberOfNodes);

        for (int o = 0; o < graph.numberOfNodes; o++) 
            e[o] = new PQElement(); 

        for (int o = 0; o < graph.numberOfNodes; o++) { 

            mstset[o] = false; 

            // Initialize key values to infinity 
            e[o].key = Integer.MAX_VALUE; 
            e[o].nodeId = o; 
            parent[o] = -1; 
        } 


        //Include the source vertex in mstset 
        mstset[0] = true; 

        // Set key value to 0 
        // so that it is extracted first 
        // out of PriorityQueue 
        e[0].key = 0; 

        // PriorityQueue 
        PriorityQueue<PQElement> queue = new PriorityQueue<>(graph.numberOfNodes, new comparator()); 

        for (int o = 0; o < graph.numberOfNodes; o++) 
            queue.add(e[o]); 

        // Loops until the PriorityQueue is not empty 
        while (!queue.isEmpty()) { 
            // Extracts a node with min key value 
            PQElement node0 = queue.poll(); 

            // Include that node into mstset 
            mstset[node0.nodeId] = true; 

            // For all adjacent vertex of the extracted vertex V 
            for (Edge iterator : graph.edges[node0.nodeId]) { 

                // If V is in PriorityQueue 
                if (mstset[iterator.destId] == false) { 
                    // If the key value of the adjacent vertex is 
                    // more than the extracted key 
                    // update the key value of adjacent vertex 
                    // to update first remove and add the updated vertex 
                    if (e[iterator.destId].key > iterator.weight) { 
                        queue.remove(e[iterator.destId]); 
                        e[iterator.destId].key = iterator.weight; 
                        queue.add(e[iterator.destId]); 
                        parent[iterator.destId] = node0.nodeId; 
                    } 
                } 
            } 
        } 
        return parent;
    }
    public static float readFloat(Scanner s){
        while(s.hasNext()){
            if(s.hasNextFloat()){
                return s.nextFloat();
            }
            s.next();
        }
        return -1;
    }
    public static int readInt(Scanner s){
        while(s.hasNext()){
            if(s.hasNextInt()){
                return s.nextInt();
            }
            s.next();
        }
        return -1;
    }


}


