import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.*;

public class Test {
	public static void main(String[] args) {
		
		String line;
	
		try {

			BufferedReader bf_in = new BufferedReader(new FileReader(String.format("I_%s.txt", args[0])));

			line = bf_in.readLine();
			String infoLine[] = line.split(",");
			int m = Integer.parseInt(infoLine[0]);

			HashMap<Integer, Integer> lengths = new HashMap<Integer, Integer>();

			for (int ii = 0; ii < m; ii++) {
				line = bf_in.readLine();
				String lengthLine[] = line.split(",");
				lengths.put(Integer.parseInt(lengthLine[0]), Integer.parseInt(lengthLine[3]));
			}


			line = bf_in.readLine();
			int range = Integer.parseInt(line);

			line = bf_in.readLine();
			double defectChance = Double.parseDouble(line);

			bf_in.close();

			BufferedReader bf_test = new BufferedReader(new FileReader(String.format("O_%s.txt", args[0])));

			line = bf_test.readLine();
			String testLine[] = line.split(",");

			int testRes = 0, testNoOfRoads = 0;
			double testChance = 0;
			for (int ii = 0; ii < testLine.length; ii++) {
				testRes += lengths.get(Integer.parseInt(testLine[ii]));
				testChance += defectChance * lengths.get(Integer.parseInt(testLine[ii]));
				testNoOfRoads++;
			}

			bf_test.close();
			
			BufferedReader bf_my = new BufferedReader(new FileReader(String.format("out_%s.txt", args[0])));

			line = bf_my.readLine();
			String myLine[] = line.split(",");

			int myRes = 0, myNoOfRoads = 0;
			double myChance = 0;
			for (int ii = 0; ii < myLine.length; ii++) {
				myRes += lengths.get(Integer.parseInt(myLine[ii]));
				myChance += defectChance * lengths.get(Integer.parseInt(myLine[ii]));
				myNoOfRoads++;
			}

			bf_my.close();

			System.out.printf("TestRes was %d with the chance %f on %d roads, mine was %d with the chance %f on %d roads\n", testRes, testChance, testNoOfRoads, myRes, myChance, myNoOfRoads);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}


	}
}