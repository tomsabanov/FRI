import java.util.Scanner;
import java.io.File;
import java.io.FileWriter;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Arrays;



class Node{
    private short id;
    private ArrayList<Edge> edges;
    private boolean visited = false;
    public Node(short id){
        this.id = id;
        this.edges = new ArrayList<Edge>(1000);
    }
    public short getId(){
        return this.id;
    }
    public void addEdge(Edge edge){
        this.edges.add(edge);
    }
    public ArrayList<Edge> getEdges(){
        return this.edges;
    }
    public void setVisited(Boolean bool){
        this.visited = bool;
    }
    public Boolean getVisited(){
        return this.visited;
    }
}

class Edge{
    private short nodeId;
    private short edgeId;
    private short length;
    private boolean inTree = false;
    public Edge(short id, short edgeId, short length){
        this.nodeId = id;
        this.edgeId = edgeId;
        this.length = length;
    }
    public short getNodeId(){
        return this.nodeId;
    }
    public short getLength(){
        return this.length;
    }
    public short getEdgeId(){
        return this.edgeId;
    }
    public Boolean getInTree(){
        return this.inTree;
    }
    public void setInTree(Boolean bool){
        this.inTree = bool;
    }
}

class Graph{
    private Node[] nodes;
    private int numberOfNodes;
    
    private float basicProbability;
    private short basicReach;

    private Node firstNode;

    public Graph(){
        this.nodes = new Node[10000];
        this.numberOfNodes = 0;
    }
    public void setGraph(short carReach, float probability){
        this.basicReach = carReach;
        this.basicProbability = probability;
    }
    public float getBasicProbability(){
        return this.basicProbability;
    }
    public short getBasicReach(){
        return this.basicReach;
    }
    public Node getNode(short id){
        //get (and set) node
        Node node = this.nodes[id];
        if(node != null){
            return node;
        }
        node = new Node(id);
        this.nodes[id] = node;
        
        if(this.numberOfNodes == 0) this.firstNode = node;
        this.numberOfNodes++;

        return node;
    }
    public void add(short roadId, short idA, short idB, short length){
        Node a = this.getNode(idA);
        Node b = this.getNode(idB);

        Edge ab = new Edge(idB,roadId,length);
        Edge ba = new Edge(idA,roadId,length);

        a.addEdge(ab);
        b.addEdge(ba);
    }
    public Boolean start(Node current){
        return false;
    } 
    public Node getNode(){
        return this.firstNode;
    }
    public Node getNode(int index){
        return this.nodes[index];
    }
    public  int getNumberOfNodes(){
        return this.numberOfNodes;
    }
    public Short[] start(){
        Node first = getNode();  // get starting point
        first.setVisited(true);
        
        ArrayList<Short> path = new ArrayList<Short>(10000);
       
        Short[] usedNodes = new Short[10000];
        usedNodes[0]=first.getId();

        int numberOfNodes = this.getNumberOfNodes(); // min path should visit that many nodes


        float baseProbability = this.getBasicProbability();
        short baseReach = this.getBasicReach();

        int i = 1; // number of nodes in 'usedNodes'
        while(i != numberOfNodes){
            int size = i; // size of nodes already in the tree

            short min = Short.MAX_VALUE;
            Edge selected = null;
            short selectedNodeId = -1;
            // iterate over every node in already used nodes
            for(int j = 0; j<i;j++){ 
                short id = usedNodes[j]; 
                Node node = this.getNode(id);
                // iterate over every edge in the node
                ArrayList<Edge> edges = node.getEdges();
                for(Edge edge: edges){
                    // check if edge is already in tree
                    if(edge.getInTree()) continue;
                    short toNodeId = edge.getNodeId();
                    //check if id is already used, if it is continue with next edge
                    Node newNode = this.getNode(toNodeId);
                    if(newNode.getVisited()) continue;

                    //check if we can even travel on this edge (length of the edge should be <= baseReach)   
                    short length  = edge.getLength();
                    if(length>baseReach) continue;

                    // this node isn't yet in the tree
                    // we calculate the edge weight (probability
                    short weight = length; 
                    if(length<min){
                        min = weight;
                        selected = edge;
                        selectedNodeId = newNode.getId();
                    }

                }

            }

            i++;
            path.add(selected.getEdgeId());
            this.getNode(selectedNodeId).setVisited(true);
            selected.setInTree(true);
            usedNodes[i-1] = selectedNodeId;


        }
        
        Short[] p = path.toArray(new Short[path.size()]);    
        return p;
    }

}

public class Naloga8{

   static FileWriter writer;

    public static void main(String[] args){
            File input = new File(args[0]);
            String output = args[1];
            
            try{
                Graph graph = new Graph();
                writer = new FileWriter(output);
                Scanner sc = new Scanner(input);
                sc.useDelimiter(",|\\s+");

                int M = readInt(sc);
                for(int i = 0; i<M; i++){
                    short roadId = readShort(sc);
                    short cityA = readShort(sc);
                    short cityB = readShort(sc);
                    short length = readShort(sc);
                    graph.add(roadId, cityA,cityB,length);
                }
                short carReach = readShort(sc);
                float probability = readFloat(sc);

                graph.setGraph(carReach, probability);

                Short[] result = graph.start();
                Arrays.sort(result);
                System.out.println(Arrays.toString(result));
               
                String s = "";
                for(int i = 0; i<result.length; i++){
                    s+=result[i] + ",";
                }
                s = s.substring(0, s.length() - 1);
                writer.write(s);

                //printGraph(graph);

                writer.close();

            }
            catch(Exception e){
                System.out.println(e);
            }
    
    }

    public static short readShort(Scanner s){
        while(s.hasNext()){
            if(s.hasNextShort()){
                return s.nextShort();
            }
            s.next();
        }
        return -1;
    }
    public static float readFloat(Scanner s){
        while(s.hasNext()){
            if(s.hasNextFloat()){
                return s.nextFloat();
            }
            s.next();
        }
        return -1;
    }
    public static int readInt(Scanner s){
        while(s.hasNext()){
            if(s.hasNextInt()){
                return s.nextInt();
            }
            s.next();
        }
        return -1;
    }

    public static void printGraph(Graph g){
        int numberOfNodes = g.getNumberOfNodes();
        Node node = g.getNode(); // get first node, from which we will derive all other nodes
        ArrayList<Node> usedNodes = new ArrayList<Node>();
        usedNodes.add(node);
        
        short  i = 0;
        while(i != numberOfNodes){
            Node n = usedNodes.get(i);
            printNode(n);
            i++;
            ArrayList<Edge> edges = n.getEdges();
            for(int j = 0; j<edges.size();j++){
                Edge e = edges.get(j);
                int nodeId = e.getNodeId();
                Node newNode = g.getNode(nodeId);
                if(usedNodes.contains(newNode)) continue;
                usedNodes.add(newNode);
            }
        }
    }
    public static void printNode(Node node){
        ArrayList<Edge> edges = node.getEdges();
        short nodeId = node.getId();
        int size = edges.size();
        System.out.println("Node " + nodeId + " has edges:");
        for(int i = 0; i<size;i++){
            Edge e = edges.get(i);
            String s = "     " + nodeId + " -----> " + e.getNodeId(); 
            System.out.println(s);
        }
    }

}


