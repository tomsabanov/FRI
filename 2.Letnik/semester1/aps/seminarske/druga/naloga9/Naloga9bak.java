import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.*;

public class Naloga9bak {

	private static int n;

	public static void main(String[] args) {

	    // long start = System.currentTimeMillis();

	    Network network = new Network();

		try {

			BufferedReader bf = new BufferedReader(new FileReader(args[0]));
			PrintWriter pw = new PrintWriter(args[1]);
			String line;

			line = bf.readLine();
			String infoLine[] = line.split(",");
			n = Integer.parseInt(infoLine[0]);

			for (int ii = 0; ii < n; ii++) {
				line = bf.readLine();
				String connectionLine[] = line.split(",");
				if (!network.befriend(Integer.parseInt(connectionLine[0]), Integer.parseInt(connectionLine[1]))) {
					// System.out.println(line);
					pw.println(line);
				}
			}

			bf.close();
			pw.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	    // long end = System.currentTimeMillis();
	    // System.out.printf("The program ran for %d ms.\n", end - start);

	}

	private static class Person implements Comparable<Person> {
		int id;

		public Person (int id) {
			this.id = id;
		}

		@Override
		public int compareTo(Person p1) {
			return this.id - ((Person) p1).id;
		}
	}

	private static class Network {
		HashMap<Integer, Person> people;
		TreeSet<ComparableTreeSet<Person>> groups;

		public Network () {
			this.makenull();
		}

		public void makenull () {
			this.people = new HashMap<Integer, Person>();
			this.groups = new TreeSet<ComparableTreeSet<Person>>();
		}

		private boolean areFriends (Person p1, Person p2) {
			for (ComparableTreeSet<Person> group : this.groups) {
				boolean containsP1 = group.contains(p1);
				boolean containsP2 = group.contains(p2);
				if (containsP1 && containsP2) return true;
				else if (containsP1 ^ containsP2) return false;
			}
			return false;
		}

		private void combine (Person p1, Person p2) {
			ComparableTreeSet<Person> p1Group = null, p2Group = null;
			for (ComparableTreeSet<Person> group : this.groups) {
				if (p1Group == null && group.contains(p1)) p1Group = group;
				if (p2Group == null && group.contains(p2)) p2Group = group;
				if (p1Group != null && p2Group != null) break;
			}

			p1Group.addAll(p2Group);
			p2Group.clear();            
//			this.groups.remove(p2Group);

		}

		private void addTo (Person existing, Person justAdded) {

			for (ComparableTreeSet<Person> group : this.groups) {
				if (group.contains(existing)) {
					group.add(justAdded);
				}
			}

		}

		private void create (Person p1, Person p2) {

			ComparableTreeSet<Person> newGroup = new ComparableTreeSet<Person>();

			newGroup.add(p1);
			newGroup.add(p2);

			this.groups.add(newGroup);
			
		}

		public boolean befriend (int id1, int id2) {
			Person p1 = this.people.get(id1);
			Person p2 = this.people.get(id2);

			if (p1 != null && p2 != null) {
				if (this.areFriends(p1, p2)) { // they were already friends
					return false;
				} else { // they were not friends before
					this.combine(p1, p2);
					return true;
				}
			}

			boolean p1New = p1 == null, p2New = p2 == null;

			if (p1New) {
				p1 = new Person(id1);
				this.people.put(id1, p1);
				if (!p2New) this.addTo(p2, p1);
			}

			if (p2New) {
				p2 = new Person(id2);
				this.people.put(id2, p2);
				if (!p1New) this.addTo(p1, p2);
			}

			if (p1New && p2New) this.create(p1, p2);

			return true; // they were not friends before
		}
	}

	private static class ComparableTreeSet<T> extends TreeSet implements Comparable {
		
		@Override
		public int compareTo(Object o) {
			if (this.isEmpty() || ((ComparableTreeSet<Person>) o).isEmpty()) return this.hashCode() - o.hashCode();
			else return ((Person) this.first()).compareTo((Person) ((ComparableTreeSet) o).first());
		}

	}
}
