import java.util.Scanner;
import java.io.File;
import java.io.FileWriter;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Arrays;
class Node{
    private int cityId;
    private ArrayList<Edge> edges;
    private boolean visited = false;
    public Node(int id){
        this.cityId = id;
        this.edges = new ArrayList<Edge>();
    }
    public int getId(){
        return this.cityId;
    }
    public void addEdge(Edge edge){
        this.edges.add(edge);
    }
    public ArrayList<Edge> getEdges(){
        return this.edges;
    }
    public void setVisited(Boolean bool){
        this.visited = bool;
    }
    public Boolean getVisited(){
        return this.visited;
    }
}

class Edge{
    private Node friend;
    public Edge(Node friend){
        this.friend = friend;
    }
    public Node getFriend(){
        return this.friend;
    }
}

class Graph{
    private Node start;
    private Node end;
    private HashMap<Integer, Node> nodes;

    public Graph(){
        this.start = null;
        this.end = null;
        nodes = new HashMap<Integer,Node>();
    }
    public void setGraph(int startId, int endId){
        this.start = getNode(startId);
        this.end = getNode(endId);
    }
    public Node getNode(int id){
        //get (and set) node
        if(nodes.containsKey(id)){
            return nodes.get(id);
        }
        Node node = new Node(id);
        this.nodes.put(id, node);
        return node;
    }
    public Boolean nodeExists(int id){
        if(nodes.containsKey(id)){
            return true;
        }
        return false;
    }
    public Boolean add(int idA, int idB){
        
        // we have to get from friendA to friendB
        //set start node and end node
        this.setGraph(idA, idB);

        //if either one of these nodes don't exist, we immediately return false, the information isn't usseless
        if(this.nodeExists(idA) == false || this.nodeExists(idB) == false) return false;


        Node nodeA = getNode(idA);

        nodeA.setVisited(true);
        int number = this.calculatePaths(nodeA);
        nodeA.setVisited(false);

        if(number > 0) return true;


        Node a = this.getNode(idA);
        Node b = this.getNode(idB);

        Edge ab = new Edge(b);
        Edge ba = new Edge(a);

        a.addEdge(ab);
        b.addEdge(ba);

        return false;
    }
    public Node getStartNode(){
        return this.start;
    }
    public  Node getEndNode(){
        return this.end;
    }
    public HashMap<Integer, Node> getNodes(){
        return this.nodes;
    }
    public int calculatePaths(Node current){
        //check if current node is 'end' node 
        if(current == this.end){
            return 1;
        }
        //get all edges of node
        ArrayList<Edge> edges = current.getEdges();
        
        int sum = 0;
        //iterate over edges and check if the friend in edge has already been visited
        for(Edge edge : edges){
            Node friend = edge.getFriend();
            if(friend.getVisited()) continue;
                
            //visit the friend in edge, mark it as visited, get the number of different paths from there
            friend.setVisited(true);
            sum += this.calculatePaths(friend);
            friend.setVisited(false);
        }
        return sum;
    }
}

public class Naloga9{

   static FileWriter writer;

    public static void main(String[] args){
            File input = new File(args[0]);
            String output = args[1];
            
            try{
                Graph graph = new Graph();
                writer = new FileWriter(output);
                Scanner sc = new Scanner(input);
                sc.useDelimiter(",|\\s+");

                int M = readInt(sc);
                for(int i = 0; i<M; i++){
                    int friendA = readInt(sc);
                    int friendB = readInt(sc);
                    Boolean bool = graph.add(friendA, friendB);
                    if(bool == true) writer.write(""+friendA+","+friendB  + "\n");
                }

                writer.close();
                // printGraph(graph);

            }
            catch(Exception e){
                System.out.println(e);
            }
    
    }

    public static int readInt(Scanner s){
        while(s.hasNext()){
            if(s.hasNextInt()){
                return s.nextInt();
            }
            s.next();
        }
        return -1;
    }

    public static void printGraph(Graph g){
        HashMap<Integer,Node> nodes = g.getNodes();
        Iterator it = nodes.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry pair = (Map.Entry)it.next();
            Node node = (Node)pair.getValue(); 
            printNode(node);
        }
    }
    public static void printNode(Node node){
        ArrayList<Edge> edges = node.getEdges();
        System.out.println("Node " + node.getId() + " has edges :");
        for(Edge edge: edges){
            String s = "      " + node.getId() + " ------> " + edge.getFriend().getId();
            System.out.println(s);
        }
    }

}


