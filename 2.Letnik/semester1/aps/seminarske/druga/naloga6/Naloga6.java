import java.util.Scanner;
import java.io.File;
import java.io.FileWriter;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Arrays;
class Node{
    private int cityId;
    private ArrayList<Edge> edges;
    private boolean visited = false;
    public Node(int id){
        this.cityId = id;
        this.edges = new ArrayList<Edge>();
    }
    public int getId(){
        return this.cityId;
    }
    public void addEdge(Edge edge){
        this.edges.add(edge);
    }
    public ArrayList<Edge> getEdges(){
        return this.edges;
    }
    public void setVisited(Boolean bool){
        this.visited = bool;
    }
    public Boolean getVisited(){
        return this.visited;
    }
}

class Edge{
    private Node city;
    private int height;
    public Edge(Node city, int height){
        this.city = city;
        this.height = height;
    }
    public Node getCity(){
        return this.city;
    }
    public int getHeight(){
        return this.height;
    }

}

class Graph{
    private Node start;
    private Node end;
    private int height;
    private HashMap<Integer, Node> nodes;

    public Graph(){
        this.start = null;
        this.end = null;
        nodes = new HashMap<Integer,Node>();
    }
    public void setGraph(int startId, int endId, int height){
        this.start = getNode(startId);
        this.end = getNode(endId);
        this.height = height;
    }
    public Node getNode(int id){
        //get (and set) node
        if(nodes.containsKey(id)){
            return nodes.get(id);
        }
        Node node = new Node(id);
        this.nodes.put(id, node);
        return node;
    }
    public void add(int idA, int idB, int height){
        Node a = this.getNode(idA);
        Node b = this.getNode(idB);

        Edge ab = new Edge(b,height);
        Edge ba = new Edge(a, height);

        a.addEdge(ab);
        b.addEdge(ba);

    }
    public Node getStartNode(){
        return this.start;
    }
    public  Node getEndNode(){
        return this.end;
    }
    public HashMap<Integer, Node> getNodes(){
        return this.nodes;
    }
    public int calculatePaths(Node current){
        //check if current node is 'end' node 
        if(current == this.end){
            return 1;
        }
        //get all edges of node
        ArrayList<Edge> edges = current.getEdges();
        
        int sum = 0;
        //iterate over edges and check if the city in edge has already been visited
        for(Edge edge : edges){
            Node city = edge.getCity();
            if(city.getVisited()) continue;
            
            if(edge.getHeight() < this.height && edge.getHeight() != -1) continue;
                
            //visit the city in edge, mark it as visited, get the number of different paths from there
            city.setVisited(true);
            sum += this.calculatePaths(city);
            city.setVisited(false);
        }
        return sum;
    }
}

public class Naloga6{

   static FileWriter writer;

    public static void main(String[] args){
            File input = new File(args[0]);
            String output = args[1];
            
            try{
                Graph graph = new Graph();
                writer = new FileWriter(output);
                Scanner sc = new Scanner(input);
                sc.useDelimiter(",|\\s+");

                int M = readInt(sc);
                for(int i = 0; i<M; i++){
                    int cityA = readInt(sc);
                    int cityB = readInt(sc);
                    int height = readInt(sc);
                    graph.add(cityA,cityB,height);
                }

                int start = readInt(sc);
                int end = readInt(sc);
                int height = readInt(sc);
                graph.setGraph(start,end, height);
                
                Node startNode = graph.getStartNode();
                startNode.setVisited(true);
                String number = graph.calculatePaths(startNode) + "\n";
                System.out.print(number);
                writer.write(number);
                writer.close();
                printGraph(graph);

            }
            catch(Exception e){
                System.out.println(e);
            }
    
    }

    public static int readInt(Scanner s){
        while(s.hasNext()){
            if(s.hasNextInt()){
                return s.nextInt();
            }
            s.next();
        }
        return -1;
    }

    public static void printGraph(Graph g){
        HashMap<Integer,Node> nodes = g.getNodes();
        Iterator it = nodes.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry pair = (Map.Entry)it.next();
            Node node = (Node)pair.getValue(); 
            printNode(node);
        }
    }
    public static void printNode(Node node){
        ArrayList<Edge> edges = node.getEdges();
        System.out.println("Node " + node.getId() + " has edges :");
        for(Edge edge: edges){
            String s = "      " + node.getId() + " ------> " + edge.getCity().getId() + " height " + edge.getHeight();
            System.out.println(s);
        }
    }

}


