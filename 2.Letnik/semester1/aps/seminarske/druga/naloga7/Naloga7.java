import java.util.Scanner;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

public class Naloga7{
    static FileWriter writer;
    static List<Data> list = new ArrayList<>();
    static Map<Integer, List<Data>> mapData = new HashMap<>();    

    public static void main(String[] args){
            File input = new File(args[0]);
            String output = args[1];
            try{
              writer = new FileWriter(output);
            }
            catch(Exception e){

            }

            Tree tree = new Tree();
            int N = 0;
            try{
                Scanner sc = new Scanner(input);
                sc.useDelimiter(",|\\s+");
                N = readInt(sc);
                for(int i = 0; i<N; i++){
                    int id = readInt(sc);
                    int value = readInt(sc);
                    int left = readInt(sc);
                    int right = readInt(sc);

                    tree.addNode(id, value,left,right);
                }
                tree.setRoot();
                
                Node root = tree.getRoot();
                sortToQueue(root,0);

                dumpData();
                
            }
            catch(Exception e){
                System.out.println(e);
            }
    
    }
    public static int readInt(Scanner s){
        while(s.hasNext()){
            if(s.hasNextInt()){
                return s.nextInt();
            }
            s.next();
        }
        return -1;
    }

    public static void sortToQueue(Node root, int y){
        if(root == null) return;

        sortToQueue(root.left,y+1);
        
        Data data = new Data(root.value, y);
        list.add(data);

        sortToQueue(root.right, y+1);


    }
    public static void  dumpData() throws IOException{
        int x = 0;
        Iterator<Data> it = list.iterator();
        
        //set the x coordinates and save data to hashmap
        Data data;
        while(it.hasNext()){
            data = it.next();
            data.setX(x);
            if(mapData.containsKey(data.y) == false){
                List<Data> l = new ArrayList<>();
                l.add(data);
                mapData.put(data.y,l);
            }
            else{
                mapData.get(data.y).add(data);
            }
            x++;
        }

        for(int i=0;;i++){
            //check if map has key i
            if(mapData.containsKey(i) == false) break;
            
            List<Data> dataList = mapData.get(i);
            Iterator<Data> iterator = dataList.iterator();
            while(iterator.hasNext()){
                writeToFile(iterator.next());
            }
        }

        writer.close();
    }

    public static void writeToFile(Data data){
        try{
            String s = data.value + "," + data.x + "," + data.y + "\n";
            writer.write(s);        
        }
        catch(Exception e){
            System.out.println(e);
        }
    }
}

class Data{
    int y;
    int value;
    int x;
    public Data(int value, int y){
        this.value = value;
        this.y = y;
    }
    public void setX(int x){
        this.x = x;
    }
}



class Node{
    int id;
    int value = -1;
    Node right;
    Node left;

    public Node(int id, int value, Node left, Node right){
        this.id = id;
        this.value = value;
        this.right = right;
        this.left = left;
    }

    public Node(int id){
        this.id = id;
        this.right = null;
        this.left = null;
    }

    public void setNode(int value, Node left, Node right){
        this.value = value;
        this.right = right;
        this.left = left;
    }

}

class Tree{
    Node root;

    HashMap<Integer, Node> map = new HashMap<Integer,Node>();
    HashMap<Integer, Integer> parentMap = new HashMap<Integer, Integer>();
    public Tree(){
        this.root = null;
    }

    public void addNode(int id, int value, int left, int right){
        //first we get/init child nodes
        Node leftChild = initChildNode(left);
        Node rightChild = initChildNode(right);

        Node root = initRootNode(id, value, leftChild, rightChild);
    }

    public Node initChildNode(int id){
        if(id == -1) return null;

        if(parentMap.containsKey(id)){
            parentMap.remove(id);

        }
        else{
            parentMap.put(id, -1);
        }

        if(map.containsKey(id)){
            return map.get(id);
        }
        Node new_node = new Node(id);

        map.put(id, new_node);

        return new_node;

    }

    public Node initRootNode(int id, int value, Node left, Node right){
        if(parentMap.containsKey(id)){
                parentMap.remove(id);
        }
        else{
                parentMap.put(id,-1);
        }
        
        if(map.containsKey(id)){
            Node node =  map.get(id);
            node.setNode(value, left, right);
            return node;
        }

        Node new_node = new Node(id, value, left, right);
        map.put(id, new_node);
        return new_node;
    }

    public void setRoot(){
                Iterator it = parentMap.entrySet().iterator();
                int i = 0;
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry)it.next();

                    int rootKey = (int)pair.getKey();
                    root = map.get(rootKey);

                    it.remove(); 
                    i++;
                }
    }
    public Node getRoot(){
        return root;
    }

}

