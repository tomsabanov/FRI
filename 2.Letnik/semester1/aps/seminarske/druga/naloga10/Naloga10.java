import java.util.Scanner;
import java.io.File;
import java.io.FileWriter;

import java.util.*;

import java.io.IOException;

class Node{
    private int id;
    private LinkedList<Edge> edges;
    private int distance = 0; // represents the aproximated distance to the end node

    private int iteration = 0;

    public Node(int id){
        this.id = id;
        edges = new LinkedList<Edge>();
    }

    public void addEdge(int id,Edge edge){
        edges.add(edge);
    }
    public LinkedList<Edge> getEdges(){
        return this.edges;
    }

    public void setAproximation(int val){
        this.distance = val;
    }
    public int getAproximation(){
        return this.distance;
    }
    public int getId(){
        return this.id;
    }
    public int getIteration(){
        return this.iteration;
    }
    public void setIteration(int i){
        this.iteration = i;
    }
    
}

class Edge{
    private Node to;
    private int price;
    public Edge(Node to, int price){
        this.to = to;
        this.price = price;
    }
    public Node getEndNode(){
        return this.to;
    }
    public int getPrice(){
        return this.price;
    }
}

class Graph{
    private Node start;
    private Node end;
    
    private Node[] nodes = null;


    public  Graph(){
        this.start = null;
        this.end = null;
        nodes = new Node[10001];
    }
    public Node getStart(){
        return this.start;
    }
    public  void add(int from ,int to, int price){
        //create the edge on from node
        Node a  = getNode(from);
        Node b = getNode(to);
        
        Edge edge = new Edge(b, price);

        //add the edge to 'a' node ('a' = 'from node')
        a.addEdge(to,edge);
    }
    public void setGraph(int start, int end){
        this.start = getNode(start);
        this.end = getNode(end);
    }

    private Node getNode(int id){
        //check if Node from already exists and retrieve it, otherwise create it
        if(nodes[id] != null){
            return nodes[id];
        }
        Node node = new Node(id);
        nodes[id] = node;

        /*if(nodes.containsKey(id)){
            return nodes.get(id);
        }
        Node node = new Node(id);
        this.nodes.put(id, node);
        */
        return node;
    }


    private int lastIterationPrice = 0;
    private int iteration = 1;
    private boolean sameIterations = true;

    public String iterate(){

       Node current = start;    
       String path = "";
       this.sameIterations = true;
       while(true){
            int currentId = current.getId();
            current.setIteration(this.iteration);
            path += currentId + ",";
            
            if(current == this.end) break; //check if current ==  end node
        
            Node next = this.chooseNextNode(current); //get next Node to which we will go to, price is updated
            if(next == null) break; //if next node is null, we have nowhere to go from here, so we end the iteration

            current = next; // we go to the next node
       }
       this.iteration++; 
       return path.substring(0, path.length() - 1) + "\n";
    }
    public void resetIterationPrice(){
        this.lastIterationPrice = 0;
    }
    public boolean checkIterations(){
        return this.sameIterations;
    }
    
    private Node chooseNextNode(Node current){
        LinkedList<Edge> edges = current.getEdges();
        ListIterator<Edge> it = edges.listIterator();

        int sum = Integer.MAX_VALUE;
        int chosenEdgePrice = Integer.MAX_VALUE;

        Node chosen = null;
        int chosenId = Integer.MAX_VALUE;
        while(it.hasNext()){
            Edge edge = it.next(); 
            Node nextNode = edge.getEndNode();
            int nextNodeId = nextNode.getId();
            
            if(nextNode.getIteration() == this.iteration) continue; // check if this node was already in path in current iteration

            int edgePrice = edge.getPrice();
            int nextNodeAproximation = nextNode.getAproximation();

            int newPrice = edgePrice + nextNodeAproximation;
            if(newPrice <= sum){
                if(newPrice == sum && (chosen != null && nextNodeId>chosenId))continue; //we chose the one with smaller ID
                sum = newPrice;
                chosen = nextNode;
                chosenId = nextNode.getId();
                chosenEdgePrice = edgePrice;
            }
        }

        int currentAproximation = current.getAproximation();
        if(currentAproximation < sum && chosen!=null){
            current.setAproximation(sum);
            this.sameIterations = false;
        }
        
        if(chosen != null) this.lastIterationPrice += chosenEdgePrice; //we increase the price of the iteration
                
        return chosen;
    }


    public int getLastIterationPrice(){
        return this.lastIterationPrice;
    }

    public Node[] getNodes(){
        return this.nodes;
    }
}

public class Naloga10{

   static FileWriter writer;

    public static void main(String[] args){
        long startt = System.currentTimeMillis(); 
            
            File input = new File(args[0]);
            String output = args[1];
            
            try{
                writer = new FileWriter(output);
                Graph graph = new Graph();
                
                Scanner sc = new Scanner(input);
                sc.useDelimiter("\\D");
                String m = sc.nextLine();
                int M = Integer.parseInt(m);
                for(int i = 0; i<M; i++){
                    String line = sc.nextLine();
                    String[] inputs = line.split(",");
                    int from = Integer.parseInt(inputs[0]);
                    int to = Integer.parseInt(inputs[1]);
                    int price = Integer.parseInt(inputs[2]); 
                    graph.add(from, to, price);
                }
                
                String last = sc.nextLine();
                String [] lastInputs = last.split(",");
                int start = Integer.parseInt(lastInputs[0]);
                int end = Integer.parseInt(lastInputs[1]);
                graph.setGraph(start, end);
                
                start(graph);
                //printGraph(graph);

            }
            catch(Exception e){
                System.out.println(e);
            }
    
    }
    public static void printGraph(Graph g){
        Node[] map = g.getNodes();
        for(int i = 0; i<map.length; i++){
            Node node = map[i];
            if(node == null) continue;
            printNode(node);
            System.out.println("Printed a node!");
        }
    }
    public static void printNode(Node node){
        LinkedList<Edge> edges = node.getEdges();
        ListIterator<Edge> it = edges.listIterator();
        
        String s1 = "Node " + node.getId() + " has the following edges:\n";
        System.out.println(s1);
        while(it.hasNext()){
            Edge edge = it.next();
            String s = "    " + node.getId() + " ----> " +  edge.getEndNode().getId() +" with price " + edge.getPrice() + "\n";        
            System.out.println(s);
        }
    }

    public static void  start(Graph g) throws IOException{

        int oldSum = Integer.MAX_VALUE;
        String result = "";
        while(true){
            //we iterate until end condition
            result += g.iterate();
            long end = System.currentTimeMillis(); 

            int sum = g.getLastIterationPrice();
            g.resetIterationPrice();//we must reset the iterationPrice

            if(sum == oldSum){
                //there is a chance that the iterations are the same
                if(g.checkIterations()) break; //we stop iterating if the paths were the same with no change        
            }
            oldSum = sum;
        }
        writeToFile(result);
        writer.close();
    }

    public static void writeToFile(String path){
        try{
            writer.write(path);

        }
        catch(Exception e){
            System.out.println(e);
        }
    }

}


