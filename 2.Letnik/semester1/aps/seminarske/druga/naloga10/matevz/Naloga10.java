import java.io.*;
import java.util.*;

public class Naloga10 {
	// All nodes from input
	static HashMap<Integer, Node> nodes = new HashMap<Integer, Node>();
	// Element inside current path
	static boolean[] insidePath;
	static boolean[] insidePathPrev;
	// Output string
	static String output;
	
	static int startID, finishID;
	static boolean endReached = false;
	static int maxID = 0;
	
	public static void main(String[] args) {
		// Read input file
		try (BufferedReader br = new BufferedReader(new FileReader(args[0]))) {
			int edgeCount = Integer.parseInt(br.readLine());

			for (int i = 0; i < edgeCount; i++) {
				String[] customerData = br.readLine().split(",");

				int nodeID1 = Integer.parseInt(customerData[0]);
				int nodeID2 = Integer.parseInt(customerData[1]);
				int price = Integer.parseInt(customerData[2]);
				
				if (nodeID1 > maxID)
					maxID = nodeID1;
				if (nodeID2 > maxID)
					maxID = nodeID2;
				
				Node tmpNode1 = nodes.get(nodeID1);
				Node tmpNode2 = nodes.get(nodeID2);
				
				if (tmpNode2 == null) {
					tmpNode2 = new Node(nodeID2);
					nodes.put(nodeID2, tmpNode2);
				}
				if (tmpNode1 == null) {
					tmpNode1 = new Node(nodeID1);
					nodes.put(nodeID1, tmpNode1);
				}
				tmpNode1.addEdge(price, tmpNode2);	
			}
			
			String[] customerData = br.readLine().split(",");
			startID = Integer.parseInt(customerData[0]);
			finishID = Integer.parseInt(customerData[1]);
		}
		catch (IOException e) {
			e.printStackTrace();
	    }
		
		output = new String();
		insidePath = new boolean[maxID + 1];

		findPath();
		
		try {
			PrintWriter out = new PrintWriter(args[1]);
			out.println(output);
			out.close();
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
	    }
	}
	
	private static void findPath() {
		// Main loop
		while (!endReached) {
			Node currNode = nodes.get(startID);
			insidePath = new boolean[maxID + 1];
			
			// Single iteration loop
			while (true) {
				output = output.concat(Integer.toString(currNode.id));
				insidePath[currNode.id] = true;
				
				// Check for destination
				if (currNode.id == finishID) {
					if (Arrays.equals(insidePath, insidePathPrev)) {
						endReached = true;
					}	
					else {
						insidePathPrev = insidePath.clone();
						output = output.concat("\n");
					}
					break;
				}
				
				// Get edge with minimal distance
				if (!currNode.edges.isEmpty()) {
					int minDist = -1;
					Edge minEdge = null;
					
					for (Edge edge : currNode.edges) {
						if (!insidePath[edge.child.id] && edge.child.approxDist != -1) {
							int dist = edge.price + edge.child.approxDist;
						    if (dist < minDist || minDist == -1 || (dist == minDist && edge.child.id < minEdge.child.id)) {
						    	minDist = dist;
						    	minEdge = edge;
						    }
						}
					}
					
					if (minEdge == null) {
						output = output.concat("\n");
						break;
					}
					else {
						output = output.concat(",");
						if (minDist > currNode.approxDist)
							currNode.approxDist = minDist;
						
						currNode = minEdge.child;
					}	
				}
				else {
					currNode.approxDist = -1;
					break;
				}
			}
		}
	}
}

class Node {
	int id;
	int approxDist = 0;
	LinkedList<Edge> edges = new LinkedList<Edge>();
	
	public Node(int id) {
		this.id = id;
	}
	
	public void addEdge(int price, Node child) {
		this.edges.add(new Edge(price, child));
	}
}

class Edge {
	int price;
	Node child;
	
	public Edge(int price, Node child) {
		this.price = price;
		this.child = child;
	}
}