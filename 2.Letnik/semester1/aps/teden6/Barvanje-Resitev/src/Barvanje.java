
//
// Razred resuje problem barvanja grafa, ki je podan v matricni obliki.
// Ob instanciranju razreda konstruktor prejme nxn matriko binarnih
// vrednosti, kjer je n stevilo vozlisc. Ce sta vozlisci i in j povezani,
// sta v matriki na mestih (i,j) ter (j,i) enici.
//

import java.util.*;

public class Barvanje 
{	
	// Hranili bomo graf in barve vozlisc.
	int[][] graf;
	int[] vozlisca;
	
	Barvanje(int[][] g)
	{
		graf = g;
		vozlisca = new int[graf.length];
	}
	
	public void reset()
	{
		for (int i = 0; i < vozlisca.length; i++)
			vozlisca[i] = 0;
	}
	
	// Vrne stevilo povezav, ki vkljucujejo podano vozlisce.
	private int stopnja(int vozlisce)
	{
		int stopnja = 0;
		for (int i= 0; i < graf[vozlisce].length; i++)
			if (graf[vozlisce][i] > 0)
				stopnja++;
			
		return stopnja;
	}
	
	// Vrne indeks se nepobarvanega vozlisca z najvecjo stopnjo.
	private int izberi()
	{
		int v = -1;
		int vStopnja = -1;
		
		for (int j = 0; j < vozlisca.length; j++)
		{
			if (vozlisca[j] == 0)
			{
				int st = stopnja(j);
				if (st > vStopnja)
				{
					v = j;
					vStopnja = st;
				}
			}
		}
		
		return v;
	}
	
	// Preveri, ce podano vozlisce lahko pobarvamo s podano barvo,
	// ne da bi pri tem krsili omejitve barvanja.
	public boolean dovoljeno(int vozlisce, int barva)
	{
		for (int i = 0; i < graf[vozlisce].length; i++)
			if (graf[vozlisce][i] > 0 && vozlisca[i] == barva)
				return false;
			
		return true;
	}
	
	// Pristop, ki vozlisca barva po vrsti, kot se pojavijo v matriki
	// Vsako vozlisce pobarva in se premakne na naslednje do zadnjega.
	public void pobarvaj_zaporedno()
	{
		int stBarv = 1;
		
		reset();
		
		for(int v=0; v < graf.length; v++)
		{
			// poskusimo ga pobarvati
			for (int j = 1; j <= stBarv; j++)
			{
				if (dovoljeno(v, j))
				{
					vozlisca[v] = j;
					break;
				}
			}
			
			if (vozlisca[v] == 0)
			{
				vozlisca[v] = ++stBarv;
			}
		}
	}
	
	// Na vsakem koraku izmed se nepobarvanih vozlisc izberemo
	// tisto z najvecjo stopnjo in ga pobaravno.
	public void pobarvaj_pozresno()
	{
		int stBarv = 1;
		int v;
	
		reset();
		
		for(;;)
		{
			v = izberi();
			
			if (v == -1)
				break;
				
			// poskusimo ga pobarvati
			for (int j = 1; j <= stBarv; j++)
			{
				if (dovoljeno(v, j))
				{
					vozlisca[v] = j;
					break;
				}
			}
			
			if (vozlisca[v] == 0)
			{
				vozlisca[v] = ++stBarv;
			}
		}
	}
	
	// Pomozna funkcija. Dodeli trenutnemu vozliscu barvo
	// med 1 in max ter poskusi pobarvati preostanek vozlisc.
	// Ce barvanje ne uspe poskusi trenutno vozlisce pobarvati
	// z naslednjo barvo.
	private boolean pobarvaj_izcrpnoRek(int v, int max)
	{
		if (v >= vozlisca.length)
			return true;
			
		for (int b = 1; b <= max; b++)
		{
			if (dovoljeno(v, b)) 
			{
				vozlisca[v] = b;
				
				if (pobarvaj_izcrpnoRek(v+1, max))
					return true;
			}
		}
		
		vozlisca[v] = 0;
		
		return false;
	}
	
	// Preizkusi vsa mozna barvanja z dolocenim najvecjim stevilom barv.
	// Ce nobeno izmed barvanj ni veljavno, povecaj stevilo barv in poskusi
	// ponovno.
	public void pobarvaj_izcrpno()
	{
		reset();
		
		for (int max = 1; max <= vozlisca.length; max++)
		{
			if(pobarvaj_izcrpnoRek(0, max))
				break;
		}
	}
	
	public void izpisi_barve()
	{
		for (int i = 0; i < vozlisca.length; i++)
			System.out.print(vozlisca[i] + ", ");

		System.out.println();
	}
	
	public static int[][] generiraj_graf(int n, double p, Random rand)
	{
		int[][] g = new int[n][n];
		
		for (int i = 1; i < n; i++)
			for (int j = i + 1; j < n; j++)
			{
				if (rand.nextDouble() < p)
				{
					g[i][j] = 1;
					g[j][i] = 1;
				}
			}
		
		return g;
	}
	
	public static void izpisi_graf(int[][] graf)
	{
		for (int i = 0; i < graf.length; i++)
		{
			for (int j = 0; j < graf[i].length; j++)
				System.out.print(graf[i][j] + " ");
			
			System.out.println();
		}
	}
	
	public static void main(String[] args) 
	{
		Random rand = new Random();
		
		/*
		int[][] graf = {
				{0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1},
				{0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0},
				{0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1},
				{1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0},
				{0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1},
				{1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0},
				{0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0},
				{1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0},
				{0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1},
				{0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0},
				{0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1},
				{1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0},
				{0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1},
				{1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0},
				{0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0},
				{1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0}};
		*/
		
		int[][] graf = generiraj_graf(20, 0.4, rand);
		izpisi_graf(graf);
		
		System.out.println();
		
		Barvanje bojan = new Barvanje(graf);
		int stPonovitev = 1000;
		
		// zaporedno barvanje vozlisc
		System.out.println("Zaporedno barvanje vozlisc:");
		long zacetniCas = System.nanoTime();

		for (int i = 0; i < stPonovitev; i++)
			bojan.pobarvaj_zaporedno();
		
		double povCasIzvajanja = (double)(System.nanoTime() - zacetniCas) / stPonovitev;
		
		bojan.izpisi_barve();
		System.out.println("Povprecen cas izvajanja: " + povCasIzvajanja + "\n");
		
		///////////////////////////////////////
		
		System.out.println("Pozresno barvanje vozlisc:");
		zacetniCas = System.nanoTime();

		for (int i = 0; i < stPonovitev; i++)
			bojan.pobarvaj_pozresno();
		
		povCasIzvajanja = (double)(System.nanoTime() - zacetniCas) / stPonovitev;
		
		bojan.izpisi_barve();
		System.out.println("Povprecen cas izvajanja: " + povCasIzvajanja + "\n");
		
		///////////////////////////////////////
		
		System.out.println("Izcrpno barvanje vozlisc:");
		zacetniCas = System.nanoTime();

		for (int i = 0; i < stPonovitev; i++)
			bojan.pobarvaj_izcrpno();
			
		povCasIzvajanja = (double)(System.nanoTime() - zacetniCas) / stPonovitev;
		
		bojan.izpisi_barve();
		System.out.println("Povprecen cas izvajanja: " + povCasIzvajanja + "\n");
	}

}
