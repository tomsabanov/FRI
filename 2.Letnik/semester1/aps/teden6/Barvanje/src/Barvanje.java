
//
// Razred resuje problem barvanja grafa, ki je podan v matricni obliki.
// Ob instanciranju razreda konstruktor prejme nxn matriko binarnih
// vrednosti, kjer je n stevilo vozlisc. Ce sta vozlisci i in j povezani,
// sta v matriki na mestih (i,j) ter (j,i) enici.
//

import java.util.*;

public class Barvanje 
{	
	// Hranili bomo graf in barve vozlisc.
	int[][] graf;
	int[] vozlisca;
	
	Barvanje(int[][] g)
	{
		graf = g;
		vozlisca = new int[graf.length];
	}
	
	public void izpisi_barve()
	{
		for (int i = 0; i < vozlisca.length; i++)
			System.out.print(vozlisca[i] + ", ");

		System.out.println();
	}
	
	public static int[][] generiraj_graf(int n, double p, Random rand)
	{
		int[][] g = new int[n][n];
		
		for (int i = 1; i < n; i++)
			for (int j = i + 1; j < n; j++)
			{
				if (rand.nextDouble() < p)
				{
					g[i][j] = 1;
					g[j][i] = 1;
				}
			}
		
		return g;
	}
	
	public static void izpisi_graf(int[][] graf)
	{
		for (int i = 0; i < graf.length; i++)
		{
			for (int j = 0; j < graf[i].length; j++)
				System.out.print(graf[i][j] + " ");
			
			System.out.println();
		}
	}
	
	public void pobarvaj()
	{
		
	}
	
	
	public static void main(String[] args) 
	{
		Random rand = new Random();
		
		/*
		int[][] graf = {
				{0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1},
				{0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0},
				{0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1},
				{1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0},
				{0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1},
				{1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0},
				{0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0},
				{1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0},
				{0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1},
				{0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0},
				{0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1},
				{1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0},
				{0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1},
				{1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0},
				{0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0},
				{1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0}};
		*/
		
		int[][] graf = generiraj_graf(20, 0.4, rand);
		izpisi_graf(graf);
		
		System.out.println();
		
		Barvanje bojan = new Barvanje(graf);
		int stPonovitev = 1000;
		
		System.out.println("Barvanje vozlisc:");
		long zacetniCas = System.nanoTime();

		for (int i = 0; i < stPonovitev; i++)
			bojan.pobarvaj();
		
		double povCasIzvajanja = (double)(System.nanoTime() - zacetniCas) / stPonovitev;
		
		bojan.izpisi_barve();
		System.out.println("Povprecen cas izvajanja: " + povCasIzvajanja + "\n");
	}

}
