
5. PUSH DOWN AUTOMATA PDA
-----------------------------------------------------------------------------------------------------------------------------------
    - je  koncni avtomat, ki ima kontrolo nad svojim vhodnim trakom in skladom
    - je nedeterministicna "device", in njegova deterministicna verzija "DPDA - deterministic push down automata" sprejme
      le podmnozico vseh CFL-jev.... ta podmnozica vklucuje vecino programskih jezikov

    - PDA ima vhodni trak, kontrolono enoto in sklad
            - sklad je zaporedje simbolov iz neke abecede,
              najbolj levi simbol zaporedja predstavlja vrh sklada

              vhodni trak ------> kontrolna enota <-------------> sklad[top, second to top,.......]

            - ta naprava je po definiciji nedeterministicna, saj ima v vsaki situaciji koncno stevilo moznosti za naslednjo
              potezo (eng. "next move")
                 |
                 |
                 |
                 |
                 V
            imamo dve vrsti poteze : regularne poteze in E-poteze (eng. "epsilon moves")

            1. Regularna poteza:
               -v regularni potezi preberemo oz. 'consumamo' vhodni simbol na traku.
                Glede na:
                    - stanje 'q' v kontrolni enoti
                    - vhodni simbol 'a'
                    - simbol na vrhu sklada ( oznacimo ga 'Z' )
                je mozno koncno stevilo moznosti.
                I-ta moznost vsebuje:
                    - naslednjo stanje 'p-i' za koncno kontrolo oziroma za kontrolno enoto
                    - ( mozno da prazno ) zaporedje simbolov 'y-i' simbolov, za zamenjavo vrhnjega simbola na skladu 'Z'

                Po nedeterministicni izbrani moznosit se okno na vhodnem traku premakne za en simbol

            2. E-poteza
               - v E-potezi vhodni simbol in prebran oz. 'consumed'
                 Glede na :
                    - stanje 'q' v koncni kontroli oziroma kontrolni enoti
                    - vrhnji simbol 'Z' na skladu
                in neodvisno od simbola na vhodnem traku, je mozno koncno stevilo moznosti.
                I-ta moznost vsebuje:
                    - nasleddnjo stanje 'p-i' za vhodno kontrolo oziroma kontrolno enoto
                    - (mozno da prazno) zaporedje simbolov y-i, za zamenjavo vrhnjega simbola na skladu

                Po nedeterministicni izbiri moznosti se okno na vhodnem traku ne premakne.
                (E-poteze omogocajo PDA-ju da manipulira sklad brez branja vhodnih simbolov)

    - definiramo lahku tudi jezik, ki ga sprejme PDA, definiramo ga lahko na 2 nacina, in sicer kot mnozica vseh vhodov za katere
      velja:
        1. nekatera zaporedja vhodov povzrocijo da PDA izprazni sklad -> to je jezik, ki ga sprejme prazen sklad

            ali
        2. nekatera zaporedja potez povzrocijo da PDA vstopi v koncno stanje -> to je jezik, ki ga sprejme koncno stanje

        |
        |
        V

        Te dve definiciji sta pravzaprav ekvivalentni, v smuslu da
        L je sprejet by empty staeck v nekem PDA-ju, ce , in samo ce (eng. "iff - if and only if"),  je L sprejet
        by final state v nekaterem drugem PDA-ju

        Druga definicija je bolj pogosta, a z uporabo prve defiinicije je lazje dokazati osnoven teorem o PDA-jih, ki pravi da:
            PDA sprejme L, ce in samo ce je L CFL (Context free language)

    - Definicije:

      PDA je 7-tuple M = (Q, znak za vsoto, T, sigma, q0, Z0, F)
      kjer je :
        Q - koncna mnozica stanj
        znak za vsoto E - vhodna abeceda
        T - skladovna abeceda
        q0 je element Q in je zacetno stanje
        Z0 je element T in je zacetni simbol
        F je podmnozica Q in je mnozica koncnih stanj
        sigma - funkcija tranzicije (na sigmo lahko gledamo kot program PDA-ja, vsak PDA ma svojo specificno sigmo)
                kar je mapping Q x (E U {epsilon}) x T to finite subsets of Q x T*


- Intepretacije potez:
    - sigma(q,a,Z) = {(p1,y1), (p2,y2), .... (pm,ym)}
     PDA v stanju q z vhodnim simbolom a in vrhnjim simbolom sklada Z lahko za katerikoli i, 1<=i<=m
     vstopi v stanje pi, zamenja simbol Z z zaporedjem simbolov yi in premakne okno za en simbol -> regular poteza, navadna poteza

    -sigma(q, epsilon, Z) = {(p1,y1), (p2,y2), ..... , (pm, ym)}
     PDA v stanju q neodvisno od vhodnega simbola z vrhnjim simbolom Z na skladu vstopi v stanje pi in zamenja
     Z z yi za katerikoli i, 1<=i<=m. V tem primeru se okno ne premakne. ----> epsilon-poteza
      |
      |
      |
      V
    najbolj levi simbol zaporedja simbolov yi je postavljen na vrh sklada, in najbolj desni simbol je najnizji ..

    a,b,c - vhodni simboli
    u,v,w - besede oz. zaporedja  vhodnih simbolov
    velike crke za simbole na skladu
    grske crke za besede na skladu oziroma zaporedja simbolov na skladu

- Opis konfiguracija PDA-ja v nekem trenutku -> instantaneous description (ID) je tripple (q,w,y) kjer je
  q stanje, w beseda oz. zaporedje vhodnih simbolov in y beseda oz. zaporedje skladovnih simbolov

    - ce je M ODA, lahko recemom da  ID (q,ax, ZBeta) lahko direktno postane ID (pi, x, yiBeta),
     kar zapiseno (q,ax,Zbeta) m|- (pi, x, yiBeta),  ce sigma(q,a,Z) vsebuje (pi,yi).
     'a' je lahko tukaj vhodni simbol ali pa epsilon

    - m|-* zapisemo za refleksivni in tranzitivno zaprtje m|/ in recemo da ID I lahko postane ID J,
      ce I m|-* J. Zapisemo lahko tudi I m|-k J ce I m|-* J v natacno k potezah

- Jeziki, ki jih sprejme PDA

    - za PDA M definiramo dva jezika:

        1. L(M) jezik, ki ga sprejme pda s koncnim stanjem
           L(M) = {w element E* | (q0,w,Z0) |-* (p,epsilon,y) za nek p element F in y element T*}

        2. N(M), jezik, ki ga sprejme PDA s praznim skladom
           N(M) = {w element E* | (q0, w, Z0) |-* (p, epsilon, epsilon) za nek p element Q }


        L(M) vsebuje besedo w, ce je po branju besede w M 'lahko' (nedeterminizem) v nekem koncnem stanju
        N(M) vsebuje besedo w, ce ima po branju besede w M 'lahko' (nederetrminizem) prazen sklad

        If acceptance is by empty stack final states are irrelevant; in this case, we usually let F = 0



- PDA M je deterministicen, ce sigma fulfills dva dodatna pogoja za vsak q element Q in Z element T:
    1.sigma(q, epsilon, Z) != ) -----> za vsak a element E : sigma(q, a,Z) = 0
    2. za vsak a element E U {epsilon} : |sigma(q,a,Z)| <= 1
    Pogoj 1 prepreci moznost izbire med epsilon-potezo in regularno-potezo
    Pogoj 2 prepreci moznost izbire v primeru epsilon-poteze  in moznost izbire v primeru regular-poteze


PDA je assumed nedeterministecn, unless we state otherwise ..in this case we denote it with DPDA






PDA in CFL-ji
------------------
- deterministicni FAji sprejmejo enak razred jezikov kot nedeterministicni FAji

- Ali deterministicni PDA sprejme enak razred jezikov kot nedeterministicni PDAji?
  - NE; ww^R je sprejet v nedeterministicnem PDAju, ni pa sprejet v nobenem DPDAju

- Ekvivalenca sprejetja s koncnim stanjem in s praznim skladom:
    -razred jezikov ki jih sprejmejo PDAji s koncnim stanjem = razred jezikov ki jih sprejemjo PDAji s praznim skladom ???
    - pokazati moramo, da ce je jezik L sprejet v nekem PDaju s koncnim stanjom, potem je L sprejet tudi v PDAju z praznim skladom
      ... in obratno

    - Theorem: ce L=L(M2) za nek PDA M2, potem L=N(M1) za nek PDA M1
        Proof idea: za nek arbitraren L=L(M2) zgradi PDA M1, ki simulira M2 ampak izbrise sklad ko M2 pride v koncno stanje,
                    torej imamo L=N(M1)
    - Theorem: ce L=N(M1) za nek PDA M1, potem L=L(M2) za nek PDA M2
        Proof idea: za nek arbitraren L = N(M1) zgradi PDA M2 ki simulira M1, ampak preide v koncno stanje,
                    ko M1 izbrise celoten sklad... imamo L=L(M2)

    - razred jezikov ki jih spremjemo PDAji s koncnim stanjem je enako kot razred jezikov ki jih sprejmejo PDAji s praznim skladom


- Ekvivalenca PDAjev in CFLjev
    - Ce je L CFL, potem je L sprejet v nekem PDAju -> ce je to res, potem lahjko PDA cprejme vec kot CFLje?
        -> da dokazemo da nemorejo, moramo pokazati da ce je L sprejet v PDA, potem je L CFL

    - Theorem : Ce je L CFL, potem obstaja PDA M, da velja L=N(M)
        Proof idea: L arbirtraten CFL. L lahko generira CFG G v Greibachcovi normalni obliki
                    Sestavi PDA M, ki simulira najbolj levo derivacijo Gja.
    - Theorem. Ce L=N(M) za nek PDA M, potem je L CFL
        -Proof Idea: M je arbitraren PDA. Sestavi FG G, tako da je najoblj leva derivacija Gja stringa x simulacija
                     PDA M, ce damo vhod x... L=L(G), CFG
    - razred jezikov ki jih sprejmejo PDAji je enak kot razred jezikov CFLjev

-Deterministicni vs netedetrministicni PDAji:
    -nedeterministicni PDAji sprejmejo samo CFLje... kaj pa deterministicni?
        Je razred jezikov ki jih sprejem DPDA enak kot razred CFLjev?
        Odg: Ne, obstaja CFL, ki ni sprejet v nobenem deterministicnem PDAju

    - Theorem {ww^R | w element (0+1)*} je sprejet v PDAju, ne pa v DPDAju

    - Deterministicni PDAji so manj mocni kot nedeterministicni PDAji






















