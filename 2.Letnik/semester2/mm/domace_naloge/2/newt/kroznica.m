function [r, dr, ddr] = kroznica(t)
r = [t^2; cos(t)];
dr = [2*t; -sin(t)];
ddr = [2; -cos(t)];
endfunction
