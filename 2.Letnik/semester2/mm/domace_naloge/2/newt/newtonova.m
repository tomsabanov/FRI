function [d,t,s] = newtonova(F,G,t0,s0)

	p = [t0; s0];
	[x,k] = newton(F,G,p,0.000000001,1000000);

%	[JM,M] = calc(F,G,p);


	t = x(1);
	s = x(2);
	d = norm(x,2);

	[f,df,ddf] = F(t)
	[g,dg,ddg] = G(s)
endfunction

function [JM,M] = calc(F,G,p)
	t = p(1);
	s = p(2);
	[f,df,ddf] = F(t);
	[g,dg,ddg] = G(s);

	M = [(f-g)'*df; (f-g)'*dg];
	JM = [df'*df + (f-g)'*ddf, -dg'*df;
	      df'*dg, - dg'*dg + (f-g)'*ddg];

	%JM = [df'*(2.*df - 2.*dg) + 2.*ddf'*(f-g), - dg'*(2.*df - 2.*dg) - 2.*ddg'*(f-g);
	%     df'*(df-dg) + ddf'*(f-g), - dg'*(df-dg) - ddg'*(f-g)];
	%M = [(f-g)'*(2.*df - 2.*dg); (f-g)'*(df+dg)];
endfunction


function [x, k] = newton(F,G, x0, tol, maxit)
%[x, k] = newton(F, JF, x0, tol, maxit) poisce priblizek 
%x za resitev enacbe F(x) = 0 z Newtonovo metodo.
%(k je stevilo korakov, ki jih metoda porabi.)
%x0... zacetni priblizek
%tol... zahtevana natancnost
%maxit... najvecje stevilo iteracij

for k = 1:maxit
	%Izvedemo en korak Newtonove metode...
	[JM,M] = calc(F,G,x0);
	x = x0 - JM\M;
	%... in testiramo, ce je metoda ze 'konvergirala'.
	if(norm(x - x0) < tol)
		break;
	end
	x0 = x;
end

%Izpisemo opozorilo v primeru, da zadnji priblizek ni znotraj tolerancnega obmocja.
if(k == maxit)
	disp("Warning: The method did not converge after maxit iterations.")
end

endfunction
