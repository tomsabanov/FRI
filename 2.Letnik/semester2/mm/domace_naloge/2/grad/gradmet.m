function [x, k] = gradmet(gradf, h, x0, tol, maxit, pic = 0, f = 0)
%[x, k] = gradmet(gradf, h, x0, tol, maxit) vrne lokalni minimum x
%funkcije f z gradientom gradf, ki ga poisce z metodo najhitrejsega 
%spusta. 
%gradf ... gradient funkcije f
%h ... korak
%x0 ... zacetni priblizek
%tol ... zahtevana natancnost
%maxit ... najvecje stevilo iteracij
%pic ... opcijski parameter, ce je != 0 izrisemo sliko nivojnic (le za funkcije 2 spremenljivk)
%f ... opcijski parameter (nujen, ce pic != 0), funkcija 2 spr., za katero izrisemo nivojnice

%ce pic != 0, narisemo nivojnice f
if(pic)
	ezcontour(f, [-pi, pi]);
	hold on;
end
%glavna zanka metode najhitrejsega spusta
for k = 1:maxit
	if(pic)
		%za izris korakov si zapomnimo zaporedne priblizke grad. metode
		xx(:, k) = x0;
	end
	grad0 = feval(gradf, x0);
	if(norm(grad0) < tol)
		break;
	end
	x = x0 - h*grad0;
	x0 = x;
end

if(pic)
	%narisemo se dejansko zaporedje priblizkov
	plot(xx(1, :), xx(2, :), "r");
	hold off;
end
