function [d,t,s] = gradientna(F,G,t0,s0)
    
    p0 = [t0;s0];
    [p,k] = gradmet(F,G,0.01, p0, 0.000001, 1000);
    
    d = norm(p)
    t = p(1)
    s = p(2)
end

function [gradH] = calculateGradient(F,G,p)
    t = p(1);
    s = p(2);
    [f,df,ddf] = F(t);
    [g,dg,ddg] = G(s);

    const = 2 .* (f - g)';
    gradH = [ const * df; -const * dg ];
end

function [x, k] = gradmet(F,G, h, x0, tol, maxit, pic = 0, f = 0)
%[x, k] = gradmet(gradf, h, x0, tol, maxit) vrne lokalni minimum x
%funkcije f z gradientom gradf, ki ga poisce z metodo najhitrejsega 
%spusta. 
%gradf ... gradient funkcije f
%h ... korak
%x0 ... zacetni priblizek
%tol ... zahtevana natancnost
%maxit ... najvecje stevilo iteracij
%pic ... opcijski parameter, ce je != 0 izrisemo sliko nivojnic (le za funkcije 2 spremenljivk)
%f ... opcijski parameter (nujen, ce pic != 0), funkcija 2 spr., za katero izrisemo nivojnice
%glavna zanka metode najhitrejsega spusta
for k = 1:maxit
	grad0 = calculateGradient(F,G,x0);
	if(norm(grad0) < tol)
		break;
	end
	x = x0 - h*grad0;
	x0 = x;
end

endfunction


