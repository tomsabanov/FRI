function [r, dr, ddr] = kroznica(t)
r = [sin(t); cos(t)];
dr = [cos(t); -sin(t)];
ddr = -r; % [-sin(t); -cos(t)]
endfunction
