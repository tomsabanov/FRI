% Funkcija naive(X,Y) izracuna kvadratno matriko Q reda k in k-dimenzionalni stolpicni vektor,
% ki najbolje ustrezata sistemu enacb Q*xi + b = yi za mnozico x in y tock
% Matrika Q predstavlja zasuk telesa, stolpicni vektor b pa vzporedni premik.
%
% Vhodni podatki:
% X - matrika k-dimenzionalnih stolpicnih vektorjev xi
% Y - matrika k-dimenzionalnih stolpicnih vektorjev yi
%
% Izhodni podatki:
% Q - zasuk togega telesa, kvadratna matrika reda k
% b - vzporedni premik togega telesa, k-dimenzionalni stolpicni vektor

% Avtor: Tom Sabanov

function [Q,b] = naive(X,Y)

   [Q,b] = calculateNaive(X,Y); 

   % Q potrebujemo popraviti tako, da bo ortogonalna
   [Q,R] = qr(Q);

   % QR razcep ni unikaten, potrebno je le
   % popraviti predznake, da bodo ustrezal predznakom matriki Q pred razcepom
   I = eye(columns(Q));
   for i=1:columns(Q)
       if R(i,i) < 0
            I(i,i) = -1;
       end
   end
   Q = Q * I;
end

function [Q,b] = calculateNaive(X,Y)
% calculateNaive sestavi matriko X,Y ter Q'
% in izracuna Q' = X\ Y
% vrne pa Q in b, ki ju dobi iz Q' 
% Q' = M v kodi

    A = [];
    B = [];
    for i=1:columns(X)
        x = X(:,i);
        x = [x;1];
        A = [A; x'];
           
        y = Y(:,i);
        B = [B; y'];
    end
    M = A\B;

    % iz M moramo dobiti Q' in b
    cols = columns(M);
    rows = rows(M);

    Q = M(1:rows-1, :)';
    b = M(rows, :)';
end



%Test - trikotnik

%!test
%! X = [0 2 0 0; 0 0 2 0];
%! Y = [-2 -4 -2 -2; -5 -5 -7 -5];
%! [Q,b] = naive(X,Y);
%! ep = eps(100);
%! assert(Q,[-1 0; 0 -1], ep)
%! assert(b,[-2;-5], ep)


