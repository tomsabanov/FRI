% Funkcija kabsch(X,Y) izracuna kvadratno matriko Q reda k in k-dimenzionalni stolpicni vektor,
% ki najbolje ustrezata sistemu enacb Q*xi + b = yi za mnozico x in y tock z Kabschevim algoritmom
% Matrika Q predstavlja zasuk telesa, stolpicni vektor b pa vzporedni premik.
%
% Vhodni podatki:
% X - matrika k-dimenzionalnih stolpicnih vektorjev xi
% Y - matrika k-dimenzionalnih stolpicnih vektorjev yi
%
% Izhodni podatki:
% Q - zasuk togega telesa, kvadratna matrika reda k
% b - vzporedni premik togega telesa, k-dimenzionalni stolpicni vektor

% Avtor: Tom Sabanov

function [Q,b] = kabsch(X,Y)

    [x_avg,y_avg] = calcAvg(X,Y);
    [X,Y] = convert(X,Y,x_avg,y_avg);

    C = Y * X';
    [U,S,V] = svd(C);

    d = det(C);
    if d > 0
        d = 1;
    elseif d < 0
        d = -1;
    end
    
    D = eye(columns(S));
    D(rows(S), columns(S)) = d;

    Q = U*D*V';
    b = y_avg - Q*x_avg;

end

function [x,y] = calcAvg(X,Y)
% calcAvg izracuna povprecni x in y vektor
    x = X(:,1);
    y = Y(:,1);

    for i=2:columns(X)
        x = x + X(:,i);
        y = y + Y(:,i);
    end

    x = x./columns(X);
    y = y./columns(Y);

end

function [X,Y] = convert(X,Y,x_avg, y_avg)
% convert pretvori vhod X in Y kot x_i = x_i - x_avg,
% isto za y
    for i=1:columns(X)
        vec_x = X(:,i);
        vec_x = vec_x - x_avg;
        X(:,i) = vec_x;

        vec_y = Y(:,i);
        vec_y = vec_y - y_avg;
        Y(:,i) = vec_y;
    end
end

%!test
%! X = [0 2 0 0; 0 0 2 0];
%! Y = [-2 -4 -2 -2; -5 -5 -7 -5];
%! [Q,b] = kabsch(X,Y);
%! ep = eps(100);
%! assert(Q,[-1 0; 0 -1], ep)
%! assert(b,[-2;-5], ep)
