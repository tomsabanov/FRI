function pT = projekcija(A,b,T)
%pT = projekcija(A,b,T) poisce pravokotno projekcijo tocke s krajevnim vektorjem T
% na afin podprostor dan z enacbo Ax=b.

%iz izpeljave na vajah
pT = T + pinv(A)*(b - A*T);


% testi

%!test
%! assert(projekcija([0 1], -1, [1;1]), [1;-1], eps);

%!test
%! assert(projekcija([1 0], -1, [1;1]), [-1;1], eps);

