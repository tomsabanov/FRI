function pl = ploscina(A)
% pl = ploscina( [A1, ..... Ak]) izracuna ploscino poligona
% ki je dan z zaporedjem krajevnih vektorjev tock A1....Ak, A1

    pl = 0;
    for i=2:(length(A)-1)
        % ze izracunani ploscini pristejemo ploscino naslednjega
        % predznaceno ploscino trikotnika A1AjAj+1
        pl += 0.5 * det([A(:,i) - A(:,1), A(:,i+1) - A(:,1)]);
    end
    pl = abs(pl);
end

