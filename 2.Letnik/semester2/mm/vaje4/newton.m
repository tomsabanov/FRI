function [X,k] = newton(F,JF, X0, tol, maxit)
% funkcija poisce resitev F(x) = 0 z Newtonovo iteracijo z zacetnim 
% priblizkom x0, tol je toleranca, maxit je najvecjo dovoljeno stevilo 
% operacij.

Y = F(X0);
for k = 1:maxit
    X = X0 - JF(X0)\F(X0);
    %if(norm(X-X0) < tol)
    Y = F(X);
    if(norm(F(Y) < tol)); 
        %boljsi izstopni pogoj, ker je razlika dveh zaporednih priblizkov zares majhna,
        % ceprav je dejanska vrednost se dalec od 0, zato gledao vrednost tega priblizka v funkciji
        
            break;
    end
    X0 = X;
end
