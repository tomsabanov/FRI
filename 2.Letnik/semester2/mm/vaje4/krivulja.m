function K = krivulja(f, gradf, x0, delta, n)
% funkcija vrne nabor n tock v  2 x n
% matriki K, ki so na zaporednih razdaljah delta vzdolz implicitne krivulje f(x,y) = 0,
% gradf je gradient f.

g = gradf(x0(1),x0(2));
ng = [-g(2); g(1)];
F = @(X) [ng'*(X-x0); f(X(1), X(2))];
JF = @(X) [ng';  gradf(X(1), X(2))'];

K(:,1) = newton(F,JF, x0, 1e-10, 100);

for k = 2:n
    %n =  stevilo tock ki jih zelimo najti, n v zvezku je indeks
    F = @(X) [f(X(1),X(2));(X - K(:,k-1))' * (X - K(:,k-1)) - delta^2]
    JF = @(X) [gradf(X(1), X(2))';2*(X-K(:,k-1))']
    g = gradf(K(1, k-1), K(2, k-1));
    ng = [-g(2); g(1)];
    K(:,k) = newton(F,JF,K(:, k-1) + delta*ng/norm(ng), 1e-10, 100)
end

