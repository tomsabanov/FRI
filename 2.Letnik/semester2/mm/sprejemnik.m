function X = sprejemnik(P,d)
%(x,y) = sprejemnik([pi,qi], [di]) vrne polozaj sprejemnika [x,y], ki je na 
%oddaljenostih [di] od oddajnikov [pi,qi]


%pripravimo matriko sistema
A = 2* (P(2:end, :)) -2*( P(1:end-1, :));
b = d(1:end-1).^2 - d(2:end).^2 - P(1:end-1, 1).^2 + P(2:end,1).^2 - P(1:end-1,2).^2 + P(2:end, 2).^2;

%resimo sistem Ax=b z linearno metodo najmanjsih kvadratov
X = (A\b)';


% testi
%!test
% P=[1 0 ; 0 1; -1 0; 0 -1];
%d=[1;1;1;1]
%assert(sprejemnik(P,d), [0,0], eps)
