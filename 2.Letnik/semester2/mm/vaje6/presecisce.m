function P = presecisce(K,L)

    n = size(K,2);   
    m = size(L,2);

    P = [];
    for i=1:n-1
        for j=1:m-1
            A1 = K(:,i);
            A2 = K(:, i+1);
            B1 = L(:, j);
            B2 = L(:, j+1);
            P = [P presek_daljic(A1,A2,B1,B2)];
        end
    end
end
