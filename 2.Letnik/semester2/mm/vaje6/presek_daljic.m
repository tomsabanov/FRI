function P = presek_daljic(A1,A2,B1,B2)
    P = [];
    M = [A2 - A1, B1 - B2];
    d = [B1 - A1];
    x = M\d;
    t = x(1);
    s = x(2);
    if t<=1 && t>=0 &&  s<=1 && s>=0
        P = A1 + t*(A2-A1);
    end
end
