function [H,R] = naloga1(besedilo,p)
% besedilo - stolpicni vektor znakov (char)
% p  - stevilo poznanih predhodnih znakov; 0, 1, 2 ali 3.
%    p = 0 pomeni, da racunamo povprecno informacijo na znak
%        abecede brez poznanih predhodnih znakov: H(X1)
%    p = 1 pomeni, da racunamo povprecno informacijo na znak 
%        abecede pri enem poznanem predhodnemu znaku: H(X2|X1)
%    p = 2: H(X3|X1,X2)
%    p = 3: H(X4|X1,X2,X3)
%
% H - skalar; povprecna informacija na znak abecede 
%     z upostevanjem stevila poznanih predhodnih znakov p
% R - skalar; redundanca znaka abecede z upostevanjem 
%     stevila poznanih predhodnih znakov p
  
  besedilo=convertString(besedilo)
  if p == 0
      [H,R] = ZeroKnown(besedilo);
  elseif p == 1
      [H,R] = OneKnown(besedilo);
  elseif p == 2
      [H,R] = TwoKnown(besedilo);
  elseif p == 3
      [H,R] = ThreeKnown(besedilo);
  end

end

function [string] = convertString(str)
    M = isalnum(str);
    string = str(M);
    string = upper(string);
end

function [H,R] = ZeroKnown(str)
    uniq = unique(str);
    M = length(uniq);
    U = histc(str, uniq);
    P = U/sum(U);

    H = -sum(P.*log2(P));
    R = 1 - (H/log2(M));
end

function [H,R] = OneKnown(str)
 M = length(unique(str));
 pari = [str(1:end-1) str(2:end)];

 [U, I, J] = unique(pari, 'rows');
 F = histc(J, unique(J));
 P = F/sum(F);
 
 H0 = ZeroKnown(str);

 H = -P'*log2(P) - H0;
 R = 1 - (H/log2(M));
end

function [H,R] = TwoKnown(str)
 M = length(unique(str));
 pari = [str(1:end-2) str(2:end - 1) str(3:end)];

 [U, I, J] = unique(pari, 'rows');
 F = histc(J, unique(J));
 P = F/sum(F);
 
 H0 = ZeroKnown(str);
 H1 = OneKnown(str);

 H = -P'*log2(P) - H1 - H0;
 R = 1 - (H/log2(M));
end

function [H,R] = ThreeKnown(str)
 M = length(unique(str));
 pari = [str(1:end-3) str(2:end - 2) str(3:end-1) str(4:end)];

 [U, I, J] = unique(pari, 'rows');
 F = histc(J, unique(J));
 P = F/sum(F);
 
 H0 = ZeroKnown(str);
 H1 = OneKnown(str);
 H2 = TwoKnown(str);

 H = -P'*log2(P) - H2 - H1 - H0;
 R = 1 - (H/log2(M));
end
