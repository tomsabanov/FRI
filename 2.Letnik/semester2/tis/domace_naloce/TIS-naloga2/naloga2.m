function [izhod, R, kodBela, kodCrna] = naloga2(input)
  % Izvedemo kodiranje vhodne binarne slike (matrike) vhod
  % po modificiranem standardu ITU-T T.4.
  % Slika vsebuje poljubno stevilo vrstic in 1728 stolpcev.
  %
  % vhod     - matrika, ki predstavlja sliko
  % izhod    - binarni vrsticni vektor
  % R        - kompresijsko razmerje
  % kodBela  - matrika dolzin zaporedij belih slikovnih tock
  %		         in dolzin kodnih zamenjav
  % kodCrna  - matrika dolzin zaporedij crnih slikovnih tock
  %		         in dolzin kodnih zamenjav

  [W, B] = findLengths(input);
    
  CodeW = [];
  CodeB = [];
  CodeLengthW = [];
  CodeLengthB = [];

  if length(W) != 0
    [TreeW, aW, bW] = buildTree(W);
    [CodeLengthW,TreeW] = HuffmanCodeLengths(TreeW, aW, bW);
    [CodeW, newCode] = buildCode(CodeLengthW);

  end

  if length(B) != 0
    [TreeB, aB, bB] = buildTree(B);
    [CodeLengthB, TreeB] = HuffmanCodeLengths(TreeB, aB, bB);
    [CodeB, b] = buildCode(CodeLengthB);
  end

  [output] = encode(CodeW, CodeB, W, B);
    
  input_length = rows(input) * columns(input);
  output_length = columns(output);

  izhod = output;
  R = double(output_length)/double(input_length);
  kodBela = CodeLengthW;
  kodCrna = CodeLengthB;

end


function [W,B] = findLengths(input)
    % findLengths returns the row vectors of lengths of successive symbols
    % for white and black pixels

    W = [];
    B = [];

    for  i = 1:rows(input)
        % loop through each row of input
        [tempW, tempB] = extractLengths(input(i, :));
        W = horzcat(W, tempW);
        B = horzcat(B, tempB);
    end
end

function [W, B] = extractLengths(input)
    % extractLengths extracts white and black successive lengths
    % from a single row

    x = runlength(input);
    if input(1) == 0
        x = horzcat([0], x);
    end

    W = x(1:2:end);
    B = x(2:2:end);

end

function [Tree,length, i] = buildTree(X)
    % builds a basic tree with probabilities and values
    % unique represents the number of basic symbols
    % length represent the index of last combined symbol
    unique = unique(X);
    U = histc(X, unique);
    P = U/sum(U);
    length = length(unique);

    Tree = [unique ; P ]';
    Tree = [Tree ; zeros(length,columns(Tree))];
    Tree = [Tree , zeros(rows(Tree),3)];
    [Tree,i] = Huffman(Tree, length);
end

function [Tree,i] = Huffman(Tree, i)
    % builds a Huffman tree
    % i represents the index of last combined symbol
    sumProb = 0;
    while sumProb < 1
        x = Tree(1:i,2); % get all probabilitie
         
        [prob1, index1] = min(x);
        x(index1) = 1;
        [prob2, index2] = min(x);

        Tree(index1, 2) = 1;
        Tree(index2, 2) = 1;

        if(index1 == index2)
            break;
        end
        
        % now we calculate sumProb and insert it into Tree at index i in column 2
        i = i +1;
        sumProb = prob1 + prob2;
        Tree(i, 2) = sumProb;
        Tree(i,4) = index1;
        Tree(i,5) = index2;

    end
end

function [CodeLength, Tree] = HuffmanCodeLengths(Tree, a , b)
    % HuffmanCodeLengths returns a vector  of lengths of codes
    % we are going to traverse the tree between the rows of a and b
    % b > a
    if(a==b)
       Tree(a,3) = 1;
    end
    while b!=a
        index1 = Tree(b, 5);
        index2 = Tree(b, 4);
        length = Tree(b,3);

        Tree(index1,3) += length + 1;
        Tree(index2,3) += length + 1;
        b--;
    end

    CodeLength = Tree(1:a, [1,3]);
    CodeLength = sortrows(CodeLength, [2,1]);
    % first column represents the symbols, which are in our case 
    % the lengths of successive symbols
    % second column represents the length of the code lengths of 
    % the symbols of first column
    
end

function [Code, CodeLength] = buildCode(CodeLength)
    x = max(CodeLength(:,1)) + 1;
    y = 2;
    Code = zeros(x,y);
    I = 0;
    
    prevLength = 10000000;
    for  i = 1:rows(CodeLength)
        value = CodeLength(i,1) + 1;
        length = CodeLength(i,2);

        if length>prevLength
           power = 2 ^ (length - prevLength);

           I = I * power;
        end
        prevLength = length;
        Code(value, 1) = length;
        Code(value, 2) = I;

        CodeLength(i,2) = I;
        
        I++;
    end
end

function [Output] = encode(CodeW, CodeB, W, B)
    lengthW = length(W);
    lengthB = length(B);

    Output = [];
    lengthSum = 0;
    i = 1;
    j = 1;
    while i<=lengthW || j<=lengthB

        if i<=lengthW
            valW = W(i) +1;
            code_w = dec2bin(CodeW(valW,2), CodeW(valW,1)) - '0'; 
            i++;
            lengthSum+=valW -1;
        end

        if(lengthSum == 1728)
            lengthSum = 0;
            Output = horzcat(Output, code_w);
            continue;
        end

        if j<=lengthB
            valB = B(j) +1;
            code_b = dec2bin(CodeB(valB,2), CodeB(valB,1)) - '0'; 
            j++;
            lengthSum+=valB -1;
        end

        if(lengthSum == 1728)
            lengthSum = 0;
            Output = horzcat(Output, code_w, code_b);
            continue;
        end

        Output = horzcat(Output, code_w, code_b);

    end

end








