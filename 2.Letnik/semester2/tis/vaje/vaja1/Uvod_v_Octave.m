% Celotna dokumentacija za Octave: 
% http://www.gnu.org/software/octave/doc/interpreter/index.html

% Ukaze lahko vnašate neposredno v ukazno vrstico.
% Lahko napišete tudi skriptno datoteko s končnico ".m"
% Skriptno datoteko poženemo tako, da v ukazno vrstico vpišete ime datoteke
% brez končnice, npr.:
% vaja1
% ali pa jo odpremo v urejevalniku kode in pritisnemo F5.

% Enovrstični komentar
%{ 
Več-
vrstični
komentar 
%}

%---------------------------------
% Sistemski ukazi
%---------------------------------
pwd
dir
ls
cd ..

%---------------------------------
% Spremenljivke
%---------------------------------
%	- Podpičje na koncu pomeni, da se rezultat ukaza ne bo izpisal.
%	- Octave razlikuje med velikimi in malimi črkami.
%	- Pazite, da ne izbirate imen že obstoječih funkcij, npr. sum = 0;
%	- Za nize uporabljamo enojne ali dvojne narekovaje. 
%     Za združljivost z MATLAB uporabljaj enojne narekovaje.

% Prirejanje
t = true       % enako kot: t = logical(1)
f = false      % enako kot: t = logical(0)
a = 4
b = 5.3;       % rezultat prirejanja se ne bo izpisal
niz = "Octave" % niz znakov (string), lahko uporabljamo enojne ali dvojne narekovaje
im = 2 + 3i    % imaginarno število; 
               % realni del dobimo z real(im), imaginarni pa z imag(im)

%---------------------------------
% Aritmetične operacije
%---------------------------------
c = a + b
d = a / c
e = a^d    % pritisni AltGr+3 in predslednico

%---------------------------------
% Logični operatorji
%---------------------------------
a = 1;     % ali: a = true; ali a = logical(1);
b = 0;     % ali: b = false
~a         % ali: not(a)
a | b      % ali: or(a,b)
a & b      % ali: and(a,b)
xor(a,b)

%---------------------------------
% Pogoji
%---------------------------------
a = true;
b = false;
c = true;
a == b
a ~= b
a > b
a < b
a >= b
a <= b
a == b && b == c
a ~= b || b == c

%---------------------------------
% Vektorji, matrike
%---------------------------------
% Prirejanje, spreminjanje vsebine
A = [5, 6, 7] % vejice niso obvezne
B = [1, 2, 3; 3, 4, 5; 3, 6, 9] % podpičje ločuje vrstice med sabo
C = [1, 3, 5]'  % transpozicija vrstičnega vektorja je stolpični vektor
B(1,2)          % naslovimo element v matriki B, ki je v 1. vrstici in 2. stolpcu
B(2,:)          % cela druga vrstica
B(:,3)          % cel tretji stolpec
B(1:2,2:3)      % podmatrika
B(1,:) = A      % prva vrstica matrike B bo prepisana z vsebino vektorja A
A(1) = B(1,1)
A(2:3) = [4 5]
A(end) = 1      % rezervirana beseda end označuje indeks zadnjega elementa
C = []          % pobrišemo vsebino vektorja C; C postane prazen vektor
B(2,:) = []     % izbrišemo drugo vrstico matrike B

% Dimenzije
length(A)       % dolžina vektorja; pazi, če uporabljaš na matrikah!
size(B,1)       % število vrstic matrike
size(B,2)       % število stolpcev matrike
[vrstic, stolpcev] = size(B)  % oboje skupaj
numel(B)        % število elementov v B

% Inicializacija matrik
A1 = zeros(3)   % A1 postane matrika velikosti [3x3] s samimi ničlami; 
                % lahko napišemo tudi A1 = zeros(3,3)
A2 = zeros(1,3) % A2 postane vektor velikosti [1x3] s samimi ničlami
A3 = ones(2,4)  % A3 postane matrika velikosti [2x4] s samimi enicami
A4 = rand(5)    % A4 postane matrika velikosti [5x5] z naključnimi števili med 0 in 1

% Matematične operacije nad vektorji in matrikami
A = [5, 6, 7]
B = magic(3) % Zanimiva funkcija ;)
C = (1:2:5)' % Stolpec C vsebuje števila od 1 do 5 s korakom 2 (zaporedje 1,3,5).
A + 1        % Enako kot: A + [1 1 1].
A*2
A^2          % Napaka! Zakaj?
A.^2         % Kvadriramo vsak element vektorja A.
A * B        % [1x3] * [3x3] -> [1x3]
A * C        % [1x3] * [3x1] -> [1x1]  = skalarni produkt
C * A        % [3x1] * [1x3] -> [3x3]
C'.*A        % Vsak element vektorja C pomnožimo z istoležnim elementom vektorja A.
V = A >= 6   % Poiščimo vse elemente vektorja A, ki so večji ali enaki 6.
A(V)         % Naslovimo vse elemente, ki ustrezajo zgornjemu pogoju.
find(V)      % Dobimo indekse vseh elementov, ki ustrezajo zgornjemu pogoju.

%---------------------------------
% Stavek if
%---------------------------------
d = 0;
if (d>0)
    niz='pozitiven';
elseif (d<0)
    niz='negativen';
else
    niz='nicla';
end
niz

%---------------------------------
% Zanke for in while
%---------------------------------
vsota = 0;
for i=1:2:20
    vsota = vsota+i;
end

% Zanka while
vsota = 0;
i = 1;
while i < 20
	vsota = vsota+i;
	i = i+2;
end

%---------------------------------
% Funkcije 
% Kodo shrani v datoteko 'vsota.m', 
% potem kliči rezultat = vsota([1,2]).
%---------------------------------
function V = vsota(A)
    V = 0;
    for i = 1:length(A)
        V = V + A(i);
    end
end

%---------------------------------
% Čiščenje, ponastavitev
%---------------------------------
who            % izpiše seznam vseh spremenljivk, ki so trenuntno vidne
whos           % bolj informativen izpis
a = 3;
clear a;       % počisti spremenljivko a
a
sum = 0;       % tega sicer ne počnemo, ker je sum vgrajena funkcija!
sum([1, 2, 3]) % napaka, sum ni več funkcija, temveč skalar
clear sum;     % ponastavimo sum
sum([1, 2, 3]) % sedaj je sum zopet dobra stara funkcija
clear          % počisti vse spremenljivke v spominu
clear all      % počisti vse definirane spremenljivke in funkcije 
               % ter kontrolne točke za razhroščevanje
clc            % počisti ukazno okno
history -c     % počisti zgodovino ukazov

%---------------------------------
% Pomoč
%---------------------------------
help sum % prikaže pomoč v ukaznem oknu
doc sum  % prikaže pomoč v oknu za dokumentacijo

%---------------------------------
% Branje datotek
%---------------------------------
datoteka = fopen('test.txt','r');
vsebina = fread(datoteka,inf,'uchar');
vsebina
fclose(datoteka);

%---------------------------------
% Risanje
%---------------------------------
% Včasih se zgodi, da pri klicu funkcije plot(), Octave zmrzne.
% Pomaga, če nastavite drugo knjižnico za izris.
% V terminalsko okno Octava vpišite graphics_toolkit, da vidite, katera
% knjižnica je izbrana in jo potem zamenjajte z drugo, npr:
% graphics_toolkit("gnuplot")
% Seznam razpoložljivih knjižnic dobite s klicem available_graphics_toolkits.
x = 0:0.1:2*pi;
y = sin(x);
plot(x,y);

%---------------------------------
% Razhroščevanje
%---------------------------------
% Grafični vmesnik omogoča tudi razhroščevanje. 
% V urejevalniku kode označiš vrstice, kjer želiš, da se program ustavi.
% Bližnjice:
%     F5 - zagon skripte/programa
%     Shift+F5 - ustavi razhroščevanje
%     F9 - poženi označeno kodo v urejevalniku
%     F10 - "step" = en korak v razhroščevanju
%     F11 - "step in" = razhroščuj dalje v funkciji
%     Shift+F11 - "step out" = nadaljuj zunaj funkcije
