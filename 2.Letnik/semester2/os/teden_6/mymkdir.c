#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

void help(){
    printf("Program mymkdir ustvari direktorij.\n");
    printf("Uporaba: mymkdir <pot_direktorija>\n");
    printf("Z nastevanjem <pot_direktorija> lahko ustvarimo vec imenikov naenkrat");
}
int main(int argc, char** argv){
    if(argc < 2){
        help();
    }
    for(int i = 1; i<argc;i++){
        char* dirPath = argv[i];
        int status;
        status = mkdir(dirPath, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        if(status == -1){
            perror("Prislo je do napake!");
        }
    }
}
