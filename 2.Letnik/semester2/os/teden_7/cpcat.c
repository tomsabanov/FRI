#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/file.h>

int setDestination(char* source, int def, int mode){
	int fd;
	if(source[0]=='-' && sizeof(source)==8 ){
		fd=def;
	}
	else{
		fd=open(source, mode);
		if(fd < 0){
			perror("Prislo je do napake");
			exit(errno);
		}
	}
	return fd;
}
void closeFile(int fd){
	if(close(fd) == -1){
		perror("Prislo je do napake pri zapiranju datoteke");	
		exit(errno);
	}
}
void unlockFile(int fd){
	int fl = flock(fd, LOCK_UN);
	if(fl == -1){
		perror("Prislo je do napake pri odklepanju datoteke");
		exit(errno);
	}
}
void lockFile(int fd, int lock){
	int fl = flock(fd,lock);
	if(fl == -1){
		perror("Prislo je do napake pri zaklepanju datoteke");
		exit(errno);
	}
}

int main(int argc, char** argv){
	
	char* input = argv[1];
	char* output = argv[2];

	int fd_input=setDestination(input,0,O_RDONLY);
	int fd_output=setDestination(output,1,O_WRONLY | O_CREAT | O_APPEND);

	lockFile(fd_input, LOCK_SH);
	lockFile(fd_output, LOCK_EX);	
		
	char data[1];
	while(read(fd_input, data, 1) > 0){
		write(fd_output,&data,1);
	}
	
	unlockFile(fd_input);
	unlockFile(fd_output);

	closeFile(fd_input);
	closeFile(fd_output);
	return 1;
}
