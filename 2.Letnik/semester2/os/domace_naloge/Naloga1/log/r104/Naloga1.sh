#!/bin/bash
SCRIPT_NAME=$0
AKCIJA=$1


function pomoc {
 echo "da"
}

function status {
	a=$1
	b=$2
	gcd=1
	i=1
	while [ $i -le $a ]  && [ $i -le $b ]
	do
		m=$((a%i))
		n=$((b%i))
		if [ $m -eq 0 ] && [ $n -eq 0 ]; then
			gcd=$i
		fi

		i=$((i+1))
	done
	return $gcd
}
function leto {
	for year in "$@"
	do
		if [ $((year%4)) -eq 0 ] && [ $((year%100)) -ne 0 ] || [ $((year%400)) -eq 0 ];then
			echo "Leto $year je prestopno."
		else
			echo "Leto $year ni prestopno."
		fi
	done
}	
function calcFib {
	if [ "$1" -eq 0 ];then
		echo "$1: 0"
		return
	fi
	a=0
	b=1
	i=2
	orig=$1
	orig=$((orig+1))
	while [ "$i" != "$orig" ]
	do
		fn=$((a+b))
		a=$b
		b=$fn
		i=$((i+1))
	done
	echo "$1: $b"
}
function fib {
	for num in "$@"
	do
		calcFib $num
	done
}
function id {
	string="$1:"
	info=$(getent passwd $1)
	if [ -z $info ]; then
		string="$string err"
		echo $string
		return
	fi
	uid="$(cut -d':' -f3 <<< $info)"
	gid="$(cut -d':' -f4 <<< $info)"
	if [ $uid -eq $gid ]; then
		string="$string enaka "
	fi

	dir1="/home/$1"
	dir2="/home/uni/$1"
	if [ -d $dir1 ]; then
		string="$string obstaja"
	elif [ -d $dir2 ]; then
		string="$string obstaja"
	fi
	num=$(groups $1 | wc -w)
	num=$((num-2))
	string="$string $num"
	echo $string
}
function userinfo {
	for user in "$@"
	do
		id $user
	done
}
function tocke {
	RANDOM=42
	num=0
	total=0
	while read -r line;
       	do 	
		vpisna="$(cut -d' ' -f1 <<< $line)"
		if [ $vpisna = "#" ]; then
			continue
		fi
		num=$((num+1))
		a="$(cut -d' ' -f2 <<< $line)"
		b="$(cut -d' ' -f3 <<< $line)"
		c="$(cut -d' ' -f4 <<< $line)"
		tip="$(cut -d' ' -f5 <<< $line)"
		sum=$((a+b+c))

		pogoj1=0
		if [ "$tip" = 'p' ] || [ "$tip" = 'P' ]; then
			pogoj1=1
		fi

		pogoj2=0
		chars=${vpisna:2:2}
		if [ "$chars" = "14" ]; then
			pogoj2=1
		fi

		if [ $pogoj1 -eq 1 ] && [ $pogoj2 -eq 1 ]; then
			sum=$((sum/2))
		elif [ $pogoj1 -eq 1 ]; then
			sum=$((sum/2))
		elif [ $pogoj2 -eq 1 ]; then
			rand=$(( ($RANDOM % 5) + 1))
			sum=$((sum+rand))
		fi
		if [ $sum -gt 50 ]; then
			sum=50
		fi
		total=$((total + sum))
		echo "$vpisna: $sum"
	done 
	echo "St. studentov: $num"
	avg=$((total/num))
	echo "Povprecne tocke: $avg"
}
function printFile {
	local file="$1"
	local base="$file"
	local maxDepth=$2
	local currentDepth=$3
	local string=""
	for (( i=0; i<=$currentDepth; i++ )) do
		string="$string----"
	done
	out="$(ls -ld "$file")"
	type=${out:0:1}
	if [ $type = "-" ]; then
		echo "${string}FILE  $base"
	elif [ $type = "d" ]; then
		local d=$((currentDepth+1))
		echo "${string}DIR   $base"
		if [ $d -eq $maxDepth ]; then
			return
		else
			cd "$file"
			drevo "$file" $maxDepth $d
		fi
	elif [ $type = "c" ]; then
		echo "${string}CHAR  $base"
	elif [ $type = "b" ]; then
		echo "${string}BLOCK $base"
	elif [ $type = "s" ]; then
		echo "${string}SOCK  $base"
	elif [ $type = "p" ]; then
		echo "${string}PIPE  $base"
	elif [ $type = "l" ]; then
		echo "${string}LINK  $base"
	fi
}

function drevo {
	local dir="${PWD}"
	local path=$(echo $1)
	dir="${dir}/$path/*"
	local maxDepth=$2
	local currentDepth=$3
	for f in *; do
		if [ -e "$f" ] || [ -L "$f" ]; then
			printFile "$f" $maxDepth $currentDepth
		fi
	done
	cd ../
}

prostor_size=0
prostor_bloki=0
prostor_prostor=0
function prostor {
	local dir="${PWD}"
	local path=$(echo $1)
	dir="${dir}/$path/*"
	local maxDepth=$2
	local currentDepth=$3
	for f in *; do
		if [ -e "$f" ] || [ -L "$f" ]; then
			printProstor "$f" $maxDepth $currentDepth
		fi
	done
	cd ../
}
function printProstor {
	local file="$1"
	local base="$file"
	local maxDepth=$2
	local currentDepth=$3
	local string=""
	out="$(ls -ld "$file")"
	type=${out:0:1}

	local blocks=0
	local block_size=0
	local size=0
	if [ $type = "d" ]; then
		blocks="$(stat -c "%b" "$file")"
		block_size="$(stat -c "%B" "$file")"
		size="$(stat -c "%s" "$file")"
		local d=$((currentDepth+1))
		if [ $d -eq $maxDepth ]; then
			return
		else
			cd "$file"
			prostor "$file" $maxDepth $d
		fi
	else
		blocks="$(stat -c "%b" $file)"
		block_size="$(stat -c "%B" $file)"
		size="$(stat -c "%s" $file)"
	fi

	prostor_bloki=$((prostor_bloki + blocks))
	prostor_size=$((prostor_size + size))
	prostor_prostor=$((block_size * prostor_bloki))

}
if [ "$AKCIJA" = "pomoc" ];then
    pomoc
elif [ "$AKCIJA" = "status" ]; then
    status $2 $3
elif [ "$AKCIJA" = "leto" ]; then
    leto "${@:2}"
elif [ "$AKCIJA" = "fib" ]; then
	fib "${@:2}"
elif [ "$AKCIJA" = "userinfo" ]; then
	userinfo "${@:2}"
elif [ "$AKCIJA" = "tocke" ]; then
	tocke
elif [ "$AKCIJA" = "drevo" ]; then
	num=$#
	num=$((num-2))
	file=""
	depth=3
	for param in "${@: 2:$num}"; do
		file="$file $param"
		i=$((i+1))
	done

	last="${@: -1}"
	regex='^[0-9]+$'
	if [[ $last =~ $regex ]]; then
		depth=$last
		file="$file"
	elif [ "$file" = "$last" ]; then 
		file=$file
	else
		file="$file $last"
	fi
	echo "DIR   $file"

	path=$(echo $file)
	cd "$path"
	drevo "$file" "$depth" 0
elif [ "$AKCIJA" = "prostor" ]; then
	num=$#
	num=$((num-2))
	file=""
	depth=3
	for param in "${@: 2:$num}"; do
		file="$file $param"
		i=$((i+1))
	done

	last="${@: -1}"
	regex='^[0-9]+$'
	if [[ $last =~ $regex ]]; then
		depth=$last
		file="$file"
	elif [ "$file" = "$last" ]; then 
		file=$file
	else
		file="$file $last"
	fi
	echo "DIR   $file"

	path=$(echo $file)
	cd "$path"
	prostor "$file" "$depth" 0

	size=$(du -s -B1 "$path")
	echo "Velikost: $size"
	echo "Blokov: $prostor_bloki"
	echo "Prostor: $prostor_prostor"
fi
