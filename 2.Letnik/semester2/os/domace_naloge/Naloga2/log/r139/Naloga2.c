#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <ctype.h>
#include <limits.h>

struct Process {
	char name[64];
	char pid[64];
	char ppid[64];
	char state;
	int threads;
	int files;
};

char* concatStrings(char* dest, char* src){
	int i = 0;
	while(*dest){
		dest++; i++;
	}
	int j = 0;
	while(*src){
		*dest = *src;
		src++;
		dest++;
		j++;
	}
	*dest = '\0';
	dest  = dest - (i+j);
	src = src - j;
}
int readVersions(char* dir){
	FILE* fp;
	
	char* filePath = (char*)malloc(sizeof(char) * 100);
	concatStrings(filePath,dir);
	concatStrings(filePath, "version");	

	fp = fopen(filePath, "r");
	if(fp == NULL){
		perror("Error while openning a file\n");
		exit(EXIT_FAILURE);
	}	

	char temp[100];
	char linux_version[100];
	char gcc_version[100];

	fscanf(fp, "%s %s %s %s %s %s %s", temp, temp, linux_version, temp, temp, temp, gcc_version);
	
	printf("Linux: %s\n", linux_version);
	printf("gcc: %s\n", gcc_version);

	fclose(fp);
	free(filePath);
	return 0;
}

int readSwap(char* dir){
	FILE *fp;

	char* filePath = (char*)malloc(sizeof(char) * 100);
	concatStrings(filePath,dir);
	concatStrings(filePath,"swaps");

	fp = fopen(filePath, "r");
	
	if(fp == NULL){
		perror("Error while oppening a file\n");
		exit(EXIT_FAILURE);
	}
	char temp[100];
	char partition[100];
	fscanf(fp, "%s %s %s %s %s", temp, temp, temp, temp, temp);
	fscanf(fp, "%s", partition);

	printf("Swap: %s\n", partition);

	fclose(fp);
	free(filePath);
	return 0;
}
int readModules(char* dir){
	FILE* fp;

	char* filePath = (char*)malloc(sizeof(char) * 100);
	concatStrings(filePath,dir);
	concatStrings(filePath, "modules");

	fp = fopen(filePath, "r");

	if(fp == NULL){
		perror("Error while opening a file\n");
		exit(EXIT_FAILURE);
	}
	
	char c;
	int count = 0;
    	for (c = getc(fp); c != EOF; c = getc(fp)){
	      if (c == '\n') count++;
	}

	printf("Modules: %d\n", count);

	fclose(fp);
	free(filePath);
	return 0;
}

int sys(char* dir){
	readVersions(dir);	
	readSwap(dir);
	readModules(dir);
}	

int isNumber (char* s, int len) {
	for(int i = 0; i<len; i++){
		if(s[i] == '\n' || s[i] == '\0' ) break;
		if(isdigit(s[i]) == 0) return 0;
	}

	return 1;
}

void getProcessStat(char* dir, char* pid, char* ppid, char* state){

	FILE* fd;
	char* stat = "/stat";

 	int i = strlen(dir) + strlen(pid) + strlen(stat);
	char* filePath = (char*)malloc(sizeof(char) * 100);
	
	concatStrings(filePath, dir);
	concatStrings(filePath, pid);
	concatStrings(filePath, stat);

	fd = fopen(filePath, "r");

	if(fd == NULL){
		perror("Error openning a file");
		exit(EXIT_FAILURE);
	}

	char temp[50];
	fscanf(fd,"%s",temp);
	fscanf(fd,"%s",temp);
	fscanf(fd, "%s",state);
	fscanf(fd, "%s",ppid);

	fclose(fd);
	free(filePath);
}
void getProcessName(char* dir,char* pid, char* name){
	FILE* fd;
	char* comm = "/comm";

 	int i = strlen(dir) + strlen(pid) + strlen(comm);
	char* filePath = (char*)malloc(sizeof(char) * 100);

	concatStrings(filePath, dir);
	concatStrings(filePath, pid);
	concatStrings(filePath, comm);

	fd = fopen(filePath, "r");

	if(fd == NULL){
		perror("Error openning a file");
		exit(EXIT_FAILURE);
	}
	
	fscanf(fd, "%s", name);
	free(filePath);
	fclose(fd);
}

void getNumberOfThreads(char* dir, char* pid, int* threads){
	FILE* fd;
	char* task = "/task";

 	int i = strlen(dir) + strlen(pid) + strlen(task);
	char* filePath = (char*)malloc(sizeof(char) * 100);

	concatStrings(filePath, dir);
	concatStrings(filePath, pid);
	concatStrings(filePath, task);

	DIR* dp = opendir(filePath);
	struct dirent* de; //directory entry
	
	if(dp == NULL){
		perror("Could not open directory");
		exit(EXIT_FAILURE);
	}
	
	int num = -2;
	while((de = readdir(dp)) != NULL){
		num++;
	}
	
	*threads = num;

	closedir(dp);
	free(filePath);
}

void getNumberOfFiles(char* dir, char* pid, int* files){

	FILE* fd;
	char* fdPath = "/fd";

 	int i = strlen(dir) + strlen(pid) + strlen(fdPath);
	char* filePath = (char*)malloc(sizeof(char) * 100);

	concatStrings(filePath, dir);
	concatStrings(filePath, pid);
	concatStrings(filePath, fdPath);

	DIR* dp = opendir(filePath);
	struct dirent* de; //directory entry
	
	if(dp == NULL){
		perror("Could not open directory");
		exit(EXIT_FAILURE);
	}
	
	int num = -2;
	while((de = readdir(dp)) != NULL){
		num++;
	}
	
	*files = num;

	closedir(dp);
	free(filePath);
}

int length = 0;
struct Process** getPS(char* dir){
	DIR* dp = opendir(dir);
	struct dirent* de; //directory entry

	struct Process** processes = (struct Process**)malloc(sizeof(struct Process *) * 1000);
	
	if(dp == NULL){
		perror("Could not open directory");
		exit(EXIT_FAILURE);
	}
	
	int len;
	while((de = readdir(dp)) != NULL){
		len = sizeof(de->d_name);
		if(isNumber(de->d_name, len) == 1){
			struct Process* ps = (struct Process*)malloc(sizeof(struct Process));
			strcpy(ps->pid, de->d_name); // PID of process
				
			getProcessName(dir,ps->pid, ps->name);	
			//getProcessStat(dir, ps->pid, ps->ppid, &ps->state); 
			//getNumberOfThreads(dir,ps->pid, &ps->threads);
			//getNumberOfFiles(dir,ps->pid, &ps->files);

			processes[length] = ps;
			length++;
		}
	}

	closedir(dp);
	return processes;
}


void printPS(struct Process* ps){
	printf("Process : %s %s %s %d %d \n", ps->name, ps->pid, ps->ppid, ps->threads, ps->files);
}

int pids(char* dir){
	
	struct Process** ps = getPS(dir);
	
	/*int min;
	for(int i = 0; i<length; i++){
		min = i;
		int minVal = atoi(ps[min]->pid);
		for(int j = i+1; j<length;j++){
			int val = atoi(ps[j]->pid);
			if(val < minVal){
				min = j;
				minVal = val;
			}
		}
		struct Process* temp = ps[i];
		ps[i] = ps[min];
		ps[min] = temp; 
	}

	for(int i = 0; i<length; i++){
		printf("%s\n", ps[i]->pid);
	}
	*/
	return 0;
}

int names(char* dir){
	struct Process** ps = getPS(dir);
	
	int min;
	for(int i = 0; i<length; i++){
		min = i;
		char* minVal = ps[min]->name;
		int V = atoi(ps[min]->pid);
		for(int j = i+1; j<length;j++){
			char* val = ps[j]->name;
			int v = atoi(ps[j]->pid);
			if(strcmp(val,minVal)<0){
				min = j;
				minVal = val;
			}
			else if(strcmp(val,minVal) == 0 && v>V){
				min = j;
				minVal = val;
			}
		}
		struct Process* temp = ps[i];
		ps[i] = ps[min];
		ps[min] = temp; 
	}

	for(int i = 0; i<length; i++){
		printf("%s %s\n", ps[i]->pid, ps[i]->name);
	}
	return 0;
}

int ps(char* dir){

	struct Process** ps = getPS(dir);
	
	int min;
	for(int i = 0; i<length; i++){
		min = i;
		int minVal = atoi(ps[min]->pid);
		for(int j = i+1; j<length;j++){
			int val = atoi(ps[j]->pid);
			if(val < minVal){
				min = j;
				minVal = val;
			}
		}
		struct Process* temp = ps[i];
		ps[i] = ps[min];
		ps[min] = temp; 
	}

	printf("%5s %5s %6s %s\n", "PID", "PPID", "STANJE", "IME");
	for(int i = 0; i<length; i++){
		printf("%5s %5s %6c %s\n", ps[i]->pid, ps[i]->ppid, ps[i]->state, ps[i]->name);
	}
	return 0;
}

int psext(char* dir){

	struct Process** ps = getPS(dir);
	
	int min;
	for(int i = 0; i<length; i++){
		min = i;
		int minVal = atoi(ps[min]->pid);
		for(int j = i+1; j<length;j++){
			int val = atoi(ps[j]->pid);
			if(val < minVal){
				min = j;
				minVal = val;
			}
		}
		struct Process* temp = ps[i];
		ps[i] = ps[min];
		ps[min] = temp; 
	}

	printf("%5s %5s %6s %6s %6s %s\n", "PID", "PPID", "STANJE","#NITI","#DAT.", "IME");
	for(int i = 0; i<length; i++){
		printf("%5s %5s %6c %6d %6d %6s\n", ps[i]->pid, ps[i]->ppid, ps[i]->state,ps[i]->threads,ps[i]->files, ps[i]->name);
	}
	return 0;
}


int main(int argc, char** argv){
	if(argc<2){
		return 0;
	}


	char* action = argv[1];

	char* const_sys= "sys";
	char* const_pids= "pids";
	char* const_names= "names";
	char* const_ps= "ps";
	char* const_psext= "psext";
	
	char* dir;
	if(argc>2){
		char* arg = argv[2];
		int arg_len = strlen(arg);
		int len = sizeof(arg) / sizeof(char);
		int i = 0;
		if(arg[len-1] != '/'){
			i = strlen(arg) + 2;
			dir = (char*)malloc(i * sizeof(char));
			concatStrings(dir,arg);
			concatStrings(dir, "/");
		}
	}
	else{
	   	dir = "/proc/";
	}
	

	if(strcmp(action,const_sys)==0){
		return sys(dir);
	}	
	if(strcmp(action,const_pids)==0){
		return pids(dir);
	}
	if(strcmp(action,const_names)==0){
		return names(dir);
	}
	if(strcmp(action,const_ps)==0){
		return ps(dir);
	}
	if(strcmp(action,const_psext)==0){
		return psext(dir);
	}
}
