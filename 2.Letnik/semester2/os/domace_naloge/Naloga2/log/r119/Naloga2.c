#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <ctype.h>
#include <limits.h>

char* concatStrings(char* dir, char* file){
	int length = strlen(dir) + strlen(file);

	char* string = (char*)malloc(sizeof(char) * length);
		
	strcat(string, dir);
	strcat(string,file);

	return string;
}

int readVersions(char* dir){
	FILE* fp;
	
	char* filePath = concatStrings(dir, "version");	

	fp = fopen(filePath, "r");
	if(fp == NULL){
		perror("Error while openning a file\n");
		exit(EXIT_FAILURE);
	}	

	char temp[100];
	char linux_version[100];
	char gcc_version[100];

	fscanf(fp, "%s %s %s %s %s %s %s", temp, temp, linux_version, temp, temp, temp, gcc_version);
	
	printf("Linux: %s\n", linux_version);
	printf("gcc: %s\n", gcc_version);

	fclose(fp);
	free(filePath);

	return 0;
}

int readSwap(char* dir){
	FILE *fp;
	char* filePath = concatStrings(dir,"swaps");

	fp = fopen(filePath, "r");
	
	if(fp == NULL){
		perror("Error while oppening a file\n");
		exit(EXIT_FAILURE);
	}
	char temp[100];
	char partition[100];
	fscanf(fp, "%s %s %s %s %s", temp, temp, temp, temp, temp);
	fscanf(fp, "%s", partition);

	printf("Swap: %s\n", partition);

	fclose(fp);
	free(filePath);

	return 0;
}

int readModules(char* dir){
	FILE* fp;
	char* filePath = concatStrings(dir, "modules");

	fp = fopen(filePath, "r");

	if(fp == NULL){
		perror("Error while opening a file\n");
		exit(EXIT_FAILURE);
	}
	
	char c;
	int count = 0;
    	for (c = getc(fp); c != EOF; c = getc(fp)){
	      if (c == '\n') count++;
	}

	printf("Modules: %d\n", count);

	fclose(fp);
	free(filePath);
	return 0;
}

int sys(char* dir){
	readVersions(dir);	
	readSwap(dir);
	readModules(dir);
}	

int isNumber (char* s, int len) {
	for(int i = 0; i<len; i++){
		if(s[i] == '\n' || s[i] == '\0' ) break;
		if(isdigit(s[i]) == 0) return 0;
	}

	return 1;
}

int pids(char* dir){
	DIR* dp = opendir(dir);

	struct dirent *de; // directory entry

	if(dp == NULL){
		perror("Could not open directory");
		exit(EXIT_FAILURE);
	}
	
	int pids[100000];
	int index = 0; //index of last entry in pids
	while((de = readdir(dp)) != NULL){
		int len = sizeof(de->d_name);
		if(isNumber(de->d_name, len) == 1){
			pids[index] = atoi(de->d_name);
			index++;
		}
	}
		
	int min;
	for(int i = 0; i<index; i++){
		min = i;
		for(int j = i+1; j<index+1;j++){
			if(pids[j] < pids[min]){
				min = j;
			}
		}
		int temp = pids[i];
		pids[i] = pids[min];
		pids[min] = temp; 
	}

	for(int i = 0; i<=index; i++){
		if(pids[i] != 0) printf("%d\n", pids[i]);
	}
	

	closedir(dp);
	return 0;
}


int main(int argc, char** argv){
	if(argc<2){
		return 0;
	}


	char* action = argv[1];

	char* const_sys= "sys";
	char* const_pids= "pids";
	
	char* dir;
	if(argc>2){
		dir = argv[2];
		int len = sizeof(dir) / sizeof(char);
		if(dir[len-1] != '/'){
			dir = concatStrings(dir, "/");
		}
	}
	else{
	   	dir = "/proc/";
	}
	

	if(strcmp(action,const_sys)==0){
		return sys(dir);
	}	
	if(strcmp(action,const_pids)==0){
		return pids(dir);
	}

}
