#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char* concatStrings(char* dir, char* file){
	int length = strlen(dir) + strlen(file);

	char* string = (char*)malloc(sizeof(char) * length);
		
	strcat(string, dir);
	strcat(string,file);

	return string;
}

int readVersions(char* dir){
	FILE* fp;
	
	char* filePath = concatStrings(dir, "version");	

	fp = fopen(filePath, "r");
	if(fp == NULL){
		perror("Error while openning a file\n");
		exit(EXIT_FAILURE);
	}	

	char temp[100];
	char linux_version[100];
	char gcc_version[100];

	fscanf(fp, "%s %s %s %s %s %s %s", temp, temp, linux_version, temp, temp, temp, gcc_version);
	
	printf("Linux: %s\n", linux_version);
	printf("gcc: %s\n", gcc_version);

	fclose(fp);
	free(filePath);

	return 0;
}

int readSwap(char* dir){
	FILE *fp;
	char* filePath = concatStrings(dir,"swaps");

	fp = fopen(filePath, "r");
	
	if(fp == NULL){
		perror("Error while oppening a file\n");
		exit(EXIT_FAILURE);
	}
	char temp[100];
	char partition[100];
	fscanf(fp, "%s %s %s %s %s", temp, temp, temp, temp, temp);
	fscanf(fp, "%s", partition);

	printf("Swap: %s\n", partition);

	fclose(fp);
	free(filePath);

	return 0;
}

int readModules(char* dir){
	FILE* fp;
	char* filePath = concatStrings(dir, "modules");

	fp = fopen(filePath, "r");

	if(fp == NULL){
		perror("Error while opening a file\n");
		exit(EXIT_FAILURE);
	}
	
	char c;
	int count = 0;
    	for (c = getc(fp); c != EOF; c = getc(fp)){
	      if (c == '\n') count++;
	}

	printf("Modules: %d\n", count);

	fclose(fp);
	free(filePath);
	return 0;
}

int sys(char* dir){
	readVersions(dir);	
	readSwap(dir);
	readModules(dir);
}	

int main(int argc, char** argv){
	if(argc<2){
		return 0;
	}


	char* action = argv[1];

	char* const_sys= "sys";
	
	char* dir;
	if(argc>2){
		dir = argv[2];
	}
	else{
	   	dir = "/proc/";
	}
	

	if(strcmp(action,const_sys)==0){
		return sys(dir);
	}	

}
