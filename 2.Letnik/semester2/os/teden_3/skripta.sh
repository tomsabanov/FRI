#!/bin/bash

write () {
    echo "$1)" >> $3
    echo "$2" >> $3
    echo "----------------------" >> $3
}


touch ukazi.sh
touch regex.sh

chmod +x ukazi.sh
chmod +x regex.sh


write "a" "sed -n 3,7p file.txt" "ukazi.sh"
write "b" "cat test.txt | uniq -u > test.txt" "ukazi.sh"
write "c" "cat /etc/passwd | wc -l" "ukazi.sh"
write "d" "cat /etc/passwd | awk -F: '\$3'>99 {print \$1}" "ukazi.sh"


write "a" "egrep \\ba file" "regex.sh"
write "c" "egrep -w '\\w{3}' file"  "regex.sh"
write "d" "egrep '<[a,b]\\w{2}>' file" "regex.sh"
write "f" "egrep '. [a-z]' file" "regex.sh"


