Ukazi
-----------------------------------

a) Sestavite ukaz, ki izpiše vsebino neke datoteke od 3. do 7. vrstice.
sed -n 3,7p file

b) Sestavite ukaz, ki iz neke datoteke odstrani vse podvojene vrstice. (Urejenost vrstic izhodne datoteke je lahko drugačna od vhodne.)
 cat test.txt | uniq -u > test.txt

c) Sestavitev ukaz, ki izpiše število uporabniških računov sistema.
 cat /etc/passwd | wc -l


d) Izpišite vse uporabnike, katerih user ID je večji od 99.
 cat /etc/passwd | awk -F: '$3' > 99 {print $1}'

e) Izpišite uporabniške račune iz /etc/passwd, katerih prijavna lupina je /bin/false. Pri tem naj bodo različna polja med seboj ločena s tabulatorjem, vrstice pa oštevilčene.

cat /etc/passwd | grep /bin/false | tr ':' '\t' | nl


f) Izpišite prijavne lupine in število uporabnikov, ki določeno lupino uporablja. Pri tem naj bodo lupine urejene po frekvenci uporabe.

cat /etc/passwd | cut -d: -f7 | sort | uniq -c | sort -gr


g) Izpišite število, kolikokrat so bili posamezni uporabniki logirani v sistem. Namig: last.

last | head -n -2 |cut -d ' ' -f1 | sort | uniq -c


h) V eni izmed zgornjih nalog zajemite podatke sredi cevovoda in jih shranite v datoteko.

last | head -n -2 | cut -d ' ' -f1 | sort | uniq -c > datoteka.txt



Regularni izrazi
--------------------------------------------


a) Vrstica vsebuje niz, ki se začenja na a.

 egrep '\ba'


b) Vrstica vsebuje besedo, ki se začenja na a.

 egrep '\ba'

c) Vrstica vsebuje besedo dolžine tri.
 egrep -w '\w{3}'

d) Vrstica vsebuje besedo dolžine tri, ki se začne na a ali b.
egrep '<[a,b]\w{2}>' tekst.txt

d) Vrstica vsebuje besedo dolžine tri, ki vsebuje kvečjemu eno števko.
!!!!!!!!!!!!!!!!!!!!!!!

e) Vrstica vsebuje kvečjemu eno števko.

egrep -v '[0-9]{2,}' tekst.txt

f) Vrstica vsebuje stavek, ki se začne z malo začetnico.

egrep ". [a-z]" tekst.txt







