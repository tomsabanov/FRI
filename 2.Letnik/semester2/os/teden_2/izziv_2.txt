useradd -D

sudo useradd -m -c "Uporabnik 1"  -s /bin/bash uprabnik1
sudo useradd -m -c "Uporabnik 2"  -s /bin/bash uprabnik2


#c) Domači direktorij prvega uporabnika naj bo nedostopen vsem, domači direktorij drugega uporabnika pa naj bo dostopen vsem uporabnikom.
#
sudo chmod -R 000 /home/uporabnik1
sudo chmod -R 777 /home/uporabnik2

sudo passwd uporabnik1
sudo passwd uporabnik2

sudo groupadd grupa
usermod -a -G grupa uporabnik1
usermod -a -G grupa uporabnik2


sudo userdel -r uporabnik1
sudo userdel -r uporabnik2
