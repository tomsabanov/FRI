/*
 *Omogocit potrebno read dostop do datoteke vsem uporabnikom
 * */

#include <stdio.h>
#include <stdlib.h>

int main(){
 char *filename = "/etc/shadow";
    
 int c;
 FILE *file;
 file = fopen(filename, "r");
 if (file) {
         while ((c = getc(file)) != EOF)
                     putchar(c);
             fclose(file);

 }


 return 1;
}
