Povezave
-----------------------

a) Ustvarite eno ali več simboličnih povezav. S katerim ukazom izpišete cilj, na katerega kaže povezava? Kaj se zgodi s povezavo, če pobrišemo cilj povezave?

 readlink datoteka         -> pove nam kam kaze simbolicna povezava

 Ce pobrisemo cilj povezave, povezava ostane, le da sedaj kaze na neobstojeco datoteko.


b) Ustvarite več trdih povezav na neko datoteko. Kje v izpisu ukaza ls, se nahaja število trdih povezav na neko datoteko?

 ls -l -> poleg permissionov je stevilka, ki nam pove koliko povezav ima

 ls -l prikaze za simbolicne datoteke v obliki povezava -> cilj na koncu izpisa, ne pristevajo pa  se simbolicne povezave skupaj z trdimi povezavami (torej tista cifra je zares samo za stevilo trdih povezav)


c)Ustvarite datoteko s petimi trdimi povezavami.....

 touch bla.txt
 ln bla.txt hardlink1.txt
                .
                .
                .
 ....  ... hardlink5.txt

d) Rešite nalogo c) brez ukaza ln. Namig: Datoteki . in .. sta pravzaprav trdi povezavi.

 Okej, ko ustvarimo direktorij (ki je spet samo datoteka ane),
 in pogledamo koliko trdih povezav ima z ls -l, nam izpis kaze, da ima ta prazen direktorij 2 trdi povezavi. Najverjetneje sta te povezavi potem zares '.' in '..', torej trenutni direktorij in prejsni direktorij.
 To lahko preverimo z ukazom ls -a, ki nam izpise vse skrite datoteke.
 Torej da ustvarimo datoteko s petimi trdimi povezavami ustvarimo direktorij, in v njem ustvarimo se
 tri direktorije.


 Ampak ce ustvarimo datoteko znotraj tega direktorija, se stevilo trdih povezav direktorija ne spremeni.
 Zakaj? !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



Dovoljenja datotek
-------------------------------------
Ustvarite neko datoteko in preizkusite naslednje:

-spremenite ji lastnika
 chown root test.txt  -> nastavimo za  lastnika datoteke root

-spremenite ji skupino
 chown :root test.txt -> nastavimo skupino datoteke test.txt root

 ali

 chgrp root datoteka -> chgrp spremeni skupino datoteke na root

-spremenite ji lastnika in skupino hkrati z enim ukazom
chown root:root test.txt -> nastavimo lastnika in skupino kot root datoteke test.txt

-spremenite ji dovoljenja, da pri tem preizkusite simbolicno in numericno podajanje dovoljenj

 simbolicno : chmod [-R] [augo][+-=][rwx]

             zastavca -R je za rekurzivno dodajanje teh dovoljenj vseh poddatotek
             augo je all, user, group, others....
            primer uporabe:
            chmod a-r test.txt -> datoteki test.txt smo odvzeli read pravice uporabniku, grupam in drugim

            Numericno bi to bilo chmod 330 test.txt
            vsaka cifra je v osmiskem = rwx = 4 2 1 -> XXX predstavla dovoljenja za UGO (user, group, others)
            torej vsa dovoljenja izgledajo takole: chmod 777 test.txt -> dodelili rwx uporabnikom, grupam, drugim



Uporabniki in skupine
--------------------------------

a) Katere datoteke hranijo podatke o uporabnikih? Oglejte si jih! Katere podatke lahko pridobite o nekem uporabniku?
 /etc/shadow -> gesla uporabnikov & sol
 /etc/passwd -> detajli o uporabniskih racunih


b) Kateri uporabniki imajo privzeto prijavno lupino bash?

 root, moje uporabniski racun (postgres ima tudi)

 to vidim z cat /etc/passwd | grep /bin/bash


c) Kako lahko spremenimo geslo? (Če spremenite geslo skupnega uporabniškega računa, prosim, nastavite geslo na prvotno vrednost.)
 passwd novogeslo

d) Kje se nahaja vaš domači imenik?
  v direktoriju /home -> /home/uporabnik

e) To nalogo izvedite na virtualki, ki ste jo dobili v okviru vaj:

    Poiščite vse uporabnike, katerih prijavna lupina je bash.
    Za vsakega od njih ugotovite primarno skupino in sekundarne skupine.





