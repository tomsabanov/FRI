#!/bin/bash

function fakrek {
    if [[ $1 -le 1 ]]
    then
        echo 1
    else
        fak=$(fakrek $[$1 -1])
        echo $(($1 * fak))
    fi
}

function fakiter {
    fak=1
    for(( i= 1; i <= $1; i++ ))
    do
        fak=$(($fak * $i))
    done
    echo $fak
}

function fibiter {
    first=0
    second=1
    for(( i=1; i<$1; i++ ))
    do
        fn=$((first + second))
        first=$second
        second=$fn
    done
    if [ $1 -eq 0 ];then
        echo 0
        return 1
    elif [ $1 -eq 1 ]; then
        echo 1
        return 1
    fi

    echo $second
}

function fibrek {
    if [ $1 -le 0 ]
    then
        echo 0
    elif [ $1 -eq 1 ]
    then
        echo 1
    else
        fib1=$(fibrek $[$1-2])
        fib2=$(fibrek $[$1 -1])
        echo $((fib1 + fib2))
    fi
}

function help {
    echo "Funkciji fakrek in fakiter izracunata faktorijel podanega stevila. Uporaba: fakrek n ali fakiter n"
    echo "Funkciji fibrek in fibiter izracunata n-to fibonacijevo stevilo. Uporaba: fibrek n ali fibiter n"
    echo "Ce parameter n ni podan funkcijam, se za privzeto vrednost privzame 10"
}


akcija=$1
parameter=$2

if [ -z "$parameter" ]
then
    parameter=10
fi

$akcija $parameter


