#!/bin/bash

function help {
	echo "Uporaba programa:"
	echo "q - izhod iz programa"
	echo "h - izpis pomoci"
	echo "c - izpis ukaza (toggle)"
	echo "m - vidnost porabe pomnilnika (toggle)"
	echo "p - vidnost zasedenosti cpu (toggle)"
	echo "u - vidnost uporabnika (toggle)"
}
TOGGLE_C=1
TOGGLE_M=1
TOGGLE_P=1
TOGGLE_U=1

function printPS {
	comm="ps -Ao pid,comm"
	
	if [ $TOGGLE_M -eq 1 ]; then
		comm="${comm},%mem"
	fi

	if [ $TOGGLE_P -eq 1 ]; then
		comm="${comm},%cpu"
	fi

	if [ $TOGGLE_U -eq 1 ]; then
		comm="${comm},user"
	fi

	comm="$comm --sort=-pcpu | head -n 10"
	
	if [ $TOGGLE_C -eq 1 ]; then
		echo $comm
	fi	

	eval $comm
}

function main {
	
	while :
	do
		read -t 1 ukaz
		if [ "$ukaz" == "q" ]; then
			exit 0
		elif [ "$ukaz" == "h" ]; then
			help
		elif [ "$ukaz" == "c" ]; then
			TOGGLE_C=$((TOGGLE_C ^= 1))
		elif [ "$ukaz" == "m" ]; then
			TOGGLE_M=$((TOGGLE_M ^= 1))
		elif [ "$ukaz" == "p" ]; then
			TOGGLE_P=$((TOGGLE_P ^= 1))
		elif [ "$ukaz" == "u" ]; then
			TOGGLE_U=$((TOGGLE_U ^= 1))
		fi

		printPS
	done

}



main



