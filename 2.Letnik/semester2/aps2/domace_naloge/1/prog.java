import java.util.*;
class prog{

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String type = sc.next();
        String algorithm = sc.next();
        String direction = sc.next();
        int N = sc.nextInt();
        int[] table = new int[N];

        for(int i = 0; i<N;i++){
            table[i] = sc.nextInt();
        }

        prog naloga = new prog(table, type,algorithm,  direction);
        switch(algorithm){
            case "bs": naloga.bs();break;
            case "ss": naloga.ss();break;
            case "is": naloga.is();break;        
            case "hs": naloga.hs();break;
            case "qs": naloga.qs();break;
            case "ms": naloga.ms();break;
            case "cs": naloga.cs();break;
            case "rs": naloga.rs();break;
        }
    }
    
    int[] table;
    int n;
    Type type;
    Direction direction;
    String algo;
    int count_compares = 0;
    int count_rigs = 0;
    enum Direction {
        UP,
        DOWN
    }
    enum Type {
        TRACE,
        COUNT
    }
    public prog(int[] table, String op, String alg, String direction){
        this.table = table;
        this.n = table.length;
        this.algo = alg;

        if(direction.equals("up")){this.direction = Direction.UP;}
        else{this.direction = Direction.DOWN;}
        if(op.equals("trace")){this.type = Type.TRACE;}
        else{this.type = Type.COUNT;}
    }

    public void printTable(int ordered){
        for(int i = 0; i<table.length; i++){
            if(i == ordered){
                System.out.print("| ");
            }
            System.out.print(table[i] + " ");
        }

        if(algo.equals("is") && ordered == n) System.out.print("|");
        System.out.println();
    }
    public void print(int n){
        if(type==Type.TRACE) printTable(n-1);
        if(type == Type.COUNT){
            System.out.printf("%d %d\n",count_compares,count_rigs);
        }
    }
    public void swapElements(int i, int j){
        int temp = table[i];
        table[i] = table[j];
        table[j] = temp;
        count_rigs = count_rigs + 3;
    }
    public int comparison(int i, int j){
        count_compares++;
        if(direction == Direction.UP && table[i] < table[j]){
            if(algo.equals("bs"))swapElements(i,j);
            return i;
        }
        else if(direction == Direction.DOWN && table[i] > table[j]){
           if(algo.equals("bs"))swapElements(i,j);
           return i;
        }
        return -1;
    }
    public void switchDirection(){
        if(direction == Direction.UP){
            direction = Direction.DOWN;
        }
        else{
            direction = Direction.UP;
        }
    }
    public void resetCounts(){
        count_compares = 0;
        count_rigs = 0;
    }
    public void bs(){
        if(type == Type.TRACE){
            bubbleSort();
        }
        else{
            for(int i=0;i<3;i++){
                if(i==2) switchDirection();
                bubbleSort();
                resetCounts();
            }
        }
    }
    public void bubbleSort(){
        for(int i = 0; i < n-1; i++){
            if(type==Type.TRACE) printTable(i);
            for(int j = n-1; j>i; j--){
                comparison(j, j-1);
            }
        }
        print(n);
    }
    public void ss(){
        if(type == Type.TRACE){
            selectionSort();
        }
        else{
            for(int i=0;i<3;i++){
                if(i==2) switchDirection();
                selectionSort();
                resetCounts();
            }
        }
    }
    public void selectionSort(){
        for(int i = 0; i< n-1; i++){
            int min = i;
            if(type==Type.TRACE) printTable(i);
            for(int j = i+1; j < n; j++){
                int c = comparison(j, min);
                if(c != -1) min = c;
            }
            swapElements(min, i);
        }
        print(n);
    }

    public void is(){
        if(type == Type.TRACE){
            insertionSort();
        }
        else{
            for(int i=0;i<3;i++){
                if(i==2) switchDirection();
                insertionSort();
                resetCounts();
            }
        }
    }
    public void insertionSort(){
        for(int i = 1; i < n; i++){
            if(type==Type.TRACE) printTable(i);
            int curr = table[i]; 
            int j = i - 1;
            while(j>=0){
                count_compares++;
                if(direction == Direction.UP && table[j] > curr){
                    table[j+1] = table[j]; count_rigs+=3;
                    j = j - 1;
                }
                else if(direction == Direction.DOWN && table[j] < curr){
                    table[j+1] = table[j]; count_rigs+=3;
                    j = j - 1;
                }
                else{
                    break;
                }
            }
            table[j + 1] = curr;
        }        
        print(n+1);
    }

    public void hs(){
        switchDirection();
        if(type == Type.TRACE){
            heapSort();
        }
        else{
            for(int i=0;i<3;i++){
                if(i==2) switchDirection();
                heapSort();
                resetCounts();
            }
        }
    }
    public void heapSort(){
       for(int i = (n/2) -1; i>=0; i--){
            pogrezni(i, n);
        }
        if(type == Type.TRACE) printHeap(n);
        buildHeap(n);
        if(type == Type.COUNT){
            count_rigs-=3;
            System.out.printf("%d %d\n",count_compares,count_rigs);
        }
    }
    public void pogrezni(int i , int dolzKopice){
        int max = i;
        int left = 2*i + 1;
        int right = 2*i + 2;

        if(left<dolzKopice && comparison(left, max) == left){
            max = left;
        }
        if(right<dolzKopice && comparison(right,max) == right){
            max = right;
        }

        if(max != i){
            swapElements(i,max);
            pogrezni(max, dolzKopice);
        }
    }
   public void buildHeap(int n){
        for(int i = n-1;i>=0;i--){
            swapElements(0,i);
            pogrezni(0, i);
            if(type == Type.TRACE) printHeap(i);
        }
    }
    public void printHeap(int n){
        int count = 0;
        int numOfEl = 1;
        for(int i = 0; i<n; i++){
            System.out.print(table[i] + " ");
            count++;
            if(count == numOfEl && i!=(n-1)){
                System.out.print("| ");
                count = 0;
                numOfEl = numOfEl * 2;
            }
        }
        System.out.println();
    }

    public void qs(){
        if(type == Type.TRACE){
            sortQS(0,n-1);
        }
        else{
            for(int i=0;i<3;i++){
                if(i==2) switchDirection();
                sortQS(0,n-1);
                System.out.printf("%d %d\n",count_compares,count_rigs);
                resetCounts();
            }
        }
    }
    public void printTableQS(int[] table, int start, int end, boolean part){
        for(int i = start; i<=end; i++){
            System.out.print(table[i] + " ");
        }
        if(part) System.out.print("| ");
    }
    public void sortQS(int i, int j){
        int pivot = table[(i+j)/2];
        int start = i;
        int end = j;
        count_rigs++;
        while(i<=j){
            while(direction == Direction.UP && table[i] < pivot){i++;count_compares++;}
            while(direction == Direction.UP && table[j] > pivot){j--;count_compares++;}
            while(direction == Direction.DOWN && table[i] > pivot){i++;count_compares++;}
            while(direction == Direction.DOWN && table[j] < pivot){j--;count_compares++;}
            count_compares+=2;
            if (i<=j){
                swapElements(i,j);
                i++; 
                j--;
            }
        }
        if(type == Type.TRACE){
            printTableQS(table,start,j, true);
            printTableQS(table,j+1, i-1, true);
            printTableQS(table,i,end,false);
            System.out.println();
        }
        if(start<j) sortQS(start,j);
        if(i<end) sortQS(i,end);
    }
    public void ms(){
        if(type == Type.TRACE){
            mergeSort(0, n-1);
        }
        else{
            for(int i=0;i<3;i++){
                if(i==2) switchDirection();
                mergeSort(0, n-1);
                if(type == Type.COUNT)System.out.printf("%d %d\n",count_compares, count_rigs+1);
                resetCounts();
            }
        }
    }
    public void mergeSort(int start, int end){
        if(start<end){
            int m = (start+end)/2;
            if(type == Type.TRACE){
                printTableQS(table,start, m , true);
                printTableQS(table,m+1,end,false);
                System.out.println();
            }
            mergeSort(start,m);
            mergeSort(m+1, end);
            merge(start, m, end);
        }
    }    
    public void merge(int start, int m, int end){
        count_rigs++;
        int length_a = m - start + 1;
        int length_b = end - m;
        int[] a = new int[length_a];
        int[] b = new int[length_b];
        for(int i = 0; i< length_a; i++){
            a[i] = table[start + i];
        }
        for(int j = 0; j < length_b; j++){
            b[j] = table[m + j + 1];
        }

        int i = 0;
        int j = 0;
        String order = "";
        int init = start;
        while(i<length_a && j<length_b ){
            count_compares++;
            if(compareValues(a[i],b[j], true)){
                table[init] = a[i];
                order+=a[i] + " ";
                i++;
            }
            else{
                table[init] = b[j];
                order+=b[j] + " ";
                j++;
            }
            count_rigs++;
            init++;
        }
        while(i<length_a){
            count_rigs++;
            table[init] = a[i];
            order += a[i] + " ";
            i++;init++;
        }
        while(j<length_b){
            count_rigs++;
            table[init] = b[j];
            order += b[j] + " ";
            j++;init++;
        }
        if(type == Type.TRACE){
       	 System.out.println(order);
        }
    }
    public boolean compareValues(int a, int b, boolean bool){
        if(direction == Direction.UP && a <= b && bool==true)return true;
        if(direction == Direction.UP && b<a  && bool == false)return true;
        if(direction == Direction.DOWN && b <= a && bool==true)return true;
        if(direction == Direction.DOWN && a<b && bool==false)return true;
        return false;
    }

    public void cs(){
        if(type == Type.TRACE){
            countingSort();
        }
        else{
            for(int i=0;i<3;i++){
                if(i==2) switchDirection();
                countingSort();
                if(type == Type.COUNT) 
                    System.out.printf("%d %d\n",count_rigs, count_compares);
                resetCounts();
            }
        }
    }
    public void countingSort(){
        int[] freq = new int[256];
        for(int i = 0; i<n;i++){
            freq[table[i]]++;
        }
        for(int i = 1; i<=255; i++){
            freq[i] += freq[i-1];
        }
        printTableQS(freq,0,255,false);
        System.out.println();
        int[] sorted = new int[n];
        for(int i = n-1; i>=0; i--){
            int orig = table[i];
            int position = freq[orig]-1;
            System.out.print(position + " ");
            sorted[position] = orig;
            freq[orig] = freq[orig] - 1;
        }
        System.out.println();

        for(int i = 0; i<n;i++){
            System.out.print(sorted[i] + " ");
        }
    }

    public void rs(){
       radixSort();
    }
    public void radixSort(){
        int[] masks = {255,65280,16711680,0xFF000000};
        for(int i = 1; i<5; i++){
            countSortR(masks[i-1],i);
        }
    }
    public void countSortR(int mask, int part){
        int count[] = new int[256]; 
        for(int i = 0; i < table.length; i++){
            count[(table[i]&mask)>>((part-1)*8)]++;
        }

        for(int i = 1; i < count.length; i++){
                count[i] += count[i-1];
        }
        for(int i = 0; i < count.length; i++){
            System.out.print(count[i] + " ");
        }
        System.out.println();

        int [] newTable = new int [table.length+1];
        int offset = (part-1)*8;
        if(direction == Direction.UP){
            for(int i = table.length-1; i >= 0; i--){
                int bitmap = table[i]&mask;
                int shift = bitmap >> offset;
                int position = count[shift];
                newTable[position]= table[i];
                System.out.print((position-1) + " ");
                count[shift]--;
            }
        }else{
            for(int i = 0; i < table.length; i++){
                int bitmap = table[i]&mask;
                int shift = bitmap >> offset;
                int position = count[shift];
                newTable[newTable.length-position]= table[i];
                System.out.print((newTable.length-position-1) + " ");
                count[shift]--;
            }
        }
        System.out.println();
        for(int i = 0; i < table.length; i++){
            table[i] = newTable[i+1];
            System.out.print(table[i] + " ");
        }
        System.out.println();
    }
}
