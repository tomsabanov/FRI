import java.util.*;

class prog{
	public static void main(String[] args){
		prog naloga = new prog();
		naloga.start();	
	}
	Scanner sc;
	String alg;
	public prog(){
		sc = new Scanner(System.in);
		alg = sc.next();

	}

	public void start(){
		switch(alg){
			case "os": os();break;
			case "bl": bl();break;
			case "dv": dv();break;
			case "st": st();break;
		}
	}
	
	public int[][] getMatrix(){
		int n = sc.nextInt(); //rows
		int m = sc.nextInt(); //columns

		int[][] mat = new int[n][m];
		for(int i = 0; i<n; i++){
			for(int j = 0; j<m; j++){
				mat[i][j] = sc.nextInt();
			}
		}
		return mat;
	}
	public void printMatrix(int[][] A){
		for(int i = 0; i<A.length; i++){
			for(int j = 0; j<A[0].length; j++){
				System.out.print(A[i][j] + " ");
			}
			System.out.println();
		}
	}
	public void os(){
		int[][] A = getMatrix();
		int[][] B = getMatrix();
		int[][] C = multMatrices(A,B);
		System.out.printf("DIMS: %dx%d\n",C.length, C[0].length);
		printMatrix(C);
	}
	public int[][] multMatrices(int[][] A, int[][] B){
		int[][] C = new int[A.length][B[0].length];
		for(int i = 0; i<A.length; i++){
			for(int j = 0; j<B[0].length; j++){
				for(int k = 0; k<A[0].length; k++){
					C[i][j] += A[i][k] * B[k][j];
				}
			}
		}
		return C;
	}
	public int getSumOfMatrix(int[][] A){
		int sum = 0;
		for(int i=0; i<A.length; i++){
			for(int j = 0; j<A[0].length; j++){
				sum+=A[i][j];
			}
		}
		return sum;
	}
	public void bl(){
		int dim = sc.nextInt();
		int[][] A = getMatrix();
		int[][] B = getMatrix();
	
		int n = A.length;
		int m = A[0].length;

		int p = B[0].length;
		int[][] C = new int[n][p];	
		
		int mat_size = dim;
		for(int i = 0; i < n ; i+=dim){
			for(int j = 0; j < p; j+= dim){

				for(int k = 0; k < m; k+=dim){
					
					int sumOfEl = 0;	
					for(int I = i; I<Math.min(i+dim,n);I++){
						for(int J=j; J<Math.min(j+dim,p); J++){
							int sum = 0;
							for(int K = k; K<Math.min(k+dim, m); K++){
								sum+= A[I][K] * B[K][J];
							}
							C[I][J] += sum;
							sumOfEl += sum;
						}
					}
					System.out.println(sumOfEl);
				}
			}
		}
		System.out.printf("DIMS: %dx%d\n",C.length, C[0].length);
		printMatrix(C);
	}

	public int[][] getSubMatrix(int[][] A, int dim, int row, int col){
		// returned submatrix will be of dimension 2
		// we start partitioning from row-columnd onward
		int[][] B = new int[dim][dim];
		
		int indexRow = 0;	
		for(int i = row; i<A.length; i++){
			if(indexRow == dim) break;
			int indexCol = 0;
			for(int j = col; j<A[0].length; j++){
				if(indexCol == dim)break;
				B[indexRow][indexCol] = A[i][j];
				indexCol++;
			}
			indexRow++;
		}
		return B;
	}
	
	public int[][] getExpandedMatrix(){
		int n = sc.nextInt();
		int m = sc.nextInt();

		int size = Math.max(n,m);
		boolean result = size > 0 && ((size & (size - 1)) == 0); // if size is of pow 2
		if(n == m && result == true){
			return getMatrix();
		}

		int pow = 1;
		while(pow < size) pow*=2;
		
		return getExpandedMatrix(n,m,pow);
	}

	public int[][] getExpandedMatrix(int n, int m, int size){
		int[][] A = new int[size][size];
		for(int i = 0; i<n; i++){
			for(int j = 0; j<m; j++){
				A[i][j] = sc.nextInt();
			}
		}
		return A;
	}
	public int[][] expandMatrix(int[][] A, int size){
		int[][] C = new int[size][size];
		for(int i = 0; i<A.length; i++){
			for(int j = 0; j<A.length; j++){
				C[i][j] = A[i][j];
			}
		}
		return C;
	}
	public void dv(){
		int n = sc.nextInt();
		int[][] A = getExpandedMatrix();
		int[][] B = getExpandedMatrix();
		
		if(A.length > B.length){
			B = expandMatrix(B,A.length);
		}
		else if(B.length > A.length){
			A = expandMatrix(A, B.length);
		}

		int[][] C = new int[A.length][A.length];	
		C = divideAndConquer(A,B, n);

		System.out.printf("DIMS: %dx%d\n",C.length, C[0].length);
		printMatrix(C);
	}

	public int[][] divideAndConquer(int[][] A, int[][] B, int n){
		if(A.length == n){
			return multMatrices(A,B);
		}
		
		int dim = A.length/2;
		int[][] A11 = getSubMatrix(A, dim, 0,0);
		int[][] A12 = getSubMatrix(A, dim, 0,dim);
		int[][] A21 = getSubMatrix(A, dim, dim,0);
		int[][] A22 = getSubMatrix(A, dim,dim,dim);

		int[][] B11 = getSubMatrix(B, dim, 0,0);
		int[][] B12 = getSubMatrix(B, dim, 0,dim);
		int[][] B21 = getSubMatrix(B, dim, dim,0);
		int[][] B22 = getSubMatrix(B, dim,dim,dim);

		int[][] A11_B11 = divideAndConquer(A11,B11,n);
		System.out.println(getSumOfMatrix(A11_B11));

		int[][] A12_B21 = divideAndConquer(A12,B21,n);
		System.out.println(getSumOfMatrix(A12_B21));

		int[][] A11_B12 = divideAndConquer(A11,B12,n);
		System.out.println(getSumOfMatrix(A11_B12));

		int[][] A12_B22 = divideAndConquer(A12,B22,n);
		System.out.println(getSumOfMatrix(A12_B22));

		int[][] A21_B11 = divideAndConquer(A21,B11,n);
		System.out.println(getSumOfMatrix(A21_B11));

		int[][] A22_B21 = divideAndConquer(A22,B21,n);
		System.out.println(getSumOfMatrix(A22_B21));

		int[][] A21_B12 = divideAndConquer(A21,B12,n);
		System.out.println(getSumOfMatrix(A21_B12));

		int[][] A22_B22 = divideAndConquer(A22,B22,n);
		System.out.println(getSumOfMatrix(A22_B22));
		

		int[][] sum_11 = sumMatrices(A11_B11, A12_B21);
		int[][] sum_12 = sumMatrices(A11_B12, A12_B22);
		int[][] sum_21 = sumMatrices(A21_B11, A22_B21);
		int[][] sum_22 = sumMatrices(A21_B12, A22_B22);

		int[][] C = new int[A.length][A.length];
		C = putSubMatrix(0,0,sum_11,C);
		C = putSubMatrix(0,dim,sum_12,C);
		C = putSubMatrix(dim, 0, sum_21,C);
		C = putSubMatrix(dim,dim,sum_22,C);

		return C;	
	}
	public int[][] sumMatrices(int[][] A, int[][] B){
		int[][] C = new int[A.length][A[0].length];
		for(int i = 0; i<A.length; i++){
			for(int j = 0; j<A[0].length; j++){
				C[i][j] = A[i][j] + B[i][j];	
			}
		}
		return C;
	}
	public int[][] subtractMatrices(int[][] A, int[][] B){
		// A - B
		int[][] C = new int[A.length][A[0].length];
		for(int i = 0; i<A.length; i++){
			for(int j = 0; j<A[0].length; j++){
				C[i][j] = A[i][j] - B[i][j];	
			}
		}
		return C;
	}
	public int[][] putSubMatrix(int startRow, int startCol, int[][] A, int[][] C){
		// write A into C
		for(int i = 0; i<A.length; i++){
			for(int j = 0; j<A[0].length; j++){
				C[startRow +i][startCol + j] = A[i][j];
			}
		}
		return C;
	}
	

	public void st(){
		int n = sc.nextInt();
		int[][] A = getExpandedMatrix();
		int[][] B = getExpandedMatrix();
		
		if(A.length > B.length){
			B = expandMatrix(B,A.length);
		}
		else if(B.length > A.length){
			A = expandMatrix(A, B.length);
		}

		int[][] C = new int[A.length][A.length];	
		C = strassen(A,B, n);
		System.out.printf("DIMS: %dx%d\n",C.length, C[0].length);
		printMatrix(C);
	}

	public int[][] strassen(int[][] A, int[][] B, int n){
		if(A.length == n){
			return multMatrices(A,B);
		}
		
		int dim = A.length/2;
		int[][] A11 = getSubMatrix(A, dim, 0,0);
		int[][] A12 = getSubMatrix(A, dim, 0,dim);
		int[][] A21 = getSubMatrix(A, dim, dim,0);
		int[][] A22 = getSubMatrix(A, dim,dim,dim);

		int[][] B11 = getSubMatrix(B, dim, 0,0);
		int[][] B12 = getSubMatrix(B, dim, 0,dim);
		int[][] B21 = getSubMatrix(B, dim, dim,0);
		int[][] B22 = getSubMatrix(B, dim,dim,dim);

		int[][] P1 = strassen(A11, subtractMatrices(B12,B22),n);
		System.out.println(getSumOfMatrix(P1));

		int[][] P2 = strassen(sumMatrices(A11,A12), B22,n);
		System.out.println(getSumOfMatrix(P2));

		int[][] P3 = strassen(sumMatrices(A21,A22),B11,n);
		System.out.println(getSumOfMatrix(P3));

		int[][] P4 = strassen(A22, subtractMatrices(B21,B11),n);
		System.out.println(getSumOfMatrix(P4));

		int[][] P5 = strassen(sumMatrices(A11,A22), sumMatrices(B11,B22),n);
		System.out.println(getSumOfMatrix(P5));

		int[][] P6 = strassen(subtractMatrices(A12,A22), sumMatrices(B21,B22),n);
		System.out.println(getSumOfMatrix(P6));

		int[][] P7 = strassen(subtractMatrices(A11,A21), sumMatrices(B11,B12),n);
		System.out.println(getSumOfMatrix(P7));


		int[][] sum_11 = sumMatrices(sumMatrices(P5,P6), subtractMatrices(P4,P2));
		int[][] sum_12 = sumMatrices(P1,P2);
		int[][] sum_21 = sumMatrices(P3,P4);
		int[][] sum_22 = sumMatrices(subtractMatrices(P1,P7),subtractMatrices(P5,P3));

		int[][] C = new int[A.length][A.length];
		C = putSubMatrix(0,0,sum_11,C);
		C = putSubMatrix(0,dim,sum_12,C);
		C = putSubMatrix(dim, 0, sum_21,C);
		C = putSubMatrix(dim,dim,sum_22,C);

		return C;	
	}

}
