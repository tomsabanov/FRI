import java.util.*;

class Number{
	int[] nums;
	int base;
	String str;
	public Number(String n, int base){
		this.str = n;
		this.base = base;
		int length = n.length();
		nums = new int[length];
		for(int i = 0; i<length; i++){
			nums[i] = toInt(str.charAt(i));
		}
	}
	public String toChar(int i){
	    return i > 9 && i < 27 ? String.valueOf((char)(i + 'a' - 10)) : i + "";
	}
	public int toInt(char c){
	  int res = (char) c;
	  if(res>=48 && res<=57)return res - '0';
	  res = res - 'a' + 10;
	  return res;
	}

	public Number add(Number b){
		Number a = this;
		int base = a.base;
		int length = Math.max(a.str.length(), b.str.length());

		Number a_tmp = expand(a, length);
		Number b_tmp = expand(b, length);

		String res = "";
		int carry = 0;
		for(int i = length-1; i>=0; i--){
			int p = a_tmp.nums[i] + carry;
			int l = b_tmp.nums[i];
			carry = 0;

			int sum = p + l;
			
			if(sum>=base){
				sum = sum - base;
				carry = 1;
			}
			String s = toChar(sum);
			res = s + "" + res;
		}
		if(carry == 1) res = "1" + res;
		return new Number(res, base);
	}
	public Number subtract(Number b){
		// a - b
		Number a = this;
		int base = a.base;
		int length = Math.max(a.str.length(), b.str.length());

		Number a_tmp = expand(a, length);
		Number b_tmp = expand(b, length);

		String res = "";
		int carry = 0;
		for(int i = length-1; i>=0; i--){
			int p = a_tmp.nums[i];
			int l = b_tmp.nums[i]+carry;
			carry = 0;

			int sum = p - l;

			if(sum<0){
				sum = (p+base) - l;
				carry = 1;
			}
			
			String s = toChar(sum);
			res = s + "" + res;
		}
		Number result = new Number(res,base);
		result = result.removeLeadingZeros();
		return result;
	}

	public Number expand(Number a,int length){
		if(a.str.length() == length) return a;
		String result = "";
		for(int i = 0; i<(length - a.str.length()); i++){
			result+="0";
		}
		result = result + a.str;
		return new Number(result, a.base);
	}
	
	boolean bool = false;
	public  Number mult(Number b){
		Number a = this;
		int base = a.base;
		int length = Math.max(a.str.length(), b.str.length());

		Number a_tmp = a;
		Number b_tmp = b;

		String[] products = new String[a.str.length()];
		for(int i = 0; i<a.str.length(); i++){
			int p = a_tmp.nums[i];

			String tmp = "";
			int carry = 0;
			for(int j = b.str.length()-1; j>=0; j--){
				int l = b_tmp.nums[j];

				int sum = p * l;
				sum = sum + carry;
				carry = sum/a.base;
				sum = sum % a.base;	

				String s = toChar(sum);
				tmp = s + "" + tmp;
			}
			if(carry != 0) {
				String carryStr = toChar(carry);
				tmp = carryStr + tmp;
			}
			if(tmp.charAt(0) == '0') tmp="0";
			products[i] = tmp;
			if(bool)System.out.println(tmp);
		}

		String zeros="";
		Number result = new Number("0",base);
		for(int i = products.length-1; i>=0; i--){
			String fin = products[i] + zeros;
			Number tmp = new Number(fin,base);
			result = result.add(tmp);
			zeros+="0";
		}
		return result;
	}

    public Number removeLeadingZeros(){
	int i = 0;
	String n = this.str;
	while(i < str.length()){
		if(str.charAt(i) == '0'){
			i++;
			n = str.substring(i);
			continue;
		}
		break;
	}
	return new Number(n,base);
    }

    public boolean isZero(){
	for(int i = 0; i<nums.length; i++){
		if(nums[i] != 0 ) return false;			
	}
	return true;
    }

    public Number expand_dv(int n){
	String temp = "";
	for(int i = 0; i<(n-this.str.length()); i++){
		temp+="0";
	}
	String s = temp+""+this.str;
	return new Number(s, base);
    }

    public Number expandRight(int n){
	String temp = "";
	for(int i = 0; i<n; i++){
		temp+="0";
	}
	String s = this.str + temp;
	return new Number(s,this.base);
    }

}

class prog{
    
    public static void main(String[] args){
        prog naloga = new prog();    
        naloga.start();

	/*
    	Number n = new Number("231",4);
	Number p = new Number("213",4);	
	n.bool=true;
	p.bool=true;

	Number c = n.subtract(p);
	System.out.println(c.str);
	*/
	
    }

    Scanner sc;
    String alg;
    String a;
    String b;
    int base;
    public prog(){
        sc = new Scanner(System.in);
        alg = sc.next();
        base = sc.nextInt();
        a = sc.next();
        b = sc.next();
    }

    public void start(){
	Number p1 = new Number(a, base);
	Number p2 = new Number(b, base);
        switch(alg){
            case "os": os();break;
            case "dv": System.out.println(dv(p1,p2).str);break;
            case "ka": System.out.println(ka(p1,p2).str);break;
        }
    }

    public void os(){
	 Number p = new Number(a,base);
	 Number d = new Number(b, base);
	
 	 p.bool = true;	 
	 d.bool = true;
	 Number num = d.mult(p);	

	 String lines = generateLines(num.str.length());
  	 System.out.println(lines);
	 System.out.println(num.str);
    }
    public String generateLines(int n){
        String str = "";
        for(int i =0; i<n; i++){
            str+="-";
        }
        return str;
    }
    public Number dv(Number a, Number b){

	a = a.removeLeadingZeros();
	b = b.removeLeadingZeros();
	if(a.str.equals("")) a.str="0";
	if(b.str.equals("")) b.str="0";
	System.out.println(a.str+ " " + b.str);
	if(a.isZero() == true  || b.isZero()==true==true ){
		return new Number("0", a.base);
	}
	int n = Math.max(a.str.length(),b.str.length());
	

	if(a.str.length() ==1 || b.str.length()==1){
		Number c = a.mult(b);
		return  c;
	}
	if(n%2==1)n++;
	if(a.str.length() != n || b.str.length() !=n){
		a = a.expand_dv(n);
		b = b.expand_dv(n);
	}
	Number a1 = new Number(a.str.substring(0,n/2),a.base);
	Number a0 = new Number(a.str.substring(n/2, n),a.base);

	Number b1 = new Number(b.str.substring(0,n/2),b.base);
	Number b0 = new Number(b.str.substring(n/2, n),b.base);

	Number a0_b0 = dv(a0,b0);	
	System.out.println(a0_b0.str);

	Number a0_b1 = dv(a0,b1);
	System.out.println(a0_b1.str);

	Number a1_b0 = dv(a1,b0);
	System.out.println(a1_b0.str);

	Number a1_b1 = dv(a1,b1);
	System.out.println(a1_b1.str);

	Number ab = new Number("0",a.base);
	
 	/*	
	System.out.println("IZPIS " + n);
	System.out.println("	a = "+ a.str + " |   a1 = " + a1.str + ",  a0 = " + a0.str);
	System.out.println("	b = "+ b.str + " |   b1 = " + b1.str + ",  b0 = " + b0.str);
	System.out.println("	a1*b1 = " + a1_b1.str);
	System.out.println("	a0*b1 = " + a0_b1.str);
	System.out.println("	a1*b0 = " + a1_b0.str);
	System.out.println("	a0*b0 = " + a0_b0.str);
	*/
	
	a1_b1 = a1_b1.expandRight(n);	

	Number middle = a0_b1.add(a1_b0);
	middle = middle.expandRight(n/2);

	ab = ab.add(a1_b1).add(middle).add(a0_b0);
	ab = ab.removeLeadingZeros();
		
	return ab;

    }

    public Number ka(Number a, Number b){

	a = a.removeLeadingZeros();
	b = b.removeLeadingZeros();
	if(a.str.equals("")) a.str="0";
	if(b.str.equals("")) b.str="0";
	System.out.println(a.str+ " " + b.str);
	if(a.isZero() == true  || b.isZero()==true ){
		return new Number("0", a.base);
	}
	int n = Math.max(a.str.length(),b.str.length());
	
	if(a.str.length() ==1 || b.str.length()==1){
		Number c = a.mult(b);
		return  c;
	}
	if(n%2==1)n++;
	if(a.str.length() != n || b.str.length() !=n){
		a = a.expand_dv(n);
		b = b.expand_dv(n);
	}
	Number a1 = new Number(a.str.substring(0,n/2),a.base);
	Number a0 = new Number(a.str.substring(n/2, n),a.base);

	Number b1 = new Number(b.str.substring(0,n/2),b.base);
	Number b0 = new Number(b.str.substring(n/2, n),b.base);

	Number a0_b0 = ka(a0,b0);	
	System.out.println(a0_b0.str);

	Number a1_b1 = ka(a1,b1);
	System.out.println(a1_b1.str);

	
	Number zmn1=a0.add(a1);
	Number zmn2=b0.add(b1);
	
	Number zmnozek=ka(zmn1, zmn2);
	System.out.println(zmnozek.str);

	zmnozek = zmnozek.subtract(a1_b1);
	zmnozek = zmnozek.subtract(a0_b0);

	a1_b1 = a1_b1.expandRight(n);
	zmnozek = zmnozek.expandRight(n/2);

	Number ab = new Number("0",a.base);
	ab = ab.add(a1_b1).add(zmnozek).add(a0_b0);

	/*
	System.out.println("IZPIS " + n);
	System.out.println("	a = "+ a.str + " |   a1 = " + a1.str + ",  a0 = " + a0.str);
	System.out.println("	b = "+ b.str + " |   b1 = " + b1.str + ",  b0 = " + b0.str);
	System.out.println("	a1*b1 = " + a1_b1.str);
	System.out.println("	a0*b0 = " + a0_b0.str);
	System.out.println("	zmnozek = " + zmnozek.str);
	System.out.println("	FINAL SUM " + ab.str);
	*/
	
        ab = ab.removeLeadingZeros();		
	return ab;

    }

}
