import java.util.*;
class izziv_2{
    public static void main(String[] args){
        //int n = 8;
        //int[] table = {0, 13, 0, 6, 15, 22, 7, 9};

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] table = new int[n];
        for(int i = 0; i< n; i++){
            table[i] = scanner.nextInt();
        }
        
        for(int i = (n/2) -1; i>=0; i--){
            pogrezni(table, i, n);
        }
        printHeap(table,n);
        buildHeap(table,n);
    }

    static void pogrezni(int a[], int i , int dolzKopice){
        int max = i;
        int left = 2*i + 1;
        int right = 2*i + 2;

        if(left<dolzKopice && a[left] > a[max]){
            max = left;
        }
        if(right<dolzKopice && a[right] > a[max]){
            max = right;
        }

        if(max != i){
            int temp = a[max];
            a[max] = a[i];
            a[i] = temp;
            pogrezni(a, max, dolzKopice);
        }

    }
    static void buildHeap(int a[], int n){
        for(int i = n-1;i>=0;i--){
            int temp = a[0];
            a[0] = a[i];
            a[i] = temp;
            pogrezni(a, 0, i);
            printHeap(a,i);
        }
    }
    static void printHeap(int a[], int n){
        int count = 0;
        int numOfEl = 1;
        for(int i = 0; i<n; i++){
            System.out.print(a[i] + " ");
            count++;
            if(count == numOfEl && i!=(n-1)){
                System.out.print("| ");
                count = 0;
                numOfEl = numOfEl * 2;
            }
        }
        System.out.println();
    }
}
