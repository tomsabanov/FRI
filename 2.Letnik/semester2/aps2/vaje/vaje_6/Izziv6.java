import java.util.*;
class Matrix {
	private int[][] m;

	public int n; //only square matrices

	public Matrix(int n){

		this.n = n;

		m = new int[n][n];

	}

    public int vsota(){
        int sum = 0;
        for(int i = 0; i<n;i++){
            for(int j = 0; j<n;j++){
                sum = sum + m[i][j];
            }
        }
        return sum;
    }


    //set value at i,j
	public void setV(int i, int j, int val){

		m[i][j] = val;

	}


    // get value at index i,j
	public int v(int i, int j){

		return m[i][j];

	}


    // return a square submatrix from this
	public Matrix getSubmatrix(int startRow, int startCol, int dim){

		Matrix subM = new Matrix(dim);

		for (int i = 0; i<dim ; i++ )

			for (int j=0;j<dim ; j++ )

				subM.setV(i,j, m[startRow+i][startCol+j]);



		return subM;

	}


    // write this matrix as a submatrix from b (useful for the result of multiplication)
	public void putSubmatrix(int startRow, int startCol, Matrix b){

		for (int i = 0; i<b.n ; i++ )

			for (int j=0;j<b.n ; j++ )

				setV(startRow+i,startCol+j, b.v(i,j));

	}


    // matrix addition
	public Matrix sum(Matrix b){

		Matrix c = new Matrix(n);

		for(int i = 0; i< n;i++){

			for(int j = 0; j<n;j++){

				c.setV(i, j, m[i][j]+b.v(i, j));

			}

		}

		return c;

	}





    // matrix subtraction
	public Matrix sub(Matrix b){

		Matrix c = new Matrix(n);

		for(int i = 0; i< n;i++){

			for(int j = 0; j<n;j++){

				c.setV(i, j, m[i][j]-b.v(i, j));

			}

		}

		return c;

	}



	//simple multiplication
	public Matrix mult(Matrix b){
        //TODO
        Matrix c = new Matrix(n);
        for(int i = 0; i < n; i++){
            for(int j = 0; j<n;j++){
                for(int k = 0; k<n; k++){
                    c.setV(i,j,c.v(i,j)+m[i][k]*b.v(k,j));
                }
            }
        }
        return c;
	}


    // Strassen multiplication
	public Matrix multStrassen(Matrix b, int cutoff){
        int l = this.n;
        Matrix r = new Matrix(l);
        if(l == cutoff){
            return this.mult(b);
        }
        Matrix a11 = this.getSubmatrix(0,0,l/2);
        Matrix a12 = this.getSubmatrix(0,l/2,l/2);
        Matrix a21 = this.getSubmatrix(l/2,0,l/2);
        Matrix a22 = this.getSubmatrix(l/2, l/2,l/2);

        Matrix b11 = b.getSubmatrix(0,0,l/2);
        Matrix b12 = b.getSubmatrix(0,l/2,l/2);
        Matrix b21 = b.getSubmatrix(l/2,0,l/2);
        Matrix b22 = b.getSubmatrix(l/2,l/2,l/2);

        Matrix temp = b11.sum(b22);
        Matrix m1 = (a11.sum(a22)).multStrassen(temp,cutoff);

        Matrix m2 = (a21.sum(a22)).multStrassen(b11,cutoff);

        temp = b12.sub(b22);
        Matrix m3 = a11.multStrassen(temp, cutoff);

        temp = b21.sub(b11);
        Matrix m4 = a22.multStrassen(temp, cutoff);

        Matrix m5 = (a11.sum(a12)).multStrassen(b22,cutoff);

        temp = b11.sum(b12);
        Matrix  m6 = (a21.sub(a11)).multStrassen(temp,cutoff);

        temp = b21.sum(b22);
        Matrix m7 = (a12.sub(a22)).multStrassen(temp,cutoff);

        System.out.printf("m1: %d\n",m1.vsota());
        System.out.printf("m2: %d\n",m2.vsota());
        System.out.printf("m3: %d\n",m3.vsota());
        System.out.printf("m4: %d\n",m4.vsota());
        System.out.printf("m5: %d\n",m5.vsota());
        System.out.printf("m6: %d\n",m6.vsota());
        System.out.printf("m7: %d\n",m7.vsota());


        	Matrix c11 = m1.sum(m4);
        	c11 = c11.sub(m5);
        	c11 = c11.sum(m7);

        	Matrix c12 = m3.sum(m5);
        	Matrix c21 = m2.sum(m4);
        	//c22
        	Matrix c22 = m1.sum(m3);
        	c22 = c22.sub(m2);
        	c22 = c22.sum(m6);

        	r.putSubmatrix(0,0,c11);	
        	r.putSubmatrix(0,l/2,c12);
        	r.putSubmatrix(l/2,0,c21);
        	r.putSubmatrix(l/2,l/2,c22);
		return r;

	}
    public void print(){
        for(int i = 0; i<n;i++){
            for(int j = 0; j<n; j++){
                System.out.print(m[i][j] + " ");
            }
            System.out.println();
        }
    }


}




public class Izziv6{

	public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int max = sc.nextInt();

	    Matrix A = new Matrix(n);
        Matrix B = new Matrix(n);

        for(int i = 0; i<n;i++){
            for(int j = 0; j<n; j++){
                A.setV(i, j, sc.nextInt());
            }
        }

        for(int i = 0; i<n;i++){
            for(int j = 0; j<n; j++){
                B.setV(i, j, sc.nextInt());
            }
        }
        Matrix R = A.multStrassen(B, max);
        R.print();
        
	}



}

