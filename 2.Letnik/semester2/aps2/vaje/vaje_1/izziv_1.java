import java.util.*;

class izziv_1{

    public static void main(String[] args){
        izziv_1 obj = new izziv_1();
        obj.test();
    }

    void test(){
        System.out.printf("\tn\t|\tlinearno\t|\tdvojisko\t|\n");
        System.out.printf("-----------------------------------------------------------------\n");
        for(int i = 1000; i<=100000; i+=1000){
               long linear = this.timeLinear(i);
               long binary = this.timeBinary(i);
               this.printTest(i, linear, binary);
        }
    }
    void printTest(int n, long linear, long binary){
        System.out.printf("\t%d\t|\t%d\t|\t%d\t|\n", n, linear, binary);
    }

    int[] generateTable(int n){
        int[] table = new int[n];
        for(int i = 0; i<n;i++){
            table[i] = i+1;
        }
        return table;
    }

    int findLinear(int[] a, int v){
        for(int i = 0; i<a.length; i++){
            if(a[i] == v){
                return i;
            }
        }
        return -1;
    }

    int findBinary(int[] a , int l, int r, int v){
        int mid = (l+r)/2;
        if(a[mid] == v){
            return mid;
        }
        else if(a[mid] > v){
            return this.findBinary(a, l,mid-1,v);
        }
        return this.findBinary(a,mid+1,r,v);
    }
    long timeLinear(int n){
        int[] tab = this.generateTable(n);
        long startTime = System.nanoTime();
        for(int i = 0; i<1000; i++){
            int random = (int)(Math.random()*n)+1;
            this.findLinear(tab,random);
        }
        long executionTime = System.nanoTime() - startTime;
        return executionTime/1000;
    }
    long timeBinary(int n){
        int[] tab = this.generateTable(n);
        long startTime = System.nanoTime();
        for(int i = 0; i<1000; i++){
            int random = (int)(Math.random()*n)+1;
            this.findBinary(tab,0,tab.length-1,random);
        }
        long executionTime = System.nanoTime() - startTime;
        return executionTime/1000;
    }



}
