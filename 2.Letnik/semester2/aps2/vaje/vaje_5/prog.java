import java.util.*;
class prog{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        Num start = new Num();     
        start.num = sc.nextInt();

        Num b = start;
        int length = 1;
        while(sc.hasNextInt()){
            int a = sc.nextInt();
            Num n = new Num();
            n.num = a;
            b.nextNum = n;
            b = n;
            length++;
        }

        int[] table = new int[length];
        for(int i = 0; i<length; i++){
            table[i] = start.num;
            start = start.nextNum;
        }

        int max = maxAlgoritem(table, 0, length-1);	

    }

    public static int maxAlgoritem(int[] table, int start, int end){
        if(start == end){
	    print(table, start, end,table[start]);
            return table[start];
        }

        int mid = (start+end)/2;
        
        int maxL = maxAlgoritem(table, start, mid);
        int maxD = maxAlgoritem(table, mid+1, end);
        int maxS = maxSredina(table,start,mid,end);
        int max = Math.max(Math.max(maxL,maxS), maxD);
	print(table, start, end,max);
	return max;
    }
    public static void print(int[] table, int i, int j,int max){
	System.out.print("[");
	for(;i<=j;i++){
		String presledek = ", ";
		if(i == j){
			presledek = "";
		}	
		System.out.print(table[i] + presledek);
	}
	System.out.print("]");
	System.out.print(": " + max);
	System.out.println();

    }

    public static int maxSredina(int[] table, int start,int mid, int end){
        int sum = 0;
        int sumL = Integer.MIN_VALUE;
        for(int i = mid; i>=start; i--){
            sum += table[i];
            if(sum > sumL){
                sumL = sum;
            }
        }

        int sumR = Integer.MIN_VALUE;
        sum = 0;
        for(int i = mid+1; i<= end; i++){
            sum += table[i];
            if(sum > sumR){
                sumR = sum;
            }
        }

        return sumL + sumR;

    }
}

class Num{
    int num;
    Num nextNum;
    Num(){
        this.nextNum = null;
    }
}
