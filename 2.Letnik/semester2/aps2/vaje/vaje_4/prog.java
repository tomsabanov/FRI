import java.util.*;

class prog{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();

        int[] table = new int[n];
        for(int i = 0; i<n;i++){
            table[i] = sc.nextInt();
        }


        /*
         * freq drzi frekvenco pojavitev bitov v  table
        */
        int[] freq = new int[32]; // 32 zato, ker  imamo samo 32 bitna stevila
        for(int i = 0; i<n;i++){
            int bits = Integer.bitCount(table[i]);
            freq[bits] = freq[bits] + 1;
        }
        
        //System.out.println(Arrays.toString(table));
        //System.out.println(Arrays.toString(freq));


        /*
         * freq drzi zdaj dejansko pozicijo vrednosti v sortirani tabeli
        */
        for(int i = 1; i<32; i++){
            freq[i] += freq[i-1];
        }

        //System.out.println(Arrays.toString(freq));
    
        /*
         * Zgradi sortiratno tabelo
        */
        int[] sorted = new int[n];
        for(int i = n-1; i>=0; i--){
            int orig = table[i];
            int bits = Integer.bitCount(orig);
            //System.out.println("Orig is " + orig);
            int position = freq[bits]-1;
            //System.out.println("Position is " + position);
            sorted[position] = orig;
            System.out.printf("(%d,%d)\n", orig, position);
            freq[bits] = freq[bits] - 1;
        }

        for(int i = 0; i<n;i++){
            System.out.print(sorted[i] + " ");
        }
    }
    
}
