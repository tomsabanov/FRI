import java.util.*;
import java.math.BigInteger;
class prog{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		fourier(n);
	}

	static boolean checkPrime(int p){
		for(int i = 2; i < p; i++){
			if(p%i==0) return false;
		}
		return true;
	}
	static void fourier(int n){
		int p = n;
		int min = Integer.MAX_VALUE;
		while(true){
			p++;
			if(checkPrime(p) == false) continue;
			boolean bool = false;	
			String primes="";
			BigInteger m = new BigInteger(p + "");
			for(int i = 2; i < p; i++){
				BigInteger a = new BigInteger(i + "");
				a=a.pow(n);
				BigInteger mod = a.mod(m);
				if(mod.intValue() == 1 && isPrimeCandidate(i,p,n)==true){
					// Imamo primitivni n-ti koren
					primes+="" + i + " ";
					if(i<min) min = i;
					bool=true;
				}
			}
			if(bool){
				System.out.println(p + ": " + primes);
				break;
			}
		}
		vandermond(min,n,p);
	}

	static boolean isPrimeCandidate(int a, int p, int n){
		BigInteger m = new BigInteger(p+ "");
		for(int i = 1; i<=(n-1); i++){
			BigInteger pow = new BigInteger(a + "");
			pow = pow.pow(i);
			BigInteger mod = pow.mod(m);
			if(mod.intValue() == 1) return false;
		}
		return true;
	}
	static void vandermond(int root, int n, int p){
		BigInteger m = new BigInteger(p + "");
		int[][] matrix = new int[n][n];
		for(int i = 0; i<n; i++){
			for(int j = 0; j<n; j++){
				BigInteger pow = new BigInteger(root + "");
				pow = pow.pow(i*j);
				pow = pow.mod(m);
				matrix[i][j] = pow.intValue();
			}
		}

		for (int row = 0; row < matrix.length; row++) {
			for (int col = 0; col < matrix[row].length; col++) {
				if(matrix[row][col] >=10){
					System.out.print(matrix[row][col] + " ");
				}
				else{
					System.out.print(" " + matrix[row][col] + " ");
				}
			}
			System.out.println();
		 }

	}
}
