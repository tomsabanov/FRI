import java.util.*;
class Complex{
	double re;
	double im;

    public Complex(double real, double imag) {
        re = real;
        im = imag;
    }

    public String toString() {
    	double tRe = (double)Math.round(re * 100000) / 100000;
    	double tIm = (double)Math.round(im * 100000) / 100000;
        if (tIm == 0) return tRe + "";
        if (tRe == 0) return tIm + "i";
        if (tIm <  0) return tRe + "-" + (-tIm) + "i";
        return tRe + "+" + tIm + "i";
    }

	public Complex conj() {
		return new Complex(re, -im);
	}

    // sestevanje
    public Complex plus(Complex b) {
        Complex a = this;
        double real = a.re + b.re;
        double imag = a.im + b.im;
        return new Complex(real, imag);
    }

    // odstevanje
    public Complex minus(Complex b) {
        Complex a = this;
        double real = a.re - b.re;
        double imag = a.im - b.im;
        return new Complex(real, imag);
    }

    // mnozenje z drugim kompleksnim stevilo
    public Complex times(Complex b) {
        Complex a = this;
        double real = a.re * b.re - a.im * b.im;
        double imag = a.re * b.im + a.im * b.re;
        return new Complex(real, imag);
    }

    // mnozenje z realnim stevilom
    public Complex times(double alpha) {
        return new Complex(alpha * re, alpha * im);
    }

    // reciprocna vrednost kompleksnega stevila
    public Complex reciprocal() {
        double scale = re*re + im*im;
        return new Complex(re / scale, -im / scale);
    }

    // deljenje
    public Complex divides(Complex b) {
        Complex a = this;
        return a.times(b.reciprocal());
    }

    // e^this
    public Complex exp() {
        return new Complex(Math.exp(re) * Math.cos(im), Math.exp(re) * Math.sin(im));
    }

    //potenca komplesnega stevila
    public Complex pow(int k) {

    	Complex c = new Complex(1,0);
    	for (int i = 0; i <k ; i++) {
			c = c.times(this);
		}
    	return c;
    }
}

public class prog{

    public static void main(String[] args){
        prog task = new prog();
        Complex[] p1 = task.getPolynom();
        Complex[] p2 = task.getPolynom(); 

        task.n = p1.length;

        Complex[] A = task.FFT(p1);
        Complex[] B = task.FFT(p2);
        Complex[] C = new Complex[A.length];
        for(int i = 0; i<A.length; i++){
            C[i] = A[i].times(B[i]);
        }
       
        
        Complex[] result = task.invFFT(C);
        int n = result.length;
        double d = (1.0/n);
        for(int i = 0; i<n; i++){
            Complex t = result[i].times(d);
            System.out.print(t.toString() + " ");
        }
        
    }

    int n;
    Scanner sc;
    public prog(){
        sc = new Scanner(System.in);
        n = sc.nextInt();
    }
    public Complex[] getPolynom(){
        Complex[] p = new Complex[2*n];
        for(int i = 0; i<(2*n); i++){
            if(i < n){
                p[i] = new Complex(sc.nextDouble(),0);
            }
            else{
                p[i] = new Complex(0.0, 0.0);
            }
        }

        int num = 2*n;
        boolean result = num > 0 && ((num & (num - 1)) == 0); 
        if(!result){
            // a je potrebno razsiriti
            return expand(p);
        }
        return p;
    }
    public Complex[] split(Complex[] a, int n, int start){
        int size = n/2;
        Complex[] p  = new Complex[size];
        int d = 0;
        for(int i = start; i<n;i+=2){
            p[d] = a[i];
            d++;
        }
        return p;
    }

    public void printFFT(Complex[] A ){
        int n = A.length;
        for(int i = 0; i<n; i++){
            System.out.print(A[i].toString() + " ");
        }
        System.out.println();
    }
    

    public Complex[] expand(Complex[] a){
        int exp = 32 - Integer.numberOfLeadingZeros(a.length - 1);
        int k = 1;
        for(int i = 0; i<exp; i++){k*=2;}

        Complex[] p = new Complex[k];
        for(int i=0; i<a.length;i++){
            p[i] = a[i];
        }
        for(int i = a.length; i<k; i++){
            p[i] = new Complex(0.0,0.0);
        }
        return p;
    }
    
    public Complex[] FFT(Complex[] a){
        int n = a.length;

        if(n==1) return a;

        Complex[] even = split(a,n,0);
        Complex[] odd = split(a,n,1);

        Complex[] S = FFT(even);
        Complex[] L = FFT(odd);

        Complex[] A = new Complex[n];
        for(int i = 0; i<n; i++){
            A[i] = new Complex(0.0,0.0);
        }

        Complex wn = new Complex(Math.cos(2 * Math.PI / n), Math.sin(2 * Math.PI / n));
        Complex wk = new Complex(1.0, 0.0);

        for(int k = 0; k<=(n/2)-1; k++){
            Complex fact = wk.times(L[k]);
            A[k] = S[k].plus(fact);
            A[k + n/2] = S[k].minus(fact);
            wk = wk.times(wn);
        }
        printFFT(A);
        return A;
    }

    public Complex[] invFFT(Complex[] a){
        int n = a.length;

        if(n==1) return a;

        Complex[] even = split(a,n,0);
        Complex[] odd = split(a,n,1);

        Complex[] S = invFFT(even);
        Complex[] L = invFFT(odd);

        Complex[] A = new Complex[n];
        for(int i = 0; i<n; i++){
            A[i] = new Complex(0.0,0.0);
        }

        Complex wn = new Complex(Math.cos(2 * Math.PI / n), Math.sin(2 * Math.PI / n)); // INVERSE
        wn = wn.conj();
        Complex wk = new Complex(1, 0.0);

        for(int k = 0; k<=(n/2)-1; k++){
            Complex fact = wk.times(L[k]);
            A[k] = S[k].plus(fact);
            A[k + n/2] = S[k].minus(fact);
            wk = wk.times(wn);
        }
        printFFT(A);
        return A;
    }
}
