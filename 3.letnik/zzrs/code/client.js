var fs = require('fs');
const request = require("request-promise");

const url = "http://40.113.159.110:8080/upload";
const local = "http://127.0.0.1:8080/upload";

async function postFile(){
    console.log("Sending request")
    const options = {
        method: 'POST', 
        uri: url,
        formData: {
          image: fs.createReadStream('./img.jpg')
        }
      };
     await request(options).then(res =>{
         console.log("got response")
         postFile()
     })
}
postFile()