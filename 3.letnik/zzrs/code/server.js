var express = require('express'),
    HttpStatus = require('http-status-codes'),
    morgan = require('morgan'),
    packageConfig = require('./package.json'),
    path = require('path'),
    fs = require('fs'),
    Busboy = require('busboy');

let testConnections = false;
let testWriteSpeed = true;

function statistics(time,filename) {
    server.server.getConnections((error, count) => {
        let content = time + " " + count  + "\n"
        fs.appendFile(filename, content, (err) => {
            if (err) throw err;
            //console.log('The "data to append" was appended to file!');
        });
    });
}

express()
    .use(morgan('combined'))
    .get('/', function(req, res) {
        res.status(HttpStatus.OK).json({
            msg: 'OK',
            service: 'Server works'
        })
    })
    .post('/upload', function(req, res) {
        console.log("Got new request!")
        var busboy = new Busboy({
            headers: req.headers
        });
        //console.log(req.headers);
        let startTime = 
        busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
            filename = filename + "_" + Date.now()
            file.on('data', function(data) {
                //console.log('File [' + fieldname + '] got ' + data.length + ' bytes');
            });
            file.on('end', function() {
                //console.log('File [' + fieldname + '] Finished');
            });
            var saveTo = path.join(__dirname, 'files', path.basename(filename));
            var outStream = fs.createWriteStream(saveTo);
            file.pipe(outStream);
        });
        busboy.on('finish', function() {
            res.writeHead(200, "OK", {'Content-Type': 'text/plain'});
            res.end()
        });
        return req.pipe(busboy);
    }).listen(process.env.PORT || 8080, function() {
        var address = this.address();
        var service = packageConfig.name + ' version: ' + packageConfig.version + ' ';
        console.log('%s Listening on %d', service, address.port);
    });