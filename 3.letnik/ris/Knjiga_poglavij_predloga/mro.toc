\babel@toc {nil}{}
\contentsline {chapter}{Predgovor}{iii}%
\contentsline {chapter}{\numberline {1}Analiza zmogljivosti oblačnega ponudnika Azure (T. "Sabanov, M. Resman)}{1}%
\contentsline {section}{\numberline {1.1}Uvod}{1}%
\contentsline {section}{\numberline {1.2}Osnove oblačnih Benchmark programov}{2}%
\contentsline {subsection}{\numberline {1.2.1}SPEC Cloud IaaS 2018}{2}%
\contentsline {subsection}{\numberline {1.2.2}Cloud Spectator}{3}%
\contentsline {subsection}{\numberline {1.2.3}Geekbench 3}{3}%
\contentsline {subsection}{\numberline {1.2.4}Fio}{3}%
\contentsline {subsection}{\numberline {1.2.5}IOPing}{3}%
\contentsline {subsection}{\numberline {1.2.6}Public Cloud Benchmark}{3}%
\contentsline {subsection}{\numberline {1.2.7}Cloud Performance Benchmark}{3}%
\contentsline {subsection}{\numberline {1.2.8}PerfKit Benchmarker}{4}%
\contentsline {section}{\numberline {1.3}Povzetek orodij}{4}%
\contentsline {section}{\numberline {1.4}Implementacija merilnega okolja}{5}%
\contentsline {section}{\numberline {1.5}Rezultati zmogljivosti omrežja}{5}%
\contentsline {subsection}{\numberline {1.5.1}Opis povezav}{5}%
\contentsline {subsection}{\numberline {1.5.2}Ping}{6}%
\contentsline {subsection}{\numberline {1.5.3}Traceroute}{7}%
\contentsline {subsection}{\numberline {1.5.4}Speedtest}{7}%
\contentsline {subsection}{\numberline {1.5.5}Iperf}{7}%
\contentsline {subsection}{\numberline {1.5.6}Testiranje strežnika z vzporednimi klienti}{8}%
\contentsline {section}{\numberline {1.6}Rezultati zmogljivosti procesorja}{8}%
\contentsline {subsection}{\numberline {1.6.1}Coremark}{8}%
\contentsline {subsection}{\numberline {1.6.2}Copy throughput}{9}%
\contentsline {subsection}{\numberline {1.6.3}Iskanje praštevil v intervalu}{9}%
\contentsline {section}{\numberline {1.7}Rezultati zmogljivosti vhodno/izhodnih naprav}{10}%
\contentsline {subsection}{\numberline {1.7.1}Fio}{11}%
\contentsline {subsection}{\numberline {1.7.2}Testiranje zmogljivosti diska z vzporednimi klienti}{11}%
\contentsline {subsection}{\numberline {1.7.3}Merilci resursov}{12}%
\contentsline {section}{\numberline {1.8}Ekstremna obremenitev sistemskih resursov platforme}{15}%
\contentsline {subsection}{\numberline {1.8.1}Maksimalna obremenjenost pomnilnika}{15}%
\contentsline {subsection}{\numberline {1.8.2}Maksimalna zasedenost diska}{16}%
\contentsline {section}{\numberline {1.9}Testiranje zmogljivosti podatkovne baze}{16}%
\contentsline {section}{\numberline {1.10}Zaključek}{16}%
