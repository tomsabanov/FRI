# import the necessary packages
from imutils.video import VideoStream
import numpy as np
import argparse
import imutils
import time
import cv2
import math
import os
import uuid 

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--prototxt", required=True,
    help="path to Caffe 'deploy' prototxt file")
ap.add_argument("-m", "--model", required=True,
    help="path to Caffe pre-trained model")
ap.add_argument("-c", "--confidence", type=float, default=0.5,
    help="minimum probability to filter weak detections")

ap.add_argument("-s", "--stream", default=0,
    help="Path to video stream, default is camera")

ap.add_argument("-o", "--hair", type=bool,default=False,
    help="Extending the detected face to include hairstyle")

args = vars(ap.parse_args())



print("[INFO] loading model...")
net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])
# initialize the video stream and allow the camera sensor to warm up
print("[INFO] starting video stream...")
vs = VideoStream(src=args["stream"]).start()
time.sleep(2.0)


base = os.path.basename(args['stream'])
base = "./faces/"+ os.path.splitext(base)[0]
os.makedirs(base)


numberOfFrames = 0
numberOfDetected = 0

# loop over the frames from the video stream
while True:
    # grab the frame from the threaded video stream and resize it
    # to have a maximum width of 400 pixels
    frame = vs.read()

    if frame is None:
        print("Finished with stream!")
        break
    numberOfFrames = numberOfFrames + 1

    frame = imutils.resize(frame, width=400)
 
    # grab the frame dimensions and convert it to a blob
    (h, w) = frame.shape[:2] 

    blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 1.0,
        (300, 300), (104.0, 177.0, 123.0))
 
    # pass the blob through the network and obtain the detections and
    # predictions
    net.setInput(blob)
    detections = net.forward()

    numberOfDetected = numberOfDetected +1

    # loop over the detections
    for i in range(0, detections.shape[2]):

        # extract the confidence (i.e., probability) associated with the
        # prediction
        confidence = detections[0, 0, i, 2]
        # filter out weak detections by ensuring the `confidence` is
        # greater than the minimum confidence
        if confidence < args["confidence"]:
            continue
        # compute the (x, y)-coordinates of the bounding box for the
        # object
        box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
        (startX, startY, endX, endY) = box.astype("int")

        if args["hair"] is True:
            startX = math.floor(startX * 0.95)
            startY = math.floor(startY * 0.95)
            endX = math.floor(endX * 1.05)
            endY = math.floor(endY * 1.05)

        crop_img = frame[startY:endY, startX:endX]

        random = uuid.uuid4().hex + ".jpg"
        filename = base+"/" + random
        print(filename)
        cv2.imwrite(filename,crop_img)

        # draw the bounding box of the face along with the associated
        # probability
        text = "{:.2f}%".format(confidence * 100)
        y = startY - 10 if startY - 10 > 10 else startY + 10
        cv2.rectangle(frame, (startX, startY), (endX, endY),
            (0, 0, 255), 2)
        cv2.putText(frame, text, (startX, y),
            cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)

        # show the output frame
        cv2.imshow("Frame", frame)
        key = cv2.waitKey(1) & 0xFF    
        # if the `q` key was pressed, break from the loop
        if key == ord("q"):
            break


# do a bit of cleanup
cv2.destroyAllWindows()
vs.stop()


print("Number of frames: ", numberOfFrames)
print("Number of detected frames: ", numberOfDetected)