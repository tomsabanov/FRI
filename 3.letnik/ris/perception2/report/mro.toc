\babel@toc {slovene}{}
\contentsline {chapter}{Predgovor}{iii}
\contentsline {section}{\numberline {0.1}Introduction}{1}
\contentsline {section}{\numberline {0.2}Osnove obla\IeC {\v c}nih Benchmark programov}{2}
\contentsline {subsection}{\numberline {0.2.1}SPEC Cloud IaaS 2018}{2}
\contentsline {subsection}{\numberline {0.2.2}Cloud Spectator}{2}
\contentsline {subsection}{\numberline {0.2.3}Geekbench 3}{2}
\contentsline {subsection}{\numberline {0.2.4}Fio}{2}
\contentsline {subsection}{\numberline {0.2.5}IOPing}{3}
\contentsline {subsection}{\numberline {0.2.6}Public Cloud Benchmark}{3}
\contentsline {subsection}{\numberline {0.2.7}Cloud Performance Benchmark}{3}
\contentsline {subsection}{\numberline {0.2.8}PerfKit Benchmarker}{4}
\contentsline {section}{\numberline {0.3}Povzetek orodij}{4}
\contentsline {section}{\numberline {0.4}Implementacija merilnega okolja}{4}
\contentsline {section}{\numberline {0.5}Rezultati zmogljivosti omre\IeC {\v z}ja}{5}
\contentsline {subsection}{\numberline {0.5.1}Opis povezav}{5}
\contentsline {subsection}{\numberline {0.5.2}Ping}{5}
\contentsline {subsection}{\numberline {0.5.3}Traceroute}{5}
\contentsline {subsection}{\numberline {0.5.4}Speedtest}{6}
\contentsline {subsection}{\numberline {0.5.5}Iperf}{6}
\contentsline {subsection}{\numberline {0.5.6}Testiranje stre\IeC {\v z}nika z vzporednimi klienti}{6}
\contentsline {section}{\numberline {0.6}Rezultati zmogljivosti procesorja}{7}
\contentsline {subsection}{\numberline {0.6.1}Coremark}{8}
\contentsline {subsection}{\numberline {0.6.2}Copy throughput}{8}
\contentsline {subsection}{\numberline {0.6.3}Iskanje pravih \IeC {\v s}tevil v intervalu}{8}
\contentsline {section}{\numberline {0.7}Rezultati zmogljivosti vhodno/izhodnih naprav}{8}
\contentsline {subsection}{\numberline {0.7.1}Fio}{8}
\contentsline {subsection}{\numberline {0.7.2}Testiranje zmogljivosti diska z vzporednimi klienti}{10}
