import numpy as np
import argparse
import imutils
import time
import cv2
import math
import os
import uuid 
import shutil

# This script should seperate our dataset in faces/ to train in val directories 

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--face_dataset", required=True,
    help="path to dataset with faces")

ap.add_argument("-t", "--train", default=0.5,
    help="Percentage of train/validation")
args = vars(ap.parse_args())


train_base = "./train/"
val_base = "./val/"
faces_base = args["face_dataset"]

train_perc = args["train"]

try:
    shutil.rmtree(train_base)
    shutil.rmtree(val_base)

    os.makedirs(train_base)
    os.makedirs(val_base)
except OSError as error:
    print(error)
    exit

for face_dir in os.listdir(faces_base):
    if face_dir.isnumeric():
        continue
    
    path_face = os.path.join(faces_base,face_dir)
    
    files = os.listdir(path_face)
    num = len(files)

    train_num = math.floor(train_perc * num)

    train_files = files[:train_num]
    val_files = files[train_num:]

    print("Path face ", path_face)
    print("Number of all files: ",num)
    print("train_files : ", len(train_files))
    print("val files : ", len(val_files))
    print("------------------------------")

    train_path = os.path.join("train",face_dir)
    val_path = os.path.join("val",face_dir)

    os.makedirs(train_path)
    os.makedirs(val_path)

    ############### Copy files to train and val directories, get base from face_dir
    for f in train_files:
        src = os.path.join(path_face,f)
        dest = os.path.join(train_path, f)
        shutil.copyfile(src, dest)

    for f in val_files:
        src = os.path.join(path_face,f)
        dest = os.path.join(val_path, f)
        shutil.copyfile(src, dest)
