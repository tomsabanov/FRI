\babel@toc {english}{}
\babel@toc {slovene}{}
\contentsline {chapter}{Povzetek}{}{chapter*.2}%
\babel@toc {english}{}
\contentsline {chapter}{Abstract}{}{chapter*.3}%
\babel@toc {slovene}{}
\contentsline {chapter}{\numberline {1}Uvod}{1}{chapter.1}%
\contentsline {chapter}{\numberline {2}Sintetizatorji govora v sloven\IeC {\v s}\IeC {\v c}ini}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}Govorec}{3}{section.2.1}%
\contentsline {section}{\numberline {2.2}eBralec}{4}{section.2.2}%
\contentsline {chapter}{\numberline {3}Sodobni sintetizatorji govora}{5}{chapter.3}%
\contentsline {section}{\numberline {3.1}WaveNet}{5}{section.3.1}%
\contentsline {section}{\numberline {3.2}Tacotron}{5}{section.3.2}%
\contentsline {chapter}{\numberline {4}Opis metod in okolja}{7}{chapter.4}%
\contentsline {chapter}{\numberline {5}Opis arhitekture za sintezo govora}{9}{chapter.5}%
\contentsline {chapter}{\numberline {6}Mno\IeC {\v z}ica glasovnih posnetkov}{11}{chapter.6}%
\contentsline {chapter}{\numberline {7}Treniranje na mno\IeC {\v z}ici glasovnih posnetkov}{13}{chapter.7}%
\contentsline {chapter}{\numberline {8}Analiza rezultatov}{15}{chapter.8}%
\contentsline {chapter}{\numberline {9}Sklepne ugotovitve}{17}{chapter.9}%
\contentsline {chapter}{Literatura}{19}{chapter.9}%
