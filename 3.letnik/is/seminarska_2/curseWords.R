# IS Assignment 2 - Mining insults

# Load the training dataset
trainingDataTable <- read.table(
  file = "train.tsv",
  sep = '\t',
  quote = "", # THIS IS IMPORTANT
  header = TRUE,
  fileEncoding="UTF-8",
  stringsAsFactors = FALSE # ALSO IMPORTANT
  )
# Clean up the text

# Regex for special characters
stupidRRegexShitFuck = "[^a-zA-Z0-9]|\\\\t|\\\\n|\\\\\\\\n|\\\\\\\\'|\\\\x[0-9a-f]{2}|\\\\\\\\x[0-9a-f]{2}|<[^\\>]+>|\"|\\\\\\\\|\\\\u[0-9]{4}"

# Regex for stopwords
words = scan("english.stop.txt", what="character", sep="\n")
stopWordsRegex = paste(words, sep="", collapse = "|")
stopWordsRegex1 = paste("( |^)", words, "( |$)", sep="", collapse = "|")

cleanup <- function(text) {
  clean <- gsub(stupidRRegexShitFuck, " ", text)
  clean <- gsub(stopWordsRegex1, "", clean)
  # Remove leftover single letters
  #clean <- gsub(" [a-zA-Z] ", " ", clean)
  clean <- gsub("\\s+", " ", clean)
  clean
}

for (i in c(1:length(trainingDataTable$label))) {
  print(i)
  trainingDataTable$text_a[i] <- cleanup(trainingDataTable$text_a[i])
}


# Save processed dataframe
write.table(trainingDataTable, file="semiprocessed.tsv", sep="\t", row.names = FALSE)


# Load the cleaned up version
trainingDataTable <- read.table(
  file = "semiprocessed.tsv",
  sep = '\t',
  quote = "", # THIS IS IMPORTANT
  header = TRUE,
  fileEncoding="UTF-8",
  stringsAsFactors = FALSE # ALSO IMPORTANT
)

# ONE OPTION
# library(udpipe)
# library(dplyr)
# udmodel <- udpipe_load_model("english-ewt-ud-2.4-190531.udpipe")
# Section <- c(trainingDataTable$text_a[14])
# Section
# tokenizationFrame <- data.frame(Section, stringAsFactors = FALSE)

# x <- udpipe_annotate(udmodel, tokenizationFrame$Section)
# x <- as.data.frame(x)
# x %>% select(token, upos)
# BUT THIS FAILS FOR LITERALLY THE FIRST WORD (Xanax -> brand name)

library(koRpus.lang.en)
library(openssl)

for (i in c(1:length(trainingDataTable$X.label.))) {
  print(i)
  # Save the text in a txt file so that we can pass it to treetag()
  fileConn<-file("temp.txt")
  # Skip empty or all whitespace strings
  if(length(grep('^"\\s*"$', trainingDataTable$X.text_a.[i])) > 0) {
    next
  }
  writeLines(trainingDataTable$X.text_a.[i], fileConn)
  close(fileConn)
  # Tag the text in the file
  tagged.text <- treetag(
    "temp.txt",
    treetagger="manual",
    lang="en",
    TT.options=list(
      path="c:/TreeTagger",
      tokenizer="utf8-tokenize.perl",
      tagger="tree-tagger.exe",
      params="english.par",
      preset="en"
    ),
    doc_id="sample"
  )
  # Anonymize the words tagged with NP
  words <- strsplit(trainingDataTable$X.text_a.[i], " ")[[1]]
  for(j in c(1:length(words))) {
    # Check if the word is NP
    word <- words[j]
    for(k in c(1:length(tagged.text@TT.res$doc_id))) {
      #print(c(word, "VS", tagged.text[k]$token))
      if(word == tagged.text[k]$token & tagged.text[k]$tag == "NP") {
        # Replace the word in the list with its MD4 hash
        words[j] = md4(word)
      }
    }
  }
  
  # Write the anonymized vector back to the dataFrame
  trainingDataTable$X.text_a.[i] <- paste(words, collapse=" ")
}

# Save the fully processed training data
write.table(trainingDataTable, file="fullyprocessed.tsv", sep="\t", row.names = FALSE)
