# IS_Assignment_2
This repository holds the project files for the second assignment for the Inteligent Systems course.
To use properly, make sure TreeTagger is installed on your system:
https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/
