---
title: "Assignment 1"
author: "Tom Šabanov, Andrej Gorjan"
date: "11/9/2019"
output:
  pdf_document: default
  word_document: default
subtitle: Intelligent Information Systems - Labs
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Task 1: Maximization of a non-convex function.

### What does it mean for a function to be non-convex?
A function is non-convex if it is not convex, which means it is not a continious function whose value at the midpoint of every interval in its domain does not exceed the arithmetic mean of its values at the ends of the interval. 

### Computing the value of Rosenbrock function
```{r}
  computeZ <- function(x,y){
    z = (1-x)^2 + exp(1) * (y - x^2)^2;
  }
```

### Visualizing Rosenbrock function in 3D
``` {r persp}
  show <- function(num){
    # num - number of x and y points
    x <- seq(-1,1,length=num)
    y <- seq(-1,1,length=num)
    z <- outer(x,y, computeZ)
    
     # With rgl package 
     #plot.new()
     #persp3d(x, y, z ,col ="lightblue")
     #surface3d(x,y,z, back="lines", front="lines")
     
     persp(x,y,z,col="lightblue",theta=100)
  }
  show(50) # We compute 2500 values of z
```
A true interactive 3D visualization is done with the help of the rgl package. 

### Global maximum of the Rosenbrock function.

The global maximum is found in the point x=-1,y=-1 with a value of 14.87313, so with the fit function we are trying to maximize the 'z' value of our (x,y) point. In the monitor function we choose the agent with the maximum z-value and plot it on our visualization.
To trace the evolution we save each point into a global list of vectors, where each points coordinates are saved and then displayed with lines3d function, which is a part of the rgl package.


```{r eval=FALSE}
  monitor <- function(obj) { 
    
    # We take the maximum z value in our fitness values
    fitness = obj@fitness;
    max_fit = max(fitness);
    
    # We get the x and y coordinates of our max value
    max_fit_indx = match(max_fit, fitness);
    xy = obj@population[max_fit_indx,1:2];
    x = xy[1];
    y = xy[2];
    z = max_fit;
    
    # We save each coordinate into a global list of vectors of x,y and z coordinates
    max_points[[1]] <<- c(max_points[[1]],x);
    max_points[[2]] <<- c(max_points[[2]],y);
    max_points[[3]] <<- c(max_points[[3]],z);
    
    # Plotting the trace of evolution with rql package
    # We plot the point onto our visualization in 3D
    #points3d(x,y,z, color=c("red"), size=10.0);  
    
    # We draw the lines between each point in the saved global list of vectors of x,y and z coordinates
    #lines3d(max_points[[1]], max_points[[2]],max_points[[3]], color=c("red"), lwd=2);  
    
    Sys.sleep(0.1)
  }
  # Our fitness function, where we want to maximize the computed z-value
  fit <- function(x){
    computeZ(x[1],x[2])
  }
 ```
The calcMax function takes the number of iterations,crossover and mutation settings with the population size and executes our genetic algorithm with x and y being bounded to interval [-1,1] respectively.
 ```{r eval=FALSE}
  # Function that displays our visualization and uses the cross and mut options for crossover and mutation respectively
  calcMax <- function(iter, cross=gaControl("real-valued")$crossover, mut=gaControl("real-valued")$mutation,size=50){
    show(50)
    
    # We create the list of vectors for x,y and z coordiantes
    max_points <<- list(c(),c(),c());
    
    GA <- ga(type = "real-valued", fitness = fit, 
             lower=c(-1,-1) , upper=c(1,1), monitor=monitor,
             maxiter=iter, crossover=cross, mutation=mut,
             popSize=size)
    
    sol = GA@solution;
    sol_x = sol[1];
    sol_y = sol[2];
    sol_z = computeZ(sol_x, sol_y);
    
    # We plot the solution with a green color
    #points3d(sol_x,sol_y,sol_z, color=c("green"), size=15.0);  
    
    plot(GA)
    summary(GA)
  }
  calcMax(100)
  
  # Examples of different combinations of crossover and mutation settings, that worked pretty well
  #calcMax(100,gabin_spCrossover,gareal_raMutation)
  #calcMax(100,gabin_uCrossover,gareal_raMutation)
  #calcMax(100,gabin_uCrossover,gareal_nraMutation)
  #calcMax(100,gareal_spCrossover,gareal_raMutation)
  #calcMax(200,gareal_spCrossover,gareal_nraMutation)
  #calcMax(200,gareal_blxCrossover,gareal_raMutation)
  #calcMax(500,gareal_blxCrossover,gareal_nraMutation)
  #calcMax(200,gareal_blxCrossover,gareal_powMutation)
  #calcMax(200,gareal_laplaceCrossover,gareal_raMutation)
  #calcMax(100,gareal_laplaceCrossover,gareal_nraMutation)
  #calcMax(200,gareal_waCrossover,gareal_nraMutation)
```

```{r example_1, echo=FALSE, fig.cap="Trace of evolution with global maximum colored green", out.width = '100%'}
knitr::include_graphics("Example.png")
```
```{r example_2, echo=FALSE, fig.cap="Trace of evolution of our GA getting close to a local maximum", out.width = '100%'}
knitr::include_graphics("Example2.png")
```
### Performance

We achived the global maximum of the Rosenbrock function with multiple different crossover and mutation settings. As we increased the number of iterations our computation of course takes longer, but the chance of reaching the global maximum increases. With the default population size of 50 we can get close to the global maximum after only 20 iterations, after that it takes about 80 additional iterations to reach the global maximum. As we are getting closer to it, the distances between the computed points and the global maximum become smaller and smaller, thus making it more difficult for our genetic algorithm to reach it.
The populations size also plays a large part of finding the global maximum. As we decrease the population size we have to increase the number of iterations with hope of reaching the global maximum, as we are more likely to get stuck on a local maximum, but if we increase the population size, we will get close to the global maximum a lot faster, but if we wish to truly reach it, we still require additional iterations, as genetic algorithms heavily realy on probability. 


### Local and global maximum
A maximum is said to be local, if it is the largest value of the function on a certain interval, whereas the global maximum is the largest value of the function on the entire domain of the function. An example of a global maximum can be seen on Figure 1, while on Figure 2 an attempt of finding a local maximum can be seen.


## Task 2: Genetic feature selection

The dataset 'DLBCL.csv' contains 1070 attributes, but some of them might be redundant or poorly discriminate the examples, so we would like to only use the 'best' attributes for our classification. For this, we used a genetic algorithm over a population of binary vectors where each one was 1070 bits long and symbolised wheather a certain attribute was chosen to be used in classification. For example, if the vector is [1, 0, ...] we use the first attribute, not use the second one and so on. This method of feature selection was somewhat successfull, since we managed to narrow down the number of attributes to 406. We do suspect though, that this number is still rather large, given that we only have 77 examples to learn from.

```{r results='hide', echo=FALSE}
load('./result1.Rdata')
```

```{r message=FALSE}
summary(GA)
plot(GA)
```

### Genetic algorithm progress
We can see the progress of our genetic algorithm on the graph. Initially, the performance rises quicky, but then tapers off after around generation 200. After that, we can see a slow, approximately constant improvement for the rest of the selection process. In the end, the genetic algorithm selected 406 attributes from the dataset, which produced a classification tree with an average accuracy (due to 3 fold cross validation) of 90%. The fitness value was calculated with the following fitness function:

```{r eval=FALSE}
# The fitness function
f <- function(x)
{
  # x is a binary vector, if it 'selects' less than 2 or more than 1000
  # attributes, automatically return 0, because of the hard limit
  if (sum(x) < 2 || sum(x) > 1000) {
    f = 0
  } else {
    # Create a temporary vector of integers from 1 to the number of
    # elements in x (which represent attributes).
    s <- seq(1, length(x))
    # Select only those integers where the matching element in x is 1
    p <- s[as.logical(x)]
    # Add 1071, which is the index of our target class, which must always
    # be included for classification
    p <- append(p, 1071, after = length(p))
    
    # Select the chosen columns from the dataset
    d <- select(mydata, p)
    
    # Train the model using the selected data. The target class is
    # is dependent on all of the attributes in the passed data. We
    # use a tree model for classification
    model <- train(class~., data = d, trControl=trn_ctrl, method = 'ctree')
    
    # Calculate the fitness value. Since we are using 3 fold cross validation
    # we average the accuracy calculated on each of the 3 testing sets. We
    # multiply this value with a weight, calculated from the number of
    # selected attributes.
    f = mean(model$results$Accuracy) * w(sum(x))
  }
}
```

For our application, attribute numbers lower than 2 and higher than 1000 produce the value 0, since we have a hard limit in place. Otherwise, the function uses the selected attributes to construct the classification tree and calculates its accuracy. The next step, where we multiply this result with a weight is key, since this is where we decide to prefer lower attribute counts. Our weight function `w()` is:

```{r eval=FALSE}
# A weight function for the fitness function, that prefers lower
# values of x and reaches the minimum at x = 1000
w <- function(x)
{
  w = 1 + 400 / (1 + exp(0.01 * (x - 500))) + 400 / (1 + exp(0.01 * x))
}
```

```{r message=FALSE}
x <- seq(0, 1000, 1)
plot(x, 1 + 400 / (1 + exp(0.01 * (x - 500))) + 400 / (1 + exp(0.01 * x)), main="The weight function w(x)", ylab="Weight value", xlab="Number of selected attributes", type="l", col="blue")
```

### Possible improvements

Despite our high accuracy, the process may still be improved. Progress towards the end of the run may have been slow, but it appeared constant, so we would probably reach a better solution if we had allowed the algorithem to run longer. We could also have increased the population size, but both of these options would increase the calculation time, which was already quite long (about 12h on a laptop and 25h on a low powered virtual server, both calculating in parallel mode). Another key factor for success was the weight function. Initially, we used a linear function with a very mild slope, but quickly realised that it was unfit since it chose upwards of 600 attributes. We changed it to the more nuanced function used here, but it may still be improved to strongly prefer lower values. However, even an extreme function like `(x - 1000)^2` selected 398 attributes, which is not that better than what we achieved here.

### How do we prevent overfitting of the dataset?

The search for the best attributes was performed on 77 examples in the data set. But how can we be sure that the genetic algorithm didn't overfit the data and select attributes that work best only for these 77 cases. One method would be cross validation, where we would perform the feature selection on some subset and then test its performance on the remaining unseen data (like we do with the tree model in the fitness function). Since overfitting is associated with too much specificity, we could also choose only the best of the 406 attributes (a kind of Ockhams razor if you will, 406 attributes is still a pretty complex solution). In the next chapter we can see an example of this where we analyse which attributes are the best in terms of correlation. Another way is to increase the size of the training set, but we may not always have this luxury.

### How good is the result?

We can use the following code to check how good our selection was based on the corelation between the attributes and the class.

```{r, eval=FALSE}
# Convert the class from string values to a simple binary representation
class <- 1 * (mydata[, 1071] == 'DLBCL')

# Calculate the corelation
corelation <- cor(x = mydata[,1:1070], y = class)

# Sort the vector and get the 409th value
val <- (sort(corelation, decreasing = TRUE))[406]

# Calculate indices that are higher than the value
result <- (corelation >= val) * 1
```

This gives us a binary vector with 1s at locations where the corelation was highest. Using this, we can see where `result` and our solution intersect:

```{r eval=FALSE}
sum(GA@solution[1, ] * result)
```

We get a result of 153, so about 37% of the attributes that we chose matched the ones with the highest corelation to the class.

### The full code

```{r eval=FALSE}
library(caret)
library(dplyr)
library(GA)
library(parallel)

# Import the data from the csv
mydata <- read.csv("./DLBCL.csv",
                   header=TRUE,
                   sep=",")

# The X argument is definitely pointless as it just enumerates the
# rows. We aren't sure what the attribute 'atclass' is, but it's 
# name doesn't match the scheme used for other attributes, so we
# will presume it is not important as well.
mydata$X = NULL
mydata$atclass = NULL

# Use 3 fold cross validation
trn_ctrl = trainControl(method = 'cv', number = 3)

# A weight function for the fitness function, that prefers lower
# values of x and reaches the minimum at x = 1000
w <- function(x)
{
  w = 1 + 400 / (1 + exp(0.01 * (x - 500))) + 400 / (1 + exp(0.01 * x))
}

# The fitness function
f <- function(x)
{
  # x is a binary vector, if it 'selects' less than 2 or more than 1000
  # attributes, automatically return 0, because of the hard limit
  if (sum(x) < 2 || sum(x) > 1000) {
    f = 0
  } else {
    # Create a temporary vector of integers from 1 to the number of
    # elements in x (which represent attributes).
    s <- seq(1, length(x))
    # Select only those integers where the matching element in x is 1
    p <- s[as.logical(x)]
    # Add 1071, which is the index of our target class, which must always
    # be included for classification
    p <- append(p, 1071, after = length(p))
    
    # Select the chosen columns from the dataset
    d <- select(mydata, p)
    
    # Train the model using the selected data. The target class is
    # is dependent on all of the attributes in the passed data. We
    # use a tree model for classification
    model <- train(class~., data = d, trControl=trn_ctrl, method = 'ctree')
    
    # Calculate the fitness value. Since we are using 3 fold cross validation
    # we average the accuracy calculated on each of the 3 testing sets. We
    # multiply this value with a weight, calculated from the number of
    # selected attributes.
    f = mean(model$results$Accuracy) * w(sum(x))
  }
}

# Start the genetic algorithm. Use binary vector specimens and the fitness
# function 'f'. nBits is set to 1070, since this is the number of attributes
# we can choose from.
GA <- ga(type = 'binary',
         fitness = f,
         nBits = 1070,
         maxiter = 1000,
         run = 1000,
         popSize = 50,
         parallel = TRUE)
save(GA, file = './result.RData')
summary(GA)

# Convert the class from string values to a simple binary representation
class <- 1 * (mydata[, 1071] == 'DLBCL')

# Calculate the corelation
corelation <- cor(x = mydata[,1:1070], y = class)

# Sort the vector and get the appropriate value
val <- (sort(corelation, decreasing = TRUE))[sum(GA@solution[1, ])]

# Calculate indices that are higher than the value
result <- (corelation >= val) * 1

# Count the number of matching attributes
matching <- sum(GA@solution[1, ] * result)
```