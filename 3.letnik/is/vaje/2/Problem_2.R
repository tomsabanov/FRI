
##########################################################################################################################
#
# - Use GA search to find the minimum of the real-valued two-dimensional function 
#   f(x1, x2) = 20 + x1^2 + x2^2 - 10*(cos(2*pi*x1) + cos(2*pi*x2)), where x1 and x2 are from the interval [-5.12, 5.12].
#	
##########################################################################################################################

# We first define our model function (argument params represents a vector of the parameters a, b, and c) 
f <- function(x)
{
  y <- (20 + x[1]^2 + x[2]^2 - 10*(cos(2*pi * x[1])) + cos(2*pi*x[2]))
  y ### return(y)
}


# For the maximization of this function we may use f directly as the fitness function
GA <- ga(type = "real-valued", fitness = f, lower = c(-5.12,-5.12), upper = c(5.12, 5.12))
plot(GA)
summary(GA)


