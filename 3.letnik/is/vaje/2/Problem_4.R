##########################################################################################################################
#
# - Use a binary GA to select (sub)optimal attribute subset for a linear model:
#
#   train.data <- read.table("AlgaeLearn.txt", header = T)
#   test.data <- read.table("AlgaeTest.txt", header = T)
#   lm.model <- lm(a1 ~., train.data)
#
##########################################################################################################################
train.data <- read.table("/home/tom/Desktop/AlgaeLearn.txt", header = T)
test.data <- read.table("/home/tom/Desktop/AlgaeTest.txt", header = T)
lm.model <- lm(a1 ~., train.data)


values <- train.data[0,]
weights <-train.data

bin <- function(x) 
{
  f <- sum(x * values)
  w <- sum(x * weights)
  
  if (w > Capacity)
    f <- Capacity - w
  
  f	
}


