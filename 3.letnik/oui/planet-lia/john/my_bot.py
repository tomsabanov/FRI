import asyncio
import random

from core.bot import Bot
from core.networking_client import connect
from core.enums import Direction
import math

import path
import LiaBot


class MyBot(Bot):

    # Called only once before the match starts. It holds the
    # data that you may need before the game starts.
    def setup(self, initial_data):
        self.initial_data = initial_data
        self.map = initial_data["map"]
        self.height = initial_data["mapHeight"]
        self.width = initial_data["mapWidth"]

        self.prevPath = None
        self.prevTarget = None
        self.prevPathIndex = 0
        self.numberOfSaws = 0
    
    def getSawMap(self, saws):
        h = self.height
        w = self.width
        m=[[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]]
        
        for saw in saws:
            y = saw["y"]
            x = saw["x"]
            m[y][x] += 1
        return m

    # Called repeatedly while the match is generating. Each
    # time you receive the current match state and can use
    # response object to issue your commands.
    def update(self, state, response):
        me = state["yourUnit"]
        my_pos = [me["y"], me["x"]]

        # obstacles on map
        saws = state["saws"]
        sawMap = self.getSawMap(saws)
        # Coin selection
        target_pos = self.coinSelect(state,my_pos)

        # If new saws spawned or if target pos changed, we calculate new path
        if(len(saws) != self.numberOfSaws or target_pos != self.prevTarget):
            self.numberOfSaws = len(saws)
            self.prevTarget = target_pos
            bot = LiaBot.LiaBot(self.map,sawMap,self.height, self.width,my_pos,target_pos,saws)     
            solution = path.A(bot)
            
            self.prevPath = solution
            self.prevPathIndex = 0
        else:
            self.prevPathIndex += 1

        move = self.prevPath[self.prevPathIndex][0]

        if move[0]==0 and move[1]==1: # Desno
            direction = Direction.RIGHT
        elif move[0]==0 and move[1]==-1: # Levo
            direction = Direction.LEFT
        elif move[0]==1 and move[1]==0: # Gor
            direction = Direction.UP
        elif move[0]==-1 and move[1]==0: # Dol
            direction = Direction.DOWN
        elif move[0]==0 and move[1]==0: # Stoj na miru
            return
        
        response.move_unit(direction)

    
    def coinSelect(self, state,my_pos):
        opponent = state["opponentUnit"]
        opp_pos = [opponent["y"], opponent["x"]]

        coins = state["coins"] # Cpins on map

        # If opponent is closer to coin0, then we go for the coin1
        d0 = abs(my_pos[0] - coins[0]["y"]) + abs(my_pos[1] - coins[0]["x"])
        d2 = abs(opp_pos[0] - coins[0]["y"]) + abs(opp_pos[1] - coins[0]["x"])

        if d2 < d0 :
            target = coins[1]
        elif d0 < d2 :
            target = coins[0]
        else:
            ind = random.choice([1,0])
            target = coins[ind]

        target_pos = [target["y"], target["x"]]

        return target_pos
              



# Connects your bot to match generator, don't change it.
if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(connect(MyBot()))
