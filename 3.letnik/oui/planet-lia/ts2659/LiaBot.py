import math

class LiaBot:
    def __init__(self,map,sawMap,height,width,bot, target, saws):
        self.map = map
        self.width = width
        self.height = height
        self.bot = bot
        self.target = target
        self.saws = saws
        self.sawMap = sawMap
        self.lives = 3


    def solved(self):
        return self.bot == self.target  

    def moveSaws(self):
        h = self.height - 1
        w = self.width - 1
        for saw in self.saws:
            move = self.getSawMove(saw)

            # Make saw move and change values on sawMap
            self.sawMap[saw["y"]][saw["x"]] -= 1
            saw["y"] += move[0]
            saw["x"] += move[1]
            self.sawMap[saw["y"]][saw["x"]] += 1

            # Check if saw direction needs to change in current spot -> it only 
            # needs to change if its in the corner or on the edge
            if saw["y"]==0 or saw["y"]==h or saw["x"]==0 or saw["x"]==w:
               saw["direction"] = self.sawChangeDirection(saw)


    def undoMoveSaws(self):
        h = self.height - 1
        w = self.width - 1
        
        for saw in self.saws:

            # First change direction if in corner or border
            if saw["y"]==0 or saw["y"]==h or saw["x"]==0 or saw["x"]==w:
               saw["direction"] = self.sawReverseChangeDirection(saw)

            move = self.getSawMove(saw)
            move[0] = -move[0]
            move[1] = -move[1]

            # Make saw move and change values on sawMap
            self.sawMap[saw["y"]][saw["x"]] -= 1
            saw["y"] += move[0]
            saw["x"] += move[1]
            self.sawMap[saw["y"]][saw["x"]] += 1           


    def move(self, move):
        self.bot[0] += move[0]
        self.bot[1] += move[1]
        self.lives -= self.sawMap[self.bot[0]][self.bot[1]]

    def undo_move(self, move):
        self.lives += self.sawMap[self.bot[0]][self.bot[1]]
        self.bot[0] -= move[0]
        self.bot[1] -= move[1]


    def legalMove(self,row, col,move):
        row = row + move[0]
        col = col + move[1]
        h = self.height - 1
        w = self.width - 1
        if row < 0 or col < 0 or row > h or col > w or self.map[row][col] == False:
            return False
        return True     

    def valueMove(self, row, col, move):
        cost = 1 # Vsaka poteza sama po sebi je vredna 1
        y = row + move[0]
        x = col + move[1]
        h = self.height - 1
        w = self.width - 1
        
        # Ce je na robu, zvisaj ceno
        # Ce je na zagi, se bolj zvisaj ceno
    
        if(y == 0 or y == h or x == 0 or x == w):
            cost = cost + 5

        #e = (self.sawMap[y][x] - self.lives)
        cost = cost + self.sawMap[y][x]*50

        return cost

    def generate_moves(self):
        row, col = self.bot
        moves = []

        # Premaknemo se lahko levo, desno, gor, dol, stoj
        move = [0,-1] # Levo
        if self.legalMove(row,col,move):
            cost = self.valueMove(row,col,move)
            if cost < 50:
                moves.append( (move, cost) )

        move = [0,1] # Desno
        if self.legalMove(row,col,move):
            cost = self.valueMove(row,col,move)
            if cost < 50:
                moves.append((move,cost))

        move = [1, 0] # Gor
        if self.legalMove(row,col,move):
            cost = self.valueMove(row,col,move)
            if cost < 50:
                moves.append( (move, cost) )

        move = [-1,0] # Dol
        if self.legalMove(row,col,move):
            cost = self.valueMove(row,col,move)
            if cost < 50:
                moves.append( (move, cost) )
        
        move = [0,0] # Stop
        cost = self.valueMove(row,col,move) 
        if cost < 50:
            moves.append( (move, cost) )

        return moves

    def getSawMove(self,saw):
        d = saw["direction"]
        if d == "UP_LEFT":
            return [1,-1]
        if d == "UP_RIGHT":
            return [1,1]
        if d == "DOWN_LEFT":
            return [-1,-1]
        return [-1,1]
    

    def sawChangeDirection(self,saw):
        d = saw["direction"]
        y = saw["y"]
        x = saw["x"]
        h = self.height-1
        w = self.width-1

        
        if d == "UP_LEFT" and y==h and x==0: #Top Left Corner
            return "DOWN_RIGHT"
        if d == "UP_LEFT" and y==h: # Top Edge
            return "DOWN_LEFT"
        if d == "UP_LEFT": # Left edge remains
            return "UP_RIGHT"


        if d == "UP_RIGHT" and y==h and x==w: #Top Right Corner
            return "DOWN_LEFT"
        if d == "UP_RIGHT" and y==h: # Top Edge
            return "DOWN_RIGHT"
        if d == "UP_RIGHT": # Right edge remains
            return "UP_LEFT"

        if d == "DOWN_LEFT" and y==0 and x==0: #Bottom Left Corner
            return "UP_RIGHT"
        if d == "DOWN_LEFT" and y==0: # Bottom Edge
            return "UP_LEFT"
        if d == "DOWN_LEFT": # Left edge remains
            return "DOWN_RIGHT"

        if d == "DOWN_RIGHT" and y==0 and x==w: #Bottom Right Corner
            return "UP_LEFT"
        if d == "DOWN_RIGHT" and y==0: # Bottom Edge
            return "UP_RIGHT"
        if d == "DOWN_RIGHT": # Left edge remains
            return "DOWN_LEFT"

    def sawReverseChangeDirection(self,saw):
        d = saw["direction"]
        y = saw["y"]
        x = saw["x"]
        h = self.height - 1
        w = self.width - 1

        if d == "UP_LEFT" and y==0 and x==w: #Bottom Right Corner
            return "DOWN_RIGHT"
        if d == "UP_LEFT" and y==0: # Bottom Edge
            return "DOWN_LEFT"
        if d == "UP_LEFT": # Right edge remains
            return "UP_RIGHT"

        if d == "UP_RIGHT" and y==0 and x==0: #Bottom Left Corner
            return "DOWN_LEFT"
        if d == "UP_RIGHT" and y==0: # Bottom Edge
            return "DOWN_RIGHT"
        if d == "UP_RIGHT": # Left edge remains
            return "UP_LEFT"

        if d == "DOWN_LEFT" and y==h and x==w: #Top Right Corner
            return "UP_RIGHT"
        if d == "DOWN_LEFT" and y==h: # Top Edge
            return "UP_LEFT"
        if d == "DOWN_LEFT": # Right edge remains
            return "DOWN_RIGHT"

        if d == "DOWN_RIGHT" and y==h and x==0: #Top Left Corner
            return "UP_LEFT"
        if d == "DOWN_RIGHT" and y==h: # Top Edge
            return "UP_RIGHT"
        if d == "DOWN_RIGHT": # Left edge remains
            return "DOWN_LEFT"

      

    def ID(self):
        return tuple(self.bot)

    def evaluate(self):
        return (abs(self.bot[0]-self.target[0]) + abs(self.bot[1]-self.target[1]))
