import heapq
import collections

def A(pos, limit = 500):
    # A* best-first heuristic search
    # cand_paths: a priority queue of candidate paths;
    # visited: dictionary of already generated nodes and best g-values (and f-values) seen to reach them (thus no duplicates!)
    
    cand_paths = [(pos.evaluate(), [], pos.ID())]
    visited = { pos.ID(): (0, pos.evaluate()) }
    ix = 1

    numOfMoved = 0

    while cand_paths:
        if ix > limit: 
            val, path, id = heapq.heappop(cand_paths)
            return path     # limit on expanded nodes reached

        ix += 1

        val, path, id = heapq.heappop(cand_paths)
        
        # Prevrti zage naprej ali nazaj glede na dolzino trenutne poti
        if(len(path) > numOfMoved):
            moveSawsForward(len(path) - numOfMoved,pos)
            numOfMoved = len(path)
        elif (len(path) < numOfMoved):
            moveSawsBack(numOfMoved - len(path), pos)
            numOfMoved = len(path)


        execute_sequence(pos, path)

        if pos.solved():
            if pos.sawMap[pos.bot[0]][pos.bot[1]]>0:
                undo_sequence(pos,path)
                continue
            return path

        if visited[id][1] < val:
            undo_sequence(pos, path)
            continue

        # generate candidates
        pos.moveSaws()
        numOfMoved += 1

        for move, cost in pos.generate_moves():    
            pos.move(move) 
            new_id = pos.ID()
            if new_id not in visited or visited[new_id][0] > visited[id][0] + cost:
                # if a better path to state is found, add it to candidates
                visited[new_id] = (visited[id][0] + cost, visited[id][0] + cost + pos.evaluate())
                heapq.heappush(cand_paths, (visited[new_id][1], path + [(move, cost)], new_id))
            pos.undo_move(move)
        undo_sequence(pos, path)

    return None


def execute_sequence(pos, sequence):
    for move, cost in sequence:
        pos.move(move)

def undo_sequence(pos, sequence):
    for move, cost in reversed(sequence):
        pos.undo_move(move)

def moveSawsForward(i,pos):
    for j in range(i):
        pos.moveSaws()
def moveSawsBack(i,pos):
    for j in range(i):
        pos.undoMoveSaws()