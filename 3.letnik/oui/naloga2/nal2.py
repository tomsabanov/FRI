import pandas as pd  
import matplotlib.pyplot as plt
import numpy as np

from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split 
from sklearn.neighbors import KNeighborsRegressor
from sklearn.datasets import load_iris 
from sklearn.model_selection import cross_val_score
from sklearn.cluster import AgglomerativeClustering, KMeans
from scipy.cluster.hierarchy import dendrogram, linkage

def naloga_1():
    data = pd.read_csv("reg/75.csv",usecols=["X3","Y"])
    print(data.corr())

def naloga_2():
    X = pd.read_csv("reg/141.csv")
    Y = X[X.columns[-1]]
    X = X[X.columns[0:-1]]

    reg = LinearRegression().fit(X, Y)

    print(reg.coef_)

def naloga_3():
    X = pd.read_csv("reg/146.csv")
    Y = X[X.columns[-1]]
    X = X[X.columns[0:-1]]
    reg = LinearRegression().fit(X,Y)
    print(reg.coef_)

def naloga_4():
    X = pd.read_csv("reg/100.csv")
    X = X.head(49)

    Y = X[X.columns[-1]]
    X = X[X.columns[0:-1]]

    reg = LinearRegression()
    reg.fit(X,Y)
    err = mean_squared_error(Y, reg.predict(X))

    print(X)
    print(Y)
    print(err)

def naloga_5():
    X = pd.read_csv("reg/122.csv")

    X_test = X[52:]
    Y_test = X_test[X_test.columns[-1]]
    X_test = X_test[X_test.columns[0:-1]]

    X_train = X[0:52]
    Y_train = X_train[X_train.columns[-1]]
    X_train = X_train[X_train.columns[0:-1]]


    reg = LinearRegression()
    reg.fit(X_train,Y_train)

    err = mean_squared_error(Y_test, reg.predict(X_test))

    print(err)
    

def naloga_6():
    X = pd.read_csv("reg/159.csv")

    X_train = X[0:33]
    Y_train = X_train[X_train.columns[-1]]
    X_train = X_train[X_train.columns[0:-1]]

    X_test = X[33:]
    Y_test = X_test[X_test.columns[-1]]
    X_test = X_test[X_test.columns[0:-1]]


    k_scores = []
    k_range = range(1, 29)
    for k in k_range:
        knn = KNeighborsRegressor(n_neighbors=k)
        scores = cross_val_score(knn, X_train, Y_train, cv=10)
        k_scores.append(scores.mean())

    print(k_scores)
    m = max(k_scores)
    optimal = k_scores.index(m) + 1
    print(m)
    print(optimal)

    plt.plot(k_scores)
    plt.plot(k_range, k_scores)
    plt.xlabel('Value of K for KNN')
    plt.ylabel('Cross-Validated Accuracy')
    plt.show()



def naloga_7():
    X = pd.read_csv("reg/176.csv")
    X_train = X[0:37]
    Y_train = X_train[X_train.columns[-1]]
    X_train = X_train[X_train.columns[0:-1]]

    X_test = X[37:]
    Y_test = X_test[X_test.columns[-1]]
    X_test = X_test[X_test.columns[0:-1]]


    k_scores = []
    k_range = range(1, 31)
    for k in k_range:
        knn = KNeighborsRegressor(n_neighbors=k)
        scores = cross_val_score(knn, X_train, Y_train, cv=10)
        k_scores.append(scores.mean())

    #print(k_scores)
    m = max(k_scores)
    optimal_k = k_scores.index(m) + 1
    print(optimal_k)

    model = KNeighborsRegressor(n_neighbors = optimal_k)
    model.fit(X_train, Y_train)  #fit the model
    pred=model.predict(X_test) #make prediction on test set
    error = (mean_squared_error(Y_test,pred)) #calculate mse
    print('MSE value for k= ' , optimal_k , 'is:', error)


def naloga_8():
    X = pd.read_csv("clu/74.csv")


    #linked = linkage(X, 'complete')
    #labelList = range(1, 36)

    print(X)

    #plt.figure(figsize=(10, 7))
    #dendrogram(linked,
    #            orientation='top',
    #            labels=labelList,
    #            distance_sort='descending',
    #            show_leaf_counts=True)
    #plt.show()

    cluster = AgglomerativeClustering(n_clusters=2, affinity='manhattan', linkage='complete')
    c=cluster.fit_predict(X)
    print(c)
    print(c.size - sum(c))
    print(sum(c))




def naloga_9():
    X = pd.read_csv("clu/151.csv")
    print(X)


    startpts=np.array([ [92,59], [28,29]], np.float64)

    centroids= KMeans(n_clusters=2, init=startpts, n_init=1, max_iter=100)
    centroids.fit(X)
    arr=centroids.cluster_centers_
    
    labels = centroids.labels_
    print(labels)
    print(labels.size - sum(labels))
    print(sum(labels))


def naloga_10():
    X = pd.read_csv("clu/44.csv")
    print(X)


    startpts=np.array([ [-58,20], [-95,-65]], np.float64)

    centroids= KMeans(n_clusters=2, init=startpts, n_init=1, max_iter=100)
    centroids.fit(X)
    arr=centroids.cluster_centers_
    
    print(arr)

naloga_10()

