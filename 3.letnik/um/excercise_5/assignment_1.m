function [] = assignment_1()
    

    %b();
    %c();
    d();
    %e();
end


function [] = b()
    % in milimiters
    f = 2.5;
    T = 120;
    
    pz = linspace(1,120,12);
    
    d = (f * T) ./ pz;
    
    plot(pz,d,'Color',[0,0.7,0.9]);
    xlabel('pz');
    ylabel('d');
    % Blizji kot je predmet, vecji bo disparity
    
end

function [] = c()
    % All values are in milimiters
    
    f = 2.5;
    T = 120;
    
    % camera resolution
    
    h = 648;
    w = 488;
    
    pw = 0.001 * 7.4; 
    
    Px_l = 550;
    Px_r = 300;
    
    
    % What is the depth of the object?
    % disparity = x1 - x2;
    d = (Px_l - Px_r) * pw;
    pz = (f * T)/d;
    
    pz/10 % 16.21 cm
    
    Px_r = 540;
    d = (Px_l - Px_r) * pw;
    pz = (f * T)/d;
    pz/10 % 405 cm
end

function [] = d()
    A = imread("./disparity/office2_right.png");
    B = imread("./disparity/office2_left.png");
    
    %[h,w] = size(A)

    %d = disparityBM(A,B);
    d = disparity(A,B,[5 5],50);
    
    %https://sites.google.com/site/georgeevangelidis/encc
    % TUDI ZA e!!!!!!!
        
    figure;
    imagesc(d)
    
end

function [] = e()
    A = imread("./disparity/office2_right.png");
    B = imread("./disparity/office2_left.png");
    
    %[h,w] = size(A)

    %d = disparityBM(A,B);
    d1 = disparity(A,B,[3 3],200);
    d2 = disparity(A,B,[3 3],200);
    

end


