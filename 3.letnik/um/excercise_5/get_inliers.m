function [x1in x2in, x1out, x2out] = get_inliers(F,X1,X2,eps)
    % x and x' are inliers if the distance between x and 
    % the epipolar line F' * x' as well as the other way around
    % is lower than eps
      
    [h,w] = size(X1);
    
    x1in = zeros(h,w);
    x2in = zeros(h,w);
    
    x1out = zeros(h,w);
    x2out = zeros(h,2);
    
    ind = 1;
    indo = 1;
    
    for i=1:w
        x1 = X1(:,i);
        x2 = X2(:,i);        
        
        [d,d1,d2] = reprojection_error(x1,x2,F);

        if(d<=eps)
            x1in(:,ind) = x1;
            x2in(:,ind) = x2;
            ind = ind + 1;
        else
            x1out(:,indo) = x1;
            x2out(:,indo) = x2;
            indo = indo + 1;        
        end 
    end
    
    x1in = x1in(:,1:(ind-1));
    x2in = x2in(:,1:(ind-1));
    
    x1out = x1out(:,1:(indo-1));
    x2out = x2out(:,1:(indo-1));
end