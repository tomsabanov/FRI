function [indices, distances] = find(D1, D2)
        
    [h1,w1] = size(D1);
    [h2, w2] = size(D2);
    
    indices = zeros(h1, 1);
    distances = zeros(h1, 1);
    
    for i=1:h1
        H1 = D1(i,:);
        
        Min = zeros(1,2); % first el - distance, second el - index of D2
        Min(1) = realmax('single');
        Min(2) = -1;
        for j=1:h2
            H2 = D2(j,:);
            d = sqrt( 0.5 * sum( (sqrt(H2) - sqrt(H1)) .^ 2));
            
            
            if (d <= Min(1))
                Min(1) = d;
                Min(2) = j;
            end
        end
        indices(i) = Min(2);
        distances(i) = Min(1);
    end

end