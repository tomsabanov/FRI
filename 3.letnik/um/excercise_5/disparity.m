function D = disparity(X,Y,patchSize,maxDisp)
    % e)
    X = medfilt2(X,patchSize);
    Y = medfilt2(Y,patchSize);

    X = double(X);
    Y = double(Y);
    
    [h,w] = size(X);
    D = zeros(h,w);
    
    
    hH = floor(patchSize(1)/2);
    hW = floor(patchSize(2)/2);
    
    score = -5 * ones(1,w);
    
    for i=1+hH:h-hH
       
        for j=1+hW:w-hW
           window = X(i-hH:i+hH,j-hW:j+hW);
           norm_window = normalize(window);
           
           for k=j:-1:max(1+hW,j-maxDisp)
              patch = Y(i-hH:i+hH, k-hW:k+hW);
              patch_norm = normalize(patch);
              score(k) = sum(sum(norm_window .* patch_norm));
           end
           
           [match,idx] = max(score);
           
           D(i,j) = abs(j-idx);
           
           score = -1 .* ones(1,w);
         
        end
    end
   
end

function y = normalize(x)
    d = x - mean2(x);   
    v = d.^2;
    y = d./sqrt(sum(v(:)));
end