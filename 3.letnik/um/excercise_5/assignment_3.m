function [] = assignment_3()

    a();
end

function [] = a()

    A = readmatrix("./epipolar/house_points.txt");
    x1 = A(:,1);
    y1 = A(:,2);
    
    x2 = A(:,3);
    y2 = A(:,4);
    
    
    L = imread("./epipolar/house1.jpg");
    R = imread("./epipolar/house2.jpg");
    
    subplot(1,3,1);
    imagesc(L); hold on;
    plot(x1,y1,'r*');

    subplot(1,3,2);
    imagesc(R); hold on;
    plot(x2,y2,'r*');
    
    C1 = readmatrix("./epipolar/house1_camera.txt");
    C2 = readmatrix("./epipolar/house2_camera.txt");
    
    
    P1 = [x1';y1';ones(1,length(x1))];
    P2 = [x2';y2';ones(1,length(x1))];
    
    X = triangulate(P1,P2,C1,C2);
    
    subplot(1,3,3);
    plot3(X(1,:), X(3,:), X(2,:),'r*');
    xlabel('X')
    ylabel('Y')
    zlabel('Z')
    grid on;
    
end

