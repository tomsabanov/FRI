function Hn = RansacHomography(x1,y1,x2,y2)

    eps = 0.9;
    p = 0.9;
    N = length(x1);

    k = 300; % no. of iterations
    
    
    larg_n = 0; % largest number of inliers
    
    P1 = ones(3,N);
    P1(1:2,:) = [x1';y1'];
        
    P2 = ones(3,N);
    P2(1:2,:) = [x2';y2'];
    
    
    for i=1:k
        % Randomly select minimal set of point matches to estimate a model
        p = randperm(N,8);
        
        X1 = x1(p); Y1 = y1(p);
        X2 = x2(p); Y2 = y2(p);
        
        
        
        % Estimate homography
        H = estimate_homography(X1,Y1,X2,Y2);
        
        % Projection error
        %T1 = H * P1;
        %T1(1:2,:) = T1(1:2,:) ./ T1(3,:);       
        %error1 = sum((T1(1:2,:) - P2(1:2,:)).^2,2).^0.5;
        
        %T2 = H * P2;
        %T2(1:2,:) = T2(1:2,:) ./ T2(3,:);       
        %error2 = sum((T2(1:2,:) - P1(1:2,:)).^2,2).^0.5;
 
        %error = (error1 + error2)/2;
        
        % Determine the inliers based on the projection error
        [x1in,x2in, x1out,x2out] = get_inliers(H,P1,P2,eps);
        
        %If percentage of inliers is big enough (>=m), calculate new
        %fundamental matrix
        n = size(x1in,2); % Number of inliers
        if(n > larg_n)
           larg_n = n;
           X1t = x1in(1,:);
           Y1t = x1in(2,:);
           X2t = x2in(1,:);
           Y2t = x2in(2,:);
           Hn = estimate_homography(X1t,Y1t,X2t,Y2t);
        end
        
    end

end