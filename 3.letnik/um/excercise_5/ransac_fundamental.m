function [Fn, e1n, e2n, x1n, x2n,x1no,x2no] = ransac_fundamental(x1,x2,eps,k)
% Input:
% x1, x2 : 3xN matrix of N homogeneous points in 2D space
% eps : threshold for inliers
% k : number of iterations
% Output:
% F : 3x3 fundamental matrix: x2'* F * x1 = 0
% e1 : epipole in image 1: F * e1 = 0
% e2 : epipole in image 2: F'* e2 = 0
% x1, x2 : 3xNi matrix of Ni homogeneous inlier points

    [h,w] = size(x1);
    N = w; % Numbers of matched pairs
    
    larg_n = 0; % largest number of inliers
    
    for i=1:k
        % Randomly select minimal set of point matches to estimate a model
        p = randperm(N,8);
        X1 = x1(:,p);
        X2 = x2(:,p);
        
        % Estimate fundamental matrix using the selected sub-set
        [F,e1,e2] = fundemental_matrix(X1,X2);
        
        % Determine the inliers for the estimated fundamental matrix
        [x1in,x2in, x1out,x2out] = get_inliers(F,x1,x2,eps);
        
        %If percentage of inliers is big enough (>=m), calculate new
        %fundamental matrix
        n = size(x1in,2); % Number of inliers
        if(n > larg_n)
           larg_n = n;
           x1n = x1in;
           x2n = x2in;
           [Fn,e1n,e2n] = fundemental_matrix(x1n,x2n);
           x1no = x1out;
           x2no = x2out;
        end
        
    end
    
end