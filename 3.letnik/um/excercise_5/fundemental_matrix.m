function [F, e1, e2] = fundemental_matrix(x1,x2)
    
    % we transform the points
    [x1, T1] = normalize_points(x1);
    [x2, T2] = normalize_points(x2);

    [h,w] = size(x1);
    N = w;
    
    % Construct matrix A
    A = zeros(N, 9);
    for i=1:N
        A(i,:) = [x1(1,i) * x2(1,i), x1(1,i) * x2(2,i), x1(1,i), x1(2,i) * x2(1,i), x1(2,i)*x2(2,i),x1(2,i), x2(1,i), x2(2,i), 1];
    end
    
    
    [U,S,V] = svd(A);
    
    Ft = reshape(V(:,9), [3,3]);
    
    [U,S,V] = svd(Ft);
    S(3,3) = 0;
    
    F = U * S * V';
    
    F = T2' * F * T1;
    
    [U,S,V] = svd(F);
    e1 = V(:,3)/V(3,3);
    e2 = U(:,3)/U(3,3);
end