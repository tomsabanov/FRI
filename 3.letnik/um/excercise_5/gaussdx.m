function [g] = gaussdx(sigma)
    x = -round(3.0 * sigma):round(3.0 * sigma);
    g = -(1.0/(sqrt(2 * pi)*sigma^3)) .* x .* exp(-(x.^2)/(2 * sigma^2));
    
    val = sum(abs(g));
    g = g ./ val;
      
end