function [] = assignment_2()

    %b();
    %c();
    %e(10,200,2);
    
    %f();
    g();
end

function [] = b()
    P = readmatrix("./epipolar/house_points.txt");
    
    P1 = P(:,1:2)';
    P2 = P(:,3:4)';
    % Transform P to two homogonous 3xN matrices
    
    X1 = ones(3,size(P1,2));
    X2 = ones(3,size(P1,2));
    
    X1(1:2,:) = P1;
    X2(1:2,:) = P2;
    
    [F, e1, e2] = fundemental_matrix(X1,X2); 
    Fcorrect = readmatrix("./epipolar/house_fundamental");
    
 
       
    H_l = imread("./epipolar/house1.jpg");
    H_r = imread("./epipolar/house2.jpg");
    [h,w] = size(H_r);
    
    p1 = [85,233,1]'; % Point in the first image
    
    % Determine epipolar line for p1 in the second image
    l2 = F * p1;
    
    imagesc(H_r); hold on;
    draw_line(l2,w, h, 'g');
    
    p2 = [67,219];
    plot(p2(1),p2(2),'r*');
    
end

function [] = c()
   
    % Test 2
    P = readmatrix("./epipolar/house_points.txt");
    
    P1 = P(:,1:2)';
    P2 = P(:,3:4)';
    % Transform P to two homogonous 3xN matrices
    X1 = ones(3,size(P1,2));
    X2 = ones(3,size(P1,2));
    X1(1:2,:) = P1;
    X2(1:2,:) = P2;
    
    [F, e1, e2] = fundemental_matrix(X1,X2); 
    s = 0;
    for i=1:size(X1,2)
        [d,d1,d2] = reprojection_error(X1(:,i),X2(:,i),F);
        s = s + d;
    end
    s/size(X1,2)
  
    
    % Test 1
    p1 = [85,233,1]';
    p2 = [67,219,1]';
    [d, d1, d2] = reprojection_error(p1,p2,F)
    d
    
    %[x2in,x1in] = get_inliers(F,X1,X2,0.3);

end

function [] = e(eps,k,selected)
    P = readmatrix("./epipolar/house_matches.txt");
    
    P1 = P(:,1:2)';
    P2 = P(:,3:4)';
    X1 = ones(3,size(P1,2));
    X2 = ones(3,size(P1,2));
    
    X1(1:2,:) = P1;
    X2(1:2,:) = P2;
    
    H_l = (imread("./epipolar/house1.jpg"));
    H_r = (imread("./epipolar/house2.jpg"));
    
    
    [F,e1,e2,x1in,x2in,x1out,x2out] = ransac_fundamental(X1,X2,eps,k);
    
    subplot(1,2,1);
    imagesc(H_l); hold on;
    title("Outliers(red), inliers(blue), selected(green)");
    plot(x1in(1,:),x1in(2,:),'b*'); % inliers
    plot(x1out(1,:),x1out(2,:),'r*'); % outliers

    sel = x1in(:,selected); % selected point (inlier)
    plot(sel(1),sel(2),'g*');
    
    perc = size(x2in,2)/size(X2,2);
    err = size(x2out,2)/size(X1,2); % KOLIKO JE TA NAPAKA????? Na assignmentu je neka cudna cifra!!!!

    subplot(1,2,2);
    imagesc(H_l); hold on;
    title(['Inliers: ' num2str(perc) '% ,Error: ' num2str(err) '%']);

    plot(x2in(1,:),x2in(2,:),'b*'); % inliers
    plot(x2out(1,:),x2out(2,:),'r*'); % outliers
     
    % Determine epipolar line for sel in the second image
    [h,w] = size(H_r);
    l = F * sel;
    draw_line(l,w, h, 'g');
    
    sel = x2in(:,selected); % selected point (inlier) in second image
    plot(sel(1),sel(2),'g*');
end


function [] = f()

    H_l = imread("./epipolar/house1.jpg");
    H_r = imread("./epipolar/house2.jpg");
    
    [M,B] = find_matches(rgb2gray(H_l), rgb2gray(H_r),31,3,16,15,100);
    displaymatches(H_l,M(:,1),M(:,2),H_r,M(:,3),M(:,4));

    
    X1 = ones(3,size(M,1));
    X2 = ones(3,size(M,1));
    
    P1 = [M(:,1), M(:,2)]';
    P2 = [M(:,3),M(:,4)]';
    
    X1(1:2,:) = P1;
    X2(1:2,:) = P2;
    
    eps = 0.5;
    k = 500;
    selected = 5;
    
    
    [F,e1,e2,x1in,x2in,x1out,x2out] = ransac_fundamental(X1,X2,eps,k);
    
    subplot(1,2,1);
    imagesc(H_l); hold on;
    title("Outliers(red), inliers(blue), selected(green)");
    plot(x1in(1,:),x1in(2,:),'b*'); % inliers
    plot(x1out(1,:),x1out(2,:),'r*'); % outliers

    sel = x1in(:,selected); % selected point (inlier)
    plot(sel(1),sel(2),'g*');
    
    perc = size(x2in,2)/size(X2,2);
    err = size(x2out,2)/size(X1,2); % KOLIKO JE TA NAPAKA????? Na assignmentu je neka cudna cifra!!!!

    subplot(1,2,2);
    imagesc(H_l); hold on;
    title(['Inliers: ' num2str(perc) '% ,Error: ' num2str(err) '%']);

    plot(x2in(1,:),x2in(2,:),'b*'); % inliers
    plot(x2out(1,:),x2out(2,:),'r*'); % outliers
     
    % Determine epipolar line for sel in the second image
    [h,w] = size(H_r);
    l = F * sel;
    draw_line(l,w, h, 'g');
    
    sel = x2in(:,selected); % selected point (inlier) in second image
    plot(sel(1),sel(2),'g*');
    
end


function [] = g()

    colormap gray;
    A1=rgb2gray(imread("panorama/panorama1.jpg"));
    A2=rgb2gray(imread("panorama/panorama2.jpg"));
    A3=rgb2gray(imread("panorama/panorama3.jpg"));
    A4=rgb2gray(imread("panorama/panorama4.jpg"));
    A5=rgb2gray(imread("panorama/panorama5.jpg"));
    A6=rgb2gray(imread("panorama/panorama6.jpg"));
    
    %subplot(1,6,1); imagesc(A1);
    %subplot(1,6,2); imagesc(A2);
    %subplot(1,6,3); imagesc(A3);
    %subplot(1,6,4); imagesc(A4);
    %subplot(1,6,5); imagesc(A5);
    %subplot(1,6,6); imagesc(A6);
    

    P = find_matches(A1, A2, 81, 3, 16, 1,16);
    H = RansacHomography(P(:,3), P(:,4), P(:,1), P(:,2));
    IT = merge_homography(A1,A2,H);
    displaymatches(A1, P(:,1), P(:,2), A2, P(:,3), P(:,4));
    imagesc(IT)
     
    P = find_matches(A2, A3, 81,3, 16, 1,16);
    H = RansacHomography(P(:,3), P(:,4), P(:,1), P(:,2));
    %displaymatches(A2, P(:,1), P(:,2), A3, P(:,3), P(:,4));
    IT = merge_homography(A2,A3,H);
    %imagesc(IT);
    
    %%%%%%%%%%%%%%%
    
    P = find_matches(A3, A4, 81, 3, 16, 1, 16);
    H1 = RansacHomography(P(:,3), P(:,4), P(:,1), P(:,2));
    %displaymatches(A3, P(:,1), P(:,2), A4, P(:,3), P(:,4));
    IT4 = merge_homography(A3,A4,H1);
    %imagesc(IT4);
    
    
    P = find_matches(A4, A5, 81, 3, 16, 1, 16);
    H2 = RansacHomography(P(:,3), P(:,4), P(:,1), P(:,2));
    %displaymatches(A3, P(:,1), P(:,2), A4, P(:,3), P(:,4));
    %IT5 = merge_homography(A4,A5,H2);
    %imagesc(IT5);
    
    H = H1 * H2;
    IT = merge_homography(IT4,A5,H);
    imagesc(IT);
    
    %%%%%%%%%%%%%%%%%
    
    P = find_matches(A5, A6, 81, 3, 16, 1, 16);
    H2 = RansacHomography(P(:,3), P(:,4), P(:,1), P(:,2));
    %displaymatches(A5, P(:,1), P(:,2), A6, P(:,3), P(:,4));
    IT5 = merge_homography(A5,A6,H2);
    %figure;colormap gray;imagesc(IT5);




end








