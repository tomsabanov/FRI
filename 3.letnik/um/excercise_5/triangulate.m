function X = triangulate(pts_1, pts_2, P_1, P_2)
% Input:
% pts_1 ... 3xN left camera ponts in homogenous cordinates
% pts_2 ... 3xN right camera points in homogenous coordinates
% P_1 ... 3x4 calibration matrix of the left camera
% P_2 ... 3x4 calibration matrix of the right camera
% Output:
% X ... 4xN vector of 3D points in homogenous coordinates

    X = [];
    for i = 1:size(pts_1,2)
        a1 = cross_form(pts_1(:,i));
        a2 = cross_form(pts_2(:,i));
        c1 = a1 * P_1;
        c2 = a2 * P_2;
        A = [ c1(1:2,:); c2(1:2,:) ];
        
        [U,S,V] = svd(A);
        x = V(:,size(V,2));
        x = x ./ x(length(x));
        
        X = [X, x];
    end


end

function Cx = cross_form(a)
Cx = [ 0, -a(3), a(2); a(3), 0, -a(1); -a(2), a(1), 0];
end