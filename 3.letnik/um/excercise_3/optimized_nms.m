function Imax = optimized_nms(Imag, Idir)
    [h,w] = size(Imag);
    Imax = zeros(h,w);
        
    offx = [-1 -1 0 1 1 1 0 -1 -1];
    offy = [0 -1 -1 -1 0 1 1 1 0];
    DIR = round(((Idir + pi) ./ pi) .* 4) + 1;
    
    %A = zeros(h+2,w+2);
    %A(2:end-1,2:end-1) = Imag;
    %B = zeros(h+2, w+2);
    %B(2:end-1, 2:end-1) = DIR;
    A = Imag;
    B = DIR;
    
    R = circshift(A,[0 1]);
    L = circshift(A,[0 -1]);
    
    U = circshift(A,[-1 0]);
    D = circshift(A,[1 0]);
    
    UL = circshift(A,[-1 -1]);
    DR = circshift(A,[1 1]); 
    
    UR = circshift(A,[-1 1]);
    DL = circshift(A,[1 -1]);
        
    index = A>=R & A>=L & (B==1 | B==5 | B==9);
    A1 = A.*index; 
    
    index = A>=U & A>=D & (B==3 | B==7);
    A2 = A.*index;
        
    index = A>=UL & A>=DR & (B==2 | B==6);
    A3 = A.*index;
    
    index = A>=UR & A>=D & (B==4 | B==8); 
    A4 = A.*index;
        
    A = A1 + A2 + A3 + A4;
    
    %Imax = A(2:end-1,2:end-1);
    Imax = A;
end
