 function [] = assignment_1()
    
    %b();
    %c();
    d(25,'chi2',1);
        
    %B = reshape(uint8(1:25),[5 5])'
    %B = im2col(B, [2,2], 'distinct')
    
end

function [] = b()
    colormap gray;
    
    impulse = zeros(25,25); 
    impulse(13,13) = 255;
    
    sigma = 6.0;
    G = gauss(sigma);
    D = gaussdx(sigma);
    
    subplot(2,3,1);
    imagesc(impulse);
    title('Impulse');
    
   GGT = conv2(impulse, G);
   GGT = conv2(GGT, G');
   subplot(2,3,4);
   imagesc(GGT);
   title("G,Gt");
   
   GDT = conv2(impulse, D');
   GDT = conv2(GDT, G); % vrstni red operacije  - komutativnost
   subplot(2,3,2);
   imagesc(GDT);
   title("G,Dt");
   
   DGT = conv2(impulse, D);
   DGT = conv2(DGT, G');
   subplot(2,3,3);
   imagesc(DGT);
   title("D,Gt");
   
   GTD = conv2(impulse, G');
   GTD = conv2(GTD, D);
   subplot(2,3,5);
   imagesc(GTD);
   title("Gt,D");
   
   DTG = conv2(impulse, D');
   DTG = conv2(DTG, G);
   subplot(2,3,6);
   imagesc(DTG);
   title("Dt,G");

end

function [] = c()
    colormap gray;
    A = rgb2gray(imread("./museum.jpg"));
    sigma = 1;
    
    
    [Ix, Iy] = image_derivatives(A, sigma);
    [Ixx,Iyy,Ixy] = image_derivatives2(A, sigma);
    
    [Imag, Idir] = gradient_magnitude(A, sigma);
    
    subplot(2,4,1);
    imagesc(A);
    title("Original");
    
    subplot(2,4,2);
    imagesc(Ix);
    title("Ix");
    
    subplot(2,4,3);
    imagesc(Iy);
    title("Iy");
    
    subplot(2,4,4);
    imagesc(Imag);
    title("Imag");
    
    subplot(2,4,5);
    imagesc(Ixx);
    title("Ixx");
    
    subplot(2,4,6);
    imagesc(Ixy);
    title("Ixy");
    
    subplot(2,4,7);
    imagesc(Iyy);
    title("Iyy");
    
    subplot(2,4,8);
    imagesc(Idir);
    title("Idir");
    
end


function d = compare_histograms(h1,h2, dist_name)
    switch dist_name
        case 'l2'
            d = sqrt( sum( (h1-h2).^2 ) );
        case 'chi2'
            d = sum( ((h1-h2).^2) ./ (h1 + h2 + 1e-10)) ./ 2;
        case 'hellinger'
            d = sqrt( sum( (sqrt(h1) - sqrt(h2)) .^ 2) ./ 2  );
        case 'intersect'
            d = 1 - sum( min(h1,h2)); 
        otherwise
            error('Unknown distance type!')' % Throw an exception
    end
end


function [] = d(selected, dist_name, sigma)
    %[histograms, files] = weighted(8,10);
    [histograms,files] = load_histogram_database('./db', 8,sigma);

    sel = histograms(selected, :);
    
    distances = zeros(size(histograms,1), 2);
    
    figure;
    for i = 1: size(histograms,1)
        d = compare_histograms(sel, histograms(i,:), dist_name);
        distances(i,1) = d;
        distances(i,2) = i;
    end
    
    Sorted = sortrows(distances);
    
    for i = 1:6
       I = imread(files{Sorted(i,2)}); 
       subplot(2,6,i);
       imshow(I);
       title(['Image ' num2str(Sorted(i,2))]);
       
       subplot(2,6, i + 6);
       hist = histograms(Sorted(i,2), :);
       plot(hist');
       title([ dist_name " = " num2str(Sorted(i,1)) ]);
    end

end

function [hist, files] = weighted(bins,lambda)
    [histograms,files] = load_histogram_database('./db', bins,1);

    H = double(zeros(bins,1));
    for i = 1:size(histograms,1)
       A = histograms(i,:);
       A = reshape(A,[bins,1]);
       H = H + A;
    end
    H = H/size(histograms,1);
    
    %  Probimo dobit weighting factor za vsak bin
    F = H(:);
    W = zeros(1, length(F));
    hist = histograms;
    for i=1:length(F)
         W(i) = exp(-lambda * F(i));
    end
    
    for j=1:size(histograms,1)
        for i=1:length(W)
           hist(j,i) = hist(j,i) * W(i);
        end
        H=hist(j,:);
        H = H/sum(H);
        hist(j,:) = H;
     end
   
end

