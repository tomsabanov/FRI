function [Ie1,Ie2] = findedges(I, sigma, theta)
    
    [Imag, Idir] = gradient_magnitude(I, sigma);

    Imag1 = nonmaxima_suppression_line(Imag, Idir);
    Imag2 = optimized_nms(Imag,Idir);
        
    % a)
    %Ie = Imag >= theta; 
    %razlika = sum(Imag1(:)) - sum(Imag2(:))
    
    Ie1 = hysteresis(I, Imag1);
    Ie2 = hysteresis(I, Imag2);
end
