function [Ixx, Iyy, Ixy] = image_derivatives2(I,sigma)
    G = gauss(sigma)
    D = gaussdx(sigma);
    
    [Ix, Iy] = image_derivatives(I,sigma);
    
    Iyy = conv2(Iy, G,'same');
    Iyy = conv2(Iyy, D','same');
    
    Ixx = conv2(Ix, G','same');
    Ixx = conv2(Ixx, D,'same');
    
    Ixy = conv2(I,D,'same');
    Ixy = conv2(Ixy, D','same');
end