function [] = assignment_3()

    %a()
    %b()
    %e()
    f()
    
end

function [] = a()
    bins_theta = 300;
    bins_rho = 300; % resolution of the accumulator array
    max_rho = 100; % Ussualy the diagonal of the image
    
    val_theta = (linspace(-90, 90, bins_theta) / 180) * pi; %Values of theta are known
    val_rho = linspace(-max_rho, max_rho, bins_rho);
    A = zeros(bins_rho, bins_theta);
    
    % for point at (50,90)
    
    x = 30;
    y = 60;
    rho = x * cos(val_theta) + y * sin(val_theta); % compute rho for all thetas
    bin_rho = round(((rho + max_rho) / (2 * max_rho)) * length(val_rho)); % Compute bins for rho
    
    for i = 1:bins_theta % Go over all the points
        if bin_rho(i) > 0 && bin_rho(i) <= bins_rho % Mandatory out of bounds check
            A(bin_rho(i),i) = A(bin_rho(i), i) + 1; % Increment theaccumulator cells
        end
    end
    
    imagesc(A); % Display the status of the accumulator

end

function [out_ro, out_theta, A] = hough_find_lines(Ie, bins_rho, bins_theta, threshold)

    [h, w] = size(Ie);
    
    max_rho = (sqrt(h^2 + w^2)); % Ussualy the diagonal of the image
    
    val_theta = (linspace(-90, 90, bins_theta) / 180) * pi; %Values of theta are known
    val_rho = linspace(-max_rho, max_rho, bins_rho);
    A = zeros(bins_rho, bins_theta);
            
    [y,x] = find(Ie == 1);

    for i = 1:size(x)
            rho = x(i) * cos(val_theta) + y(i) * sin(val_theta); % compute rho for all thetas
            bin_rho = round(((rho + max_rho) / (2 * max_rho)) * length(val_rho)); % Compute bins for rho

            for j = 1:bins_theta % Go over all the points
                if bin_rho(j) > 0 && bin_rho(j) <= bins_rho % Mandatory out of bounds check
                    A(bin_rho(j),j) = A(bin_rho(j), j) +1; % Increment theaccumulator cell                        
                end 
            end
    end
    

    [r,t] = find(A > threshold);
    out_ro = val_rho(r);
    out_theta = val_theta(t);  
end

function A = nonmaxima_suppression_box(A)
    [h, w] = size(A);
    
    for y = 1:h
        for x = 1:w
               pix = A(y,x);
               
               % check if it is local maxima
               y1 = y-1;
               y2 = y+1;
     
               x1 = x-1;
               x2 = x+1;
               
               x1 = max([1,x1]); x2 = min([w,x2]); % constrain coordinates
               y1 = max([1, y1]);y2 = min([h,y2]);
                         
               S = A((y1 : y2), (x1 : x2));
               
               m = max(S(:));
               [row col] = find(S == m);
               
               if pix < m
                   A(y,x) = 0;
               end
        end
    end
end






function [] = b()
    colormap jet;
    %colormap gray;
    
    E = zeros(100);
    E(10,10) = 1;
    E(20, 20) = 1;
    [out_rho, out_theta, E] = hough_find_lines(E, 300, 300, 1);
    subplot(1,3,1);
    imagesc(E);
    title("synthetic");
 
    B = rgb2gray(imread("./oneline.png"));
    %B = edge(B,'Canny');
    [B1,B2] = findedges(B, 1, 1);
    [out_rho, out_theta, B] = hough_find_lines(B1, 300, 300, 1);
    subplot(1,3,2);
    %B = nonmaxima_suppression_box(B);
    imagesc(B);
    title("oneline");

    C = rgb2gray(imread("./rectangle.png"));
    %C = edge(C, 'Canny');
    [C1, C2] = findedges(C, 1, 1);    
    [out_rho, out_theta, C] = hough_find_lines(C1, 300, 300, 10);
    %C = nonmaxima_suppression_box(C);

    subplot(1,3,3);
    imagesc(C);
    title("rectangle");

end

function [] = e()
    colormap jet;
    
    E = zeros(100);
    E(10,10) = 1;
    E(20, 20) = 1;
    [out_rho, out_theta, Ecc] = hough_find_lines(E, 300, 300, 1);
    subplot(1,3,1);
    hough_draw_lines(E, out_rho, out_theta)
    
    B = rgb2gray(imread("./oneline.png"));
    [B1,B2] = findedges(B, 1, 10);
    [rho, theta, Bcc] = hough_find_lines(B2, 300, 300,200);
    subplot(1,3,2);
    hough_draw_lines(B, rho, theta);
    
    C = rgb2gray(imread("./rectangle.png"));
    [C1,C2] = findedges(C, 1, 1);    
    [rho, theta, Ccc] = hough_find_lines(C2, 300, 300,200);
    %C = nonmaxima_suppression_box(C);
    subplot(1,3,3);
    hough_draw_lines(C, rho, theta);

end


function [out_ro, out_theta, A] = hough_find_lines_num(Ie, bins_rho, bins_theta, threshold,num)

    [h, w] = size(Ie);
    
    max_rho = (sqrt(h^2 + w^2)); % Ussualy the diagonal of the image
    
    val_theta = (linspace(-90, 90, bins_theta) / 180) * pi; %Values of theta are known
    val_rho = linspace(-max_rho, max_rho, bins_rho);
    A = zeros(bins_rho, bins_theta);
            
    [y,x] = find(Ie == 1);

    for i = 1:size(x)
            rho = x(i) * cos(val_theta) + y(i) * sin(val_theta); % compute rho for all thetas
            bin_rho = round(((rho + max_rho) / (2 * max_rho)) * length(val_rho)); % Compute bins for rho

            for j = 1:bins_theta % Go over all the points
                if bin_rho(j) > 0 && bin_rho(j) <= bins_rho % Mandatory out of bounds check
                    A(bin_rho(j),j) = A(bin_rho(j), j) +1; % Increment theaccumulator cell                        
                end 
            end
    end
    

    [r,t] = find(A > threshold);
    out_ro = val_rho(r);
    out_theta = val_theta(t);
    
    [B,I] = sort(A(:),'descend');
    [r c] = ind2sub(size(A),I(1:num));   
    out_ro = val_rho(r);
    out_theta = val_theta(c);
    
end
function [] = f()
    A_orig = rgb2gray(imread("./bricks.jpg"));
    A = findedges(A_orig,3,10);
    [rho, theta, Acc] = hough_find_lines_num(A, 300, 300,200,20);
    subplot(1,2,1);
    hough_draw_lines(A_orig, rho, theta);

    
    subplot(1,2,2);
    A_orig = rgb2gray(imread("./pier.jpg"));
    A = findedges(A_orig,3,10);
    [rho, theta, Acc] = hough_find_lines_num(A, 300, 300,200,20);
    hough_draw_lines(A_orig, rho, theta);
end

