function [histograms, files] = load_histogram_database(dir,bins, sigma)
    files = cell(30 * 4, 1); % Cell array to store filenames
    
    %initialize matrix for histograms
    histograms=zeros(30 * 4, bins);
    %calculate histogram for each image
    for i = 1:30; % iterate objects
        for j = 1:4 % iterate orientations
            image = (i-1) * 4 + j;
            files{image} = fullfile(dir, sprintf('object_%02d_%d.png',i,j));
            % Load image, extract histogram
            I = imread(files{image});
            I = rgb2gray(I);   
            
            H = calculateHistogram(I, sigma);
            
            H = H(:);
            histograms(image, :) = H;
        end
    end
end

function H = calculateHistogram(I,sigma)    
    step = 16;
    [Imag, Idir] = gradient_magnitude(I, sigma);
    Imag = im2col(Imag, [step, step], 'distinct');
    Idir = im2col(Idir, [step, step], 'distinct');
    
    H = zeros(1,8);
    
    [h,w] = size(Imag);
    

    for i=1:w
        % Vsak stolpec predstavlja eno celico v gridu    
        H1 = zeros(1,8);
        for j=1:h
            dir = Idir(j,i);
            
            idx = floor((dir + pi) / (2*pi) * 8);
            idx(idx == 0) = 1;
            H1(idx) = H1(idx) + Imag(j,i);     
        end
        H1 = H1 / 64;
        H = H + H1;
    end
        
    H = H / (sum(H));  
end