function Ie = hysteresis(I, Imag)
    
    [h,w] = size(I);
    T_Low = 0.1;
    T_High = 0.2;
   
    T_Low = T_Low * max(max(Imag));
    T_High = T_High * max(max(Imag));
    
    T_res = zeros (h,w);

    for i = 2  : h-1
        for j = 2 : w-1
            if (Imag(i, j) < T_Low)
                T_res(i, j) = 0;
            elseif (Imag(i, j) > T_High)
                T_res(i, j) = 1;
        elseif ( Imag(i+1,j)>T_High || Imag(i-1,j)>T_High || Imag(i,j+1)>T_High || Imag(i,j-1)>T_High || Imag(i-1, j-1)>T_High || Imag(i-1, j+1)>T_High || Imag(i+1, j+1)>T_High || Imag(i+1, j-1)>T_High)
                T_res(i,j) = 1;
            end;
        end;
    end;

    Ie = T_res;
    
    
    
    %Ie = hist(I, Imag);
end

function Ie = hist(I,Imag)

    L = bwlabel(Imag);
    Num = max(L(:));
    
    T_low = 17;
    T_high = 50;
    [h,w] = size(I);
    
    Ie = zeros(h,w);
    
    for i=1:Num
        [r,c] = find(L==i);
        rc = [r c]; % y x
        
        M = Imag(sub2ind(size(Imag),r,c));
        M_vec = M(:);
        l = length(M_vec);
                
        low = (M_vec >= T_low);
        high = (M_vec >= T_high);
        if((sum(low) >= l * 0.75)  && (sum(high) > 0))
            % vse vrednosti vecje od T_low,
            % vsaj ena vecja od T_High
            
            Ie(sub2ind(size(Imag),r,c)) = 1;
        else   
            Ie(sub2ind(size(Imag),r,c)) = 0;
        end
    end
    
end




