function [] = assignment_2()
    
    a();
    
    
    %A = rand(2)
    %[r,c] = find(A>0.5)
    %B = [r c];
    %A(sub2ind(size(A), r, c))
    
end


function [] = a()
    colormap gray;
    A = rgb2gray(imread("./museum.jpg"));
    
    theta = 20;
    sigma = 1;
    [Ie,Ie2] = findedges(A, sigma, theta);
    subplot(1,2,1);
    imagesc(Ie); 
    
    subplot(1,2,2);
    imagesc(Ie2);
    
    
    %BW = edge(A,'canny');
    %subplot(1,2,2);
    %imagesc(BW);
    
end

