function [Ix, Iy] = image_derivatives(I, sigma)
    G = gauss(sigma);
    D = gaussdx(sigma);
    
    Ix = conv2(I, G','same');
    Ix = conv2(Ix, D,'same');
    
    Iy = conv2(I,G,'same');
    Iy = conv2(Iy, D', 'same');

end