function [] = assignment_3()
    %a();
    %b();
    %c();
    %d();
    %e();
end

function [g,x] = gauss(sigma)
    x = -round(3.0*sigma):round(3.0*sigma);
    g = (1.0 /(sigma * sqrt(2*pi))) .* exp(-(x.^2)./(2*sigma^2));
    g = g / sum(g); % normalisation
end

function [Ig] = gaussfilter(I, sigma)
    [g,x] = gauss(sigma);
    
    Ib = conv2(I,g,'same');
    Ig = conv2(Ib, g', 'same');
end

% a)
function [] = a()
    A = rgb2gray(imread('./files/lena.png'));
    Icg=imnoise(A,'gaussian',0,0.01); % Gaussian noise
    
    figure;
    subplot(2,2,1); imshow(Icg); colormap gray;
    axis equal; axis tight; title('Gaussian noise');
    
    Ics = imnoise(A,'salt & pepper', 0.1); % Salt & pepper noise
    subplot(2,2,2); imshow(uint8(Ics)); colormap gray;
    axis equal; axis tight; title('Salt and pepper');
    
    Icg_b = gaussfilter(double(Icg),1);
    Ics_b = gaussfilter(double(Ics), 1);
    subplot(2,2,3); imshow(uint8(Icg_b)); colormap gray;
    axis equal; axis tight; title('Filtered');
    subplot(2,2,4); imshow(uint8(Ics_b)); colormap gray;
    axis equal; axis tight; title('Filtered');
    
    % Odgovor:
    % Gaussian noise se bolje odstrani, pri salt&pepper se vidi 
    % ostanke
end

% b)
function [] = b()
    A = rgb2gray(imread('./files/museum.jpg'));
    
    subplot(1,2,1); imshow(A);
    
    g = eye(3);
    g = (g .* rot90(g)).*2;
    g = g - (ones(3)./ 9)
    
    Is = conv2(A,g,'same');
    subplot(1,2,2); imshow(uint8(Is));
end

function [Ig] = simple_median(I,W)
    N = (W - 1) / 2;
    Ig = zeros(1, length(I));
    for i = N+1:length(I)-N
       i_left = max([1,i-N]);
       i_right = min([length(I), i + N]);
       vec = [I(i_left:i_right)];
       vec = sort(vec);
       
       Ig(i) = vec(N + 1);
    end
end
function [Ig] = median2D(I,W)
    N = (W-1)/2;
    sz = size(I);
    Ig = zeros(sz(1), sz(2));
    for i = N + 1: sz(1) - N
       i_left = max([1, i-N]);
       i_right = min([sz(1), i + N]);    
       for j = N + 1: sz(2) - N
           j_left = max([1, j-N]);
           j_right = min([sz(2), j + N]);
           list = I(i_left:i_right, j_left:j_right);
           list = sort(list(:));
           
           Ig(i,j) = list((length(list) - 1)/2 + 1);
       end
    end

end

function [] = c()
    x = [zeros(1,14), ones(1,11), zeros(1,15)]; % input signal
    xc = x; xc(11) = 5; xc(18) = 5; % Corrupted Signal
    figure;
    subplot(1,4,1); plot(x); axis([1,40,0,7]); title("Input");
    subplot(1,4,2); plot(xc); axis([1,40,0,7]); title("Corrupted");
    
    g = gauss(1);
    x_g = conv(xc, g, 'same');
    x_m = simple_median(xc,5);
    subplot(1,4,3); plot(x_g); axis([1,40,0,7]); title("Gauss");
    subplot(1,4,4); plot(x_m); axis([1,40,0,7]); title("Median");
end

function [] = d()
    A = rgb2gray(imread('./files/lena.png'));
    
    Icg=imnoise(A,'gaussian',0,0.01); % Gaussian noise
    figure;
    subplot(2,3,1); imshow(Icg); colormap gray;
    axis equal; axis tight; title('Gaussian noise');
    Icg_b = gaussfilter(double(Icg),1);
    subplot(2,3,2); imshow(uint8(Icg_b)); colormap gray;
    axis equal; axis tight; title('Gauss Filtered');
    Icg_m = median2D(Icg,3);
    subplot(2,3,3); imshow(uint8(Icg_m)); colormap gray;
    axis equal; axis tight; title('Median Filtered');
    
    Ics = imnoise(A,'salt & pepper', 0.1); % Salt & pepper noise
    subplot(2,3,4); imshow(uint8(Ics)); colormap gray;
    axis equal; axis tight; title('Salt and pepper');
    Ics_b = gaussfilter(double(Ics), 1);
    subplot(2,3,5); imshow(uint8(Ics_b)); colormap gray;
    axis equal; axis tight; title('Gauss Filtered');
    Ics_m = median2D(Ics,5);
    subplot(2,3,6); imshow(uint8(Ics_m)); colormap gray;
    axis equal; axis tight; title('Median Filtered');
    
end
function [I] = LoG(A, sigma)
    Af = A;
    Af(:,:,1) = gaussfilter(double(A(:,:,1)), sigma);
    Af(:,:,2) = gaussfilter(double(A(:,:,2)),sigma);
    Af(:,:,3) = gaussfilter(double(A(:,:,3)), sigma);
    
    I = A - Af;

end
function [] = e()
    A = imread('./files/cat1.jpg');
    B = imread('./files/cat2.jpg');
    
    sigma = 6;
    
    Af = A;
    Af(:,:,1) = gaussfilter(double(A(:,:,1)), sigma);
    Af(:,:,2) = gaussfilter(double(A(:,:,2)),sigma);
    Af(:,:,3) = gaussfilter(double(A(:,:,3)), sigma);
    
    Bf = LoG(B, sigma);
    
    subplot(1,3,1);
    imshow(Af);
    
    subplot(1,3,2);
    imshow(Bf);
    
    I = Bf + Af;
    subplot(1,3,3);
    imshow(I);
   
end