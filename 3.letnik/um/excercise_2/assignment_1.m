function [] = assignment_1()
    
    %d()
    %e("hellinger", 8,20);    
    %f("hellinger", 8,20);  
    %g(8, 30,15)
end

function [] = c_old()
    A = imread('./files/umbrellas.jpg');
    H1 = myhist3(A, 8);
    H2 = truly_myhist3(A,8);
    
    % we must compare H1 and H2 
    result = H1==H2
end

% b)
function [] = b()
    A = imread('./files/umbrellas.jpg');
    H = myhist3(A,8);
    colormap 'jet';
    
    subplot(1,4,1);
    imshow(A);
        
    BG = squeeze(sum(H,1)); 
    subplot(1,4,2);
    bar3(BG);
    bg = gca;
    bg.YLabel.String = "Blue";
    bg.XLabel.String = "Green";
    
    BR = squeeze(sum(H,2));
    subplot(1,4,3);
    bar3(BR);
    br = gca;
    br.YLabel.String = "Blue";
    br.XLabel.String = "Red";
    
    RG = squeeze(sum(H,3));
    subplot(1,4,4);
    bar3(RG);
    rg = gca;
    rg.YLabel.String = "Green";
    rg.XLabel.String = "Red";
end


% c)
function d = compare_histograms(h1,h2, dist_name)
    switch dist_name
        case 'l2'
            d = sqrt( sum( (h1-h2).^2 ) );
        case 'chi2'
            d = sum( ((h1-h2).^2) ./ (h1 + h2 + 1e-10)) ./ 2;
        case 'hellinger'
            d = sqrt( sum( (sqrt(h1) - sqrt(h2)) .^ 2) ./ 2  );
        case 'intersect'
            d = 1 - sum( min(h1,h2)); 
        otherwise
            error('Unknown distance type!')' % Throw an exception
    end
end


% d)
function [] = d()
    A = imread('./files/images/object_01_1.png');
    B = imread('./files/images/object_02_1.png');
    C = imread('./files/images/object_03_1.png');
    
    HA = myhist3(A,8);
    HB = myhist3(B,8);
    HC = myhist3(C,8);
    
    Ha = HA(:);
    Hb = HB(:);
    Hc = HC(:);
    
    subplot(2,3,1);
    imshow(A);
    title('Image 1');

    
    subplot(2,3,2);
    imshow(B);
    title('Image 2');   

    
    subplot(2,3,3);
    imshow(C);
    title('Image 3');
    
    d0 = compare_histograms(Ha,Ha,'l2');
    d0 = sum(sum(d0));
    d1 = compare_histograms(Ha,Hb,'l2');
    d1 = sum(sum(d1));
    d2 = compare_histograms(Ha,Hc,'l2');
    d2 = sum(sum(d2));
    
    subplot(2,3,4);
    plot(Ha);
    title(["D(H1,H1) = " num2str(d0) ]);

    subplot(2,3,5);
    plot(Hb);
    title(["D(H1,H2) = " num2str(d1) ]);

    
    subplot(2,3,6);
    plot(Hc);
    title(["D(H1,H3) = " num2str(d2) ]);

    
    % Image 3 is more similar to image 1, the sum of d1 = 0.42
    % and the sum of d2 is 0.09, meaning that the 
    % histograms of image 1 and 3 are closer
    
    d1 = compare_histograms(Ha,Hb,'chi2');
    d2 = compare_histograms(Ha,Hc,'chi2');
    sum(sum(d1)) % 0.4322
    sum(sum(d2)) % 0.1307
    
    d1 = compare_histograms(Ha,Hb,'hellinger');
    d2 = compare_histograms(Ha,Hc,'hellinger');
    sum(sum(d1)) % 0.5745
    sum(sum(d2)) % 0.3207
    
    d1 = compare_histograms(Ha,Hb,'intersect');
    d2 = compare_histograms(Ha,Hc,'intersect');
    sum(sum(d1)) % 0.6
    sum(sum(d2)) % 0.1970
    
    % The first bin has much higher value than others,
    % it represents the black color

end


% e)
function [distances] = e(dist_name, bins,selected, histograms,files)
    if nargin == 3
        [histograms,files] = load_histogram_database('./files/images', bins);
    end
    
    sel = histograms(selected, :);
    
    distances = zeros(size(histograms,1), 2);
    
    figure;
    for i = 1: size(histograms,1)
        d = compare_histograms(sel, histograms(i,:), dist_name);
        distances(i,1) = d;
        distances(i,2) = i;
    end
    
    Sorted = sortrows(distances);
    
    for i = 1:6
       I = imread(files{Sorted(i,2)}); 
       subplot(2,6,i);
       imshow(I);
       title(['Image ' num2str(Sorted(i,2))]);
       
       subplot(2,6, i + 6);
       plot(histograms(Sorted(i,2),:));
       title([ dist_name " = " num2str(Sorted(i,1)) ]);
    end
    
    %  Questions:
    %  Which distance measure is in your opinion best suited for the 
    %  specific task (elaborate your answer)?
    
    %  How does the retrieved sequence change if we change the number 
    %  of histogram bins (e.g.16 or 32)?
    % Oblika enaka, se bolj ocitna je prevladujoca crna, zelo majhna
    % verjetnost, da pixel pade v koncne bine (na koncu so namrec
    % prazni)

    %  Does the time required to perform the operation also change?
    %  Hitrost ostane podobna, casovna zahtevnost racunanja distance med 
    %  histogramoma je O(n), n = N * N * N
end

% f)
function [] = f(dist_name, bins,selected)  

    distances = e(dist_name, bins, selected);
    
    sorted = sortrows(distances);
    
    clf;
    subplot(1,2,1);
    plot(distances(:,1))
    
    hold on;
    for i=1:5
       plot(sorted(i,2), sorted(i,1),'ro'); 
    end
    
    subplot(1,2,2);
    plot(sorted(:,1));
    
    hold on;
    for i=1:5
       plot(i, sorted(i,1),'ro'); 
    end
end


% g)
function [] = g(bins, selected, lambda)
    [histograms,files] = load_histogram_database('./files/images', bins);

    H = double(zeros(bins,bins,bins));
    for i = 1:size(histograms,1)
       A = histograms(i,:);
       A = reshape(A,[bins,bins,bins]);
       H = H + A;
    end
    H = H/size(histograms,1);
    
    %bar(H(:));
    
    %bar3(squeeze(sum(H,2))); 
    % Tukaj dominira prvi bin - crna barva
    
    %  Probimo dobit weighting factor za vsak bin
    F = H(:);
    W = zeros(1, length(F));
    hist = histograms;
    for i=1:length(F)
         W(i) = exp(-lambda * F(i));
    end
    
    for j=1:size(histograms,1)
        for i=1:length(W)
           hist(j,i) = hist(j,i) * W(i);
        end
        H=hist(j,:);
        H = H/sum(H);
        hist(j,:) = H;
     end
    
    e("chi2",bins, selected, hist,files);
    
    % Razdalje so pri weighted histogramih malenkost vecje
    % pri istih predmetih, ampak pri ostalih predmetih je pa 
    % razdalja veliko veliko vecja
   
end

