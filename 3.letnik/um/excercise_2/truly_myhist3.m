function H = truly_myhist3(img, bins)
   % when computing cell indices add a small factor to avoid overflow 
    % problems
    idx = floor(double(img) * bins / (255 + 1e-5)) + 1;
    H = zeros(bins, bins, bins);
    
    % increment the appropriate cell of the H(R,G,B) for each pixel in the 
    % image
    IDX = idx(:);
    w = size(img,1);
    h = size(img, 2);
    Size = w * h;
    for i = 1: Size
        R  = IDX(i);
        G = IDX(i + Size);
        B = IDX(i + 2*Size);
        H(R,G,B) = H(R,G,B) + 1;
    end
    
    % normalize the histogram (sum of cell values should equal to 1)
    H = H / sum(sum(sum(H)));

end