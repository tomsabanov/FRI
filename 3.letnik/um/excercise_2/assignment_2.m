function [] = assignment_2()   
    %b()
    %c()
    d()
end


function Ig = simple_convolution(I, g)
    N = (length(g) - 1) / 2;
    Ig = zeros(1, length(I));
    for i = N+1:length(I)-N
       i_left = max([1,i-N]);
       i_right = min([length(I), i + N]);
       Ig(i) = sum(g .* I(i_left:i_right));
    end

end

% b)
function [] = b()
    signal = load('./files/signal.txt');
    kernel = load('./files/kernel.txt');
    Ig = simple_convolution(signal, kernel);
    
    hold on;
    figure(1);
    plot(signal);
    plot(kernel);
    plot(Ig);
    
    % Sum of elements in kernel = 1 -> Gauss
    sum(kernel)
end

% c)
function [] = c()
    signal = load('./files/signal.txt');
    kernel = load('./files/kernel.txt');
    Ig = conv(signal, kernel, 'same');
    
    hold on;
    plot(signal);
    plot(kernel);
    plot(Ig);
    
    % Pri simple convolution se ne zracuna konvolucija za 
    % prvih in zadnjih N elementov signala I
    
end


function [g,x] = gauss(sigma)
    x = -round(3.0*sigma):round(3.0*sigma);
    g = (1.0 /(sigma * sqrt(2*pi))) .* exp(-(x.^2)./(2*sigma^2));
    g = g / sum(g); % normalisation
end

% d)
function [] = d()
    hold on;
    
    [g,x] = gauss(2.0);
    sum(x);
    plot(x,g);
    
    K = load('./files/kernel.txt');
    plot(x,K);
    
    [g,x] = gauss(0.5);
    plot(x,g);
    
    [g,x] = gauss(1.0);
    plot(x,g);
    
    [g,x] = gauss(3.0);
    plot(x,g);
    
    [g,x] = gauss(4.0);
    plot(x,g);
        
end

% e)
function [] = e()
    signal = load('./files/signal.txt');
    k1 = gauss(2.0);
    k2 = [0.1, 0.6, 0.4];
    
    clf;
    subplot(1,4,1);
    plot(signal);
    title("Signal");
    
    subplot(1,4,2);
    A = conv(signal, k1, 'same');
    A = conv(A, k2, 'same');
    plot(A);
    title("(s * k1) * k2");
    
    subplot(1,4,3);
    A = conv(signal,k2,'same');
    A = conv(A,k1,'same');
    plot(A);
    title("(s * k2) * k1");
    
    subplot(1,4,4);
    k = conv(k1,k2);
    A = conv(signal, k);
    plot(A);
    title("s(k1 * k2)");
    
end


