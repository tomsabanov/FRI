function [histograms, files] = load_histogram_database(dir,bins)
    files = cell(30 * 4, 1); % Cell array to store filenames
    
    %initialize matrix for histograms
    histograms=zeros(30 * 4, bins^3);
    %calculate histogram for each image
    for i = 1:30; % iterate objects
        for j = 1:4 % iterate orientations
            image = (i-1) * 4 + j;
            files{image} = fullfile(dir, sprintf('object_%02d_%d.png',i,j));
            % Load image, extract histogram
            I = imread(files{image});
            H = truly_myhist3(I,bins);
            
            H = H(:);
            histograms(image, :) = H;
        end
    end
end