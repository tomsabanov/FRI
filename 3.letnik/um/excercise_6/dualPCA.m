function [Mu,C,U,S,V] = dualPCA(X)
    
    %2
    %Mu = [mean(X(1,:)) mean(X(2,:))]';
    Mu = mean(X)';
    
    %3
    Xd = X - Mu;
    
    %4
    M = size(Xd,1);
    C = (1/(M-1)) .* (Xd' * Xd);
    
    %5
    [U,S,V] = svd(C);
    s = diag(S) + 1e-15; % Extract the diagonal as a vector and add a small constant
    S = diag(1 ./ sqrt(s * (M - 1))); % Compute the inverse and construct a diagonal matrix
    
    T = 1 ./ sqrt((M-1) .* S);
    
    U = Xd * U * T;
    
end