function direct_pca_demo()
% https://stats.stackexchange.com/questions/2691/making-sense-of-principal-component-analysis-eigenvectors-eigenvalues

figure(1); clf;
P = load('points.txt');

% a)
[Mu,C,U,S,V] = directPCA(P);

subplot(1,2,1);
plot(P(1,:),P(2,:),'+'); hold on;
for i = 1 : size(P,2)
   text( P(1,i)-0.5, P(2,i), num2str(i)); 
end
xlabel('x_1'); ylabel('x_2');
xlim([-10 10]);
ylim([-10 10]);

draw_gauss2d(Mu,C,'r',1);

% b)
vec1 = (S(1,1) .* U(:,1));
vec2 = (S(2,2) .* U(:,2));

vec1 = [Mu';(vec1+Mu)';];
vec2 = [Mu';(vec2+Mu)'];

plot(vec1(:,1),vec1(:,2),'r');
plot(vec2(:,1),vec2(:,2),'g');


% c)
subplot(1,2,2);
D = diag(S);
B = cumsum(D);
B = B ./ max(B(:));
% We retain 83 if we discard second eigenvector
plot(B); hold on;


% d)

U(:,2) = 0; % We remove the second eigenvector
%U = U(:,1);
y = U' * (P - Mu);
Pq = U * y + Mu;
draw_reconstructions(P,Pq);

% e)
q = [3 6]';
%Compute closest point
closest = P(:,1);
dist = 1000;
for i=1:size(P,2)
    d = norm(q - P(:,i));
    if(d < dist)
       dist = d;
       closest = P(:,i);
    end
end
closest
[Mu,C,U,S,V] = directPCA(P);
U(:,1) = 0;
P(1:2,size(P,2)+1) = q;
y = U' * (P-Mu);
Pq = U * y + Mu


qs = Pq(:,size(Pq,2))
closest = Pq(:,1);
dist = 1000;
for i=1:size(Pq,2)-1
    d = norm(qs - Pq(:,i));
    if(d < dist)
       dist = d;
       closest = Pq(:,i);
    end
end
closest
draw_reconstructions(P,Pq);

