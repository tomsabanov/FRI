function A = read_series(series)
    cd(['faces/' num2str(series)]);
    
    imagefiles = dir('*.png');
    n = length(imagefiles);
    
    [h,w,d] = size(imread(imagefiles(1).name));
    
    A = zeros(n,h*w);
    
    for k=1:n
       name=imagefiles(k).name;
       I = (imread(name));
       v = reshape(I,[1,h*w]);
       A(k,:) = v;
    end
   
   cd('../../');
end
