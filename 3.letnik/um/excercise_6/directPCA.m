function [Mu,C,U,S,V] = directPCA(X)
    
    %2
    Mu = [mean(X(1,:)) mean(X(2,:))]';
    
    %3
    Xd = X - Mu;
    
    %4
    N = size(Xd,2);
    C = (1/(N-1)) .* (Xd * Xd');
    
    %5
    [U,S,V] = svd(C);
end