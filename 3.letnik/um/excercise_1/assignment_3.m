function [] = assignment_3()
    
    A = logical(imread("./files/mask.png"));
    %a(A);
    
    B = imread("./files/bird.jpg");
    %b(B);

    %c(B);
    
    D = imread("./files/eagle.jpg");
    %d(D);
    
    E = imread("./files/coins.jpg");
    %e(E);

    f();
        
end



% a)

function [] = a(M)

    SE = ones(3);
    figure;
    
    subplot(1,4,1);
    imagesc(imerode(M,SE)); axis equal; axis tight; title('Erode');
    
    subplot(1,4,2);
    imagesc(imdilate(M,SE)); axis equal; axis tight; title('Dilate');
    
    subplot(1,4,3);
    imagesc(imerode(imdilate(M,SE), SE)); axis equal; axis tight; title('Dilate &  Erode');
    
    subplot(1,4,4);
    imagesc(imdilate(imerode(M,SE), SE)); axis equal; axis tight; title('Erode & Dilate');
    

end



% b)


function result = b(A)
   A_gray = (rgb2gray(A));
   thresh = otsu(A);
   
   colormap gray;
   
   M = A_gray > thresh;
   SE = ones(18);
   
   result = imerode(imdilate(M,SE),SE);
   
   imagesc(result); 
   axis equal; axis tight;

end



% c)
function [] = c(A)
   A_gray = (rgb2gray(A));
   thresh = otsu(A);
      
   M = A_gray > thresh;
   SE = ones(18);
   
   B = imerode(imdilate(M,SE),SE);
   
   result = immask(A,B);   
   imagesc(result);
end



% d)
function [] = d(A)
 colormap gray;
  A_gray = (rgb2gray(A));
 thresh = otsu(A);
      
 % Dobimo masko
 M = A_gray > thresh;
 SE = ones(10);
 
 
 B = M;
 B = B .* (-1) + 1;
 B = imerode(imdilate(B,SE), SE);
 
 % The inverted mask comes from the '>' inequality, instead of setting 
 % 1 to pixels which are larger than the treshold, we could set 1 to
 % pixels that are smaller than the treshold. So instead of >, we could use
 % < , OR, leave the inequality and just invert the mask by multyplying it
 % with -1 and adding 1, so that 0's and 1's switch.
 
 
 result = immask(A,B);
 subplot(1,3,1); imagesc(M);
 subplot(1,3,2); imagesc(B);
 subplot(1,3,3); imagesc(result);

end


% e)
function [] = e(A)
    
    % Compute mask M
    A_gray = (rgb2gray(A));
    thresh = otsu(A);     
    M = A_gray > thresh;
    M = M ./ (-1) + 1;
    
    SE = ones(15);
    M = imerode(imdilate(M, SE), SE);
        
        
    
    L = bwlabel(M); % Use connected components algorithm to label all components
    label_max = max(L(:));
    for i = 1:label_max
        if sum(L(:) == i) < 10 
            L(L == i) = 0;
        end
    end
    % With changing of the value at the end of the inequality in the if statement 
    % we can isolate bigger/smaller components
    
    % We could get rid of smaller elements by first eroding and then
    % dilating, but removing bigger objects is tougher, since we cannot 
    % just use erodation, since that will also remove the smaller
    % components. We could of course remove them by just applyng a
    % condition to the entire mask ( min <= num_of_pixels <= max)
    
    subplot(1,3,1); 
    imshow(A); title('Original');
    
    subplot(1,3,2);
    imshow(M); title('Mask');
    
    subplot(1,3,3);
    imshow(immask(A, L > 0)); title('Processed');
    
    colormap gray;

end


% f)

function [] = f()    
    A = imread("./files/candy.jpg");  
    Ad = double(A);
    
    subplot(1,3,1);
    imshow(A);
    
    hold on;
    [x,y] = ginput(23);
    x = floor(x);
    y = floor(y);
    plot(x, y, 'w.', 'MarkerSize', 15); % Prikazimo izbrano tocko na slki
     
    A_gray = uint8((Ad(:,:,1) + Ad(:,:,2) + Ad(:,:,3)) / 3.0 );
    thresh = otsu(A_gray);
    M = A_gray < thresh;
        
    SE = strel('disk', 10); % strukturni element v obliki diska
    M = imclose(M,SE); % dilaton -> erosion
    M = imopen(M,SE); % erosion -> dilation
 
    subplot(1,3,2);
    imshow(M);
    
    L = bwlabel(M); % Use connected components algorithm to label all components
    label_max = max(L(:));
    
    selected = L(y,x);     
    % Get the indexes of pixels of a candy we selected
    [rs, cs] = find(L == selected);
    rc_s = [rs, cs]; % matrixa koordinat pixlov
    
    [r1,g1,b1,xs,ys] = getAvg(Ad, rc_s);
    
    subplot(1,3,3);
    imshow(A);
    hold on;
    candies = 0;
    for i = 1 : label_max
        
        [r, c] = find(L == i);
        rc = [r c];
        [r2,g2, b2, y, x] = getAvg(Ad,rc);
        % dobimo povprecni R, G in B, s katerimi bomo 
        % izracunali evklidovo distanco
        % dobimo tudi center, x in y koordinati
              
        distance = (r1 - r2)^2 + (g1 - g2)^2 + (b1 - b2)^2;
        distance = sqrt(distance)
        
        
        % threshold smo nastavili na 0.15
        if distance <= 0.15
            candies = candies + 1; 
            plot(x, y, 'bo', 'MarkerSize', 5);
        end
        
    end
    
    text(30,30,"Count: " + candies);
          
end


function [r,g,b, x, y] = getAvg(A, rc)
    l = length(rc);
    r = 0; g = 0; b = 0;
    x = 0;
    y = 0;
    for i=1 : l
       cx = rc(i,1);
       cy = rc(i,2);
       
       r = r + A(cx, cy, 1);
       g = g + A(cx, cy, 2);
       b = b + A(cx, cy, 3);
       
       x = x + rc(i,1);
       y = y + rc(i,2);
    end
    
    x = floor(x/l);
    y = floor(y/l);
    
    r = (double(r)/l)/255;
    g = (double(g)/l)/255;
    b = (double(b)/l)/255;
    
end






