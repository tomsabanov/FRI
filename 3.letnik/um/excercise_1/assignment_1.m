% Assignment 1
function [] = assignment_1()
    A = imread('./files/umbrellas.jpg');
    % Image A is in 8-bit format (uint8)
    
    %a(A);
    %b(A); 
    %c(A);
    %d(A);
    e(A);
end


% a)
function [] = a(A)
    figure(1); clf; imagesc(A);
    figure(2); clf; imshow(A); 
    
    % We open two figures, clear them and draw the image with imagesc and
    % imshow
end


% b)
function [] = b(A)
    Ad = double(A); % convert to double because of the uint8 overflow
    [h, w, d] = size(A); 
    % A is a three channel image (R,G,B) with height h and width w
    
    A_gray = uint8((Ad(:,:,1) + Ad(:,:,2) + Ad(:,:,3)) / 3.0 );
    % We get the average of R,G,B channels
    
     
    %colormap jet;
    %colormap bone;
    %colormap winter;
    %colormap gray; 
    
    % Colormaps define the colors that are used for single-chanell images
    % jet - low-values as dark blue, high value as red
    % bone - low values as black, high values as light blue-ish
    
    figure(1); clf; imagesc(A_gray); 
    figure(2); clf; imshow(A_gray);
    
    % imshow(I) displays the grayscale image I in a figure. imshow uses the default display range 
    % for the image data type and optimizes figure, axes, and image object properties for image display.
    
    % imagesc(C) displays the data in array C as an image that uses the FULL RANGE OF COLORS in the colormap
    
end

% c)
function [] = c(A)
    A1 = A;
    A1(130:260, 240:450, 3) = 0;
    figure;
    subplot(1,2,1);
    imshow(A1);
    % We set the values in the blue chanell of A in the rectangle to zero
    % and display the image
    
    
    A2 = A(130:260, 240:450, 1);
    % We cut out the same rectangle, but from the red channel, and display
    % it
    
    subplot(1,2,2);
    imshow(A2);
end

% d)
function [] = d(A)
    Ad = double(A); % To double because of overflow when summing values
    A_gray = uint8((Ad(:,:,1) + Ad(:,:,2) + Ad(:,:,3)) / 3.0); 
    % gray scaling the image by averaging the channels
    
    A_gray(130:260, 240:450) = 255 - A_gray(130:260, 240:450);
    % We set the values of a rectangle to their inverse ( 255 - x)
    
    figure;
    imshow(A_gray);
end

% e)
function [] = e(A)
    %{
        Podrobna razlaga razlike med imagesc in imshow pri A_gray in A_new ?
    %}
    
    Ad = double(A);
    A_gray = uint8((Ad(:,:,1) + Ad(:,:,2) + Ad(:,:,3))/ 3.0);
    A_gray = double(A_gray);
    
    Max = max(A_gray(:)) % Max value is 255
    Min = min(A_gray(:)) % Min value is 0
    
    
    A_new = uint8(A_gray .* (63.0/255.0));
    % We perform the reduction of grayscale levels, so the highest value of
    % 255 will translate  to 63, and 0 will stay at 0
    % uint8 floors the numbers
    
    
    Max = max(A_new(:)) % Max value is now 63
    Min = min(A_new(:)) % Min value is still 0
    
    %colormap gray;
    subplot(1,2,1); imagesc(A_new);
    subplot(1,2,2); imshow(A_new);
    
    % Imagesc uses the full range of colors in a colormap, so even if 
    % we performed the reduction of the grayscale levels, it does not
    % matter, as the highest value of A_new, which is 63, will be shown as
    % 255 in the default colormap
    
    % Imshow displays the grayscale image as is, and it uses the default
    % display range for the image data type, so in our case, the reduction
    % in grayscaled levels resulted in imshow displaying a darker image
    % as the values are from 0-63.
    
end

