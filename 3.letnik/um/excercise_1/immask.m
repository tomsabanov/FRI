
function result = immask(A, B)
    [h_a, w_a, ~ ] = size(A);
    [h_b, w_b, ~] = size(B);
    
    if h_a ~= h_b || w_a ~= w_b 
        result = "Sliki nista enake dimenzije";
        return
    end

    A(:,:,1) = uint8(double(A(:,:,1))  .*  double(B));
    A(:,:,2) = uint8(double(A(:,:,2))  .*  double(B));
    A(:,:,3) = uint8(double(A(:,:,3))  .*  double(B));
    
    
    
    result  = A;
end