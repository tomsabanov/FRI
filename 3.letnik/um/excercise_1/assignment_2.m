% Assignment 2
function [] = assignment_2()
    A = imread("./files/bird.jpg");
    %a(A);
    %b(A);
    
    C = imread("./files/umbrellas.jpg");
    %c(C);
    
    D = imread("./files/eagle.jpg");
    %(D,10);
    %d(A,10);
    %d(C,10);
    
    
    Light_1 = imread("./files/1_light.jpg");
    Dark_1 = imread("./files/1_dark.jpg");
    Light_2 = imread("./files/2_light.jpg");
    Dark_2 = imread("./files/2_dark.jpg");
    Light_3 = imread("./files/3_light.jpg");
    Dark_3 = imread("./files/3_dark.jpg");
    
    %e(Light_1, Dark_1);
    % Na prvi sliki sta  dva crna predmeta, razlika med histogramoma
    % ni ogromna,sredina histogramov sta si dokaj podobni, pri obeh je 
    % histogram nagnjen desno, pri temnejsi pa je razvidna razlika na levi
    % strani histograma, kjer imamo vec temnejsih pixlov
    
    %e(Light_2, Dark_2);
    % Na sliki 2 je zvezek, z svetlimi barvami, histograma med svetlo in 
    % temno slika sta si spet podobna, pri temnejsi sliki je razviden odsek
    % svetlih vrednosti na desni strani histograma, in povecanje vrednosti
    % na levi strani, a center histograma ostaja enak
    
    e(Light_3, Dark_3);
    % Na tretji sliki je vecja razlika med histogramoma, na svetlejsi sliki
    % je cetrtino vec vrednosti na desni strani histograma, torej na
    % "svetlejsem" delu, odsek se vidi na histogramu temne sliki od
    % vrednosti 175 naprej
    
    F = imread("./files/bird.jpg");
    %f(F);
    
    
end


% a)
function [] = a(A)
    tresh = 52; 
    % The treshold wemanually set
    
    A = rgb2gray(A);
    M = A > tresh; % All elements with value > 52
    
    figure;
    colormap gray;
    imagesc(M);
end

% b)
function [] = b(A)
    A_gray = double(rgb2gray(A));
    num_of_bins = 256;
  
    % Pri bins = 10 se oblika histograma ze definira, 
    % oblika histograma se torej ne spreminja kaj prevec, je pa razvidno,
    % da se gre - sodec po obliki in vrednosti v histogramu za sliko,
    % ki je dokaj temna oziroma ima vecino pixlov barve blizje crni, torej 0 vrednosti, 
    % ker se vecino vrednosti se nahaja v spodnjem 
    % delu histograma, npr. za bins = 50 se vidi, da je vec kot 90% pixlov v
    % prvih devetih binih, podobno za bins=256 je vecino vrednosti v prvi
    % petini vseh binov
     
    % Sama oblika histograma se priblizno ohranja od bin=10,
    % bin predstavlja stevilo skatel, v katere se porazdelijo pixli
    
    [H, bins] = myhist(A_gray, num_of_bins);
    
    figure;
    bar(bins,H);
end

% c)
function [] = c(A)    
    % Moramo ocitno ugotoviti, kako je funkcija hist implementirana, 
    % ker oblika grafa se kaj dosti spet ne spreminja
    
    % Myhist normalizira histogram, hist ga ne
    
    % Ko spremenimo stevilo binov, se zacnejo vrednosti drugace grupirati,
    % kar se opazi pri izgledu histograma.
    
    
    A_gray = double(rgb2gray(A));
    P = A_gray(:);
    figure(1); clf;
    bins = 10;
    H = hist(P, bins);
    subplot(1,3,1); bar(H,'b');
    bins = 20;
    H = hist(P,bins);
    subplot(1,3,2); bar(H,'b');
    bins = 40;
    H = hist(P, bins);
    subplot(1,3,3); bar(H, 'b');
    
end


function [] = d(A, num_of_bins)

% V myhist vrednost y posameznega bina predstavlja delez pixlov, ki spada v
% ta bin, prav tako je verjetnost, da bo pixel v zadnjem binu zelo majhna, 
% namrec v zadnji bin pridejo le tisti pixli, ki imajo vrednost 255, kar
% pomeni, da intervali binov niso enako siroki. V primeru da bi namesto
% floor uporabili ceil, bi bilo ravno obratno, v prvi bin bi prisli samo
% pixli ki imajo vrednost 0.
% V myhistu zaokrozimo navzdol, in vrednost binov na x-osi predstavlja
% spodnjo mejo vrednosti pixlov, v tistem binu.


% V hist pa vrednost v posameznem binu predstavlja stevilo vseh pixlov, ki
% spadajo v ta bin, prav tako pa so sirine vseh binov enako velike, po 
% defaultu pa hist plot-a centre teh binov, in ne spodnjo mejo, tako kot 
% pri nasi funkciji myhist ( hist lahko vrne centre binov v drugem argumentu
% kot [H, Centers] = hist(A) ).

% Hist najde razpon enostavno tako, da uporabi minimalno vrednost v sliki
% za levi rob prvega bina, in maksimalno vrednost v sliki za rob zadnjega 
% bina.


A_gray = double(rgb2gray(A));
P = A_gray(:);

[H1,bins] = myhist(A_gray, num_of_bins);
[H2, Centers] = hist(P, num_of_bins);
Centers

figure;
subplot(1,2,1); bar(bins, H1);
subplot(1,2,2); bar(H2, 'b');

end


% e)
function [] = e(A,B)
% A is lighter image
% B is darker image

A_gray = rgb2gray(A);
B_gray = rgb2gray(B);

figure; clf;

[H,bins] = myhist(A_gray, 50);
subplot(2,2,1); bar(bins, H);

[H,bins] = myhist(B_gray,50);
subplot(2,2,2); bar(bins, H);

[H,bins] = myhist(A_gray, 150);
subplot(2,2,3); bar(bins, H);

[H,bins] = myhist(B_gray,150);
subplot(2,2,4); bar(bins, H);


end


function [] = f(A)
   A_gray = (rgb2gray(A));
   thresh = otsu(A);
   
   M = A_gray > thresh;
   figure;clf;
   colormap gray;
   imagesc(M);
   
end
