function H = irls(px1, py1, px2, py2)

    % Construct matrix A
    W = eye(2*length(px1));
    A = zeros(2 * length(px1), 9);
    b = zeros(2 * length(px1), 1);
    for i=1:length(px1)
        A(2*i-1,:) = [px1(i),py1(i),1,0,0,0,-px1(i)*px2(i),-px2(i)*py1(i),-px2(i)];
        A(2*i,:) = [0,0,0,px1(i),py1(i),1,-px1(i)*py2(i),-py2(i)*py1(i),-py2(i)];
        b(2*i - 1) = px2(i);
        b(2*i) = py2(i);
    end
    
    E_old = 0;
    for j=1:1000
      % 2. Decomposition SVD
      [U,S,V] = svd(W * A);

      % 3. Compute vector h
      [hv,wv] = size(V);
       h = V(:,wv);
       h = h ./ h(length(h));
        
       e = A * h - b; % Projection errors  
       delta = 1.345 * mad(e)/0.6745;
       
       w = zeros(length(e),1);
       
       for i=1:length(w)
           huber = 0;
           if (e(i) < -delta)
               huber = -delta; 
           elseif (e(i) >= -delta && e(i) <= delta)
               huber = e(i);
           else
               huber = delta;
           end
           w(i) = huber/abs(e(i) + 10e-10);
       end  
       W = diag(w);
     
    end
   
    % 4. Reorder elements h to 3x3
    H = reshape(h,[3,3]);
    H = H';
end