function [M,B] = find_matches(I1,I2,m, sigma, bins, thresh, num)

    [px1, py1, I1_hess] = harris_points(I1, sigma,thresh);
    %[px1, py1, I1_hess] = hessian_points(I1, sigma,thresh);

    [px2, py2, I2_hess] = harris_points(I2, sigma,thresh);
    %[px2, py2, I2_hess] = hessian_points(I2, sigma,thresh);

    D1 = descriptors_maglap(I1, px1, py1, m, sigma, bins);
    D2 = descriptors_maglap(I2, px2, py2, m, sigma, bins);

    [indices1, distances1] = find_correspondences(D1, D2);
   [indices2, distances2] = find_correspondences(D2, D1);

   indices = zeros(max(length(indices1), length(indices2)), 1); % P1 -> P2 indexes to indices1, indices2
   distances = ones(length(indices),1);
   
   % Keep only symetric matches
   for i = 1:length(indices1)
      matched = indices1(i);
      if(i == indices2(matched))
          indices(i) = matched;
          distances(i) = distances1(i);
      end
   end
    
    % Sort matches by distances
    [B,Indx] = sort(distances, 'ascend');
       
    D = indices(Indx);
    D = nonzeros(D);
    l = length(D);
    Indx = Indx(1:l);
    
    distances = B(1:l);
    px1 = px1(Indx);
    py1 = py1(Indx);
    
    px2 = px2(indices(Indx));
    py2 = py2(indices(Indx));
    

    % Filtriraj slabe matche z barvo?
    [px1,py1, px2, py2, distances, B] = filterMatches(I1, px1,py1, I2,px2,py2, distances);
   
    M = [px1  py1  px2  py2 distances];
    M = M(any(M,2),:);
    
    B = B(any(B,2),:);
    M = M(1:num,:);
end