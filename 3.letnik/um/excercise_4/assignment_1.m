function [] = assignment_1()

    %a();
    b();

end


function [] = a()
    colormap gray;
    A = rgb2gray(imread("graf/graf1.jpg"));
    %A = rgb2gray(imread("test_points.jpg"));
    %A = (imread("newyork/newyork1.jpg"));

    [px1, py1, A_hess1] = hessian_points(A, 3,100);
    [px2, py2, A_hess2] = hessian_points(A, 6,100);
    [px3, py3, A_hess3] = hessian_points(A, 9,100);
        
    subplot(2,3,1); imagesc(A_hess1);
    subplot(2,3,2); imagesc(A_hess2);
    subplot(2,3,3); imagesc(A_hess3);
    subplot(2,3,4); plotPoints(A, px1, py1);
    subplot(2,3,5); plotPoints(A, px2,py2);
    subplot(2,3,6); plotPoints(A, px3, py3);
end

function [] = b()
    colormap gray;
    %A = rgb2gray(imread("graf/graf1.jpg"));
    A = (imread("newyork/newyork1.jpg"));

    [px1, py1, A_hess1] = harris_points(A, 3,100);
    [px2, py2, A_hess2] = harris_points(A, 6,100);
    [px3, py3, A_hess3] = harris_points(A, 9,100);
        
    subplot(2,3,1); imagesc(A_hess1);
    subplot(2,3,2); imagesc(A_hess2);
    subplot(2,3,3); imagesc(A_hess3);
    subplot(2,3,4); plotPoints(A, px1, py1);
    subplot(2,3,5); plotPoints(A, px2,py2);
    subplot(2,3,6); plotPoints(A, px3, py3);

end




