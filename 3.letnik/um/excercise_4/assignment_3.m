function [] = assignment_3()

    %b("newyork/newyork1.jpg","newyork/newyork2.jpg", "newyork/newyork.txt");
    %b("graf/graf1.jpg","graf/graf2.jpg", "graf/graf.txt");
    
    c("newyork/newyork1.jpg","newyork/newyork2.jpg");
    %e()
    
    %A = (imread("newyork/newyork1.jpg"));
    %B = (imread("newyork/newyork2.jpg"));
    %[P,Bad] = find_matches(A, B,91,3,18,100,10);
    %displaymatches(A,P(:,1),P(:,2), B, P(:,3), P(:,4));
    
end


function [] = b(image1,image2, file_points)

    A = (imread(image1));
    B = (imread(image2));
    
    P = readmatrix(file_points);
    displaymatches(A, P(:,1), P(:,2), B, P(:,3), P(:,4));
    H = estimate_homography(P(:,1), P(:,2), P(:,3), P(:,4));
    IT = transform_homography(A,H); 
    
    figure; colormap gray;
    subplot(1,3,1); imagesc(A);
    subplot(1,3,2); imagesc(B);
    subplot(1,3,3); imagesc(IT);
end

function [] = c(image1, image2)
    A = (imread(image1));
    B = (imread(image2));
    
    [P,Bad] = find_matches(A, B,61,1,16,10e4, 7);
    
    displaymatches(A, P(:,1), P(:,2), B, P(:,3), P(:,4));
    H1 = estimate_homography(P(:,1), P(:,2), P(:,3), P(:,4));
    H2 = irls(P(:,1), P(:,2), P(:,3), P(:,4));
       
    IT1 = transform_homography(A,H1); 
    IT2 = transform_homography(A,H2);
    
    
    
    figure;colormap gray;
    colormap gray;
    subplot(1,2,1); imagesc(IT1);
    subplot(1,2,2); imagesc(IT2);
    
    IT = merge_homography(B,A,H2);
    %imagesc(IT) 
    
end

function [] = e()
    colormap gray;
    A1=rgb2gray(imread("panorama/panorama1.jpg"));
    A2=rgb2gray(imread("panorama/panorama2.jpg"));
    A3=rgb2gray(imread("panorama/panorama3.jpg"));
    A4=rgb2gray(imread("panorama/panorama4.jpg"));
    A5=rgb2gray(imread("panorama/panorama5.jpg"));
    A6=rgb2gray(imread("panorama/panorama6.jpg"));
    
    %subplot(1,6,1); imagesc(A1);
    %subplot(1,6,2); imagesc(A2);
    %subplot(1,6,3); imagesc(A3);
    %subplot(1,6,4); imagesc(A4);
    %subplot(1,6,5); imagesc(A5);
    %subplot(1,6,6); imagesc(A6);
    

    P = find_matches(A1, A2, 81, 3, 16, 10e3,6);
    H = irls(P(:,3), P(:,4), P(:,1), P(:,2));
    IT = merge_homography(A1,A2,H);
    displaymatches(A1, P(:,1), P(:,2), A2, P(:,3), P(:,4));
    imagesc(IT)
     
    P = find_matches(A2, A3, 81,3, 16, 10e4,4);
    H = irls(P(:,3), P(:,4), P(:,1), P(:,2));
    %displaymatches(A2, P(:,1), P(:,2), A3, P(:,3), P(:,4));
    IT = merge_homography(A2,A3,H);
    %imagesc(IT);
    
    %%%%%%%%%%%%%%%
    
    P = find_matches(A3, A4, 81, 3, 16, 10e3, 6);
    H1 = irls(P(:,3), P(:,4), P(:,1), P(:,2));
    %displaymatches(A3, P(:,1), P(:,2), A4, P(:,3), P(:,4));
    IT4 = merge_homography(A3,A4,H1);
    %imagesc(IT4);
    
    
    P = find_matches(A4, A5, 81, 3, 16, 10e3, 6);
    H2 = irls(P(:,3), P(:,4), P(:,1), P(:,2));
    %displaymatches(A3, P(:,1), P(:,2), A4, P(:,3), P(:,4));
    %IT5 = merge_homography(A4,A5,H2);
    %imagesc(IT5);
    
    H = H1 * H2;
    IT = merge_homography(IT4,A5,H);
    imagesc(IT);
    
    %%%%%%%%%%%%%%%%%
    
    P = find_matches(A5, A6, 81, 3, 16, 10e3, 6);
    H2 = irls(P(:,3), P(:,4), P(:,1), P(:,2));
    %displaymatches(A5, P(:,1), P(:,2), A6, P(:,3), P(:,4));
    IT5 = merge_homography(A5,A6,H2);
    %figure;colormap gray;imagesc(IT5);
    
end
