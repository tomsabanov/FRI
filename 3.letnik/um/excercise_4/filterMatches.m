function [px1,py1, px2,py2, distances, B] = filterMatches(I1,px1,py1,I2,px2, py2, distances)
    % Remove bad matches with replacing their values with zeros

    len = length(px1);
    I1 = double(I1);
    I2 = double(I2);
    
    Sum1 = movsum(I1,16,16);
    Sum2 = movsum(I2,16,16);
    num = 1;
    % Bad matches
    B = zeros(length(px1), 5);
    
    for i=1:len
        x = px1(i);
        y = py1(i);
        c1 = Sum1(y,x);
        
        x = px2(i);
        y = py2(i);
        c2 = Sum2(y,x);
        
        mi = min(c1,c2);
        ma = max(c1,c2);
        res = 1-mi/ma;
        if(res<0.001)
            % mismatch - remove the row
  
            B(num,:) = [px1(i) py1(i) px2(i) py2(i) distances(i)];
            num = num + 1;
 
            px1(i) = 0;
            px(i) = 0;
            px2(i) = 0;
            py2(i) = 0;
            distances(i) = 0;
       end

    end


end