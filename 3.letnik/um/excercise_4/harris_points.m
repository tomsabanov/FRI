function [px, py, C] = harris_points( I, sigma, thresh )    
    [Ix, Iy] = image_derivatives(I,sigma);
      
    el1 = Ix .^ 2;
    el1 = imgaussfilt(el1, 1.6 * sigma);

    el4 = Iy .^ 2;
    el4 = imgaussfilt(el4, 1.6 * sigma);

    el2 = Ix .* Iy;
    el2 = imgaussfilt(el2, 1.6 * sigma);

    
    det = (sigma^4) .* (el1 .* el4 - (el2 .^ 2));    
    trace = (sigma^2) .* (el1 + el4);
    
    C = det - (0.06 .* (trace .^ 2));
        
    D = C > thresh;
    C = D .* C;
       
    % POST-PROCESSING - non-maxima suppression
    [py, px] = find(imregionalmax(C,8));
    
end



