function [] = plotPoints(I, x, y)
    colormap gray;
    imagesc(I);
    hold on;
    plot(x,y, 'r+', 'MarkerSize', 10, 'LineWidth', 1);
    hold off;
end
