function [] = assignment_2()
    
    I = rgb2gray(imread('test_points.jpg'));
    %G1 = rgb2gray(imread('graf/graf1_small.jpg'));
    %G2 = rgb2gray(imread('graf/graf2_small.jpg'));
    G1 = (imread('newyork/newyork1.jpg'));
    G2 = (imread('newyork/newyork2.jpg'));
    
    %b(I,I, 41, 3, 8, 100);
    %b(G1,G2,81,3,16,10e4);
    
    [M,B] = find_matches(G1,G2,41,3,8,100, 10);
    displaymatches(G1,M(:,1),M(:,2),G2,M(:,3),M(:,4));
    %displaymatches(G1,B(:,1),B(:,2),G2,B(:,3),B(:,4));


end

function [] = b(I1, I2,m,sigma, bins, thresh)
    
    [px1, py1, I1_hess] = harris_points(I1, sigma,thresh);
    %[px1, py1, I1_hess] = hessian_points(I1, sigma,thresh);

    [px2, py2, I2_hess] = harris_points(I2, sigma,thresh);
    %[px2, py2, I2_hess] = hessian_points(I2, sigma,thresh);
    
    D1 = descriptors_maglap(I1, px1, py1, m, sigma, bins);
    D2 = descriptors_maglap(I2, px2, py2, m, sigma, bins);
    
    [indices, distances] = find_correspondences(D1, D2);
    
    [B,Indx] = sort(distances, 'ascend');
    
    Indx = Indx(1:20);
    
    px1 = px1(Indx);
    py1 = py1(Indx);
    
    px2 = px2(indices(Indx));
    py2 = py2(indices(Indx));
    
    displaymatches(I1, px1, py1, I2, px2, py2);
    
end