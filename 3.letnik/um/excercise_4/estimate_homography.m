function H = estimate_homography(px1, py1, px2, py2)

    % Construct matrix A
    
    A = zeros(2 * length(px1), 9);
    
    for i=1:length(px1)
        A(2*i-1,:) = [px1(i),py1(i),1,0,0,0,-px1(i)*px2(i),-px2(i)*py1(i),-px2(i)];
        A(2*i,:) = [0,0,0,px1(i),py1(i),1,-px1(i)*py2(i),-py2(i)*py1(i),-py2(i)];
    end   
    
    % 2. Decomposition SVD
    [U,S,V] = svd(A);
    
    % 3. Compute vector h
    [hv,wv] = size(V);
    h = V(:,wv);
    h = h ./ h(length(h));
    
    % 4. Reorder elements h to 3x3
    H = reshape(h, [3,3]);
    H = H';
end