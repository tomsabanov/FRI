function [px, py, I] = hessian_points(I,sigma,threshold)    

    I = imgaussfilt(I,sigma);
    [Ixx, Iyy,Ixy] = image_derivatives2(I, sigma);
    
    % We calculate the determinant with normalization factor of sigma^4
    I_hess = (Ixx .* Iyy) - (Ixy).^2;
    I_hess = (sigma^4) .* I_hess;    
   
     
    I = I_hess > threshold;
    I = I .* I_hess;
    
    % Non-maximum suppression 
    [py, px] = find(imregionalmax(I,8));
  
end
