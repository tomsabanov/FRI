#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <FreeImage.h>
#include <CL/cl.h>
#include <chrono>
#include <ostream>

using namespace std::chrono; 
using namespace std;

#define SIZE		1024
#define WORKGROUP_SIZE	32
#define MAX_SOURCE_SIZE	16384


void mandelbrotCPU(unsigned char *image, int height, int width) {
	float x0, y0, x, y, xtemp;
	int i, j;
	int color;
	int iter;
	int max_iteration = 800;   //max stevilo iteracij
	unsigned char max = 255;   //max vrednost barvnega kanala

	//za vsak piksel v sliki							
	for (i = 0; i < height; i++){
		for (j = 0; j < width; j++){
			x0 = (float)j / width * (float)3.5 - (float)2.5; //zacetna vrednost
			y0 = (float)i / height * (float)2.0 - (float)1.0;
			x = 0;
			y = 0;
			iter = 0;
			//ponavljamo, dokler ne izpolnemo enega izmed pogojev
			while ((x*x + y * y <= 4) && (iter < max_iteration))
			{
				xtemp = x * x - y * y + x0;
				y = 2 * x*y + y0;
				x = xtemp;
				iter++;
			}
			//izracunamo barvo (magic: http://linas.org/art-gallery/escape/smooth.html)
			color = 1.0 + iter - log(log(sqrt(x*x + y * y))) / log(2.0);
			color = (8 * max * color) / max_iteration;
			if (color > max)
				color = max;
			//zapisemo barvo RGBA (v resnici little endian BGRA)
			image[4 * i*width + 4 * j + 0] = color; //Blue
			image[4 * i*width + 4 * j + 1] = color; // Green
			image[4 * i*width + 4 * j + 2] = color; // Red
			image[4 * i*width + 4 * j + 3] = 255;   // Alpha
		}
	}
}


int main(void) 
{

	char ch;
	cl_int ret;

    // Branje datoteke
    FILE *fp;
    char *source_str;
    size_t source_size;

    fp = fopen("kernel.cl", "r");
    if (!fp) 
	{
		fprintf(stderr, ":-(#\n");
        exit(1);
    }
    source_str = (char*)malloc(MAX_SOURCE_SIZE);
    source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
	source_str[source_size] = '\0';
    fclose( fp );

	// Rezervacija pomnilnika
	int height = SIZE;
	int width = SIZE;
	int pitch = ((32 * width + 31) / 32) * 4;

	//rezerviramo prostor za sliko (RGBA)
	unsigned char *image = (unsigned char *)malloc(height * width * sizeof(unsigned char) * 4);

 
    // Podatki o platformi
    cl_platform_id	platform_id[10];
    cl_uint			ret_num_platforms;
	char			*buf;
	size_t			buf_len;
	ret = clGetPlatformIDs(10, platform_id, &ret_num_platforms);
			// max. "stevilo platform, kazalec na platforme, dejansko "stevilo platform
	
	// Podatki o napravi
	cl_device_id	device_id[10];
	cl_uint			ret_num_devices;
	// Delali bomo s platform_id[0] na GPU
	ret = clGetDeviceIDs(platform_id[0], CL_DEVICE_TYPE_GPU, 10,	
						 device_id, &ret_num_devices);				
			// izbrana platforma, tip naprave, koliko naprav nas zanima
			// kazalec na naprave, dejansko "stevilo naprav

    // Kontekst
    cl_context context = clCreateContext(NULL, 1, &device_id[0], NULL, NULL, &ret);
			// kontekst: vklju"cene platforme - NULL je privzeta, "stevilo naprav, 
			// kazalci na naprave, kazalec na call-back funkcijo v primeru napake
			// dodatni parametri funkcije, "stevilka napake
 
    // Ukazna vrsta
    cl_command_queue command_queue = clCreateCommandQueue(context, device_id[0], 0, &ret);
			// kontekst, naprava, INORDER/OUTOFORDER, napake

    // Alokacija pomnilnika na napravi
    cl_mem c_mem_obj = clCreateBuffer(context, CL_MEM_WRITE_ONLY, 
									  height*width*4*sizeof(unsigned char), NULL, &ret);
  
    // Priprava programa
    cl_program program = clCreateProgramWithSource(context,	1, (const char **)&source_str,  
												   NULL, &ret);
			// kontekst, "stevilo kazalcev na kodo, kazalci na kodo,		
			// stringi so NULL terminated, napaka													
 
    // Prevajanje
    ret = clBuildProgram(program, 1, &device_id[0], NULL, NULL, NULL);
			// program, "stevilo naprav, lista naprav, opcije pri prevajanju,
			// kazalec na funkcijo, uporabni"ski argumenti

	// Log
	size_t build_log_len;
	char *build_log;
	ret = clGetProgramBuildInfo(program, device_id[0], CL_PROGRAM_BUILD_LOG, 
								0, NULL, &build_log_len);
			// program, "naprava, tip izpisa, 
			// maksimalna dol"zina niza, kazalec na niz, dejanska dol"zina niza
	build_log =(char *)malloc(sizeof(char)*(build_log_len+1));
	ret = clGetProgramBuildInfo(program, device_id[0], CL_PROGRAM_BUILD_LOG, 
							    build_log_len, build_log, NULL);
	printf("%s\n", build_log);
	free(build_log);
 
    // "s"cepec: priprava objekta
    cl_kernel kernel = clCreateKernel(program, "mandelbrot", &ret);
			// program, ime "s"cepca, napaka
 
    // "s"cepec: argumenti
    clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&c_mem_obj);
    clSetKernelArg(kernel, 1, sizeof(cl_int), (void *)&width);
    clSetKernelArg(kernel, 2, sizeof(cl_int), (void *)&height);
			// "s"cepec, "stevilka argumenta, velikost podatkov, kazalec na podatke

	// Delitev dela
	size_t local_item_size[2] = {WORKGROUP_SIZE, WORKGROUP_SIZE};
	size_t global_item_size[2] = {SIZE, SIZE};

	// "s"cepec: zagon
	auto start = high_resolution_clock::now(); 
	//mandelbrotCPU(image,height,width);
    ret = clEnqueueNDRangeKernel(command_queue, kernel, 2, NULL,						
								 global_item_size, local_item_size, 0, NULL, NULL);	
			// vrsta, "s"cepec, dimenzionalnost, mora biti NULL, 
			// kazalec na "stevilo vseh niti, kazalec na lokalno "stevilo niti, 
			// dogodki, ki se morajo zgoditi pred klicem

	auto stop = high_resolution_clock::now(); 
	auto duration = duration_cast<microseconds>(stop - start); 
  
	// To get the value of duration use the count() 
	// member function on the duration object 
	printf("%d\n",duration_cast<microseconds>(stop - start).count());



    // Kopiranje rezultatov
    ret = clEnqueueReadBuffer(command_queue, c_mem_obj, CL_TRUE, 0,						
							  height*width*4*sizeof(unsigned char), image, 0, NULL, NULL);				
			// branje v pomnilnik iz naparave, 0 = offset
			// zadnji trije - dogodki, ki se morajo zgoditi prej

	//shranimo sliko
	FIBITMAP *dst = FreeImage_ConvertFromRawBits(image, width, height, pitch,
		32, FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK, TRUE);
	FreeImage_Save(FIF_PNG, dst, "mandelbrot.png", 0);


    // "ci"s"cenje
    ret = clFlush(command_queue);
    ret = clFinish(command_queue);
    ret = clReleaseKernel(kernel);
    ret = clReleaseProgram(program);
    ret = clReleaseMemObject(c_mem_obj);
    ret = clReleaseCommandQueue(command_queue);
    ret = clReleaseContext(context);
	
    free(image);

    return 0;
}
