#define MAX_SOURCE_SIZE	16384
#include <stdio.h>
#include <stdlib.h>
#include <FreeImage.h>
#include <CL/cl.h>
#include <time.h>
#include <math.h>
#include "sum.cpp"


void printPartialMatrix(float* A, int h, int w, int dim){
	    // Prikaz rezultatov
	int size = w * h;

	for(int k = 0; k<dim; k++){
		for(int i=0; i<h; i++){
			for(int j=0; j<w; j++){
				printf("%f ", A[i*w+j + k*size]);
			}
			printf("\n-------------------------------------\n");
		}
	}

}
int new_multiply(float* A, float* B, float* C, int hA, int wA, int hB, int wB, int wsize){
    char ch;
    int i, j;
	cl_int ret;


    // Branje datoteke
    FILE *fp;
    char *source_str;
    size_t source_size;

    fp = fopen("new.cl", "r");
    if (!fp) 
	{
		fprintf(stderr, ":-(#\n");
        exit(1);
    }
    source_str = (char*)malloc(MAX_SOURCE_SIZE);
    source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
	source_str[source_size] = '\0';
    fclose( fp );


	int dim = 1;
	if (wA % wsize == 0){dim = wA / wsize;}
	else {dim = wA / wsize + 1;}
	float* D =(float*)malloc(sizeof(float) * hA * wB * dim);


    // Podatki o platformi
    cl_platform_id	platform_id[10];
    cl_uint			ret_num_platforms;
	char			*buf;
	size_t			buf_len;
	ret = clGetPlatformIDs(10, platform_id, &ret_num_platforms);
			// max. "stevilo platform, kazalec na platforme, dejansko "stevilo platform
	
	// Podatki o napravi
	cl_device_id	device_id[10];
	cl_uint			ret_num_devices;
	// Delali bomo s platform_id[0] na GPU
	ret = clGetDeviceIDs(platform_id[0], CL_DEVICE_TYPE_GPU, 10,	
						 device_id, &ret_num_devices);				
			// izbrana platforma, tip naprave, koliko naprav nas zanima
			// kazalec na naprave, dejansko "stevilo naprav

    // Kontekst
    cl_context context = clCreateContext(NULL, 1, &device_id[0], NULL, NULL, &ret);
			// kontekst: vklju"cene platforme - NULL je privzeta, "stevilo naprav, 
			// kazalci na naprave, kazalec na call-back funkcijo v primeru napake
			// dodatni parametri funkcije, "stevilka napake
 
    // Ukazna vrsta
    cl_command_queue command_queue = clCreateCommandQueue(context, device_id[0], 0, &ret);
			// kontekst, naprava, INORDER/OUTOFORDER, napake

    // Alokacija pomnilnika na napravi
    cl_mem a_mem_obj = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
									  hA*wA*sizeof(float), A, &ret);
			// kontekst, na"cin, koliko, lokacija na hostu, napaka	
    cl_mem b_mem_obj = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
									  hB*wB*sizeof(float), B, &ret);
    cl_mem c_mem_obj = clCreateBuffer(context, CL_MEM_WRITE_ONLY, 
									  hA*wB*dim*sizeof(float), NULL, &ret);
  
    // Priprava programa
    cl_program program = clCreateProgramWithSource(context,	1, (const char **)&source_str,  
												   NULL, &ret);
			// kontekst, "stevilo kazalcev na kodo, kazalci na kodo,		
			// stringi so NULL terminated, napaka													
 
    // Prevajanje
    ret = clBuildProgram(program, 1, &device_id[0], NULL, NULL, NULL);
			// program, "stevilo naprav, lista naprav, opcije pri prevajanju,
			// kazalec na funkcijo, uporabni"ski argumenti

	// Log
	size_t build_log_len;
	char *build_log;
	ret = clGetProgramBuildInfo(program, device_id[0], CL_PROGRAM_BUILD_LOG, 
								0, NULL, &build_log_len);
			// program, "naprava, tip izpisa, 
			// maksimalna dol"zina niza, kazalec na niz, dejanska dol"zina niza
	build_log =(char *)malloc(sizeof(char)*(build_log_len+1));
	ret = clGetProgramBuildInfo(program, device_id[0], CL_PROGRAM_BUILD_LOG, 
							    build_log_len, build_log, NULL);
	free(build_log);

    // "s"cepec: priprava objekta
    cl_kernel kernel = clCreateKernel(program, "multiply", &ret);
			// program, ime "s"cepca, napaka
 
    // "s"cepec: argumenti
    clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&a_mem_obj);
    clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&b_mem_obj);
    clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&c_mem_obj);
    clSetKernelArg(kernel, 3, sizeof(cl_int), (void *)&hA);
    clSetKernelArg(kernel, 4, sizeof(cl_int), (void *)&wA);
    clSetKernelArg(kernel, 5, sizeof(cl_int), (void *)&hB);
    clSetKernelArg(kernel, 6, sizeof(cl_int), (void *)&wB);
	clSetKernelArg(kernel, 7, sizeof(cl_int), (void *)&wsize);
	// "s"cepec, "stevilka argumenta, velikost podatkov, kazalec na podatke

	// Delitev dela
	size_t local_item_size[2] = {wsize, wsize};
	int widthFixed = 1;
	int heightFixed = 1;
	if (wA % wsize == 0){widthFixed = wA;}
	else {widthFixed = wA + (wsize - wA % wsize);}
	size_t global_item_size[2] = { widthFixed,wsize};


	// "s"cepec: zagon
    ret = clEnqueueNDRangeKernel(command_queue, kernel, 2, NULL,						
								 global_item_size, local_item_size, 0, NULL, NULL);	
			// vrsta, "s"cepec, dimenzionalnost, mora biti NULL, 
			// kazalec na "stevilo vseh niti, kazalec na lokalno "stevilo niti, 
			// dogodki, ki se morajo zgoditi pred klicem
	printf("%d\n", ret);

    // Kopiranje rezultatov
    ret = clEnqueueReadBuffer(command_queue, c_mem_obj, CL_TRUE, 0,						
							  hA*wB*dim*sizeof(float), D, 0, NULL, NULL);				
			// branje v pomnilnik iz naparave, 0 = offset
			// zadnji trije - dogodki, ki se morajo zgoditi prej
	printf("%d\n",ret);																		


    // "ci"s"cenje
    ret = clFlush(command_queue);
    ret = clFinish(command_queue);
    ret = clReleaseKernel(kernel);
    ret = clReleaseProgram(program);
    ret = clReleaseMemObject(a_mem_obj);
    ret = clReleaseMemObject(b_mem_obj);
    ret = clReleaseMemObject(c_mem_obj);
    ret = clReleaseCommandQueue(command_queue);
    ret = clReleaseContext(context);


	//printPartialMatrix(D,hA,wB,dim);

	// D summamo v C
	sum(D,C,hA, wA, hB,wB, dim , wsize);

    return 0;
}