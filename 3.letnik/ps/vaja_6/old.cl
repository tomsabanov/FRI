#define WORKGROUP_SIZE	32
  
__kernel void matrixMultiply_OLD(__global float *A,
							 __global float *B,		
							 __global float *C,				
							 int widthA,
							 int heightA,
							 int widthB,
							 int heightB)						
{
    // indeks bloka
    int bx = get_group_id(0);
    int by = get_group_id(1);
 
    // indeks niti
    int tx = get_local_id(0);
    int ty = get_local_id(1);
 
    // indeks elementa iz A, ki ozna"cuje za"cetek vrstice v matriki
    int aBegin = widthA * WORKGROUP_SIZE * by; 
    // indeks elementa iz A, ki ozna"cuje konec(+1) vrstice v matriki
    int aEnd   = aBegin + widthA;
	// korak v A do naslednjega bloka
    int aStep  = WORKGROUP_SIZE;
    // indeks prvega elementa iz B, ki je v bloku
    int bBegin = WORKGROUP_SIZE * bx;
    // korak v B do nasledenjga bloka
    int bStep  = WORKGROUP_SIZE * widthB;
 
	float Csub = 0.0f;
    // gremo "cez vse podmatrike A in B
      		// alociramo lokalni pomnilnik

    for (int a = aBegin, b = bBegin; a < aEnd; a += aStep, b += bStep) 
    {
        __local float As[WORKGROUP_SIZE][WORKGROUP_SIZE];
        __local float Bs[WORKGROUP_SIZE][WORKGROUP_SIZE];
 
		// vsaka nit vpi"se v lokalni pomnilnik dva podatka
        As[ty][tx] = A[a + widthA * ty + tx];
        Bs[ty][tx] = B[b + widthB * ty + tx];
 
        // po"cakamo, da vse niti zaklju"cijo
        barrier(CLK_LOCAL_MEM_FENCE);
 
		// zmno"zimo
        for (int k = 0; k < WORKGROUP_SIZE; ++k)
            Csub += As[ty][k] * Bs[k][tx];
		
		// po"cakamo, da vse niti zaklju"cijo
        barrier(CLK_LOCAL_MEM_FENCE); 
    }

	// indeks prvega elementa v bloku
    int c = widthB * WORKGROUP_SIZE * by + WORKGROUP_SIZE * bx;

	// vpi"semo vrednost
    C[c + widthB * ty + tx] = Csub;
}
