  
__kernel void sum(__global float *A,
							 __global float *Result,		
							 int w,
							 int h,
							 int dim)						
{
    int i = get_global_id(0);
    int j = get_global_id(1);

    int index = i*w + j;

    int size = w * h;

	if(i<h && j<w){
        float temp = 0.0f;
        for(int k = 0; k<dim; k++){
            temp += A[index + size*k];
        }		
        Result[index] =  temp;
	}

		
}
