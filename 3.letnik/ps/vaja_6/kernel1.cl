int getNum(int i, int j,__global int *matrix, int width, int height)
{
	if (i < 0 || i >= width)
		return 0;
	if (j < 0 || j >= height)
		return 0;
	return matrix[j*width + i];
}



__kernel void matrix(__global int *matrixA,
	__global int *matrixB,
	__global int *matrixP,
	int heightA,
	int widthA,
	int heightB,
	int widthB)
{

	int i = get_global_id(0);
	int j = get_global_id(1);
	int lokI = get_local_id(0);
	int lokJ = get_local_id(1);

	int slice = i/16;

	//extended height
	int heightA_exp = 1;
	if (heightA % 16 == 0)
		heightA_exp = heightA;
	else {
		heightA_exp = heightA + (16 - heightA % 16);
	}
	//extended width
	int widthB_exp = 1;
	if (widthB % 16 == 0)
		widthB_exp = widthB;
	else {
		widthB_exp = widthB + (16 - widthB % 16);
	}

	__local int tableA[16][16];
	__local int tableB[16][16];

	int zapisJ = lokJ;

	while(j<heightA_exp){
		tableA[lokI][lokJ] = getNum(i,j,matrixA,widthA,heightA);
		barrier(CLK_LOCAL_MEM_FENCE);

		int jB = i;
		int iB = lokJ;
		int zapisI = lokI;
		while(iB<widthB_exp){
			tableB[lokJ][lokI] = getNum(iB,jB,matrixB,widthB,heightB);
			barrier(CLK_LOCAL_MEM_FENCE);
			
			/*if(lokI==0&&lokJ==0){
				for(int f = 0;f<16;f++){
					for(int g = 0;g<16;g++ )
						printf("%d ",tableB[g][f]);
					printf("\n");
				}
			}*/

			//calculate and write back
			int temp = 0;
			for(int ni = 0;ni<16;ni++)
				temp = temp + tableA[ni][lokJ]*tableB[lokI][ni];

			//uredu index, neki se sfuka pol i guess? ta if je cudn
			/*if(temp!=0)
				printf("%d na poz: %d\n",temp,(zapisI+widthB*zapisJ+slice*widthB*heightA));*/
			if(temp!=0){
				matrixP[zapisI+widthB*zapisJ+slice*widthB*heightA] = temp; 
			}
			barrier(CLK_GLOBAL_MEM_FENCE);
			iB+=16;
			zapisI+=16;
		}

		zapisJ+=16;
		j+=16;
	}
	
			
}
