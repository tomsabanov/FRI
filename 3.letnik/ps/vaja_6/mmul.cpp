#include <stdio.h>
#include <stdlib.h>
#include <FreeImage.h>
#include <CL/cl.h>
#include <time.h>
#include <math.h>
#include "old.cpp"
#include "new.cpp"
#include <chrono>

#define SIZE			512
#define WORKGROUP_SIZE	32
#define MAX_SOURCE_SIZE	16384


float getSumOfMatrix(float* A, int h, int w){
	float temp = 0.0f;
	for(int i = 0; i<h*w; i++){
		temp += A[i];
	}
	return temp;
}

void matrixCPU(float* a,float* b,float* mult,int widthA,int heightA,int widthB,int heightB) {
	auto t1 = std::chrono::high_resolution_clock::now();
	for (int i = 0; i < heightA; i++) {
		for (int j = 0; j < widthB; j++) {
			for (int k = 0; k < widthA; k++) {
				mult[j + i * widthB] += a[i*widthA+k] * b[k*widthB+j];
			}
		}
	}

}

void printMatrix(float* A, int h, int w){
	    // Prikaz rezultatov
    for(int i=0; i<h; i++){
		for(int j=0; j<w; j++){
			printf("%f ", A[i*w+j]);
		}
		printf("\n-------------------------------------\n");
	}

}


int main(void) {
	// device[0] = NVIDIA CUDA,OpenCL 1.2, max_work_group_size=256, max_work_item_dimensions=3
	// device[1] = Intel OpenCLHD Graphics,OpenCL 2.1, max_work_group_size=1024, max_work_item_dimensions=3
	// device[2] = Intel HD Graphics,OpenCL 2.0, max_work_group_size=512, max_work_item_dimensions=3

	int hA = 7*SIZE;
	int wA = SIZE;
	int hB = SIZE;
	int wB = 5*SIZE;

    // Rezervacija pomnilnika
	float *A = (float*)malloc(hA*wA*sizeof(float));
    float *B = (float*)malloc(hB*wB*sizeof(float));
    float *Ccpu = (float*)malloc(hA*wB*sizeof(float));
    float *Cgpu = (float*)malloc(hA*wB*sizeof(float));
    float *Cgpu_old = (float*)malloc(hA*wB*sizeof(float));


    // Inicializacija matrik
	srand((int)time(NULL));
	float ind = 1.0f;
	for(int i=0; i<hA; i++){
		for(int j=0; j<wA; j++){
			A[i*wA+j] = ind;
			ind = ind + 1.0f;
		}
	} 

	ind = 1.0f;
	for(int i=0; i<hB; i++){
		for(int j=0; j<wB; j++){
			B[i*wB+j] = ind;
			ind = ind + 1.0f;
		}
	} 

	//matrixCPU(A,B,Ccpu,wA,hA,wB,hB);
	//printMatrix(Ccpu, hA, wB);


	//old(A,B,Cgpu_old,hA, wA, hB,wB, WORKGROUP_SIZE);
	new_multiply(A,B,Cgpu,hA, wA, hB,wB, WORKGROUP_SIZE);
	//printMatrix(Cgpu, hA, wB);
	
	//float gpu_old = getSumOfMatrix(Cgpu_old,hA,wB);
	//printf("GPU old:  %f\n",gpu_old);

	float gpu = getSumOfMatrix(Cgpu,hA,wB);
	printf("GPU new:  %f\n",gpu);

	float cpu = getSumOfMatrix(Ccpu,hA,wB);
	printf("CPU %f\n",cpu);



	free(A);
	free(B);
	free(Ccpu);
	free(Cgpu);
	free(Cgpu_old);
}			
