float getNum(int i, int j,__global float *matrix, int width, int height)
{
	if (i < 0 || i >= width)
		return 0;
	if (j < 0 || j >= height)
		return 0;
	return matrix[j*width + i];
}



__kernel void multiply(__global float *matrixA,
	__global float *matrixB,
	__global float *matrixP,
	int heightA,
	int widthA,
	int heightB,
	int widthB,
	int wsize)
{
	
	int i = get_global_id(0);
	int j = get_global_id(1);
	int lokI = get_local_id(0);
	int lokJ = get_local_id(1);

	int slice = i/wsize;

	//extended height
	int heightA_exp = 1;
	if (heightA % wsize == 0)
		heightA_exp = heightA;
	else {
		heightA_exp = heightA + (wsize - heightA % wsize);
	}
	//extended width
	int widthB_exp = 1;
	if (widthB % wsize == 0)
		widthB_exp = widthB;
	else {
		widthB_exp = widthB + (wsize - widthB % wsize);
	}

	__local float tableA[32][32];
	__local float tableB[32][32];

	int zapisJ = lokJ;
	
	
	while(j<heightA_exp){
		tableA[lokI][lokJ] = getNum(i,j,matrixA,widthA,heightA);

		int jB = i;
		int iB = lokJ;
		int zapisI = lokI;
		while(iB<widthB_exp){
			tableB[lokJ][lokI] = getNum(iB,jB,matrixB,widthB,heightB);
			barrier(CLK_LOCAL_MEM_FENCE);
			
			//calculate and write back
			float temp = 0;
			for(int ni = 0;ni<wsize;ni++){
				temp = temp + tableA[ni][lokJ]*tableB[lokI][ni];
			}

			matrixP[zapisI+widthB*zapisJ+slice*widthB*heightA] = temp; 
						
			barrier(CLK_GLOBAL_MEM_FENCE);
			iB+=wsize;
			zapisI+=wsize;
		}

		zapisJ+=wsize;
		j+=wsize;
	}
	
	
}
