#include <stdio.h> 
#include <string.h> 
#include <vector>
#include <cmath>
#include <iostream>
#include <chrono> 
#include "omp.h"

using namespace std; 
using namespace std::chrono; 

// Fiksni intervali - za K threadov bomo ustvarili K intervalov 
// med [1,N]

long* NUMBERS;

long getFriendNumber(long x){ 
    long sum = 1; 
    for (long i = 2; i <= sqrt(x); i++) { 
        if (x % i == 0){ 
            sum += i; 
            if (x / i != i) sum += x / i; 
        } 
    } 
    return sum; 
} 
  
int main(int argc, char** argv){ 
    // Get starting timepoint 
    auto start_time = high_resolution_clock::now(); 

    if(argc < 2) return 0;

    long N = 1;
    if(argc == 2) N = atoi(argv[1]);
    NUMBERS = (long *)calloc(sizeof(long), N);


    #pragma omp parallel for
    for(long i = 0; i < N; i++){
            long friendNum = getFriendNumber(i);
            // rabimo pogledat, ce je v nasem longervalu [1-N] in kje se nahaja 
            // relativno nasega longervala start-end
            if(friendNum > N) continue;
            if(friendNum < i) continue;
            if(friendNum == i) continue;

            long reverse = getFriendNumber(friendNum);
            if(getFriendNumber(friendNum) == i ){
                NUMBERS[i] = i;
                NUMBERS[friendNum] = friendNum;
                //printf("%d and %d are friends\n",i, friendNum);
            }
    }

  

    long sum = 0;
    printf("Prijatelska stevila : \n");
    for(long i = 0; i < N; i++){
        if(NUMBERS[i] == 0)continue;
        sum += NUMBERS[i];
        printf("%ld\n", NUMBERS[i]);
    }

    printf("Sum is : %ld \n", sum);




    // Get ending timepoint 
    auto stop = high_resolution_clock::now(); 
  
    // Get duration. Substart timepoints to  
    // get durarion. To cast it to proper unit 
    // use duration cast method 
    auto duration = duration_cast<microseconds>(stop - start_time); 
  
    cout << "Time taken by function: "
         << duration.count() << " microseconds" << endl; 


    return 0; 
} 