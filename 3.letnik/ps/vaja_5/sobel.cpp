#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <FreeImage.h>
#include <CL/cl.h>

#define SIZE		512
#define WORKGROUP_SIZE	32
#define MAX_SOURCE_SIZE	16384


inline int getPixel(unsigned char *image, int width, int height, int y, int x)
{
	if (x < 0 || x >= width)
		return 0;
	if (y < 0 || y >= height)
		return 0;
	return image[y*width + x];
}

void sobelCPU(unsigned char *imageIn, unsigned char *imageOut, int width, int height)
{
	int i, j;
	int Gx, Gy;
	int tempPixel;

	//za vsak piksel v sliki
	for (i = 0; i < (height); i++)
		for (j = 0; j < (width); j++)
		{
			Gx = -getPixel(imageIn, width, height, i - 1, j - 1) - 2 * getPixel(imageIn, width, height, i - 1, j) -
				getPixel(imageIn, width, height, i - 1, j + 1) + getPixel(imageIn, width, height, i + 1, j - 1) +
				2 * getPixel(imageIn, width, height, i + 1, j) + getPixel(imageIn, width, height, i + 1, j + 1);
			Gy = -getPixel(imageIn, width, height, i - 1, j - 1) - 2 * getPixel(imageIn, width, height, i, j - 1) -
				getPixel(imageIn, width, height, i + 1, j - 1) + getPixel(imageIn, width, height, i - 1, j + 1) +
				2 * getPixel(imageIn, width, height, i, j + 1) + getPixel(imageIn, width, height, i + 1, j + 1);
			tempPixel = sqrt((float)(Gx * Gx + Gy * Gy));
			if (tempPixel > 255)
				imageOut[i*width + j] = 255;
			else
				imageOut[i*width + j] = tempPixel;
		}
}


int main(int argc, char** argv){
char ch;
	cl_int ret;

    // Branje datoteke
    FILE *fp;
    char *source_str;
    size_t source_size;

    fp = fopen("kernel.cl", "r");
    if (!fp) 
	{
		fprintf(stderr, ":-(#\n");
        exit(1);
    }
    source_str = (char*)malloc(MAX_SOURCE_SIZE);
    source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
	source_str[source_size] = '\0';
    fclose( fp );

	// Rezervacija pomnilnika
	FIBITMAP *imageBitmap = FreeImage_Load(FIF_PNG, "index.png", 0);
	FIBITMAP *imageBitmapGrey = FreeImage_ConvertToGreyscale(imageBitmap);
	int width = FreeImage_GetWidth(imageBitmapGrey);
	int height = FreeImage_GetHeight(imageBitmapGrey);
	int pitch = FreeImage_GetPitch(imageBitmapGrey);

	printf("Width/Height : %d/%d\n", width, height);

	unsigned char *imageIn = (unsigned char*)malloc(height*width * sizeof(unsigned char));
	unsigned char *imageOut = (unsigned char*)malloc(height*width * sizeof(unsigned char));

	FreeImage_ConvertToRawBits(imageIn, imageBitmapGrey, pitch, 8, 0xFF, 0xFF, 0xFF, TRUE);

	FreeImage_Unload(imageBitmapGrey);
	FreeImage_Unload(imageBitmap);

 
    // Podatki o platformi
    cl_platform_id	platform_id[10];
    cl_uint			ret_num_platforms;
	char			*buf;
	size_t			buf_len;
	ret = clGetPlatformIDs(10, platform_id, &ret_num_platforms);
			// max. "stevilo platform, kazalec na platforme, dejansko "stevilo platform
	
	// Podatki o napravi
	cl_device_id	device_id[10];
	cl_uint			ret_num_devices;
	// Delali bomo s platform_id[0] na GPU
	ret = clGetDeviceIDs(platform_id[0], CL_DEVICE_TYPE_GPU, 10,	
						 device_id, &ret_num_devices);				
			// izbrana platforma, tip naprave, koliko naprav nas zanima
			// kazalec na naprave, dejansko "stevilo naprav

    // Kontekst
    cl_context context = clCreateContext(NULL, 1, &device_id[0], NULL, NULL, &ret);
			// kontekst: vklju"cene platforme - NULL je privzeta, "stevilo naprav, 
			// kazalci na naprave, kazalec na call-back funkcijo v primeru napake
			// dodatni parametri funkcije, "stevilka napake
 
    // Ukazna vrsta
    cl_command_queue command_queue = clCreateCommandQueue(context, device_id[0], CL_QUEUE_PROFILING_ENABLE, &ret);
			// kontekst, naprava, INORDER/OUTOFORDER, napake

    // Alokacija pomnilnika na napravi
    cl_mem in_mem_obj = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, height*width*sizeof(unsigned char), imageIn, &ret);
	cl_mem out_mem_obj = clCreateBuffer(context, CL_MEM_WRITE_ONLY, height*width*sizeof(unsigned char), NULL, &ret);

  
    // Priprava programa
    cl_program program = clCreateProgramWithSource(context,	1, (const char **)&source_str,  
												   NULL, &ret);
			// kontekst, "stevilo kazalcev na kodo, kazalci na kodo,		
			// stringi so NULL terminated, napaka													
 
    // Prevajanje
    ret = clBuildProgram(program, 1, &device_id[0], NULL, NULL, NULL);
			// program, "stevilo naprav, lista naprav, opcije pri prevajanju,
			// kazalec na funkcijo, uporabni"ski argumenti

	// Log
	size_t build_log_len;
	char *build_log;
	ret = clGetProgramBuildInfo(program, device_id[0], CL_PROGRAM_BUILD_LOG, 
								0, NULL, &build_log_len);
			// program, "naprava, tip izpisa, 
			// maksimalna dol"zina niza, kazalec na niz, dejanska dol"zina niza
	build_log =(char *)malloc(sizeof(char)*(build_log_len+1));
	ret = clGetProgramBuildInfo(program, device_id[0], CL_PROGRAM_BUILD_LOG, 
							    build_log_len, build_log, NULL);
	printf("%s\n", build_log);
	free(build_log);
 
    // "s"cepec: priprava objekta
    cl_kernel kernel = clCreateKernel(program, "sobel", &ret);
			// program, ime "s"cepca, napaka
 
    // "s"cepec: argumenti
    clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&in_mem_obj);
	clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&out_mem_obj);
    clSetKernelArg(kernel, 2, sizeof(cl_int), (void *)&width);
    clSetKernelArg(kernel, 3, sizeof(cl_int), (void *)&height);

			// "s"cepec, "stevilka argumenta, velikost podatkov, kazalec na podatke

	// Delitev dela
	size_t local_item_size[2] = {WORKGROUP_SIZE, WORKGROUP_SIZE};
	//size_t global_item_size[2] = {width - width%WORKGROUP_SIZE, height - height%WORKGROUP_SIZE};
	size_t global_item_size[2] = {width + WORKGROUP_SIZE - width%WORKGROUP_SIZE, height + WORKGROUP_SIZE - height%WORKGROUP_SIZE};
	//size_t global_item_size[2] = {width + width%WORKGROUP_SIZE, height + height%WORKGROUP_SIZE};

	// "s"cepec: zagon
	cl_event event;
    ret = clEnqueueNDRangeKernel(command_queue, kernel, 2, NULL,						
								 global_item_size, local_item_size, 0, NULL,  &event);	
			// vrsta, "s"cepec, dimenzionalnost, mora biti NULL, 
			// kazalec na "stevilo vseh niti, kazalec na lokalno "stevilo niti, 
			// dogodki, ki se morajo zgoditi pred klicem
	printf("Runnning %d\n", ret);
	clWaitForEvents(1, &event);
	clFinish(command_queue);

    // Kopiranje rezultatov
    ret = clEnqueueReadBuffer(command_queue, out_mem_obj, CL_TRUE, 0,						
							  height*width*sizeof(unsigned char), imageOut, 0, NULL, NULL);				
			// branje v pomnilnik iz naparave, 0 = offset
			// zadnji trije - dogodki, ki se morajo zgoditi prej
	printf("Copying %d\n", ret);


	cl_ulong time_start;
	cl_ulong time_end;

	clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
	clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);

	double nanoSeconds = time_end-time_start;
	printf("OpenCl Execution time is: %0.3f milliseconds \n",nanoSeconds / 1000000.0);




	//sobelCPU(imageIn, imageOut, width, height);




	FIBITMAP *imageOutBitmap = FreeImage_ConvertFromRawBits(imageOut, width, height, pitch, 8, 0xFF, 0xFF, 0xFF, TRUE);
	FreeImage_Save(FIF_PNG, imageOutBitmap, "sobel_slika.png", 0);
	FreeImage_Unload(imageOutBitmap);



    // "ci"s"cenje
    ret = clFlush(command_queue);
    ret = clFinish(command_queue);
    ret = clReleaseKernel(kernel);
    ret = clReleaseProgram(program);
    ret = clReleaseMemObject(in_mem_obj);
	ret = clReleaseMemObject(out_mem_obj);
    ret = clReleaseCommandQueue(command_queue);
    ret = clReleaseContext(context);

	return 0;
}

