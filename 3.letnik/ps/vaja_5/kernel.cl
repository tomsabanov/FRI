inline int indexOf(int row, int col, int width, int height){
	if(row < 0){
		row = 0;
	}
	if(col < 0){
		col = 0;
	}
	if(col >= width){
		col = width-1;
	}
	if(row >= height){
		row = height-1;
	}


	int index = row * width + col;
	return index;
}
inline int getPixel(__global unsigned char *image, int width, int height, int y, int x)
{
	if (x < 0 || x >= width)
		return 0;
	if (y < 0 || y >= height)
		return 0;
	return image[y*width + x];
}


// kernel
__kernel void sobel(__global unsigned char *image, write_only __global uchar *out , int width, int height){											
    
	int row = get_global_id(1);
	int col = get_global_id(0);

	int cacheRow = get_local_id(1)+1;
	int cacheCol = get_local_id(0)+1;

	
	int size = get_local_size(0);

	__local uchar cache[34][34];
	
	
	cache[cacheRow][cacheCol] = image[indexOf(row, col,width,height)];
	//cache[cacheRow][cacheCol] = getPixel(image,width, height, row, col);



	const bool atTopRow = (cacheRow == 1);
	const bool atBottomRow = (cacheRow == size);

	if(atTopRow) {
		cache[0][cacheCol] = image[ indexOf(row-1, col,width,height) ]; 
		//cache[0][cacheCol] = getPixel(image,width, height, row-1, col);

	} else if (atBottomRow) {
		cache[size + 1][cacheCol] = image[ indexOf(row + 1, col,width,height) ];
	 	//cache[size + 1][cacheCol] =  getPixel(image,width, height, row+1, col);

	}


	const bool atLeftCol = (cacheCol == 1);
	const bool atRightCol = (cacheCol == size);

	if(atLeftCol) { 
		cache[cacheRow][0] = image[ indexOf(row, col - 1,width,height) ];
		//cache[cacheRow][0] = getPixel(image,width, height, row, col-1);


	} else if (atRightCol) {
		cache[cacheRow][size + 1] = image[ indexOf(row, col + 1,width,height) ];
		//cache[cacheRow][size+1] = getPixel(image,width, height, row, col+1);
	}

	const bool atTLCorner = atTopRow && atLeftCol;
	const bool atTRCorner = atTopRow && atRightCol;
	const bool atBLCorner = atBottomRow && atLeftCol;
	const bool atBRCorner = atBottomRow && atRightCol;

	if(atTLCorner) {
		cache[0][0] = image[ indexOf(row - 1, col - 1, width,height) ];
		//cache[0][0] = getPixel(image,width, height, row-1, col-1);

	} else if (atTRCorner) { 
		cache[0][size + 1] = image[ indexOf(row - 1, col + 1, width, height) ];
		//cache[0][size+1] = getPixel(image,width, height, row-1, col+1);

	} else if (atBLCorner) {
		cache[size + 1][0] = image[ indexOf(row + 1, col - 1, width, height) ];
		//cache[size+1][0] = getPixel(image,width, height, row+1, col-1);

	} else if (atBRCorner) {
		cache[size + 1][size + 1] = image[ indexOf(row + 1, col + 1, width, height) ];
		//cache[size+1][size+1] = getPixel(image,width, height, row+1, col+1);

	}

	barrier(CLK_LOCAL_MEM_FENCE); 

	// CALCULATION ----------------------------------------------

	int i = cacheRow;
	int j = cacheCol;

	int Gx = -cache[i-1][j-1] - 2 * cache[i-1][j] - cache[i-1][j+1] + cache[i+1][j-1] + 2*cache[i+1][j] + cache[i+1][j+1];		
	int Gy = -cache[i-1][j-1] - 2 * cache[i][j-1] - cache[i+1][j-1] + cache[i-1][j+1] + 2* cache[i][j+1] + cache[i+1][j+1];
			
	int tempPixel = sqrt((float)(Gx * Gx + Gy * Gy));
	if (tempPixel > 255){
		out[row*width + col] = 255;
	}
	else{
		out[row*width + col] = tempPixel;
	}
	
}														
