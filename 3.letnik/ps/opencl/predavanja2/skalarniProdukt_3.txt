// lokalno seštevanje, skalarji
// C --> P, dodamo partial, P je dolg num_groups
// seštevamo po drevesu z deljenjem (zmanjševanje koraka)

// kernel
__kernel void dotProduct(__global const float *A,
						 __global const float *B,		
						 __global float *P,		
						 int size,		
						 __local float *partial)						
{		
	int lid = get_local_id(0);
    int gid = get_global_id(0); 
 
	float sum = 0.0f;
	while( gid < size )
	{
		sum += A[gid] * B[gid];
		gid += get_global_size(0);
	}
	partial[lid] = sum;

	barrier(CLK_LOCAL_MEM_FENCE);

	for(int i = get_local_size(0)/2; i>0; i >>= 1) 
	{
		if(lid < i) 
			partial[lid] += partial[lid + i];
		barrier(CLK_LOCAL_MEM_FENCE);
	}

	if(lid == 0) 
		P[get_group_id(0)] = partial[0];
}														
