// uporabimo �e vektorsko enoto (float4)
// v glavnem programu je sprememba:
	size_t num_groups = ((vectorSize/4-1)/local_item_size+1);		

// kernel
__kernel void dotProduct(__global const float4 *A,
						 __global const float4 *B,		
						 __global float *P,		
						 int size,		
						 __local float4 *partial)						
{
 
    // Get the index of the current element to be processed
	int lid = get_local_id(0);
    int gid = get_global_id(0); 
 
    // Do the operation
	float4 sum = (float4)(0.0f);
	while( gid < size/4 )
	{
		sum += A[gid] * B[gid];
		gid += get_global_size(0);
	}
	partial[lid] = sum;

	barrier(CLK_LOCAL_MEM_FENCE);

	int floorPow2 = get_local_size(0);
    if ( floorPow2 & (floorPow2-1) )										
	{
		while ( floorPow2 & (floorPow2-1) ) 
			floorPow2 &= floorPow2-1;
		if ( lid >= floorPow2 )
            partial[lid - floorPow2] += partial[lid];
		barrier(CLK_LOCAL_MEM_FENCE);
    }

	for(int i = (floorPow2>>1); i>0; i >>= 1) 
	{
		if(lid < i) 
			partial[lid] += partial[lid + i];
		barrier(CLK_LOCAL_MEM_FENCE);
	}

	if(lid == 0) 
		P[get_group_id(0)] = dot(partial[0], (float4)(1.0f));	
}
