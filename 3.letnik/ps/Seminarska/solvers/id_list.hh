//-----------------------------------------------------------------------------
// File: id_list.hh
//
// Purpose: Implements the interface of a list, using (integer) IDs for
//          manipulating entries. IDs must be in the range 1..maxID
//
// Remarks: Dynamic arrays are utilized. Position 0 is *not* to be
//          utilized; hence arrays must have size larger by 1.
//
// History: 03/22/99 - JPMS - created.
//
// Copyright (c) 1999 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#ifndef __ID_LIST__
#define __ID_LIST__

#include "darray.hh"

enum { EmptyMark = 0, FirstID = 1 };    // ID=0 is disallowed.


//-----------------------------------------------------------------------------
// Class: ListID
//
// Purpose: 
//-----------------------------------------------------------------------------

class ListID {
public:

  //---------------------------------------------------------------------------
  // Constructor/destructor.
  //---------------------------------------------------------------------------

  ListID(int max_size);
  ~ListID();

protected:                         // Put these functions before to inline them

  inline int incr_list_size() {    // Increment size of ID list
    CHECK(if(_size == _list.size())
	  {Abort("TOO LONG ID LIST??");});
    if (_size == 0) {
      _first = _last = FirstID;
    } else {
      if (_last == _list.size()-1) {
	_last = FirstID;
      }
      else {
	_last++;
      }
    }
    _size++;
    return _last;
  }

  inline int decr_list_size() {    // Decrement size of ID list
    CHECK(if(_size == 0)
	  {Abort("TOO SHORT ID LIST??");});
    _size--;
    if ((_size == 0) || (_first == _last)) {
      _first = _last = FirstID;
    } else {
      if (_first == _list.size()-1) {
	_first = FirstID;
      }
      else {
	_first++;
      }
    }
    return _last;
  }

  inline void swap_id (int eID) {
    CHECK(if (_size<=0) Abort("Empty ID list??"));
    int tmpA = _list[_first];
    int tmpB = _mark[eID];

    _list[_first] = eID;
    _list[tmpB] = tmpA;

    _mark[eID] = _first;
    _mark[tmpA] = tmpB;
  }

public:

  //---------------------------------------------------------------------------
  // Interface contract.
  //---------------------------------------------------------------------------

  inline int lookup_elem (int eID) {
    return (_mark[eID] > 0);
  }

  inline void add_elem(int eID) {
    if (_mark[eID] <= 0) {
      int npos = incr_list_size();
      _mark[eID] = npos;
      _list[npos] = eID;
    }
  }

  inline void del_elem(int eID) {  // Must swap last elem in delete operation
    if (_mark[eID] > 0) {
      if (_first != eID) {
	swap_id (eID);            // Move elem to delete to first position
      }
      decr_list_size();           // Delete element
      _mark[eID] = EmptyMark;
    }
  }

  inline int size() { return _size; }

  inline void dump() {
    DBGn(cout << "MAX SIZE: " << _max_size << "\n";);
    int i = _first, cnt = 0;
    cout << "ID List (" << _size << "): [ ";
    while (cnt < _size) {
      cout << _list[i] << " ";
      if (i == _max_size-1) { i = 0; }
      else                  { i++; }
      cnt++;
    }
    cout << "]\n";
  }
  
protected:
  
  //---------------------------------------------------------------------------
  // Data structures.
  //---------------------------------------------------------------------------
  
  DArray<int> _mark;
  DArray<int> _list;

  int _first;
  int _last;
  int _size;

  int _max_size;  // Max size of list

private:
  
};

#endif // __ID_LIST__

/*****************************************************************************/
