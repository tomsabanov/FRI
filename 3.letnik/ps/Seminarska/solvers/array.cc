//-----------------------------------------------------------------------------
// File: array.cc
//
// Purpose: Class definition for the array template.
//
// Remarks: --.
//
// History: 04/17/97 - JPMS - created.
//
// Copyright (c) 1997 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

/* ****
#include "array.hh"

template<class T>
Array<T>::Array() : _size(0) { _array = NULL; }

template<class T>
Array<T>::Array(int size) : _size(size)
{
    _array = (size==0) ? 0 : new T [size];
}

template<class T>
Array<T>::~Array(void) { if(_array) delete[] _array; }

*** */

/*****************************************************************************/
