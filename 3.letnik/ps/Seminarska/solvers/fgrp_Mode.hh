//-----------------------------------------------------------------------------
// File: fgrp_Mode.hh
//
// Purpose: Declaration of a working environment for the sat solver.
//
// Remarks: --
//
// History: 03/30/98 - JPMS - created.
//
// Copyright (c) 1998 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#ifndef __FGRP_MODE__
#define __FGRP_MODE__

#include <iostream.h>

#include "defs.hh"
#include "array.hh"

#include "fgrp_Defines.hh"


//-----------------------------------------------------------------------------
// Class: ModeOptions
//
// Purpose: Definition of working mode options.
//-----------------------------------------------------------------------------

class ModeOptions {
public:

  //-------------------------------------------------------------------------
  // Constructor/destructor.
  //-------------------------------------------------------------------------

  ModeOptions();
  ~ModeOptions();

  Array<int> &mode() { return _mode; }

protected:

  Array<int> _mode;

private:

};

#endif // __FGRP_MODE__

/*****************************************************************************/

