//-----------------------------------------------------------------------------
// File: darray.hh
//
// Purpose: Template definition of a dynamic one-dimensional array.
//
// Remarks: The index of each entry *can* change dynamically. This means
//          that code that requires maintenance of the index of each entry
//          *must* update each index when it is modified.
//
// History: 02/01/97 - JPMS - created.
//
// Copyright (c) 1997 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#ifndef __DARRAY__
#define __DARRAY__

#include <stdlib.h>
#include <iostream.h>

#include "defs.hh"


#define D_CONSTANT_SIZE    1     // Used for denoting fixed-size dynamic arrays
#define D_GROW_ONLY        2     // Dynamic arrays whose size can only grow
#define D_DYNAMIC_SIZE     3     // Dynamic arrays whose size can grow/shrink



//-----------------------------------------------------------------------------
// Class: DArray
//
// Purpose: One dimensional array of abstract objects. No specific size
//          is required at construction time.
//-----------------------------------------------------------------------------

template<class T>
class DArray {

public:
  //-------------------------------------------------------------------------
  // Constructors/destructors.
  //-------------------------------------------------------------------------

  DArray (int dim = 1) : _size (0), _dimension (dim) {
    _growth = D_DYNAMIC_SIZE;
    _array = new T [dim];
  }
  DArray (int growth, int dim)
    : _size (0), _dimension (dim), _growth (growth) { _array = new T [dim]; }

  virtual ~DArray (void) { if (_array) delete[] _array; }

  //-------------------------------------------------------------------------
  // array information accessors/modifiers
  //-------------------------------------------------------------------------

  inline T& operator[] (int index) {
    CHECK(if(index < 0 || index >= _size)
	  {cout<<"Index: "<<index<<" in Size: "<<_size<<endl;
	  Abort("Invalid array range specification");});
    return _array[index];
  }
  inline const T& operator[] (int index) const {
    CHECK(if(index < 0 || index >= _size)
	  Abort("Invalid array range specification"););
    return _array[index];
  }

  //-------------------------------------------------------------------------
  // Swap two elements.
  //-------------------------------------------------------------------------
  
  inline void swap (int idx1, int idx2) { exchange (idx1, idx2); }

  //-------------------------------------------------------------------------
  // Size manipulation, i.e. increment and decrement array size.
  //-------------------------------------------------------------------------
  
  inline int incr_size (int nsize=1) { return increment_size (nsize); }
  inline int decr_size (int nsize=1) { return decrement_size (nsize); }

  inline int size(void) const { return _size; }

  inline int resize(int newdim) { return set_new_size (newdim); }

  //-------------------------------------------------------------------------
  // Debugging.
  //-------------------------------------------------------------------------

  friend ostream &operator<<(ostream &os, const DArray<T>& array);
  void dump(void) { cout << *this; }

protected:

  //-------------------------------------------------------------------------
  // Protected member functions for direct array manipulation.
  //-------------------------------------------------------------------------

  inline void exchange (int idx1, int idx2) {
    CHECK(if(idx1 < 0 || idx1 >= _size || idx2 < 0 || idx2 >= _size)
	  Abort("Invalid array range specification"););
    T tmp_data = _array[idx1];
    _array[idx1] = _array[idx2];
    _array[idx2] = tmp_data;
  }

  inline int increment_size (int nsize) {
    CHECK(if(_dimension == 0)
	  {Info("Incrementing 0-size array...");});
    if (_size+nsize > _dimension) {
      if (_growth == D_CONSTANT_SIZE) {
	Abort("Unable to increment constant dimension array.");
      }
      register T *_tmp_array = _array;
      register int new_dim = MAX(_size+nsize,2*_dimension);
      _array = new T [new_dim];
      for (register int i = 0; i < _size; i++) {
	_array[i] = _tmp_array[i];
      }
      _dimension = new_dim;
      delete[] _tmp_array;
    }
    _size += nsize;
    return _size - 1;        // return last array entry
  }

  inline int decrement_size (int nsize) {
    CHECK(if(_size == 0 || nsize>_size)
	  {Abort("Unable to decrement this array size!?");});
    _size -= nsize;
    if (_size <= _dimension / 2 && _growth == D_DYNAMIC_SIZE) {
      register T *_tmp_array = _array;
      _array = new T [_dimension / 2];
      for (register int i = 0; i < _size; i++) {
	_array[i] = _tmp_array[i];
      }
      _dimension = _dimension / 2;
      delete[] _tmp_array;
    }
    return _size - 1;        // return last array entry
  }

  inline int set_new_size(int newdim) {
    CHECK(if(_growth == D_CONSTANT_SIZE) {
      Abort ("Trying to resize a constant dimension dynamic array.");
    }                                         // newsize MUST be > current size
    else if((_growth==D_GROW_ONLY) && (newdim<_size))
    {Abort("Invalid new dimension in resizing");});

    if(newdim == 0) {                        // if newdim is zero, delete array
      if(_array) delete[] _array;
      _array = 0;
      _dimension = 0;
      _size = 0;
    } else {
      T *newarray = new T [newdim];
      if(newarray) {
	for(register int i = 0; i < _size; i++)     // copy previous array data
	  newarray [i] = _array [i];

	// delete previous array and update internal variables

	if(_array) { delete[] _array; }
	_array = newarray;
	if (newdim < _size) { _size = newdim; }
	_size = _dimension = newdim;
      }
      CHECK(else Abort("ARRAY MEMORY ALLOCATION FAILURE"););
    }
    return _size - 1;
  }

protected:
  T *_array;		// points to array storage

  int _size;		// usable size of dynamic array
  int _dimension;       // maximum size of dynamic array
  int _growth;          // type of size variation of array
};


//-----------------------------------------------------------------------------
// Function: operator<<
//
// Purpose: Print out contents of array.
//-----------------------------------------------------------------------------

template<class T>
ostream &operator<<(ostream &os, const DArray<T> &array) {
  os << "[array] = [ ";
  for(int k=0; k<array._size; k++) {
    os << array._array[k] << " ";
  }
  os << "]" << endl;
  return os;
}

#endif // __DARRAY__

/*****************************************************************************/
