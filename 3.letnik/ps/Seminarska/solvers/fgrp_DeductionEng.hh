//-----------------------------------------------------------------------------
// File: fgrp_DeductionEng.hh
//
// Purpose: Class declaration of the deduction engine operating on clause
//          databases.
//
// Remarks: The current deduction engine only implements BCP. K-depth
//           probing will be incorporated into a more general deduction
//           engine. K-depth probing can also be applied independently
//           as a preprocessing phase.
//
// History: 03/30/98 - JPMS - created.
//
// Copyright (c) 1998 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#ifndef __FGRP_DEDUCTIONENG__
#define __FGRP_DEDUCTIONENG__

#include <iostream.h>

#include "defs.hh"
#include "memory.hh"
#include "darray.hh"
#include "list.hh"

#include "fgrp_Defines.hh"
#include "fgrp_STS.hh"
#include "fgrp_CDB.hh"
#include "fgrp_BCP.hh"
#include "fgrp_BCA.hh"



//-----------------------------------------------------------------------------
// Typedefs for the deduction engine.
//-----------------------------------------------------------------------------

class DeductionEngine;

typedef DeductionEngine *DeductionEnginePtr;


enum { _DO_PREPROCESS_ = 0x1010, _DO_DEDUCTION_ };



class AssignmentDescription ;

typedef AssignmentDescription *AssignmentDescriptionPtr;
typedef ListItem<AssignmentDescription*> *AssignDescListPtr;
typedef List<AssignmentDescription*> AssignDescList;


extern void dump_assign_desc_list (AssignDescList &desc_list);



//-----------------------------------------------------------------------------
// Class: AssignmentDescription
//
// Purpose: Contains data structures associated with necessary assignments.
//-----------------------------------------------------------------------------

class AssignmentDescription {
public:

  //---------------------------------------------------------------------------
  // Constructor/destructor.
  //---------------------------------------------------------------------------

  AssignmentDescription (VariablePtr var, int val);
  ~AssignmentDescription();

  //---------------------------------------------------------------------------
  // Interface.
  //---------------------------------------------------------------------------

  inline VariablePtr variable() { return _variable; }
  inline int value() { return _value; }
  inline ClausePtr antecedent() { return _antec; }

  inline VariableList &var_cause_list() { return _var_cause_list; }
  inline List<int> &sign_cause_list() { return _sign_cause_list; }

  //inline VariableList &antecedent_variables() { return _antec_vars; }
  //inline VariableList &consequent_variables() { return _conseq_vars; }

protected:

  VariablePtr _variable;       // Variable associated with necessary assignment
  int _value;                                         // Associated logic value
  ClausePtr _antec;                  // Antecedent implying variable assignment

  VariableList _var_cause_list;         // Explanation for necessary assignment
  List<int> _sign_cause_list;           // Same...

  //VariableList _antec_vars;           // Variables directly implying assignment
  //VariableList _conseq_vars;            // Variables implied by this assignment

private:

};


//-----------------------------------------------------------------------------
// Class: CommonNecessaryAssingments
//
// Purpose: Contains all the data associated with identification of necessary
//          assignments.
//-----------------------------------------------------------------------------

class CommonNecessaryAssingments {
public:

  //-------------------------------------------------------------------------
  // Constructor/destructor.
  //-------------------------------------------------------------------------

  CommonNecessaryAssingments();
  ~CommonNecessaryAssingments();

protected:

  //  AssignDescList _CNA_list;      // List of commmon necessary assignments (CNA)
  //List<List<AssignmentDescription *> > _implic_sequences; // List of implic seq
  
private:

};




//-----------------------------------------------------------------------------
// Class: DeductionEngine
//
// Purpose: The deduction engine uses the clause database for identifying
//          necessary assignments and updating the information on the
//          decision tree.
//-----------------------------------------------------------------------------

class DeductionEngine {
public:

  //---------------------------------------------------------------------------
  // Constructor/destructor.
  //---------------------------------------------------------------------------

  DeductionEngine (Array<int> &mode,
		   ClauseDatabase &clDB, SearchTree &stree,
		   BCP &bcp, BCA &bca);
  virtual ~DeductionEngine();

  //---------------------------------------------------------------------------
  // Interface contract.
  //---------------------------------------------------------------------------

  virtual int deduce();       // Derive necessary assignments while searching
  virtual int preprocess();  // Derive necessary assignments before searching

  virtual void setup();                    // Set up internal data structures
  virtual void cleanup();                // Clean up internal data structures

  virtual void output_stats();            // Print out stats due to deduction

protected:

  //---------------------------------------------------------------------------
  // Internal functions.
  //
  // A- Implementation of (draft) recursive learning.
  //---------------------------------------------------------------------------

  int examine_DLevel_unresolved_clauses();

  int iterate_probing_assignments();         // Probes assignments to variables
  int probe_var_assignments (VariablePtr var);

  int compute_justification_relations (VariablePtr var);
  
  int evaluate_clause_justifications (ClausePtr cl);

  int analyze_justification (ClausePtr cl, VariablePtr var,
			     int value, int num_assigns = FALSE);
  int update_common_assignments_set (int num_assigns = FALSE);

  int compute_assignments_explanations (ClausePtr cl);

  int redo_justification (ClausePtr cl, VariablePtr var, int value);

  void add_conf_clause_dependencies();
  void add_justifying_clause_dependencies (ClausePtr cl);

  void update_explaining_clauses();    // Update clauses explaining assignments

  void establish_necessary_assignments();

  void identify_pure_literals();

  void clean_internal_data_structures();
  
  //---------------------------------------------------------------------------
  // Data structures.
  //---------------------------------------------------------------------------

  ClauseDatabase &_clDB;         // Clause database is the constraint network
  SearchTree &_stree;           // Search tree that keeps the search info

  BCP &_bcp;                             // Engine for constraint propagation
  BCA &_bca;                                  // Engine for conflict analysis

  Array<int> &_mode;                       // Configuration options for GRASP

  //---------------------------------------------------------------------------
  // Implementation of recursive learning.
  //---------------------------------------------------------------------------

  AssignDescList _assignments_list; // Assignments common to all justifications
  DArray<int> _var_marks;
  DArray<int> _var_values;
  ClauseList _conf_clauses;       // Clauses due to inconsistent justifications

  int _preprocessing;                // Set to TRUE if performing preprocessing
  VariablePtr _probe_var;                           // Specify probing variable
  
  //---------------------------------------------------------------------------
  // Stats gathering.
  //---------------------------------------------------------------------------

  int _num_conflicts;          // Counts number of times conflicts were found
  int _num_conf_cls;            // Counts number of conflicting clauses found
  int _num_pure_lits;                        // Number of pure literals found
  
private:

};

#endif // __FGRP_DEDUCTIONENG__

/*****************************************************************************/

