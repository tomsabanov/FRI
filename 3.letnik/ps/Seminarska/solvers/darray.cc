//-----------------------------------------------------------------------------
// File: darray.cc
//
// Purpose: Definition of some of the member functions for the dynamic array.
//
// Remarks: --.
//
// History: 02/01/97 - JPMS - created.
//
// Copyright (c) 1997 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#include "darray.hh"


//-----------------------------------------------------------------------------
// Constructors/destructors.
//-----------------------------------------------------------------------------


/* Currently, this does not work with templates. -jpms 4/19/97.

template<class T> DArray<T>::DArray (int dim)
: _size (0), _dimension (dim)
{
    _array = new T [dim];
}

template<class T> DArray<T>::~DArray (void)
{
    if (_array) delete[] _array;
}

*/

/*****************************************************************************/
