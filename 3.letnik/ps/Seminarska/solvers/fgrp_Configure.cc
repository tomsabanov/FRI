//-----------------------------------------------------------------------------
// File: fgrp_Configure.cc
//
// Purpose: Defines the configuration options of GRASP.
//
// Remarks: --
//
// History: 03/30/98 - JPMS - created.
//
// Copyright (c) 1998 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#include "fgrp_Defines.hh"
#include "fgrp_SAT.hh"



//-----------------------------------------------------------------------------
// Member functions for mode configuration.
//-----------------------------------------------------------------------------

void GRASP::register_allowed_switches()
{
    DBG1(cout<<"Entering GRASP::register_allowed_switches()"<<endl;);
    _sw_handler.register_switch ("B", SUFFIX_ARG, SW_EN_ONLY);
    _sw_handler.register_switch ("C", SUFFIX_ARG, SW_EN_ONLY);
    _sw_handler.register_switch ("S", SUFFIX_ARG, SW_EN_ONLY);
    _sw_handler.register_switch ("T", SUFFIX_ARG, SW_EN_ONLY);

    _sw_handler.register_switch ("O", ZERO_ARG, SW_EN_ONLY);
    _sw_handler.register_switch ("W", ZERO_ARG, SW_EN_ONLY);

    _sw_handler.register_switch ("b", SUFFIX_ARG, SW_EN_ONLY);
    _sw_handler.register_switch ("cj", SUFFIX_ARG, SW_EN_ONLY);
    _sw_handler.register_switch ("d", SUFFIX_ARG, SW_EN_ONLY);
    _sw_handler.register_switch ("dr", SUFFIX_ARG, SW_EN_ONLY);
    _sw_handler.register_switch ("drs", SUFFIX_ARG, SW_EN_ONLY);
    _sw_handler.register_switch ("g", SUFFIX_ARG, SW_EN_ONLY);
    _sw_handler.register_switch ("il", SUFFIX_ARG, SW_EN_ONLY);
    _sw_handler.register_switch ("im", SUFFIX_ARG, SW_EN_ONLY);
    _sw_handler.register_switch ("l", SUFFIX_ARG, SW_EN_ONLY);
    _sw_handler.register_switch ("p", SUFFIX_ARG, SW_EN_ONLY);
    _sw_handler.register_switch ("pbA", SUFFIX_ARG, SW_EN_ONLY);
    _sw_handler.register_switch ("pbL", SUFFIX_ARG, SW_EN_ONLY);
    _sw_handler.register_switch ("pbC", SUFFIX_ARG, SW_EN_ONLY);
    _sw_handler.register_switch ("pbV", SUFFIX_ARG, SW_EN_ONLY);
    _sw_handler.register_switch ("pl", SUFFIX_ARG, SW_EN_ONLY);
    _sw_handler.register_switch ("pm", SUFFIX_ARG, SW_EN_ONLY);
    _sw_handler.register_switch ("pr", SUFFIX_ARG, SW_EN_ONLY);
    _sw_handler.register_switch ("rt", SUFFIX_ARG, SW_EN_ONLY);
    _sw_handler.register_switch ("s", SUFFIX_ARG, SW_EN_ONLY);
    _sw_handler.register_switch ("ws", SUFFIX_ARG, SW_EN_ONLY);

    _sw_handler.register_switch ("sa", ZERO_ARG, SW_EN_ONLY);

    _sw_handler.register_switch ("c", ZERO_ARG, SW_ED_BOTH);
    _sw_handler.register_switch ("dc", ZERO_ARG, SW_ED_BOTH);
    _sw_handler.register_switch ("dv", ZERO_ARG, SW_ED_BOTH);
    _sw_handler.register_switch ("m", ZERO_ARG, SW_ED_BOTH);
    _sw_handler.register_switch ("r", ZERO_ARG, SW_ED_BOTH);
    _sw_handler.register_switch ("sc", ZERO_ARG, SW_ED_BOTH);
    _sw_handler.register_switch ("t", ZERO_ARG, SW_ED_BOTH);
    _sw_handler.register_switch ("u", ZERO_ARG, SW_ED_BOTH);
}


void GRASP::configure_options()
{
    DBG1(cout<<"Entering GRASP::configure_options()"<<endl;);
    SwitchMap *sw_map;

    sw_map = _sw_handler.switch_arg ("V");
    if (sw_map->active()) {
	_mode[_GRP_VERBOSITY_] = (int) (*sw_map);
    }

    sw_map = _sw_handler.switch_arg ("B");
    if (sw_map->active()) {
	_mode[_GRP_BACKTRACK_LIMIT_] = (int) *sw_map;
    } else {
	_mode[_GRP_BACKTRACK_LIMIT_] = 100000;        // Default max backtracks
    }

    sw_map = _sw_handler.switch_arg ("C");
    if (sw_map->active()) {
	_mode[_GRP_CONFLICT_LIMIT_] = (int) *sw_map;
    } else {
	_mode[_GRP_CONFLICT_LIMIT_] = 200000;          // Default max conflicts
    }

    sw_map = _sw_handler.switch_arg ("S");
    if (sw_map->active()) {
	_mode[_GRP_SPACE_LIMIT_] = (int) *sw_map;
    } else {
	_mode[_GRP_SPACE_LIMIT_] = 100;                 // Default space growth
    }

    sw_map = _sw_handler.switch_arg ("T");
    if (sw_map->active()) {
	_mode[_GRP_TIME_LIMIT_] = (int) *sw_map;
    } else {
	_mode[_GRP_TIME_LIMIT_] = 10000;                // Default max CPU time
    }

    sw_map = _sw_handler.switch_arg ("O");
    if (sw_map->active()) {
	_mode[_GRP_OUTPUT_SOLUTION_] = TRUE;
    } else {
	_mode[_GRP_OUTPUT_SOLUTION_] = FALSE;         // Solution is not output
    }

    sw_map = _sw_handler.switch_arg ("W");
    if (sw_map->active()) {
	_mode[_GRP_OUTPUT_CLDB_] = TRUE;
    } else {
	_mode[_GRP_OUTPUT_CLDB_] = FALSE;                 // clDB is not output
    }

    sw_map = _sw_handler.switch_arg ("b");
    _mode[_GRP_BACKTRACKING_STRATEGY_] = _NON_CHRONOLOGICAL_B_;
    if (sw_map->active()) {
	char *opt = (char*) *sw_map;
	if (!strcmp (opt, "D")) {
	    _mode[_GRP_BACKTRACKING_STRATEGY_] = _DYNAMIC_B_;
	} else if (!strcmp (opt, "C")) {
	    _mode[_GRP_BACKTRACKING_STRATEGY_] = _CHRONOLOGICAL_B_;
	} else if (strcmp (opt, "N")) {
	    Warn("Invalid backtracking strategy option. Using defaults.");
	}
    }

    sw_map = _sw_handler.switch_arg ("d");
    _mode[_GRP_DECISION_MODE_] = _DYNAMIC_ORD_;
    _mode[_GRP_DECISION_LEVEL_] = _DLIS_;
    if (sw_map->active()) {
	char *opt = (char*) (*sw_map);
	if (!strcmp (opt, "LDLIS")) {
	    _mode[_GRP_DECISION_LEVEL_] = _LDLIS_;
	} else if (!strcmp (opt, "LMSMM")) {
	    _mode[_GRP_DECISION_LEVEL_] = _LMSMM_;
	} else if (!strcmp (opt, "LMSTS")) {
	    _mode[_GRP_DECISION_LEVEL_] = _LMSTS_;
	} else if (!strcmp (opt, "LMSOS")) {
	    _mode[_GRP_DECISION_LEVEL_] = _LMSOS_;
	} else if (!strcmp (opt, "LBOHM")) {
	    _mode[_GRP_DECISION_LEVEL_] = _LBOHM_;
	} else if (!strcmp (opt, "LDLCS"))  {
	    _mode[_GRP_DECISION_LEVEL_] = _LDLCS_;
	} else if (!strcmp (opt, "DLIS")) {
	    _mode[_GRP_DECISION_LEVEL_] = _DLIS_;
	} else if (!strcmp (opt, "RDLIS")) {
	    _mode[_GRP_DECISION_LEVEL_] = _RDLIS_;
	} else if (!strcmp (opt, "DLISB")) {
	    _mode[_GRP_DECISION_LEVEL_] = _DLISB_;
	} else if (!strcmp (opt, "DLCS")) {
	    _mode[_GRP_DECISION_LEVEL_] = _DLCS_;
	} else if (!strcmp (opt, "DSIS")) {
	    _mode[_GRP_DECISION_LEVEL_] = _DSIS_;
	} else if (!strcmp (opt, "MSOS")) {
	    _mode[_GRP_DECISION_LEVEL_] = _MSOS_;
	} else if (!strcmp (opt, "MSTS")) {
	    _mode[_GRP_DECISION_LEVEL_] = _MSTS_;
	} else if (!strcmp (opt, "MSMM")) {
	    _mode[_GRP_DECISION_LEVEL_] = _MSMM_;
	} else if (!strcmp (opt, "BOHM")) {
	    _mode[_GRP_DECISION_LEVEL_] = _BOHM_;
	} else if (!strcmp (opt, "MVBS")) {
	    _mode[_GRP_DECISION_LEVEL_] = _MVBS_;
	} else if (!strcmp (opt, "LMVBS")) {
	    _mode[_GRP_DECISION_LEVEL_] = _LMVBS_;
	} else if (!strcmp (opt, "RAND")) {
	    _mode[_GRP_DECISION_LEVEL_] = _SRAND_;
	} else if (strcmp (opt, "DLIS")) {

	    _mode[_GRP_DECISION_MODE_] = _STATIC_ORD_;
	    if (!strcmp (opt, "LIS")) {
		_mode[_GRP_DECISION_LEVEL_] = _LIS_;
	    } else if (!strcmp (opt, "LCS")) {
		_mode[_GRP_DECISION_LEVEL_] = _LCS_;
	    } else if (!strcmp (opt, "JW")) {
		_mode[_GRP_DECISION_LEVEL_] = _JW_;
	    } else if (!strcmp (opt, "CA")) {
		_mode[_GRP_DECISION_LEVEL_] = _CA_;
	    } else if (!strcmp (opt, "FIX")) {
		_mode[_GRP_DECISION_LEVEL_] = _FIXED_;
	    } else {
		Warn("Invalid selection strategy option. Using defaults.");
	    }
	}
    }

    sw_map = _sw_handler.switch_arg ("dr");
    if (sw_map->active()) {
	_mode[_GRP_RANDOMIZE_] = (int) (*sw_map);
    } else {
	_mode[_GRP_RANDOMIZE_] = 0;
    }

    sw_map = _sw_handler.switch_arg ("drs");
    if (sw_map->active()) {
	_mode[_GRP_RAND_SEED_] = (long) (*sw_map);
    }
    else {
	_mode[_GRP_RAND_SEED_] = NONE;
    }

    // Otherwise use default seed.
    
    sw_map = _sw_handler.switch_arg ("g");
    if (sw_map->active()) {
	_mode[_GRP_DB_GROWTH_] = _POLYNOMIAL_DB_;
	_mode[_GRP_CONF_CLAUSE_SIZE_] = (int) (*sw_map);
    } else {
	_mode[_GRP_DB_GROWTH_] = _EXPONENTIAL_DB_;
	_mode[_GRP_CONF_CLAUSE_SIZE_] = INFINITY;
    }

    sw_map = _sw_handler.switch_arg ("il");
    if (sw_map->active()) {
	_mode[_GRP_DEDUCTION_LEVEL_] = (int) (*sw_map);
    } else {
	_mode[_GRP_DEDUCTION_LEVEL_] = 0;
    }

    sw_map = _sw_handler.switch_arg ("im");
    _mode[_GRP_DEDUCTION_MODE_] = _RECURSIVE_LEARNING_;    // Default mode is R
    if (sw_map->active()) {
	char *opt = (char*) *sw_map;
	if (!strcmp (opt, "L")) {
	    _mode[_GRP_DEDUCTION_MODE_] = _LOCAL_PROBING_;
	} else if (!strcmp (opt, "G")) {
	    _mode[_GRP_DEDUCTION_MODE_] = _GLOBAL_PROBING_;
	} else if (strcmp (opt, "R")) {
	    Warn("Invalid probe mode selection. Using defaults.");
	}
    }

    sw_map = _sw_handler.switch_arg ("l");
    if (sw_map->active()) {
	if (sw_map->enabled()) { _mode[_GRP_APPLY_PLR_] = TRUE; }
	else                   { _mode[_GRP_APPLY_PLR_] = FALSE; }
    } else { _mode[_GRP_APPLY_PLR_] = FALSE; }

    sw_map = _sw_handler.switch_arg ("p");
    _mode[_GRP_PARTITION_MODE_] = _NO_PARTITION_;
    if (sw_map->active()) {
	char *opt = (char*) (*sw_map);
	if (!strcmp (opt, "T")) {
	    _mode[_GRP_PARTITION_MODE_] = _TOPOLOGICAL_PARTITION_;
	} else if (!strcmp (opt, "V")) {
	    _mode[_GRP_PARTITION_MODE_] = _VARIABLE_PARTITION_;
	} else {
	    _mode[_GRP_PARTITION_MODE_] = _LITERAL_PARTITION_;
	}
    }

    sw_map = _sw_handler.switch_arg ("cj");
    _mode[_GRP_CL_JUSTIFY_TYPES_] = _PSPEC_CL_JUSTIFY_;
    if (sw_map->active()) {
	char *opt = (char*) *sw_map;
	if (!strcmp (opt, "A")) {
	  _mode[_GRP_CL_JUSTIFY_TYPES_] = _ALL_CL_JUSTIFY_;
	} else if (!strcmp (opt, "C")) {
	  _mode[_GRP_CL_JUSTIFY_TYPES_] = _CONF_CL_JUSTIFY_;
	} else if (strcmp (opt, "O")) {
	    Warn("Invalid clause justification selection. Using defaults.");
	}
    }

    sw_map = _sw_handler.switch_arg ("pbA");
    if (sw_map->active()) {
	_mode[_GRP_CNA_LIMIT_] = (int) (*sw_map);
    } else {
	_mode[_GRP_CNA_LIMIT_] = INFINITY;
    }

    sw_map = _sw_handler.switch_arg ("pbL");
    if (sw_map->active()) {
	_mode[_GRP_LIT_PROBE_LIMIT_] = (int) (*sw_map);
    } else {
	_mode[_GRP_LIT_PROBE_LIMIT_] = INFINITY;
    }

    sw_map = _sw_handler.switch_arg ("pbC");
    if (sw_map->active()) {
	_mode[_GRP_CL_PROBE_LIMIT_] = (int) (*sw_map);
    } else {
	_mode[_GRP_CL_PROBE_LIMIT_] = INFINITY;
    }

    sw_map = _sw_handler.switch_arg ("pbV");
    if (sw_map->active()) {
	_mode[_GRP_VAR_PROBE_LIMIT_] = (int) (*sw_map);
    } else {
	_mode[_GRP_VAR_PROBE_LIMIT_] = INFINITY;
    }

    sw_map = _sw_handler.switch_arg ("pl");
    if (sw_map->active()) {
	_mode[_GRP_PREPROC_LEVEL_] = (int) (*sw_map);
    } else {
	_mode[_GRP_PREPROC_LEVEL_] = 0;
    }

    sw_map = _sw_handler.switch_arg ("pm");
    _mode[_GRP_PREPROC_MODE_] = _RECURSIVE_LEARNING_;      // Default mode is R
    if (sw_map->active()) {
	char *opt = (char*) *sw_map;
	if (!strcmp (opt, "L")) {
	    _mode[_GRP_PREPROC_MODE_] = _LOCAL_PROBING_;
	} else if (!strcmp (opt, "G")) {
	    _mode[_GRP_PREPROC_MODE_] = _GLOBAL_PROBING_;
	} else if (strcmp (opt, "R")) {
	    Warn("Invalid probe mode selection. Using defaults.");
	}
    }

    sw_map = _sw_handler.switch_arg ("pr");
    _mode[_GRP_PREPROC_RECORDING_] = _FULL_RECORDING_;
    if (sw_map->active()) {
	char *opt = (char*) *sw_map;
	if (!strcmp (opt, "S")) {
	    _mode[_GRP_PREPROC_RECORDING_] = _SIMPLE_RECORDING_;
	} else if (strcmp (opt, "F")) {
	    Warn("Invalid probe mode selection. Using defaults.");
	}
    }

    sw_map = _sw_handler.switch_arg ("rt");
    if (sw_map->active()) {
      _mode[_GRP_CONF_CL_RM_THRES_] = (int) *sw_map;
    } else {
      _mode[_GRP_CONF_CL_RM_THRES_] = 1;       // 1 unassigned var => delete cl
    }

    sw_map = _sw_handler.switch_arg ("s");
    _mode[_GRP_SOLUTION_NUMBER_] = 1;
    if (sw_map->active()) {
	_mode[_GRP_SOLUTION_NUMBER_] = (int) *sw_map;
	if (_mode[_GRP_SOLUTION_NUMBER_] <= 0) {
	    Warn("Unspecified number of solutions to compute. Using defaults");
	    _mode[_GRP_SOLUTION_NUMBER_] = 1;
	}
    }

    sw_map = _sw_handler.switch_arg ("ws");
    _mode[_GRP_WRT_CL_SIZE_] = _mode[_GRP_CONF_CLAUSE_SIZE_]; // Use +g by def
    if (sw_map->active()) {
	_mode[_GRP_WRT_CL_SIZE_] = (int) *sw_map;
	if (_mode[_GRP_WRT_CL_SIZE_] < 0) {
	  Warn("Unspecified recorded clause size to print. Using defaults");
	  // By default print out all effectively recorded clauses.
	  _mode[_GRP_WRT_CL_SIZE_] = _mode[_GRP_CONF_CLAUSE_SIZE_];
	}
    }

    sw_map = _sw_handler.switch_arg ("sa");
    if (sw_map->active()) {
	_mode[_GRP_ITERATE_SOLUTIONS_] = TRUE;
    } else {
	_mode[_GRP_ITERATE_SOLUTIONS_] = FALSE;
    }

    sw_map = _sw_handler.switch_arg ("c");
    if (sw_map->active()) {
	if (sw_map->enabled()) { _mode[_GRP_SUBSUME_CLAUSES_] = TRUE; }
        else                   { _mode[_GRP_SUBSUME_CLAUSES_] = FALSE; }
    } else { _mode[_GRP_SUBSUME_CLAUSES_] = TRUE; }

    sw_map = _sw_handler.switch_arg ("dc");
    if (sw_map->active()) {
	if (sw_map->enabled()) { _mode[_GRP_DOMINATE_CLAUSES_] = TRUE; }
        else                   { _mode[_GRP_DOMINATE_CLAUSES_] = FALSE; }
    } else { _mode[_GRP_DOMINATE_CLAUSES_] = FALSE; }

    sw_map = _sw_handler.switch_arg ("dv");
    if (sw_map->active()) {
	if (sw_map->enabled()) { _mode[_GRP_DOMINATE_VARIABLES_] = TRUE; }
        else                   { _mode[_GRP_DOMINATE_VARIABLES_] = FALSE; }
    } else { _mode[_GRP_DOMINATE_VARIABLES_] = FALSE; }

    sw_map = _sw_handler.switch_arg ("m");
    _mode[_GRP_MULTI_CONF_HANDLING_] = _SEPARATE_;
    if (sw_map->active()) {
	if (sw_map->enabled()) { _mode[_GRP_MULTI_CONFLICTS_] = TRUE; }
        else                   { _mode[_GRP_MULTI_CONFLICTS_] = FALSE; }
    } else { _mode[_GRP_MULTI_CONFLICTS_] = FALSE; }

    sw_map = _sw_handler.switch_arg ("r");
    if (sw_map->active()) {
	if (sw_map->enabled()) { _mode[_GRP_CONF_CLAUSE_REPACKING_] = TRUE; }
	else                   { _mode[_GRP_CONF_CLAUSE_REPACKING_] = FALSE; }
    } else { _mode[_GRP_CONF_CLAUSE_REPACKING_] = FALSE; }

    sw_map = _sw_handler.switch_arg ("sc");
    if (sw_map->active()) {
	if (sw_map->enabled()) { _mode[_GRP_SKIP_CONF_CLS_] = TRUE; }
	else                   { _mode[_GRP_SKIP_CONF_CLS_] = FALSE; }
    } else { _mode[_GRP_SKIP_CONF_CLS_] = FALSE; }

    sw_map = _sw_handler.switch_arg ("t");
    if (sw_map->active()) {
	if (sw_map->enabled()) { _mode[_GRP_TRIM_SOLUTIONS_] = TRUE; }
	else                   { _mode[_GRP_TRIM_SOLUTIONS_] = FALSE; }
    } else { _mode[_GRP_TRIM_SOLUTIONS_] = FALSE; }

    sw_map = _sw_handler.switch_arg ("u");
    if (sw_map->active()) {
	if (sw_map->enabled()) { _mode[_GRP_IDENTIFY_UIPS_] = TRUE; }
	else                   { _mode[_GRP_IDENTIFY_UIPS_] = FALSE; }
    } else { _mode[_GRP_IDENTIFY_UIPS_] = TRUE; }

    // Other (yet unavailable/unused options);

    _mode[_GRP_PROBE_LEVEL_] = 0;

    _mode[_GRP_DIAGNOSIS_LEVEL_] = 0;
    _mode[_GRP_DIAGNOSIS_MODE_] = NONE;

    DBG1(cout<<"Exiting GRASP::configure_options()"<<endl;);
}

/*****************************************************************************/

