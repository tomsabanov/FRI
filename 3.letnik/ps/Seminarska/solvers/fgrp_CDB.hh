//-----------------------------------------------------------------------------
// File: fgrp_CDB.hh
//
// Purpose: Represents the clause database associated with an instance of SAT.
//
// Remarks: --.
//
// History: 03/26/98 - JPMS - created.
//
// Copyright (c) 1998 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#ifndef __FGRP_CDB__
#define __FGRP_CDB__

#include <iostream.h>

#include "defs.hh"
#include "memory.hh"
#include "darray.hh"
#include "list.hh"
#include "id_list.hh"

#include "fgrp_Defines.hh"
#include "fgrp_Mode.hh"



//-----------------------------------------------------------------------------
// Definitions for the clause database in a search context.
//-----------------------------------------------------------------------------

#define UNKNOWN    (-2)            // Using (-2) to avoid excessive use of (-1)

enum ClauseState {
    UNRESOLVED = 0x10,   // Constraint that can become SATISFIED or UNSATISFIED
    SATISFIED,           // Constraint is SATISFIED  given existing assignments
    UNSATISFIED,         // Constraint is UNSATISFIED  ""      ""         ""
    UNIT                 // UNRESOLVED clause BUT with 1 free literal
};

enum RegisterFlags {
  RegEmpty = 0x0,        // No flags have been used
  RegUnit = 0x1,         // Clause has been registered as unit
  RegUnsat = 0x2,        // Clause has been registered as unsat
  RegDeleted = 0x4,      // Clause has been registered as deleted
  RegLarge = 0x8         // Clause has been registered as large
};


enum ClauseTypes {
  TypeNone = 0x0,        // No type specified for clause
  TypeProbSpec = 0x1,    // Clause is part of original prob spec
  TypeConflict = 0x2     // Clause is derived from conflict
};



//-----------------------------------------------------------------------------
// Class names and typedefs used for simplying interface with clause DBs.
//-----------------------------------------------------------------------------

class Literal;
class Variable;
class Clause;
class LrgClause;
class ClauseSet;

typedef Literal *LiteralPtr;
typedef DArray<Literal*> DLiteralArray;
typedef List<Literal*> LiteralList;
typedef ListItem<Literal*> *LiteralListPtr;

typedef Variable *VariablePtr;
typedef DArray<Variable*> DVariableArray;
typedef List<Variable*> VariableList;
typedef ListItem<Variable*> *VariableListPtr;

typedef Clause *ClausePtr;
typedef DArray<Clause*> DClauseArray;
typedef List<Clause*> ClauseList;
typedef ListItem<Clause*> *ClauseListPtr;

typedef LrgClause *LrgClausePtr;
typedef DArray<LrgClause*> DLrgClauseArray;
typedef List<LrgClause*> LrgClauseList;
typedef ListItem<LrgClause*> *LrgClauseListPtr;


extern void dump_clause_list (ClauseList &cl_list, int full_info=TRUE);
extern void dump_clause_vect (DClauseArray &cl_vect, int full_info=TRUE);
extern void dump_clause_literals (ClausePtr cl);



//-----------------------------------------------------------------------------
// Class: Literal
//
// Purpose: Literals associated with variables and clauses.
//-----------------------------------------------------------------------------

class Literal {
public:

  //---------------------------------------------------------------------------
  // Constructor/destructor.
  //---------------------------------------------------------------------------

  Literal (ClausePtr cl, VariablePtr var, int sign);
  ~Literal();

  //---------------------------------------------------------------------------
  // Interface contract
  //---------------------------------------------------------------------------

  int sign() { return _sign; }

  inline void set_lit_IDXs (int cidx, int vidx) { _cidx = cidx; _vidx = vidx; }

  inline int &varIDX() { return _vidx; }
  inline int &clIDX() { return _cidx; }

  inline VariablePtr variable() { return _variable; }
  inline ClausePtr clause() { return _clause; }

protected:

  int _sign;                 // Sign of literal

  VariablePtr _variable;     // Variable containing reference to literal
  ClausePtr _clause;         // Clause containing reference to literal
  int _vidx;                 // IDX reference of literal within variable
  int _cidx;                 // IDX reference of literal within clause

private:

};



//-----------------------------------------------------------------------------
// Class: Variable
//
// Purpose: Variables in CNF formula.
//-----------------------------------------------------------------------------

class Variable {
public:

  //---------------------------------------------------------------------------
  // Constructor/destructor.
  //---------------------------------------------------------------------------

  Variable (int nID, char *name = NULL);
  ~Variable();

  //---------------------------------------------------------------------------
  // Interface contract.
  //
  // A- Structural information.
  //---------------------------------------------------------------------------

  inline char *name() { return _name; }
  inline int ID() { return _ID; }

  DLiteralArray &pos_literals() { return _pos_literals; }
  DLiteralArray &neg_literals() { return _neg_literals; }

  inline int add_pos_literal (LiteralPtr lit) {         // Add positive literal
    int idx = _pos_literals.incr_size();
    _pos_literals[idx] = lit;
    return idx;
  }
  inline int del_pos_literal (LiteralPtr lit) {      // Delete positive literal
    if (lit->varIDX() != _pos_literals.size()-1 ) {
      LiteralPtr llit = _pos_literals[_pos_literals.size()-1];
      _pos_literals.swap(lit->varIDX(), llit->varIDX());
      llit->varIDX() = lit->varIDX();             // update ID of moved literal
    }
    _pos_literals.decr_size();
    return _pos_literals.size();
  }

  inline int add_neg_literal (LiteralPtr lit) {         // Add negative literal
    int idx = _neg_literals.incr_size();
    _neg_literals[idx] = lit;
    return idx;
  }
  inline int del_neg_literal (LiteralPtr lit) {      // Delete negative literal
    if (lit->varIDX() != _neg_literals.size()-1 ) {
      LiteralPtr llit = _neg_literals[_neg_literals.size()-1];
      _neg_literals.swap(lit->varIDX(), llit->varIDX());
      llit->varIDX() = lit->varIDX();             // update ID of moved literal
    }
    _neg_literals.decr_size();
    return _neg_literals.size();
  }

  inline VariableListPtr traverse_ref() { return _traverse_ref; }
  inline VariableListPtr imply_ref() { return _imply_ref; }
  inline VariableListPtr partition_ref() { return _partition_ref; }

  //---------------------------------------------------------------------------
  // B- State information.
  //---------------------------------------------------------------------------

  inline int value() { return _value; }
  inline ClausePtr antecedent() { return _antec; }
  inline int DLevel() { return _DLevel; }

  inline void set_state (int val, int dlevel, ClausePtr antec) {
    _value = val; _DLevel = dlevel; _antec = antec;
  }
  inline void reset_state() {
    _value = UNKNOWN; _antec = NULL; _DLevel = NONE;
  }

  //---------------------------------------------------------------------------
  // C- Consistency and debugging information.
  //---------------------------------------------------------------------------

  void dump (ostream &outs = cout);

protected:

  //---------------------------------------------------------------------------
  // Structural information.
  //---------------------------------------------------------------------------

  char *_name;                    // In debugging mode variables use names.
  int _ID;                        // Unique identifier for variable

  DLiteralArray _pos_literals;    // Clauses with var as a positive literal
  DLiteralArray _neg_literals;    // Clauses with var as a negative literal

  // Additional structures: self-referential pointers for external uses.

  VariableListPtr _traverse_ref;      // Var ref used for traversals
  VariableListPtr _imply_ref;         // Implied assignment ref
  VariableListPtr _partition_ref;     // Used for manipulating partitions

  //---------------------------------------------------------------------------
  // State information.
  //---------------------------------------------------------------------------

  int _value;           // Logic value assigned to variable
  ClausePtr _antec;     // Clause implying variable assignment
  int _DLevel;          // Decision level at which value is implied
  
private:

};


//-----------------------------------------------------------------------------
// Class: Clause
//
// Purpose: Clauses in CNF formula.
//-----------------------------------------------------------------------------

class Clause {
public:

  //---------------------------------------------------------------------------
  // Constructor/destructor.
  //---------------------------------------------------------------------------

  Clause (int nID, int cl_type = TypeProbSpec);
  ~Clause();

  //---------------------------------------------------------------------------
  // Interface contract.
  //
  // A- Structural information.
  //---------------------------------------------------------------------------

  inline int ID() { return _ID; }

  DLiteralArray &pos_literals() { return _pos_literals; }
  DLiteralArray &neg_literals() { return _neg_literals; }

  inline int add_pos_literal (LiteralPtr lit) {         // Add positive literal
    int idx = _pos_literals.incr_size();
    _pos_literals[idx] = lit;
    _num_lits++;
    return idx;
  }
  inline int del_pos_literal (LiteralPtr lit) {      // Delete positive literal
    if (lit->clIDX() != _pos_literals.size()-1 ) {
      LiteralPtr llit = _pos_literals[_pos_literals.size()-1];
      _pos_literals.swap(lit->clIDX(), llit->clIDX());
      llit->clIDX() = lit->clIDX();               // update ID of moved literal
    }
    _pos_literals.decr_size();
    _num_lits--;
    return _pos_literals.size();
  }

  inline int add_neg_literal (LiteralPtr lit) {         // Add negative literal
    int idx = _neg_literals.incr_size();
    _neg_literals[idx] = lit;
    _num_lits++;
    return idx;
  }
  inline int del_neg_literal (LiteralPtr lit) {      // Delete negative literal
    if (lit->clIDX() != _neg_literals.size()-1 ) {
      LiteralPtr llit = _neg_literals[_neg_literals.size()-1];
      _neg_literals.swap(lit->clIDX(), llit->clIDX());
      llit->clIDX() = lit->clIDX();               // update ID of moved literal
    }
    _neg_literals.decr_size();
    _num_lits--;
    return _neg_literals.size();
  }

  //---------------------------------------------------------------------------
  // B- State information.
  //---------------------------------------------------------------------------

  inline int size() { return _num_lits; }
  inline int num_literals() { return _num_lits; }

  inline int &val0_pos_literals() { return _val0_pos_lits; }
  inline int &val1_pos_literals() { return _val1_pos_lits; }

  inline int &val0_neg_literals() { return _val0_neg_lits; }
  inline int &val1_neg_literals() { return _val1_neg_lits; }

  inline int val0_lits() { return _val0_pos_lits + _val0_neg_lits; }
  inline int val1_lits() { return _val1_pos_lits + _val1_neg_lits; }

  inline int free_literals() { return _num_lits - (val0_lits()+val1_lits()); }

  inline int is_satisfied() { return (val1_lits() > 0); }

  inline int is_unsatisfied() { return (val0_lits()==_num_lits); }

  inline int is_unit() { return (val1_lits()==0 && val0_lits()==_num_lits-1); }
  inline int is_pos_unit() {
    return (is_unit() && _val0_neg_lits==_neg_literals.size());
  }
  inline int is_neg_unit() {
    return (is_unit() && _val0_pos_lits==_pos_literals.size());
  }

  inline int is_unresolved() {
    return (val1_lits()==0 && val0_lits()<_num_lits-1);
  }

  inline int is_fully_unresolved() {          // NO clause literal yet assigned
    return val1_lits()==0 && val0_lits()==0;
  }

  inline int state() {                        // Return current state of clause
    if (val1_lits() > 0) {
      return SATISFIED;
    } else if (val0_lits() == _num_lits-1) {
      return UNIT;
    } else if (val0_lits() == _num_lits) {
      return UNSATISFIED;
    }
    return UNRESOLVED;
  }

  inline ClauseListPtr unit_ref() { return _unit_ref; }
  inline ClauseListPtr unsat_ref() { return _unsat_ref; }
  inline ClauseListPtr del_ref() { return _del_ref; }

  //---------------------------------------------------------------------------
  // Type information.
  //---------------------------------------------------------------------------

  int is_conflicting() { return _clause_type & TypeConflict; }
  int is_probspec() { return _clause_type & TypeProbSpec; }
  
  //---------------------------------------------------------------------------
  // Flag registration. This permits auxiliar status to be properly handled.
  //---------------------------------------------------------------------------

  void set_registered (int reg_flag) { _registered_flags |= reg_flag; }
  void unset_registered (int reg_flag) { _registered_flags &= ~reg_flag; }
  int test_registered (int reg_flag) {return (_registered_flags&reg_flag)!=0;}
  
  //---------------------------------------------------------------------------
  // C- Consistency and debugging information.
  //---------------------------------------------------------------------------

  void dump (ostream &outs = cout);
  int check_consistency();

protected:

  int _ID;               // ID of clause in clause set

  int _num_lits;         // Number of literals
  int _val0_pos_lits;    // Number of pos literals whose value is 0
  int _val1_pos_lits;    // Number of pos literals whose value is 1 (=> cl=1)
  int _val0_neg_lits;    // Number of neg literals whose value is 0
  int _val1_neg_lits;    // Number of neg literals whose value is 1 (=> cl=1)

  DLiteralArray _pos_literals;                 // Positive literals of clause
  DLiteralArray _neg_literals;                 // Negative literals of clause

  ClauseListPtr _unit_ref;      // Clause ref used when clause is unit
  ClauseListPtr _unsat_ref;     // Clause ref used when clause is unsat
  ClauseListPtr _del_ref;       // Clause ref used for deleting clause

  int _registered_flags;       // Indicates additional status info of clause
  int _clause_type;            // Field denoting type of clause
private:

};


//-----------------------------------------------------------------------------
// Class: LrgClause
//
// Purpose: Clause for storing large numbers of dependencies from conflicts.
//          The state and the implied variable are specified elsewhere.
//-----------------------------------------------------------------------------

class LrgClause : public Clause {
public:

  //---------------------------------------------------------------------------
  // Constructor/destructor.
  //---------------------------------------------------------------------------

  LrgClause(int nID, int max_num_deps);
  ~LrgClause();

  //---------------------------------------------------------------------------
  // Interface contract.
  //---------------------------------------------------------------------------

  inline int &state() { return _state; }

  inline int &implied_var() { return _trgt_var; }

  inline void add_dependency (int nID) { _dependencies.add_elem (nID); }
  inline void del_dependency (int nID) { _dependencies.del_elem (nID); }

  inline int num_dependencies() { return _dependencies.size(); }

  void dump (ostream &outs = cout);

protected:

  int _state;
  ListID _dependencies;
  VariablePtr _trgt_var;

private:

};


//-----------------------------------------------------------------------------
// Class: ClauseDatabase
//
// Purpose: A clause database for the purpose of search for SAT must maintain
//          structural information as well as information regarding consistency
//          and constraining conditions.
//-----------------------------------------------------------------------------

class ClauseDatabase {
public:

  //---------------------------------------------------------------------------
  // Constructor/destructor.
  //---------------------------------------------------------------------------

  ClauseDatabase(ModeOptions &mode);
  virtual ~ClauseDatabase();

  //---------------------------------------------------------------------------
  // Interface contract.
  //
  // A- Functions used for initalization, setup and cleanup (either temporary
  //    or final).
  //---------------------------------------------------------------------------

  virtual void setup();      // Set up data structures for solving SAT instance
  virtual void cleanup(); // Clean up data structures from solving SAT instance

  virtual int solution_found() { }

  //---------------------------------------------------------------------------
  // B- Functions for modifying the internal structure of the database.
  //---------------------------------------------------------------------------

  inline LiteralPtr add_literal (ClausePtr cl, VariablePtr var, int sign) {
    LiteralPtr lit = new Literal (cl, var, sign);
    int vidx, cidx;
    if (!sign) {                                   // Adding a positive literal
      vidx = var->add_pos_literal (lit);
      cidx = cl->add_pos_literal (lit);

      if (var->value() == TRUE) { cl->val1_pos_literals()++; }
      else if (var->value() == FALSE) { cl->val0_pos_literals()++; }
    }
    else {                                         // Adding a negative literal
      vidx = var->add_neg_literal (lit);
      cidx = cl->add_neg_literal (lit);

      if (var->value() == FALSE) { cl->val1_neg_literals()++; }
      else if (var->value() == TRUE) { cl->val0_neg_literals()++; }
    }
    lit->set_lit_IDXs (cidx, vidx);

    _lit_number++;
    update_state_registration (cl);
    return lit;
  }

  inline void del_literal (LiteralPtr lit) {
    VariablePtr var = lit->variable();
    ClausePtr cl = lit->clause();

    if (!lit->sign()) {                          // Deleting a positive literal
      var->del_pos_literal (lit);
      cl->del_pos_literal (lit);

      if (var->value() == TRUE) { cl->val1_pos_literals()--; }
      else if (var->value() == FALSE) { cl->val0_pos_literals()--; }
    }
    else {                                       // Deleting a negative literal
      var->del_neg_literal (lit);
      cl->del_neg_literal (lit);

      if (var->value() == FALSE) { cl->val1_neg_literals()--; }
      else if (var->value() == TRUE) { cl->val0_neg_literals()--; }
    }

    _lit_number--;
    update_state_registration (cl);
    delete lit;
  }

  inline VariablePtr add_variable (char *vname = NULL) {
    VariablePtr var;
    if (_avail_var_IDs.size() == 0) {
      var = new Variable (++_varIDs, vname);
      int idx = _variables.incr_size();
      _variables[idx] = var;

      CHECK(if(idx!=var->ID()-1){
	cout<<"IDX: "<<idx<<"  **AND**  vID: "<<var->ID()<<endl;
	Warn("Invalid variable ID manipulation??");});
    }
    else {
      int var_id = _avail_var_IDs.first()->data();
      _avail_var_IDs.remove(_avail_var_IDs.first());
      CHECK(if(_variables[var_id-1] != NULL)
	    {Warn("Invalid variable ID manipulation??");});

      var = new Variable (var_id, vname);
      _variables[var_id-1] = var;
    }
    return var;
  }

  inline void del_variable (VariablePtr var) {
    // Variables *must* assume all its literals have been deleted
    // *No* entry in array of variables is deleted

    CHECK(if(var->value() != UNKNOWN) {Abort("Deleting assigned var??");}
	  if(var->pos_literals().size() || var->neg_literals().size())
	  {Warn("Deleting variable with literals??");});

    _variables[var->ID()-1] = NULL;         // Empty slot in array of variables
    _avail_var_IDs.append (var->ID());             // New available variable ID
    delete var;
  }

  inline ClausePtr add_clause (int cl_type = TypeProbSpec) {
    ClausePtr cl;
    if (_avail_cl_IDs.size() == 0) {
      cl = new Clause (++_clIDs, cl_type);
      int idx = _clauses.incr_size();
      _clauses[idx] = cl;
      
      CHECK(if(idx!=cl->ID()-1) {
	cout<<"IDX: "<<idx<<"  **AND**  cID: "<<cl->ID()<<endl;
	Warn("Invalid clause ID manipulation??");}
	    if(idx < _initial_cl_number) {
	      Warn("Invalid IDX for clause??");});
    }
    else {
      int cl_id = _avail_cl_IDs.first()->data();
      _avail_cl_IDs.remove(_avail_cl_IDs.first());
      CHECK(if(_clauses[cl_id-1] != NULL)
	    {Warn("Invalid clause ID manipulation??");});

      cl = new Clause (cl_id, cl_type);
      _clauses[cl_id-1] = cl;
      CHECK(if(cl_id < _initial_cl_number) {
	      Warn("Invalid CL_ID for clause??");});
    }
    return cl;
  }

  inline void del_clause (ClausePtr cl) {
    // When a clause is deleted *all* its literals get deleted

    register int i;
    while ((i = cl->pos_literals().size()-1) >= 0) {
      del_literal (cl->pos_literals()[i]);
    }
    while ((i = cl->neg_literals().size()-1) >= 0) {
      del_literal (cl->neg_literals()[i]);
    }
    unregister_unit_clause (cl);
    unregister_unsat_clause (cl);

    _clauses[cl->ID()-1] = NULL;
    _avail_cl_IDs.prepend (cl->ID());                // New available clause ID
    delete cl;
  }

  //---------------------------------------------------------------------------
  // Registration of clauses in different lists (i.e. unit/unsat/deleted).
  //---------------------------------------------------------------------------

  inline ClausePtr first_unit_clause() {
    ClausePtr cl = NULL;
    if (_unit_clauses.first()) {
      cl = _unit_clauses.first()->data();
    }
    return cl;
  }
  inline void register_unit_clause (ClausePtr cl) {
    if (!cl->test_registered (RegUnit)) {
      cl->set_registered (RegUnit);             // Clause added to unit cl list
      _unit_clauses.append (cl->unit_ref());

      CHECK(if(_mode[_GRP_VERBOSITY_]>=10)
	    {cout<<"\nREGISTERED UNIT: "<<endl;cl->dump();cout<<endl;
	    dump_clause_list(_unit_clauses,FALSE);});
    }
  }
  inline void unregister_unit_clause (ClausePtr cl) {
    if (cl->test_registered (RegUnit)) {
      cl->unset_registered (RegUnit);             // Can be added to list again
      _unit_clauses.extract (cl->unit_ref());

      CHECK(if(_mode[_GRP_VERBOSITY_]>=10)
	    {cout<<"\nUNREGISTERED UNIT: "<<endl;cl->dump();cout<<endl;
	    dump_clause_list(_unit_clauses,FALSE);});
    }
  }

  inline ClausePtr first_unsat_clause() {
    ClausePtr cl = NULL;
    if (_unsat_clauses.first()) {
      cl = _unsat_clauses.first()->data();
    }
    return cl;
  }
  inline void register_unsat_clause (ClausePtr cl) {
    if (!cl->test_registered (RegUnsat)) {
      cl->set_registered (RegUnsat);           // Clause added to unsat cl list
      _unsat_clauses.append (cl->unsat_ref());

      CHECK(if(_mode[_GRP_VERBOSITY_]>=10)
	    {cout<<"\nREGISTERED UNSAT: "<<endl;cl->dump();cout<<endl;
	    dump_clause_list(_unsat_clauses,FALSE);});
    }
  }
  inline void unregister_unsat_clause (ClausePtr cl) {
    if (cl->test_registered (RegUnsat)) {
      cl->unset_registered (RegUnsat);            // Can be added to list again
      _unsat_clauses.extract (cl->unsat_ref());

      CHECK(if(_mode[_GRP_VERBOSITY_]>=10)
	    {cout<<"\nUNREGISTERED UNSAT: "<<endl;cl->dump();cout<<endl;
	    dump_clause_list(_unsat_clauses,FALSE);});
    }
  }

  inline int number_unsat_clauses() { return _unsat_clauses.size(); }
  
  inline ClausePtr first_del_clause() {
    ClausePtr cl = NULL;
    if (_deleted_clauses.first()) {
      cl = _deleted_clauses.first()->data();
    }
    return cl;
  }
  inline void register_deleted_clause (ClausePtr cl) {
    if (!cl->test_registered (RegDeleted) && cl->is_unresolved() &&
	cl->free_literals() >= _mode[_GRP_CONF_CL_RM_THRES_]) {
      cl->set_registered (RegDeleted);                // Clause becomes deleted
      _deleted_clauses.append (cl->del_ref());

      CHECK(if(_mode[_GRP_VERBOSITY_]>=10)
	    {cout<<"\nREGISTERED DELETED: "<<endl;cl->dump();cout<<endl;
	    dump_clause_list(_deleted_clauses,FALSE);});
    }
  }
  inline void unregister_deleted_clause (ClausePtr cl, int force_unreg=FALSE) {
    if (cl->test_registered (RegDeleted) &&
	(!cl->is_unresolved() ||
	 cl->free_literals() < _mode[_GRP_CONF_CL_RM_THRES_] ||
	 force_unreg)) {
      cl->unset_registered (RegDeleted);            // Can become deleted again
      _deleted_clauses.extract (cl->del_ref());

      CHECK(if(_mode[_GRP_VERBOSITY_]>=10)
	    {cout<<"\nUNREGISTERED DELETED: "<<endl;cl->dump();cout<<endl;
	    dump_clause_list(_deleted_clauses,FALSE);});
    }
  }

  inline void update_state_registration (ClausePtr cl) {
    switch (cl->state()) {
    case UNIT:
      register_unit_clause (cl);
      unregister_unsat_clause (cl);
      break;
    case UNSATISFIED:
      register_unsat_clause (cl);
      unregister_unit_clause (cl);
      break;
    case UNRESOLVED:
    case SATISFIED:
      unregister_unit_clause (cl);
      unregister_unsat_clause (cl);
      break;
    default:
      CHECK(Abort("Invalid state in clause"););
    }
  }

  inline int can_probe (ClausePtr cl) {
    return
      (cl->is_probspec() &&
       _mode[_GRP_CL_JUSTIFY_TYPES_]!=_CONF_CL_JUSTIFY_) ||
      (cl->is_conflicting() &&
       _mode[_GRP_CL_JUSTIFY_TYPES_]!=_PSPEC_CL_JUSTIFY_);
  }

  inline int writable_clause (ClausePtr cl) {
    // Clause is writable if it is either original or its size is no
    // greater than the largest allowed size to print out.
    return
      (!((cl->is_conflicting() && cl->size() > _mode[_GRP_WRT_CL_SIZE_]) ||
	 cl->test_registered(RegLarge) ||
	 cl->test_registered(RegDeleted)));
  }

  //---------------------------------------------------------------------------
  // D- Internal data.
  //---------------------------------------------------------------------------

  inline int num_variables() {
    return _variables.size()-_avail_var_IDs.size();
  }
  inline int num_clauses() { return _clauses.size() - _avail_cl_IDs.size(); }

  inline DVariableArray &variables() { return _variables; }
  inline DClauseArray &clauses() { return _clauses; }

  inline int &sat_clause_count() {
    CHECK(if (_sat_clause_count < 0 || _sat_clause_count > _clauses.size())
	  { Warn("Invalid number of SAT clauses??"); });
    return _sat_clause_count;
  }

  //---------------------------------------------------------------------------
  // I- Functions for statistics gathering and accessing, for dumping data,
  //    and for consistency checking.
  //---------------------------------------------------------------------------

  virtual void output_stats();
  virtual void output_assignments (ostream &outs, int assigned_only = TRUE);

  virtual int database_growth() { return _lit_number/_initial_lit_number; }

  virtual void dump (ostream &outs = cout);

  virtual void print_database();

  virtual int check_solution();
  virtual int check_consistency (int consistent_state = TRUE);
  virtual int check_final_consistency (int consistent_state = TRUE);

  friend ostream &operator<<(ostream &os, ClauseDatabase &clDB);

protected:

  Array<int> &_mode;

  DVariableArray _variables;             // Set of variables
  DClauseArray _clauses;                 // Set of clauses

  int _clIDs;                            // Counter of clause IDs
  int _varIDs;                           // Counter of variable IDs

  List<int> _avail_var_IDs;              // Available variable IDs
  List<int> _avail_cl_IDs;               // Available clause IDs

  //---------------------------------------------------------------------------
  // Search-state dependent lists of clauses.
  //---------------------------------------------------------------------------
  
  ClauseList _unit_clauses;               // List of unit clauses
  ClauseList _unsat_clauses;              // List of unsat clauses
  ClauseList _deleted_clauses;            // List of deletable clauses

  int _sat_clause_count;                  // Counts number of satisfied clauses

  //---------------------------------------------------------------------------
  // Stats gathering.
  //---------------------------------------------------------------------------

  int _initial_var_number;                     // Initial number of variables
  int _initial_cl_number;                        // Initial number of clauses
  int _initial_lit_number;                      // Initial number of literals
  int _lit_number;                              // Current number of literals
  //  int _tot_del_clauses;                    // Number of large deleted clauses
  //  int _tot_subsumed_cls;                        // Number of subsumed clauses
  //  int _tot_dominated_cls;                      // Number of dominated clauses
  //  int _tot_partitions;                          // Total number of partitions

private:

};

#endif // __FGRP_CDB__

/*****************************************************************************/
