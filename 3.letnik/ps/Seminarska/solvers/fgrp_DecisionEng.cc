//-----------------------------------------------------------------------------
// File: fgrp_DecisionEng.cc
//
// Purpose: Definition of the member functions of the decision engine.
//
// Remarks: --
//
// History: 03/30/98 - JPMS - created.
//
// Copyright (c) 1998 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#include <math.h>
#include "random.hh"

#include "fgrp_DecisionEng.hh"


//-----------------------------------------------------------------------------
// Local defines and variable definitions.
//-----------------------------------------------------------------------------

#define TARGET_VAR_SIZE    10
#define TARGET_LIT_SIZE    2

#define MAX_RELEVANT_SIZE  5

static int shifted_pivot[MAX_RELEVANT_SIZE+1];

#define Balpha 1
#define Bbeta  2

static double swap_range;


//-----------------------------------------------------------------------------
// Constructors/Destructors and initialization functions for the different
// classes.
//-----------------------------------------------------------------------------

VariableOrdering::VariableOrdering() {_var = NULL; _pref_value = NONE;}

VariableOrdering::~VariableOrdering() { }



DecisionEngine::DecisionEngine (Array<int> &mode,
				ClauseDatabase &clDB,
				SearchTree &stree,
				BCP &bcp, BCA &bca)
  : _mode (mode), _clDB (clDB), _stree (stree), _bcp (bcp), _bca (bca),
    _var_ord(0), _dec_index(0),
    _list_vars(), _visit_flag(0)
{
  _num_decisions = 0;
  _max_dec_depth = 0;
}

DecisionEngine::~DecisionEngine() { }



//-----------------------------------------------------------------------------
// Member functions for static and dynamic decision making.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Function: decide()
//
// Purpose: Selects a decision assignment and returns an indication whether
//          such assignment was made (it all variables are assigned no
//          decisions can be made).
//-----------------------------------------------------------------------------

int DecisionEngine::decide()
{
  DBG1(cout<<"-> Entering DecisionEngine::decide..."<<endl;);

  int status = (_mode[_GRP_DECISION_MODE_] == _STATIC_ORD_) ?
    select_static_assignment() : select_dynamic_assignment();

  if (status == DECISION) {
    _num_decisions++;
    if (_stree.DLevel() > _max_dec_depth) {
      _max_dec_depth = _stree.DLevel();
    }
  }
  return status;
}


//-----------------------------------------------------------------------------
// Function: setup()
//
// Purpose: Sets up internal data structures prior to starting making decision
//          assignments.
//-----------------------------------------------------------------------------

void DecisionEngine::setup()
{
  DBG1(cout<<"-> Entering DecisionEngine::setup..."<<endl;);

  if (_mode[_GRP_DECISION_MODE_] == _STATIC_ORD_) {
    _dec_index.resize (_stree.num_decisions());
    _var_ord.resize (_clDB.num_variables());

    // Ordering for fixed order decision making.

    for (register int k = 0; k < _var_ord.size(); k++) {
      _var_ord[k] = new VariableOrdering();
      _var_ord[k]->setup_ordering (_clDB.variables()[k], TRUE);
    }
  } else {
    unsigned int pivot = 1;
    for (register int k = 0; k <= MAX_RELEVANT_SIZE; k++) {
      shifted_pivot[k] = pivot << k;
    }
    _visit_flag.resize (_clDB.num_variables());
    for (register int k = 0; k < _visit_flag.size(); k++) {
      _visit_flag[k] = FALSE;
    }
  }
  _init_cl_num = _clDB.clauses().size();

  if (_mode[_GRP_RANDOMIZE_]) {
    if(_mode[_GRP_RAND_SEED_] != NONE) {
      srandom((unsigned int) _mode[_GRP_RAND_SEED_]);        // Define seed.
    }
    swap_range = (_mode[_GRP_RANDOMIZE_] - 2) * 10 / 100.0;  // Starts at 2...
  }
  DBG1(cout<<"-> Exiting DecisionEngine::setup..."<<endl;);
}


//-----------------------------------------------------------------------------
// Function: cleanup()
//
// Purpose: After solving an instance of SAT, this procedure can be used to
//          clean up internal data structures.
//-----------------------------------------------------------------------------

void DecisionEngine::cleanup()
{
  _num_decisions = 0;
  _max_dec_depth = 0;
}


//-----------------------------------------------------------------------------
// Function: make_decision_assignment()
//
// Purpose: Makes assignment corresponding to a given decision.
//-----------------------------------------------------------------------------

void DecisionEngine::make_decision_assignment (VariablePtr var, int value)
{
  DBG1(cout<<"-> Entering DecisionEngine::make_decis_assign..."<<endl;);

  if (_mode[_GRP_RANDOMIZE_]) {
    int toggle_value = (int) rint((random()*1.0)/RAND_MAX);
    value = (toggle_value) ? value : 1-value; // Decide whether to toggle value
  }
  _stree.push_decision();

  /* *****
  if (_mode[_GRP_PARTITION_MODE_] != _NO_PARTITION_) {
    _stree.update_current_partition(); // Update partition info  -jpms 4/8/97

    CHECK(_stree.check_partition_invariant(1););
    DBG1(cout<<"@DLevel "<<_stree.DLevel()<<" ACTIVATED ";
	 _stree.current_partition()->dump();cout<<endl;);
  }
  ***** */

  var->set_state (value, _stree.DLevel(), NULL);
  _stree.push_implied_variable (var);

  if (_mode[_GRP_VERBOSITY_]>=4) {
    cout << "DECISION:             " << var->name() << " = ";
    cout << value << " @ DLevel " << _stree.DLevel() << endl;
  }
}


//-----------------------------------------------------------------------------
// Decision making procedures.
//
// A- STATIC. (Static ordering procedures also included.)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Function: select_static_assignment()
//
// Purpose: Given a fixed ordering of the variables, as well as preferred
//          values this procedure selects the next decision assignment.
//
// Notes: It must consult the *current* decision level in order to decide
//        the local index order for choosing a decision assignment.
//        it must also push another decision onto the decision stack.
//-----------------------------------------------------------------------------

int DecisionEngine::select_static_assignment()
{
  register int level = _stree.DLevel();
  register int idx = (level > 0) ? _dec_index[level]+1 : 0;
  register VariablePtr var;
  for (; idx < _var_ord.size(); idx++) {
    var = _var_ord[idx]->var();

    if (var->value() == UNKNOWN) {
      _dec_index[level+1] = idx;      // Next decision corresponds to idx
      make_decision_assignment (var, _var_ord[idx]->pref_value());
      return DECISION;
    }
  }
  DBG0(cout<<"SOLUTION FOUND <-> node more decisions to be made\n";);
  return NO_DECISION;
}


//-----------------------------------------------------------------------------
// Accessing stats.
//-----------------------------------------------------------------------------

void DecisionEngine::output_stats()
{
  printItem ("Total number of decisions", _num_decisions);
  printItem ("Largest depth in decision tree", _max_dec_depth);
  printItem();
}


//-----------------------------------------------------------------------------
// Static orderings for selecting assignments.
//-----------------------------------------------------------------------------

// To be defined.


//-----------------------------------------------------------------------------
// B- DYNAMIC.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Function: select_dynamic_assignment()
//
// Purpose: For the current state of the search, and according to a chosen
//          dynamic ordering heuristic, a variable assignment is selected.
//
// Side-effects: A variable assignment is selected.
//-----------------------------------------------------------------------------

int DecisionEngine::select_dynamic_assignment()
{
  DBG1(cout<<"Making dyamic decision assignment"<<endl;);

  /* *****
  VariableList *var_list = (_mode[_GRP_PARTITION_MODE_] == _NO_PARTITION_) ?
    &_clDB.variables() : &_stree.top_partition()->variables();
    ***** */

  DVariableArray *var_set = &_clDB.variables();

  /* *****

  switch( _mode[_GRP_DECISION_LEVEL_] ) {
  case _MSOS_:
  case _MSTS_:
  case _MSMM_:
  case _BOHM_:
  case _DLIS_:
  case _DLCS_:
  case _DSIS_:
    break;
  case _LMSOS_:
  case _LMSTS_:
  case _LMSMM_:
  case _LBOHM_:
  case _LDLIS_:
  case _LDLCS_:
    compute_local_var_set();
    if (_list_vars.size() != 0) {
      var_list = (VariableList*) &_list_vars;
    }
    break;
  case _MVBS_:
    identify_target_var_set();
    break;
  case _LMVBS_:
    compute_target_var_set();
    break;
  case _DJW_:
  default:
    Abort( "Invalid dynamic selection option" );
    break;
  }
  ***** */

  int outcome;
  switch( _mode[_GRP_DECISION_LEVEL_] ) {
  case _MSOS_:
    outcome = select_MSOS (*var_set);
    break;
  case _MSTS_:
    outcome = select_MSTS (*var_set);
    break;
  case _BOHM_:
    outcome = select_BOHM (*var_set);
    break;
  case _MSMM_:
    outcome = select_MSMM (*var_set);
    break;
  case _DLIS_:
    outcome = select_DLIS (*var_set);
    break;
  case _RDLIS_:
    outcome = select_RDLIS (*var_set);
    break;
  case _DLISB_:
    outcome = select_DLISB (*var_set);
    break;
  case _DLCS_:
    outcome = select_DLCS (*var_set);
    break;
  case _DSIS_:
    outcome = select_DSIS (*var_set);
    break;
  case _SRAND_:
    outcome = select_SRAND (*var_set);
    break;

  /* *****  

  case _LMSOS_:
    outcome = select_MSOS (*var_list);
    cleanup_var_set();
    break;
  case _LMSTS_:
    outcome = select_MSTS (*var_list);
    cleanup_var_set();
    break;
  case _LMSMM_:
    outcome = select_MSMM (*var_list);
    cleanup_var_set();
    break;
  case _LBOHM_:
    outcome = select_BOHM (*var_list);
    cleanup_var_set();
    break;
  case _LDLIS_:
    outcome = select_DLIS (*var_list);
    cleanup_var_set();
    break;
  case _LDLCS_:
    outcome = select_DLCS (*var_list);
    cleanup_var_set();
    break;
  case _MVBS_:
  case _LMVBS_:
    outcome = select_MVBS (*var_list);
    break;

  ***** */
    
  case _DJW_:
  default:
    Abort( "Invalid dynamic selection option" );
    break;
  }
  return outcome;
}


/* *****

//-----------------------------------------------------------------------------
// Definition of a local set of variables for dynamic decision making.
// The setis said to be local be cause variables are nighbors to
// previously assigned variables.
//-----------------------------------------------------------------------------

int DecisionEngine::compute_local_var_set()
{
  DBG1(cout<<"-> Entering DecisionEngine::compute_local_vars..."<<endl;);

  int DLevel = _stree.DLevel();
  do {
    for_each(apvar,_stree.assigned_variables(DLevel),VariablePtr){
      register VariablePtr var = (VariablePtr) apvar->data();

      for_each(plit,var->literals(),LiteralPtr) {
	LiteralPtr lit = plit->data();
	ClausePtr cl = lit->clause();

	for_each(plitB,cl->literals(),LiteralPtr) {
	  LiteralPtr litB = plitB->data();

	  VariablePtr nvar = litB->variable();

	  if (nvar == var) { continue; }
	  if (nvar->value() == UNKNOWN && !_visit_flag[nvar->ID()]) {
	    _list_vars.append (nvar);
	    _visit_flag[nvar->ID()] = TRUE;
	  }
	}
      }
    }
  } while (_list_vars.size() == 0 && --DLevel >= 0);
}


int DecisionEngine::cleanup_var_set()
{
  DBG1(cout<<"-> Entering DecisionEngine::cleanup_var_set..."<<endl;);
  VariableListPtr pvar;
  while (pvar = _list_vars.first()) {
    VariablePtr var = pvar->data();

    _visit_flag[var->ID()] = FALSE;
    _list_vars.remove (pvar);
  }
}


//-----------------------------------------------------------------------------
// Computes set of target variables for implication probing based decision
// making strategies.
// Second function uses the complete list of variables instead of the
// current list of assigned variables.
//-----------------------------------------------------------------------------

int DecisionEngine::compute_target_var_set()
{
  DBG1(cout<<"-> Entering DecisionEngine::comp_target_var_set..."<<endl;);

  int DLevel = _stree.DLevel();
  VariableListPtr pvar = NULL;
  do {
    for_each(apvar,_stree.assigned_variables(DLevel),VariablePtr){
      register VariablePtr var = (VariablePtr) apvar->data();

      for_each(plit,var->literals(),LiteralPtr) {
	LiteralPtr lit = plit->data();
	ClausePtr cl = lit->clause();

	if (cl->state() == SATISFIED) { continue; }

	for_each(plitB,cl->literals(),LiteralPtr) {
	  LiteralPtr litB = plitB->data();

	  VariablePtr nvar = litB->variable();

	  if (nvar == var) { continue; }
	  if (nvar->value() == UNKNOWN && !_visit_flag[nvar->ID()]) {
	    _visit_flag[nvar->ID()] = TRUE;
	    _list_vars.append (nvar);
	    if (pvar == NULL) { pvar = _list_vars.last(); }
	  }
	}
      }
    } // Done with finding neighbor unassigned vars. Now visit all in partition

    for (; pvar; pvar = pvar->next()) {
      register VariablePtr var = pvar->data();

      int figN = 0, figP = 0;
      for_each(plit,var->literals(),LiteralPtr) {
	LiteralPtr lit = plit->data();
	ClausePtr cl = lit->clause();

	if (cl->state() == SATISFIED) { continue; }
	if (cl->free_literals() <= TARGET_LIT_SIZE) {
	  if (lit->sign()) { figN++; }
	  else { figP++; }
	}
	for_each(plitB,cl->literals(),LiteralPtr) {
	  LiteralPtr litB = plitB->data();

	  VariablePtr nvar = litB->variable();

	  if (nvar == var) { continue; }
	  if (nvar->value() == UNKNOWN && !_visit_flag[nvar->ID()]) {
	    _list_vars.append (nvar);
	    _visit_flag[nvar->ID()] = TRUE;
	  }
	}
      }
      int fig = figP * figN + figP + figN;
      _target_vars.insert (var, fig);
    }
  } while (_target_vars.size() < TARGET_VAR_SIZE && --DLevel >= 0);

  CHECK(if (_mode[_GRP_VERBOSITY_] >= 4)
	{cout<<"TARGET VAR SIZE: "<<_target_vars.size()<<endl;});

  SortedListItem<VariablePtr> *tpvar, *spvar = _target_vars.first();
  for (int k = 0; k < TARGET_VAR_SIZE && spvar ; k++) {
    spvar = spvar->next();
  }
  while ((tpvar = _target_vars.last()) != spvar) {
    _target_vars.remove (tpvar);
  }
  cleanup_var_set();
}


int DecisionEngine::identify_target_var_set()
{
  DBG1(cout<<"-> Entering DecisionEngine::ident_target_var_set..."<<endl;);

  VariableListPtr pvar;
  for (pvar = _clDB.variables().first(); pvar; pvar = pvar->next()) {
    register VariablePtr var = (VariablePtr) pvar->data();

    if (var->value() != UNKNOWN) { continue; }

    int figN = 0, figP = 0;
    for_each(plit,var->literals(),LiteralPtr) {
      LiteralPtr lit = plit->data();
      ClausePtr cl = lit->clause();

      if (cl->state() == SATISFIED) { continue; }
      if (cl->free_literals() == 2) {
	if (lit->sign()) { figN++; }
	else { figP++; }
      }
    }
    int fig = figP * figN + figP + figN;
    _target_vars.insert (var, fig);
  }
  CHECK(if (_mode[_GRP_VERBOSITY_] >= 4)
	{cout<<"TARGET VAR SIZE: "<<_target_vars.size()<<endl;});

  SortedListItem<VariablePtr> *tpvar, *spvar = _target_vars.first();
  for (int k = 0; k < TARGET_VAR_SIZE && spvar ; k++) ;
  while ((tpvar = _target_vars.last()) != spvar) {
    _target_vars.remove (tpvar);
  }
}

***** */



//-----------------------------------------------------------------------------
// Dynamic orderings for decision variables.
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Ordering procedures for dynamic decision making.
//
// Key: MSOS - my own version of the one-sided JW-based heuristic.
//      MSTS - my own version of the two-sided JW-based heuristic.
//      MSMM - my own version of the MOM's heuristic.
//      BOHM - Bohm's heuristic.
//      DLIS - dynamic variation of LIS.
//      DLCS - dynamic variation of LCS.
//      DSIS - dynamic variation of SIS.
//-----------------------------------------------------------------------------

int DecisionEngine::select_MSOS (DVariableArray &var_set)
{
  DBG1(cout<<"Making MSOS dynamic decision assignment"<<endl;);
  int target_figure = (-1);
  VariablePtr target_var = NULL;
  int target_value = NONE;

  for (register int k = 0; k < var_set.size(); k++) {
    VariablePtr var = var_set[k];
    if (var->value() != UNKNOWN) { continue; }

    // Traverse literals of var and update figures.

    register int figureT = 0, figureF = 0;
    for (register int j = 0; j < var->pos_literals().size(); j++) {
      ClausePtr cl = var->pos_literals()[j]->clause();
      if (cl->state()== SATISFIED || cl->free_literals()>MAX_RELEVANT_SIZE ||
	  _mode[_GRP_SKIP_CONF_CLS_] && (cl->ID()>_init_cl_num)) {
	continue;
      }
      figureT += shifted_pivot[MAX_RELEVANT_SIZE-cl->free_literals()];
      figureF++;              // Clause that is satisfied if var is F
    }
    for (register int j = 0; j < var->neg_literals().size(); j++) {
      ClausePtr cl = var->neg_literals()[j]->clause();
      if (cl->state()== SATISFIED || cl->free_literals()>MAX_RELEVANT_SIZE ||
	  _mode[_GRP_SKIP_CONF_CLS_] && (cl->ID()>_init_cl_num)) {
	continue;
      }
      figureF += shifted_pivot[MAX_RELEVANT_SIZE-cl->free_literals()];
      figureT++;              // Clause that is satisfied if var is F
    }
    if (figureT >= figureF && figureT > target_figure) {
      target_var = var;
      target_value = TRUE;
      target_figure = figureT;
    }
    else if (figureF >= figureT && figureF > target_figure) {
      target_var = var;
      target_value = FALSE;
      target_figure = figureF;
    }
    else if (_mode[_GRP_RANDOMIZE_] > 2) {
      int swap_vars = (int) rint((random()*1.0)/RAND_MAX);
      if (swap_vars) {
	if (figureT >= figureF && (1+swap_range)*figureT > target_figure) {
	  target_var = var;
	  target_value = TRUE;
	}
	else if (figureT < figureF && (1+swap_range)*figureF > target_figure) {
	  target_var = var;
	  target_value = FALSE;
	}
      }
    }
    else if (_mode[_GRP_RANDOMIZE_] > 1) {
      int swap_vars = (int) rint((random()*1.0)/RAND_MAX);
      if (swap_vars) {
	if (figureT == target_figure) {
	  target_var = var;
	  target_value = TRUE;
	}
	else if (figureF == target_figure) {
	  target_var = var;
	  target_value = FALSE;
	}
      }
    }
  }
  if (!target_var) { return NO_DECISION; }
  make_decision_assignment (target_var, target_value );
  return DECISION;
}

int DecisionEngine::select_MSTS (DVariableArray &var_set)
{
  DBG1(cout<<"Making MSTS dynamic decision assignment"<<endl;);
  int target_figure = (-1);
  VariablePtr target_var = NULL;
  int target_value = NONE;

  for (register int k = 0; k < var_set.size(); k++) {
    VariablePtr var = var_set[k];
    if (var->value() != UNKNOWN) { continue; }

    // Traverse literals of var and update figures.

    register int figureT = 0, figureF = 0;
    for (register int j = 0; j < var->pos_literals().size(); j++) {
      ClausePtr cl = var->pos_literals()[j]->clause();
      if (cl->state()== SATISFIED || cl->free_literals()>MAX_RELEVANT_SIZE ||
	  _mode[_GRP_SKIP_CONF_CLS_] && (cl->ID()>_init_cl_num)) {
	continue;
      }
      figureT += shifted_pivot[MAX_RELEVANT_SIZE-cl->free_literals()];
      figureF++;              // Clause that is satisfied if var is F
    }
    for (register int j = 0; j < var->neg_literals().size(); j++) {
      ClausePtr cl = var->neg_literals()[j]->clause();
      if (cl->state()== SATISFIED || cl->free_literals()>MAX_RELEVANT_SIZE ||
	  _mode[_GRP_SKIP_CONF_CLS_] && (cl->ID()>_init_cl_num)) {
	continue;
      }
      figureF += shifted_pivot[MAX_RELEVANT_SIZE-cl->free_literals()];
      figureT++;              // Clause that is satisfied if var is F
    }
    if( (figureT + figureF) > target_figure ) {
      target_var = var;
      target_value = (figureT >= figureF) ? TRUE : FALSE;
      target_figure = figureT + figureF;
    }
    else if (_mode[_GRP_RANDOMIZE_] > 2) {
      int swap_vars = (int) rint((random()*1.0)/RAND_MAX);
      if (swap_vars) {
	if ((1+swap_range)*(figureF+figureT) > target_figure) {
	  target_var = var;
	  target_value = TRUE;
	}
      }
    }
    else if (_mode[_GRP_RANDOMIZE_] > 1) {
      int swap_vars = (int) rint((random()*1.0)/RAND_MAX);
      if (swap_vars) {
	if (figureF+figureT == target_figure) {
	  target_var = var;
	  target_value = TRUE;
	}
      }
    }
  }
  if (!target_var) { return NO_DECISION; }
  make_decision_assignment (target_var, target_value );
  return DECISION;
}

int DecisionEngine::select_BOHM (DVariableArray &var_set)
{
  //-------------------------------------------------------------------------
  // Compute H(x) and W(x). H(x) is defined as a figure of merit for a given
  // clause size, whereas W(x) is a figure of merit for x over a range of
  // clause sizes. These two figures are used for finding a 2-dimensional
  // maximum figure.
  //-------------------------------------------------------------------------
  DBG1(cout<<"Making BOHM dynamic decision assignment"<<endl;);

  int H_figure = (-1);
  int W_figure = (-1);
  // The highest figure of merit is defined by the smallest clause.
  int min_index = _clDB.clauses().size();

  VariablePtr target_var = NULL;
  int target_value = NONE;

  for (register int k = 0; k < var_set.size(); k++) {
    VariablePtr var = var_set[k];
    if (var->value() != UNKNOWN) { continue; }
    register int H_var_figure = 0, W_var_figure = 0;
    register int H_varb_figure = 0, W_varb_figure = 0;
    register int var_min_index = _clDB.clauses().size();

    // Traverse literals of var and update figures.

    for (register int j = 0; j < var->pos_literals().size(); j++) {
      ClausePtr cl = var->pos_literals()[j]->clause();
      if (cl->state()== SATISFIED ||
	  _mode[_GRP_SKIP_CONF_CLS_] && (cl->ID()>_init_cl_num)) { continue; }

      if (cl->free_literals() < var_min_index) {
	var_min_index = cl->free_literals();
	H_var_figure = 1;     // Reset H(x) given the new min index
	H_varb_figure = 0;    // -jpms 4/9/98.
      }
      else if (cl->free_literals() == var_min_index) {
	H_var_figure++;
      }
      W_var_figure += (cl->free_literals() <= MAX_RELEVANT_SIZE) ?
	shifted_pivot[MAX_RELEVANT_SIZE - cl->free_literals()] : 0;
    }
    for (register int j = 0; j < var->neg_literals().size(); j++) {
      ClausePtr cl = var->neg_literals()[j]->clause();
      if (cl->state()== SATISFIED ||
	  _mode[_GRP_SKIP_CONF_CLS_] && (cl->ID()>_init_cl_num)) { continue; }

      if (cl->free_literals() < var_min_index) {
	var_min_index = cl->free_literals();
	H_varb_figure = 1;
	H_var_figure = 0;
      }
      else if (cl->free_literals() == var_min_index) {
	H_varb_figure++;
      }
      W_varb_figure += (cl->free_literals() <= MAX_RELEVANT_SIZE) ?
	shifted_pivot[MAX_RELEVANT_SIZE - cl->free_literals()] : 0;
    }
    int minH = (H_var_figure<H_varb_figure) ? H_var_figure : H_varb_figure;
    int maxH = (H_var_figure>H_varb_figure) ? H_var_figure : H_varb_figure;
    int tmp_H_figure = Balpha*maxH + Bbeta*minH;
    int tmp_W_figure = W_var_figure + W_varb_figure;

    if( var_min_index < min_index ||
	var_min_index == min_index &&
	(tmp_H_figure > H_figure ||
	 tmp_H_figure == H_figure && tmp_W_figure > W_figure) ) {
      min_index = var_min_index;
      H_figure = tmp_H_figure;
      W_figure = tmp_W_figure;
      target_var = var;
      // Using H figure to decide selected value.
      target_value = (H_var_figure > H_varb_figure) ? FALSE : TRUE;
    }
    else if (_mode[_GRP_RANDOMIZE_] > 2) {
      int swap_vars = (int) rint((random()*1.0)/RAND_MAX);
      if (swap_vars) {
	if ((1+swap_range)*tmp_H_figure > H_figure) {
	  min_index = var_min_index;
	  target_var = var;
	  // Using H figure to decide selected value.
	  target_value = (H_var_figure > H_varb_figure) ? FALSE : TRUE;
	}
      }
    }
    else if (_mode[_GRP_RANDOMIZE_] > 1) {
      int swap_vars = (int) rint((random()*1.0)/RAND_MAX);
      if (swap_vars) {
	if (tmp_H_figure == H_figure) {
	  min_index = var_min_index;
	  H_figure = tmp_H_figure;
	  W_figure = tmp_W_figure;
	  target_var = var;
	  // Using H figure to decide selected value.
	  target_value = (H_var_figure > H_varb_figure) ? FALSE : TRUE;
	}
      }
    }
  }
  if (!target_var) { return NO_DECISION; }
  make_decision_assignment (target_var, target_value );
  return DECISION;
}

int DecisionEngine::select_MSMM (DVariableArray &var_set)
{
  //-------------------------------------------------------------------------
  // Compute H(x) and W(x). H(x) is defined as a figure of merit for a given
  // clause size, whereas W(x) is a figure of merit for x over a range of
  // clause sizes. These two figures are used for finding a 2-dimensional
  // maximum figure.
  //-------------------------------------------------------------------------
  DBG1(cout<<"Making MSMM dynamic decision assignment"<<endl;);

  int H_figure = (-1);
  int W_figure = (-1);
  // The highest figure of merit is defined by the smallest clause.
  int min_index = _clDB.clauses().size();

  VariablePtr target_var = NULL;
  int target_value = NONE;

  for (register int k = 0; k < var_set.size(); k++) {
    VariablePtr var = var_set[k];
    if (var->value() != UNKNOWN) { continue; }
    register int H_var_figure = 0, W_var_figure = 0;
    register int H_varb_figure = 0, W_varb_figure = 0;
    register int var_min_index = _clDB.clauses().size();

    // Traverse literals of var and update figures.

    for (register int j = 0; j < var->pos_literals().size(); j++) {
      ClausePtr cl = var->pos_literals()[j]->clause();
      if (cl->state()== SATISFIED ||
	  _mode[_GRP_SKIP_CONF_CLS_] && (cl->ID()>_init_cl_num)) { continue; }

      if (cl->free_literals() < var_min_index) {
	var_min_index = cl->free_literals();
	H_var_figure = 1;     // Reset H(x) given the new min index
	H_varb_figure = 0;    // -jpms 4/9/98.
      }
      else if (cl->free_literals() == var_min_index) {
	H_var_figure++;
      }
      W_var_figure += (cl->free_literals() <= MAX_RELEVANT_SIZE) ?
	shifted_pivot[MAX_RELEVANT_SIZE - cl->free_literals()] : 0;
    }
    for (register int j = 0; j < var->neg_literals().size(); j++) {
      ClausePtr cl = var->neg_literals()[j]->clause();
      if (cl->state()== SATISFIED ||
	  _mode[_GRP_SKIP_CONF_CLS_] && (cl->ID()>_init_cl_num)) { continue; }

      if (cl->free_literals() < var_min_index) {
	var_min_index = cl->free_literals();
	H_varb_figure = 1;
	H_var_figure = 0;
      }
      else if (cl->free_literals() == var_min_index) {
	H_varb_figure++;
      }
      W_varb_figure += (cl->free_literals() <= MAX_RELEVANT_SIZE) ?
	shifted_pivot[MAX_RELEVANT_SIZE - cl->free_literals()] : 0;
    }
    int tmp_H_figure =
      H_var_figure*H_varb_figure + H_var_figure + H_varb_figure;
    int tmp_W_figure = W_var_figure + W_varb_figure;

    if (var_min_index < min_index ||
	var_min_index == min_index &&
	(tmp_H_figure > H_figure ||
	 tmp_H_figure == H_figure && tmp_W_figure > W_figure) ) {
      min_index = var_min_index;
      H_figure = tmp_H_figure;
      W_figure = tmp_W_figure;
      target_var = var;
      // Using H figure to decide selected value.
      target_value = (H_var_figure > H_varb_figure) ? FALSE : TRUE;
    }
    else if (_mode[_GRP_RANDOMIZE_] > 2) {
      int swap_vars = (int) rint((random()*1.0)/RAND_MAX);
      if (swap_vars) {
	if ((1+swap_range)*tmp_H_figure > H_figure) {
	  min_index = var_min_index;
	  target_var = var;
	  // Using H figure to decide selected value.
	  target_value = (H_var_figure > H_varb_figure) ? FALSE : TRUE;
	}
      }
    }
    else if (_mode[_GRP_RANDOMIZE_] > 1) {
      int swap_vars = (int) rint((random()*1.0)/RAND_MAX);
      if (swap_vars) {
	if (tmp_H_figure == H_figure) {
	  min_index = var_min_index;
	  H_figure = tmp_H_figure;
	  W_figure = tmp_W_figure;
	  target_var = var;
	  // Using H figure to decide selected value.
	  target_value = (H_var_figure > H_varb_figure) ? FALSE : TRUE;
	}
      }
    }
  }
  if (!target_var) { return NO_DECISION; }
  make_decision_assignment (target_var, target_value );
  return DECISION;
}

int DecisionEngine::select_DLIS (DVariableArray &var_set)
{
  DBG1(cout<<"Making DLIS dynamic decision assignment"<<endl;);
  VariablePtr target_var = NULL;
  int target_figure = (-1);
  int target_value = NONE;

  register int vsize = var_set.size();
  for (register int k = 0; k < vsize; k++) {
    VariablePtr var = var_set[k];
    if (var->value() != UNKNOWN) { continue; }

    // Traverse literals of var and update figures.

    int figureT = 0, figureF = 0;
    register int lit_size = var->pos_literals().size();
    for (register int j = 0; j < lit_size; j++) {
      ClausePtr cl = var->pos_literals()[j]->clause();
      if (cl->state() == SATISFIED ||
	  _mode[_GRP_SKIP_CONF_CLS_] && (cl->ID()>_init_cl_num)) { continue; }
      else { figureT++; }               // Clause that is satisfied if var is T
    }
    lit_size = var->neg_literals().size();
    for (register int j = 0; j < lit_size; j++) {
      ClausePtr cl = var->neg_literals()[j]->clause();
      if (cl->state() == SATISFIED ||
	  _mode[_GRP_SKIP_CONF_CLS_] && (cl->ID()>_init_cl_num)) { continue; }
      else { figureF++; }               // Clause that is satisfied if var is F
    }
    if (figureT >= figureF && figureT > target_figure) {
      target_var = var;
      target_value = TRUE;
      target_figure = figureT;
    }
    else if (figureF >= figureT && figureF > target_figure) {
      target_var = var;
      target_value = FALSE;
      target_figure = figureF;
    }
    else if (_mode[_GRP_RANDOMIZE_] > 2) {
      int swap_vars = (int) rint((random()*1.0)/RAND_MAX);
      if (swap_vars) {
	if (figureT >= figureF && (1+swap_range)*figureT > target_figure) {
	  target_var = var;
	  target_value = TRUE;
	}
	else if (figureT < figureF && (1+swap_range)*figureF > target_figure) {
	  target_var = var;
	  target_value = FALSE;
	}
      }
    }
    else if (_mode[_GRP_RANDOMIZE_] > 1) {
      int swap_vars = (int) rint((random()*1.0)/RAND_MAX);
      if (swap_vars) {
	if (figureT == target_figure) {
	  target_var = var;
	  target_value = TRUE;
	}
	else if (figureF == target_figure) {
	  target_var = var;
	  target_value = FALSE;
	}
      }
    }
  }
  if (!target_var) { return NO_DECISION; }
  make_decision_assignment (target_var, target_value);
  return DECISION;
}

int DecisionEngine::select_RDLIS (DVariableArray &var_set)
{
  DBG1(cout<<"Making RDLIS dynamic decision assignment"<<endl;);
  VariablePtr target_var = NULL;
  int target_figure = (-1);
  int target_value = NONE;
  
  register int vsize = var_set.size();
  for (register int k = 0; k < vsize; k++) {
    VariablePtr var = var_set[k];
    if (var->value() != UNKNOWN) { continue; }

    // Traverse literals of var and update figures.

    int figureT = 0, figureF = 0;
    register int lit_size = var->pos_literals().size();
    for (register int j = 0; j < lit_size; j++) {
      ClausePtr cl = var->pos_literals()[j]->clause();
      if (cl->state() == SATISFIED ||
	  // cl->free_literals()>MAX_RELEVANT_SIZE ||
	  _mode[_GRP_SKIP_CONF_CLS_] && (cl->ID()>_init_cl_num)) { continue; }
      else { figureT++; }               // Clause that is satisfied if var is T
    }
    lit_size = var->neg_literals().size();
    for (register int j = 0; j < lit_size; j++) {
      ClausePtr cl = var->neg_literals()[j]->clause();
      if (cl->state() == SATISFIED ||
	  // cl->free_literals()>MAX_RELEVANT_SIZE ||
	  _mode[_GRP_SKIP_CONF_CLS_] && (cl->ID()>_init_cl_num)) { continue; }
      else { figureF++; }               // Clause that is satisfied if var is F
    }
    if (figureT >= figureF && figureT > target_figure) {
      target_var = var;
      target_value = TRUE;
      target_figure = figureT;
    }
    else if (figureF >= figureT && figureF > target_figure) {
      target_var = var;
      target_value = FALSE;
      target_figure = figureF;
    }
    else if (_mode[_GRP_RANDOMIZE_] > 2) {
      int swap_vars = (int) rint((random()*1.0)/RAND_MAX);
      if (swap_vars) {
	if (figureT >= figureF && (1+swap_range)*figureT > target_figure) {
	  target_var = var;
	  target_value = TRUE;
	}
	else if (figureT < figureF && (1+swap_range)*figureF > target_figure) {
	  target_var = var;
	  target_value = FALSE;
	}
      }
    }
    else if (_mode[_GRP_RANDOMIZE_] > 1) {
      int swap_vars = (int) rint((random()*1.0)/RAND_MAX);
      if (swap_vars) {
	if (figureT == target_figure) {
	  target_var = var;
	  target_value = TRUE;
	}
	else if (figureF == target_figure) {
	  target_var = var;
	  target_value = FALSE;
	}
      }
    }
  }
  if (!target_var) {
    CHECK(if (_mode[_GRP_VERBOSITY_] >= 4)
	  {Info("NO TARGET VAR IN RDLIS");});
    return NO_DECISION;
  }
  make_decision_assignment (target_var, target_value);
  return DECISION;
}

int DecisionEngine::select_DLISB (DVariableArray &var_set)
{
  DBG1(cout<<"Making DLISB dynamic decision assignment"<<endl;);
  VariablePtr target_var = NULL;
  int target_figure = (-1);
  int target_value = NONE;

  register int vsize = var_set.size();
  for (register int k = 0; k < vsize; k++) {
    VariablePtr var = var_set[k];
    if (var->value() != UNKNOWN) { continue; }

    // Traverse literals of var and update figures.

    int figureT = 0, figureF = 0;
    register int lit_size = var->pos_literals().size();
    for (register int j = 0; j < lit_size; j++) {
      ClausePtr cl = var->pos_literals()[j]->clause();
      if (cl->state() == SATISFIED || cl->free_literals()>MAX_RELEVANT_SIZE ||
	  _mode[_GRP_SKIP_CONF_CLS_] && (cl->ID()>_init_cl_num)) { continue; }
      else { figureT++; }               // Clause that is satisfied if var is T
    }
    lit_size = var->neg_literals().size();
    for (register int j = 0; j < lit_size; j++) {
      ClausePtr cl = var->neg_literals()[j]->clause();
      if (cl->state() == SATISFIED || cl->free_literals()>MAX_RELEVANT_SIZE ||
	  _mode[_GRP_SKIP_CONF_CLS_] && (cl->ID()>_init_cl_num)) { continue; }
      else { figureF++; }               // Clause that is satisfied if var is F
    }
    if (figureT >= figureF && figureT > target_figure) {
      target_var = var;
      //target_value = TRUE;
      target_value = FALSE;
      target_figure = figureT;
    }
    else if (figureF >= figureT && figureF > target_figure) {
      target_var = var;
      //target_value = FALSE;
      target_value = TRUE;
      target_figure = figureF;
    }
    else if (_mode[_GRP_RANDOMIZE_] > 2) {
      int swap_vars = (int) rint((random()*1.0)/RAND_MAX);
      if (swap_vars) {
	if (figureT >= figureF && (1+swap_range)*figureT > target_figure) {
	  target_var = var;
	  target_value = TRUE;
	}
	else if (figureT < figureF && (1+swap_range)*figureF > target_figure) {
	  target_var = var;
	  target_value = FALSE;
	}
      }
    }
    else if (_mode[_GRP_RANDOMIZE_] > 1) {
      int swap_vars = (int) rint((random()*1.0)/RAND_MAX);
      if (swap_vars) {
	if (figureT == target_figure) {
	  target_var = var;
	  target_value = TRUE;
	}
	else if (figureF == target_figure) {
	  target_var = var;
	  target_value = FALSE;
	}
      }
    }
  }
  if (!target_var) {
    CHECK(if (_mode[_GRP_VERBOSITY_] >= 4)
	  {Info("NO TARGET VAR IN DLISB");});
    return NO_DECISION;
  }
  make_decision_assignment (target_var, target_value);
  return DECISION;
}

int DecisionEngine::select_DLCS (DVariableArray &var_set)
{
  DBG1(cout<<"Making DLCS dynamic decision assignment"<<endl;);
  VariablePtr target_var = NULL;
  int target_figure = (-1);
  int target_value = NONE;

  for (register int k = 0; k < var_set.size(); k++) {
    VariablePtr var = var_set[k];
    if (var->value() != UNKNOWN) { continue; }

    // Traverse literals of var and update figures.

    register int figureT = 0, figureF = 0;
    for (register int j = 0; j < var->pos_literals().size(); j++) {
      ClausePtr cl = var->pos_literals()[j]->clause();
      if (cl->state() == SATISFIED ||
	  _mode[_GRP_SKIP_CONF_CLS_] && (cl->ID()>_init_cl_num)) { continue; }
      else { figureT++; }               // Clause that is satisfied if var is T
    }
    for (register int j = 0; j < var->neg_literals().size(); j++) {
      ClausePtr cl = var->neg_literals()[j]->clause();
      if (cl->state() == SATISFIED ||
	  _mode[_GRP_SKIP_CONF_CLS_] && (cl->ID()>_init_cl_num)) { continue; }
      else { figureF++; }               // Clause that is satisfied if var is F
    }
    if ((figureT + figureF) > target_figure) {
      target_var = var;
      target_value = (figureT > figureF) ? TRUE : FALSE;
      target_figure = figureT + figureF;
    }
    else if (_mode[_GRP_RANDOMIZE_] > 2) {
      int swap_vars = (int) rint((random()*1.0)/RAND_MAX);
      if (swap_vars) {
	if ((1+swap_range)*(figureF+figureT) > target_figure) {
	  target_var = var;
	  target_value = TRUE;
	}
      }
    }
    else if (_mode[_GRP_RANDOMIZE_] > 1) {
      int swap_vars = (int) rint((random()*1.0)/RAND_MAX);
      if (swap_vars) {
	if (figureF+figureT == target_figure) {
	  target_var = var;
	  target_value = TRUE;
	}
      }
    }
  }
  if (!target_var) { return NO_DECISION; }
  make_decision_assignment (target_var, target_value);
  return DECISION;
}

int DecisionEngine::select_DSIS (DVariableArray &var_set)
{
  DBG1(cout<<"Making DSIS dynamic decision assignment"<<endl;);
  VariablePtr target_var = NULL;
  int target_figure = var_set.size();
  int target_value = NONE;

  register int vsize = var_set.size();
  for (register int k = 0; k < vsize; k++) {
    VariablePtr var = var_set[k];
    if (var->value() != UNKNOWN) { continue; }

    // Traverse literals of var and update figures.

    int figureT = 0, figureF = 0;
    register int lit_size = var->pos_literals().size();
    for (register int j = 0; j < lit_size; j++) {
      if (var->pos_literals()[j]->clause()->state() == SATISFIED) { continue; }
      else { figureT++; }               // Clause that is satisfied if var is T
    }
    lit_size = var->neg_literals().size();
    for (register int j = 0; j < lit_size; j++) {
      if (var->neg_literals()[j]->clause()->state() == SATISFIED) { continue; }
      else { figureF++; }               // Clause that is satisfied if var is F
    }
    if (figureT <= figureF && figureT < target_figure) {
      target_var = var;
      target_value = TRUE;
      target_figure = figureT;
    }
    else if (figureF <= figureT && figureF < target_figure) {
      target_var = var;
      target_value = FALSE;
      target_figure = figureF;
    }
    else if (_mode[_GRP_RANDOMIZE_] > 2) {
      int swap_vars = (int) rint((random()*1.0)/RAND_MAX);
      if (swap_vars) {
	if (figureT >= figureF && (1+swap_range)*figureT > target_figure) {
	  target_var = var;
	  target_value = TRUE;
	}
	else if (figureT < figureF && (1+swap_range)*figureF > target_figure) {
	  target_var = var;
	  target_value = FALSE;
	}
      }
    }
    else if (_mode[_GRP_RANDOMIZE_] > 1) {
      int swap_vars = (int) rint((random()*1.0)/RAND_MAX);
      if (swap_vars) {
	if (figureT == target_figure) {
	  target_var = var;
	  target_value = TRUE;
	}
	else if (figureF == target_figure) {
	  target_var = var;
	  target_value = FALSE;
	}
      }
    }
  }
  if (!target_var) { return NO_DECISION; }
  make_decision_assignment (target_var, target_value);
  return DECISION;
}

int DecisionEngine::select_SRAND (DVariableArray &var_set)
{
  DBG1(cout<<"Making DSIS dynamic decision assignment"<<endl;);
  VariablePtr target_var = NULL;
  int target_value = (int) rint((random()*1.0)/RAND_MAX);// Define target value
  register int vsize = var_set.size();
  int var_count = 0;
  for (register int k = 0; k < vsize; k++) {
    if (var_set[k]->value() == UNKNOWN) { var_count++; }
  }
  // Define target index.
  var_count--;    // First, decr by 1 to get indeces right.
  int target_index = (int) rint(var_count*(random()*1.0)/RAND_MAX);
  DBG1(cout << "VAR CNT:  " << var_count << endl;
       cout << "TRGT IDX: " << target_index << endl;
       cout << "TRGT VAL: " << target_value << endl;);
  var_count = 0;
  for (register int k = 0; k < vsize; k++) {
    if (var_set[k]->value() == UNKNOWN) {
      if (var_count == target_index) {
	target_var = var_set[k];
      }
      var_count++;
    }
  }
  if (!target_var) { return NO_DECISION; }
  make_decision_assignment (target_var, target_value);
  return DECISION;
}


//-----------------------------------------------------------------------------
// Decision making strategies based on evaluated number of implics.
//-----------------------------------------------------------------------------

// My version of Bayardo-Schrag decision making strategy.


/* *****

int DecisionEngine::select_MVBS (VariableList &var_list)
{
  if (_target_vars.size() == 0) { return select_DLIS (var_list); }

  _decision_var = NULL;
  _decision_value = NONE;

  int result = rank_target_variables();

  CHECK(if (_mode[_GRP_VERBOSITY_] >= 4)
	{cout<<"DECISION VAR: "<<_decision_var->name()<< " WITH VALUE: ";
	cout<<_decision_value<<endl;});

  if (_decision_value == NONE) { _decision_value = bool_random(); }
  make_decision_assignment (_decision_var, _decision_value);

  return DECISION;
}


//-----------------------------------------------------------------------------
// Function: rank_target_variables()
//
// Purpose: Gives a rank to each variable in a list of variables.
//
// Side-effects: The input list is erased.
//
// Notes: Ranking function ought to be configurable.
//-----------------------------------------------------------------------------

int DecisionEngine::rank_target_variables()
{
  // -> postponed: SortedList<VariablePtr> &sort_vars;
  _stree.push_decision();                    // Define a scratch decision level

  int status, max_fig = NONE;
  SortedListItem<VariablePtr> *pvar;
  while (pvar = _target_vars.first()) {
    VariablePtr var = (VariablePtr) pvar->data();

    var->set_state (TRUE, _stree.DLevel(), NULL);
    _stree.add_implied_variable (var);

    status =_bcp.propagate_constraints();
    int figT = _stree.assigned_variables(_stree.DLevel()).size();
    _bca.erase_decision (_stree.DLevel());

    if (status == CONFLICT) {
      _decision_var = var;
      _decision_value = TRUE;
      _target_vars.erase();
      _stree.pop_decision();
      return FALSE;
    }

    var->set_state (FALSE, _stree.DLevel(), NULL);
    _stree.add_implied_variable (var);

    status =_bcp.propagate_constraints();
    int figF = _stree.assigned_variables(_stree.DLevel()).size();
    _bca.erase_decision (_stree.DLevel());

    if (status == CONFLICT) {
      _decision_var = var;
      _decision_value = TRUE;
      _target_vars.erase();
      _stree.pop_decision();
      return FALSE;
    }
    int figure = figT * figF + figT + figF;

    if (figure > max_fig) {
      max_fig = figure;
      _decision_var = var;
    }

    // -> postponed: sort_vars.insert (var, figure);
    _target_vars.remove (pvar);
  }
  _stree.pop_decision();                            // Eliminate scratch DLevel

  _decision_value = bool_random();                     // Randomly choose value

  return TRUE;                          // No immediate conflict was identified
}

***** */

/*****************************************************************************/

