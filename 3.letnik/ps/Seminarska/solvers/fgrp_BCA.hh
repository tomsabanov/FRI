//-----------------------------------------------------------------------------
// File: fgrp_BCA.hh
//
// Purpose: Declaration of a class for implementing Boolean Conflict Analysis
//          (BCA) on clause databases.
//
// Remarks: This class includes the procedure for erasing assignments. It is
//          not clear yet whether this is the best solution.
//          No new variables should be created after running setup(), since
//          a local array indexed with variable IDs is created.
//
// History: 03/27/98 - JPMS - created.
//
// Copyright (c) 1998 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#ifndef __FGRP_BCA__
#define __FGRP_BCA__

#include "fgrp_Defines.hh"
#include "fgrp_STS.hh"
#include "fgrp_CDB.hh"


//-----------------------------------------------------------------------------
// Typedefs for the BCA engine.
//-----------------------------------------------------------------------------

class BCA;

typedef BCA *BCAPtr;


//-----------------------------------------------------------------------------
// Types for tagging variables during literal recording.
//-----------------------------------------------------------------------------

enum ClauseTagFlags {
  TagEmpty = 0x0,        // No tags specified
  TagConf = 0x1,         // Tagging for conf_clause purposes only
  TagBtrack = 0x2,       // Tagging for back_clause purposes only
  TagRecord = 0x4        // Tagging for reording dependencies only
};



//-----------------------------------------------------------------------------
// Class: BCA
//
// Purpose: Implementation of conflict analysis on clause databases.
//-----------------------------------------------------------------------------

class BCA {
  public:

  //---------------------------------------------------------------------------
  // Constructor/destructor.
  //---------------------------------------------------------------------------

  BCA (Array<int> &mode, ClauseDatabase &clDB, SearchTree &stree);
  virtual ~BCA();

  //---------------------------------------------------------------------------
  // Interface contract.
  //---------------------------------------------------------------------------

  int analyze_conflict (ClausePtr cl);     // Analyze conflict due to clause cl

  void erase_decision();            // Erase last implic sequence w/o diagnosis

  ClausePtr explain_conflict (ClausePtr cl);      // Explain causes of conflict
  void record_causes (VariablePtr tvar, VariableList &vlist, List<int> &ilist);
  ClausePtr construct_conf_clause (VariableList &vlist, List<int> &ilist);
  
  void setup();                // Initialize data structures given SAT instance
  void cleanup();               // Reset from data structures wrt past instance

  void output_stats();                           // Print out BCA-related stats

protected:

  //---------------------------------------------------------------------------
  // Variable marking and tagging during conflict analysis
  //---------------------------------------------------------------------------

  inline void mark_variable (VariablePtr var) {  // tag var, but count as marks
    if (!_var_marks[var->ID()]) {
      _var_marks[var->ID()] = TRUE;
      _marked_vars++;
    }
  }
  inline void unmark_variable (VariablePtr var) {
    if (_var_marks[var->ID()]) {
      _var_marks[var->ID()] = FALSE;
      _marked_vars--;
    }
  }
  inline int marked_variable(VariablePtr var) { return _var_marks[var->ID()]; }

  inline void tag_variable (VariablePtr var, int tag) {
    CHECK(if(!tagged_variable(var, tag)) {_tagged_vars++;});
    _var_tags[var->ID()] |= tag;
  }
  inline void untag_variable (VariablePtr var, int tag) {
    CHECK(if(tagged_variable(var, tag)) {_tagged_vars--;});
    _var_tags[var->ID()] &= ~tag;
  }
  inline int tagged_variable (VariablePtr var, int tag) {
    return ((_var_tags[var->ID()] & tag) != 0);
  }
  
  //---------------------------------------------------------------------------
  // Internal inlined functions for simplifying conflict analysis.
  //---------------------------------------------------------------------------

  inline ClausePtr check_btrack_conditions() {
    VariablePtr tvar = _stree.trigger_var_assignment();
    ClausePtr cl = NULL;
    if (tvar) {                         // If tvar=NULL, let analyzer handle it
      if (tvar->antecedent()) {
	cl = _clDB.add_clause (TypeConflict);
      }
      else { _BLevel = _DLevel; }                   // No backtracking required
    }
    return cl;
  }

  inline void add_clause_literal (VariablePtr var, int sign,
				  ClausePtr cl, int tag) {
    if (cl && !tagged_variable (var, tag)) {
      _clDB.add_literal (cl, var, sign);
      tag_variable (var, tag);

      CHECK(if (_mode[_GRP_VERBOSITY_] >= 7) {
	cout << "ADDING literal ";
	if(sign)cout<<"-";cout << var->name()<<endl;
	cl->check_consistency();cout<<endl;});
    }
  }

  void categorize_recorded_clause (ClausePtr cl);

  void tag_list_vars (VariableList &vlist, int tag);
  void untag_list_vars (VariableList &vlist, int tag);

  void untag_clause_vars (ClausePtr cl, int tag);

  void del_clause_literals (ClausePtr cl, int tag);
  void del_conf_clause (ClausePtr cl, int tag);

protected:

  //---------------------------------------------------------------------------
  // Internal general purpose functions.
  //---------------------------------------------------------------------------

  void reset_implied_variables();

  void undo_literal_counters (VariablePtr var);

  int record_lits_n_mark_variables(VariablePtr var, ClausePtr cl);
  int record_lits_n_mark_variables(VariableList &vlist, List<int> &ilist,
				   VariablePtr var,ClausePtr cl);

  void output_var_antecs (VariablePtr var);

protected:
  
  //---------------------------------------------------------------------------
  // Data structures accessed.
  //---------------------------------------------------------------------------

  ClauseDatabase &_clDB;            // Identification of the constraint network
  SearchTree &_stree;                      // Identification of the search tree
  
  Array<int> &_mode;                         // Configuration options for GRASP

  //---------------------------------------------------------------------------
  // Local variables used for implementing the conflict analysis algorithm.
  //---------------------------------------------------------------------------

  int _DLevel;                                 // Current search decision level
  int _BLevel;                          // Computed backtracking decision level

  ClausePtr _conf_clause;              // Conflict-induced clause being created
  ClausePtr _back_clause;                // Clause associated with backtracking
  
  int _marked_vars;
  CHECK(int _tagged_vars;);

  DArray<int> _var_marks;                // Array for associating marks to vars
  DArray<int> _var_tags;                  // Array for associating tags to vars

  //---------------------------------------------------------------------------
  // Stats gathering.
  //---------------------------------------------------------------------------

  int _tot_num_UIPs;                              // Total number of UIPs found
  int _max_num_UIPs;                 // Max number of UIPs for a given conflict
  
  int _tot_conf_clause;                // Number of conflicting clauses created
  int _sum_conf_clause;           // Total # of literals in conflicting clauses

  int _max_conf_clause;                   // Largest conflicting clause created
  int _min_conf_clause;                  // Smallest conflicting clause created

private:

};

#endif // __FGRP_BCA__

/*****************************************************************************/
