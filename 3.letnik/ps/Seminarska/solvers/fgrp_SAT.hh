//-----------------------------------------------------------------------------
// File: fgrp_SAT.hh
//
// Purpose: Declaration of the top-level searhc algorithm for SAT on CNF
//          formulas.
//
// Remarks: Interface with GRASP:
//          -> create an instance of GRASP              (new GRASP(sw_handler))
//          -> configure GRASP and setup its engines    (initialize())
//          -> setup search structures                  (setup())
//          -> solve instance of SAT                    (solve())
//          -> cleanup associated info                  (cleanup())
//
// History: 03/30/98 - JPMS - created.
//
// Copyright (c) 1998 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#ifndef __FGRP_SAT__
#define __FGRP_SAT__


#include "defs.hh"
#include "time.hh"
#include "array.hh"
#include "list.hh"
#include "switches.hh"

#include "fgrp_Defines.hh"
#include "fgrp_Mode.hh"
#include "fgrp_CDB.hh"
#include "fgrp_STS.hh"
#include "fgrp_BCP.hh"
#include "fgrp_BCA.hh"
#include "fgrp_DeductionEng.hh"
#include "fgrp_DiagnosisEng.hh"
#include "fgrp_DecisionEng.hh"



//-----------------------------------------------------------------------------
// Class: GRASP
//
// Purpose: Declaration of the GRASP SAT algorithm.
//-----------------------------------------------------------------------------

class GRASP {
public:

  //-------------------------------------------------------------------------
  // Constructor/destructor.
  //-------------------------------------------------------------------------

  GRASP (ModeOptions &mode, SwitchHandler &sw_handler);
  GRASP (ModeOptions &mode, SwitchHandler &sw_handler, ClauseDatabase &clDB);
  virtual ~GRASP();

  //-------------------------------------------------------------------------
  // Interface contract.
  //-------------------------------------------------------------------------

  virtual void initialize();  // Create engines used during the search process
  virtual void setup();       // Set up data structures given SAT instance
  virtual void cleanup();     // Reset from data structures wrt past instance

  virtual int  solve();       // Evaluate satisfiability of specified instance

  //-------------------------------------------------------------------------
  // Access and setup data structures and access stats.
  //-------------------------------------------------------------------------

  inline void register_const_network (ClauseDatabase &clDB) {
    if (!_clDB) { _clDB = &clDB; }
  }
  inline int &mode (int field) { return _mode[field]; }

  virtual void output_stats();
  virtual void output_assignments (ostream &outs, int assigned_only = TRUE);

protected:

  //-------------------------------------------------------------------------
  // Member functions for implementing different search organizations.
  // Among others the following are available:
  //  - Plain search engine, with no specific features.
  //  - Search procedure with the ability to iterate solutions.
  //  - Search engine with the explicit handling of clause dominance and
  //    partitioning.
  //-------------------------------------------------------------------------

  virtual int find_solution();
  virtual int find_n_iterate_solution();
  /*
    virtual int find_solution_w_partition();
    */
  virtual int preprocess();
  /*
    virtual int identify_dominances_n_partitions();
    */

  //-------------------------------------------------------------------------
  // General-purpose functions for configuration and resource usage.
  //-------------------------------------------------------------------------

  virtual void clean_temporary_structures();

  virtual void register_allowed_switches();
  virtual void configure_options();

  virtual void issue_induced_conflict();      // Used for iterating solutions

  virtual int handle_outcome (int status);

  virtual int solution_found();

  virtual int resources_exceeded();

  virtual int check_consistency();

protected:

  //-------------------------------------------------------------------------
  // Major data structures and engines.
  //-------------------------------------------------------------------------

  ClauseDatabase *_clDB;
  SearchTree *_stree;

  DeductionEnginePtr _deduction_eng;
  DiagnosisEnginePtr _diagnosis_eng;
  DecisionEnginePtr _decision_eng;

  BCPPtr _bcp;
  BCAPtr _bca;

  //  PartitionPtr _root_partition;

  Array<int> &_mode;                                   // Configuration modes
  SwitchHandler &_sw_handler;    // Object for handling configuration options

  //  Array<int> _used_decision;      // Decisions used for satisfying assignment

  Timer _time;                                   // Time elapsed in searching

private:

};


/*

//-----------------------------------------------------------------------------
// Class: GRASP
//
// Purpose: SAT solver algorithm. Basic functionality is also provided as
//          part of the interface. This aloows different algorithmic
//          organizations.
//-----------------------------------------------------------------------------

class GRASP {
  public:

    //-------------------------------------------------------------------------
    // Constructor/destructor.
    //-------------------------------------------------------------------------

    GRASP();
    virtual ~GRASP();

    //-------------------------------------------------------------------------
    // Interface contract.
    //-------------------------------------------------------------------------

    virtual int initialize();
    virtual int setup();
    virtual int reset();

    virtual int find_solution();

    virtual int configure (mode_ref, mode_value);

    //-------------------------------------------------------------------------
    // Structural manipulation.
    //-------------------------------------------------------------------------


    //-------------------------------------------------------------------------
    // Utilities:
    //
    // A- Derivation of necessary assignments.
    //-------------------------------------------------------------------------

    virtual int BooleanConstraintPropagation();
    virtual int ProbeAssignments();

    //-------------------------------------------------------------------------
    // B- Analysis of conflicts.
    //-------------------------------------------------------------------------

    virtual int BooleanConflictAnalysis();

    //-------------------------------------------------------------------------
    // C- Decision making procedures.
    //-------------------------------------------------------------------------

    virtual int MakeDecisionAssignment();

  protected:

    //-------------------------------------------------------------------------
    // Data structures maintained by the search process.
    //-------------------------------------------------------------------------

    // Stack of variable *must* be an open stack, i.e. with multiple iterators
    // unsat and unit clauses ought to be maintained as lists.
    // Where will dominated clauses go??

    Stack<VariablePtr> _search_tree;    // Stack of assigned variables
    Queue<ClausePtr> _unsat_clauses;    // Queue of unsat clauses
    Queue<ClausePtr> _unit_clauses;     // Queue of unit clauses

    //-------------------------------------------------------------------------
    // Data structures associated with structural information.
    // (Maybe included in clause database).
    //-------------------------------------------------------------------------

    DArray<VariablePtr> _variables;
    DArray<ClausePtr> _clauses;

  private:

};

*/

#endif // __FGRP_SAT__

/*****************************************************************************/

