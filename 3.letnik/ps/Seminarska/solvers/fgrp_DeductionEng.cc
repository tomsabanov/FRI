//-----------------------------------------------------------------------------
// File: fgrp_DeductionEng.cc
//
// Purpose: Definition of the deduction engine, currently based on BCP.
//
// Remarks: --
//
// History: 03/30/98 - JPMS - created.
//
// Copyright (c) 1998 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#include "fgrp_DeductionEng.hh"


extern void dump_clause_literals (ClausePtr cl);



//-----------------------------------------------------------------------------
// Constructors/Destructors and initialization functions for the different
// classes.
//-----------------------------------------------------------------------------


AssignmentDescription::AssignmentDescription (VariablePtr var, int val)
  : _var_cause_list(), _sign_cause_list()
  //: _antec_vars(), _conseq_vars()
{
  _variable = var; _value = val;
}

AssignmentDescription::~AssignmentDescription()
{
  _var_cause_list.erase();
  _sign_cause_list.erase();
  
  //_antec_vars.erase();
  //_conseq_vars.erase();
}



DeductionEngine::DeductionEngine (Array<int> &mode,
				  ClauseDatabase &clDB, SearchTree &stree,
				  BCP &bcp, BCA &bca)
  : _mode (mode), _clDB (clDB), _stree (stree), _bcp (bcp), _bca (bca),
    _assignments_list(), _conf_clauses(),
    _var_marks(), _var_values()
{
  _num_conflicts = _num_conf_cls = _num_pure_lits = 0;
}

DeductionEngine::~DeductionEngine()
{
  CHECK(if(_assignments_list.size())
	{Warn("Leaving assignments in necessary assignments list??");}
	if(_conf_clauses.size())
	{Warn("Leaving recorded conflicting clauses during RL??");});
  _var_marks.resize (0);
}



//-----------------------------------------------------------------------------
// Function: setup()
//
// Purpose: If required, performs internal initializations.
//-----------------------------------------------------------------------------

void DeductionEngine::setup()
{
    DBG1(cout<<"-> Entering DeductionEngine::setup..."<<endl;);
    _var_marks.resize (_clDB.variables().size()+1);
    _var_values.resize (_clDB.variables().size()+1);
    for (register int k = 0; k < _var_marks.size(); k++) {
      _var_marks[k] = 0;
      _var_values[k] = UNKNOWN;
    }
    _num_conflicts = _num_conf_cls = _num_pure_lits = 0;
}


//-----------------------------------------------------------------------------
// Function: cleanup()
//
// Purpose: If required, performs internal clean up.
//-----------------------------------------------------------------------------

void DeductionEngine::cleanup() { }



//-----------------------------------------------------------------------------
// Definition of BCP (Boolean Constraint Propagation) for CNF formulas.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Function: deduce()
//
// Purpose: Implements BCP on CNF formulas. The search tree is updated with
//          identified assigned variables.
//
// Side-effects: Decision tree becomes updated with assigned variables.
//-----------------------------------------------------------------------------

int DeductionEngine::deduce()
{
  DBG1(cout<<"-> Entering DeductionEngine::deduce..."<<endl;);

  _preprocessing = FALSE;
  int consistent = _bcp.propagate_constraints();
  if (!consistent) {
    _num_conflicts++;
    _num_conf_cls += _clDB.number_unsat_clauses();
  }
  else {
    // Current additional deduction modes are based on recursive learning

    if (_mode[_GRP_DEDUCTION_MODE_] == _RECURSIVE_LEARNING_ &&
	_mode[_GRP_DEDUCTION_LEVEL_] >= 1) {                 // Implement RL

      if (!(consistent = examine_DLevel_unresolved_clauses())) {
	_num_conflicts++;
	_num_conf_cls += _clDB.number_unsat_clauses();
      }
    }
  }
  if (consistent && _mode[_GRP_APPLY_PLR_]) {
    identify_pure_literals();
  }
  CHECK(if(!consistent && !_clDB.first_unsat_clause())
	{Warn("Inconsistency detected W/O UNSAT clauses???");});
  DBG1(cout<<"-> Exiting DeductionEngine::deduce..."<<endl;);
  return ((consistent) ? NO_CONFLICT : CONFLICT);
}


//-----------------------------------------------------------------------------
// Function: preprocess()
//
// Purpose: Before starting the search process, identifies necessary
//          assignments.
//
// Side-effects: Decision level 0 becomes updated with assigned variables.
//
// Notes: Current version *only* implements BCP.
//-----------------------------------------------------------------------------

int DeductionEngine::preprocess()
{
    DBG1(cout<<"-> Entering DeductionEngine::preprocess..."<<endl;);

  _preprocessing = TRUE;
  int consistent = _bcp.propagate_constraints();
  if (!consistent) {
    _num_conflicts++;
    _num_conf_cls += _clDB.number_unsat_clauses();
  }
  else {
      // Current additional preprocessing modes are based on recursive learning

    if (_mode[_GRP_PREPROC_MODE_] == _RECURSIVE_LEARNING_ &&
	_mode[_GRP_PREPROC_LEVEL_] >= 1) {                    // Implement RL

      Info("Entering Preprocessing Using Recursive Learning!");

      // 1. Build set of variables used for triggering RL     -> preproc!!
      // 2. For each variable in set:
      //   a. Make truth assignment and apply BCP
      
      if (!(consistent = iterate_probing_assignments())) {
	_num_conflicts++;
	_num_conf_cls += _clDB.number_unsat_clauses();
      }
      //   b. If conflict detected, apply regular conflict analysis
      //   c. For each resulting unresolved clause with assigned literals:
      //      i. Enumerate assignments that satisfy (justify) clause
      //     ii. For each such assignment:
      //        - apply BCP and record resulting assignments
      //        - if inconsistent, analyze conflict
      //    iii. Intersect lists of assignments.
      //     iv. If intersection not empty, then for each common assignment:
      //        - create clause that implies common assignment.
      //        - with conflicts, also include dependencies from conflicts.
      //      v. Apply BCP and check for conflicts.
      Info("Exiting Preprocessing Using Recursive Learning!");
    }
  }
  if (consistent && _mode[_GRP_APPLY_PLR_]) {
    identify_pure_literals();
  }
  DBG1(cout<<"-> Exiting DeductionEngine::preprocess..."<<endl;);
  return ((consistent) ? NO_CONFLICT : CONFLICT);
}


//-----------------------------------------------------------------------------
// Function: iterate_probing_assignments()
//
// Purpose: For preprocessing purposes, each variable is assigned to both
//          logic values so that recursive learning can be triggered.
//-----------------------------------------------------------------------------

int DeductionEngine::iterate_probing_assignments()
{
  DBG1(cout<<"-> Entering DeductionEng::iterate_probing_assigns..."<<endl;);
  int consistent = TRUE;
  for (register int k = 0; k < _clDB.variables().size() && consistent; k++) {
    VariablePtr var = _clDB.variables()[k];
    if (var->value()==UNKNOWN && !(consistent = probe_var_assignments(var))) {
      break;
    }
  }
  DBG1(cout<<"-> Exiting DeductionEng::iterate_probing_assigns..."<<endl;);
  return consistent;
}


//-----------------------------------------------------------------------------
// Function: probe_var_assignments()
//
// Purpose: Probes assigning both logic values to a given target variable.
//
// Side-effects: Decision level 0 becomes updated with assigned variables.
//
// Notes: Function compute_justification_relations() is invoked for each
//        variable triggering an implication sequence.
//        If we used examine_DLevel_unresolved_clauses() instead, many
//        variable assignments would be studied for justification several
//        times.
//        Independently of whether it was implied before, each variable
//        assignment *must* be evaluated separately, so that all the
//        constraints it implies can be identified. Consequently, we do
//        *not* skip any probing assignments.
//-----------------------------------------------------------------------------

int DeductionEngine::probe_var_assignments (VariablePtr var)
{
  DBG1(cout<<"-> Entering DeductionEng::probe_var_assignments ..."<<endl;);
  int consistent = TRUE;

  _probe_var = var;                                // Identify probing variable
  _stree.push_decision();

  if (_mode[_GRP_VERBOSITY_] >= 2) {
    cout<<"PROBING "<<var->name()<<"=1 IN RL"<<endl;
  }
  var->set_state (TRUE, _stree.DLevel(), NULL);             // Apply value TRUE
  _stree.push_implied_variable (var);
  consistent =
    _bcp.propagate_constraints() && compute_justification_relations (var);
  if (consistent) {
    _bca.erase_decision();             // Cleanup previous implication sequence

    if (_mode[_GRP_VERBOSITY_] >= 2) {
      cout<<"PROBING "<<var->name()<<"=0 IN RL"<<endl;
    }
    var->set_state (FALSE, _stree.DLevel(), NULL);         // Apply value FALSE
    _stree.push_implied_variable (var);
    consistent =
      _bcp.propagate_constraints() && compute_justification_relations (var);
    if (consistent) {
      _bca.erase_decision();                    // Cleanup implication sequence
    }
  }
  if (!consistent) {                          // Conflict -> analyze its causes
    ClausePtr unsat_cl = _clDB.first_unsat_clause();
    _clDB.unregister_unsat_clause (unsat_cl);
    int btrack_level;
    if ((btrack_level = _bca.analyze_conflict(unsat_cl)) == _stree.DLevel()) {
      consistent = TRUE;
    }
  }
  _stree.pop_decision();                           // Undo last pushed decision

  // If still consistent identify additional necessary assignments.
  
  consistent = consistent && _bcp.propagate_constraints();
  DBG1(cout<<"-> Exiting DeductionEng::probe_var_assignments ..."<<endl;);
  return consistent;
}


//-----------------------------------------------------------------------------
// Function: examine_DLevel_unresolved_clauses()
//
// Purpose: Applies (level 1) recursive learning during deduction.
//-----------------------------------------------------------------------------

int DeductionEngine::examine_DLevel_unresolved_clauses()
{
  DBG1(cout<<"-> Entering DeductionEng::examn_DLevel_unres_clauses..."<<endl;);
  int consistent = TRUE;
  register int var_count = _mode[_GRP_VAR_PROBE_LIMIT_];
  VariablePtr var;
  _stree.start_var_traversal (FALSE);              // Traverse vars backward...
  while (var = _stree.current_traversal_variable()) {
    if (!(consistent = compute_justification_relations(var)) ||
	(!_preprocessing && --var_count == 0)) {
      break;
    }
    _stree.iterate_var_traversal();
  }
  _stree.stop_var_traversal();
  DBG1(cout<<"-> Exiting DeductionEng::examn_DLevel_unres_clauses..."<<endl;);
  return consistent;
}


//-----------------------------------------------------------------------------
// Function: compute_justification_relations()
//
// Purpose: Examines clauses associated with a variable and evaluates their
//          satisfying justufications, with the objective of identifying
//          necessary assignments.
//-----------------------------------------------------------------------------

int DeductionEngine::compute_justification_relations (VariablePtr var)
{
  DBG1(cout<<"-> Entering DeductionEng::comp_justific_relations ..."<<endl;);
  register int cl_max_count = _mode[_GRP_CL_PROBE_LIMIT_];
  int consistent = TRUE;
  if (var->value()) {      // Only clauses where var is neg literal need justif
    for (register int k = 0; k < var->neg_literals().size(); k++) {
      ClausePtr cl = var->neg_literals()[k]->clause();
      if (cl->is_unresolved() && _clDB.can_probe (cl) &&
	  (_preprocessing ||
	   cl->free_literals() <= _mode[_GRP_LIT_PROBE_LIMIT_])) {
	if (!evaluate_clause_justifications (cl)) {
	  consistent = FALSE;
	  break;
	}
	if (!_preprocessing && --cl_max_count == 0) { break; }
      }
    }
  } else {     // Only clauses where var is positive literal need justification
    for (register int k = 0; k < var->pos_literals().size(); k++) {
      ClausePtr cl = var->pos_literals()[k]->clause();
      if (cl->is_unresolved() && _clDB.can_probe (cl) &&
	  (_preprocessing ||
	   cl->free_literals()<=_mode[_GRP_LIT_PROBE_LIMIT_])) {
	if (!evaluate_clause_justifications (cl)) {
	  consistent = FALSE;
	  break;
	}
	if (!_preprocessing && --cl_max_count == 0) { break; }
      }
    }
  }
  DBG1(cout<<"-> Exiting DeductionEng::comp_justific_relations ..."<<endl;);
  return consistent;
}


//-----------------------------------------------------------------------------
// Function: evaluate_clause_justifications()
//
// Purpose: Given an unresolved clause, its possible satisfying justifications
//          are evaluated.
//
// Notes: num_assigns *must* only be 0 for the first iteration of the
//        first loop. This allows building the initial list of potential
//        assignments.
//-----------------------------------------------------------------------------

int DeductionEngine::evaluate_clause_justifications (ClausePtr cl)
{
  DBG1(cout<<"-> Entering DeductionEng::evaluate_clause_justifics ..."<<endl;);
  int num_assigns = 0;

  // Identify common necessary assignments among justifications of clause
  
  _stree.push_decision();        // Define DLevel for evaluating justifications
  for (register int k = 0; k < cl->pos_literals().size(); k++) {
    VariablePtr tvar = cl->pos_literals()[k]->variable();
    if (tvar->value() == UNKNOWN) {
      if (!analyze_justification (cl, tvar, TRUE, num_assigns++)) { break; }
    }
  }
  for (register int k = 0; k < cl->neg_literals().size(); k++) {
    VariablePtr tvar = cl->neg_literals()[k]->variable();
    if (tvar->value() == UNKNOWN) {
      if (!analyze_justification (cl, tvar, FALSE, num_assigns++)) { break; }
    }
  }
  CHECK(if(_mode[_GRP_VERBOSITY_] >= 3)
	{cout<<"  NUMBER OF COMMON ASSIGNMENTS: "<<_assignments_list.size();
	cout<<endl;});
  // After defining the necessary assignments, we must *explain* why those
  // assignments are indeed necessary if doing deduction.

  if (!_clDB.first_unsat_clause()) {
    if (!_preprocessing || _mode[_GRP_PREPROC_RECORDING_]==_FULL_RECORDING_) {
      compute_assignments_explanations (cl);
      _stree.pop_decision();
    } else {
      _stree.pop_decision();
      establish_necessary_assignments();
    }
  }
  else { _stree.pop_decision(); }             // Go back to the correct DLevel

  clean_internal_data_structures();
  CHECK(if (_conf_clauses.size())
	{Warn("Invalid management of conf clauses during RL!?");}
	if(_assignments_list.size())
	{Warn("Invalid management of CNA's during RL!?");});
  DBG1(cout<<"-> Exiting DeductionEng::evaluate_clause_justifics ..."<<endl;);
  return _bcp.propagate_constraints(); // *Check* for other conflict conditions
}


//-----------------------------------------------------------------------------
// Function: analyze_justification()
//
// Purpose: Evaluates effects of a given satisfying justification to a clause.
//-----------------------------------------------------------------------------

int DeductionEngine::analyze_justification (ClausePtr cl, VariablePtr var,
					    int value, int num_assigns)
{
  DBG1(cout<<"-> Entering DeductionEngine::analyze_justification ..."<<endl;);
  CHECK(if(_mode[_GRP_VERBOSITY_] >= 3)
	{cout<<"  USING "<<var->name()<<"="<<value<<" TO JUSTIFY ";
	cl->dump();cout<<endl;});
  var->set_state (value, _stree.DLevel(), NULL);
  _stree.push_implied_variable (var);
  if (_bcp.propagate_constraints()) {      // NO conflict -> Record assignments
    update_common_assignments_set (num_assigns);   // Update common assigns set
    _bca.erase_decision();
  }
  else {               // Conflict -> Analyze conflict BUT keep recorded clause
    // Identify causes of conflict and update set of recorded clauses.
    // Note that a dedicated conflict analysis engine is being used.
    
    ClausePtr conf_cl = _clDB.first_unsat_clause();
    _clDB.unregister_unsat_clause (conf_cl);
    ClausePtr new_cl = _bca.explain_conflict(conf_cl);
    _conf_clauses.append(new_cl);
    if (!_bcp.propagate_constraints()) {
      conf_cl = _clDB.first_unsat_clause();
      _clDB.unregister_unsat_clause (conf_cl);
      new_cl = _bca.explain_conflict(conf_cl);
      _conf_clauses.append(new_cl);
      return FALSE;
    }
    _bca.erase_decision();
    CHECK(if(conf_cl = _clDB.first_unsat_clause())
	  {cout<<"UNSAT ";conf_cl->dump();cout<<endl;
	  Warn("Leaving UNSAT clauses after explaining conflict!?");});
  }
  DBG1(cout<<"-> Exiting DeductionEngine::analyze_justification ..."<<endl;);
  return TRUE;        // *Check* whether another condition needs to be returned
}


//-----------------------------------------------------------------------------
// Function: update_common_assignments_set()
//
// Purpose: Updates existing set of potential common assignments.
//-----------------------------------------------------------------------------

int DeductionEngine::update_common_assignments_set (int num_assigns)
{
  DBG1(cout<<"-> Entering DeductionEng::update_common_assigns_set ..."<<endl;
       cout<<"NUM_ASSIGNS: "<<num_assigns<<endl;);
  // 1. Prepare set of currently assumed common assignments

  VariablePtr var;
  register AssignDescListPtr pdesc, tpdesc;
  for (pdesc = _assignments_list.first(); pdesc; pdesc = pdesc->next()) {
    var = pdesc->data()->variable();
    _var_marks[var->ID()] = 1;                    // Initialize match count = 1
    _var_values[var->ID()] = pdesc->data()->value();
  }

  // 2. For each most recently implied assignment, check whether it belongs
  //    to the existing set.

  _stree.start_var_traversal();
  while (var = _stree.current_traversal_variable()) {
    if (_var_marks[var->ID()] && var->value() == _var_values[var->ID()]) {
      _var_marks[var->ID()]++;                     // Update match count (<= 2)
    }
    else if (num_assigns == 0) {
      AssignmentDescriptionPtr adesc = new
	AssignmentDescription (var, var->value());
      _assignments_list.append (adesc);
    }
    _stree.iterate_var_traversal();
  }
  _stree.stop_var_traversal();

  // 3. If first implication sequence, identify potential common assignments.
  //     Otherwise, remove those initial assignments that do not have 2 matches

  for (pdesc = _assignments_list.first(); pdesc;) {
    var = pdesc->data()->variable();
    tpdesc = pdesc->next();
    if (num_assigns) {
      if (_var_marks[var->ID()] < 2) {           // Must del assign description
	delete pdesc->data();
	_assignments_list.remove (pdesc);
      }
      _var_marks[var->ID()] = 0;
      _var_values[var->ID()] = UNKNOWN;
    }
    pdesc = tpdesc;
  }                   // Now, _assignments_list contains the common assignments
  DBG1(if(_mode[_GRP_VERBOSITY_]>=5){dump_assign_desc_list(_assignments_list);}
       cout<<"-> Exiting DeductionEng::update_common_assigns_set ..."<<endl;);
  return TRUE;  
}


//-----------------------------------------------------------------------------
// Function: compute_assignments_explanations()
//
// Purpose: For each assignment satisfying target clause, identify (again)
//          the implied assignments. From these assignments, select each
//          of the computed necessary assignments and establish a
//          justification for why the assignments is necessary. Recorded
//          conflict-induced clauses *must* also be taken into account.
//-----------------------------------------------------------------------------

int DeductionEngine::compute_assignments_explanations (ClausePtr cl) 
{
  DBG1(cout<<"-> Entering DeductionEng::comp_assigns_explanations ..."<<endl;
       if(_mode[_GRP_VERBOSITY_]>=5)
       {dump_assign_desc_list(_assignments_list);});
  // 1. Start by associating conf clause dependencies with each common
  //    assignment. Moreover, add also justifying clause related literals
  //    Before that, resize number of CNA's (common necessary assignments).

  AssignDescListPtr pdesc;
  while (_assignments_list.size() > _mode[_GRP_CNA_LIMIT_]) {
    pdesc = _assignments_list.last();
    delete pdesc->data();
    _assignments_list.remove (pdesc);
  }

  add_conf_clause_dependencies();
  add_justifying_clause_dependencies (cl);

  // 2. Trace justifications in order to construct assignments justifications.

  for (register int k = 0; k < cl->pos_literals().size(); k++) {
    VariablePtr tvar = cl->pos_literals()[k]->variable();
    if (tvar->value() == UNKNOWN) { redo_justification (cl, tvar, TRUE); }
  }
  for (register int k = 0; k < cl->neg_literals().size(); k++) {
    VariablePtr tvar = cl->neg_literals()[k]->variable();
    if (tvar->value() == UNKNOWN) { redo_justification (cl, tvar, FALSE); }
  }
  // 3. Create new conflicting clause and clean up used structures.

  while (pdesc = _assignments_list.first()) {    // Clean up common assignments
    AssignmentDescriptionPtr adesc = pdesc->data();

    adesc->var_cause_list().append (adesc->variable());
    adesc->sign_cause_list().append (1-adesc->value());

    _bca.construct_conf_clause (adesc->var_cause_list(),
				adesc->sign_cause_list());
    delete adesc;
    _assignments_list.remove (pdesc);
  }
  DBG1(cout<<"-> Exiting DeductionEng::comp_assigns_explanations ..."<<endl;);
  return TRUE;
}


//-----------------------------------------------------------------------------
// Function: redo_justification()
//
// Purpose: For a given satisfying justification to a clause, the resulting
//          implication sequence is recreated, so that explanations for the
//          identified necessary assignments can be identified.
//-----------------------------------------------------------------------------

int DeductionEngine::redo_justification (ClausePtr cl,
					 VariablePtr var, int value)
{
  DBG1(cout<<"-> Entering DeductionEngine::redo_justification ..."<<endl;);
  var->set_state (value, _stree.DLevel(), NULL);
  _stree.push_implied_variable (var);
  if (_bcp.propagate_constraints()) {       // Skip inconsistent justifications
    CHECK(if(_clDB.first_unsat_clause())
	  {Warn("Unsat clauses while redoing justification??");});
    update_explaining_clauses();      // Update clauses explanating assignments
  }
  _bca.erase_decision();
  return TRUE;        // *Check* whether another condition needs to be returned
}


//-----------------------------------------------------------------------------
// Function: update_explaining_clauses()
//
// Purpose: Given an implication sequence, assignments deemed necessary are
//          used for defining the explanations of common assignments.
//
// Side-effects: Explaining clauses get updated.
//-----------------------------------------------------------------------------

void DeductionEngine::update_explaining_clauses()
{
  DBG1(cout<<"-> Entering DeductionEng::update_explaining_clauses..."<<endl;);
  register AssignDescListPtr pdesc;
  for (pdesc = _assignments_list.first(); pdesc; pdesc = pdesc->next()) {
    AssignmentDescriptionPtr adesc = pdesc->data();
    _bca.record_causes (adesc->variable(),
			adesc->var_cause_list(), adesc->sign_cause_list());
  }
  DBG1(cout<<"-> Entering DeductionEng::update_explaining_clauses..."<<endl;);
}


//-----------------------------------------------------------------------------
// Function: add_conf_clause_dependencies()
//
// Purpose: Adds dependencies charactering each common assignments.
//
// Side-effects: Selected literals of each conflicting clause are defined
//               as dependencies.
//
// Notes: Skipped literal *must* be associated with the trigger variable
//        assignment used for attempting a clause satisfying justification.
//        This literal *must* be the only literal with an unassigned variable.
//-----------------------------------------------------------------------------

void DeductionEngine::add_conf_clause_dependencies()
{
  DBG1(cout<<"-> Entering DeductionEng::add_conf_clause_depends ..."<<endl;);
  register ClauseListPtr pcl;
  for (pcl = _conf_clauses.first(); pcl; pcl = pcl->next()) {
    ClausePtr conf_cl = pcl->data();
    for (register int k = 0; k < conf_cl->pos_literals().size(); k++) {
      LiteralPtr lit = conf_cl->pos_literals()[k];
      if (lit->variable()->value() != UNKNOWN) {
	AssignDescListPtr pdesc;
	for (pdesc = _assignments_list.first(); pdesc; pdesc = pdesc->next()) {
	  AssignmentDescriptionPtr adesc = pdesc->data();
	  adesc->var_cause_list().append (lit->variable());
	  adesc->sign_cause_list().append (lit->sign());
	}
      }
    }
    for (register int k = 0; k < conf_cl->neg_literals().size(); k++) {
      LiteralPtr lit = conf_cl->neg_literals()[k];
      if (lit->variable()->value() != UNKNOWN) {
	AssignDescListPtr pdesc;
	for (pdesc = _assignments_list.first(); pdesc; pdesc = pdesc->next()) {
	  AssignmentDescriptionPtr adesc = pdesc->data();
	  adesc->var_cause_list().append (lit->variable());
	  adesc->sign_cause_list().append (lit->sign());
	}
      }
    }
  }
  DBG1(cout<<"-> Exiting DeductionEng::add_conf_clause_depends ..."<<endl;);
}


//-----------------------------------------------------------------------------
// Function: add_justifying_clause_dependencies()
//
// Purpose: Given clause being justified, its specific dependencies are also
//          recorded.
//-----------------------------------------------------------------------------

void DeductionEngine::add_justifying_clause_dependencies (ClausePtr cl)
{
  DBG1(cout<<"-> Entering DeductionEng::add_justif_clause_depends ..."<<endl;);
  for (register int k = 0; k < cl->pos_literals().size(); k++) {
    LiteralPtr lit = cl->pos_literals()[k];
    if (lit->variable()->value() != UNKNOWN) {
      AssignDescListPtr pdesc;
      for (pdesc = _assignments_list.first(); pdesc; pdesc = pdesc->next()) {
	AssignmentDescriptionPtr adesc = pdesc->data();
	adesc->var_cause_list().append (lit->variable());
	adesc->sign_cause_list().append (lit->sign());
      }
    }
  }
  for (register int k = 0; k < cl->neg_literals().size(); k++) {
    LiteralPtr lit = cl->neg_literals()[k];
    if (lit->variable()->value() != UNKNOWN) {
      AssignDescListPtr pdesc;
      for (pdesc = _assignments_list.first(); pdesc; pdesc = pdesc->next()) {
	AssignmentDescriptionPtr adesc = pdesc->data();
	adesc->var_cause_list().append (lit->variable());
	adesc->sign_cause_list().append (lit->sign());
      }
    }
  }
}


//-----------------------------------------------------------------------------
// Function: establish_necessary_assignments()
//
// Purpose: During preprocessing identify necessary assignments.
//-----------------------------------------------------------------------------

void DeductionEngine::establish_necessary_assignments()
{
  DBG1(cout<<"-> Entering DeductionEng::establsh_necessary_assigns..."<<endl;);
  register AssignDescListPtr pdesc;
  while (pdesc = _assignments_list.first()) {    // Clean up common assignments
    AssignmentDescriptionPtr adesc = pdesc->data();

    ClausePtr cl = _clDB.add_clause();
    _clDB.add_literal (cl, _probe_var, _probe_var->value());
    _clDB.add_literal (cl, adesc->variable(), 1-adesc->value());

    if (cl->is_satisfied()) { _clDB.sat_clause_count()++; }
    
    CHECK(if (_mode[_GRP_VERBOSITY_] >= 3)
	  {cout << "NEW RECORDED ";cl->dump(cout);cout<<endl;});
    _assignments_list.remove (pdesc);
    delete adesc;
  }
}


//-----------------------------------------------------------------------------
// Function: identify_pure_literals()
//
// Purpose: Applies the pure literal rule.
//
// Side-effects: Pure literals are identified.
//
// Notes: Traversing all variables. This is slow and ought to be improved upon.
//-----------------------------------------------------------------------------

void DeductionEngine::identify_pure_literals()
{
  register VariablePtr var;
  register ClausePtr cl;

  for (int i = 0; i < _clDB.variables().size(); i++) {
    var = _clDB.variables()[i];
    if (var->value() != UNKNOWN) { continue; }

    int countP = 0, countN = 0;
    for (register int k=0; k<var->pos_literals().size(); k++) {
      if (var->pos_literals()[k]->clause()->is_satisfied()) { countP++; }
    }
    for (register int k=0; k<var->neg_literals().size(); k++) {
      if (var->neg_literals()[k]->clause()->is_satisfied()) { countN++; }
    }
    if (countP == var->pos_literals().size() &&
	countN == var->neg_literals().size()) {
      continue;                             // Does not need to assign variable
    }
    if (countP == var->pos_literals().size()) {     // Variable can be set to 0
      _num_pure_lits++;
      var->set_state (FALSE, _stree.DLevel(), NULL);

      for (register int k=0; k<var->neg_literals().size(); k++) {
	cl = var->neg_literals()[k]->clause();
	++cl->val1_neg_literals();
	if (cl->val1_lits() == 1) {
	  _clDB.sat_clause_count()++;              // Increase clause SAT count
	}
	if (cl->test_registered (RegLarge)) {
	  _clDB.unregister_deleted_clause (cl);
	}
      }
      for (register int k=0; k<var->pos_literals().size(); k++) {
	cl = var->pos_literals()[k]->clause();
	cl->val0_pos_literals()++;

	if (cl->test_registered (RegLarge)) {
	  _clDB.unregister_deleted_clause (cl);
	}
      }
      _stree.push_assigned_variable (var);
    }
    if (countN == var->neg_literals().size()) {     // Variable can be set to 1
      _num_pure_lits++;
      var->set_state (TRUE, _stree.DLevel(), NULL);

      for (register int k=0; k<var->pos_literals().size(); k++) {
	cl = var->pos_literals()[k]->clause();
	++cl->val1_pos_literals();
	if (cl->val1_lits() == 1) {
	  _clDB.sat_clause_count()++;              // Increase clause SAT count
	}
	if (cl->test_registered (RegLarge)) {
	  _clDB.unregister_deleted_clause (cl);
	}
      }
      for (register int k=0; k<var->neg_literals().size(); k++) {
	cl = var->neg_literals()[k]->clause();
	cl->val0_neg_literals()++;

	if (cl->test_registered (RegLarge)) {
	  _clDB.unregister_deleted_clause (cl);
	}
      }
      _stree.push_assigned_variable (var);
    }
  }
}


//-----------------------------------------------------------------------------
// Function: clean_internal_data_structures()
//
// Purpose: Erases list of common necessary assignments.
//-----------------------------------------------------------------------------

void DeductionEngine::clean_internal_data_structures()
{
  AssignDescListPtr pdesc;
  while (pdesc = _assignments_list.first()) {
    delete pdesc->data();
    _assignments_list.remove (pdesc);
  }
  _conf_clauses.erase();                    // Clean up list of created clauses
}


//-----------------------------------------------------------------------------
// Debugging methods.
//-----------------------------------------------------------------------------

void dump_assign_desc_list (AssignDescList &desc_list)
{
  cout<<"----------DUMPING LIST OF ASSIGNMENT DESCRIPTIONS----------\n";
  AssignDescListPtr pdesc;
  for (pdesc = desc_list.first(); pdesc; pdesc = pdesc->next()) {
    AssignmentDescriptionPtr adesc = pdesc->data();
    cout<<"\tASSIGNMENT DESCRIPTION:\n";
    if(adesc->variable()) {cout<<"\t  VAR: "<<adesc->variable()->name()<<endl;}
    cout<<"\t  VALUE: "<<adesc->value()<<endl;
    cout<<"\t  DEPENDS: ";
    VariableListPtr pvar = adesc->var_cause_list().first();
    ListItem<int> *pint = adesc->sign_cause_list().first();
    for(;pvar;pvar=pvar->next(),pint=pint->next()) {
      cout<<"VAR: "<<pvar->data()->name()<<" SGN: "<<pint->data()<<" ";
    }
    cout << endl;
  }
  cout<<"-----------------------------------------------------------\n";
}


//-----------------------------------------------------------------------------
// Accessing stats.
//-----------------------------------------------------------------------------

void DeductionEngine::output_stats()
{
    printItem ("Total number of conflicts", _num_conflicts);
    printItem ("Number of conflict clauses in conflicts", _num_conf_cls);
    printItem();
    printItem ("Number of pure literals", _num_pure_lits);
    printItem();
}

/*****************************************************************************/

