//-----------------------------------------------------------------------------
// File: sat-grasp.cc
//
// Purpose: Front-end for solving instances of SAT specified in CNF format.
//          This is a new version of sat-grasp, with several changes made to
//          FGRASP in terms of its interface.
//
// Remarks: --
//
// History: 04/01/98 - JPMS - created.
//
// Copyright (c) 1998 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <iostream.h>

#include "time.hh"
#include "random.hh"
#include "memory.hh"
#include "switches.hh"

#include "fgrp_Defines.hh"
#include "fgrp_CDB.hh"
#include "fgrp_Mode.hh"
#include "fgrp_SAT.hh"

#include "cnf_Parser2.hh"

#include "printman.hh"


extern void handle_outcome (int status, Timer &time, ClauseDatabase &database);
extern void configure_options (SwitchHandler &sw_handler);
extern void initialize(int argc, char *argv[]);



//-----------------------------------------------------------------------------
// Function: main
//
// Purpose: Top-level actions of sat-grasp.
//-----------------------------------------------------------------------------

main( int argc, char *argv[] )
{
    DBG1(cout<<"-> Entering ::main()."<<endl;);

    initialize(argc, argv);

    //-------------------------------------------------------------------------
    // Next, definition of sat-grasp.
    //-------------------------------------------------------------------------

    Timer time;
    SwitchHandler sw_handler;
    ModeOptions mode;

    ClauseDatabase database (mode);                   // Create clause database
    GRASP sat_solver (mode, sw_handler, database);         // Create SAT solver

    CNF_Parser cnf_parser;
    char *mpage = "http://algos.inesc.pt/~jpms/grasp";
    int inv_flag;
    
    char *fname;
    if( fname = sw_handler.parse (argc, argv, &inv_flag) ) {
	DBG1(cout<<"Filename: "<<fname<<endl;);
	if (inv_flag) {
	  printman();
	  Abort ("Unable to match command line argument. Check man page");
	}
	else {
	  configure_options (sw_handler);
	  if( !cnf_parser.load_CDB (fname, database) ) {
	    printman();
	    cout << "\n    File name: " << fname << endl;
	    Abort("Cannot open input file");
	  }
	}
	sat_solver.initialize();
	sat_solver.setup();
	printTime ("Done creating structures", time.elapsedTime(), "Elapsed");

	int status = sat_solver.solve();                  // Solve SAT instance

	handle_outcome (status, time, database);

	sat_solver.output_stats();                     // Output relevant stats
	sat_solver.cleanup();
    } else {
      printman();
      Abort( "NO file name specified. CNF file name MUST be specified." );
    }
    printTime ("Terminating GRASP", time.totalTime(), "Total");
    CHECK(database.check_final_consistency(FALSE););    // Not consistent state
    DBG1(cout<<"-> Exiting ::main()."<<endl;);
    exit(0);
}

//-----------------------------------------------------------------------------
// Functions used by main for configuration and SAT outcome handling purposes.
//-----------------------------------------------------------------------------


enum TopLevelModes { _VERBOSITY_ = 0x0};

Array<int> _mode;


//-----------------------------------------------------------------------------
// Function: configure_options
//
// Purpose: Sets the top-level run time options.
//-----------------------------------------------------------------------------

void configure_options (SwitchHandler &sw_handler)
{
    DBG1(cout<<"-> Entering configure_options ()."<<endl;);
    SwitchMap *sw_map = sw_handler.switch_arg ("V");

    _mode.resize(2);

    if (sw_map->active()) {
	_mode[_VERBOSITY_] = (int) (*sw_map);
    }
}


//-----------------------------------------------------------------------------
// Function: handle_outcome
//
// Purpose: Prints out information regarding outcome of the SAT algorithm.
//-----------------------------------------------------------------------------

void handle_outcome (int status, Timer &time, ClauseDatabase &database)
{
    DBG1(cout<<"-> Entering handle_outcome ()."<<endl;);
    if( status == SATISFIABLE ) {
	printTime ("Done searching.... SATISFIABLE INSTANCE",
		   time.elapsedTime(), "Elapsed");

//	    if (_mode[_TRIM_SOLUTIONS_]) {
//		int tot_dec, req_dec;
//		if (database.can_trim_solution (tot_dec, req_dec)) {
//		    cout << "\n\tSolution can be trimmed from ";
//		    cout << tot_dec << " to " << req_dec << " decisions\n";
//		    cout << endl;
//		}
//		else {
//		    cout << "\n\t Solution cannot be trimmed\n" << endl;
//		}
//          }

	if (_mode[_VERBOSITY_]) {
	    Info("Solution found");
	    database.output_assignments (cout);
	}
    } else if( status == UNSATISFIABLE ) {
	printTime ("Done searching.... UNSATISFIABLE INSTANCE",
		   time.elapsedTime(), "Elapsed");
	if (_mode[_VERBOSITY_]) {
	    Info("NO solution found");
	}
    } else {
	printTime ("Done searching.... RESOURCES EXCEEDED",
		   time.elapsedTime(), "Elapsed");
	if (_mode[_VERBOSITY_]) {
	    Info("Resources exceeded... Aborted");
	}
    }
}


//-----------------------------------------------------------------------------
// Function: print_cmd_line
//
// Purpose: Prints out the command line arguments.
//-----------------------------------------------------------------------------

void print_cmd_line (int argc, char *argv[])
{
  cout << "GRASP: ";
  for (int i = 0; i < argc; i++) {
    cout << argv[i] << " ";
  }
  cout << "\n";
}


//-----------------------------------------------------------------------------
// Function: initialize
//
// Purpose: Setup up required auxiliar structures to run sat-grasp.
//-----------------------------------------------------------------------------

void initialize(int argc, char *argv[])
{
  //-------------------------------------------------------------------------
  // Initilization of memory management -> depends on the compilation options
  //-------------------------------------------------------------------------

  MEM_MNG_INIT();

  //-------------------------------------------------------------------------
  // Allow for random numbers to be generated
  //-------------------------------------------------------------------------

  init_random (0);
  setTrailString ("GRASP");    // Indicate that GRASP is being run!
  print_cmd_line(argc, argv);
}

/*****************************************************************************/
