//-----------------------------------------------------------------------------
// File: fgrp_DecisionEng.hh
//
// Purpose: Declaration of the decision engine for SAT on CNF formulas.
//
// Remarks: --
//
// History: 03/30/98 - JPMS - created.
//
// Copyright (c) 1998 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#ifndef __FGRP_DECISIONENG__
#define __FGRP_DECISIONENG__

#include "sorted_list.hh"

#include "fgrp_Defines.hh"
#include "fgrp_STS.hh"
#include "fgrp_CDB.hh"
#include "fgrp_BCP.hh"
#include "fgrp_BCA.hh"



//-----------------------------------------------------------------------------
// Typedefs for the decision engine.
//-----------------------------------------------------------------------------

class DecisionEngine;

typedef DecisionEngine *DecisionEnginePtr;



//-----------------------------------------------------------------------------
// Class: VariableOrdering
//
// Purpose:
//-----------------------------------------------------------------------------

class VariableOrdering {
public:

  //-------------------------------------------------------------------------
  // Constructor/destructor.
  //-------------------------------------------------------------------------

  VariableOrdering();
  virtual ~VariableOrdering();

  //-------------------------------------------------------------------------
  // Interface contract.
  //-------------------------------------------------------------------------

  void setup_ordering (VariablePtr nvar, int prefv) {
    _var = nvar; _pref_value = prefv;
  }

  int pref_value() { return _pref_value; }
  VariablePtr var() { return _var; }

protected:

  VariablePtr _var;
  int _pref_value;

private:

};

//-----------------------------------------------------------------------------
// Class: DecisionEngine
//
// Purpose: Decision engine for a SAT algorithm on CNF formulas.
//-----------------------------------------------------------------------------

class DecisionEngine {
public:

  //-------------------------------------------------------------------------
  // Constructor/destructor.
  //-------------------------------------------------------------------------

  DecisionEngine (Array<int> &mode,
		  ClauseDatabase &clDB, SearchTree &stree,
		  BCP &bcp, BCA &bca);
  virtual ~DecisionEngine();

  //-------------------------------------------------------------------------
  // Interface contract.
  //-------------------------------------------------------------------------

  virtual int decide();                    // Select next decision assignment

  virtual void setup();                     // Setup internal data structures
  virtual void cleanup();                // Clean up internal data structures

  virtual void output_stats();            // Print out decision-related stats

protected:

  //-------------------------------------------------------------------------
  // Internal member functions.
  //-------------------------------------------------------------------------

  void make_decision_assignment (VariablePtr var, int value);

  int select_static_assignment();  // Select assignment given static ordering
  int select_dynamic_assignment();      // The same but for dynamic orderings

  //-------------------------------------------------------------------------
  // Functions for static variable ordering.
  //-------------------------------------------------------------------------


  //-------------------------------------------------------------------------
  // Functions for dynamic variable ordering.
  //-------------------------------------------------------------------------

  int select_MSOS (DVariableArray &var_set);
  int select_MSTS (DVariableArray &var_set);
  int select_DLIS (DVariableArray &var_set);
  int select_RDLIS (DVariableArray &var_set);
  int select_DLISB (DVariableArray &var_set);
  int select_DLCS (DVariableArray &var_set);
  int select_BOHM (DVariableArray &var_set);
  int select_MSMM (DVariableArray &var_set);
  int select_DSIS (DVariableArray &var_set);
  int select_SRAND (DVariableArray &var_set);

  //  int select_MVBS (VariableList &var_list);

  //  int compute_local_var_set();
  //  int cleanup_var_set();
  //  int compute_target_var_set();
  //  int identify_target_var_set();
  //  int rank_target_variables();

  //-------------------------------------------------------------------------
  // Data structures for static decision amking procedures.
  //-------------------------------------------------------------------------

  Array<VariableOrdering*> _var_ord;      // Static ordering of variables
  Array<int> _dec_index;                 // Var ID of decision at each DLevel

  //-------------------------------------------------------------------------
  // Externally accessible data structures.
  //-------------------------------------------------------------------------

  ClauseDatabase &_clDB;         // Clause database is the constraint network
  SearchTree &_stree;           // Search tree that keeps the search info

  BCP &_bcp;                             // Engine for constraint propagation
  BCA &_bca;                                  // Engine for conflict analysis

  Array<int> &_mode;                       // Configuration options for GRASP

  int _init_cl_num;        // Initial number of clauses

  //-------------------------------------------------------------------------
  // Stats gathering.
  //-------------------------------------------------------------------------

  int _num_decisions;                       // Total number of decisions made
  int _max_dec_depth;                           // Maximum depth for decision

private:

  //-------------------------------------------------------------------------
  // Identification of a "local" set of variables for decision making.
  //-------------------------------------------------------------------------

  VariableList _list_vars;
  Array<int> _visit_flag;

  //-------------------------------------------------------------------------
  // Data structures used for metric-based decision making.
  //-------------------------------------------------------------------------

  SortedList<Variable*> _target_vars;
  VariablePtr _decision_var;
  int _decision_value;

};

#endif // __FGRP_DECISIONENG__

/*****************************************************************************/

