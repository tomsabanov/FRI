#include <stdio.h>
#include <stdlib.h>
#include <iostream.h>

void printman()
{
cout << "\nsat-grasp syntax: \n";
cout << "    sat-grasp [options] <fname> \n\n";
cout << "Available options: \n";
cout<< "   +Vn\n";
cout<< "   +Bn\n";
cout<< "   +Cn\n";
cout<< "   +Sn\n";
cout<< "   +Tn\n";
cout<< "   +O\n";
cout<< "   +W\n";
cout<< "   +b[NCD]\n";
cout<< "   +d< opt >\n";
cout<< "   +gn\n";
cout<< "   +rtn\n";
cout<< "   +drN\n";
cout<< "   +drsN\n";
cout<< "   +sn\n";
cout<< "   +sa\n";
cout<< "   [+-] u\n";
cout<< "   +wsN\n";
cout<< "   +iln\n";
cout<< "   +im[LGR]\n";
cout<< "   +l\n";
cout<< "   +pln\n";
cout<< "   +pm[LGR]\n";
cout<< "   +pb[ACLV]n\n";
cout<< "   +c\n";
cout<< "   +cj[AOC]\n";
cout<< "   +dc\n";
cout<< "   +dv\n";
cout<< "   +m\n";
cout<< "   +p\n";
cout<< "   +pr[FS]\n";
cout<< "   +r\n";
cout<< "   [+-]sc\n";
cout<< "   +t\n";
cout<< "\nCheck the man page at URL http://algos.inesc.pt/~jpms/grasp.\n";
}
