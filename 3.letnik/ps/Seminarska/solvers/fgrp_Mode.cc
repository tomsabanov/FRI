//-----------------------------------------------------------------------------
// File: fgrp_Mode.cc
//
// Purpose: Definition of a working environment for the sat solver.
//
// Remarks: --
//
// History: 03/30/98 - JPMS - created.
//
// Copyright (c) 1998 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#include "fgrp_Mode.hh"


//-----------------------------------------------------------------------------
// Constructors/destructors.
//-----------------------------------------------------------------------------

ModeOptions::ModeOptions() : _mode(GRP_MAX_OPTION_SIZE)
{
  _mode[_GRP_VERBOSITY_] = 1;           // Others do not require initialization
}

ModeOptions::~ModeOptions() { }


/*****************************************************************************/

