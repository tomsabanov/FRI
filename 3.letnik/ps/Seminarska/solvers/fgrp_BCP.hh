//-----------------------------------------------------------------------------
// File: fgrp_BCP.hh
//
// Purpose: Declaration of an engine that implements Boolean Constraint
//          Propagation (BCP).
//
// Remarks: --
//
// History: 03/26/98 - JPMS - created.
//
// Copyright (c) 1998 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#ifndef __FGRP_BCP__
#define __FGRP_BCP__

#include "fgrp_Defines.hh"
#include "fgrp_CDB.hh"
#include "fgrp_STS.hh"


//-----------------------------------------------------------------------------
// Typedefs for the BCP engine.
//-----------------------------------------------------------------------------

class BCP;

typedef BCP *BCPPtr;



//-----------------------------------------------------------------------------
// Class: BCP
//
// Purpose: Implements BCP on clause databases.
//-----------------------------------------------------------------------------

class BCP {
public:

  //---------------------------------------------------------------------------
  // Constructor/destructor.
  //---------------------------------------------------------------------------

  BCP (Array<int> &mode, ClauseDatabase &clDB, SearchTree &stree);
  virtual ~BCP();

  //---------------------------------------------------------------------------
  // Interface contract.
  //---------------------------------------------------------------------------

  int propagate_constraints();

  void setup();                // Initialize data structures given SAT instance
  void cleanup();               // Reset from data structures wrt past instance

  void output_stats();                           // Print out BCP-related stats

protected:

  //---------------------------------------------------------------------------
  // Internal member functions.
  //---------------------------------------------------------------------------

  void imply_variable_assignment (ClausePtr cl);
  void identify_trigger_assignments();

  inline int resolve_clause_consistency (ClausePtr cl) {
    int consistent = TRUE;
    switch (cl->state()) {
    case UNSATISFIED:
      _clDB.register_unsat_clause (cl);
      consistent = FALSE;
      break;
    case UNIT:
      imply_variable_assignment (cl);
      break;
    default:
      break;
    }
    return consistent;
  }
  
  //---------------------------------------------------------------------------
  // Data structures accessed.
  //---------------------------------------------------------------------------

  ClauseDatabase &_clDB;          // Identification of the constraint network
  SearchTree &_stree;                    // Identification of the search tree

  Array<int> &_mode;                       // Configuration options for GRASP

  //---------------------------------------------------------------------------
  // Stats gathering.
  //---------------------------------------------------------------------------

  int _num_implications;    // Counts number of implied (handled) assignments

private:

};

#endif // __FGRP_BCP__

/*****************************************************************************/
