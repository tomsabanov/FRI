//-----------------------------------------------------------------------------
// File: fgrp_STS.cc
//
// Purpose: Definition of the search tree structures member functions.
//
// Remarks: --
//
// History: 03/28/98 - JPMS - created.
//
// Copyright (c) 1998 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#include "fgrp_STS.hh"



//-----------------------------------------------------------------------------
// Constructors/destructors.
//-----------------------------------------------------------------------------

SearchTree::SearchTree (Array<int> &mode)
  : _mode(mode), _implied_variables(), _assigned_variables(),
    _DLevel_mapping(), _vtraverse_idx(), _forward_flag()
{
  _var_idx = _DLevel = NONE;
}

SearchTree::~SearchTree()
{
  _implied_variables.erase();
  _assigned_variables.resize (0);
  _DLevel_mapping.resize (0);
  _vtraverse_idx.resize(0);
  _forward_flag.resize(0);
}


//-----------------------------------------------------------------------------
// Initialization and clean up of the search process.
//-----------------------------------------------------------------------------

void SearchTree::setup (int max_size)
{
  DBG1(cout<<"-> Entering SearchTree::setup ..."<<endl;
       cout<<"Decision variable size: "<<max_size<<endl;);
  _assigned_variables.resize (2*max_size);
  _DLevel_mapping.resize (2*max_size);
  _toggled_var.resize (2*max_size);
  _vtraverse_idx.resize (2*max_size);
  _forward_flag.resize (2*max_size);
  for (register int k = 0; k < _assigned_variables.size(); k++) {
    _assigned_variables[k] = NULL;
    _vtraverse_idx[k] = NONE;
    _forward_flag[k] = NONE;
  }
  _var_idx = _DLevel = NONE;
  DBG1(cout<<"-> Exiting SearchTree::setup ..."<<endl;);
}

void SearchTree::cleanup()
{
  CHECK(if(_DLevel >= 0) Warn("Search has not backtracked all the way??"););
}


//-----------------------------------------------------------------------------
// Definition of conditions for consistency of decision tree.
//-----------------------------------------------------------------------------

int SearchTree::check_consistency()
{
  DBG1(cout<<"-> Entering SearchTree::check_consistency..."<<endl;);

  int no_dec = TRUE;
  int dlevel = NONE;
  return TRUE;
}


//-----------------------------------------------------------------------------
// Dump function.
//-----------------------------------------------------------------------------

void SearchTree::dump (ostream &outs)
{
  DBG1(cout<<"-> Entering SearchTree::dump ..."<<endl;);

  cout<<"\nSTREE -> DLevel: "<<_DLevel<<" **AND** vIDX: "<<_var_idx<<endl;
  for (register int k = 0; k <= _var_idx; k++) {
    VariablePtr var = _assigned_variables[k];
    cout << " ";
    if (var) {
      if (var->value()) { cout << var->name(); }
      else              { cout << "-" << var->name(); }
      cout << "@" << var->DLevel();
    }
    else { cout << "__"; }
  }
  cout << endl << endl;

  /* *****
  
  register int level=0;
  for (register int k=0; k <= _var_idx; k++) {
    if (_assigned_variables[k] == NULL) {
      outs << "\nDECISION LEVEL: " << level++ << endl;
    }
    else {
      _assigned_variables[k]->dump(outs);
      outs << endl;
    }
  }
  ***** */

  DBG1(cout<<"-> Exiting SearchTree::dump ..."<<endl;);
}

/*****************************************************************************/

