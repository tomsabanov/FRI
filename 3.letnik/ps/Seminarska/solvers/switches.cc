//-----------------------------------------------------------------------------
// File: switches.cc
//
// Purpose: Functions for registering switches in a symbol table and
//          parsing a line of command arguments.
//
// Remarks: --
//
// History: 07/28/96 - JPMS - created.
//
// Copyright (c) 1996 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <iostream.h>

#include "defs.hh"
#include "switches.hh"



//-----------------------------------------------------------------------------
// Constructors and Destructors.
//-----------------------------------------------------------------------------

SwitchMap::SwitchMap (int atype, int etype)
{
    _arg_type = atype;
    _en_type = etype;
    _active = FALSE;
    _arg = NULL;
}

SwitchMap::~SwitchMap() { delete _arg; }


SwitchHandler::SwitchHandler() : _switch_maps (MAX_SWITCHES) {
    register_default_switches();
}

SwitchHandler::~SwitchHandler() { }



//-----------------------------------------------------------------------------
// Utility functions for class SwitchMap.
//-----------------------------------------------------------------------------

void SwitchMap::define_arg(char *str)
{
    _arg = new char [strlen(str)+1];
    char *p = _arg;
    char *q = str;
    while(*(p++) = *(q++)) ;
    DBGn(cout<<"\t-> Input arg:  "<<str<<endl;
         cout<<"\t-> Output arg: "<<_arg<<endl;);
}



//-----------------------------------------------------------------------------
// Definition of operators for the SwitchMap class.
//-----------------------------------------------------------------------------

SwitchMap &SwitchMap::operator= (char *arg) {
    DBGn(cout<<"\t-> Cmd oper = CHAR"<<endl;);
    define_arg(arg);
    _active = TRUE;
    return *this;
}

SwitchMap::operator char*(void) { return _arg; }

SwitchMap::operator int(void) {
    int value;
    if (!_arg || sscanf (_arg, "%d", &value) < 1) { value = 0; }
    return value;
}

SwitchMap::operator long(void) {
    long value;
    if (!_arg || sscanf (_arg, "%ld", &value) < 1) { value = 0; }
    return value;
}

SwitchMap::operator float(void) {
    float value;
    if (!_arg || sscanf (_arg, "%f", &value) < 1) { value = 0; }
    return value;
}

SwitchMap::operator double() {
    double value;
    if (!_arg || sscanf (_arg, "%lf", &value) < 1) { value = 0; }
    return value;
}


void SwitchMap::activate (int nenable)
{
    _enabled = nenable;
    _active = TRUE;
}

void SwitchMap::activate (int nenable, char *narg)
{
    _enabled = nenable;
    _active = TRUE;
    define_arg (narg);
}



//-----------------------------------------------------------------------------
// Member functions for the SwitchHandler class.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Function: register_switch()
//
// Purpose: Defines a new switch in the symbol table of valid switches.
//-----------------------------------------------------------------------------

int SwitchHandler::register_switch (char *sname, int arg_type, int en_type)
{
    DBG1(cout<<"Entering SwitchHandler::register_switch()"<<endl;);
    if (_switch_maps.lookup (sname)) {
	DBG0(cout<<"Already inserted switch: "<<sname<<"\n";);
	return ERROR;                                 // Switch already defined
    }
    char *name = new char [strlen(sname)+1];
    strcpy (name, sname);
    SwitchMap *smap = new SwitchMap (arg_type, en_type);

    _switch_maps.insert (smap, name);

    DBG1(cout<<"Switch added:"<<name<<endl;);
    return NO_ERROR;
}

int SwitchHandler::register_default_switches()
{
    DBG1(cout<<"Entering SwitchHandler::register_default_switches()"<<endl;);
    register_switch ("V", SUFFIX_ARG, SW_EN_ONLY);

    return NO_ERROR;
}


//-----------------------------------------------------------------------------
// Function: parse()
//
// Purpose: Parses command line arguments.
//-----------------------------------------------------------------------------

char *SwitchHandler::parse (int argc, char *argv[], int *inv_flag)
{
    DBG1(cout<<"Entering SwitchHandler::parse()"<<endl;);
    char buffer [MAX_SWITCH_SIZE], *rstr;
    int slength, blength;
    SwitchMap *smap;
    *inv_flag = FALSE;
    for (int k = 1; k < argc-1; ) {      // Last argument *must* be a file name
	strcpy (buffer, argv[k]);
	slength = blength = strlen(buffer);

	//---------------------------------------------------------------------
	// Look up equivalent registered switch in symbol table.
	//---------------------------------------------------------------------

	while (blength) {
	    if (smap = _switch_maps.lookup (&(buffer[1]))) {
		break;
	    } else {
		buffer[blength-1] = '\0';
		blength--;
	    }
	}

	//---------------------------------------------------------------------
	// Identify possible errors.
	//---------------------------------------------------------------------

	if (!smap) {
	  *inv_flag = TRUE;
	  break;
	}
	if (smap->active()) {
	    Abort (argv[k], "Duplicate switches not allowed");
	}
	else if (smap->arg_type() != SUFFIX_ARG && blength != slength) {
	    Abort (argv[k], "Invalid switch construction. Check man page");
	}
	else if (buffer[0] == '+' && smap->en_type() == SW_DS_ONLY) {
	    Abort (argv[k], "Switch cannot be enabled. Check man page");
	}
	else if (buffer[0] == '-' && smap->en_type() == SW_EN_ONLY) {
	    Abort (argv[k], "Switch cannot be disabled. Check man page");
	}

	//---------------------------------------------------------------------
	// Activate switch in symbol table.
	//---------------------------------------------------------------------

	switch (smap->arg_type()) {
	  case ZERO_ARG:
	    smap->activate ((buffer[0]=='+'));
	    break;
	  case SUFFIX_ARG:
	    smap->activate ((buffer[0]=='+'), &(argv[k][blength]));
	    break;
	  case SECOND_ARG:
	    smap->activate ((buffer[0]=='+'), argv[k+1]);
	    k++;
	    break;
	}
	k++;    // Access next argument
    }
    rstr = ((argc>1) && (argv[argc-1][0]!='+') && (argv[argc-1][0]!='-')) ?
      argv[argc-1] : NULL;
    return  rstr;
}

/*****************************************************************************/
