//-----------------------------------------------------------------------------
// File: switches.hh
//
// Purpose: 
//
// Remarks: 
//
// History: 07/28/96 - JPMS - created.
//
// Copyright (c) 1996 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#ifndef __SWITCHES__
#define __SWITCHES__

#include "symbTable.hh"



//-----------------------------------------------------------------------------
// Defines for defining arguments of switches.
//-----------------------------------------------------------------------------

#define MAX_SWITCH_SIZE  128                    // Buffer size to hold switches
#define MAX_SWITCHES      64               // Estimated upper bound on switches

enum { ZERO_ARG = 0x100,                 // No arguments associated with switch
       SUFFIX_ARG,                            // Argument is appended to switch
       SECOND_ARG                    // Argument is defined as a second cmd arg
       };

enum { SW_EN_ONLY = 0x100,               // Only enabling allowed (ie + option)
       SW_DS_ONLY,                      // Only disabling allowed (ie - option)
       SW_ED_BOTH                           // Can be both enabled and disabled
       };

enum { SW_ENABLE = 0x200, SW_DISABLE };



//-----------------------------------------------------------------------------
// Class: SwitchMap
//
// Purpose: Specification of the argument associated with a switch.
//-----------------------------------------------------------------------------

class SwitchMap {
  public:

    //-------------------------------------------------------------------------
    // Constructor/destructor.
    //-------------------------------------------------------------------------

    SwitchMap (int atype, int etype);
    virtual ~SwitchMap();

    //-------------------------------------------------------------------------
    // Interface contract.
    //-------------------------------------------------------------------------

    void activate (int nenable);
    void activate (int nenable, char *narg);

    int arg_type() { return _arg_type; }
    int active() { return _active; }
    int en_type() { return _en_type; }
    int enabled() { return _enabled; }

    SwitchMap &operator= (char *arg);

    operator char*();
    operator int();
    operator long();
    operator float();
    operator double();

  protected:

    void define_arg (char *str);

    int _arg_type;       // Indicates the type of additional arguments accepted
    int _en_type;
    int _active;                     // Indicates whether the switch was chosen
    int _enabled;             // True if switch enables its associated function
    char *_arg;                              // Argument associated with switch

  private:

};


//-----------------------------------------------------------------------------
// Class: SwitchHandler
//
// Purpose: This class permits defining a set of valid switches which can
//          be specified at the command line. Different classes can register
//          their own configuration switches, which will then be recognized
//          as valid. The registration of each switch also permits defining
//          how additional data can be associated with each switch.
//-----------------------------------------------------------------------------

class SwitchHandler {
  public:

    //-------------------------------------------------------------------------
    // Constructor/destructor.
    //-------------------------------------------------------------------------

    SwitchHandler();
    virtual ~SwitchHandler();

    //-------------------------------------------------------------------------
    // Interface contract.
    //-------------------------------------------------------------------------

    char *parse (int argc, char *argv[], int *inv_flag);

    int register_switch (char *sname, int arg_type, int en_type);

    SwitchMap *switch_arg (char *sname) { return _switch_maps.lookup (sname); }

  protected:

    int register_default_switches();

    SymbolTable<SwitchMap*> _switch_maps;

  private:

};

#endif // __SWITCHES__

/*****************************************************************************/
