//-----------------------------------------------------------------------------
// File: larray.hh
//
// Purpose: Template definition of a list, implemented on top of an array.
//
// Remarks: Each item in the entry *must* be accessed by its ID, i.e. a
//          unique integer identification associated with each object in
//          the array. This means each array *must* contains objects each
//          of which is given a unique and distinct ID.
//
// History: 09/26/97 - JPMS - created.
//
// Copyright (c) 1997 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#ifndef __LARRAY__
#define __LARRAY__






#endif // __LARRAY__

/*****************************************************************************/

