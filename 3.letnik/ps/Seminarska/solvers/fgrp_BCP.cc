//-----------------------------------------------------------------------------
// File: fgrp_BCP.cc
//
// Purpose: Definition of the engine for implementing BCP.
//
// Remarks: --
//
// History: 03/26/98 - JPMS - created.
//
// Copyright (c) 1998 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#include "fgrp_BCP.hh"



//-----------------------------------------------------------------------------
// Constructors and Destructors.
//-----------------------------------------------------------------------------

BCP::BCP (Array<int> &mode, ClauseDatabase &clDB, SearchTree &stree)
: _mode(mode), _clDB (clDB), _stree (stree)
{
    _num_implications = 0;
}

BCP::~BCP() { }


//-----------------------------------------------------------------------------
// Initialization and clean up functions.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Function: setup()
//
// Purpose: Prepare BCP for a new instance of SAT.
//-----------------------------------------------------------------------------

void BCP::setup()
{
  DBG1(cout<<"-> Entering BCP::setup..."<<endl;);
  _num_implications = 0;
}


//-----------------------------------------------------------------------------
// Function: cleanup()
//
// Purpose: Clean data structures from previous instance of SAT.
//-----------------------------------------------------------------------------

void BCP::cleanup() { _num_implications = 0; }


//-----------------------------------------------------------------------------
// Implementation of BCP.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Function: propagate_constraints()
//
// Purpose: Applies the basic BCP algorithm for clause databases.
//
// Side-effects: Assigned variables are put into the search tree.
//
// Notes: It is assumed that *all* current unit clauses have already been
//        used to imply variable assignments.
//        Function resolve_clause_consistency can imply a variable
//        assignment or declare the clause unsatisfied.
//-----------------------------------------------------------------------------

int BCP::propagate_constraints()
{
  DBG1(cout<<"-> Entering BCP::propagate_constraints..."<<endl;);

  int consistent = TRUE;
  VariablePtr var;
  register ClausePtr cl;

  identify_trigger_assignments();      // Imply assignments due to unit clauses
  
  while ((consistent || _mode[_GRP_MULTI_CONFLICTS_]) &&
	 (var = _stree.pop_implied_variable())) {
    CHECK(if(_mode[_GRP_VERBOSITY_] >= 8){
      cout<<"BCP: ASSIGNING "<<var->name()<<" = "<<var->value()<<endl;});
    register int status = TRUE;
    if (var->value()) {                            // Variable assigned value 1
      for (register int k=0; k<var->pos_literals().size(); k++) {
	cl = var->pos_literals()[k]->clause();
	++cl->val1_pos_literals();
	if (cl->val1_lits() == 1) {
	  _clDB.sat_clause_count()++;              // Increase clause SAT count
	}
	if (cl->test_registered (RegLarge)) {
	  _clDB.unregister_deleted_clause (cl);
	}
      }
      for (register int k=0; k<var->neg_literals().size(); k++) {
	cl = var->neg_literals()[k]->clause();
	cl->val0_neg_literals()++;
	status = resolve_clause_consistency (cl) && status;

	if (cl->test_registered (RegLarge)) {
	  _clDB.unregister_deleted_clause (cl);
	}
      }
    }
    else {                                         // Variable assigned value 0
      for (register int k=0; k<var->neg_literals().size(); k++) {
	cl = var->neg_literals()[k]->clause();
	++cl->val1_neg_literals();
	if (cl->val1_lits() == 1) {
	  _clDB.sat_clause_count()++;              // Increase clause SAT count
	}
	if (cl->test_registered (RegLarge)) {
	  _clDB.unregister_deleted_clause (cl);
	}
      }
      for (register int k=0; k<var->pos_literals().size(); k++) {
	cl = var->pos_literals()[k]->clause();
	cl->val0_pos_literals()++;
	status = resolve_clause_consistency (cl) && status;

	if (cl->test_registered (RegLarge)) {
	  _clDB.unregister_deleted_clause (cl);
	}
      }
    }
    _stree.push_assigned_variable (var);
    consistent = consistent && status;
  }
  CHECK(if (consistent && _clDB.first_unsat_clause()){
    Warn("Consistency in the presence of UNSAT clauses??");});
    
  return consistent;
}


//-----------------------------------------------------------------------------
// Function: imply_variable_assignment()
//
// Purpose: Given a unit clause, identifies which variable needs to be assigned
//
// Side-effects: A variable assignment is implied.
//
// Notes: --
//-----------------------------------------------------------------------------

void BCP::imply_variable_assignment (ClausePtr cl)
{
  DBG1(cout<<"-> Entering BCP::imply_variable_assignment ..."<<endl;);
  VariablePtr var;
  if (cl->is_neg_unit()) {                        // Negative literal w/o value
    for (register int k=0; k < cl->neg_literals().size(); k++) {
      var = cl->neg_literals()[k]->variable();
      if (var->value() == UNKNOWN) {
	var->set_state (FALSE, _stree.DLevel(), cl);
	_stree.push_implied_variable (var);
	_num_implications++;
	break;
      }
    }
  } else {                                        // Positive literal w/o value
    for (register int k=0; k < cl->pos_literals().size(); k++) {
      VariablePtr var = cl->pos_literals()[k]->variable();
      if (var->value() == UNKNOWN) {
	var->set_state (TRUE, _stree.DLevel(), cl);
	_stree.push_implied_variable (var);
	_num_implications++;
	break;
      }
    }
  }
  CHECK(if (!var) {Warn("NO implied variable!?");});
}


//-----------------------------------------------------------------------------
// Function: identify_trigger_assignments()
//
// Purpose: For each unit clause registered in the decision tree, implies
//          the variable assignment required for consistency.
//
// Side-effects: Variable assignments get implied.
//
// Notes: It does not detect conflicting assignments. Conflict are only
//        detected when updating clause counters.
//-----------------------------------------------------------------------------

void BCP::identify_trigger_assignments()
{
  DBG1(cout<<"-> Entering BCP::identify_trigger_assignments..."<<endl;);
  ClausePtr cl;

  while (cl = _clDB.first_unit_clause()) {
    CHECK(if(!cl->is_unit())
	  {Warn("Non-unit clause in stack of unit clauses");});
    imply_variable_assignment (cl);
    _clDB.unregister_unit_clause (cl);
  }
}


//-----------------------------------------------------------------------------
// Accessing stats.
//-----------------------------------------------------------------------------

void BCP::output_stats()
{
    printItem ("Total number of implied assignments", _num_implications);
    printItem();
}

/*****************************************************************************/
