//-----------------------------------------------------------------------------
// File: sorted_list.hh
//
// Purpose: Template declaration of a sorted list.
//
// Remarks: --.
//
// History: 04/19/97 - JPMS - created.
//
// Copyright (c) 1997 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#ifndef __SORTED_LIST__
#define __SORTED_LIST__


#include <stdio.h>
#include <stdlib.h>
#include <fstream.h>
#include <iostream.h>

#include "defs.hh"
#include "macros.hh"
#include "memory.hh"


template<class T>
class SortedList;


//-----------------------------------------------------------------------------
// Class definitions: SortedListItem and SortedList.
//-----------------------------------------------------------------------------

//
// class: 	SortedListItem
// purpose:	defines entries in a linked list of objects
//
template<class T>
class SortedListItem {
  friend class SortedList<T>;
public:

  //-------------------------------------------------------------------------
  // constructors and destructors
  //-------------------------------------------------------------------------

  inline SortedListItem (const T& data, int rank)
    : _data (data), _prev(0), _next(0), _rank(rank) { }
  inline ~SortedListItem (void) {
    CHECK(if(_next || _prev)
	  Warn("Invalid pointer management while deleting item"););
  }

  //-------------------------------------------------------------------------
  // accessors and modifiers
  //-------------------------------------------------------------------------

  inline SortedListItem *prev(void) const { return _prev; }
  inline SortedListItem *next(void) const { return _next; }
  inline T& data(void) { return _data; }
  inline T& data(const T& n_data) { _data = n_data; return _data; }

  inline int rank(void) { return _rank; }
  inline int rank(int n_rank) { _rank = n_rank; return _rank; }

  //-------------------------------------------------------------------------
  // Memory management of objects of class SortedListItem<T>. External
  // variables have to be used since gcc 2.7.2 does not yet support static
  // variables in templates.
  //-------------------------------------------------------------------------

  DMEM(inline void *operator new(size_t _size) {
    // *** if(!mem_defined) init_memory(); DONE BEFORE ALLOCATING ITEMs ***
    if( !::newList[_size] ) {
      STAT(::memblock_allocs[_size]++;)
	::newList[_size] = (void*) new char[ALLOC_BLOCK_SIZE * _size];
	register int k;
	for( k = 0; k < ALLOC_BLOCK_SIZE-1; k++ ) {
	  ((SortedListItem*)::newList[_size])[k]._next =
	    &(((SortedListItem*)::newList[_size])[k+1]);
	}
	((SortedListItem*)::newList[_size])[k]._next = NULL;
    }
    STAT(::new_count[_size]++;)
      SortedListItem *ret_ptr = (SortedListItem*) ::newList[_size];
      ::newList[_size] = (void*) ((SortedListItem*)::newList[_size])->_next;
      return ret_ptr;
  }
    inline void operator delete(void *ptr) {
      STAT(::del_count[sizeof(SortedListItem)]++;)
	((SortedListItem*)ptr)->_next =
	  (SortedListItem*) ::newList[sizeof(ListItem)];
	::newList[sizeof(SortedListItem)] = ptr;
    })

protected:
  T _data;		 // list data
  int _rank;             // used for sorting purposes
  SortedListItem *_prev;     // pointer to previous list entry
  SortedListItem *_next;     // pointer to next list entry

  //-------------------------------------------------------------------------
  // Restricted methods for contents manipulation.
  //-------------------------------------------------------------------------

  inline void setPrev(SortedListItem *n_prev) { _prev = n_prev; }
  inline void setNext(SortedListItem *n_next) { _next = n_next; }
};


//-----------------------------------------------------------------------------
// Class: SortedList
//
// Purpose: Defines a list of objects of another class T.
//-----------------------------------------------------------------------------

template <class T>
class SortedList {
public:

  //-------------------------------------------------------------------------
  // Constructors, destructors and operators.
  //-------------------------------------------------------------------------

  SortedList(void) : _first(0), _last(0), _size(0) {}
  ~SortedList(void) { erase(); }

  //-------------------------------------------------------------------------
  // Basic interface: access & modification; O(1) operations.
  // SortedList fields access and manipulation methods.
  //-------------------------------------------------------------------------

  inline int size (void) const { return _size; }
  inline SortedListItem<T> *first (void) const { return _first; }
  inline SortedListItem<T> *last (void) const { return _last; }

  //-------------------------------------------------------------------------
  // Insert an item (or a list of items) to a list.
  //-------------------------------------------------------------------------

  inline SortedListItem<T> *insert(SortedListItem<T> *item) {
    int ival = item->rank();

    if (_first) {
      if (_first->rank() <= item->rank()) {
	_first->setPrev (item);
	item->setNext (_first);
	item->setPrev (NULL);
	_first = item;
	_size++;
      }
      else if (_last->rank() >= item->rank()) {
	_last->setNext (item);
	item->setPrev (_last);
	item->setNext (NULL);
	_last = item;
	_size++;
      }
      else {
	register SortedListItem<T> *ptr;
	for (ptr = _first; ptr; ptr = ptr->next()) {
	  if (ptr->rank() <= item->rank()) {
	    insertBefore (item, ptr);
	    break;
	  }
	}
      }
    }
    else {
      _first = _last = item;
      item->setNext (NULL);
      item->setPrev (NULL);
      _size++;
    }
    return item;
  }

  inline SortedListItem<T> *insert (const T& data, int rank) {
    SortedListItem<T> *ptr = new SortedListItem<T> (data, rank);
    insert (ptr);
    return ptr;
  }

    //-------------------------------------------------------------------------
    // Take out an item (or a list of items) from a list. Extract does not
    // delete item, whereas remove does.
    //   Warnings: 1. Extracting a complete list is only consistent if
    //                the specified list is contained in the current list.
    //             2. Iterator should not be active
    //-------------------------------------------------------------------------

    inline void extract(SortedListItem<T> *item) {
	CHECK(if(!item || !_size || !_first)
	      {if(!item) Warn("Extracting NO item!?");
	       if(!_size) Warn("Extracting from list with NO size!?");
	       if(!_first) Warn("Extracting from list with NULL first!?");
	       Abort("Invalid list management: cannot extract entry");});
	if (_first == item)  {
	    if(_last == item)
		_first = _last = NULL;
	    else {
		_first = item->next();
		_first->_prev = NULL;
	    }
	}
	else if (_last == item)  {
	    _last = item->prev();
	    _last->_next = NULL;
	} else {
	    (item->prev())->_next = item->next();
	    (item->next())->_prev = item->prev();
	}
	item->_next = NULL;
	item->_prev = NULL;
	_size--;
    }
  inline void extract(SortedListItem<T> *Lfirst, SortedListItem<T> *Llast,
		      int Lsize) {
	CHECK(if(_size < Lsize)
	      Abort("Invalid list management: cannot extract sub list"););
	if(_first == Lfirst) {
	    if(_last == Llast)
		_first = _last = NULL;
	    else {
		_first = Llast->next();
		_first->_prev = NULL;
	    }
	}
	else if(_last == Llast) {
	    _last = Lfirst->prev();
	    _last->_next = NULL;
	} else {
	    Lfirst->prev()->setNext(Llast->next());
	    Llast->next()->setPrev(Lfirst->prev());
	}
	Llast->setNext(NULL);
	Lfirst->setPrev(NULL);
	_size -= Lsize;
    }
    inline void remove(SortedListItem<T> *item) {
	extract(item);
	delete item;
    }

  //-------------------------------------------------------------------------
  // List management.
  //-------------------------------------------------------------------------

  inline SortedListItem<T> *insertAfter(SortedListItem<T> *ref_ptr,
					SortedListItem<T> *n_ptr) {
    if(ref_ptr->next()) {
      ref_ptr->next()->_prev = n_ptr;
      n_ptr->_next = ref_ptr->next();
    } else {
      _last = n_ptr;
      n_ptr->_next = NULL;
    }
    n_ptr->_prev = ref_ptr;
    ref_ptr->_next = n_ptr;
    _size++;
    return n_ptr;
  }
  inline SortedListItem<T> *insertBefore(SortedListItem<T> *n_ptr,
					 SortedListItem<T> *ref_ptr) {
    if(ref_ptr->prev()) {
      ref_ptr->prev()->_next = n_ptr;
      n_ptr->_prev = ref_ptr->prev();
    } else {
      _first = n_ptr;
      n_ptr->_prev = NULL;
    }
    n_ptr->_next = ref_ptr;
    ref_ptr->_prev = n_ptr;
    _size++;
    return n_ptr;
  }
    //-------------------------------------------------------------------------
    // Additional interface: global list access; O(n) operations.
    //-------------------------------------------------------------------------

    inline void clear(void) { while(_first) extract(_first); }
    inline void erase(void) { while(_first) remove(_first); }

    //-------------------------------------------------------------------------
    // Dump method
    //-------------------------------------------------------------------------

  //    friend ostream &operator<<(ostream &os, const List& list);
  friend ostream &operator<<(ostream &os, const SortedList<T>& list);
  void dump(void) { cout << *this; }

protected:
  SortedListItem<T> *_first;	// points to first entry in list
  SortedListItem<T> *_last;     // points to last entry in list
  int _size;		        // contains length of list
};


//-----------------------------------------------------------------------------
// Definition of the inlined functions of clasess ListItem and List.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Class methods for List.
//-----------------------------------------------------------------------------

// Basic interface: access & modification; O(1) operations. List fields
// access and manipulation methods.

//
// Operators.
//
//template<class T>
//List<T>& List<T>::operator=(List<T>& _from) {
//    erase();
//    for(ListItem<T> *item = _from.first(); item; item = item->next())
//	append (item->data());
//    return *this;
//}


//
// function:   operator<<
// purpose:    provides stream output for lists
// parameters: none
// returns:    void
//
template<class T>
ostream &operator<<(ostream &os, const SortedList<T> &list) {
  for(SortedListItem<T> *item = list.first(); item; item = item->next()) {
    os << item->data() << " ";
  }
  os << endl;
  return os;
}


//-----------------------------------------------------------------------------
// Macros to iterate over lists. These macros are supposed to lead to more
// efficient code than the iterators above.
//-----------------------------------------------------------------------------

#define for_each_sorted(iterV,ListN,typeT) \
for( register SortedListItem<typeT> *iterV = ListN.first(); iterV; \
    iterV=iterV->next() )

#define while_first_sorted(iterV,ListN,typeT) \
register SortedListItem<typeT> *iterV; \
while( iterV = ListN.first() )


#endif // __SORTED_LIST__

/*****************************************************************************/
