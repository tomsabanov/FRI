//-----------------------------------------------------------------------------
// File: random.hh
//
// Purpose: Generator of random numbers.
//
// Remarks: --.
//
// History: 04/19/97 - JPMS - created (inspired on R.Bayardo's generator).
//
// Copyright (c) 1997 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#ifndef __RANDOM__
#define __RANDOM__

extern void init_random (long seed);
extern int bool_random();
extern unsigned int int_random (unsigned int range);

#endif // __RANDOM__

/*****************************************************************************/
