//-----------------------------------------------------------------------------
// File: fgrp_SAT.cc
//
// Purpose: Definition of the search algorithm for SAT on CNF formulas.
//
// Remarks: --
//
// History: 03/30/98 - JPMS - created.
//
// Copyright (c) 1998 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#include "fgrp_SAT.hh"



//-----------------------------------------------------------------------------
// Constructors and Destructors.
//-----------------------------------------------------------------------------

GRASP::GRASP(ModeOptions &mode, SwitchHandler &sw_handler)
  : _sw_handler(sw_handler), _mode(mode.mode())
  //, _used_decision (0)
{
  _clDB = NULL;
  _stree = NULL;
  _deduction_eng = NULL;
  _diagnosis_eng = NULL;
  _decision_eng = NULL;
  _bcp = NULL;
  _bca = NULL;

  register_allowed_switches();          // Define how GRASP can be configured
}

GRASP::GRASP(ModeOptions &mode,SwitchHandler &sw_handler,ClauseDatabase &clDB)
  : _sw_handler(sw_handler), _mode(mode.mode())
{
  _clDB = &clDB;
  _stree = NULL;
  _deduction_eng = NULL;
  _diagnosis_eng = NULL;
  _decision_eng = NULL;
  _bcp = NULL;
  _bca = NULL;

  //  _root_partition = NULL;

  register_allowed_switches();          // Define how GRASP can be configured
}

GRASP::~GRASP()
{
  delete _stree;
  delete _deduction_eng;
  delete _diagnosis_eng;
  delete _decision_eng;
  delete _bcp;
  delete _bca;

  // if (_root_partition) { delete _root_partition; }
}


//-----------------------------------------------------------------------------
// Functions for initialization and finalization of the search process.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Function: initialize()
//
// Purpose: Sets up engines and data structures used by the SAT algorithm.
//
// Notes: I'm using a *single* BCA procedure for both exponential and
//        polynomial clause database growths.
//-----------------------------------------------------------------------------

void GRASP::initialize()
{
  DBG1(cout<<"Inside GRASP_SAT setup function"<<endl;);

  configure_options();

  //-------------------------------------------------------------------------
  // Create search tree according to the number of variables. Note that
  // the worst case is to have N decisions for N variables, thus N+1 entries
  // are required.
  //-------------------------------------------------------------------------

  _stree = new SearchTree (_mode);

  //-------------------------------------------------------------------------
  // Define the forward and backward reasoning engines.
  //-------------------------------------------------------------------------

  _bcp = new BCP (_mode, *_clDB, *_stree);
  _bca = new BCA (_mode, *_clDB, *_stree);

  //-------------------------------------------------------------------------
  // Define the engines directly used by the search process.
  //-------------------------------------------------------------------------

  _deduction_eng = new DeductionEngine(_mode,*_clDB,*_stree,*_bcp,*_bca);
  _diagnosis_eng = new DiagnosisEngine(_mode,*_clDB,*_stree,*_bcp,*_bca);
  _decision_eng = new DecisionEngine(_mode,*_clDB,*_stree,*_bcp,*_bca);

  //-------------------------------------------------------------------------
  // Set up asuxiliar data structures.
  //-------------------------------------------------------------------------

  /* *****
  _used_decision.resize (_clDB->variables().size()+1);
  register int k;
  for (k = 0; k < _used_decision.size(); k++) {
    _used_decision[k] = FALSE;
  }
  ***** */
}


//-----------------------------------------------------------------------------
// Function: setup()
//
// Purpose: Sets up data structures and engines for solving a pre-specified
//          instance of SAT.
//
// Side-effects: Prepares data structures and engines for a new SAT instance.
//
// Notes: I will assume that BCP and BCA have not state and need not any
//        form of initilization and/or cleanup tasks.
//-----------------------------------------------------------------------------

void GRASP::setup()
{
  DBG1(cout<<"-> Entering GRASP::setup..."<<endl;);
  DBG0(if (_mode[_GRP_VERBOSITY_]>=5)
       {cout<<"CLAUSE DATABASE DUMP IN INIT: "<<endl;_clDB->dump(cout);});
  _clDB->setup();
  _stree->setup (_clDB->num_variables()+1);     // DLevel 0 is *not* a decision

  _bcp->setup();
  _bca->setup();
  
  _deduction_eng->setup();
  _diagnosis_eng->setup();
  _decision_eng->setup();

  DBG1(cout<<"-> Exiting GRASP::setup..."<<endl;);
}


//-----------------------------------------------------------------------------
// Function: cleanup()
//
// Purpose: Cleans up the different engines and data structures from solving
//          a previous instance of SAT.
//
// Side-effects: All variables become unassigned. No clause is satisfied or
//               unsatisfied.
//
// Notes: This function's intent is to effectively reset the search after a
//        given instance is solved.
//-----------------------------------------------------------------------------

void GRASP::cleanup()
{
  DBG1(cout<<"-> Entering GRASP::cleanup..."<<endl;);
  _deduction_eng->cleanup();
  _diagnosis_eng->cleanup();
  _decision_eng->cleanup();

  _stree->cleanup();
  _clDB->cleanup();
  DBG1(cout<<"-> Exiting GRASP::cleanup..."<<endl;);
}


//-----------------------------------------------------------------------------
// Function: resources_exceeded()
//
// Purpose: Checks whether too much effort has been spent searching for a
//          soolution to the current instance of SAT.
//-----------------------------------------------------------------------------

int GRASP::resources_exceeded()
{
  DBG1(cout<<"-> Entering GRASP::resources_exceeded..."<<endl;);
  int must_abort = (_time.readTime() > _mode[_GRP_TIME_LIMIT_]) ||
    (_diagnosis_eng->number_conflicts() > _mode[_GRP_CONFLICT_LIMIT_]) ||
    (_diagnosis_eng->number_backtracks() > _mode[_GRP_BACKTRACK_LIMIT_]) ||
    (_clDB->database_growth() > _mode[_GRP_SPACE_LIMIT_]);
  return must_abort;
}


//-----------------------------------------------------------------------------
// Function for searching for a solution.
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Function: solve()
//
// Purpose: Implements the top-level tasks of the search process.
//          Depending on user-defined configuration options, the search
//          procedure can be organized differently.
//
// Side-effects: The clause database is updated to reflect the evolution of
//               the search
//
// Notes:
//-----------------------------------------------------------------------------

int GRASP::solve()
{
  DBG1(cout<<"-> Entering GRASP::solve..."<<endl;);
  _time.presetTime();

  //-------------------------------------------------------------------------
  // Setup phase. Create DLevel = 0.
  //-------------------------------------------------------------------------

  if (!preprocess()) {
    return handle_outcome (UNSATISFIABLE);
  }
  if (_mode[_GRP_VERBOSITY_]) {
    printTime ("Done preprocessing...", _time.elapsedTime(), "SAT Elapsed");
  }

  //-------------------------------------------------------------------------
  // Search phase -- Select which search organization to choose from.
  //                 Currently, three options are available:
  //                 1) Search with dynamic dominance and partitioning.
  //                 2) Search with iteration of computed solutions.
  //                 3) Basic GRASP search algorithm.
  //-------------------------------------------------------------------------

  /*
  if (_mode[_GRP_PARTITION_MODE_] != _NO_PARTITION_) {      // Use partitioning
    CHECK(if (_mode[_GRP_PARTITION_MODE_] != _TOPOLOGICAL_PARTITION_)
	  {Abort("Unavailable partition option was chosen. Aborting...");});
    return handle_outcome (find_solution_w_partition());
  }
  else
  */
  if (_mode[_GRP_ITERATE_SOLUTIONS_] ||
      _mode[_GRP_SOLUTION_NUMBER_] > 1) {         // Iterate computed solutions
    return find_n_iterate_solution();
  }
  else {                                              // Basic search algorithm
    return handle_outcome (find_solution());
  }
}


//-----------------------------------------------------------------------------
// Function: preprocess()
//
// Purpose: Joins together all the preprocessing tasks.
//
// Side-effects: Search process evolves, hence state uis updated.
//-----------------------------------------------------------------------------

int GRASP::preprocess()
{
  DBG1(cout<<"-> Entering GRASP::preprocess..."<<endl;);
  DBG0(if (_mode[_GRP_VERBOSITY_]>=5) _clDB->dump (cout););
  _stree->push_decision();

  if (_deduction_eng->preprocess() == CONFLICT) {
    _bca->erase_decision();
    _stree->pop_decision();
    return FALSE;
  }

  /*
  if (_mode[_GRP_DOMINATE_CLAUSES_]) {
    ConstraintList dom_clauses;
    _clDB->identify_dominated_clauses (dom_clauses);
    _stree->add_dominated_clauses (dom_clauses);
  }
  if (_mode[_GRP_PARTITION_MODE_] == _TOPOLOGICAL_PARTITION_) {
    PartitionList sub_partitions;

    PartitionPtr ipart = _clDB->init_partitions();
    _stree->init_partition_stack (ipart);

    _clDB->identify_partitions (sub_partitions, _stree->current_partition());
    _stree->push_partitions (sub_partitions);
    DBG0(_stree->dump_whole_partition_info(););
  }
  clean_auxiliar_structures();
  DBG1(cout<<"-> Exiting GRASP::preprocess..."<<endl;);
  */
  return TRUE;
}



//-----------------------------------------------------------------------------
// Function: find_solution()
//
// Purpose: Basic search algorithm for satisfiability.
//
// Side-effects:
//
// Notes:
//-----------------------------------------------------------------------------

int GRASP::find_solution()
{
  DBG1(cout<<"-> Entering GRASP::find_solution..."<<endl;);
  while (!solution_found() && _decision_eng->decide() == DECISION) {
    DBG1(if(_mode[_GRP_VERBOSITY_]>=7) {_clDB->dump();_stree->dump();});

    while (_deduction_eng->deduce() == CONFLICT) {
      if (_diagnosis_eng->diagnose() == CONFLICT) {
	return UNSATISFIABLE;
      }
      CHECK(else if(_mode[_GRP_VERBOSITY_]>=2) {check_consistency();});
      DBG1(if(_mode[_GRP_VERBOSITY_]>=7) {_clDB->dump(); _stree->dump();});
    }
    clean_temporary_structures();
    if (resources_exceeded()) { return ABORTED; }

    CHECK(if(_mode[_GRP_VERBOSITY_]>=5) {check_consistency();});
    DBGn(if(_mode[_GRP_VERBOSITY_]>=9) {_clDB->dump(); _stree->dump();});
  }
  return SATISFIABLE;
}


//-----------------------------------------------------------------------------
// Function: find_n_iterate_solution()
//
// Purpose: Search algorithm for SAT with iteration of solutions.
//
// Side-effects:
//
// Notes:
//-----------------------------------------------------------------------------

int GRASP::find_n_iterate_solution()
{
  int number_solutions = _mode[_GRP_SOLUTION_NUMBER_];

  while (_mode[_GRP_ITERATE_SOLUTIONS_] || number_solutions--) {
    CHECK(if (_mode[_GRP_VERBOSITY_]>=5) { check_consistency(); });
    DBG0(if (_mode[_GRP_VERBOSITY_]>=5)
	 {_clDB->dump(cout); _stree->dump(cout);});

    while (!solution_found() && _decision_eng->decide() == DECISION) {
      DBG1(if (_mode[_GRP_VERBOSITY_]>=7)
	   {_clDB->dump(cout); _stree->dump(cout);});
      
      while (_deduction_eng->deduce() == CONFLICT) {
	if (_diagnosis_eng->diagnose() == CONFLICT) {
	  return handle_outcome (UNSATISFIABLE);
	}
	CHECK(else if (_mode[_GRP_VERBOSITY_]>=5)
	{check_consistency();});
	DBG1(if (_mode[_GRP_VERBOSITY_]>=7)
	     {_clDB->dump(cout); _stree->dump(cout);});
      }
      clean_temporary_structures();
      if (resources_exceeded()) { return handle_outcome (ABORTED); }
      CHECK(if (_mode[_GRP_VERBOSITY_]>=5) {check_consistency();});
      DBGn(if(_mode[_GRP_VERBOSITY_]>=9)
	   {_clDB->dump (cout); _stree->dump();});
    }
    handle_outcome (SATISFIABLE);
    if (_mode[_GRP_ITERATE_SOLUTIONS_] || number_solutions > 0) {
      issue_induced_conflict();
      if (_diagnosis_eng->diagnose() == CONFLICT) { return SATISFIABLE; }
    }
  }
  return SATISFIABLE;
}


/* **********

//-----------------------------------------------------------------------------
// Function: find_solution_w_partition()
//
// Purpose: Search algorithm for SAT with identification of partititions.
//
// Side-effects:
//
// Notes:
//-----------------------------------------------------------------------------

int GRASP::find_solution_w_partition()
{
  DBG1(cout<<"-> Entering GRASP::find_solution_w_partition..."<<endl;);

  while (!_clDB->solution_found()) {
    if (_decision_eng->decide() != DECISION) {
      _stree->save_current_partition();
      if(!_stree->current_partition()) {
	break;
      }
    }
    DBG1(if (_mode[_GRP_VERBOSITY_]>=7)
	 {_clDB->dump(cout); _stree->dump(cout);});

    while (_deduction_eng->deduce() == CONFLICT) {
      if (_diagnosis_eng->diagnose() == CONFLICT) {
	return handle_outcome (UNSATISFIABLE);
      }
      CHECK(else if (_mode[_GRP_VERBOSITY_]>=5) {check_consistency();});
      DBG1(if (_mode[_GRP_VERBOSITY_]>=7)
	   {_clDB->dump(cout); _stree->dump(cout);});
    }
    identify_dominances_n_partitions();   // Find partitions and dominated cl's

    clean_auxiliar_structures();
    if (resources_exceeded()) {
      return handle_outcome (ABORTED);
    }
    CHECK(if (_mode[_GRP_VERBOSITY_]>=5) {check_consistency();});
    DBGn(if(_mode[_GRP_VERBOSITY_]>=9) {_clDB->dump (cout); _stree->dump();});
  }
  CHECK(if (_stree->current_partition())
	{_stree->current_partition()->dump();
	 cout<<"Partition stack size: "<<_stree->partition_stack_size()<<endl;
	 Warn("Still partitions available?");});
  return handle_outcome (SATISFIABLE);
}



//-----------------------------------------------------------------------------
// Function: identify_dominances_n_partitions()
//
// Purpose: After a non-conflicting decision and implication sequence,
//          identifies dominated clauses and new sub-partitions.
//
// Side-effects:
//
// Notes:
//-----------------------------------------------------------------------------

int GRASP::identify_dominances_n_partitions()
{
  DBG1(cout<<"-> Entering GRASP::identify_dominances_n_partitions..."<<endl;);
  ConstraintList dom_clauses;
  PartitionList sub_partitions;

  if (!_stree->current_partition()) { return FALSE; }

  // A. Find dominated clauses

  if (_mode[_GRP_DOMINATE_CLAUSES_]) {
    _clDB->identify_dominated_clauses
	(dom_clauses, _stree->assigned_variables (_stree->DLevel()));
    _stree->add_dominated_clauses (dom_clauses);
  }

  // B. Find new sub-partitions

  CHECK(if (_stree->partition_stack().first()->data() !=
	    _stree->current_partition())
	{_stree->partition_stack().first()->data()->dump();
	_stree->current_partition()->dump();
	Warn("Differing partitions during search??"); });
  DBG1(cout<<"REMOVING "; _stree->current_partition()->dump(););
  _stree->pop_partition();
  _clDB->identify_partitions (sub_partitions, _stree->current_partition());
  _stree->push_partitions (sub_partitions);

  CHECK(if (dom_clauses.size() || sub_partitions.size())
	{Warn("Invalid list management in doms+parts");}
	_stree->check_partition_invariant(4););
  DBG0(_stree->dump_whole_partition_info(););
  DBG1(cout<<"Dumping @ DOM+PART\n";_stree->top_partition(););
  return TRUE;
}



//-----------------------------------------------------------------------------
// Function: solve_bak()
//
// Purpose: Implements the top-level tasks of the search process.
//
// Side-effects: The search tree is built and the state of variables
//               constraints gets updated.
//
// Notes: Permits solutions to be iterated.
//
//-----------------------------------------------------------------------------

int GRASP::solve_bak()
{
  DBG1(cout<<"-> Entering GRASP::solve..."<<endl;);
  _time.presetTime();

  //-------------------------------------------------------------------------
  // Setup phase. Create DLevel = 0.
  //-------------------------------------------------------------------------

  DBG0(if (_mode[_GRP_VERBOSITY_]>=5) _clDB->dump (cout););
  _stree->push_decision();

  if (_deduction_eng->preprocess() == CONFLICT) {
    _bca->erase_decision (_stree->DLevel());
    return handle_outcome (UNSATISFIABLE);
  }
  clean_auxiliar_structures();

  //-------------------------------------------------------------------------
  // Search phase.
  //-------------------------------------------------------------------------

  int number_solutions = _mode[_GRP_SOLUTION_NUMBER_];

  while (_mode[_GRP_ITERATE_SOLUTIONS_] || number_solutions--) {
    CHECK(if (_mode[_GRP_VERBOSITY_]>=5) { check_consistency(); });
    DBG0(if (_mode[_GRP_VERBOSITY_]>=5)
	 {_clDB->dump(cout); _stree->dump(cout);});

    while (!_clDB->solution_found() &&
	   _decision_eng->decide() == DECISION) {
      DBG1(if (_mode[_GRP_VERBOSITY_]>=7)
	   {_clDB->dump(cout); _stree->dump(cout);});

      while (_deduction_eng->deduce() == CONFLICT) {
	if (_diagnosis_eng->diagnose() == CONFLICT) {
	  return handle_outcome (UNSATISFIABLE);
	}
	CHECK(else if (_mode[_GRP_VERBOSITY_]>=5)
	{check_consistency();});
	DBG1(if (_mode[_GRP_VERBOSITY_]>=7)
	     {_clDB->dump(cout); _stree->dump(cout);});
      }
      clean_auxiliar_structures();
      if (resources_exceeded()) {
	return handle_outcome (ABORTED);
      }
      CHECK(if (_mode[_GRP_VERBOSITY_]>=5) {check_consistency();});
      DBGn(if(_mode[_GRP_VERBOSITY_]>=9)
	   {_clDB->dump (cout); _stree->dump();});
    }
    handle_outcome (SATISFIABLE);
    if (_mode[_GRP_ITERATE_SOLUTIONS_] || number_solutions > 0) {
      issue_induced_conflict();
    }
  }
  return SATISFIABLE;
}

********** */


//-----------------------------------------------------------------------------
// Function: issue_induced_conflict()
//
// Purpose: Creates a new clause that causes the most recently computed
//          solution to be yield a conflict.
//
// Side-effects: A new clause is created, thus causing the set of solutions
//               of the resulting clDB not to be equivalent to the initial
//               set of solutions.
//
// Notes: Assumes classic CNF clauses.
//-----------------------------------------------------------------------------

void GRASP::issue_induced_conflict()
{
  ClausePtr dec_cl = _clDB->add_clause();

  register int k;
  for (k = 0; k < _clDB->variables().size(); k++) {
    VariablePtr var = _clDB->variables()[k];
    if (var->value() != UNKNOWN) {
      _clDB->add_literal (dec_cl, var, var->value());
    }
  }
  CHECK(if(!_clDB->first_unsat_clause())Warn("Unable to issue conflict??");
	else if(_mode[_GRP_VERBOSITY_]>=7)
	{cout<<"NEW SOLUTION-BASED CLAUSE:"; dec_cl->dump();});

  /* *****
  for_each(pcl,_clDB->clauses(),ClausePtr) {
    ClausePtr cl = pcl->data();

    int minDLevel = _stree->decision_number();
    for_each(plit,cl->literals(),LiteralPtr) {
      LiteralPtr lit = plit->data();
      VariablePtr var = lit->variable();
      if (var->value() != UNKNOWN &&
	  var->value() ^ lit->sign() && var->DLevel() < minDLevel) {
	minDLevel = var->DLevel();
      }
    }
    _used_decision[minDLevel] = TRUE;
  }
  ClausePtr dec_cl = _clDB->add_clause();
  register int k;
  for (k = 1; k < _used_decision.size(); k++) {
    if (_used_decision[k]) {
      VariablePtr dvar = (VariablePtr)
	_stree->assigned_variables(k).first()->data();
      _clDB->add_literal (dec_cl, dvar, dvar->value());

      _used_decision[k] = FALSE;
    }
  }
  CHECK(if(!_clDB->first_unsat_clause())Warn("Unable to issue conflict??"););
  ***** */
}


//-----------------------------------------------------------------------------
// Function: solution_found()
//
// Purpose: Checks whether a solution has been identified.
//-----------------------------------------------------------------------------

int GRASP::solution_found()
{
  DBG1(cout<<"-> Entering GRASP::solution_found..."<<endl;);
  return _clDB->sat_clause_count() == _clDB->num_clauses();
}


//-----------------------------------------------------------------------------
// Function: handle_outcome()
//
// Purpose: Handles result of the search process.
//
// Side-effects: None.
//-----------------------------------------------------------------------------

int GRASP::handle_outcome (int status)
{
  switch (status) {
  case SATISFIABLE:
    clean_temporary_structures();
    CHECK(_clDB->check_final_consistency();_clDB->check_solution(););
    if (_mode[_GRP_VERBOSITY_]) {
      printTime ("Satisfiable instance", _time.elapsedTime(), "SAT Elapsed");
    }
    if (_mode[_GRP_OUTPUT_SOLUTION_]) {
      Info("Computed solution");
      output_assignments (cout);
    }
    break;
  case UNSATISFIABLE:
    clean_temporary_structures();
    CHECK(_clDB->check_final_consistency(FALSE););
    if (_mode[_GRP_VERBOSITY_]) {
      printTime ("Unsatisfiable instance", _time.elapsedTime(), "SAT Elapsed");
    }
    break;
  case ABORTED:
    clean_temporary_structures();
    CHECK(_clDB->check_final_consistency(FALSE););
    if (_mode[_GRP_VERBOSITY_]) {
      printTime ("Aborted instance", _time.elapsedTime(), "SAT Elapsed");
    }
    break;
  default:
    Abort ("Invalid status condition in search");
    break;
  }
  if (_mode[_GRP_OUTPUT_CLDB_]) { _clDB->print_database(); }
  return status;
}


//-----------------------------------------------------------------------------
// Cleaning up *any* auxiliar structures used by the different engines, and
// whose state depends on the evolution of the search process.
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Function: clean_temporary_structures()
//
// Purpose: In general temporary structures (e.g. clauses) may be created by
//          GRASP. This function denotes a single place for cleaning those
//          structures.
//
// Side-effects: Currently, UNRESOLVED tagged clauses are deleted.
//
// Notes: Currently, only tagged clauses need be deleted at thise stage.
//        This solution allows BCP to re-use tagged clauses if appropriate.
//-----------------------------------------------------------------------------

void GRASP::clean_temporary_structures()
{
  DBG1(cout<<"-> Entering GRASP::clean_temporary_structures..."<<endl;);
  ClausePtr cl;
  while (cl = _clDB->first_del_clause()) {
    CHECK(if(_mode[_GRP_VERBOSITY_]>=5)
	  {cout<<"DELETING LARGE ";cl->dump();cout<<endl;}
	  if(cl->is_unit() || cl->is_unsatisfied() || cl->is_satisfied())
	  {Warn("Deleting non-unresolved clause??");});
    _clDB->unregister_deleted_clause (cl, TRUE);     // FORCE cl unregistration
    _clDB->del_clause (cl);
  }
  DBG1(cout<<"-> Exiting GRASP::clean_temporary_structures..."<<endl;);
}


//-----------------------------------------------------------------------------
// Printing solution information and/or stats.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Function: output_stats()
//
// Purpose: Outputs stats regarding running the SAT algorithm.
//
// Notes: Each engine and major data structure keeps their own stats.
//        This function just invokes the relevant function of each engine
//        or data structure.
//-----------------------------------------------------------------------------

void GRASP::output_stats()
{
  cout << "\n\nOutput Statistics:\n" << endl;
  _clDB->output_stats();

  _bcp->output_stats();
  _bca->output_stats();

  _deduction_eng->output_stats();
  _diagnosis_eng->output_stats();
  _decision_eng->output_stats();
  printItem();
}


//-----------------------------------------------------------------------------
// Function: output_assignments()
//
// Purpose: Outputs computed solution assignments.
//-----------------------------------------------------------------------------

void GRASP::output_assignments (ostream &outs, int assigned_only)
{
  _clDB->output_assignments (outs, assigned_only);
}


//-----------------------------------------------------------------------------
// Function: check_consistency()
//
// Purpose: Checks whether an inconsistent state of the different structures
//          has been reached.
//-----------------------------------------------------------------------------

int GRASP::check_consistency()
{
  DBG1(cout<<"-> Entering GRASP::check_consistency..."<<endl;);
  int st1 = _clDB->check_consistency();
  int st2 = _stree->check_consistency();

  return st1 && st2;
}

/*****************************************************************************/
