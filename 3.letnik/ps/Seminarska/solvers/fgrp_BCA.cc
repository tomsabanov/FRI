//-----------------------------------------------------------------------------
// File: fgrp_BCA.cc
//
// Purpose: Definition of the member functions for implementing the conflict
//          analysis procedure on clause databases.
//
// Remarks: --
//
// History: 03/27/98 - JPMS - created.
//
// Copyright (c) 1998 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#include "fgrp_BCA.hh"


//-----------------------------------------------------------------------------
// Constructors and Destructors.
//-----------------------------------------------------------------------------

BCA::BCA (Array<int> &mode, ClauseDatabase &clDB, SearchTree &stree)
:_mode (mode), _clDB (clDB), _stree (stree), _var_marks(), _var_tags() { }

BCA::~BCA() { }



//-----------------------------------------------------------------------------
// Initialization and clean up functions.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Function: setup()
//
// Purpose: Prepare BCA for a new instance of SAT.
//
// Notes: *Must* be invoked to properly initialize internal data structures.
//-----------------------------------------------------------------------------

void BCA::setup()
{
  DBG1(cout<<"-> Entering BCA::setup..."<<endl;);
  _DLevel = NONE;
  _BLevel = NONE;

  _var_marks.resize (_clDB.variables().size() + 1);       // Arrays index w/ ID
  _var_tags.resize (_clDB.variables().size() + 1);
  for (register int k = 0; k < _var_marks.size(); k++) {
    _var_marks[k] = FALSE;
    _var_tags[k] = FALSE;
  }
  CHECK(_tagged_vars = 0);
  _marked_vars = 0;

  _conf_clause = NULL;
  _back_clause = NULL;

  _tot_num_UIPs = 0;
  _max_num_UIPs = 0;

  _tot_conf_clause = 0;
  _sum_conf_clause = 0;

  _max_conf_clause = 0;
  _min_conf_clause = _clDB.variables().size() + 1;     // Start w/ invalid size
}


//-----------------------------------------------------------------------------
// Function: cleanup()
//
// Purpose: Clean data structures from previous instance of SAT.
//          Afterwards, BCA becomes ready for another instance of SAT.
//-----------------------------------------------------------------------------

void BCA::cleanup() { }



//-----------------------------------------------------------------------------
// Implementation of BCA.
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Function: analyze_conflict()
//
// Purpose: Given an unsatisfied clause, creates a new clause which prevents
//          the set of assignments that lead to the unsatisfied clause.
//
// Side-effects: A new clause is created. The last implication sequence is
//               cleared.
//
// Notes: Most recent conflict analyzer *assumes* that conflict results
//        *only* from an implication sequence conducted at a given DLevel.
//-----------------------------------------------------------------------------

int BCA::analyze_conflict (ClausePtr cl)
{
  DBG1(cout<<"-> Entering BCA::analyze_conflict..."<<endl;
       cout<<"CONF ";cl->dump();cout<<endl;);
  CHECK(if (_marked_vars != 0) {
    cout<<"MARKED VARS: "<<_marked_vars<<endl;
    Warn("Marked vars before conf analysis??");});
  // First. Clean up implied variables.

  reset_implied_variables();

  // Second. Mark initial set of variables, directly involved in unsat clause.

  register int skip_UIP = FALSE, block_UIP = FALSE;
  _DLevel = _stree.DLevel(); _BLevel = -1;  // Lowest possible backtrack DLevel
  _conf_clause = _clDB.add_clause (TypeConflict);    // Start a new conf clause
  _back_clause = check_btrack_conditions();
  record_lits_n_mark_variables (NULL, cl);
  if (!_marked_vars) {                       // Conf clause need not be created
    if(_back_clause) {
      del_conf_clause (_back_clause, TagBtrack);
      _back_clause = NULL;
    }
    del_conf_clause (_conf_clause, TagConf);
    _conf_clause = NULL;
  }
  // Third. Undo sequence of assigned variables and record literals.

  VariablePtr var;
  int uip_cnt = 0;
  while (var = _stree.pop_assigned_variable()) {
    if (marked_variable (var)) {               // Variable involved in conflict
      unmark_variable (var);                         // Unmark current variable
      if (_marked_vars == 0) {         // Check for UIP existence (ie no marks)
	if (!skip_UIP && !block_UIP &&
	    (_mode[_GRP_IDENTIFY_UIPS_] || !_stree.next_assigned_variable())) {
	  add_clause_literal (var, var->value(), _conf_clause, TagConf);
	  untag_clause_vars (_conf_clause, TagConf);
	  categorize_recorded_clause (_conf_clause);

	  _conf_clause = NULL;
	  skip_UIP = TRUE;
	  uip_cnt++;
	}
      }
      if (var->antecedent()) {
	if (!record_lits_n_mark_variables (var, var->antecedent())) {
	  block_UIP = TRUE;
	  if (_conf_clause) {
	    del_conf_clause (_conf_clause, TagConf);
	    _conf_clause = NULL;
	  }
	}
      }
      if (skip_UIP && !block_UIP && _marked_vars > 1) {
	skip_UIP = FALSE;
	_conf_clause = _clDB.add_clause (TypeConflict);
	record_lits_n_mark_variables (var, var->antecedent());
	add_clause_literal (var, 1 - var->value(), _conf_clause, TagConf);
	_clDB.sat_clause_count()++;       // This clause is currently satisfied
      }
      DBGn(cout<<"MARKED VARS: "<<_marked_vars<<endl;);
    }
    undo_literal_counters (var);
    var->reset_state();
  }
  // Fourth. Conclude conflicting clause creation.

  if (_back_clause) {
    untag_clause_vars (_back_clause, TagBtrack);
    categorize_recorded_clause (_back_clause);
  }
  _tot_num_UIPs += uip_cnt;
  if (uip_cnt > _max_num_UIPs) { _max_num_UIPs = uip_cnt; }
  CHECK(if (_conf_clause)
	{cout<<"ENTERING UNSAT ";cl->dump();cout<<endl;
	cout<<"RESULTING CONF  ";_conf_clause->dump(); cout<<endl;
	Warn("Conf clause unexpectedly created??");
	del_conf_clause (_conf_clause, TagConf); _conf_clause = NULL;}
	if (_marked_vars != 0)
	{cout<<"MARKED VARS: "<<_marked_vars<<endl;
	Warn("Marked vars after conf analysis??");}
	if (_tagged_vars != 0)
	{cout<<"TAGGED VARS: "<<_tagged_vars<<endl;
	Warn("Tagged vars after conf analysis??");});
  return _BLevel;                           // Return computed backtrack DLevel
}


//-----------------------------------------------------------------------------
// Function: erase_decision()
//
// Purpose: Erase a given decision + implication sequence without conflict
//          analysis.
//
// Notes: The current option is to let BCA control all erasing operations on
//        the decision tree.
//-----------------------------------------------------------------------------

void BCA::erase_decision()
{
  DBG1(cout<<"-> Entering BCA::erase_decision @ DLevel: ";
       cout<<_stree.DLevel()<<endl;);

  _DLevel = _stree.DLevel();
  reset_implied_variables();

  register VariablePtr var;
  while (var = _stree.pop_assigned_variable()) {
    CHECK(if(var->value()!=TRUE&&var->value()!=FALSE)
	  {Warn("Invalid assigned var!?");cout<<"\tVARIABLE: ";
	  var->dump(cout);cout<<endl;});
    DBG1(cout<<"Unsetting var:";var->dump(cout);cout<<endl;);
    undo_literal_counters (var);
    var->reset_state();
  }
  DBG1(cout<<"-> Exiting BCA::erase_decision"<<endl;);
}


//-----------------------------------------------------------------------------
// Function: reset_implied_variables()
//
// Purpose: Resets state of implied variables.
//
// Side-effects: List of implied variables in search tree is cleaned up.
//
// Notes: We must ensure the following invariant: in an unsatisfied clause
//        every variable is assigned (and none is implied).
//-----------------------------------------------------------------------------

void BCA::reset_implied_variables()
{
  DBG1(cout<<"-> Entering BCA::reset_implied_variables..."<<endl;);
  register VariablePtr var;
  while (var = _stree.pop_implied_variable()) {
    CHECK(if(var->value()!=TRUE&&var->value()!=FALSE)
	  {Warn("Invalid implied var!?");cout<<"\tVARIABLE: ";
	  var->dump(cout);cout<<endl;});
    DBG1(var->dump(cout);cout<<endl;);
    var->reset_state();
  }
  DBG1(cout<<"-> Exiting BCA::reset_implied_variables..."<<endl;);
}



//-----------------------------------------------------------------------------
// Function: undo_literal_counters()
//
// Purpose: Undo effects of a variable assignment.
//
// Side-effects: Literal counters in related clauses get updated.
//-----------------------------------------------------------------------------

void BCA::undo_literal_counters (VariablePtr var)
{
  CHECK(if(_mode[_GRP_VERBOSITY_] >= 8){
    cout<<"BCA: UNASSIGNING "<<var->name()<<" = "<<var->value()<<endl;});
  ClausePtr cl;
  if (var->value()) {                              // Variable assigned value 1
    for (register int k=0; k<var->pos_literals().size(); k++) {
      cl = var->pos_literals()[k]->clause();
      --cl->val1_pos_literals();
      if (cl->val1_lits() == 0) {
	_clDB.sat_clause_count()--;                // Decrease clause SAT count
	_clDB.update_state_registration (cl);
      }
      if (cl->test_registered (RegLarge)) {
	_clDB.register_deleted_clause (cl);
      }
    }
    for (register int k=0; k<var->neg_literals().size(); k++) {
      cl = var->neg_literals()[k]->clause();
      cl->val0_neg_literals()--;
      if (cl->test_registered (RegLarge)) {
	_clDB.register_deleted_clause (cl);
      }
      _clDB.update_state_registration (cl);
    }
  }
  else {                                           // Variable assigned value 0
    for (register int k=0; k<var->neg_literals().size(); k++) {
      cl = var->neg_literals()[k]->clause();
      --cl->val1_neg_literals();
      if (cl->val1_lits() == 0) {
	_clDB.sat_clause_count()--;                // Decrease clause SAT count
	_clDB.update_state_registration (cl);
      }
      if (cl->test_registered (RegLarge)) {
	_clDB.register_deleted_clause (cl);
      }
    }
    for (register int k=0; k<var->pos_literals().size(); k++) {
      cl = var->pos_literals()[k]->clause();
      cl->val0_pos_literals()--;
      if (cl->test_registered (RegLarge)) {
	_clDB.register_deleted_clause (cl);
      }
      _clDB.update_state_registration (cl);
    }
  }
}



//-----------------------------------------------------------------------------
// Function: record_lits_n_mark_variables()
//
// Purpose: For a given clause, antecedent of a given variable, records
//          literals in conflicting clauses and marks variables to be visited.
//
// Side-effects: Conflicting clauses get updated. Variables are tagged and/or
//               marked.
//-----------------------------------------------------------------------------

int BCA::record_lits_n_mark_variables (VariablePtr var, ClausePtr cl)
{
  int marked_DLevel_vars = FALSE;
  CHECK(
	if (var == NULL) {
	  cout << ">> Starting sequence of optional clause deps...\n";
	}
	else {
	  cout << "  >> Optional clauses...\n";
	  output_var_antecs (var);
	}
	);
  for (register int k=0; k<cl->pos_literals().size(); k++) {
    VariablePtr nvar = cl->pos_literals()[k]->variable();
    if (nvar != var) {
      CHECK(if(nvar->value()==UNKNOWN)
	    {Warn("Invalid value in traced var??");});
      if (nvar->DLevel() == _DLevel) {
	marked_DLevel_vars = TRUE;
	mark_variable (nvar);
      }
      else {
	if (_BLevel < nvar->DLevel()) { _BLevel = nvar->DLevel(); }
	add_clause_literal (nvar, nvar->value(), _conf_clause, TagConf);
	add_clause_literal (nvar, nvar->value(), _back_clause, TagBtrack);
      }
    }
  }
  for (register int k=0; k<cl->neg_literals().size(); k++) {
    VariablePtr nvar = cl->neg_literals()[k]->variable();
    if (nvar != var) {
      CHECK(if(nvar->value()==UNKNOWN)
	    {Warn("Invalid value in traced var??");});
      if (nvar->DLevel() == _DLevel) {
	marked_DLevel_vars = TRUE;
	mark_variable (nvar);
      }
      else {
	if (_BLevel < nvar->DLevel()) { _BLevel = nvar->DLevel(); }
	add_clause_literal (nvar, nvar->value(), _conf_clause, TagConf);
	add_clause_literal (nvar, nvar->value(), _back_clause, TagBtrack);
      }
    }
  }
  return marked_DLevel_vars;
}


//-----------------------------------------------------------------------------
// Function: record_lits_n_mark_variables()
//
// Purpose: For a given clause, antecedent of a given variable, records
//          literals in conflicting clauses and marks variables to be visited.
//
// Side-effects: Target clause get updated. Variables are tagged and/or marked.
//-----------------------------------------------------------------------------

int BCA::record_lits_n_mark_variables (VariableList &vlist, List<int> &ilist,
				       VariablePtr var, ClausePtr cl)
{
  DBG1(cout<<"-> Entering BCA::record_lits_n_mark_variables ..."<<endl;);
  for (register int k=0; k<cl->pos_literals().size(); k++) {
    VariablePtr nvar = cl->pos_literals()[k]->variable();
    if (nvar != var) {
      CHECK(if(nvar->value()==UNKNOWN)
	    {Warn("Invalid value in traced var??");});
      if (nvar->DLevel() == _DLevel) { mark_variable (nvar); }
      else if (!tagged_variable (nvar, TagRecord)) {
	vlist.append (nvar);
	ilist.append (nvar->value());
	tag_variable (nvar, TagRecord);
      }
    }
  }
  for (register int k=0; k<cl->neg_literals().size(); k++) {
    VariablePtr nvar = cl->neg_literals()[k]->variable();
    if (nvar != var) {
      CHECK(if(nvar->value()==UNKNOWN)
	    {Warn("Invalid value in traced var??");});
      if (nvar->DLevel() == _DLevel) { mark_variable (nvar); }
      else if (!tagged_variable (nvar, TagRecord)) {
	  vlist.append (nvar);
	  ilist.append (nvar->value());
	  tag_variable (nvar, TagRecord);
      }
    }
  }
  DBG1(cout<<"-> Exiting BCA::record_lits_n_mark_variables ..."<<endl;);
  return TRUE;
}


//-----------------------------------------------------------------------------
// Function: del_clause_literals()
//
// Purpose: Delete literals in a given clause.
//-----------------------------------------------------------------------------

void BCA::del_clause_literals (ClausePtr cl, int tag)
{
  register int k;
  while (k = cl->pos_literals().size()) {
    untag_variable (cl->pos_literals()[k-1]->variable(), tag);
    _clDB.del_literal (cl->pos_literals()[k-1]);
  }
  while (k = cl->neg_literals().size()) {
    untag_variable (cl->neg_literals()[k-1]->variable(), tag);
    _clDB.del_literal (cl->neg_literals()[k-1]);
  }
}


//-----------------------------------------------------------------------------
// Function: del_conf_clause()
//
// Purpose: Delete a conflicting clause.
//-----------------------------------------------------------------------------

void BCA::del_conf_clause (ClausePtr cl, int tag)
{
  CHECK(if(!cl) Abort("Cannot delete NO conf clause!?");
	else if (_mode[_GRP_VERBOSITY_]>=6)
	{cout<<"DELETING :";cl->dump();cout<<endl;});
  untag_clause_vars (cl, TagConf);
  del_clause_literals (cl, tag);
  _clDB.unregister_unsat_clause (cl);    // cl must be removed from all lists
  _clDB.unregister_unit_clause (cl);
  _clDB.del_clause (cl);
}


//-----------------------------------------------------------------------------
// Function: tag_list_vars()
//
// Purpose: Tags variables already specified as literals in a clause.
//-----------------------------------------------------------------------------

void BCA::tag_list_vars (VariableList &vlist, int tag)
{
  DBG1(cout<<"-> Entering BCA::tag_clause_vars..."<<endl;);
  register VariableListPtr pvar;
  for (pvar = vlist.first(); pvar; pvar = pvar->next()) {
    tag_variable (pvar->data(), tag);
  }
}


//-----------------------------------------------------------------------------
// Function: untag_list_vars()
//
// Purpose: Untags variables already specified as literals in a clause.
//-----------------------------------------------------------------------------

void BCA::untag_list_vars (VariableList &vlist, int tag)
{
  DBG1(cout<<"-> Entering BCA::tag_clause_vars..."<<endl;);
  register VariableListPtr pvar;
  for (pvar = vlist.first(); pvar; pvar = pvar->next()) {
    untag_variable (pvar->data(), tag);
  }
}


//-----------------------------------------------------------------------------
// Function: untag_clause_vars()
//
// Purpose: Untags variables already specified as literals in a clause
//-----------------------------------------------------------------------------

void BCA::untag_clause_vars (ClausePtr cl, int tag)
{
  for (register int k=0; k < cl->pos_literals().size(); k++) {
    untag_variable (cl->pos_literals()[k]->variable(), tag);
  }
  for (register int k=0; k < cl->neg_literals().size(); k++) {
    untag_variable (cl->neg_literals()[k]->variable(), tag);
  }
}


//-----------------------------------------------------------------------------
// Function: categorize_recorded_clause()
//
// Purpose: Sets up properties associated with newly recorded clause
//-----------------------------------------------------------------------------

void BCA::categorize_recorded_clause (ClausePtr cl)
{
  if (cl->size() > _mode[_GRP_CONF_CLAUSE_SIZE_]) {
    cl->set_registered (RegLarge);         // Clause declared large
  }
  if (cl->size() > _max_conf_clause) {
    _max_conf_clause = cl->size();
  }
  else if (cl->size() < _min_conf_clause) {
    _min_conf_clause = cl->size();
  }
  _tot_conf_clause++;
  _sum_conf_clause += cl->size();
  CHECK(if(_mode[_GRP_VERBOSITY_]>=5)
	{cout << "NEW RECORDED ";cl->dump(cout);cout<<endl;});
}


//-----------------------------------------------------------------------------
// Function: explain_conflict()
//
// Purpose: Simply creates a clause describing causes of a conflict.
//
// Side-effects: Creates and returns a conflict-induced clause.
//-----------------------------------------------------------------------------

ClausePtr BCA::explain_conflict (ClausePtr cl)
{
  DBG1(cout<<"-> Entering BCA::explain_conflict ..."<<endl;
       cout<<"CONF ";cl->dump();cout<<endl;);
  CHECK(if (_marked_vars != 0) {
    cout<<"MARKED VARS: "<<_marked_vars<<endl;
    Warn("Marked vars before conf analysis??");});
  // First. Clean up implied variables.

  reset_implied_variables();

  // Second. Mark initial set of variables, directly involved in unsat clause.

  ClausePtr rec_cl;
  _DLevel = _BLevel = _stree.DLevel();           // Deactivate backtrack DLevel
  _conf_clause = _clDB.add_clause (TypeConflict);    // Start a new conf clause
  _back_clause = NULL;                      // NO backtracking is to take place
  record_lits_n_mark_variables (NULL, cl);

  // Third. Undo sequence of assigned variables and record literals.

  VariablePtr var;
  while (var = _stree.pop_assigned_variable()) {
    if (marked_variable (var)) {               // Variable involved in conflict
      unmark_variable (var);                         // Unmark current variable
      if (!var->antecedent()) {
	  add_clause_literal (var, var->value(), _conf_clause, TagConf);
      }
      else {
	record_lits_n_mark_variables (var, var->antecedent());
      }
    }
    undo_literal_counters (var);
    var->reset_state();
  }
  untag_clause_vars (_conf_clause, TagConf);
  categorize_recorded_clause (_conf_clause);
  rec_cl = _conf_clause;
  _conf_clause = NULL;
  CHECK(if (_marked_vars != 0)
	{cout<<"MARKED VARS: "<<_marked_vars<<endl;
	Warn("Marked vars after conf analysis??");}
	if (_tagged_vars != 0)
	{cout<<"TAGGED VARS: "<<_tagged_vars<<endl;
	Warn("Tagged vars after conf analysis??");});
  DBG1(cout<<"-> Exiting BCA::explain_conflict ..."<<endl;);
  return rec_cl;                          // Return recorded conflicting clause
}


//-----------------------------------------------------------------------------
// Function: record_causes()
//
// Purpose: Records in a given clause, the causes of a specified variable
//          assignment.
//-----------------------------------------------------------------------------

void BCA::record_causes(VariablePtr tvar,VariableList &vlist,List<int> &ilist)
{
  DBG1(cout<<"-> Entering BCA::record_causes ..."<<endl;);
  CHECK(if (_marked_vars != 0)
	{cout<<"MARKED VARS: "<<_marked_vars<<endl;
	Warn("Marked vars before cause recording??");}
	if (_tagged_vars != 0)
	{cout<<"TAGGED VARS: "<<_tagged_vars<<endl;
	Warn("Tagged vars before cause recording??");});

  // Mark initial set of variables, directly involved in unsat clause.

  _DLevel = _stree.DLevel();                     // Deactivate backtrack DLevel
  tag_list_vars (vlist, TagRecord);
  if (tvar->antecedent()) {
    record_lits_n_mark_variables (vlist, ilist, tvar, tvar->antecedent());
  }
  // Traverse sequence of assigned variables and record literals.

  VariablePtr var;
  _stree.start_var_traversal (FALSE);        // Traverse assigned vars backward
  while (var = _stree.current_traversal_variable()) {
    if (marked_variable (var)) {               // Variable involved in conflict
      unmark_variable (var);                         // Unmark current variable
      if (var->antecedent()) {
	record_lits_n_mark_variables (vlist, ilist, var, var->antecedent());
      } else {
	//	vlist.append (var);    -jpms 4/8/98.!!!!
	//	ilist.append (var->value());
	//	tag_variable (var, TagRecord);
	CHECK(if (var->antecedent())
	      {Warn("Examining sequence cause by var w/ antecedent??");});
      }
    }
    _stree.iterate_var_traversal();
  }
  _stree.stop_var_traversal();
  untag_list_vars (vlist, TagRecord);               // Cleanup tags in var list
  CHECK(if (_marked_vars != 0)
	{cout<<"MARKED VARS: "<<_marked_vars<<endl;
	Warn("Marked vars after cause recording??");}
	if (_tagged_vars != 0)
	{cout<<"TAGGED VARS: "<<_tagged_vars<<endl;
	Warn("Tagged vars after cause recording??");});
  DBG1(cout<<"-> Exiting BCA::record_causes ..."<<endl;);
}


//-----------------------------------------------------------------------------
// Function: construct_conf_clause()
//
// Purpose: Creates a new conlicting clause, given a list of variables and
//          associated literal signs. Duplicated literals are removed.
//
// Side-effects: Input lists get erased.
//-----------------------------------------------------------------------------

ClausePtr BCA::construct_conf_clause (VariableList &vlist,List<int> &ilist)
{
  ClausePtr cl = _clDB.add_clause (TypeConflict);   // Create a new conf clause

  VariableListPtr pvar;
  ListItem<int> *pint;
  while (pvar = vlist.first()) {  // Add literals to clause
    VariablePtr var = pvar->data();
    pint = ilist.first();
    if (!tagged_variable (var, TagRecord)) {
      _clDB.add_literal (cl, pvar->data(), pint->data());
      tag_variable (var, TagRecord);
    }
    vlist.remove (pvar);
    ilist.remove (pint);
  }
  CHECK(if (_mode[_GRP_VERBOSITY_] >= 3)
	{cout << "NEW RECORDED ";cl->dump(cout);cout<<endl;});
  untag_clause_vars (cl, TagRecord);
  return cl;
}


//-----------------------------------------------------------------------------
// Auxiliar functions.
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Function: output_var_antecs()
//
// Purpose: Prints out which clauses can be antecedents of var assignment.
//
// Side-effects: None.
//
// Notes: --
//-----------------------------------------------------------------------------

void BCA::output_var_antecs (VariablePtr var)
{
  int count_cls = 0;

  if (var->value()) { // Value of assignment is true -> analyze pos literals.
    cout << "    "; var->antecedent()->dump(); cout << endl; cout << endl;
    for (register int k=0; k<var->pos_literals().size(); k++) {
      ClausePtr cl = var->pos_literals()[k]->clause();
      // Find relevant clauses. Print them.
      if ((cl != var->antecedent()) && (cl->val1_lits() == 1) &&
	  (cl->val0_lits() == cl->num_literals()-1)) {
	cout << "    "; cl->dump(); cout << endl;
	count_cls++;
      }
    }
  }
  else {
    cout << "    "; var->antecedent()->dump(); cout << endl; cout << endl;
    for (register int k=0; k<var->neg_literals().size(); k++) {
      ClausePtr cl = var->neg_literals()[k]->clause();
      // Find relevant clauses. Print them.
      if ((cl != var->antecedent()) && (cl->val1_lits() == 1) &&
	  (cl->val0_lits() == cl->num_literals()-1)) {
	cout << "    "; cl->dump(); cout << endl;
	count_cls++;
      }
    }
  }
  cout << "\n  Identified " << count_cls << " alternative clauses\n\n" << endl;
}


//-----------------------------------------------------------------------------
// Accessing stats.
//-----------------------------------------------------------------------------

void BCA::output_stats()
{
    printItem ("Total number of UIPs", _tot_num_UIPs);
    printItem ("Maximum number of UIPs", _max_num_UIPs);
    printItem();
    printItem ("Total number of conflicting clauses", _tot_conf_clause);

    if (_max_conf_clause <= _clDB.variables().size()) {
      printItem ("Largest conflicting clause", _max_conf_clause);
    }
    else {
      printItem ("Largest conflicting clause", NONE);
    }
    printItem ("Smallest conflicting clause", _min_conf_clause);
    if (_tot_conf_clause) {
	double avg_size = 1.0 * ( _sum_conf_clause / _tot_conf_clause);
	printItem ("Conflicting clause average size", avg_size);
    }
    printItem();
}

/*****************************************************************************/

