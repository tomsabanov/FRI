//-----------------------------------------------------------------------------
// File: id_list.cc
//
// Purpose: Implements the interface of a list, using (integer) IDs for
//          manipulating entries. IDs must be in the range 1..maxID
//
// Remarks: --
//
// History: 03/22/99 - JPMS - created.
//
// Copyright (c) 1999 Joao P. Marques Silva.
//-----------------------------------------------------------------------------


#include "id_list.hh"


ListID::ListID (int max_size) : _mark(0), _list(0)
{
  register int i;

  _mark.resize (max_size);
  _list.resize (max_size);

  for (i=0; i<max_size; i++) {
    _mark[i] = EmptyMark;
    _list[i] = EmptyMark;
  }
  _first = 1;    // First position.
  _last = 1;
  _size = 0;
  _max_size = max_size;
}

ListID::~ListID()
{
  _mark.resize(0);
  _list.resize(0);
}

/*****************************************************************************/

