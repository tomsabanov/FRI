//-----------------------------------------------------------------------------
// File: fgrp_STS.hh
//
// Purpose: Declaration of the search tree structures.
//
// Remarks: I decide to use the array of variable assignments with twice the
//          number of variables+1. This allows using empty positions to
//          denote the beginning of implication sequences.
//
// History: 03/28/98 - JPMS - created.
//
// Copyright (c) 1998 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#ifndef __FGRP_STS__
#define __FGRP_STS__

#include "fgrp_Mode.hh"
#include "fgrp_Partition.hh"
#include "fgrp_CDB.hh"


//-----------------------------------------------------------------------------
// Class: SearchTree
//
// Purpose: Maintains the data structures associated with the search tree.
//-----------------------------------------------------------------------------

class SearchTree {
public:

  //---------------------------------------------------------------------------
  // Constructor/destructor.
  //---------------------------------------------------------------------------

  SearchTree (Array<int> &mode);
  virtual ~SearchTree();

  //---------------------------------------------------------------------------
  // Interface contract.
  //
  // A- Initialization/clean up of the search process.
  //-------------------------------------------------------------------------

  virtual void setup (int max_size);
  virtual void cleanup();

  virtual int check_consistency();

  virtual void dump (ostream &outs=cout);

  //-------------------------------------------------------------------------
  // B- Decision levels.
  //    Empty positions in array of assigned vars denote the beginning of
  //    implication sequences.
  //---------------------------------------------------------------------------

  inline int DLevel() { return _DLevel; }
  inline int push_decision() {
    _DLevel++;
    _var_idx++;                                      // Leave an empty position
    _DLevel_mapping[_DLevel] = _var_idx;
    _toggled_var[_DLevel] = FALSE;
    return _DLevel;
  }
  inline int pop_decision() {
    _DLevel_mapping[_DLevel] = NONE;
    _toggled_var[_DLevel] = FALSE;
    _var_idx--;                                          // Skip empty position
    _DLevel--;
    return _DLevel;
  }
  
  //---------------------------------------------------------------------------
  // Variable assignments and implications.
  //---------------------------------------------------------------------------

  inline void push_assigned_variable (VariablePtr var) {
    CHECK(if (var->DLevel() != _DLevel){
      cout<<"DLevel "<<_DLevel<<"  **AND**  v->DLevel: "<<var->DLevel()<<endl;
      Warn("Invalid DLevel manipulation in STS!?"); }
	  if(_mode[_GRP_VERBOSITY_] >= 7)
	  {cout<<"PUSH ASSIGN VAR "<<var->name()<<" = "<<var->value()<<endl;});
    _assigned_variables[++_var_idx] = var;
  }
  inline VariablePtr next_assigned_variable() {
    CHECK(if (_var_idx < 0){Abort("Invalid variable access index??");}
	  VariablePtr xvar = _assigned_variables[_var_idx];
	  if (xvar && xvar->DLevel() != _DLevel)
	  {cout<<"\nVAR: "<<xvar->name()<<" @ "<<xvar->DLevel()<<endl;
	  Warn("Invalid next assigned variable!?");});
    return _assigned_variables[_var_idx];
  }
  inline VariablePtr pop_assigned_variable() {
    CHECK(if (_var_idx < 0){Abort("Invalid variable access index??");}
	  VariablePtr xvar = _assigned_variables[_var_idx];
	  if (xvar && xvar->DLevel() != _DLevel)
	  {cout<<"\nVAR: "<<xvar->name()<<" @ "<<xvar->DLevel()<<endl;
	  Warn("Invalid popped assigned variable!?");}
	  if(xvar &&_mode[_GRP_VERBOSITY_] >= 7){
	    cout<<"POP ASSIGNED VAR "<<xvar->name();
	    cout<<" = "<<xvar->value()<<endl;});
    VariablePtr var = _assigned_variables[_var_idx];
    if (var) { _assigned_variables[_var_idx--] = NULL; }
    return var;
  }

  inline void push_implied_variable (VariablePtr var) {
    CHECK(if(_mode[_GRP_VERBOSITY_] >= 7){
      cout<<"PUSH IMPLIED VAR "<<var->name()<<" = "<<var->value()<<endl;});
    _implied_variables.append (var);
  }
  inline VariablePtr pop_implied_variable() {
    VariablePtr var = NULL;
    if (_implied_variables.first()) {
      var = _implied_variables.first()->data();
      _implied_variables.remove (_implied_variables.first());
    }
    CHECK(if(var && _mode[_GRP_VERBOSITY_] >= 7){
      cout<<"POP IMPLIED VAR "<<var->name()<<" = "<<var->value()<<endl;});
    return var;
  }

  inline int num_assigned_vars() {
    return _var_idx - _DLevel_mapping[_DLevel] + 1;
  }

  inline VariablePtr trigger_var_assignment() {
    VariablePtr var = NULL;
    int base_idx = _DLevel_mapping[_DLevel]+1; // Location of 1st assigned var
    if (base_idx >= 0) {
      var = _assigned_variables[base_idx];
      CHECK(if (_mode[_GRP_VERBOSITY_] >= 7 && !var)
	    {cout<<"DLevel: "<<_DLevel<<endl; Info("NO Triggering var??");}
	    else if (_mode[_GRP_VERBOSITY_] >= 7)
	    {cout<<"TRIGGER VARIABLE: "<<var->name()<<endl;});
    }
    return var;
  }
  inline void start_var_traversal (int tforward = TRUE) {
    if (tforward) {
      _vtraverse_idx[_DLevel] = _DLevel_mapping[_DLevel]+1;
      _forward_flag[_DLevel] = TRUE;
      CHECK(if (_assigned_variables[_vtraverse_idx[_DLevel]-1] != NULL)
	    {Warn("Invalid var idx management during var traversal!?");});
    } else {
      _vtraverse_idx[_DLevel] = _var_idx;
      _forward_flag[_DLevel] = FALSE;
    }
  }
  inline void iterate_var_traversal() {
    if (_forward_flag[_DLevel]) { _vtraverse_idx[_DLevel]++; }
    else                        { _vtraverse_idx[_DLevel]--; }
  }
  inline int active_var_traversal() { return _vtraverse_idx[_DLevel] != NONE; }
  inline void stop_var_traversal() { _vtraverse_idx[_DLevel] = NONE; }

  inline VariablePtr current_traversal_variable() {
    return _assigned_variables[_vtraverse_idx[_DLevel]];
  }

  inline int is_var_toggled (int _level) { return _toggled_var[_level]; }
  inline void set_var_toggled (int _level) { _toggled_var[_level] = TRUE; }
  inline void reset_var_toggled (int _level) { _toggled_var[_level] = FALSE; }

  //---------------------------------------------------------------------------
  // Search information.
  //---------------------------------------------------------------------------

  inline int num_decisions() { return _assigned_variables.size(); }
  
  inline int num_assigned_variables() {
    int ref_diff = _var_idx - _DLevel;
    return ((_assigned_variables[_var_idx] == NULL) ? ref_diff+1 : ref_diff);
  }

protected:

  Array<int> &_mode;                         // Configuration options for GRASP

  int _DLevel;                          // Current decision level of the search

  DArray<int> _DLevel_mapping;            // Maps DLevels into var stack levels
  DArray<int> _toggled_var;                   // Indicates toggled decision var
  
  int _var_idx;                        // Insertion index in assigned var array
  DVariableArray _assigned_variables;            // Array of assigned variables

  VariableList _implied_variables;      // List of implied variable assignments

  DArray<int> _vtraverse_idx;             // Indeces for traversing DLevel vars
  DArray<int> _forward_flag;  // Indicates whether traversal is for or backward
  
private:

};

#endif // __FGRP_STS__

/*****************************************************************************/

