//-----------------------------------------------------------------------------
// File: fgrp_DiagnosisEng.cc
//
// Purpose: Definition of the diagnosis engine in GRASP.
//
// Remarks: --
//
// History: 03/30/98 - JPMS - created.
//
// Copyright (c) 1998 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#include "fgrp_DiagnosisEng.hh"


//-----------------------------------------------------------------------------
// Constructors/Destructors and initialization functions for the different
// classes.
//-----------------------------------------------------------------------------

DiagnosisEngine::DiagnosisEngine (Array<int> &mode,
				  ClauseDatabase &clDB,
				  SearchTree &stree,
				  BCP &bcp, BCA &bca)
  : _mode(mode), _clDB (clDB), _stree (stree), _bcp (bcp), _bca (bca)
{
  _tot_backtrack_number = 0;
  _nc_backtrack_number = 0;
  _max_back_jump = 0;
  _conflicts_diagnosed = 0;
}

DiagnosisEngine::~DiagnosisEngine() { }



//-----------------------------------------------------------------------------
// Initialization and cleanup.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Function: initialize()
//
// Purpose: If required, performs internal initializations.
//-----------------------------------------------------------------------------

void DiagnosisEngine::setup() { }


//-----------------------------------------------------------------------------
// Function: cleanup()
//
// Purpose: Key feature is to backtrack to decision level 0.
//          If required, performs internal clean up.
//-----------------------------------------------------------------------------

void DiagnosisEngine::cleanup()
{
  DBG1(cout<<"-> Entering DiagnosisEngine::cleanup..."<<endl;);

  //---------------------------------------------------------------------------
  // First, force search process to backtrack to decision level (-1), ie NO
  // decisions in the decision stack and no assignments.
  //---------------------------------------------------------------------------

  if (_stree.DLevel() >= 0) {    // Top decision level *must* also be cleared
    //    _bca.erase_decision();    -jpms 4/1/98.
    backtrack (-1);                               // Clean up decision tree
  }

  //---------------------------------------------------------------------------
  // Clean up stats info.
  //---------------------------------------------------------------------------

  _tot_backtrack_number = 0;
  _nc_backtrack_number = 0;
  _max_back_jump = 0;
  _conflicts_diagnosed = 0;
}



//-----------------------------------------------------------------------------
// Implementation of the diagnosis engine for conflicts on CNF formulas.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Function: diagnose()
//
// Purpose: Diagnosis conflicts. After leaving this function, either non
//          inconsistent state was reached or the instance is proved to be
//          unsatisfiable.
//
// Side-effects: Both the constraint network and the decision tree are
//               updated until a non inconsistent search state is entered.
//
// Notes: Multiple conflicts are automatically handled, since unsat clauses
//        will remain in the list of unsat clauses until a non inconsistent
//        search state is reached. Hence, it suffices to just diagnose *one*
//        unsatisfied clause.
//-----------------------------------------------------------------------------

int DiagnosisEngine::diagnose()
{
  DBG1(cout<<"-> Entering DiagnosisEngine::diagnose..."<<endl;);
  CHECK(if (_stree.DLevel() < 0){Warn("NEGATIVE backtracking DLevel??");}
	//_stree.check_partition_invariant(2);
	);
  // Repeat while unsat clauses.

  int BLevel;
  ClausePtr cl;

  if (_mode[_GRP_BACKTRACKING_STRATEGY_] == _CHRONOLOGICAL_B_) {
    _conflicts_diagnosed++;
    BLevel = chronological_backtrack();
  }
  else {
    while (cl = _clDB.first_unsat_clause()) {
      DBG0(cout<<"UNSAT Clause: ";cl->dump(cout);cout<<endl;);
      _clDB.unregister_unsat_clause (cl);
      _conflicts_diagnosed++;

      // Tag & record literals of unsat clause.
      // Trace back, reset assignments and tag/untag and record literals.

      BLevel = _bca.analyze_conflict (cl);

      _clDB.update_state_registration (cl);        // Clause may still be UNSAT

      CHECK(if(_mode[_GRP_VERBOSITY_]>=5)
	    {cout<<"COMPUTED BACKTRACK DLEVEL: "<<BLevel<<endl;});

      backtrack (BLevel);           // Backtrack until computed backtrack level

      if (BLevel <= 0) { break; }
    }
  }
  CHECK(if(!_clDB.first_unit_clause())
	{Warn("NO Unit Clauses after backtrack??");});
  return ((BLevel > 0) ? NO_CONFLICT : CONFLICT);
}


//-----------------------------------------------------------------------------
// Function: backtrack()
//
// Purpose: Backtracks search process to a new decision level. In the process
//          it is guarateed that a solution cannot be found in the skipped
//          decision levels.
//
// Side-effects: Decision tree gets updated to new decision level.
//
// Note: Starts by popping decision since identifying the need to backtrack
//       immediately implies that the current DLevel has been erased.
//       The backtrack DLevel *must not* be erased. This solution implies
//       that at the backtracking DLevel another conflict is issued, which
//       will induced a failure-driven assertion, or lead to further
//       backtracking.
//-----------------------------------------------------------------------------

void DiagnosisEngine::backtrack (int BLevel)
{
  DBG1(cout<<"-> Entering DiagnosisEngine::backtrack ..."<<endl;);

  if (BLevel != _stree.DLevel()) {
    if (_mode[_GRP_VERBOSITY_]) {
      cout << "CURRENT DLEVEL:       " << _stree.DLevel() << endl;
      cout << "BACKTRACK TO DLEVEL:  " << BLevel;
      if (_stree.DLevel() - BLevel > 1) { cout << "    *"; }
      cout << endl;
    }
    register int jump = _stree.DLevel() - BLevel;
    if (jump > 1) { _nc_backtrack_number++; }
    if (jump > _max_back_jump) { _max_back_jump = jump; }
    _tot_backtrack_number++;

    while (_stree.DLevel() >= 0 && (BLevel < _stree.DLevel() || BLevel == 0)) {
      /*
	_clDB.erase_dominated_clauses (_stree.dominated_clauses());   // jpms
	_stree.recover_partitions();                          // -jpms 4/9/97
	_stree.unset_current_partition();
	*/  // -jpms 3/30/98.

      _bca.erase_decision();
      _stree.pop_decision();
    }
    if (_stree.DLevel() >= 0) {
      /*
	_clDB.erase_dominated_clauses (_stree.dominated_clauses()); // jpms
	_stree.recover_partitions();                        // -jpms 4/9/97
	_stree.unset_current_partition();
	*/  // -jpms 3/30/98.
      //      _stree.pop_decision();           // Pop DLevel causing backtracking
      // _bca.erase_decision();               // Undo DLevel causing backtracking
      CHECK(if (!_clDB.number_unsat_clauses() && BLevel)
	    {Warn("Leaving NO UNSAT clauses after backtrack??");});
    }
  }
  else {
    if (_mode[_GRP_VERBOSITY_]) {
      cout << "CONFLICT @ DLEVEL:    " << _stree.DLevel() << endl;
    }
    /*
      _clDB.erase_dominated_clauses (_stree.dominated_clauses());     // jpms
      _stree.recover_partitions();                            // -jpms 4/9/97
      _stree.unset_current_partition();
      */  // -jpms 3/30/98.

    //    _stree.pop_decision();  -jpms 4/2/98.          // No need to erase @ DLevel = BLevel
  }
  /*
    if (_stree.DLevel() >= 0) {
    _clDB.erase_dominated_clauses (_stree.dominated_clauses());       // jpms
    _stree.recover_partitions();                              // -jpms 4/9/97
    }
    */ // -jpms 3/30/98.
  DBG1(cout<<"-> Exiting DiagnosisEngine::backtrack ..."<<endl;);
}


//-----------------------------------------------------------------------------
// Function: chronological_backtrack()
//
// Purpose: Backtracks chronologically.
//
// Side-effects: --
//
// Notes: Uses the toggled_var array in _stree.
//-----------------------------------------------------------------------------

int DiagnosisEngine::chronological_backtrack()
{
  VariablePtr tvar;
  int tvalue;
  int toggled;
  int BLevel = _stree.DLevel();
  int CLevel = BLevel;
  do {
    toggled = _stree.is_var_toggled(BLevel);
    tvar = _stree.trigger_var_assignment();
    tvalue = tvar->value();
    _bca.erase_decision();
    if (toggled) {
      _stree.pop_decision();
      BLevel--;
    }
  }
  while (BLevel > 0 && toggled);
  if (BLevel > 0) {
    _stree.set_var_toggled (BLevel);
    tvar->set_state (1-tvalue, BLevel, NULL);
    _stree.push_implied_variable (tvar);
  }
  if (CLevel != BLevel) {
    _tot_backtrack_number++;
    if (_mode[_GRP_VERBOSITY_]) {
      cout << "CURRENT DLEVEL:       " << CLevel << endl;
      cout << "BACKTRACK TO DLEVEL:  " << BLevel;
      cout << endl;
    }
  }
  else {
    if (_mode[_GRP_VERBOSITY_]) {
      cout << "CONFLICT @ DLEVEL:    " << _stree.DLevel() << endl;
    }
  }
  return BLevel;
}


//-----------------------------------------------------------------------------
// Accessing stats.
//-----------------------------------------------------------------------------

void DiagnosisEngine::output_stats()
{
    printItem ("Total number of backtracks", _tot_backtrack_number);
    printItem ("Number of non-chronological backtracks", _nc_backtrack_number);
    printItem ("Highest non-chronological jump", _max_back_jump);
    printItem ("Number of conflicts diagnosed", _conflicts_diagnosed);
    printItem();
}

/*****************************************************************************/

