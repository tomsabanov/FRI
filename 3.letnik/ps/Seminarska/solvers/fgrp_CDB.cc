//-----------------------------------------------------------------------------
// File: fgrp_CDB.cc
//
// Purpose: Member functions for the elements of the clause database, i.e.
//          variables, clauses, literals and the clause database itself.
//
// Remarks: --.
//
// History: 03/26/98 - JPMS - created.
//
// Copyright (c) 1998 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <iostream.h>
#include <fstream.h>

#include "fgrp_CDB.hh"


static char *grasp_output_filename = "grasp.output";


//-----------------------------------------------------------------------------
// Expected variable and clause literal array sizes.
//-----------------------------------------------------------------------------

#define GRP_EXPECT_V_LIT_SIZE    4           // Expected var literal array size
#define GRP_EXPECT_C_LIT_SIZE    2            // Expected cl literal array size


//-----------------------------------------------------------------------------
// Constructors and Destructors.
//-----------------------------------------------------------------------------

Literal::Literal (ClausePtr Ncl, VariablePtr Nvar, int Nsign)
{
  _clause = Ncl; _variable = Nvar; _sign = Nsign;

  _vidx = _cidx = NONE;
}

Literal::~Literal() { }


Variable::Variable (int nID, char *Nname)
  : _ID(nID), _name(Nname),
    _pos_literals(D_GROW_ONLY, GRP_EXPECT_V_LIT_SIZE),
    _neg_literals(D_GROW_ONLY, GRP_EXPECT_V_LIT_SIZE)
{
  _traverse_ref = new ListItem<Variable*> (this);
  _imply_ref = new ListItem<Variable*> (this);
  _partition_ref = new ListItem<Variable*> (this);

  reset_state();
}

Variable::~Variable ()
{
  CHECK(if (_pos_literals.size() || _neg_literals.size())
	{Warn("Deleting variable with literals?");});
  _pos_literals.resize (0);
  _neg_literals.resize (0);
  delete _traverse_ref;
  delete _imply_ref;
  delete _partition_ref;
}


Clause::Clause (int nID, int cl_type)
  : _ID(nID), _clause_type(cl_type),
    _pos_literals(D_GROW_ONLY, GRP_EXPECT_C_LIT_SIZE),
    _neg_literals(D_GROW_ONLY, GRP_EXPECT_C_LIT_SIZE)
{
  _num_lits = 0;
  _val0_pos_lits = 0;
  _val1_pos_lits = 0;
  _val0_neg_lits = 0;
  _val1_neg_lits = 0;

  _unit_ref = new ListItem<Clause*> (this);
  _unsat_ref = new ListItem<Clause*> (this);
  _del_ref = new ListItem<Clause*> (this);

  _registered_flags = RegEmpty;
}

Clause::~Clause()
{
  CHECK(if (_pos_literals.size() || _neg_literals.size())
	{Warn("Deleting clause with literals?");});
  _pos_literals.resize (0);
  _neg_literals.resize (0);
  delete _unit_ref;
  delete _unsat_ref;
  delete _del_ref;
}


LrgClause::LrgClause (int nID, int max_num_deps) :
  Clause (nID, TypeConflict), _dependencies (max_num_deps)
{
  _state = UNRESOLVED;
  _trgt_var = NULL;
}

LrgClause::~LrgClause() { }


ClauseDatabase::ClauseDatabase (ModeOptions &mode)
  : _mode (mode.mode()),
    _variables(D_GROW_ONLY, GRP_DEF_ARRAY_SIZE),
    _clauses(D_GROW_ONLY, GRP_DEF_ARRAY_SIZE),
    _avail_var_IDs(), _avail_cl_IDs(),
    _unit_clauses(), _unsat_clauses(), _deleted_clauses()
{
  _clIDs = _varIDs = 0;

  _sat_clause_count = 0;
  
  _initial_var_number = 0;
  _initial_cl_number = 0;
  _initial_lit_number = 0;
  _lit_number = 0;

  /* ****
  _tot_subsumed_cls = 0;
  _tot_del_clauses = 0;
  _tot_dominated_cls = 0;
  _tot_partitions = 0;
  **** */
}

ClauseDatabase::~ClauseDatabase()
{
  _variables.resize(0);
  _clauses.resize(0);

  _avail_var_IDs.erase();
  _avail_cl_IDs.erase();

  _unit_clauses.erase();
  _unsat_clauses.erase();
  _deleted_clauses.erase();
}



//-----------------------------------------------------------------------------
// Member functions of the clause database.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Function: setup()
//
// Purpose: Sets up the search after a CNF formula has been defined. This
//          function is required since it guarantees allocation of the data
//          structures to be used.
//
// Side-effects: Data structures are set up.
//
// Notes: varIDs and clIDs are not supposed to be reset in setup. These
//        variables are only initialized at object constructio time.
//-----------------------------------------------------------------------------

void ClauseDatabase::setup()
{
  DBG1(cout<<"-> Entering ClauseDatabase::setup()."<<endl;);
  CHECK(if (!_lit_number) {Warn("NO Literals in DATABASE??");});

  _sat_clause_count = 0;
  
  _initial_var_number = _variables.size();
  _initial_cl_number = _clauses.size();
  _initial_lit_number = _lit_number;

  /* ****
  _tot_subsumed_cls = 0;
  _tot_del_clauses = 0;
  _tot_dominated_cls = 0;
  _tot_partitions = 0;
  ***** */
  DBG1(cout<<"-> Exiting ClauseDatabase::setup()."<<endl;);
}


//-----------------------------------------------------------------------------
// Function: cleanup()
//
// Purpose: Clean up internal structures of DB.
//
// Notes: Lots of information get lost. Thus this is only used after DB
//        information is properly accounted for.
//-----------------------------------------------------------------------------

void ClauseDatabase::cleanup() { }


//-----------------------------------------------------------------------------
// Functions for interfacing BCP and BCA -- i.e. consistency maintenance.
//-----------------------------------------------------------------------------

/* **********

//-----------------------------------------------------------------------------
// Function: identify_trigger_var_assignments()
//
// Purpose: Examines unit clauses and decides which variables must be assigned.
//
// Side-effects: The state of some variables is updated and added to a list of
//               implied variables.
//
// Notes: Clauses are traversed for finding a proper DLevel; hence it must
//        be initialized to 0.
//-----------------------------------------------------------------------------

void ClauseDatabase::identify_trigger_var_assignments
(VariableList &implied_vars)
{
  DBG1(cout<<"-> Entering clDB::identify_trigger_var_assigns..."<<endl;);
  CHECK(if(_unsatClauses.size())
	{Warn("Unsat clauses while triggering var assignments??");
	dump_clause_list ((ClauseList&)_unsatClauses);});
  register ClausePtr cl;

  for_each(pcl,_unitClauses,ConstraintPtr) {
    cl = (ClausePtr) pcl->data();
    DBG1(cout<<"Unit ";cl->dump(cout);cout<<endl;);

    register VariablePtr tvar = NULL;
    register int tvalue = NONE, tlevel = 0;

    for_each(plit,cl->literals(),LiteralPtr) {
      register LiteralPtr lit = plit->data();
      register VariablePtr ivar = lit->variable();

      if (ivar->value() == UNKNOWN) {
	tvar = ivar;
	tvalue = 1 - lit->sign();
      }
      CHECK(else if (ivar->value() ^ lit->sign()) {tvar = NULL; break;})
	else if (ivar->DLevel() > tlevel) {
	  tlevel = ivar->DLevel();
	}
    }
    if (tvar != NULL) {
      tvar->set_state (tvalue, tlevel, (ConstraintPtr)cl);
      implied_vars.append (tvar->search_ref());
    }
  }
}


//-----------------------------------------------------------------------------
// Function: propagate_var_constraints()
//
// Purpose: Propagates constraints, i.e. finds necessary assignments for
//          clauses in database not to be unsatisfied.
//
// Side-effects: The state of some variables is updated and added to a list of
//               implied variables.
//
// Notes: It is *required* that the clauses' states are stored in dedicated
//        (int) variables. This guarantees that the proper state evolution
//        is recorded.
//        While trying to imply assignments, we could predict a soon-to-be
//        unsatisfied clause. However, we might return a conflict indication
//        without having unsat clauses in the corresponding list.
//-----------------------------------------------------------------------------

int ClauseDatabase::propagate_var_constraints
(VariablePtr var, VariableList &implied_vars)
{
  DBG1(cout<<"-> Entering ClauseDatabase::prop_var_constraints..."<<endl;);
  CHECK(if(var->value()==UNKNOWN||var->DLevel()<0)
	{Warn("Invalid implied variable!?");
	cout<<"Problem w/ ";var->dump(cout);cout<<endl;});
  int consistent = TRUE;

  for_each(plitA,((VariablePtr)var)->literals(),LiteralPtr) {
    LiteralPtr litA = plitA->data();
    ClausePtr cl = (ClausePtr) litA->clause();

    if (cl->state() == DOMINATED) { continue; }

    int c_state = cl->state();
    int n_state = cl->set_state (litA);
    update_state (cl, c_state, n_state);

    register VariablePtr tvar = NULL;
    register int tvalue = NONE, tlevel = 0;

    if (n_state == UNIT) {
      for_each(plitB,cl->literals(),LiteralPtr) {
	register LiteralPtr litB = plitB->data();
	register VariablePtr ivar = litB->variable();

	if (ivar->value() == UNKNOWN) {
	  tvar = ivar;
	  tvalue = 1 - litB->sign();
	}
	else if (ivar->value() ^ litB->sign()) {
	  tvar = NULL;
	  break;
	}
	else if (ivar->DLevel() > tlevel) {
	  tlevel = ivar->DLevel();
	}
      }
    }
    else if (n_state == UNSATISFIED) { consistent = FALSE; }

    if (tvar != NULL) {
      tvar->set_state (tvalue, tlevel, (ConstraintPtr)cl);
      implied_vars.append (tvar->search_ref());
    }
  }
  return consistent;
}


//-----------------------------------------------------------------------------
// Function: unpropagate_var_constraints()
//
// Purpose: Undoes any propagation of constraints that resulted from a given
//          variable.
//
// Side-effects: The state of some variables is reset.
//
// Notes: It is *required* that the clauses' states are stored in dedicated
//        (int) variables. This guarantees that the proper state evolution
//        is recorded.
//-----------------------------------------------------------------------------

void ClauseDatabase::unpropagate_var_constraints (VariablePtr var)
{
  register int c_state, n_state;
  register ClausePtr cl;

  for_each(plit,((VariablePtr)var)->literals(),LiteralPtr) {
    register LiteralPtr lit = plit->data();
    cl = lit->clause();

    if (cl->state() == DOMINATED) { continue; }

    c_state = cl->state();
    n_state = cl->unset_state (lit);
    update_state (cl, c_state, n_state);

    VariablePtr ivar = cl->implied_var();
    if (c_state == SATISFIED && ivar && ivar != var) {
      set_unassigned_variable (ivar);      // Variable becomes unassigned
    }
  }
}


//-----------------------------------------------------------------------------
// Functions for subsumption operations.
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Function: subsume_clauses()
//
// Purpose: Identifies subsumed (covered) clauses in the clause database.
//
// Side-effects:
//
// Notes: YET TO BE IMPLEMENTED.
//-----------------------------------------------------------------------------

int ClauseDatabase::subsume_clauses()
{

  Abort ("Entering subsume_clauses; a function not yet implemented.");
  return NONE;
}


//-----------------------------------------------------------------------------
// Function: subsume()
//
// Purpose: Given a clause it identifies other clauses subsumed (i.e. covered)
//          by the clause and deletes them. Current solution is to immediately
//          delete subsumed clauses. This could be a problem if subsumed clause
//          is either UNIT or UNSATISFIED.
//
// Side-effects: Some clauses are deleted.
//
// Notes: Currently, it picks the first variable and uses it as the generator
//        of potential clauses to be deleted. Maybe this can be improved.
//        Returns number of subsumed clauses as the number of deleted clauses.
//-----------------------------------------------------------------------------

int ClauseDatabase::subsume (ClausePtr cl)
{
  DBG1(cout<<"-> Entering ClauseDatabase::subsume..."<<endl;);
  if (cl->literals().size() == 0) {
    DBG0(Info("Clause covering entire DB was found. Continuing..."););
    return 0;
  }
  int subsumed_clauses = 0;
  LiteralPtr rlit = cl->literals().first()->data();
  VariablePtr rvar = rlit->variable();

  for_each(plit,rvar->literals(),LiteralPtr) {
    LiteralPtr lit = plit->data();
    ClausePtr vcl = lit->clause();

    if (rlit->sign() != lit->sign() || vcl == cl ||
	cl->literals().size() > vcl->literals().size()) { continue; }

    int match, stop = FALSE;
    for_each(plitA,cl->literals(),LiteralPtr) {
      LiteralPtr litA = plitA->data();
      match = FALSE;
      for_each(plitB,vcl->literals(),LiteralPtr) {
	LiteralPtr litB = plitB->data();
	if (litA->variable() == litB->variable()) {
	  if (litA->sign() == litB->sign()) { match = TRUE; }
	  else                              { stop = TRUE; }
	  break;
	}
      }
      if (!match || stop) { break; }
    }
    if (match) {                  // Subsumed clause found -> can delete it
      tag_clause (vcl, RED);         // Clause will eventually be deleted
      subsumed_clauses++;
      DBG0(cout << "    Subsumed "; vcl->dump(cout);
      cout << "addr: " << vcl << "\n";);
    }
  }
  DBG0(cout << "\nSubsuming "; cl->dump(cout);
  cout << "addr: " << cl << "\n";
  cout << "  Subsumed " << subsumed_clauses << " clauses\n";);
  _tot_subsumed_cls += subsumed_clauses;
  return subsumed_clauses;
}


//-----------------------------------------------------------------------------
// Member functions for consensus operations.
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Function: consensus()
//
// Purpose: Performs the consensus of two clauses, each having a literal in
//          a given variable var.
//
// Side-effects: A new clause is added to the clause database.
//
// Notes: --
//-----------------------------------------------------------------------------

ClausePtr ClauseDatabase::consensus (ClausePtr cl1,
					 ClausePtr cl2,
					 VariablePtr var)
{
  DBG1(cout<<"-> Entering ClauseDatabase::consensus ..."<<endl;
       cout<<"  Variable: "<<var->name()<<endl;
       cout<<"    1) ";cl1->dump(cout);cout<<endl;
       cout<<"    2) ";cl2->dump(cout);cout<<endl;);
  int match = 0, can_add_cl = TRUE;
  ClausePtr cl = NULL;

  for_each(plit,cl1->literals(),LiteralPtr) {
    LiteralPtr lit = plit->data();
    VariablePtr lvar = lit->variable();
    if (lvar == var) { match += (lit->sign()) ? (-1) : 1; continue; }
    _scratch_pad[lvar->ID()] = lit->sign();
  }
  for_each(plit2,cl2->literals(),LiteralPtr) {
    LiteralPtr lit = plit2->data();
    VariablePtr lvar = lit->variable();
    if (lvar == var) { match += (lit->sign()) ? (-1) : 1; continue; }

    if (_scratch_pad[lvar->ID()] != NONE &&
	_scratch_pad[lvar->ID()] != lit->sign()) {
      can_add_cl = FALSE;
      break;
    }
  }
  if (match != 0) {
    DBG1(Info("CONSENSUS: Not able to match all literals??"););
    can_add_cl = FALSE;
  }
  if (can_add_cl) {
    cl = add_clause();                                 // Create new clause
    for_each(plit,cl2->literals(),LiteralPtr) {
      LiteralPtr lit = plit->data();
      VariablePtr lvar = lit->variable();
      if (lvar == var || _scratch_pad[lvar->ID()] == lit->sign()) {
	continue;
      }
      add_literal (cl, lvar, lit->sign());
    }
  }
  for_each(plit3,cl1->literals(),LiteralPtr) {
    LiteralPtr lit = plit3->data();
    VariablePtr lvar = lit->variable();
    if (lvar == var) { continue; }
    if (can_add_cl) { add_literal (cl, lvar, lit->sign()); }
    _scratch_pad[lvar->ID()] = NONE;
  }
  DBG1(if(cl) {cout<<"Created (consensus) ";cl->dump(cout);cout<<endl;}
       else {cout<<"Consensus yields NO clause"<<endl;});
  return cl;
}


//-----------------------------------------------------------------------------
// Function: consensus_n_subsume()
//
// Purpose: Creates a clause resulting from the consensus operation.
//          Afterwards, applies subsumption to the clause in order to
//          simplify the clause database.
//
// Side-effects: One clause is added to the clause database. Others can
//               be tagged for subsequent deletion.
//
// Notes: --.
//-----------------------------------------------------------------------------

ClausePtr ClauseDatabase::consensus_n_subsume (ClausePtr cl1,
						   ClausePtr cl2,
						   VariablePtr var)
{
  ClausePtr cl = consensus (cl1, cl2, var);
  subsume (cl);
  return cl;
}


//-----------------------------------------------------------------------------
// Function: consensus()
//
// Purpose: Creates a new clause with a literal on a specified variable
//          removed from the set of literals.
//
// Side-effects: A new clause is added to the database.
//
// Notes: --.
//-----------------------------------------------------------------------------

ClausePtr ClauseDatabase::consensus(ClausePtr cl1, VariablePtr var)
{
  ClausePtr cl = add_clause();                       // Create new clause

  for_each(plit,cl1->literals(),LiteralPtr) {
    LiteralPtr lit = plit->data();
    if (lit->variable() == var) { continue; }
    add_literal (cl, lit->variable(), lit->sign());
  }
  return cl;
}


//-----------------------------------------------------------------------------
// Function: consensus_n_subsume()
//
// Purpose: Consensus and subsumption when goal is to remove literal from
//          clause.
//
// Side-effects: One new clause added to the clause database. Clause may be
//               tagged for subsequent deletion.
//
// Notes: --.
//-----------------------------------------------------------------------------

ClausePtr ClauseDatabase::consensus_n_subsume (ClausePtr cl1,
						   VariablePtr var)
{
  ClausePtr cl = consensus (cl1, var);
  subsume (cl);
  return cl;
}


//-----------------------------------------------------------------------------
// Function: identify_dominated_clauses()
//
// Purpose: Given a clause, finds other clauses dominated by this clause.
//
// Side-effects: --.
//
// Notes: --.
//-----------------------------------------------------------------------------

int ClauseDatabase::identify_dominated_clauses
(ConstraintList &dom_cls, ConstraintPtr ccl)
{
  DBG1(cout<<"-> Entering ClauseDB::identify_dominated_clauses 1..."<<endl;);
  ClausePtr cl = (ClausePtr) ccl;
  DBG0(cout<<"Studying dominance of ";cl->dump(cout);cout<<endl);
  CHECK(if(cl->state() != UNRESOLVED)
	{Warn("Invalid clause state for dominance??");});

  for_each(pclit,cl->literals(),LiteralPtr) {
    VariablePtr var = pclit->data()->variable();

    for_each(pvlit,var->literals(),LiteralPtr) {
      ClausePtr cl2 = pvlit->data()->clause();
      if (cl2 == cl || cl2->state() != UNRESOLVED ||
	  cl2->literals().size() < cl->literals().size() ||
	  !cl2->sole_tag(WHITE)) {            // Cannot dominate tagged clauses
	continue;
      }
      int dominate = TRUE;
      for_each(plitA,cl->literals(),LiteralPtr) {
	LiteralPtr litA = plitA->data();
	VariablePtr varA = litA->variable();
	if (varA->value() != UNKNOWN) { continue; }

	int match = FALSE;
	for_each(plitB,cl2->literals(),LiteralPtr) {
	  LiteralPtr litB = plitB->data();
	  VariablePtr varB = litB->variable();
	  if (varB->value() != UNKNOWN) { continue; }

	  if (varB == varA) {
	    if (litB->sign() != litA->sign()) { dominate = FALSE; }
	    match = TRUE;
	    break;
	  }
	}
	if (!match) { dominate = FALSE; break; }
	if (!dominate) { break; }
      }
      if (dominate) {             // Found a clause (cl2) dominated by cl
	cl2->new_state (DOMINATED);    // Previous state was unresolved
	dom_cls.append (cl2->dominate_ref());
	_tot_dominated_cls++;
	_dominated_clauses++;
	DBG0(cout<<"Dominated ";cl2->dump(cout);cout<<endl);
      }
      DBGn(else { cout<<"NOT dominated ";cl2->dump(cout);cout<<endl; });
    }
  }
  return TRUE;
}


//-----------------------------------------------------------------------------
// Function: identify_dominated_clauses()
//
// Purpose: Given a list of variables, finds a set of clauses dominated by
//          clauses with literals in the set of variables.
//
// Side-effects: --.
//
// Notes: --.
//-----------------------------------------------------------------------------

int ClauseDatabase::identify_dominated_clauses
(ConstraintList &dom_cls, VariableList &var_list)
{
  DBG1(cout<<"-> Entering ClauseDB::identify_dominated_clauses 2..."<<endl;);
  for_each(pvar,var_list,VariablePtr) {
    VariablePtr var = (VariablePtr) pvar->data();
    for_each(plit,var->literals(),LiteralPtr) {
      ConstraintPtr cl = plit->data()->clause();
      if (cl->state() == UNRESOLVED) {
	identify_dominated_clauses (dom_cls, cl);
      }
    }
  }
  return TRUE;
}


//-----------------------------------------------------------------------------
// Function: identify_dominated_clauses()
//
// Purpose: Given a list of clauses, finds a set of clauses dominated by
//          the list of clauses.
//
// Side-effects: --.
//
// Notes: --.
//-----------------------------------------------------------------------------

int ClauseDatabase::identify_dominated_clauses
(ConstraintList &dom_cls, ConstraintList &cl_list)
{
  DBG1(cout<<"-> Entering ClauseDB::identify_dominated_clauses 3..."<<endl;);
  for_each(pcl,cl_list,ConstraintPtr) {
    ClausePtr cl = (ClausePtr) pcl->data();
    if (cl->state() == UNRESOLVED) {
      identify_dominated_clauses (dom_cls, (ConstraintPtr)cl);
    }
  }
  return TRUE;
}


//-----------------------------------------------------------------------------
// Function: identify_dominated_clauses()
//
// Purpose: Given a list of clauses, finds a set of clauses dominated by
//          the list of clauses.
//
// Side-effects: --.
//
// Notes: --.
//-----------------------------------------------------------------------------

int ClauseDatabase::identify_dominated_clauses (ConstraintList &dom_cls)
{
  DBG1(cout<<"-> Entering ClauseDB::identify_dominated_clauses 4..."<<endl;);
  identify_dominated_clauses (dom_cls, _constraints);
  return TRUE;
}


//-----------------------------------------------------------------------------
// Function: erase_dominated_clauses()
//
// Purpose: Cleans up a list of dominated clauses.
//
// Side-effects: --.
//
// Notes: --.
//-----------------------------------------------------------------------------

int ClauseDatabase::erase_dominated_clauses (ConstraintList &dom_cls)
{
  DBG1(cout<<"-> Entering ClauseDatabase::erase_dominated_clauses ..."<<endl;
       cout<<"With "<<dom_cls.size()<<" clauses"<<endl;);

  ConstraintListPtr pcl;
  while (pcl = dom_cls.first()) {
    ClausePtr cl = (ClausePtr) pcl->data();

    CHECK(if (!cl->sole_tag(WHITE))
	  {Warn("Dominated clause w/ TAG??");});
    DBG0(cout<<"NonDominated ";cl->dump(cout);cout<<endl);

    dom_cls.extract (pcl);
    // cl->new_state (UNRESOLVED); not required -jpms 4/13/97.
    cl->set_state();
    update_state (cl, DOMINATED, cl->state());
    _dominated_clauses--;
  }
  CHECK(if (dom_cls.size()) Warn("Still dominated clauses in list??"););
  DBG1(cout<<"-> Exiting ClauseDatabase::erase_dominated_clauses ..."<<endl;);
  return TRUE;
}


//-----------------------------------------------------------------------------
// Function: identify_dominated_variables()
//
// Purpose: Identifies dominance relations between variables.
//
// Side-effects: --.
//
// Notes: --.
//-----------------------------------------------------------------------------

void ClauseDatabase::identify_dominated_variables (VariablePtr var)
{

  Abort("Quitting. Still unable to find variable dominance relations...");
}


//-----------------------------------------------------------------------------
// Function: init_partitions()
//
// Purpose: Creates first partition with all the variables.
//
// Side-effects: --.
//
// Notes: --.
//-----------------------------------------------------------------------------

PartitionPtr ClauseDatabase::init_partitions()
{
  DBG1(cout<<"-> Entering ClauseDatabase::init_partitions..."<<endl;);
  PartitionPtr fst_partition = new Partition();

  for_each(pvar,_variables,VariablePtr) {
    VariablePtr var = (VariablePtr)pvar->data();
    fst_partition->add_element ((VariableListPtr)var->partition_ref());
  }
  DBG1(cout<<"-> Exiting ClauseDatabase::init_partitions..."<<endl;);
  return fst_partition;
}


//-----------------------------------------------------------------------------
// Function: identify_partitions()
//
// Purpose: Identifies a set of partitions within a given partition of the
//          CNF formula. It is *assumed* that the current partition can be
//          partitioned into partitions.
//          Currently, *only* topological partitions are identified.
//
// Side-effects: --.
//
// Notes: Eventually three types of partitions will be identified and handled.
//-----------------------------------------------------------------------------

void ClauseDatabase::identify_partitions (PartitionList &sub_partitions,
					  PartitionPtr curr_partition)
{
  DBG1(cout<<"-> Entering ClauseDatabase::identify_partitions ..."<<endl;
       cout<<"Current partition with "<<curr_partition->variables().size();
       cout<<" variables"<<endl;);
  CHECK(int count_parts = 0;);
  VariableList assign_vars;
  List<int> fig_counters;
  int count_depends;
  _dfs_engine.start();

  PartitionPtr new_partition = NULL;
  VariableListPtr pvar;
  while (pvar = curr_partition->variables().first()) {
    VariablePtr var = pvar->data();

    DBG1(cout<<"Analyzing variable: "<<var->name()<<endl;);
    if (var->value() != UNKNOWN) {
      curr_partition->variables().extract (pvar);
      assign_vars.append (pvar);
      continue;
    }
    _dfs_engine.schedule ((void*)var, var->ID());
    count_depends = 0;

    DBG1(cout<<"Triggering DFS with variable: "<<var->name()<<endl;);
    VariablePtr nvar;
    while (nvar = (VariablePtr) _dfs_engine.currentRef()) {
      if (!new_partition) {
	DBG1(cout<<"Creating another partition... ");
	CHECK(count_parts++;);
	new_partition = new Partition();
	_tot_partitions++;
      }
      DBG1(cout<<" "<<nvar->name(););

      curr_partition->del_element ((VariableListPtr)nvar->partition_ref());
      new_partition->add_element ((VariableListPtr)nvar->partition_ref());

      for_each(pvlit,nvar->literals(),LiteralPtr) {
	ClausePtr cl = pvlit->data()->clause();
	if (cl->state() != UNRESOLVED) {
	  continue;
	}
	for_each(pclit,cl->literals(),LiteralPtr) {
	  LiteralPtr lit = pclit->data();
	  VariablePtr tvar = lit->variable();
	  if (tvar->value() != UNKNOWN) { count_depends++; continue; }
	  _dfs_engine.schedule ((void*)tvar, tvar->ID());
	}
      }
      _dfs_engine.iterate();
    }
    DBG1(cout<<" Done with partition.\n"<<endl;);
    // Give preference to a partition with many assigned neighbor variables.
    insert_ordered_partition (sub_partitions, new_partition,
			      fig_counters, count_depends);
    new_partition = NULL;                          // Can start a new partition
  }
  _dfs_engine.stop();
  fig_counters.erase();                // Clean temp figures for each partition

  curr_partition->variables().append (assign_vars);
  assign_vars.reset();

  DBG0(cout<<"Current status of current ";curr_partition->dump(););
  DBG1(cout<<"-> Exiting ClauseDatabase::identify_partitions ..."<<endl;);
}


//-----------------------------------------------------------------------------
// Function: insert_ordered_partition()
//
// Purpose: Insert a partition in a list of partitions by decreasing order
//          of a given figure of merit. Currently, the figure of merit is
//          how many assigned neighbors each partition has.
//
// Side-effects: List of sub-partitions is updated. The same holds for the
//               temporary list fig_counters.
//-----------------------------------------------------------------------------

void ClauseDatabase::insert_ordered_partition (PartitionList &sub_partitions,
					       PartitionPtr new_partition,
					       List<int> &fig_counters,int val)
{
  CHECK(int lsize = sub_partitions.size(););
  if (fig_counters.size() == 0) {
    sub_partitions.prepend (new_partition);
    fig_counters.prepend (val);
  }
  else {
    if (fig_counters.first()->data() <= val) {
      sub_partitions.prepend (new_partition);
      fig_counters.prepend (val);
    }
    else if (fig_counters.last()->data() >= val) {
      sub_partitions.append (new_partition);
      fig_counters.append (val);
    }
    else {
      PartitionListPtr ppart;
      ListItem<int> *pint;
      for (pint = fig_counters.first(), ppart = sub_partitions.first(); ppart;
	   pint = pint->next(), ppart = ppart->next()) {
	if (pint->data() <= val) {
	  PartitionListPtr npart = new ListItem<Partition*> (new_partition);
	  ListItem<int> *nint = new ListItem<int> (val);
	  sub_partitions.insertBefore (npart, ppart);
	  fig_counters.insertBefore (nint, pint);
	  break;
	}
      }
    }
  }
  CHECK(if (sub_partitions.size()<=lsize)
	{Warn("Invalid list management? NO list insertion made??");});
  return;
}

//-----------------------------------------------------------------------------
// Function: expunge_tagged_clauses()
//
// Purpose: Some clauses of the clause database can become tagged, each
//          tag having a specific meaning. Any tagged clause that has
//          become unresolved is deleted by this function.
//
// Notes: This function explicitly assumes that such tagged clauses
//        *have* to be deleted. This is the currently the case.
//-----------------------------------------------------------------------------

void ClauseDatabase::expunge_tagged_clauses()
{
  DBG1(cout<<"-> Entering ClauseDatabase::expunge_tagged_clauses..."<<endl;);

  register ClausePtr cl;
  while (cl = first_tagged_clause()) {
    DBG1(cout<<"Expunging "; cl->dump(cout); cout<<endl;);
    if (cl->free_literals() <= _mode[_GRP_CONF_CL_RM_THRES_] ||
	cl->state() != UNRESOLVED) {
      _taggedClauses.extract (_taggedClauses.first());
      cl->unset_tag (SALMON);
    }
    else {
      CHECK(if (cl->state() != UNRESOLVED)
	    {cout<<"Problem with ";cl->dump(cout);
	    Warn("Expunging non-UNRESOLVED clauses??");});
      del_clause (cl);
      _tot_del_clauses++;
    }
  }
  DBG1(cout<<"-> Exiting ClauseDatabase::expunge_tagged_clauses..."<<endl;);
}



********** */




//-----------------------------------------------------------------------------
// Function: output_stats()
//
// Purpose: Outputs stats of running SAT algorithm which are directly
//          associated with the clause database.
//-----------------------------------------------------------------------------

void ClauseDatabase::output_stats()
{
  printItem ("Initial number of variables", _initial_var_number);
  printItem ("Initial number of clauses", _initial_cl_number);
  printItem ("Initial number of literals", _initial_lit_number);
  printItem();
  printItem ("Final number of clauses", _clauses.size());
  printItem ("Final number of literals", _lit_number);
  //  printItem ("Number of subsumed clauses", _tot_subsumed_cls);
  //  printItem ("Number of deleted large conf clauses", _tot_del_clauses);
  //  printItem ("Number of dominated clauses", _tot_dominated_cls);
  //  printItem ("Number of identified partitions", _tot_partitions);

  printItem();
}

//-----------------------------------------------------------------------------
// Function: output_assignments()
//
// Purpose: Output value of variables in clause database.
//-----------------------------------------------------------------------------

void ClauseDatabase::output_assignments (ostream &outs, int assigned_only)
{
  outs << "\n    Variable Assignments Satisfying CNF Formula:\n\t";
  for (register int k=0; k < _variables.size(); k++) {
    VariablePtr var = _variables[k];
    if (var->value() != UNKNOWN) {
      if (!var->value()) {
	outs << "-" << var->name() << " ";
      } else {
	outs << var->name() << " ";
      }
    } else if (!assigned_only) {
      outs << "(" << var->name() << ") ";    // In parenthesis if unknown
    }
  }
  outs << endl << endl;
}

//-----------------------------------------------------------------------------
// Function: dump()
//
// Purpose: Dumps clause database and current state of the search process.
//-----------------------------------------------------------------------------

void ClauseDatabase::dump (ostream &outs)
{


  /* *****

  VariableListPtr pvar;
  ClauseListPtr pcl;
  VariablePtr var;

  outs << "\nDumping clause database\n";
  outs << "  Number of Variables: " << _variables.size() << endl;
  outs << "  Number of Clauses: " << _clauses.size() << endl;
  outs << "    Clause Database: " << endl;
  int k = 0;
  for (pcl = _clauses.first(); pcl; pcl = pcl->next()) {
    outs << "      ";
    ((ClausePtr) pcl->data())->dump (outs);
    outs << "\t-> [" << ++k << "]" << endl;
  }
  outs << endl;
  outs << "    End of Clause Database\n" << endl;

  //-------------------------------------------------------------------------
  // Output unit clauses.
  //-------------------------------------------------------------------------

  outs << "    Unit clauses: " << _unitClauses.size() << endl;
  for (pcl = _unitClauses.first(); pcl; pcl=pcl->next()) {
    outs << "      ";
    ((ClausePtr)pcl->data())->dump (outs); cout << endl;
  }
  outs << endl;

  //-------------------------------------------------------------------------
  // Output unsat clauses.
  //-------------------------------------------------------------------------

  outs << "    Unsat clauses: " << _unsatClauses.size() << endl;
  for (pcl = _unsatClauses.first(); pcl; pcl=pcl->next()) {
    outs << "      ";
    ((ClausePtr)pcl->data())->dump (outs); cout << endl;
  }
  outs << endl;

  //-------------------------------------------------------------------------
  // Output variables.
  //-------------------------------------------------------------------------

  outs << "    Variable Set: " << endl;
  for (pvar = _variables.first(); pvar; pvar=pvar->next()) {
    outs << "      ";
    pvar->data()->dump (outs); cout << endl;
  }
  outs << endl;
  outs << "    Done with Clause Database"<<endl<<endl;


  ***** */

}


//-----------------------------------------------------------------------------
// Function: print_database()
//
// Purpose: Prints the contents of the clause database into a file.
//
// Side-effects: None.
//-----------------------------------------------------------------------------

void ClauseDatabase::print_database()
{
  ofstream fp (grasp_output_filename, ios::out);
  CHECK(if (!fp) Abort("Cannot open output file!?"););
  fp << *this;
}


//-----------------------------------------------------------------------------
// Function: check_solution()
//
// Purpose: Checks whether current assignments satisfy all clauses in the
//          clause database.
//-----------------------------------------------------------------------------

int ClauseDatabase::check_solution()
{


  /* *****

     int non_clauses = 0;
     ClauseListPtr pcl;
     for (pcl = _clauses.first(); pcl; pcl = pcl->next()) {
    ClausePtr cl = (ClausePtr) pcl->data();
    int satisfied = FALSE;
    for_each(plit,cl->literals(),LiteralPtr) {
      LiteralPtr lit = plit->data();
      VariablePtr var = (VariablePtr) lit->variable();
      if (var->value() != UNKNOWN &&
	  var->value() ^ lit->sign()) {
	satisfied = TRUE;
      }
    }
    if (!satisfied) {
      cout << "NON SATISFIED ";cl->dump(cout);cout<<endl;
      non_clauses++;
    }
  }
  if (non_clauses) {
    cout<<"\tTOTAL NON SATISFIED CLAUSES: "<<non_clauses<<endl<<endl;
  }
  return (non_clauses > 0);

  ***** */
}


//-----------------------------------------------------------------------------
// Function: check_consistency()
//
// Purpose: Checks clause consistency for all clauses in database.
//          Checks are different depending on whether a consistent search
//          state is assumed.
//-----------------------------------------------------------------------------

int ClauseDatabase::check_consistency (int consistent_state)
{
  DBG1(cout<<"-> Entering ClauseDatabase::check_consistency ..."<<endl;);

  int sat_cnt = 0;
  int inconsist_cnt = 0;
  for (register int k = 0; k < _clauses.size(); k++) {
    if (_clauses[k]) {
      if (!_clauses[k]->check_consistency()) { inconsist_cnt++; }
      if (_clauses[k]->state() == SATISFIED) { sat_cnt++; }

      if (_clauses[k]->state() == UNRESOLVED &&
	  _clauses[k]->test_registered(RegLarge) &&
	  !_clauses[k]->test_registered(RegDeleted) &&
	  _clauses[k]->free_literals() >= _mode[_GRP_CONF_CL_RM_THRES_]) {
	cout<<"\tUNDELETED LARGE: ";_clauses[k]->dump();cout<<endl;
      }
    }
  }
  if (inconsist_cnt) {
    cout<<"\n\n\tFOUND "<<inconsist_cnt<<" INCONSISTENT CLAUSES\n\n";
  }
  if (sat_cnt != _sat_clause_count) {
    cout<<"\n\nThere exist "<<sat_cnt<<" SAT clauses, whereas SAT count is ";
    cout<<_sat_clause_count<<endl;
    Warn("Invalid number of SAT clauses??");
  }
}


//-----------------------------------------------------------------------------
// Function: check_final_consistency()
//
// Purpose: Checks clause consistency for all clauses in database after
//          search terminates.
//          Checks are different depending on whether a consistent search
//          state is assumed.
//-----------------------------------------------------------------------------

int ClauseDatabase::check_final_consistency (int consistent_state)
{
  DBG1(cout<<"-> Entering ClauseDatabase::check_final_consistency..."<<endl;);
  int unit_cls = 0;
  int unsat_cls = 0;
  int sat_cls = 0;
  int unres_cls = 0;
  int empty_cls = 0;

  cout<<"\n\n--------FINAL CONSISTENCY CHECK--------\n";
  int inconsist_cnt = 0;
  for (register int k = 0; k < _clauses.size(); k++) {
    if (_clauses[k]) {
      if(!_clauses[k]->check_consistency()) { inconsist_cnt++; }
      else {
	int cl_state = _clauses[k]->state();
	if (consistent_state && cl_state != SATISFIED) {
	  cout<<"\tINVALID ";_clauses[k]->dump();cout<<endl;
	}
	switch (cl_state) {
	case UNIT:
	  unit_cls++;
	  break;
	case UNSATISFIED:
	  unsat_cls++;
	  break;
	case SATISFIED:
	  sat_cls++;
	  break;
	case UNRESOLVED:
	  unres_cls++;
	  break;
	default:
	  Abort("Clause with invalid state in check consistency???");
	}
	if (!_clauses[k]->state() == UNRESOLVED &&
	    _clauses[k]->test_registered(RegLarge) &&
	  !_clauses[k]->test_registered(RegDeleted) &&
	    _clauses[k]->free_literals() >= _mode[_GRP_CONF_CL_RM_THRES_]) {
	  cout<<"\tUNDELETED LARGE: ";_clauses[k]->dump();cout<<endl;
	}
      }
    }
    else { empty_cls++; }
  }
  if (inconsist_cnt) {
    cout<<"\n\n\tFOUND "<<inconsist_cnt<<" INCONSISTENT CLAUSES\n\n";
  }
  cout << "\tTOTAL clauses:       "<<_clauses.size()-empty_cls<<endl;
  cout << "\tSATISFIED clauses:   "<<sat_cls<<endl;
  cout << "\tUNSATISFIED clauses: "<<unsat_cls<<endl;
  cout << "\tUNIT clauses:        "<<unit_cls<<endl;
  cout << "\tUNRESOLVED clauses:  "<<unres_cls<<endl;
}



//-----------------------------------------------------------------------------
// Member functions for class Clause, LrgClause and Variable.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Function: dump()
//
// Purpose: Dump clause information.
//-----------------------------------------------------------------------------

void Clause::dump (ostream &outs)
{
  outs<<"clause = ( ";
  int cl_dlevel = NONE;
  for (register int k=0; k<_pos_literals.size(); k++) {
    LiteralPtr lit = _pos_literals[k];
    if (lit->sign()) outs<<"-";
    outs<<lit->variable()->name()<<" ";
    if (cl_dlevel < lit->variable()->DLevel()) {
      cl_dlevel = lit->variable()->DLevel();
    }
  }
  for (register int k=0; k<_neg_literals.size(); k++) {
    LiteralPtr lit = _neg_literals[k];
    if (lit->sign()) outs<<"-";
    outs<<lit->variable()->name()<<" ";
    if (cl_dlevel < lit->variable()->DLevel()) {
      cl_dlevel = lit->variable()->DLevel();
    }
  }
  outs<<")";

  outs << "\t#ID: "<<_ID<<" #literals: " << _num_lits;
  outs << " #V1_lits: " << _val1_pos_lits+_val1_neg_lits;
  outs << " #V0_lits: " << _val0_pos_lits+_val0_neg_lits;
  outs << " #state: " << state() << " #reg_flags: "<<_registered_flags;
  outs << " #MDLevel: " << cl_dlevel <<" #";
}


//-----------------------------------------------------------------------------
// Function: check_consistency()
//
// Purpose: Checks whether bounds are consistent.
//-----------------------------------------------------------------------------

int Clause::check_consistency()
{
  DBGn(cout<<"-> Entering Clause::check_consistency..."<<endl;);
  int valP0 = 0, valP1 = 0;
  int valN0 = 0, valN1 = 0;

  for (register int k=0; k<_pos_literals.size(); k++) {
    VariablePtr var = _pos_literals[k]->variable();
    if (var->value() != UNKNOWN) {
      if (var->value()) { valP1++; }
      else              { valP0++; }
    }
  }
  for (register int k=0; k<_neg_literals.size(); k++) {
    VariablePtr var = _neg_literals[k]->variable();
    if (var->value() != UNKNOWN) {
      if (var->value()) { valN0++; }
      else              { valN1++; }
    }
  }
  int consist = TRUE;
  if (valP0 != _val0_pos_lits) {
    Warn("INV Clause. Value 0 POS literals w/ wrong sum"); consist = FALSE;
  }
  if (valP1 != _val1_pos_lits) {
    Warn("INV Clause. Value 1 POS literals w/ wrong sum"); consist = FALSE;
  }
  if (valN0 != _val0_neg_lits) {
    Warn("INV Clause. Value 0 NEG literals w/ wrong sum"); consist = FALSE;
  }
  if (valN1 != _val1_neg_lits) {
    Warn("INV Clause. Value 1 NEG literals w/ wrong sum"); consist = FALSE;
  }
  if (!consist) { dump(); cout << endl; dump_clause_literals (this); }
  return consist;
}


//-----------------------------------------------------------------------------
// Function: dump()
//
// Purpose: Dump large clause information.
//-----------------------------------------------------------------------------

void LrgClause::dump (ostream &outs)
{
  outs<<"clause = ( ";
  int cl_dlevel = NONE;
  for (register int k=0; k<_pos_literals.size(); k++) {
    LiteralPtr lit = _pos_literals[k];
    if (lit->sign()) outs<<"-";
    outs<<lit->variable()->name()<<" ";
    if (cl_dlevel < lit->variable()->DLevel()) {
      cl_dlevel = lit->variable()->DLevel();
    }
  }
  for (register int k=0; k<_neg_literals.size(); k++) {
    LiteralPtr lit = _neg_literals[k];
    if (lit->sign()) outs<<"-";
    outs<<lit->variable()->name()<<" ";
    if (cl_dlevel < lit->variable()->DLevel()) {
      cl_dlevel = lit->variable()->DLevel();
    }
  }
  outs<<")";

  outs << "\t#ID: "<<_ID<<" #literals: " << _num_lits;
  outs << " #V1_lits: " << _val1_pos_lits+_val1_neg_lits;
  outs << " #V0_lits: " << _val0_pos_lits+_val0_neg_lits;
  outs << " #state: " << state() << " #reg_flags: "<<_registered_flags;
  outs << " #MDLevel: " << cl_dlevel <<" #";
}


//-----------------------------------------------------------------------------
// Function: dump()
//
// Purpose: Dump variable information.
//-----------------------------------------------------------------------------

void Variable::dump (ostream &outs)
{
  cout << "Variable: " << name();

  outs << "\t#ID: " << ID() << " #value: " << value() << " #DL: " << DLevel();
  outs << " #antec: " << antecedent();
}


//-----------------------------------------------------------------------------
// Function: dump_clause_list()
//
// Purpose: Print out list of clauses.
//-----------------------------------------------------------------------------

void dump_clause_list (ClauseList &cl_list, int full_info=TRUE)
{
  cout<<"\n----------CLAUSE LIST----------"<<endl;
  for_each(pcl,cl_list,ClausePtr) {
    ClausePtr cl = (ClausePtr) pcl->data();
    cl->dump (cout); cout << endl;
    if (full_info) { dump_clause_literals (cl); }
  }
  cout<<"-------------------------------\n"<<endl;
}

//-----------------------------------------------------------------------------
// Function: dump_clause_vect()
//
// Purpose: Print out array of clauses.
//-----------------------------------------------------------------------------

void dump_clause_vect (DClauseArray &cl_vect, int full_info=TRUE)
{
  cout<<"\n----------CLAUSE VECT----------"<<endl;
  for (register int k=0; k<cl_vect.size(); k++) {
    ClausePtr cl = cl_vect[k];
    cl->dump (cout); cout << endl;
    if (full_info) { dump_clause_literals (cl); }
  }
  cout<<"-------------------------------\n"<<endl;
}

//-----------------------------------------------------------------------------
// Function: dump_clause_literals()
//
// Purpose: Print out list literals of a clause.
//-----------------------------------------------------------------------------

void dump_clause_literals (ClausePtr cl)
{
  for (register int k=0; k<cl->pos_literals().size(); k++) {
    cl->pos_literals()[k]->variable()->dump();
    cout << endl;
  }
  for (register int k=0; k<cl->neg_literals().size(); k++) {
    cl->neg_literals()[k]->variable()->dump();
    cout << endl;
  }
}


//-----------------------------------------------------------------------------
// Function: operator<<
//
// Purpose: Outputs clause database.
//
// Side-effects: None.
//-----------------------------------------------------------------------------

ostream &operator<<(ostream &os, ClauseDatabase &clDB)
{
  os << "c \n";
  os << "c GRASP Clause Database Output\n";
  os << "c \n";
  os << "p cnf " << clDB.num_variables() << " ";
  os << clDB.num_clauses() << endl;
  
  for (register int k=0; k < clDB.clauses().size(); k++) {  // Visit all cls
    ClausePtr cl = clDB.clauses()[k];
    if (cl == NULL || !clDB.writable_clause (cl)) {
      continue;
    }
    for (register int j=0; j<cl->pos_literals().size(); j++) {
      VariablePtr var = cl->pos_literals()[j]->variable();
      os << var->ID() << " ";             // Incr by 1 since smallest ID is 0
    }
    for (register int j=0; j<cl->neg_literals().size(); j++) {
      VariablePtr var = cl->neg_literals()[j]->variable();
      os << "-" << var->ID() << " ";      // Incr by 1 since smallest ID is 0
    }
    os << "0" << endl;
  }
  flush(os);
  return os;
}

/*****************************************************************************/

