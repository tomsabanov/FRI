//-----------------------------------------------------------------------------
// File: fgrp_Defines.hh
//
// Purpose: Defines constants that are used to configure GRASP.
//
// Remarks: --
//
// History: 03/30/98 - JPMS - created.
//
// Copyright (c) 1998 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#ifndef __FGRP_DEFINES__
#define __FGRP_DEFINES__

//-----------------------------------------------------------------------------
// General-purpose definitions.
//-----------------------------------------------------------------------------

#define GRP_DEF_ARRAY_SIZE    4096       // By default array sizes are to be 4K


//-----------------------------------------------------------------------------
// Defines for interfacing SAT solvers.
//-----------------------------------------------------------------------------

enum SearchOutcomes { SATISFIABLE = 0x30, UNSATISFIABLE, ABORTED };

enum ReasoningOutcomes { CONFLICT = 0x50, NO_CONFLICT };

enum DecisionOutcomes { DECISION = 0x70, NO_DECISION };


//-----------------------------------------------------------------------------
// Configuration modes accepted by GRASP.
//-----------------------------------------------------------------------------

#define GRP_MAX_OPTION_SIZE    64    // Maximum number of configuration options

enum GraspModes {
  _GRP_VERBOSITY_ = 0x0,
  _GRP_TIME_LIMIT_,                                 // Maximum CPU time allowed
  _GRP_SPACE_LIMIT_,               // Largest factor of growth of the clause DB
  _GRP_CONFLICT_LIMIT_,                  // Maximum number of conflicts allowed
  _GRP_BACKTRACK_LIMIT_,                // Maximum number of backtracks allowed

  _GRP_PREPROC_MODE_,              // Mode in which preprocessing is to be done
  _GRP_PREPROC_LEVEL_,                         // Level of preprocessing chosen
  _GRP_PREPROC_RECORDING_,           // Type of clause recording during preproc
  _GRP_DEDUCTION_MODE_,                 // Mode in which decution is to be done
  _GRP_DEDUCTION_LEVEL_,                           // Level of deduction chosen
  _GRP_LIT_PROBE_LIMIT_,          // Max number of clause literals to be probed
  _GRP_CL_PROBE_LIMIT_,               // Max number of var clauses to be probed
  _GRP_VAR_PROBE_LIMIT_,                // Max number of variables to be probed
  _GRP_CNA_LIMIT_,                // Max number of common necessary assignments
  _GRP_CL_JUSTIFY_TYPES_,              // Type of clauses that can be justified
  _GRP_APPLY_PLR_,                    // Whether to apply the pure literal rule
    
  _GRP_DECISION_MODE_,          // Whether decision making is static or dynamic
  _GRP_DECISION_LEVEL_,                   // Heuristic used for decision making
  _GRP_DIAGNOSIS_MODE_,                                      // Not defined yet
  _GRP_DIAGNOSIS_LEVEL_,                                     // Not defined yet
  _GRP_PROBE_LEVEL_,                                         // Not defined yet

  _GRP_SUBSUME_CLAUSES_,                    // Whether to subsume clauses in DB
  _GRP_DOMINATE_CLAUSES_,         // Identify dominance relations among clauses
  _GRP_DOMINATE_VARIABLES_,     // Identify dominance relations among variables
  _GRP_PARTITION_MODE_,                   // Modes for partition identification
  _GRP_IDENTIFY_UIPS_,                     // Whether UIPs are to be identified
  _GRP_MULTI_CONFLICTS_,     // Whether multiple conflicts should be identified
  _GRP_MULTI_CONF_HANDLING_,        // Strategy for handling multiple conflicts

  _GRP_BACKTRACKING_STRATEGY_,            // Which backtracking strategy to use
  _GRP_DB_GROWTH_,                      // Chosen growth of teh clause database
  _GRP_CONF_CLAUSE_SIZE_,        // Largest clause size to add to the clause DB
  _GRP_CONF_CL_RM_THRES_,       // Unassigned var size for conf clause deletion
  _GRP_TRIM_SOLUTIONS_,     // Prune computed solutions of irrelevant decisions
  _GRP_CONF_CLAUSE_REPACKING_,   // Retrace dependencies to decisions if useful
  _GRP_ITERATE_SOLUTIONS_,                // Whether to iterate *all* solutions
  _GRP_SKIP_CONF_CLS_,                 // For decision making skip conf clauses
  _GRP_SOLUTION_NUMBER_,               // Maximum number of solutions to report
  _GRP_OUTPUT_SOLUTION_,                 // Whether to output computed solution
  _GRP_OUTPUT_CLDB_,                       // Whether to output clause database
  _GRP_WRT_CL_SIZE_,           // Largest size of recorded clauses to print out

  _GRP_RANDOMIZE_,            // Level of randomization of branching heuristics
  _GRP_RAND_SEED_                         // Seed to consider for randomization
};

enum BackStrategies {
  _CHRONOLOGICAL_B_,             // Standard chronological backtracking is used
  _NON_CHRONOLOGICAL_B_,              // Non-chronological backtracking is used
  _DYNAMIC_B_               // Unordered backtracking/conflict handling is used
};

enum DBGrowths {
  _EXPONENTIAL_DB_,         // Growth of the clause database can be exponential
  _POLYNOMIAL_DB_              // Growth of the clause database *is* polynomial
};

enum MultiConfHandling {
  _MERGED_,                     // Handle all conflicts simultaneously (faster)
  _SEPARATE_                        // Handle each conflict separately (slower)
};

enum PreprocessingMode {
  _W_RELAXATION_,                    // Preprocess while "useful" info is found
  _WO_RELAXATION_                     // Preprocess based on a single iteration
};

enum ProbingModes {
  _GLOBAL_PROBING_,           // Probing is done between every set of variables
  _LOCAL_PROBING_,        // Probing is organized according to existing clauses
  _RECURSIVE_LEARNING_     // Implement clause-based recursive learning probing
};

enum VarPreprocProbeModes {
  _VAR_FULL_PROBE_,       // Probe all var assignments, even if already implied
  _VAR_SINGLE_PROBE_                     // Probe each var assignment only once
};

enum ClauseJustificationModes {
  _ALL_CL_JUSTIFY_,                             // Justify all types of clauses
  _PSPEC_CL_JUSTIFY_,               // Justify only clauses in the original set
  _CONF_CL_JUSTIFY_                    // Justify only conflict-induced clauses
};

enum RecordingModes {
  _FULL_RECORDING_,           // Derive full clause explanations during preproc
  _SIMPLE_RECORDING_              // Derive implic-based clauses during preproc
};

enum PartitionModes {
  _NO_PARTITION_,                       // NO types of partition are identified
  _TOPOLOGICAL_PARTITION_,                   // Identify topological partitions
  _VARIABLE_PARTITION_,               // Identify partitions based on variables
  _LITERAL_PARTITION_                 // Identify partitions based on a literal
};

enum DecisionModes {
  _STATIC_ORD_,                   // Decision ordering chosen before teh search
  _DYNAMIC_ORD_                  // Best assignment chosen before each decision
};

enum DecisionLevels {                             // Decision making procedures
  _FIXED_, _LCS_, _LIS_, _CA_, _JW_, _CAJW_, _PARTITION_,
  _MSOS_, _MSTS_, _MSMM_, _BOHM_, _DLIS_, _DLCS_,
  _RDLIS_, _DLISB_, _DSIS_,
  _MVBS_, _LMVBS_, _DJW_,
  _LMSMM_, _LMSTS_, _LMSOS_, _LBOHM_, _LDLIS_, _LDLCS_,
  _SRAND_
};

#endif // __FGRP_DEFINES__

/*****************************************************************************/

