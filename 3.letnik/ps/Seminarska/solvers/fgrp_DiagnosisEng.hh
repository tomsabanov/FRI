//-----------------------------------------------------------------------------
// File: fgrp_DiagnosisEng.hh
//
// Purpose: Declaration of a diagnosis engine for SAT algorithms on CNF
//          formulas
//
// Remarks: --
//
// History: 03/30/98 - JPMS - created.
//
// Copyright (c) 1998 Joao P. Marques Silva.
//-----------------------------------------------------------------------------

#ifndef __FGRP_DIAGNOSISENG__
#define __FGRP_DIAGNOSISENG__

#include "fgrp_Defines.hh"
#include "fgrp_STS.hh"
#include "fgrp_CDB.hh"
#include "fgrp_BCP.hh"
#include "fgrp_BCA.hh"



//-----------------------------------------------------------------------------
// Typedefs for the diagnosis engine.
//-----------------------------------------------------------------------------

class DiagnosisEngine;

typedef DiagnosisEngine *DiagnosisEnginePtr;



//-----------------------------------------------------------------------------
// Class: DiagnosisEngine
//
// Purpose: Diagnoses conflicts on clause databases.
//-----------------------------------------------------------------------------

class DiagnosisEngine {
public:

  //---------------------------------------------------------------------------
  // Constructor/destructor.
  //---------------------------------------------------------------------------

  DiagnosisEngine (Array<int> &_mode,
		   ClauseDatabase &clDB, SearchTree &stree,
		   BCP &bcp, BCA &bca);
  virtual ~DiagnosisEngine();

  //---------------------------------------------------------------------------
  // Interface contract.
  //---------------------------------------------------------------------------

  virtual int diagnose();     // Enter a non-inconsistent state of the search

  virtual void setup();                    // Set up internal data structures
  virtual void cleanup();                // Clean up internal data structures

  inline int number_conflicts() { return _conflicts_diagnosed; }
  inline int number_backtracks() { return _tot_backtrack_number; }

  virtual void output_stats();            // Print out stats due to deduction

protected:

  virtual void backtrack (int BLevel);        // Backtracks to a given DLevel
  virtual int chronological_backtrack();
  
  //---------------------------------------------------------------------------
  // Data structures used.
  //---------------------------------------------------------------------------

  ClauseDatabase &_clDB;         // Clause database is the constraint network
  SearchTree &_stree;           // Search tree that keeps the search info

  BCP &_bcp;                             // Engine for constraint propagation
  BCA &_bca;                                  // Engine for conflict analysis

  Array<int> &_mode;                       // Configuration options for GRASP

  //---------------------------------------------------------------------------
  // Stats gathering.
  //---------------------------------------------------------------------------

  int _tot_backtrack_number;
  int _nc_backtrack_number;
  int _max_back_jump;
  int _conflicts_diagnosed;

private:

};

#endif // __FGRP_DIAGNOSISENG__

/*****************************************************************************/

