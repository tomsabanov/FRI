//
// osnovnih 6 funkcij MPI
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <mpi.h>
#include <limits.h>

#define RANGE 1000000


int compare (const void * a, const void * b) {
   return ( *(int*)a - *(int*)b );
}


void merge(int* arr, int l, int m, int r) 
{ 
    int i, j, k; 
    int n1 = m - l + 1; 
    int n2 =  r - m; 
  
    /* create temp arrays */
    int L[n1], R[n2]; 
  
    /* Copy data to temp arrays L[] and R[] */
    for (i = 0; i < n1; i++) 
        L[i] = arr[l + i]; 
    for (j = 0; j < n2; j++) 
        R[j] = arr[m + 1+ j]; 
  
    /* Merge the temp arrays back into arr[l..r]*/
    i = 0; // Initial index of first subarray 
    j = 0; // Initial index of second subarray 
    k = l; // Initial index of merged subarray 
    while (i < n1 && j < n2) 
    { 
        if (L[i] <= R[j]) 
        { 
            arr[k] = L[i]; 
            i++; 
        } 
        else
        { 
            arr[k] = R[j]; 
            j++; 
        } 
        k++; 
    } 
  
    /* Copy the remaining elements of L[], if there 
       are any */
    while (i < n1) 
    { 
        arr[k] = L[i]; 
        i++; 
        k++; 
    } 
  
    /* Copy the remaining elements of R[], if there 
       are any */
    while (j < n2) 
    { 
        arr[k] = R[j]; 
        j++; 
        k++; 
    } 
} 
  
/* l is for left index and r is right index of the 
   sub-array of arr to be sorted */
void mergeSort(int* arr, int l, int r) 
{ 
    if (l < r) 
    { 
        // Same as (l+r)/2, but avoids overflow for 
        // large l and h 
        int m = l+(r-l)/2; 
  
        // Sort first and second halves 
        mergeSort(arr, l, m); 
        mergeSort(arr, m+1, r); 
  
        merge(arr, l, m, r); 
    } 
} 

int main(int argc, char* argv[])
{
	int N = atoi(argv[1]);
	//printf("Number of elements to sort : %d\n",N);

	int id, proc_num;
	int* global = NULL;
	int* local = NULL;
	int ROOT = 0;

	MPI_Init(&argc, &argv);

	MPI_Comm_rank(MPI_COMM_WORLD, &id);
	MPI_Comm_size(MPI_COMM_WORLD, &proc_num);


	if(id == 0){
		// Ustvarimo array N 
		global = (int*)malloc(sizeof(int) * N);
		for(int i = 0; i < N; i++){
			global[i] = (rand() % (RANGE + 1)); 
		}
	}

	int rem = N % proc_num;
	int counts[proc_num];
	int offsets[proc_num];
	int sum = 0;
    for (int i = 0; i < proc_num; i++) {
        counts[i] = N / proc_num;
        if (rem > 0) {
            counts[i] += 1;
            rem--;
        }
        offsets[i] = sum;
        sum += counts[i];
    }

	local = (int*)malloc(sizeof(int) * counts[id]);


    MPI_Scatterv(global, counts, offsets, MPI_INT, local, counts[id], MPI_INT, ROOT, MPI_COMM_WORLD);

	//printf("Process %d\n", id);
	//for(int i = 0; i<counts[id]; i++){
		//printf("%d ", local[i]);
	//}
	//printf("\n");


    qsort(local, counts[id], sizeof(int), compare);

	//printf("Sorted\n");
	//for(int i = 0; i<counts[id]; i++){
		//printf("%d ", local[i]);
	//}
	//printf("\n\n\n");

    MPI_Gatherv(local, counts[id], MPI_INT, global, counts, offsets, MPI_INT, ROOT, MPI_COMM_WORLD);

	if(id == 0){

        int* final = (int*)malloc(sizeof(int) * N);
        int* off = (int*)malloc(sizeof(int) * proc_num);
        for(int i = 0; i<proc_num; i++){
            off[i] = offsets[i];
        }

        for(int i = 0; i<N; i++){
            int min = INT_MAX;
            int min_off = 0;
            for(int j = 0; j<proc_num; j++){
                if(off[j] < offsets[j] + counts[j]){
                    if(global[off[j]] < min){
                        min = global[off[j]];
                        min_off = j;
                    }
                }
            }
            final[i] = min;
            off[min_off] += 1;
        }

        //for(int i = 0; i<N; i++){
		//	printf("%d ", global[i]);
		//}

	}


	free(global);
	free(local);

	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	return 0;
}
