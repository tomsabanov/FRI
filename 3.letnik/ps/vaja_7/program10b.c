//
// pakiranje in razpakiranje podatkov
//

#include <stdio.h>
#include "mpi.h"

int main(int argc, char *argv[]) 
{
	int				myid, nn;
	float			aa, bb;
	MPI_Datatype	mytype;

	char			packet[100];
	int				position;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);

	if(myid == 0)
	{
		printf("Vnesi aa, bb, nn: ");
		fflush(stdout);
		scanf("%f %f %d", &aa, &bb, &nn);

		position = 0;
		MPI_Pack(&aa, 1, MPI_FLOAT, packet, 100, 
				 &position, MPI_COMM_WORLD);
		MPI_Pack(&bb, 1, MPI_FLOAT, packet, 100, 
				 &position, MPI_COMM_WORLD);
		MPI_Pack(&nn, 1, MPI_INT, packet, 100, 
				 &position, MPI_COMM_WORLD);

		MPI_Bcast(packet, 100, MPI_PACKED, 0, MPI_COMM_WORLD);
	}
	else
	{
		MPI_Bcast(packet, 100, MPI_PACKED, 0, MPI_COMM_WORLD);

		position = 0;
		MPI_Unpack(packet, 100, &position, &aa, 1,
				   MPI_FLOAT, MPI_COMM_WORLD);
		MPI_Unpack(packet, 100, &position, &bb, 1,
				   MPI_FLOAT, MPI_COMM_WORLD);
		MPI_Unpack(packet, 100, &position, &nn, 1,
				   MPI_INT, MPI_COMM_WORLD);
	}
	
	printf("Proces: %d, aa: %f, bb: %f, nn: %d\n", myid, aa, bb, nn);

	MPI_Finalize();
}