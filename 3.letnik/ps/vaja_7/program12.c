#include "mpi.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main( int argc, char *argv[] )
{
    int myid, procs;
    int chunk = 2;
    int i;
    int *sb;
    int *rb;
	char str[255];

    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD,&myid);
    MPI_Comm_size(MPI_COMM_WORLD,&procs);

    sb = (int *)malloc(procs*chunk*sizeof(int));
    rb = (int *)malloc(procs*chunk*sizeof(int));

	for ( i=0 ; i < procs*chunk ; ++i ) 
	{
        sb[i] = myid*10 + i;
        rb[i] = 0;
    }

	sprintf( str, "Proces (SB) %d: ", myid );
	for( i=0; i<procs*chunk; i++)
		sprintf( str, "%s %02d", str, sb[i] );
	printf("%s\n", str);
	fflush(stdout);

    MPI_Alltoall(sb, chunk, MPI_INT, 
				 rb, chunk, MPI_INT, 
				 MPI_COMM_WORLD);

	sprintf( str, "Proces (RB) %d: ", myid );
	for( i=0; i<procs*chunk; i++)
		sprintf( str, "%s %02d", str, rb[i] );
	printf("%s\n", str);
	fflush(stdout);

    free(sb);
    free(rb);
    MPI_Finalize();

	return(0);
}

