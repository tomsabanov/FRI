//
// round robin - MPI_Sendreceive
//

#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

#define buffsize 1000000

int main(int argc, char* argv[])
{
	int			taskid, ntasks;
	int			i;
	int			*sendbuff, *recvbuff;
	double		inittime, totaltime;
	MPI_Request	sendrequest, recvrequest;

	MPI_Init(&argc, &argv);

	MPI_Comm_rank(MPI_COMM_WORLD, &taskid);
	MPI_Comm_size(MPI_COMM_WORLD, &ntasks);

	sendbuff = malloc(sizeof(int)*buffsize);
	recvbuff = malloc(sizeof(int)*buffsize);

	for(i=0; i<buffsize; i++)
		sendbuff[i] = i;

	MPI_Barrier(MPI_COMM_WORLD);
	inittime = MPI_Wtime();

	MPI_Sendrecv(sendbuff, buffsize, MPI_INT, (taskid+1+ntasks)%ntasks, 0, 
		         recvbuff, buffsize, MPI_INT, (taskid-1+ntasks)%ntasks, MPI_ANY_TAG,
				 MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		
	MPI_Barrier(MPI_COMM_WORLD);
	totaltime = MPI_Wtime() - inittime;

	if(taskid == 0)
		printf("Cas prenosa: %f s\n", totaltime);

	free(recvbuff);
	free(sendbuff);

	MPI_Finalize();
}
