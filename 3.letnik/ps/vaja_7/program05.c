//
// oddajanje - Bcast
//

#include <stdio.h>
#include "mpi.h"

int main(int argc, char* argv[]) 
{  
	int		id, value;

	MPI_Init(&argc, &argv);  
	MPI_Comm_rank(MPI_COMM_WORLD, &id);

	if( id == 0)
	{
		printf("Vpisi vrednost:");
		fflush(stdout);
		scanf("%d", &value);
	}

	MPI_Bcast(&value, 1, MPI_INT, 0, MPI_COMM_WORLD);

	printf("Proces %d: value = %d\n", id, value);

	MPI_Finalize();
}
