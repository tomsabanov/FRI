//
// kr�enje - reduce (max in lokacija)
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "mpi.h"

typedef struct { 
	double	value;
	int		id; 
} valueandid; 

int main(int argc, char* argv[]) 
{  
    valueandid	in, out; 
    int			myid, procs; 
 
	MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &procs); 
    MPI_Comm_rank(MPI_COMM_WORLD, &myid); 

	srand( (unsigned)time( NULL )*myid );

	in.value = rand(); 
    in.id = myid; 
	printf("%d: %lf\n", in.id, in.value);
	fflush(stdout);

	MPI_Reduce( &in, &out, 1, MPI_DOUBLE_INT, MPI_MAXLOC, 0, MPI_COMM_WORLD ); 

	if (myid == 0) 
		printf("max = %lf, id = %d\n", out.value, out.id);

	MPI_Finalize();
}
