//
// ra�unanje PI z enostavno numeri�no integracijo
//

#include <stdio.h>
#include "mpi.h"

int main(int argc, char* argv[]) 
{  
	int	myid, procs;
	int n;
	double h, mystart, mystop;
	double x, mysum, sum;

	MPI_Init(&argc, &argv);  
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);
	MPI_Comm_size(MPI_COMM_WORLD, &procs);

	if(myid == 0)
	{
		printf("Stevilo pravokotnikov = ");
		fflush(stdout);
		scanf("%d", &n);
		h = 1.0/n;
	}

	MPI_Bcast(&h, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

	mystart = myid/(double)procs;
	mystop = (myid+1)/(double)procs;

	mysum = 0.0;
	x = mystart;
	while (x < mystop)
	{
		mysum += 4.0/(1+x*x)*h;
		x += h;
	}

	MPI_Reduce(&mysum, &sum, 1, MPI_DOUBLE, 
			   MPI_SUM, 0, MPI_COMM_WORLD);

	if( myid == 0)
		printf("Pi = %lf\n", sum);

	MPI_Finalize();
}
