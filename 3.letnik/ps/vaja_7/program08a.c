//
// raztros in zbiranje (scatter & gather)
//

#include <stdlib.h>
#include <stdio.h>
#include "mpi.h"

main(int argc, char* argv[]) 
{
	int		procs, myid;
	int 	scatterSize;
	int		i;
	double	*vectorA, *vectorB;
	double	*myvectorA, *myvectorB;
	double	mydotProduct, dotProduct, *dotProducts;
	int		vectorSize = 80;

	MPI_Init(&argc, &argv); 
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);
	MPI_Comm_size(MPI_COMM_WORLD, &procs);

	vectorA = NULL;
	vectorB = NULL;
	if(myid == 0)
	{
		vectorA  = (double *) malloc(vectorSize*sizeof(double));
		vectorB  = (double *) malloc(vectorSize*sizeof(double));
		for(i=0; i<vectorSize; i++)
		{
			vectorA[i] = i/2.0;
			vectorB[i] = i;
		}
	}

	if((vectorSize < procs) || (vectorSize % procs != 0)) 
	{
		MPI_Finalize();
		if(myid == 0)
			printf("Število elementov ni večkratnih procesov...\n");
		exit(0);
	}  	

	scatterSize = vectorSize / procs;
	myvectorA = (double *)malloc(scatterSize * sizeof(double));
	myvectorB = (double *)malloc(scatterSize * sizeof(double));

	MPI_Scatter( vectorA, scatterSize, MPI_DOUBLE, 
				 myvectorA, scatterSize, MPI_DOUBLE, 
				 0, MPI_COMM_WORLD);			 
	MPI_Scatter( vectorB, scatterSize, MPI_DOUBLE, 
				 myvectorB, scatterSize, MPI_DOUBLE, 
				 0, MPI_COMM_WORLD);

	mydotProduct = 0.0;
	for(i=0; i<scatterSize; i++) 
		mydotProduct += (myvectorA[i] * myvectorB[i]);

	dotProducts = NULL;
	if( myid == 0 )
		dotProducts = (double *)malloc(procs*sizeof(double));

	MPI_Gather( &mydotProduct, 1, MPI_DOUBLE, 
	            dotProducts, 1, MPI_DOUBLE, 
	            0, MPI_COMM_WORLD );

	if( myid == 0 )
	{
		dotProduct = 0.0;
		for( i=0; i<procs; i++ )
			dotProduct += dotProducts[i];
		printf("Skalarni produkt: %f\n", dotProduct);
	}

	free(vectorA);
	free(vectorB);
	free(myvectorA);
	free(myvectorB);

	MPI_Finalize();
}
