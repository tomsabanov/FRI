#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

#define N	8
#define	M	16

int main(int argc, char* argv[])
{
	int		i, j;
	double	*matrixData;
	double	**matrix;

	double	**m;

	m = (double **)malloc(sizeof(double *)*N);
	for(i=0; i<N; i++)
		m[i] = (double *)malloc(sizeof(double)*M);
	for(i=0; i<N; i++)
	{
		for(j=0; j<M; j++)
		{
			m[i][j] = i*M+j;
			printf("%3.0lf ", m[i][j]);
		}
		printf("\n");
	}

	printf("\n\n");

	matrixData = (double *)malloc(sizeof(double)*N*M);
	for(i=0; i<N*M; i++)
		matrixData[i] = i;

	matrix = (double **)malloc(sizeof(double*)*N);
	for(i=0; i<N; i++)
		matrix[i] = &matrixData[i*M];

	for(i=0; i<N; i++)
	{
		for(j=0; j<M; j++)
			printf("%3.0lf ", matrix[i][j]);
		printf("\n");
	}

	for(i=0; i<N; i++)
		free(m[i]);
	free(m);

	free(matrix);
	free(matrixData);

}
