//
// osnovnih 6 funkcij MPI
//

#include <stdio.h>
#include <string.h>
#include <mpi.h>

#define BUF_SIZE 1024

int main(int argc, char* argv[])
{
	int		myid, size;
	char	buffer[BUF_SIZE];
	int		i;

	MPI_Init(&argc, &argv);

	MPI_Comm_rank(MPI_COMM_WORLD, &myid);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	if(myid == 0){
		// Naprej poslji naslednjemu
		sprintf(buffer, "%d-", myid);
		MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR,
			     myid + 1, myid, MPI_COMM_WORLD);


		// Potem receive od zadnjega
		MPI_Recv(buffer, BUF_SIZE, MPI_CHAR,
				    size-1, MPI_ANY_TAG, MPI_COMM_WORLD, 
					MPI_STATUS_IGNORE);
		sprintf(buffer + strlen(buffer), "%d", myid);

		printf("%s\n", buffer);
	}
	else{
		// Receive od prejsnega
		
		MPI_Recv(buffer, BUF_SIZE, MPI_CHAR,
				myid-1, MPI_ANY_TAG, MPI_COMM_WORLD, 
				MPI_STATUS_IGNORE);

		sprintf(buffer + strlen(buffer), "%d-", myid);

		int next = myid + 1;
		if(myid == size-1) next = 0;

		MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR,
			     next, myid, MPI_COMM_WORLD);
	}


	MPI_Finalize();
	return 0;
}
