//
// organizacija procesov v kartezični koordinatni sistem
//

#include <stdio.h>
#include "mpi.h"

int main(int argc, char *argv[]) 
{
	int			myid, procs;

	int			dim_sizes[2];
	int			wrap_around[2];
	int			dims, reorder;
	MPI_Comm	grid_comm;

	int			mygridid;
	int			mygridcoords[2];
	int			mysrc[2], mydest[2];

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);
	MPI_Comm_size(MPI_COMM_WORLD, &procs);

	dims = 2;
	if( procs%dims != 0)
		MPI_Abort(MPI_COMM_WORLD, 1);

	dim_sizes[0] = dims; dim_sizes[1] = procs/dims;
	wrap_around[0] = 0; wrap_around[1] = 1;
	reorder = 1;
	MPI_Cart_create(MPI_COMM_WORLD, dims, dim_sizes, 
		            wrap_around, reorder, &grid_comm);

	// koordinate lahko izvemo takole
	MPI_Comm_rank(grid_comm, &mygridid);
	MPI_Cart_coords(grid_comm, mygridid, 2, mygridcoords);

	// ali pa takole
	MPI_Cart_get(grid_comm, dims, dim_sizes, wrap_around, mygridcoords);

	// kdo so sosedje?
	MPI_Cart_shift(grid_comm, 0, 1, &mysrc[0], &mydest[0]);
	MPI_Cart_shift(grid_comm, 1, 1, &mysrc[1], &mydest[1]);

	printf("Proces: %d, gridid: %d, (%d, %d), sosedje: z: %d, s: %d, l: %d, d: %d\n",
			myid, mygridid, mygridcoords[0], mygridcoords[1], 
			mysrc[0], mydest[0], mysrc[1], mydest[1]);

	MPI_Finalize();
}
