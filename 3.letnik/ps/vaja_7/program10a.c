//
// sestavljanje novega podatkovnega tipa
//

#include <stdio.h>
#include "mpi.h"

void Build_derived_type( float *a, float *b, int *n, 
						 MPI_Datatype *newtype )
{
	MPI_Datatype	typelist[3];
	int				block_len[3];		// �tevilo elementov istega tipa
	MPI_Aint		displacement[3];	// naslov spremnljivk v pomnilniku
	MPI_Aint		addrstart, addr;	// samo za ra�unanje

	typelist[0] = MPI_FLOAT;
	typelist[1] = MPI_FLOAT;
	typelist[2] = MPI_INT;

	block_len[0] = block_len[1] = block_len[2] = 1;

	displacement[0] = 0;
	MPI_Address(a, &addrstart);
	MPI_Address(b, &addr);
	displacement[1] = addr-addrstart;
	MPI_Address(n, &addr);
	displacement[2] = addr-addrstart;

	MPI_Type_struct(3, block_len, displacement, typelist, newtype);
	MPI_Type_commit(newtype);			// s commit ga pripravimo za prenose
}

int main(int argc, char *argv[]) 
{
	int				myid, nn;
	float			aa, bb;
	MPI_Datatype	mytype;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);

	if(myid == 0)
	{
		printf("Vnesi aa, bb, nn: ");
		fflush(stdout);
		scanf("%f %f %d", &aa, &bb, &nn);
	}
	
	Build_derived_type(&aa, &bb, &nn, &mytype);

	MPI_Bcast(&aa, 1, mytype, 0, MPI_COMM_WORLD);

	printf("Proces: %d, aa: %f, bb: %f, nn: %d\n", myid, aa, bb, nn);

	MPI_Finalize();
}