//
// kr�enje - reduce (vsota)
//

#include <stdio.h>
#include "mpi.h"

int main(int argc, char* argv[]) 
{  
	int		id;
	int		valuesend, valuerecv;

	MPI_Init(&argc, &argv);  
	MPI_Comm_rank(MPI_COMM_WORLD, &id);

	valuesend = id;

	printf("Proces %d: value = %d\n", id, valuesend);

	// MPI_Reduce kli�ejo vsi procesi na enak na�in !!!
	MPI_Reduce(&valuesend, &valuerecv, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

	if (id == 0)
		printf("Vsota vrednosti z vseh procesov je %d\n", valuerecv);

	MPI_Finalize();
}
