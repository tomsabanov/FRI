#include <stdio.h>
#include <stdlib.h>
#include <chrono>
#include <array>
#include <vector>
#include <fstream>
#include <string.h>

#include "../Parser.h"
#include "../Util.h"
#include "./pool.h"


struct partialSolution{
    int firstVar;
    int lastVar;
    int bits;
    int numOfSol;
    char* solutions;
};

void getBools(std::vector<bool> b, char* bools){
    for(int i = 0; i<b.size(); i++){
        if(b.at(i) == true){
            bools[i] = '1';
        }
        else{
            bools[i] = '0';
        }
    }
}
void printPartialSolutions(char* bools, int numOfSol, int numOfBits){
        if(numOfSol>0){
            printf("Result %d-%d:\n",1,numOfBits);  
        }
        for(int j = 0; j<numOfBits; j++){
            int ele = j+1;
            if(bools[j] == '0'){
                ele = -ele;
            }
            printf("%d ", ele);
        }
        printf(" 0\n");    
}
std::vector<bool> getPartialResults(int firstVar, int lastVar, char* input_file){

    std::string command(std::string("./brute ") + input_file + " " + std::to_string(firstVar) 
                        + " " + std::to_string(lastVar) + " 2>&1");

    
    std::array<char, 250> buffer;
    std::vector<bool> partialResults;


    int length = lastVar - firstVar + 1;
    FILE* pipe = popen(command.c_str(), "r");
    if (!pipe)
    {
        std::cerr << "Couldn't start command." << std::endl;
        return partialResults;
    }
    while (fgets(buffer.data(), 128, pipe) != NULL) {
        int num = atoi(buffer.data());
        for (int j = 0; j<length; j++) {
            partialResults.push_back((num >>j) & 1);
	    }
        
    }
    auto returnCode = pclose(pipe);
    //std::cout << returnCode << std::endl;

    return partialResults;
}
std::vector<char*> combinePartialSolutions(partialSolution* p1,partialSolution* p2, char* input_file){

    std::string p1_firstVar = std::to_string(p1->firstVar);
    std::string p1_lastVar = std::to_string(p1->lastVar);
    std::string p1_bits = std::to_string(p1->bits);
    std::string p1_numOfSol = std::to_string(p1->numOfSol);

    std::string p2_firstVar = std::to_string(p2->firstVar);
    std::string p2_lastVar = std::to_string(p2->lastVar);
    std::string p2_bits = std::to_string(p2->bits);
    std::string p2_numOfSol = std::to_string(p2->numOfSol);

    // write solutions to tmp file
    ofstream p1_file;
    p1_file.open ("p1_sol.txt");
    p1_file << p1->solutions;
    p1_file.close();

    ofstream p2_file;
    p2_file.open ("p2_sol.txt");
    p2_file << p2->solutions;
    p2_file.close();


    std::string parameters(p1_firstVar+" "+p1_lastVar+" "+p1_bits+" "+p1_numOfSol+" p1_sol.txt "+
                           p2_firstVar+" "+p2_lastVar+" "+p2_bits+" "+p2_numOfSol+" p2_sol.txt");

    std::string command(std::string("./combine ") + input_file + " " +  parameters + " 2>&1");
    

    std::array<char, 250> buffer;
    std::vector<char*> partialResults;
    
    /////////// Combine with threads instead of gpu
    /*
    Parser p;
	p.parse(input_file);

	int var_count = p.getVariableCount();
    int* clauses = p.getTable();
    int cl_count = p.getClausesCount();
    ThreadPool pool(1000);
    for(int i = 0; i<p1->numOfSol; i++){
        pool.enqueue([clauses,cl_count,p1,p2](int sol_id) { 
            
            char* res = (char*)malloc(sizeof(char) * 50);
            for(int j = 0; j<25;j++){
                res[j] = p2->solutions[sol_id*25 + j];
            }
            // loop through every combination
            for(int k = 0; k<p2->numOfSol; k++){
                for(int i = 0; i<25; i++){
                    res[25 + i] = p1->solutions[k*25 + i]; 
                }
                testSolution(clauses,res,50,cl_count,3);
            }
            free(res);
        }, i);
    }
    return partialResults;
    */
    //////////////////////////////////////////////////////////////////



    int bits = p1->bits + p2->bits;
    int numOfSol = 0;

    char* selected = p1->solutions;
    char* tmp = p2->solutions;
    int selected_bits = p1->bits;
    int tmp_bits = p2->bits;
    if(p2->bits > p1->bits){
        selected = p2->solutions;
        tmp = p1->solutions;
        selected_bits = p2->bits;
        tmp_bits = p1->bits;
    }

    FILE* pipe = popen(command.c_str(), "r");
    if (!pipe)
    {
        //printf("Prislo do napake!\n");
        return partialResults;
    }
    while (fgets(buffer.data(), 256, pipe) != NULL) {

        int index_sel, index_tmp;
        sscanf(buffer.data(),"%d %d",&index_sel,&index_tmp);
        
        numOfSol++;
        
        char* res = (char*)malloc(sizeof(char) * bits);
        for(int i = 0; i<selected_bits;i++){
            res[i] = p1->solutions[index_sel*selected_bits + i];
        }    
        for(int j = 0; j<tmp_bits; j++){
            res[selected_bits + j] = p2->solutions[index_tmp*tmp_bits + j];
        }
        partialResults.push_back(res);    
    }
    //int returnCode = pclose(pipe);
    //printf("Return code %d\n",returnCode);
    //std::cout << returnCode << std::endl;


    return partialResults;

}

int divideAndConquer(char* input_file){
    Parser p;
	p.parse(input_file);

    int bits = 25;
	int var_count = p.getVariableCount();
    int* clauses = p.getTable();
    int cl_count = p.getClausesCount();

    if(var_count < bits){
        bits = var_count;
    }

    std::vector<partialSolution*> partialSolutions;

    // Dividing the space and finding the partial solutions
    int numOfPartials = var_count/bits;
    int firstVar = 1;
    int lastVar = bits;
    for(int i = 0; i<numOfPartials; i++){
        partialSolution* sol = (struct partialSolution*)malloc(sizeof(struct partialSolution));
        sol->firstVar = firstVar;
        sol->lastVar = lastVar;
        sol->bits = bits;
        
        std::vector<bool> partial = getPartialResults(sol->firstVar,sol->lastVar,input_file);
        int numOfSol = partial.size()/bits;
        sol->numOfSol = numOfSol;

        sol->solutions = (char*)malloc(sizeof(char) * bits * numOfSol);
        getBools(partial, sol->solutions);
    
        partialSolutions.push_back(sol);
        printf("Number of solutions is %d, size of each solution is %d\n", numOfSol,bits);
        //printPartialSolutions(sol->solutions,numOfSol,bits);

        firstVar = lastVar+1;
        lastVar = firstVar+bits-1;
    }

    //Check if there is only one set of partial solutions
    if(partialSolutions.size() == 1){
        // Check the solutions then return TO-DO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        printPartialSolutions(partialSolutions.at(0)->solutions,partialSolutions.at(0)->numOfSol,partialSolutions.at(0)->bits);
        return 0;
    }
    
    

    // Combining the partial solutions
    std::vector<partialSolution*> combined_partials;

    while(partialSolutions.size()>0){
        if(partialSolutions.size() == 1 && combined_partials.size()==0){
            break;
        }
        partialSolution* p1;
        partialSolution* p2;
        
        if(partialSolutions.size() == 1 && combined_partials.size() == 1){
            p1 = partialSolutions.back();
            p2 = combined_partials.back();
            partialSolutions.pop_back();
            combined_partials.pop_back();
        }
        else{
            p2 = partialSolutions.back();
            partialSolutions.pop_back();

            p1 = partialSolutions.back();
            partialSolutions.pop_back();
        }


        printf("Combining:\n");
        printf("p1 : %d-%d\n",p1->firstVar,p1->lastVar);
        printf("p2 : %d-%d\n\n",p2->firstVar,p2->lastVar);

        std::vector<char*> partialResults = combinePartialSolutions(p1,p2,input_file);

        // partialResults to one char* and in to a new struct and push to combined
        int bits_combined = p1->bits + p2->bits;
        int size_combined = bits_combined*partialResults.size();

        partialSolution* combined = (struct partialSolution*)malloc(sizeof(struct partialSolution));
        combined->firstVar = p1->firstVar;
        combined->lastVar = p2->lastVar;
        combined->bits = bits_combined;
        combined->numOfSol = partialResults.size();
        combined->solutions = (char*)malloc(sizeof(char) * size_combined);
        for(int i = 0; i<partialResults.size();i++){
            for(int j = 0; j<bits_combined; j++){
                combined->solutions[i*bits_combined + j] = partialResults.at(i)[j];
            }
        }
        combined_partials.push_back(combined);

        //printf("Got combined %d-%d with numOfSol %d and bits %d\n",
        //        combined->firstVar,combined->lastVar, combined->numOfSol, combined->bits);
        
        if(partialSolutions.size() == 0){
            int s = combined_partials.size();
            for(int i = 0; i<s;i++){
                partialSolutions.push_back(combined_partials.back());
                combined_partials.pop_back();
            }
        } 

        remove("p1_sol.txt");
        remove("p2_sol.txt");

        free(p1);
        free(p2);

        // check if partialSolutions is empty and fill it with combined, then empty combined
    }
    
    partialSolution* fin = partialSolutions.at(0);
    printPartialSolutions(fin->solutions,fin->numOfSol,fin->bits);
    return 0;
}

int main(int argc, char** argv) {
    if(argc < 2){
        printf("Supply the file with data!\n");
        return 0;
    }
	char* input_file = argv[1];
    divideAndConquer(input_file);

}

