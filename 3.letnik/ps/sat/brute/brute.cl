
void intToBits(bool array[],int stevilo,int bits){
	for (int i = 0; i < bits; i++) {
		array[i] = (stevilo >> i) & 1;
	}
}

__kernel void brute(__global const int *clauses,		
						 int max_var,				
						 int cl_count,
						int firstVar,
						int lastVar)						
{																				
	int id = get_global_id(0);	
	int var_count = lastVar - firstVar + 1;
	int velikost = (int)pow(2.0f,(int)var_count);


	while(id<velikost){
		bool bits[32];
		intToBits(bits,id,var_count);

		int najdu = 1;
		for(int i = 0;i<cl_count;i++){
			int rez = 0;
			int numSkipped = 0;
			for(int j = 0;j<max_var;j++){
				int ele = clauses[i*max_var + j];
				if (ele == 0) {
					break;
				}
				int abs = ele;
				if(abs < 0){abs = -abs;}
				if(abs<firstVar || abs>lastVar){
					numSkipped++;
					continue;
				}

				if(ele<0){
					ele = ele*(-1);
					rez = rez | !bits[ele-firstVar];
				}else{
					rez = rez | bits[ele-firstVar];
				}
			}
			if(rez == 0){
				if(numSkipped >=1){
					continue;
				}
				najdu = 0;
				break;
			}
		}

		if(najdu == 1){
			printf("%d\n",id);
			break;
		}
		id+=get_global_size(0);
	}
	
}