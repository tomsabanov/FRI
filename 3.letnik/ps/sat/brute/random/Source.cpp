#include <stdio.h>
#include <stdlib.h>
#include <chrono>
#include <math.h>     
#include "Parser.h"
#include <CL/cl.h>
#include <vector>

#define CL_USE_DEPRECATED_OPENCL_1_2_APIS // cl.hpp
#if defined(__APPLE__) || defined(__MACOSX)
#include <OpenCL/cl.hpp>
#else
#include <CL/cl.hpp>
#endif
#include <chrono>
#include <random>
#include <iostream>

using cl::Buffer;
using cl::Program;
using cl::Platform;
using cl::Device;
using cl::Kernel;
using cl::Context;
using cl::CommandQueue;
using cl::Event;
using cl::NDRange;
using cl::NullRange;
using namespace std;

#define PLATFORM 0
#define DEVICE 0
#define GENERATOR_LOCATION "./RandomCL/generators/"
#define N_THREADS 128
#define NUM 2000

#define COMPILE_OPTS "-I " GENERATOR_LOCATION

#define SIZE			(1024)
#define WORKGROUP_SIZE	(1024)
#define MAX_SOURCE_SIZE	16384

using namespace std;

void testSolution(int* clauses,bool array[], int cl_count, int max_var) {
	int najdu = 1;
	for (int i = 0;i < cl_count;i++) {
		int rez = 0;
		for (int j = 0;j < max_var;j++) {
			int ele = clauses[i*max_var + j];
			if (ele == 0) {
				break;
			}
			if (ele < 0) {
				ele = ele * (-1);
				rez = rez | !array[ele - 1];
			}
			else {
				rez = rez | array[ele - 1];
			}
		}
		if (rez == 0) {
			najdu = 0;
			break;
		}
	}
	if (najdu)
		printf("Uredu resitev\n");
	else
		printf("Ni uredu resitev\n");
}
void testSolution(int* clauses,char *array, int cl_count, int max_var,int var_count) {
	bool arr[var_count];
	for(int i = 0; i<var_count; i++){
		if(array[i] == '0'){
			arr[i] = false;
		}
		else{
			arr[i] = true;
		}
	}
	testSolution(clauses,arr,cl_count,max_var);
}

int main(int argc, char** argv) {
    if(argc < 2){
        printf("Supply the file with data!\n");
        return 0;
    }
	char* input_file = argv[1];

	int* clauses = NULL;

	Parser p;
	p.parse(input_file);
	clauses = p.getTable();
	int cl_count = p.getClausesCount();
	int var_count = p.getVariableCount();
	int max_var = p.getMaxVariables();


	int* N = (int*)malloc(sizeof(int) * var_count);
	for(int i = 0; i<var_count;i++){
			double w = (double)(i+1)/(double)var_count;
			double up = 0.01;
			double down = 1 - pow(w,(double)i+1);
			double n = log(up)/log(down);
			N[i] = (int)n + 1;
			//printf("%d ",N[i]);
	}

	int* seed = (int*)malloc(sizeof(int) * SIZE * SIZE);
	long long rnd_init = chrono::system_clock::now().time_since_epoch().count();
	mt19937_64 generator(rnd_init);
	for (int i = 0; i < SIZE; i++) {
		seed[i] = generator();
	}


	//testSolution(clauses,"10010001010011101001", cl_count,max_var,var_count);
	
	char ch;
	int i;
	cl_int ret;

	FILE *fp;
	char *source_str;
	size_t source_size;
	int wkSize = WORKGROUP_SIZE;
	fp = fopen("kernel.cl", "r");
	if (!fp)
	{
		fprintf(stderr, ":-(#\n");
		exit(1);
	}
	source_str = (char*)malloc(MAX_SOURCE_SIZE);
	source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
	source_str[source_size] = '\0';
	fclose(fp);

	// Podatki o platformi
	cl_platform_id	platform_id[10];
	cl_uint			ret_num_platforms;
	char			*buf;
	size_t			buf_len;
	ret = clGetPlatformIDs(10, platform_id, &ret_num_platforms);
	// max. "stevilo platform, kazalec na platforme, dejansko "stevilo platform

// Podatki o napravi
	cl_device_id	device_id[10];
	cl_uint			ret_num_devices;
	// Delali bomo s platform_id[0] na GPU
	ret = clGetDeviceIDs(platform_id[0], CL_DEVICE_TYPE_GPU, 10,
		device_id, &ret_num_devices);
	// izbrana platforma, tip naprave, koliko naprav nas zanima
	// kazalec na naprave, dejansko "stevilo naprav

// Kontekst
	cl_context context = clCreateContext(NULL, 1, &device_id[0], NULL, NULL, &ret);
	// kontekst: vklju"cene platforme - NULL je privzeta, "stevilo naprav, 
	// kazalci na naprave, kazalec na call-back funkcijo v primeru napake
	// dodatni parametri funkcije, "stevilka napake

// Ukazna vrsta
	cl_command_queue command_queue = clCreateCommandQueue(context, device_id[0], 0, &ret);
	// kontekst, naprava, INORDER/OUTOFORDER, napake

// Delitev dela
	size_t local_item_size = WORKGROUP_SIZE;
	size_t num_groups = WORKGROUP_SIZE;
	size_t global_item_size = num_groups * local_item_size;


	cl_mem clauseMemObject = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		cl_count*max_var * sizeof(int), clauses, &ret);
	
	cl_mem NMemObject = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		var_count * sizeof(int), N, &ret);

	cl_mem SeedMemObject = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		SIZE *SIZE* sizeof(int), seed, &ret);

	// Priprava programa
	cl_program program = clCreateProgramWithSource(context, 1, (const char **)&source_str,
		NULL, &ret);
	// kontekst, "stevilo kazalcev na kodo, kazalci na kodo,		
	// stringi so NULL terminated, napaka													

// Prevajanje
	ret = clBuildProgram(program, 1, &device_id[0], NULL, NULL, NULL);
	// program, "stevilo naprav, lista naprav, opcije pri prevajanju,
	// kazalec na funkcijo, uporabni"ski argumenti

// Log
	size_t build_log_len;
	char *build_log;
	ret = clGetProgramBuildInfo(program, device_id[0], CL_PROGRAM_BUILD_LOG,
		0, NULL, &build_log_len);
	// program, "naprava, tip izpisa, 
	// maksimalna dol"zina niza, kazalec na niz, dejanska dol"zina niza
	build_log = (char *)malloc(sizeof(char)*(build_log_len + 1));
	ret = clGetProgramBuildInfo(program, device_id[0], CL_PROGRAM_BUILD_LOG,
		build_log_len, build_log, NULL);
	free(build_log);

	// "s"cepec: priprava objekta
	cl_kernel kernel = clCreateKernel(program, "random", &ret);
	// program, ime "s"cepca, napaka

	size_t buf_size_t;
	clGetKernelWorkGroupInfo(kernel, device_id[0], CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(buf_size_t), &buf_size_t, NULL);

	// "s"cepec: argumenti
	ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&clauseMemObject);
	ret |= clSetKernelArg(kernel, 1, sizeof(cl_int), (void *)&max_var);
	ret |= clSetKernelArg(kernel, 2, sizeof(cl_int), (void *)&cl_count);
	ret |= clSetKernelArg(kernel, 3, sizeof(cl_int), (void *)&var_count);
	ret = clSetKernelArg(kernel, 4, sizeof(cl_mem), (void *)&NMemObject);
	ret = clSetKernelArg(kernel, 5, sizeof(cl_mem), (void *)&SeedMemObject);

	// "s"cepec, "stevilka argumenta, velikost podatkov, kazalec na podatke


	ret = clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL,
		&global_item_size, &local_item_size, 0, NULL, NULL);
	// vrsta, "s"cepec, dimenzionalnost, mora biti NULL, 
	// kazalec na "stevilo vseh niti, kazalec na lokalno "stevilo niti, 
	// dogodki, ki se morajo zgoditi pred klicem


	// "ci"s"cenje
	ret = clFlush(command_queue);
	ret = clFinish(command_queue);
	ret = clReleaseKernel(kernel);
	ret = clReleaseProgram(program);
	ret = clReleaseCommandQueue(command_queue);
	ret = clReleaseContext(context);
	
}