#include <stdio.h>
#include <stdlib.h>
#include <chrono>
#include <time.h> 
#include "pool.h"
#include "Parser.h"

int main(void) {
	char* input_file = "test2.cnf";
	int* clauses = NULL;

	Parser p;
	p.parse(input_file);
	clauses = p.getTable();
	int cl_count = p.getClausesCount();
	int var_count = p.getVariableCount();
	int max_var = p.getMaxVariables();





    ThreadPool pool(1000);
    for(int i = 0; i<1000; i++){
        pool.enqueue([clauses,cl_count,var_count,max_var](int id) { 
            srand (time(NULL));

            int izberi = id%20 + 1;
            int iter = 0;
            int max = 0;
            while(iter < 1000){
                bool arr[20] = {false};

                for(int i = 0; i<izberi;){
                    int r = rand() % 20 + 1;
                    if(arr[r-1] == false){
                        arr[r-1] = true;
                        i++;
                    }
                }

                int najdu = 1;
                for(int i = 0;i<cl_count;i++){
                    int rez = 0;
                    for(int j = 0;j<max_var;j++){
                        int ele = clauses[i*max_var + j];
                        if(ele == 0){
                            break;
                        }
                        if(ele<0){
                            ele = ele*(-1);
                            rez = rez | !arr[ele-1];
                        }else{
                            rez = rez | arr[ele-1];
                        }
                    }
                    if(rez==0){
                        najdu = 0;
                        if(i>max){
                            max = i;
                        }
                        break;
                    }
                }
                if(najdu){
                    printf("Nasel resitev!\n");
                    for(int i = 0;i<var_count;i++)
                        printf("%d ",arr[i]);
                    printf("\n");
                    break;
                }
                iter++;
            }

            if(max > 80){
                printf("%d\n",max);
            }
            

        }, i);
    }

}