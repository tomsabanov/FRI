#include <tyche_i.cl>

__kernel void brute(__global const int *clauses,
						 int max_var,				
						 int cl_count,
						int var_count)						
{																				
	int id = get_global_id(0);	

	int seed = 27 + id;
	
	int izbranih = id%var_count + 1;

	int max = 0;
	int iter = 0;
	while(iter<10000){
		bool bits[128] = {false};
		//flipnemo izbrane
		int k = 0;
		for(int i = 0;i<izbranih && k < 500;){
			seed = (seed * 0x5DEECE66DL + 0xBL) & ((1L << 48) - 1);
			int result = seed >> 16;
			result = abs(result);
			result = result%var_count;
			if(bits[result]==false){
				bits[result]=true;
				i++;
			}
			k++;
		}
		
		//check loop
		int najdu = 1;
		for(int i = 0;i<cl_count;i++){
			int rez = 0;
			for(int j = 0;j<max_var;j++){
				int ele = clauses[i*max_var + j];
				if(ele == 0){
					break;
				}
				if(ele<0){
					ele = ele*(-1);
					rez = rez | !bits[ele-1];
				}else{
					rez = rez | bits[ele-1];
				}
			}
			if(rez==0){
				najdu = 0;
				if(i > max){
					max = i;
				}
				break;
			}
		}
		if(najdu){
			// resitev v char array
			char res[128];
			res[var_count] = '\0';
			for(int i = 0; i<var_count; i++){
				if(bits[i]){
					res[i]='1';
				}
				else{
					res[i]='0';
				}
			}
			printf("%s\n",res);
			break;
		}
		iter++;
	}

	if(max > 80){
		printf("%d\n",max);
	}
}