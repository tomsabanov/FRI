void intToBits(int array[], int stevilo, int bits) {
	for (int i = 0; i < bits; i++) {
		array[i] = (stevilo >> i) & 1;
	}
}
bool testSolution(int* clauses,bool array[], int cl_count, int max_var) {
	int najdu = 1;
	for (int i = 0;i < cl_count;i++) {
		bool rez = false;
		for (int j = 0;j < max_var;j++) {
			int ele = clauses[i*max_var + j];
			if (ele < 0) {
				ele = ele * (-1);
				rez = rez | !array[ele - 1];
			}
			else {
				rez = rez | array[ele - 1];
			}
		}
		if (rez == false) {
			najdu = 0;
			if(i>100){
				printf("Pri clenu %d/%d : %d %d %d\n",i,cl_count,clauses[i*max_var],
												clauses[i*max_var + 1],clauses[i*max_var+2]);
			}
			break;
		}
	}
	
	if (najdu == 1){
		printf("Pravilna resitev\n");
        return true;
	}

    //printf("Napacna resitev\n");
    return false;
}

bool testSolution(int* clauses,char* array,int bits,int cl_count, int max_var) {
	bool* arr = (bool*)malloc(sizeof(bool) * bits);
	for(int i = 0; i<bits;i++){
		if(array[i] == '0'){
			arr[i] = false;
		}
		else{
			arr[i] = true;
		}
	}
	bool b = testSolution(clauses,arr,cl_count,max_var);
	free(arr);
	return b;
}