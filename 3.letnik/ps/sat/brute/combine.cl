int test(int clauses[],int array[], int cl_count, int max_var) {
	int najdu = 1;
    int problem_cl = 0;
	for (int i = 0;i < cl_count;i++) {
		int rez = 0;
		for (int j = 0;j < max_var;j++) {
			int ele = clauses[i*max_var + j];
			if (ele < 0) {
				ele = ele * (-1);
				rez = rez | !array[ele - 1];
			}
			else {
				rez = rez | array[ele - 1];
			}
		}
		if (rez == 0) {
			najdu = 0;
            problem_cl = i;
			break;
		}
	}
	
	if (najdu){
        return -1;
	}
    return problem_cl;
}

int testSolution(char p1_solutions[],char p2_solutions[], int clauses[], int cl_count,int max_var){
    int s[50];
    for(int i =0; i<25;i++){
        if(p1_solutions[i]=='0'){
            s[i] = 0;
        }
        else{
            s[i] = 1;
        }
    }
    for(int i =25; i<50;i++){
        if(p2_solutions[i]=='0'){
            s[i] = 0;
        }
        else{
            s[i] = 1;
        }
    }
    return test(clauses,s,cl_count,max_var);
}
int testSolution2(char p[],int clauses[],int cl_count, int max_var){
    int s[50];
    for(int i =0; i<50;i++){
        if(p[i]=='0'){
            s[i] = 0;
        }
        else{
            s[i] = 1;
        }
    }
    return test(clauses,s,cl_count,max_var);
}


__kernel void combine(__global const int *clauses,		
						 int max_var,				
						 int cl_count,
						int p1_firstVar,
						int p1_lastVar,
                        int p1_bits,
                        int p1_numOfSol,
                        __global const char* p1_solutions,
                        int p2_firstVar,
						int p2_lastVar,
                        int p2_bits,
                        int p2_numOfSol,
                        __global const char* p2_solutions)						
{																					
	int id = get_global_id(0);	

    // select bigger subspace
    int selected = 1; // 1:p1, 2:p2
    if(p2_bits>p1_bits){
        selected = 2;
    }


    // calculate space
	int velikost = (int)pow(2.0,(int)p1_bits);
    if(selected==2){
        velikost = (int)pow(2.0,(int)p2_bits);        
    }


    // poglej ce je id vecji od prostora
    if(id>=velikost){
        return;
    }

    // izberi resitev, ki jo bomo kombinirali z ostalimi iz manjsega podprostora
    int numOfSol_sel = p1_numOfSol;
    int bits_sel = p1_bits;
    int numOfSol_tmp = p2_numOfSol;
    int bits_tmp = p2_bits;
    if(selected==2){
        numOfSol_sel = p2_numOfSol;
        bits_sel = p2_bits;
        numOfSol_tmp = p1_numOfSol;
        bits_tmp = p1_bits;
    }

    int p_min = p1_firstVar;
    int p_max = p2_lastVar;
    if(p1_firstVar>p2_firstVar){
        p_min = p2_firstVar;
        p_max = p1_lastVar;
    }    

    /*
    if(id == 0){
        printf("P1 first-last var is %d-%d\n",p1_firstVar,p1_lastVar);
        printf("P2 first-last var is %d-%d\n",p2_firstVar,p2_lastVar);
        printf("P_min:%d , P_max:%d\n",p_min,p_max);
        printf("P1-P2 num of sol %d-%d\n",p1_numOfSol,p2_numOfSol);
    }
    */
    
    
    // poglej ce je id vecji od stevila resitev v izbrani mnozici
    if(id>=numOfSol_sel){
        return;
    }
    

    int startSel = id * bits_sel;
    // pojdi cez vsako kombinacijo 
    
    for(int k = 0; k<numOfSol_tmp; k++){
        
		int najdu = 1;
		for(int i = 0;i<cl_count;i++){
			int rez = 0;
			int numSkipped = 0;
			for(int j = 0;j<max_var;j++){
				int ele = clauses[i*max_var + j];
	
				int abs = ele;
				if(abs < 0){abs = -abs;}
                // preveri ce je abs < ali > od min in max skupnega prostora
				if(abs<p_min || abs>p_max){
					numSkipped++;
					continue;
				}
      
                // doloci, v katero mnozico spada ele
                bool p_ele = true; // true: p1, false: p2
                if(ele>=p2_firstVar){
                    if(ele<=p2_lastVar){
                        p_ele = false;
                    }
                }

                if(p_ele == true){
                    if(selected == 1){
                        int A = 1;
                        if(p1_solutions[startSel + abs - p1_firstVar] == '0'){
                            A = 0;
                        }

                        if(ele<0){
                            rez = rez | !A;
                        }else{
                            rez = rez | A;
                        }
                    }
                    else{
                        
                        int A = 1;
                        if(p1_solutions[k*bits_tmp + abs - p1_firstVar] == '0'){
                            A = 0;
                        }
                        if(ele<0){
                            rez = rez | !A;
                        }else{
                            rez = rez | A;
                        }
                        
                    }
                }
                else{
                    if(selected == 1){
                        int A = 1;
                        if(p2_solutions[k*bits_tmp + abs - p2_firstVar] == '0'){
                            A = 0;
                        }
                        if(ele<0){
                            rez = rez | !A;
                        }else{
                            rez = rez | A;
                        }
                    }
                    else{
                        int A = 1;
                        if(p2_solutions[startSel + abs - p2_firstVar] == '0'){
                            A = 0;
                        }
                        if(ele<0){
                            rez = rez | !A;
                        }else{
                            rez = rez | A;
                        } 
                    }
                }

               
			}

			if(rez==0){
				if(numSkipped >= 1){
					rez = 1;
					continue;
				}
                	
				najdu = 0;
				break;
			}
		}

		if(najdu){ 
            printf("%d %d\n",id,k);
            break;
		}
    }
    
}
