
// Client side C/C++ program to demonstrate Socket programming 
#include <stdio.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <unistd.h> 
#include <string.h> 
#include <thread>
#include <vector>

#define PORT 1053
   
  
void sendToServer(int sock, int i){
    char *hello = "Hello from "; 
    char *msg = (char*)malloc(sizeof(char) * strlen(hello) + 2 );
    char tmp[2];
    tmp[0] = i + '0';
    tmp[1] = '\0';

    strcpy(msg, hello);
    strcat(msg, tmp);

    msg[strlen(hello) -1] = ' ';
    

    int valread;

    send(sock , msg , strlen(msg) , 0 ); 
    printf("Sent message: %s \n", msg); 
    
    char buffer[1024];
    valread = read( sock , buffer, 1024);
    if(valread == 0){
        printf("Server did not send a reply for %d \n", i);
    }
    else{
        printf("Received message: %s \n",buffer ); 
    }

}

int main(int argc, char const *argv[]) 
{ 
    int i = 0;
    int num_clients = 10;
    if(argc == 2){
        num_clients = atoi(argv[1]);
    }
    else{
        num_clients = 10;
    }

    std::vector<std::thread> threads;
    while(i < num_clients){
  
        int sock = 0, valread; 
        struct sockaddr_in serv_addr; 


        if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) { 
            printf("\n Socket creation error \n"); 
            return -1; 
        } 
        
        serv_addr.sin_family = AF_INET; 
        serv_addr.sin_port = htons(PORT); 
            
        // Convert IPv4 and IPv6 addresses from text to binary form 
        if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0){ 
                printf("\nInvalid address/ Address not supported \n"); 
                return -1; 
        } 
        
        if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) { 
                printf("\nConnection Failed \n"); 
                return -1; 
        } 

        std::thread t(sendToServer,sock, i);
        threads.push_back(std::move(t));

        i++;
    }

    std::vector<std::thread>::iterator it;
    for(it = threads.begin(); it != threads.end(); it ++){
        it->join();
    }

    return 0; 
} 
