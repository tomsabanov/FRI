/*H********************************************************************************
* Ime datoteke: serverLinux.cpp
*
* Opis:
*		Enostaven streznik, ki zmore sprejeti le enega klienta naenkrat.
*		Streznik sprejme klientove podatke in jih v nespremenjeni obliki poslje
*		nazaj klientu - odmev.
*
*H*/

//Vkljucimo ustrezna zaglavja
#include<stdio.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<unistd.h>

#include <thread>
#include <vector>
#include <mutex>

/*
Definiramo vrata (port) na katerem bo streznik poslu�al
in velikost medponilnika za sprejemanje in posiljanje podatkov
*/
#define PORT 1053
#define BUFFER_SIZE 256

using namespace std;

std::mutex mtx;          

void serve(int clientSock, int* current_num_of_clients){
		
		//Spremenjlivka za preverjane izhodnega statusa funkcij
		int iResult;

		//Definiramo nov vtic in medpomnilik
		char buff[BUFFER_SIZE];

		do{
			//Sprejmi podatke
			iResult = recv(clientSock, buff, BUFFER_SIZE, 0);
			if (iResult > 0) {
				printf("Bytes received: %d, Message : %s\n", iResult, buff);

				//Vrni prejete podatke po�iljatelju
				iResult = send(clientSock, buff, iResult, 0 );
				if (iResult == -1) {
					printf("send failed!\n");
					break;
				}
				printf("Bytes sent: %d\n", iResult);
			}
			else if (iResult == 0){
				printf("Connection closing...\n");
				break;
			}
			else{
				printf("recv failed!\n");
				break;
			}

		} while (iResult > 0);
		close(clientSock);	
		
		bool  b= mtx.try_lock();
		while(b == false){
			b = mtx.try_lock();
		}
		*current_num_of_clients = *current_num_of_clients - 1;
		mtx.unlock();
}

int main(int argc, char **argv){

	/*
	Ustvarimo nov vtic, ki bo poslu�al
	in sprejemal nove kliente preko TCP/IP protokola
	*/

	int MAX_NUM_OF_CLIENTS;
	if(argc == 2){
		MAX_NUM_OF_CLIENTS = atoi(argv[1]);
	}
	else{
		MAX_NUM_OF_CLIENTS = 5;
	}

	

	int iResult;
	int listener=socket(AF_INET, SOCK_STREAM, 0);
	if (listener == -1) {
		printf("Error creating socket\n");
		return 1;
	}

	//Nastavimo vrata in mre�ni naslov vti�a
	sockaddr_in  listenerConf;
	listenerConf.sin_port=htons(PORT);
	listenerConf.sin_family=AF_INET;
	listenerConf.sin_addr.s_addr=INADDR_ANY;

	//Vti� pove�emo z ustreznimi vrati
	iResult = bind( listener, (sockaddr *)&listenerConf, sizeof(listenerConf));
	if (iResult == -1) {
		printf("Bind failed\n");
		close(listener);
		return 1;
    }

	//Za�nemo poslu�ati
	if ( listen( listener, 5 ) == -1 ) {
		printf( "Listen failed\n");
		close(listener);
		return 1;
	}


	
	/*
	V zanki sprejemamo nove povezave
	in jih stre�emo (najve� eno naenkrat)
	*/

	int CURRENT_NUM_OF_CLIENTS = 0;
	while (1)
	{
		//Sprejmi povezavo in ustvari nov vti�
		int clientSock = accept(listener,NULL,NULL);

		bool b = mtx.try_lock();
		while(b == false){  
			b = mtx.try_lock();
		}
		if(CURRENT_NUM_OF_CLIENTS == MAX_NUM_OF_CLIENTS){
			printf("Server is handling max number of clients %d/%d\n", CURRENT_NUM_OF_CLIENTS, MAX_NUM_OF_CLIENTS );
			close(clientSock);
			continue;
		}
		CURRENT_NUM_OF_CLIENTS++;
		mtx.unlock();

		if (clientSock == -1) {
			printf("Accept failed\n");
			close(listener);
			return 1;
		}

		//Postrezi povezanemu klientu
		std::thread t(serve, clientSock, &CURRENT_NUM_OF_CLIENTS);
		t.detach();
	}

	//Po�istimo vse vti�e
	close(listener);

	return 0;
}
