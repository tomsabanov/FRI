#include <stdio.h> 
#include <string.h> 
#include <thread>
#include <vector>
#include <cmath>
#include <iostream>
#include <mutex>
#include <chrono> 


using namespace std; 
using namespace std::chrono; 

// Dinamicno, ustvarimo threade, ki jemlejo 
// intervale stevil

long* NUMBERS;
long START=1;
long INTERVAL=100;
mutex mtx;

long getFriendNumber(long x){ 
    long sum = 1; 
    for (long i = 2; i <= sqrt(x); i++) { 
        if (x % i == 0){ 
            sum += i; 
            if (x / i != i) sum += x / i; 
        } 
    } 
    return sum; 
} 
  
void sumNumbers(long N){
    while(1){
        if(START > N) break;
		bool  b= mtx.try_lock();
		while(b == false){
			b = mtx.try_lock();
		}
        long start = START;
        long end = start + INTERVAL;
        START = end + 1;
        mtx.unlock();

        for(long i = start; i <= end; i++){
            if(i > N) break;
            long friendNum = getFriendNumber(i);
            // rabimo pogledat, ce je v nasem intervalu [1-N] in kje se nahaja 
            // relativno nasega intervala start-end
            if(friendNum > N) continue;
            if(friendNum < i) continue;
            if(friendNum == i) continue;

            long reverse = getFriendNumber(friendNum);
            if(getFriendNumber(friendNum) == i ){
                NUMBERS[i] = i;
                NUMBERS[friendNum] = friendNum;
                //printf("%d and %d are friends\n",i, friendNum);
            }
        }
    }
} 
 
int main(int argc, char** argv){ 
    // first argument : N
    // second argument : THREADS
    // third argument : size of intervals threads are going to take
    
    // Get starting timepoint 
    auto start_time = high_resolution_clock::now(); 

    if(argc < 2) return 0;

    long N = 1;
    long THREADS = 2;
    if(argc == 2) N = atoi(argv[1]);
    if(argc == 3){
        N = atoi(argv[1]);
        THREADS = atoi(argv[2]);
    }
    if(argc == 4){
        N = atoi(argv[1]);
        THREADS = atoi(argv[2]);
        INTERVAL = atoi(argv[3]);
    }
    NUMBERS = (long *)calloc(sizeof(long), N);

    vector<thread> threads;
    long start = 1;
    for(long i = 0; i < THREADS; i++){
		thread t(sumNumbers, N);
        threads.push_back(move(t));
    }

    for(long i = 0; i < THREADS; i++){
        threads.at(i).join();
    }

    long sum = 0;
    printf("Prijatelska stevila : \n");
    for(long i = 0; i < N; i++){
        if(NUMBERS[i] == 0)continue;
        sum += NUMBERS[i];
        printf("%ld\n", NUMBERS[i]);
    }

    printf("Sum is : %ld \n", sum);

    // Get ending timepoint 
    auto stop = high_resolution_clock::now(); 
  
    // Get duration. Substart timepoints to  
    // get durarion. To cast it to proper unit 
    // use duration cast method 
    auto duration = duration_cast<microseconds>(stop - start_time); 
  
    cout << "Time taken by function: "
         << duration.count() << " microseconds" << endl; 

    free(NUMBERS);

    return 0; 
} 