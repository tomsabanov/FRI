
##### Ping in trace med PC1 in PC2 pred prekinitvijo R1-R2 povezave
PC1> ping 192.168.7.100
192.168.7.100 icmp_seq=1 timeout
192.168.7.100 icmp_seq=2 timeout
84 bytes from 192.168.7.100 icmp_seq=3 ttl=62 time=3.319 ms
84 bytes from 192.168.7.100 icmp_seq=4 ttl=62 time=4.863 ms
84 bytes from 192.168.7.100 icmp_seq=5 ttl=62 time=3.675 ms


PC1> trace 192.168.7.100
trace to 192.168.7.100, 8 hops max, press Ctrl+C to stop
 1   10.2.0.1   1.644 ms  1.799 ms  0.667 ms
 2   1.1.1.66   3.861 ms  3.186 ms  3.134 ms
 3   *192.168.7.100   4.629 ms (ICMP type:3, code:3, Destination port unreachable)


##### Ping in trace med PC1 in PC2 po prekinitviji R1-R2 povezave
PC1> ping 192.168.7.100
84 bytes from 192.168.7.100 icmp_seq=1 ttl=61 time=6.872 ms
84 bytes from 192.168.7.100 icmp_seq=2 ttl=61 time=4.990 ms
84 bytes from 192.168.7.100 icmp_seq=3 ttl=61 time=5.338 ms
84 bytes from 192.168.7.100 icmp_seq=4 ttl=61 time=5.891 ms
84 bytes from 192.168.7.100 icmp_seq=5 ttl=61 time=5.121 ms

PC1> trace 192.168.7.100     
trace to 192.168.7.100, 8 hops max, press Ctrl+C to stop
 1   10.2.0.1   1.282 ms  1.289 ms  1.007 ms
 2   1.1.1.2   3.357 ms  2.966 ms  2.937 ms
 3   1.1.1.129   6.492 ms  5.879 ms  4.788 ms
 4   *192.168.7.100   4.104 ms (ICMP type:3, code:3, Destination port unreachable)


### Posredovalna tabela R1
   Network          Next Hop            Metric LocPrf Weight Path
*> 10.2.0.0/24      0.0.0.0                  1         32768 i
*  172.16.2.0/24    1.1.1.66                               0 65532 65533 i
*>                  1.1.1.2                  1             0 65533 i
*> 192.168.7.0      1.1.1.66                 1             0 65532 i
*                   1.1.1.2                                0 65533 65532 i

### Posredovalna tabela R2
   Network          Next Hop            Metric LocPrf Weight Path
*> 10.2.0.0/24      1.1.1.65                 1             0 65531 i
*                   1.1.1.130                              0 65533 65531 i
*  172.16.2.0/24    1.1.1.65                               0 65531 65533 i
*>                  1.1.1.130                1             0 65533 i
*> 192.168.7.0      0.0.0.0                  1         32768 i

### Posredovalna tabela R3
   Network          Next Hop            Metric LocPrf Weight Path
*  10.2.0.0/24      1.1.1.129                              0 65532 65531 i
*>                  1.1.1.1                  1             0 65531 i
*> 172.16.2.0/24    0.0.0.0                  1         32768 i
*  192.168.7.0      1.1.1.1                                0 65531 65532 i
*>                  1.1.1.129                1             0 65532 i


####### Ukazi PC1
set pcname PC1
ip 10.2.0.100 /24 10.2.0.1

####### Ukazi PC2
set pcname PC2
ip 192.168.7.100 /24 192.168.7.1

####### Ukazi PC3
set pcname PC3
ip  172.16.2.100 /24 172.16.2.1


###### Ukazi R1

set interfaces ethernet eth0 address 10.2.0.1/24
set interfaces ethernet eth1 address 1.1.1.65/26
set interfaces ethernet eth2 address 1.1.1.1/26

set protocols bgp 65531 neighbor 1.1.1.66 remote-as '65532'
set protocols bgp 65531 neighbor 1.1.1.66 update-source 1.1.1.65

set protocols bgp 65531 neighbor 1.1.1.2 remote-as '65533'
set protocols bgp 65531 neighbor 1.1.1.2 update-source 1.1.1.1

set protocols bgp 65531 parameters router-id 10.2.0.1
set protocols bgp 65531 network 10.2.0.0/24

commit,save,exit

###### Ukazi R2

set interfaces ethernet eth0 address 192.168.7.1/24
set interfaces ethernet eth1 address 1.1.1.66/26
set interfaces ethernet eth2 address 1.1.1.129/26

set protocols bgp 65532 neighbor 1.1.1.65 remote-as '65531'
set protocols bgp 65532 neighbor 1.1.1.65 update-source 1.1.1.66

set protocols bgp 65532 neighbor 1.1.1.130 remote-as '65533'
set protocols bgp 65532 neighbor 1.1.1.130 update-source 1.1.1.129

set protocols bgp 65532 parameters router-id 192.168.7.1
set protocols bgp 65532 network 192.168.7.0/24

commit,save,exit

###### Ukazi R3

set interfaces ethernet eth0 address 172.16.2.0/24
set interfaces ethernet eth1 address 1.1.1.2/26
set interfaces ethernet eth2 address 1.1.1.130/26

set protocols bgp 65533 neighbor 1.1.1.1 remote-as '65531'
set protocols bgp 65533 neighbor 1.1.1.1 update-source 1.1.1.2

set protocols bgp 65533 neighbor 1.1.1.129 remote-as '65532'
set protocols bgp 65533 neighbor 1.1.1.129 update-source 1.1.1.130

set protocols bgp 65533 parameters router-id 172.16.2.1
set protocols bgp 65533 network 172.16.2.1/24

commit,save,exit
