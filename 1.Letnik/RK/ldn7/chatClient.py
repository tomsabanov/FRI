import socket
import struct
import sys
import threading
import json
from time import ctime,time

PORT = 1234
HEADER_LENGTH = 2

my_username=''

def createMessageObject(msg_type, message, sendTo):
    t = ctime(time())
    msg = {
        'type': msg_type,
        'from':my_username,
        'to':sendTo,
        'msg':message,
        'time':t
    }
    return msg

def registerUser(sock):
    # Gegistriraj svoje ime na strezniku
    global my_username
    my_username = input("Vnesi svoje uporabnisko ime: ")

    msg = createMessageObject("REGISTRATION",None,None)
    send_message(sock,msg)

    response = receive_message(sock)
    return response


def receive_fixed_length_msg(sock, msglen):
    message = b''
    while len(message) < msglen:
        chunk = sock.recv(msglen - len(message))  # preberi nekaj bajtov
        if chunk == b'':
            raise RuntimeError("socket connection broken")
        message = message + chunk  # pripni prebrane bajte sporocilu

    return message


def receive_message(sock):
    header = receive_fixed_length_msg(sock,
                                      HEADER_LENGTH)  # preberi glavo sporocila (v prvih 2 bytih je dolzina sporocila)
    message_length = struct.unpack("!H", header)[0]  # pretvori dolzino sporocila v int

    message = None
    if message_length > 0:  # ce je vse OK
        message = receive_fixed_length_msg(sock, message_length)  # preberi sporocilo
        message = message.decode("utf-8")
        message = json.loads(message)

    return message


def send_message(sock, message):
    encoded_message = json.dumps(message)  # pretvori sporocilo v niz bajtov, uporabi UTF-8 kodno tabelo
    encoded_message = encoded_message.encode("utf-8")

    # ustvari glavo v prvih 2 bytih je dolzina sporocila (HEADER_LENGTH)
    # metoda pack "!H" : !=network byte order, H=unsigned short
    header = struct.pack("!H", len(encoded_message))

    message = header + encoded_message  # najprj posljemo dolzino sporocilo, slee nato sporocilo samo
    sock.sendall(message)
    return True


# message_receiver funkcija tece v loceni niti
def message_receiver():
    while True:
        msg = receive_message(sock)
        # Na podlagi msg objekta izpisemo public/private message
        if msg is None:
            continue
        
        fr = msg["from"]
        to = msg["to"]
        t = msg["time"]
        m = msg["msg"]

        if(msg['type'] == "PUBLIC"):
            print(t + " [RKchat]["+fr+"]: " + m)
        elif(msg['type'] == "PRIVATE"):
            print(t + " [Private]["+fr+"]: " + m)
        else:
            print(t + " [System]:" + m)
           



# povezi se na streznik
print("[system] connecting to chat server ...")
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect(("localhost", PORT))
print("[system] connected!")


response = registerUser(sock)
if(response['status'] == 'FAILED'):
    print("Got error: " + response['error'])
    exit()


# zazeni message_receiver funkcijo v loceni niti
thread = threading.Thread(target=message_receiver)
thread.daemon = True
thread.start()



# pocakaj da uporabnik nekaj natipka in poslji na streznik
while True:
    try:
        text = input("")

        # Ustvari msg objekt glede na msg_send

        # public and private messages
        # !msg 'username' message to send to 'username'
        # normal public message with whatever....

        split = text.split()
        obj = None
        if(split[0] == '!msg'):
            to = split[1]
            split = split[2:]
            msg = " ".join(split)
            obj = createMessageObject("PRIVATE",msg,to)
        else:
            obj = createMessageObject("PUBLIC",text,None)



        send_message(sock, obj)
    except KeyboardInterrupt:
        sys.exit()
