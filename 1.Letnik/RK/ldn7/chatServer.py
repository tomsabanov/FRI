import signal

signal.signal(signal.SIGINT, signal.SIG_DFL)
import socket
import struct
import threading
import json
from time import ctime,time

PORT = 1234
HEADER_LENGTH = 2



def createMessageObject(msg_type, message, sendTo):
    t = ctime(time())

    msg = {
        'type': msg_type,
        'from':"SYSTEM",
        'to':sendTo,
        'msg':message,
        'time':t
    }
    return msg

def receive_fixed_length_msg(sock, msglen):
    message = b''
    while len(message) < msglen:
        chunk = sock.recv(msglen - len(message))  # preberi nekaj bajtov
        if chunk == b'':
            raise RuntimeError("socket connection broken")
        message = message + chunk  # pripni prebrane bajte sporocilu

    return message


def receive_message(sock):
    header = receive_fixed_length_msg(sock,
                                      HEADER_LENGTH)  # preberi glavo sporocila (v prvih 2 bytih je dolzina sporocila)
    message_length = struct.unpack("!H", header)[0]  # pretvori dolzino sporocila v int

    message = None
    if message_length > 0:  # ce je vse OK
        message = receive_fixed_length_msg(sock, message_length)  # preberi sporocilo
        message = message.decode("utf-8")
        message = json.loads(message)

    return message


def send_message(sock, message):
    encoded_message = json.dumps(message)
    encoded_message = encoded_message.encode("utf-8")  # pretvori sporocilo v niz bajtov, uporabi UTF-8 kodno tabelo

    # ustvari glavo v prvih 2 bytih je dolzina sporocila (HEADER_LENGTH)
    # metoda pack "!H" : !=network byte order, H=unsigned short
    header = struct.pack("!H", len(encoded_message))

    message = header + encoded_message  # najprj posljemo dolzino sporocilo, slee nato sporocilo samo
    sock.sendall(message)


# funkcija za komunikacijo z odjemalcem (tece v loceni niti za vsakega odjemalca)
def client_thread(client_sock,id,client_addr):
    global clients

    print("[system] connected with " + client_addr[0] + ":" + str(client_addr[1]))
    print("[system] we now have " + str(len(clients)) + " clients")

    try:

        while True:  # neskoncna zanka
            msg = receive_message(client_sock)

            # Preveri ali se gre za registracijo
            if(msg['type'] == "REGISTRATION"):
                # Preveri, ali uporabnisko ime ze obstaja
                exists = False
                for client in clients:
                    if(client['id'] != id and client['username'] == msg['from']):
                        exists = True

                res = {
                    'status':'SUCCESS',
                    'error': ''
                }                
                if(exists == True):
                    res['status'] = 'FAILED'
                    res['error'] = 'User ' + msg['from'] + ' already exists'
                else:
                    clients[id]['username'] = msg['from']
                
                send_message(client_sock,res)
                continue


            if(msg['type'] == "PUBLIC"):
                for client in clients:
                    send_message(client['sock'], msg)     
                    print(msg['time'] + "[RKchat]["+msg['from']+"]:" + msg['msg'])
           
            else:
                to = msg['to']
                found = False
                for client in clients:
                    if(client['username'] == to):
                        send_message(client['sock'], msg)    
                        found = True
                        break

                if(found == False):
                    err = "Message was not delivered to " + msg['to'] + ". User does not exist!"
                    obj = createMessageObject("SYSTEM",err,clients[id]["username"])
                    send_message(client_sock,obj)
                else:
                    f = msg['from']
                    to = msg['to']
                    t = msg['time']
                    m = msg['msg']
                    print(t + "["+f+"]["+to+"]:" + m)
    

    except:
        # do izjeme pride naceloma le ce se povezava prekini
        pass

    # prisli smo iz neskoncne zanke
    with clients_lock:
        clients.remove(clients[id])
    print("[system] we now have " + str(len(clients)) + " clients")
    client_sock.close()




# kreiraj socket
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind(("localhost", PORT))
server_socket.listen(1)

# cakaj na nove odjemalce
print("[system] listening ...")
clients = list()
clients_lock = threading.Lock()
while True:
    try:
        # pocakaj na novo povezavo - blokirajoc klic
        client_sock, client_addr = server_socket.accept()
        with clients_lock:
            id = len(clients)
            client = {
                'id':id,
                'username':'',
                'sock':client_sock
            }
            clients.append(client)

        thread = threading.Thread(target=client_thread, args=(client_sock, id, client_addr))
        thread.daemon = True
        thread.start()

    except KeyboardInterrupt:
        break

print("[system] closing server socket ...")
server_socket.close()
