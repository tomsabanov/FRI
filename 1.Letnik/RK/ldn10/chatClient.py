import socket
import struct
import sys
import threading
import json
from time import ctime,time
import ssl

PORT = 1234
HEADER_LENGTH = 2

my_username=''

def setup_SSL_context():
    hosts = ["Micka","Lojze","Janez"]
    if len(sys.argv) == 1:
        return None

    global my_username
    my_username = str(sys.argv[1])

    if my_username not in hosts:
        return None

    #uporabi samo TLS, ne SSL
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    # certifikat je obvezen
    context.verify_mode = ssl.CERT_REQUIRED
    #nalozi svoje certifikate
    certf = my_username.lower()+".crt"
    keyf = my_username.lower()+".key"

    context.load_cert_chain(certfile=certf, keyfile=keyf)
    # nalozi certifikate CAjev (samopodp. cert.= svoja CA!)
    context.load_verify_locations('server.pem')
    # nastavi SSL CipherSuites (nacin kriptiranja)
    context.set_ciphers('ECDHE-RSA-AES128-GCM-SHA256')
    return context

def createMessageObject(msg_type, message, sendTo):
    # we don't need a from key anymore... server will recognize by Common Name from socket connection
    t = ctime(time())
    msg = {
        'type': msg_type,
        'to':sendTo,
        'msg':message,
        'time':t
    }
    return msg

def receive_fixed_length_msg(sock, msglen):
    message = b''
    while len(message) < msglen:
        chunk = sock.recv(msglen - len(message))  # preberi nekaj bajtov
        if chunk == b'':
            raise RuntimeError("socket connection broken")
        message = message + chunk  # pripni prebrane bajte sporocilu

    return message


def receive_message(sock):
    header = receive_fixed_length_msg(sock,
                                      HEADER_LENGTH)  # preberi glavo sporocila (v prvih 2 bytih je dolzina sporocila)
    message_length = struct.unpack("!H", header)[0]  # pretvori dolzino sporocila v int

    message = None
    if message_length > 0:  # ce je vse OK
        message = receive_fixed_length_msg(sock, message_length)  # preberi sporocilo
        message = message.decode("utf-8")
        message = json.loads(message)

    return message


def send_message(sock, message):
    encoded_message = json.dumps(message)  # pretvori sporocilo v niz bajtov, uporabi UTF-8 kodno tabelo
    encoded_message = encoded_message.encode("utf-8")

    # ustvari glavo v prvih 2 bytih je dolzina sporocila (HEADER_LENGTH)
    # metoda pack "!H" : !=network byte order, H=unsigned short
    header = struct.pack("!H", len(encoded_message))

    message = header + encoded_message  # najprj posljemo dolzino sporocilo, slee nato sporocilo samo
    sock.sendall(message)
    return True


# message_receiver funkcija tece v loceni niti
def message_receiver():
    while True:
        msg = receive_message(sock)
        # Na podlagi msg objekta izpisemo public/private message
        if msg is None:
            continue
        
        fr = msg["from"]
        to = msg["to"]
        t = msg["time"]
        m = msg["msg"]

        if(msg['type'] == "PUBLIC"):
            print(t + " [RKchat]["+fr+"]: " + m)
        elif(msg['type'] == "PRIVATE"):
            print(t + " [Private]["+fr+"]: " + m)
        else:
            print(t + " [System]:" + m)
           


my_ssl_ctx = setup_SSL_context()

if my_ssl_ctx is None:
    print("Host is not micka,janez or lojze")
    exit()

# povezi se na streznik
print("[system] connecting to chat server ...")
sock = my_ssl_ctx.wrap_socket(socket.socket(socket.AF_INET, socket.SOCK_STREAM))
sock.connect(("localhost", PORT))
print("[system] connected!")


# zazeni message_receiver funkcijo v loceni niti
thread = threading.Thread(target=message_receiver)
thread.daemon = True
thread.start()



# pocakaj da uporabnik nekaj natipka in poslji na streznik
while True:
    try:
        text = input("")

        # Ustvari msg objekt glede na msg_send

        # public and private messages
        # !msg 'username' message to send to 'username'
        # normal public message with whatever....

        split = text.split()
        obj = None
        if(split[0] == '!msg'):
            to = split[1]
            split = split[2:]
            msg = " ".join(split)
            obj = createMessageObject("PRIVATE",msg,to)
        else:
            obj = createMessageObject("PUBLIC",text,None)



        send_message(sock, obj)
    except KeyboardInterrupt:
        sys.exit()
