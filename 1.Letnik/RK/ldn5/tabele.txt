########################### PC1 
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
default         10.0.0.1        0.0.0.0         UG    0      0        0 eth0
127.0.0.1       *               255.255.255.255 UH    0      0        0 lo

Kernel IPv6 routing table
Destination                                 Next Hop                                Flags Metric Ref    Use Iface
2001:db8:e::/64                             ::                                      U     256    1        0 eth0    
fe80::/64                                   ::                                      U     256    0        0 eth0    
::/0                                        2001:db8:e::1                           UG    1024   0        0 eth0    
::1/128                                     ::                                      U     0      0        1 lo      
2001:db8:e::2/128                           ::                                      U     0      77       1 lo      
fe80::e88:2ff:fea6:dd00/128                 ::                                      U     0      20       1 lo      
ff00::/8                                    ::                                      U     256    0        0 eth0    


########################### PC2 

Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
default         10.0.2.1        0.0.0.0         UG    0      0        0 eth0
127.0.0.1       *               255.255.255.255 UH    0      0        0 lo

Kernel IPv6 routing table
Destination                                 Next Hop                                Flags Metric Ref    Use Iface
2001:db8:2:f::/64                           ::                                      U     256    0        0 eth0    
fe80::/64                                   ::                                      U     256    0        0 eth0    
::/0                                        2001:db8:2:f::1                         UG    1024   0        0 eth0    
::1/128                                     ::                                      U     0      0        1 lo      
2001:db8:2:f::2/128                         ::                                      U     0      41       1 lo      
fe80::e88:2ff:fedf:4c00/128                 ::                                      U     0      17       1 lo      
ff00::/8                                    ::                                      U     256    0        0 eth0



########################### R1
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
default         192.168.7.2     0.0.0.0         UG    0      0        0 eth1
10.0.0.0        *               255.255.255.0   U     0      0        0 eth0
192.168.7.0     *               255.255.255.0   U     0      0        0 eth1


Kernel IPv6 routing table
Destination                                 Next Hop                                Flags Metric Ref    Use Iface
2001:db8:e::/64                             ::                                      U     256    0        0 eth0    
2001:db8:ef:7::/64                          ::                                      U     256    1        0 eth1    
fe80::/64                                   ::                                      U     256    0        0 eth0    
fe80::/64                                   ::                                      U     256    0        0 eth1    
::/0                                        2001:db8:ef:7::2                        UG    1024   0        0 eth1    
::1/128                                     ::                                      U     0      0        1 lo      
2001:db8:e::/128                            ::                                      U     0      0        1 lo      
2001:db8:e::1/128                           ::                                      U     0      17       1 lo      
2001:db8:ef:7::/128                         ::                                      U     0      0        1 lo      
2001:db8:ef:7::1/128                        ::                                      U     0      25       1 lo      
fe80::/128                                  ::                                      U     0      0        1 lo      
fe80::/128                                  ::                                      U     0      0        1 lo      
fe80::e88:2ff:fe07:a700/128                 ::                                      U     0      24       1 lo      
fe80::e88:2ff:fe07:a701/128                 ::                                      U     0      17       1 lo      
ff00::/8                                    ::                                      U     256    0        0 eth0    
ff00::/8                                    ::                                      U     256    0        0 eth1

########################### R2

Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
default         172.16.2.2      0.0.0.0         UG    0      0        0 eth1
10.0.2.0        *               255.255.255.0   U     0      0        0 eth0
172.16.2.0      *               255.255.255.0   U     0      0        0 eth1


Kernel IPv6 routing table
Destination                                 Next Hop                                Flags Metric Ref    Use Iface
2001:db8:2:f::/64                           ::                                      U     256    0        0 eth0    
2001:db8:2:ef::/64                          ::                                      U     256    0        0 eth1    
fe80::/64                                   ::                                      U     256    0        0 eth0    
fe80::/64                                   ::                                      U     256    0        0 eth1    
::/0                                        2001:db8:2:ef::2                        UG    1024   0        0 eth1    
::1/128                                     ::                                      U     0      0        1 lo      
2001:db8:2:f::/128                          ::                                      U     0      0        1 lo      
2001:db8:2:f::1/128                         ::                                      U     0      19       1 lo      
2001:db8:2:ef::/128                         ::                                      U     0      0        1 lo      
2001:db8:2:ef::1/128                        ::                                      U     0      20       1 lo      
fe80::/128                                  ::                                      U     0      0        1 lo      
fe80::/128                                  ::                                      U     0      0        1 lo      
fe80::e88:2ff:fe5c:d100/128                 ::                                      U     0      17       1 lo      
fe80::e88:2ff:fe5c:d101/128                 ::                                      U     0      14       1 lo      
ff00::/8                                    ::                                      U     256    0        0 eth0    
ff00::/8                                    ::                                      U     256    0        0 eth1  


########################### R3

Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
10.0.0.0        192.168.7.1     255.255.255.0   UG    0      0        0 eth0
10.0.2.0        172.16.2.1      255.255.255.0   UG    0      0        0 eth1
172.16.2.0      *               255.255.255.0   U     0      0        0 eth1
192.168.7.0     *               255.255.255.0   U     0      0        0 eth0


Kernel IPv6 routing table
Destination                                 Next Hop                                Flags Metric Ref    Use Iface
2001:db8:2:f::/64                           2001:db8:2:ef::1                        UG    1024   0        0 eth1    
2001:db8:2:ef::/64                          ::                                      U     256    1        0 eth1    
2001:db8:e::/64                             2001:db8:ef:7::1                        UG    1024   0        0 eth0    
2001:db8:ef:7::/64                          ::                                      U     256    1        0 eth0    
fe80::/64                                   ::                                      U     256    0        0 eth0    
fe80::/64                                   ::                                      U     256    0        0 eth1    
::1/128                                     ::                                      U     0      0        1 lo      
2001:db8:2:ef::/128                         ::                                      U     0      0        1 lo      
2001:db8:2:ef::2/128                        ::                                      U     0      19       1 lo      
2001:db8:ef:7::/128                         ::                                      U     0      0        1 lo      
2001:db8:ef:7::2/128                        ::                                      U     0      28       1 lo      
fe80::/128                                  ::                                      U     0      0        1 lo      
fe80::/128                                  ::                                      U     0      0        1 lo      
fe80::e88:2ff:fe2c:5400/128                 ::                                      U     0      17       1 lo      
fe80::e88:2ff:fe2c:5401/128                 ::                                      U     0      14       1 lo      
ff00::/8                                    ::                                      U     256    0        0 eth0    
ff00::/8                                    ::                                      U     256    0        0 eth1

