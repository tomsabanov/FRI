import java.util.*;
/*public class vaja{


	public static void main(String[] args){

		Fibonnacci fib = new Fibonnacci(5,2);
		for(int i = 0;i<10;i++){
			System.out.println(fib.naslednji());
		}
	}


*/



// class Fibonnacci{
//   private int a;
//   private int b;
//   private Boolean bool = true;
//   public Fibonnacci(int a, int b){
//     this.a = a;
//     this.b = b;
//   }
//   public int naslednji(){
//     if(this.bool){
//       this.a += b;
//       this.bool = false;
//       return this.a;
//     }
//     this.bool = true;
//     this.b += a;
//     return this.b;
//   }
// }


class Knjiznica{
	private Clan[] clani;
	private Naslov[] naslovi;
	private int stIzvododvNaNaslov;
	public Knjiznica(int stClanov, int stNaslovov,int stIzvododvNaNaslov){
		this.clani = new Clan[stClanov];
		this.naslovi = new Naslov[stNaslovov];
	}

	public boolean posodi(int clan, int naslov){
		if(this.naslovi[naslov].jeNaVoljo() && this.clani[clan].imaIzvod(naslov) == false){
			// clan si lahko izposodi
			this.clani[clan].izposodi(naslov, this.naslovi[naslov]);
			this.naslovi[naslov].izposodi();
			return true;
		}
		return false;
	}
	public void clanVrne(int clan){
		this.clani[clan].vrniVse();
	}
	public int posojeni(int naslov){
		return this.naslovi[naslov].getIzposojene();
	}
	public int priClanu(int clan){
		Clan c = this.clani[clan];
		int i = 0;
		for(Map.Entry<Integer,Naslov> n : c.getIzposojene().entrySet()){
				i++;
		}
		return i;
	}
	public  int najNaslov(){
		int max = 0;
		int k = 0;
		int g = 0;
		for(int i = 0;i<this.naslovi.length; i++){
			if(this.naslovi[i].getGlobalIzposojeno()> max){
				max = this.naslovi[i].getGlobalIzposojeno();
				k = i;
				g++;
			}
		}
		if(g>0){
			int m = 0;
			int min = max;
			for(int i = 0;i<this.naslovi.length; i++){
				if(this.naslovi[i].getGlobalIzposojeno()< min){
					min = this.naslovi[i].getGlobalIzposojeno();
					m = i;
				}
			}

		}
		return k;
	}

}
class Clan{
	private Map<Integer, Naslov> izposojene =  new HashMap<Integer,Naslov>();
	private int id;
	public Clan(int id){
		this.id = id;
	}
	public  boolean imaIzvod(int naslov){
		if(this.izposojene.get(naslov) != null){
			return true;
		}
		return false;
	}
	public Map<Integer, Naslov> getIzposojene(){
		return this.izposojene;
	}

	public void izposodi(int naslov, Naslov n){
		this.izposojene.put(naslov,n);
	}
	public void vrniVse(){
		for(Map.Entry<Integer,Naslov> n : this.izposojene.entrySet()){
			n.getValue().vrni();
		}
		this.izposojene.clear();
	}
}

class Naslov{
	private int stIzvodov;
	private int izposojenih = 0;
	private int id;
	private int izposojeno = 0;

	public Naslov(int stIzvodov, int id){
		this.stIzvodov = stIzvodov;
		this.id = id;
	}
	public boolean jeNaVoljo(){
		if(this.stIzvodov > this.izposojenih){
			return true;
		}
		return false;
	}
	public void izposodi(){
		this.izposojenih++;
		this.izposojeno++;
	}
	public int getNaslov(){
		return this.id;
	}
	public void vrni(){
		this.izposojenih--;
	}
	public int getIzposojene(){
		return this.izposojenih;
	}
	public int getGlobalIzposojeno(){
		return this.izposojeno;
	}
}
