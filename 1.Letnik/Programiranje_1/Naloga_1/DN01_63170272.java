import java.util.*;
public class DN01_63170272{

    public static void main(String[] args){
        Scanner s = new Scanner(System.in);
        int[] jaka = new int[3]; // 0 - x koordinata, 1 - y koordinata, 2 - skupna prekoracena pot
        int[] darko = new int[3];

        int m = s.nextInt();
        int n = s.nextInt();

        jaka[0] = 0;
        jaka[1] = 0;

        darko[0] = m-1;
        darko[1] = n-1;

        int p = s.nextInt();

        int enaka = 2;

        for(int i = 0; i<p; i++){
          int x = s.nextInt();
          int y = s.nextInt();

          int razdalja_jaka = Math.abs(x-jaka[0]) + Math.abs(y-jaka[1]);
          int razdalja_darko =Math.abs(x-darko[0]) + Math.abs(y-darko[1]);

          if(razdalja_darko == razdalja_jaka){
            if(enaka%2 == 0){
              // gre jaka
              jaka[0] = x;
              jaka[1] = y;
              jaka[2] = jaka[2] + razdalja_jaka;
              enaka = 1;
            }
            else{
              //gre darko
              darko[0] = x;
              darko[1] = y;
              darko[2] = darko[2] + razdalja_darko;
              enaka = 2;
            }
          }
          else if(razdalja_jaka < razdalja_darko){
            //gre jaka
            jaka[0] = x;
            jaka[1] = y;
            jaka[2] = jaka[2] + razdalja_jaka;
          }
          else{
            //gre darko
            darko[0] = x;
            darko[1] = y;
            darko[2] = darko[2] + razdalja_darko;
          }

        }

        System.out.println(jaka[2]);
        System.out.println(darko[2]);

    }

}
