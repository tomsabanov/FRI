import java.util.*;
public class DN02_63170272{
  static int stevilo=0;
    public static void main(String[] args){
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int u = s.nextInt();
        for(int i = 0; i<n;i++){
            int st_1 = s.nextInt();
            int st_2 = s.nextInt();
            switch(u){
              case 1 : podnaloga_1(st_1, st_2);break;
              case 2 : podnaloga_2(st_1, st_2);break;
              case 3 : podnaloga_3(st_1, st_2);break;
              case 4 : podnaloga_4(st_1, st_2);break;
            }
        }
        System.out.println(stevilo);
    }
    public static void podnaloga_1(int st_1, int st_2){
      if(st_1 < st_2) stevilo++;
    }
    public static void podnaloga_2(int st_1, int st_2){
      if(zadnja_stevka(st_1) == zadnja_stevka(st_2)) stevilo++;
    }
    public static void podnaloga_3(int st_1, int st_2){
      if(prva_stevka(st_1) == prva_stevka(st_2)) stevilo ++;
    }
    public static int zadnja_stevka(int i){
      if(i%10 == i) return i;
      return zadnja_stevka(i%10);
    }
    public static int zadnje_stevke(int i, int j){
      if(i%(j) == i) return i;
      return zadnje_stevke(i%j,j);
    }
    public static int prva_stevka(int st){
      if(st>9)return prva_stevka(st/10);
      return st;
    }
    public static void podnaloga_4(int st_1, int st_2){
      if(dolzina(st_1)!=dolzina(st_2)) return;
      for(int i = 1;;i*=10){
        int zadnje_cifre = zadnje_stevke(st_1,i);
        int st_a = prva_stevka(zadnje_cifre);
        if(stevilo_ponovitev(st_1, st_a)!= stevilo_ponovitev(st_2, st_a))return;
        if(zadnje_cifre == st_1) break;
      }
      stevilo++;
    }
    public static int dolzina(int a){
      for(int j = 10, dolzina = 0;;j*=10,dolzina++){
        int zadnje_cifre =zadnje_stevke(a,j);
        if(zadnje_cifre == a) return dolzina;
      }
    }
    public static int stevilo_ponovitev(int st_1, int iskano_stevilo){
      int stevilo_ponovitev = 0;
      for(int j = 10;;j*=10){
        int zadnje_cifre =zadnje_stevke(st_1,j);
        if(prva_stevka(zadnje_cifre) == iskano_stevilo) stevilo_ponovitev++;
        if(zadnje_cifre == st_1) break;
      }
      return stevilo_ponovitev;
    }
}
