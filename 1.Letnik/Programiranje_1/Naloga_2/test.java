import java.util.*;
import java.io.*;
import java.nio.file.*;
import java.nio.*;

public class test{
  static int stevilo=0;
    public static void main(String[] args){
      try{
        Path filePath = Paths.get("vhod09.txt");
        Scanner s = new Scanner(filePath);
        PrintWriter writer = new PrintWriter("test_out.txt", "UTF-8");
        while(s.hasNext()){
            int trenutno = stevilo;
            int st_1 = s.nextInt();
            int st_2 = s.nextInt();
            podnaloga_4(st_1, st_2);
            if(stevilo > trenutno){
              //permutacija se najdla
              if(preveri(st_1,st_2) == false){
                System.out.println(st_1 + "    " + st_2);
              }
              String stri = st_1 +" " + st_2;
              writer.println(stri);
            }

        }
        writer.close();
        System.out.println("Stevilo parov : "+stevilo);
      }
      catch(Exception e){}

    }
    public static Boolean preveri(int st_1,int  st_2){
      String str1 = st_1 + "";
      String str2 = st_2 + "";
      if (str1.length() != str2.length())
            return false;

          char[] a = str1.toCharArray();
          char[] b = str2.toCharArray();

          Arrays.sort(a);
          Arrays.sort(b);

          return Arrays.equals(a, b);
    }

    public static int zadnja_stevka(int i){
      if(i%10 == i) return i;
      return zadnja_stevka(i%10);
    }
    public static int zadnje_stevke(int i, int j){
      if(i%(j) == i) return i;
      return zadnje_stevke(i%j,j);
    }
    public static int prva_stevka(int st){
      if(st>9)return prva_stevka(st/10);
      return st;
    }
    public static void podnaloga_4(int st_1, int st_2){
      if(dolzina(st_1)!=dolzina(st_2)) return;
      int i = 10;
      int st_a = zadnja_stevka(st_1);
      while(true){
        int zadnje_cifre = zadnje_stevke(st_1,i);
        if(stevilo_ponovitev(st_1, st_a)!= stevilo_ponovitev(st_2, st_a))return;
        if(zadnje_cifre == st_1) break;
        st_a = prva_stevka(zadnje_cifre);
        i*=10;
      }
      stevilo++;
    }
    public static int dolzina(int a){
      for(int j = 10, dolzina = 0;;j*=10,dolzina++){
        int zadnje_cifre =zadnje_stevke(a,j);
        if(zadnje_cifre == a) return dolzina;
      }
    }
    public static int stevilo_ponovitev(int st_1, int iskano_stevilo){
      int stevilo_ponovitev = 0;
      for(int j = 10;;j*=10){
        int zadnje_cifre =zadnje_stevke(st_1,j);
        if(prva_stevka(zadnje_cifre) == iskano_stevilo) stevilo_ponovitev++;
        if(zadnje_cifre == st_1) break;
      }
      return stevilo_ponovitev;
    }
}
