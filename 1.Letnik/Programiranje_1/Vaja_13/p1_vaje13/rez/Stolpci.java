
import java.awt.*;

/**
 * Stolpci s povezovalno "crto in napisi.
 */
public class Stolpci extends Platno {

    public static void main(String[] args) {
        // V odvisnosti od argumentov ukazne vrstice bodisi ustvari okno in
        // plo"s"co ali pa pripravi sliko v pomnilniku, na katero je mo"zno
        // risati.  S stali"s"ca izdelave na"sega programa je to popolnoma
        // vseeno.

        int[] podatki = {150, 70, 120, 190, 60, 130, 40, 100, 80, 60};
        //int[] podatki = {5};
        //int[] podatki = {1, 1, 1};
        Stolpci stolpci = new Stolpci(podatki);
        stolpci.sproziRisanje(args);
    }

    // podatki, ki dolo"cajo vi"sine stolpcev
    private int[] podatki;

    public Stolpci(int[] podatki) {
        this.podatki = podatki;
    }

    /**
     * Nari"se sliko na platno (zaslon ali slikovno datoteko).  To metodo
     * pokli"ce ogrodje, ko je "cas zanjo.
     * @param g objekt, ki nam omogo"ca risanje
     * @param wp "sirina platna
     * @param hp vi"sina platna
     */
    protected void narisi(Graphics2D g, double wp, double hp) {
        // dopolnite ...
        int sirinaStolpca = (int)this.sirinaStolpca(wp,hp);
        int x = 0;
        for(int i = 0; i<this.podatki.length;i++){
          g.setColor(Color.ORANGE);
              int h = (int)hp;
            int visinaStolpca=(int)this.visinaStolpca(i,wp,hp);
            int razlika  = h - visinaStolpca;
            g.drawRect(x,razlika,(int)sirinaStolpca,visinaStolpca);
            g.fillRect(x, razlika, (int)sirinaStolpca, visinaStolpca);

            g.setColor(Color.RED);
            g.drawLine(x,razlika,x,h);
            x+=sirinaStolpca;
            g.drawLine(x,razlika,x,h);
            g.drawLine(x,razlika,x-sirinaStolpca,razlika);

            if(i>0){
              g.setColor(Color.BLUE);
              int p_visinaStolpca=(int)this.visinaStolpca(i-1,wp,hp);
              int p_razlika  = h - p_visinaStolpca;
              g.drawLine(x-sirinaStolpca/2,razlika, x-sirinaStolpca-sirinaStolpca/2, p_razlika);
            }
            String s = i+1 + " ";
            g.setColor(Color.BLACK);
            g.drawString(s, x-sirinaStolpca/2, h-12);
        }


    }

    /**
     * Vrne "sirino (v pikslih) posameznega stolpca.
     */
    public double sirinaStolpca(double wp, double hp) {
        // popravite / dopolnite ...
        double rez = wp/this.podatki.length;
        return rez;
    }

    /**
     * Vrne vi"sino (v pikslih) stolpca s podanim indeksom.
     */
    public double visinaStolpca(int ixStolpec, double wp, double hp) {
        // popravite / dopolnite ...
        int max = 0;
        for(int i = 0; i<this.podatki.length;i++){
          if(this.podatki[i]>=max){
            max = this.podatki[i];
          }
        }

        // stolpec z vrednostjo max ima visino hp

        double razmerje = hp/max;
        return razmerje * this.podatki[ixStolpec];
    }

    /**
     * Vrne koordinati (v pikslih) sredine zgornjega roba stolpca s podanim indeksom.
     * @return tabela z dvema elementoma ({x, y}).
     */
    public double[] sredinaVrha(int ixStolpec, double wp, double hp) {
        // popravite / dopolnite ...
        double[] rez = new double[2];
        rez[0] = this.sirinaStolpca(wp,hp) * ixStolpec + this.sirinaStolpca(wp,hp)/2.0;
        rez[1] = hp - this.visinaStolpca(ixStolpec,wp,hp);
        return new double[]{this.sirinaStolpca(wp,hp) * ixStolpec + this.sirinaStolpca(wp,hp)/2.0,hp - this.visinaStolpca(ixStolpec,wp,hp)};
    }
}
