import java.util.*;
public class Vaja6{
    static int faktor = 10;
    static int cifra = 1;
    public static void main(String[] args){
        Scanner s  = new Scanner(System.in);
               
        while(s.hasNext()){
            int primer = s.nextInt();
            int c = 0;
            if(primer>0){
                c = s.nextInt();            
            }
            switch(primer){
                case 1 :
                    primer_1(c);
                    break;
                case 2:
                    primer_2(c);
                    break;
                case -1:
                    primer_3();
                    break;
                case -2:
                    primer_4();
                    break;
            }
            System.out.println(cifra);
        }
    }
    
    
    static void primer_1(int c){
        cifra += faktor * c;    
        faktor*=10;
    }
    static void primer_2(int c){
        cifra = cifra * faktor/10 + c;    
        faktor*=10;
    }
    static void primer_3(){
        faktor/=10;
        while(cifra > faktor){
            cifra-=faktor;  
        }
    }
    static void primer_4(){
        cifra /= 10;    
        faktor/=10;
    }
    

}
