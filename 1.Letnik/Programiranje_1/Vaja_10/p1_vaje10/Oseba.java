import java.util.*;
public class Oseba {

        private String ip;
        private int starost;
	private PostniNaslov postniNaslov; 
	public int stevilo_prijateljev;
	private ArrayList<Oseba> prijatelji = new ArrayList<Oseba>();

    public Oseba(String ip, int starost, PostniNaslov postniNaslov) {
		this.ip  = ip;
		this.starost = starost;
		this.postniNaslov = postniNaslov;
    }

    public String vrniIP() {
	return this.ip;
    }
    public String toString() {
	return this.ip + "(" + this.starost + ")," + this.postniNaslov; 
    }

    public int steviloPrijateljev() {
        return this.stevilo_prijateljev;
    }

    public boolean jePrijateljOd(Oseba os) {
	if(os.prijatelji.contains(this)){
		return true;
	}
        return false;
    }

    public static boolean vzpostaviPrijateljstvo(Oseba prva, Oseba druga) {
	if(prva != druga && prva.prijatelji.contains(druga) == false){
		prva.prijatelji.add(druga);
		druga.prijatelji.add(prva);
		prva.stevilo_prijateljev++;
		druga.stevilo_prijateljev++;	
		return true;
	}
        return false;
    }

   public String getPostniNaslov(){
	return this.postniNaslov.toString();
   }

    public boolean naIstemNaslovuKot(Oseba os) {
	if(os.getPostniNaslov().equals(this.getPostniNaslov())){
		 return true;
	}
        return false;
    }

    public int steviloPrijateljevNaIstemNaslovu() {
	int i = 0;
	int c = 0;
	while(c<this.prijatelji.size()){
		if(this.prijatelji.get(c).getPostniNaslov().equals(this.getPostniNaslov())){
			i++;
		}
		c++;
	}	
        return i;
    }
    public int getStarost(){
		return this.starost;
		
    }
    public Oseba najstarejsiPrijatelj(){
	if(this.prijatelji.size() > 0){
		Oseba n = this.prijatelji.get(0);
		int i = 1;
		while(i<this.prijatelji.size()){
			if(n.getStarost() < this.prijatelji.get(i).getStarost()){
				n = this.prijatelji.get(i);
			}
			i++;
		} 
		return n;
	}
        return null;
    }
}
