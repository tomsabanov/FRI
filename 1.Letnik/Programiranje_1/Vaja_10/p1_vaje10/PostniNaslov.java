
public class PostniNaslov {

    private String ulicaHS;  // ulica s hišno številko
    private int postnaStevilka;
    private String posta;

    public PostniNaslov(String ulicaHS, int postnaStevilka, String posta) {
        this.ulicaHS = ulicaHS;
        this.postnaStevilka = postnaStevilka;
        this.posta = posta;
    }

    public String toString() {
        return this.ulicaHS + ", " + this.postnaStevilka + " " + this.posta;
    }

    /** 
     * Vrne `true' natanko v primeru, če objekt `pn' predstavlja isti poštni
     * naslov kot objekt `this'.
     */
    public boolean jeEnakKot(PostniNaslov pn) {
        // popravite / dopolnite ...
	if(this.toString().equals(pn.toString())){
		return true;
	}
        return false;
    }
}
