
package s12345678;

import skupno.Stroj;
import skupno.Polje;
import skupno.Poteza;
import skupno.Konstante;
import java.util.Random;

/**
 * Objekt tega razreda je stroj, ki poteze izbira povsem naključno, vendar pa
 * nikoli ne izbere neveljavne poteze in se strogo drži predpisanih časovnih
 * omejitev.
 *
 * @author Naključko Randomè, Fakulteta za naključne študije
 */
public class Stroj_Nakljucko implements Stroj {

    /** število žetonov je enako dolžini stranice */
    private static final int ST_ZETONOV = Konstante.STRANICA;

    /** generator naključnih števil */
    private final Random nakljucniGenerator;

    /** true, če igram kot beli; false, če igram kot črni */
    private boolean semBeli;

    /**
     * zasedeno[vr][st]: true natanko v primeru, ko je polje v vrstici z
     * indeksom vr in stolpcu z indeksom st zasedeno.  Na zasedenem polju (vr,
     * st) z liho vsoto koordinat se nahaja beli žeton, na zasedenem polju s
     * sodo vsoto koordinat pa črni žeton.
     */
    private boolean[][] zasedeno;

    /**
     * V konstruktorju inicializiram generator naključnih števil in tabelo
     * zasedenosti polj.
     */
    public Stroj_Nakljucko() {
        this.nakljucniGenerator = new Random();
        this.zasedeno = new boolean[Konstante.STRANICA][Konstante.STRANICA];
    }

    /**
     * Ob pričetku partije nastavim svojo barvo in ponastavim tabelo
     * zasedenosti polj.
     *
     * @param beli  true, če bom igral kot beli; false, če bom igral kot črni
     */
    @Override
    public void novaPartija(boolean beli) {
        this.semBeli = beli;

        // Na začetku so zasedena vsa polja v spodnjih dveh vrsticah, ki imajo
        // liho vsoto vrstice in stolpca (beli žetoni), in vsa polja v
        // zgornjih dveh vrsticah, ki imajo sodo vsoto vrstice in stolpca
        // (črni žetoni).
        for (int vr = 0;  vr < Konstante.STRANICA;  vr++) {
            for (int st = 0;  st < Konstante.STRANICA;  st++) {
                Polje polje = new Polje(vr, st);
                this.nastaviZasedenost(polje,
                        (vr >= Konstante.STRANICA - 2 && polje.ostanek() == 1) ||
                        (vr < 2 && polje.ostanek() == 0));
            }
        }
    }

    /**
     * Ko sem na vrsti za potezo, naključno izberem enega od pomikov ali
     * skokov v smeri proti nasprotni strani šahovnice.  Če take poteze ni,
     * izberem enega od pomikov ali skokov v smeri proti moji strani
     * šahovnice.  Večkratnih skokov žal ne obvladam.  Tudi skokov levo in
     * desno ne izvajam.
     *
     * @return izbrana poteza 
     */
    @Override
    public Poteza izberiPotezo(long preostaliCas) {

        // vsak žeton ima največ 4 pomike in enkratne skoke v smeri proti
        // nasprotni strani šahovnice in največ 4 pomike in enkratne skoke v
        // smeri proti lastni strani šahovnice
        Poteza[] potezeNaprej = new Poteza[4 * ST_ZETONOV];
        Poteza[] potezeNazaj = new Poteza[4 * ST_ZETONOV];

        // dejansko število potez v smeri proti nasprotni (naprej) oz. lastni
        // (nazaj) strani šahovnice
        int stPotezNaprej = 0;
        int stPotezNazaj = 0;

        // tvorim vse možne pomike in enkratne skoke v smeri naprej in nazaj
        int ciljniOstanek = (this.semBeli ? 1 : 0);
        for (int vr = 0;  vr < Konstante.STRANICA;  vr++) {
            for (int st = 0;  st < Konstante.STRANICA;  st++) {
                Polje polje = new Polje(vr, st);
                if (this.jeZasedeno(polje) && polje.ostanek() == ciljniOstanek) {
                    stPotezNaprej = dodajPotezeZaZeton(
                            polje, potezeNaprej, stPotezNaprej, true);
                    stPotezNazaj = dodajPotezeZaZeton(
                            polje, potezeNazaj, stPotezNazaj, false);
                }
            }
        }

        // naključno izberem eno od potez naprej, če je ni, pa eno od potez
        // nazaj
        Poteza izbranaPoteza;
        if (stPotezNaprej > 0) {
            int ixIzbranePoteze = this.nakljucniGenerator.nextInt(stPotezNaprej);
            izbranaPoteza = potezeNaprej[ixIzbranePoteze];
        } else {
            int ixIzbranePoteze = this.nakljucniGenerator.nextInt(stPotezNazaj);
            izbranaPoteza = potezeNazaj[ixIzbranePoteze];
        }

        // posodobim zasedenost po izbrani potezi
        this.sprejmiPotezo(izbranaPoteza);

        return izbranaPoteza;
    }

    /**
     * V elemente poteze[stPotez], poteze[stPotez + 1] itd. shrani vse pomike in
     * enkratne skoke v podani smeri za žeton na polju (vrZacetno, stZacetno).
     *
     * @param vrZacetno  vrstični indeks polja, na katerem se nahaja izbrani žeton
     * @param stZacetno  stolpčni indeks polja, na katerem se nahaja izbrani žeton
     * @param poteze     tabela potez
     * @param stPotez    indeks elementa v tabeli `poteze', kamor naj se shrani prva poteza
     * @param protiNasprotniku  
     *     true pomeni, da se bodo tvorile samo poteze naprej (v smeri proti
     *     nasprotni strani šahovnice); false pomeni, da se bodo tvorile samo
     *     poteze nazaj (v smeri proti moji strani šahovnice
     *
     * @return  novo skupno število potez v tabeli `poteze'
     */
    private int dodajPotezeZaZeton(Polje zacetno, Poteza[] poteze, 
            int stPotez, boolean protiNasprotniku) {

        // možne razlike med končnim in začetnim stolpcem pri diagonalnem pomiku
        int[] stolpcniPomik = {-1, 1};

        // možne razlike med končnim in začetnim stolpcem pri polovičnem skoku
        int[] stolpcniPolskok = {-1, 0, 1};

        // razlika med končno in začetno vrstico
        int vrsticniPremik;
        if (this.semBeli && protiNasprotniku || !this.semBeli && !protiNasprotniku) {
            vrsticniPremik = -1;
        } else {
            vrsticniPremik = 1;
        }

        // pomiki
        for (int i = 0;  i < stolpcniPomik.length;  i++) {
            Polje koncno = zacetno.plus(vrsticniPremik, stolpcniPomik[i]);

            // končno polje mora obstajati in biti prosto
            if (koncno.obstaja() && !this.jeZasedeno(koncno)) {
                poteze[stPotez] = new Poteza(zacetno, koncno);
                stPotez++;
            }
        }

        // skoki
        for (int i = 0;  i < stolpcniPolskok.length;  i++) {
            Polje polskocno = zacetno.plus(vrsticniPremik, stolpcniPolskok[i]);

            // če želim skočiti, mora biti ciljno polje polovičnega skoka
            // zasedeno
            if (polskocno.obstaja() && this.jeZasedeno(polskocno)) {
                Polje koncno = polskocno.plus(vrsticniPremik, stolpcniPolskok[i]);

                // končno polje mora obstajati in biti prosto
                if (koncno.obstaja() && !this.jeZasedeno(koncno)) {
                    poteze[stPotez] = new Poteza(zacetno, koncno);
                    stPotez++;
                }
            }
        }
        return stPotez;
    }

    /**
     * Ko potezo odigra moj nasprotnik, posodobim tabelo zasedenosti polj.
     *
     * @param poteza  poteza, ki jo je izbral moj nasprotnik
     */
    @Override
    public void sprejmiPotezo(Poteza poteza) {
        this.nastaviZasedenost(poteza.vrniZacetno(), false);
        this.nastaviZasedenost(poteza.vrniKoncno(), true);
    }

    /**
     * Ko se partija zaključi, mi ni treba storiti ničesar, saj mi točke niso
     * pomembne ...
     *
     * @param izid  izid partije
     */
    @Override
    public void rezultat(int izid) {
    }

    /**
     * Vrne true natanko v primeru, če je podano polje zasedeno.
     */
    private boolean jeZasedeno(Polje polje) {
        return this.zasedeno[polje.vrniVrstico()][polje.vrniStolpec()];
    }

    /**
     * Nastavi zasedenost podanega polja na podano vrednost.
     */
    private void nastaviZasedenost(Polje polje, boolean vrednost) {
        this.zasedeno[polje.vrniVrstico()][polje.vrniStolpec()] = vrednost;
    }
}
