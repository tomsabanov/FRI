
package skupno;

/**
 * Ta razred vsebuje globalne konstante, ki jih lahko uporabljajo tudi stroji.
 */
public class Konstante {

    /** dolžina stranice šahovnice (mora biti soda in enaka najmanj 4) */
    public static final int STRANICA = 8;

    /** število polpotez, po katerih se partija proglasi za neodločeno (mora biti sodo in enako najmanj 2) */
    public static final int ST_POLPOTEZ_DO_REMIJA = 200;
}
