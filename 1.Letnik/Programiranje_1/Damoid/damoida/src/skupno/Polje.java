
package skupno;

/**
 * Objekt tega razreda predstavlja polje šahovnice.
 */
public class Polje {

    /** indeks vrstice, v kateri se nahaja polje this */
    private final int vrstica;

    /** indeks stolpca, v katerem se nahaja polje this */
    private final int stolpec;

    /** 
     * Izdela objekt, ki predstavlja polje s podanim indeksom vrstice in
     * stolpca.
     *
     * @param vrstica  indeks vrstice (med 0 in vključno (Konstante.STRANICA - 1)
     * @param stolpec  indeks stolpca (med 0 in vključno (Konstante.STRANICA - 1)
     */
    public Polje(int vrstica, int stolpec) {
        this.vrstica = vrstica;
        this.stolpec = stolpec;
    }

    /**
     * Vrne indeks vrstice polja this. 
     *
     * @return indeks vrstice polja this
     */
    public int vrniVrstico() {
        return this.vrstica;
    }

    /** 
     * Vrne indeks stolpca polja this.
     *
     * @return indeks stolpca polja this
     */
    public int vrniStolpec() {
        return this.stolpec;
    }

    /** 
     * Vrne true natanko v primeru, če polje this na šahovnici dejansko
     * obstaja.
     *
     * @return true, če polje this obstaja; false, če ne
     */
    public boolean obstaja() {
        return (this.vrstica >= 0) && (this.stolpec >= 0) && 
            (this.vrstica < Konstante.STRANICA) && (this.stolpec < Konstante.STRANICA);
    }

    /**
     * Vrne polje, ki je za (vrPremik, stPremik) oddaljeno od polja this.
     *
     * @return ciljno polje
     */
    public Polje plus(int vrPremik, int stPremik) {
        return new Polje(this.vrstica + vrPremik, this.stolpec + stPremik);
    }

    /**
     * Vrne tabelo z dvema elementoma, ki podajata razliko med poljem this in
     * poljem polje.
     *
     * @return tabela {vrstična razlika, stolpčna razlika}
     */
    public int[] razlika(Polje polje) {
        return new int[]{this.vrstica - polje.vrstica, this.stolpec - polje.stolpec};
    }

    /**
     * Vrne ostanek pri deljenju vsote koordinat polja this z 2.
     *
     * @return 1, če je vsota koordinat liha; 0, če je soda
     */
    public int ostanek() {
        return (this.vrstica + this.stolpec) % 2;
    }

    /**
     * Vrne predstavitev polja this kot niz oblike (v, s), kjer sta v in s
     * indeksa vrstice in stolpca polja this.
     *
     * @return niz, ki predstavlja polje this
     */
    @Override
    public String toString() {
        return String.format("%d/%d", this.vrstica, this.stolpec);
    }

    /**
     * Vrne zgoščevalno kodo polja this.  (Te metode najverjetneje ne
     * boste neposredno uporabljali.)
     *
     * @return zgoščevalna koda polja this
     */
    @Override
    public int hashCode() {
        return (this.vrstica << 8) | this.stolpec;
    }

    /** 
     * Vrne true natanko v primeru, če objekt this predstavlja isto polje kot
     * objekt obj.
     *
     * @param obj  objekt (smiselno je, da pripada razredu Polje)
     *
     * @return true, če objekta this in obj predstavljata isto polje;
     *         false sicer
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof Polje)) {
            return false;
        }
        Polje polje = (Polje) obj;
        return (this.vrstica == polje.vrstica && this.stolpec == polje.stolpec);
    }
}
