
package skupno;

/**
 * Objekt tega vmesnika predstavlja stroj za igranje damoide.
 */
public interface Stroj {

    /** 
     * Ta metoda se pokliče ob pričetku vsake partije.
     *
     * @param beli true, če stroj this igra z belimi žetoni;
     *             false, če igra s črnimi
     */
    public void novaPartija(boolean beli);

    /**
     * Ta metoda se pokliče, ko je stroj this na vrsti za izbiro poteze.
     * Metoda mora v `preostaliCas' milisekundah vrniti potezo, ki jo stroj
     * želi odigrati.
     *
     * @param preostaliCas  čas, ki ga ima stroj this na voljo do konca partije
     * 
     * @return izbrana poteza
     */
    public Poteza izberiPotezo(long preostaliCas);

    /** 
     * Ta metoda se pokliče, ko nasprotnik stroja this odigra potezo.
     *
     * @param poteza  poteza, ki jo je nasprotnik odigral
     */
    public void sprejmiPotezo(Poteza poteza);

    /**
     * Ta metoda se pokliče ob koncu vsake partije.
     *
     * @param izid   1, če je zmagal stroj this;
     *              -1, če je zmagal nasprotnik stroja this;
     *               0, če se je igra zaključila z remijem.
     */
    public void rezultat(int izid);
}
