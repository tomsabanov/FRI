
package skupno;

/**
 * Objekt tega razreda predstavlja potezo.
 */
public class Poteza {

    /** začetno polje poteze this */
    private final Polje zacetno;

    /** končno polje poteze this */
    private final Polje koncno;

    /** 
     * Izdela objekt, ki predstavlja potezo.
     *
     * @param zacetno  začetno polje poteze
     * @param koncno   končno polje poteze
     */
    public Poteza(Polje zacetno, Polje koncno) {
        this.zacetno = zacetno;
        this.koncno = koncno;
    }

    /**
     * Vrne začetno polje poteze this. 
     *
     * @return začetno polje poteze this
     */
    public Polje vrniZacetno() {
        return this.zacetno;
    }

    /** 
     * Vrne končno polje poteze this.
     *
     * @return končno polje poteze this
     */
    public Polje vrniKoncno() {
        return this.koncno;
    }

    /**
     * Vrne predstavitev poteze this v obliki niza.
     *
     * @return niz oblike z -> k, kjer sta z in k predstavitvi začetnega in
     *         končnega polja poteze this
     */
    @Override
    public String toString() {
        return String.format("%s -> %s", this.zacetno.toString(), this.koncno.toString());
    }

    /**
     * Vrne zgoščevalno kodo poteze this.  (Te metode najverjetneje ne
     * boste neposredno uporabljali.)
     *
     * @return zgoščevalna koda poteze this
     */
    @Override
    public int hashCode() {
        return (this.zacetno.hashCode() << 16) | this.koncno.hashCode();
    }

    /** 
     * Preveri, ali objekt this predstavlja isto potezo kot objekt obj.
     *
     * @param obj  objekt (smiselno je, da pripada razredu Poteza)
     *
     * @return true, če objekta this in obj predstavljata isto potezo;
     *         false sicer
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof Poteza)) {
            return false;
        }
        Poteza poteza = (Poteza) obj;
        return (this.zacetno.equals(poteza.zacetno) && this.koncno.equals(poteza.koncno));
    }
}
