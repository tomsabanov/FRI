
package ogrodje;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;
import skupno.Polje;

/**
 * Objekt tega razreda predstavlja statusno ploščo.  Gre za ploščo na dnu
 * krovne plošče, ki prikazuje koordinati trenutnega polja in izid partije.
 */
public class GuiStatusnaPlosca extends JPanel {

    /** barva ozadja */
    private static final Color B_OZADJE = GuiKrovnaPlosca.B_OZADJE;

    /** barva napisa, ki prikazuje trenutno polje */
    private static final Color B_POLJE = new Color(224, 224, 224);

    /** barva napisa, ki prikazuje oznako naslednje poteze */
    private static final Color B_POTEZA = B_POLJE;

    /** barva napisa, ki prikazuje izid partije */
    private static final Color B_IZID = new Color(224, 224, 0);

    /** minimalno razmerje med višino statusne plošče in polja igralne
     * površine */
    private static final double R_VISINA_POLJE = 0.4;

    /** razmerje med velikostjo pisave in višino plošče */
    private static final double R_PISAVA_VISINA = 0.5;

    /** referenčna velikost pisave */
    private static final int REF_VELIKOST_PISAVE = 10;

    /** minimalna velikost pisave */
    private static final int MIN_VELIKOST_PISAVE = 10;

    /** maksimalna velikost pisave */
    private static final int MAKS_VELIKOST_PISAVE = 20;

    /** pisava za izpis koordinat in izida v privzeti velikosti */
    private static final Font REF_PISAVA = new Font("SansSerif", Font.PLAIN, REF_VELIKOST_PISAVE);

    /** polje, na katerem se miška trenutno nahaja (null, če takega polja ni) */
    private skupno.Polje polje;

    /** oznaka naslednje poteze */
    private String oznakaPoteze;

    /** izid pravkar odigrane partije (null, če partija še ni zaključena) */
    private Izid izid;

    public GuiStatusnaPlosca() {
        this.setBackground(B_OZADJE);
    }

    public void novaPartija() {
        this.polje = null;
        this.izid = null;
        this.oznakaPoteze = null;
    }

    public void nastaviPolje(Polje polje) {
        this.polje = polje;
    }

    public void nastaviOznakoPoteze(String oznakaPoteze) {
        this.oznakaPoteze = oznakaPoteze;
    }

    public void nastaviIzid(Izid izid) {
        this.izid = izid;
    }

    @Override
    protected void paintComponent(Graphics g0) {
        Graphics2D g = GuiRazno.nastaviAntialiasing(g0, true);
        super.paintComponent(g);

        double wPlosca = (double) this.getWidth();
        double hPlosca = (double) this.getHeight();

        // določi najprimernejšo velikost pisave
        double hRefPisava = GuiRazno.visinaPisave(g.getFontMetrics(REF_PISAVA));
        float velikostPisave = (float) Razno.ri(
                REF_VELIKOST_PISAVE * R_PISAVA_VISINA * hPlosca / hRefPisava
        );
        velikostPisave = Math.max(MIN_VELIKOST_PISAVE,
                Math.min(MAKS_VELIKOST_PISAVE, velikostPisave));
        g.setFont(REF_PISAVA.deriveFont(velikostPisave));
        FontMetrics fm = g.getFontMetrics();
        double sirinaM = (double) fm.stringWidth("M");

        Rectangle2D rCelota = new Rectangle2D.Double(0, 0, wPlosca, hPlosca);

        // izpiši posamezne komponente

        if (this.izid != null) {
            g.setColor(B_IZID);
            GuiRazno.narisiBesedilo(g, rCelota, this.izid.rezultat());
        } else {
            if (this.oznakaPoteze != null) {
                g.setColor(B_POTEZA);
                GuiRazno.narisiBesediloLevo(g, rCelota, String.format("poteza %s", this.oznakaPoteze));
            }
            if (this.polje != null && this.polje.obstaja()) {
                g.setColor(B_POLJE);
                GuiRazno.narisiBesediloDesno(g, rCelota, String.format("polje %s", this.polje.toString()));
            }
        }
    }

    public static double visina(double dPolje) {
        return (R_VISINA_POLJE * dPolje);
    }
}
