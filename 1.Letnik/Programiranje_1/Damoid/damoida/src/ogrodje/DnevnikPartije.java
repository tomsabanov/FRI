
package ogrodje;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import skupno.Poteza;

/**
 * Objekt tega razreda predstavlja dnevnik posamezne partije.  Dnevnik se
 * zapiše v datoteko, ki je podana ob opcijskem parametru -d.
 */
public class DnevnikPartije {

    public static final String[] BARVA = {"beli", "črni"};
    public static final String NEZNAN = "?";

    /** Objekt tega razreda predstavlja zapis o potezi v dnevniku. */
    private static class ZapisPoteze {
        /** indeks igralca, ki je odigral potezo this */
        private final int akter;

        /** premik, ki ga je izvedel v potezi */
        private final Poteza poteza;

        public ZapisPoteze(int akter, Poteza poteza) {
            this.akter = akter;
            this.poteza = poteza;
        }

        @Override
        public String toString() {
            return String.format("%s: %s", BARVA[this.akter], this.poteza);
        }
    }

    /** številka partije, na katero se nanaša dnevnik this. */
    private int stevilkaPartije;

    /** ime prvega igralca */
    private String prviIgralec;

    /** ime drugega igralca */
    private String drugiIgralec;

    /** datum in čas ob pričetku partije */
    private Calendar casovnaZnacka;

    /** posamezne poteze v partiji */
    private final List<ZapisPoteze> poteze;

    /** true, če je v tej partiji beli tisti, ki je bil v prvi partiji črni,
     * in obratno */
    private boolean zamenjanaIgralca;

    /** izid partije */
    private Izid izid;

    /** ~[ix]: točkovni izkupiček igralca, ki je imel v prvi partiji indeks ix */
    private int[] skupneTocke;

    public DnevnikPartije() {
        this.poteze = new ArrayList<>();
    }

    /**
     * Inicializira dnevnik ob pričetku partije.
     * 
     * @param igralca  podatki o igralcih
     * @param stevilkaPartije  zaporedna številka partije
     */
    public void pricni(Igralec[] igralca, int stevilkaPartije, boolean zamenjanaIgralca) {
        this.stevilkaPartije = stevilkaPartije;
        this.prviIgralec = igralca[0].ime();
        this.drugiIgralec = igralca[1].ime();
        this.casovnaZnacka = Calendar.getInstance();
        this.poteze.clear();
        this.izid = null;
        this.skupneTocke = null;
        this.zamenjanaIgralca = zamenjanaIgralca;
    }

    /** 
     * Doda podano potezo partije v dnevnik this.
     * 
     * @param akter kdo je odigral potezo (0 ali 1)
     * @param poteza poteza, ki jo je akter odigral
     */
    public void dodajPotezo(int akter, Poteza poteza) {
        this.poteze.add(new ZapisPoteze(akter, poteza));
    }

    /** 
     * Doda podatke o zaključku partije v dnevnik this.
     * 
     * @param izid  izid partije
     * @param skupneTocke  trenutno število točk za oba igralca
     */
    public void zakljuci(Izid izid, int[] skupneTocke) {
        this.izid = izid;
        this.skupneTocke = skupneTocke;
    }

    /** Na podlagi podatkov v dnevniku this vrne niz za izpis v datoteko. */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("---------- Partija %d ----------%n", this.stevilkaPartije));
        builder.append(String.format("Beli: %s%n", this.prviIgralec));
        builder.append(String.format("Črni: %s%n", this.drugiIgralec));
        builder.append(String.format("Čas pričetka: %02d.%02d.%d %02d:%02d:%02d.%03d%n", 
                this.casovnaZnacka.get(Calendar.DAY_OF_MONTH),
                this.casovnaZnacka.get(Calendar.MONTH) + 1,
                this.casovnaZnacka.get(Calendar.YEAR),
                this.casovnaZnacka.get(Calendar.HOUR_OF_DAY),
                this.casovnaZnacka.get(Calendar.MINUTE),
                this.casovnaZnacka.get(Calendar.SECOND),
                this.casovnaZnacka.get(Calendar.MILLISECOND)
        ));
        builder.append(String.format("Število polpotez: %d%n", this.poteze.size()));
        int i = 1;
        for (ZapisPoteze poteza: this.poteze) {
            builder.append(String.format("(%s) %s%n", Razno.polpoteza2oznakaPoteze(i), poteza.toString()));
            i++;
        }
        if (this.skupneTocke != null) {
            int zmagovalec = this.izid.zmagovalec();
            String[] imeni = {this.prviIgralec, this.drugiIgralec};
            String barvaZmagovalca = (zmagovalec < 0) ? (NEZNAN) : (BARVA[zmagovalec]);
            String imeZmagovalca = (zmagovalec < 0) ? (NEZNAN) : (imeni[zmagovalec]);
            builder.append(String.format("Zmagovalec: %s (%s) %n", barvaZmagovalca, imeZmagovalca));
            builder.append(String.format("Opis izida: %s%n", this.izid.toString()));
            if (this.zamenjanaIgralca) {
                builder.append(String.format("Točke igralca %s: %d%n", this.drugiIgralec, this.skupneTocke[1]));
                builder.append(String.format("Točke igralca %s: %d%n", this.prviIgralec, this.skupneTocke[0]));
            } else {
                builder.append(String.format("Točke igralca %s: %d%n", this.prviIgralec, this.skupneTocke[0]));
                builder.append(String.format("Točke igralca %s: %d%n", this.drugiIgralec, this.skupneTocke[1]));
            }
        }
        builder.append(Razno.NL);
        return builder.toString();
    }
}
