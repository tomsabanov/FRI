
package ogrodje;

import skupno.Konstante;

/**
 * Ogrodje za tekmovanje strojev v igri Damoida.
 * <p/>
 * Ta razred služi kot vstopna točka v ogrodje.
 * 
 * @author Luka Fürst, 2017
 * @version 1.0
 */
public class Damoida {

    public static void main(String[] args) {
        if (Konstante.STRANICA < 4 || Konstante.STRANICA % 2 == 1) {
            throw new RuntimeException("Vrednost konstante Konstante.STRANICA mora biti soda in enaka najmanj 4.");
        }
        if (Konstante.ST_POLPOTEZ_DO_REMIJA < 2 || Konstante.ST_POLPOTEZ_DO_REMIJA % 2 == 1) {
            throw new RuntimeException("Vrednost konstante Konstante.ST_POLPOTEZ_DO_REMIJA mora biti soda in enaka najmanj 2.");
        }

        // izlušči parametre ukazne vrstice
        if (!Parametri.izlusci(args)) {
            System.exit(1);
        }
        Seansa seansa = Seansa.ustvari();
        if (seansa == null) {
            System.exit(1);
        }

        if (Parametri.jeGUI()) {
            // prični seanso v grafičnem vmesniku
            Gui gui = new Gui();
            gui.inicializiraj(seansa);

        } else {
            // prični seanso v tekstovnem vmesniku
            seansa.izvediT();
        }
    }
}
