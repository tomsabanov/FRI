
package ogrodje;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

/**
 * Objekt tega razreda predstavlja celotno `seanso' igre Damoida. Seansa je
 * sestavljena iz posameznih partij. Za pravilen potek in vzdrževanje stanja
 * posamezne partije skrbita objekta tipa Partija in GuiNadzornik.
 */
public class Seansa {

    /** osnovni podatki o igralcih;
     * [0]: trenutni beli igralec
     * [1]: trenutni črni igralec */
    private Igralec[] igralca;

    /** trenutna partija */
    private Partija partija;

    /** zaporedna številka trenutne partije */
    private int stevilkaPartije;

    /** dnevnik trenutne partije */
    private DnevnikPartije dnevnikPartije;

    /** ~[0]: skupno število točk igralca, ki je bil v prvi partiji beli;
     *  ~[1]: skupno število točk igralca, ki je bil v prvi partiji črni */
    private int[] stTock;

    /** 0, če je beli igralec v trenutni partiji tisti, ki je bil beli v prvi
     * partiji; 1 v nasprotnem primeru */
    private int ixBelega;

    /** Na podlagi podanih parametrov ustvari oba objekta tipa Igralec in vrne
     * objekt tipa Seansa. Če objektov podanih razredov ni mogoče ustvariti,
     * vrne null. */
    public static Seansa ustvari() {
        Igralec[] igralca = new Igralec[2];
        String[] razredaIgralcev = Parametri.vrniRazredaIgralcev();
        String ime0 = null;
        String ime1 = null;
        if (razredaIgralcev[0] == null && razredaIgralcev[1] == null) {
            ime0 = Igralec.IME_CLOVEKA_CC[0];
            ime1 = Igralec.IME_CLOVEKA_CC[1];
        } else if (razredaIgralcev[0] == null) { 
            ime0 = Igralec.IME_CLOVEKA_CS;
        } else if (razredaIgralcev[1] == null) {
            ime1 = Igralec.IME_CLOVEKA_CS;
        }
        igralca[0] = Igralec.ustvari(razredaIgralcev[0], ime0);
        igralca[1] = Igralec.ustvari(razredaIgralcev[1], ime1);
        if (igralca[0] == null || igralca[1] == null) {
            return null;
        }

        Seansa seansa = new Seansa(igralca);
        seansa.partija = new Partija(seansa);

        return seansa;
    }

    /**
     * Ustvari objekt, ki predstavlja seanso.
     * @param igralca  igralca v seansi
     */
    private Seansa(Igralec[] igralca) {
        this.igralca = igralca;
        this.stevilkaPartije = 1;
        this.stTock = new int[2];
        this.dnevnikPartije = new DnevnikPartije();
        this.ixBelega = 0;
    }

    /**
     * Posodobi atribute ob pričetku partije.
     * @return true natanko v primeru, če se nova partija lahko prične
     *         (ker njena zaporedna številka ni večja od ciljnega števila partij)
     */
    public boolean pricniPartijo() {
        int stPartij = Parametri.vrniSteviloPartij();
        if (this.strojVsStroj() && stPartij > 1 && this.stevilkaPartije > stPartij) {
            return false;
        }
        this.partija.pricni();
        this.dnevnikPartije.pricni(this.igralca, this.stevilkaPartije, this.ixBelega == 1);
        return true;
    }

    /** Vrne referenco na objekt, ki predstavlja trenutno partijo. */
    public Partija vrniPartijo() {
        return this.partija;
    }

    /** Vrne zaporedno številko trenutne partije. */
    public int vrniStevilkoPartije() {
        return this.stevilkaPartije;
    }

    /** Vrne dnevnik partije. */
    public DnevnikPartije vrniDnevnikPartije() {
        return this.dnevnikPartije;
    }

    /** Vrne oba igralca. */
    public Igralec[] vrniIgralca() {
        return this.igralca;
    }

    /** Vrne skupno število točk za prvega [0] in drugega [1] igralca. */
    public int[] tocke() {
        return this.stTock;
    }

    /** Vrne true natanko v primeru, ko stroj igra proti stroju. */
    public boolean strojVsStroj() {
        return this.igralca[0].jeStroj() && this.igralca[1].jeStroj();
    }

    /** Vrne true natanko v primeru, ko človek igra proti stroju. */
    public boolean clovekVsStroj() {
        return (!this.strojVsStroj() && this.vsajEnStroj());
    }

    /** Vrne true natanko v primeru, ko v igri sodeluje vsaj en stroj. */
    public boolean vsajEnStroj() {
        return (this.igralca[0].jeStroj() || this.igralca[1].jeStroj());
    }

    /** Vrne true natanko v primeru, ko človek igra proti človeku. */
    public boolean clovekVsClovek() {
        return !this.vsajEnStroj();
    }

    /** 
     * Posodobi atribute ob zaključku partije.
     * @param izid izid pravkar odigrane partije
     * @param izpisiTocke izpiši trenutno stanje točk na standardni izhod
     */
    private void zakljuciPartijo(Izid izid, boolean izpisiTocke) {
        this.stevilkaPartije++;
        this.stTock[0] += izid.vrniTocke(0);
        this.stTock[1] += izid.vrniTocke(1);
        this.dnevnikPartije.zakljuci(izid, this.stTock);
        this.zapisiDnevnikVDatoteko();

        if (izpisiTocke) {
            if (this.ixBelega == 0) {
                System.out.printf("Točke igralca %s: %d%n", this.igralca[0].ime(), this.stTock[0]);
                System.out.printf("Točke igralca %s: %d%n", this.igralca[1].ime(), this.stTock[1]);
            } else {
                System.out.printf("Točke igralca %s: %d%n", this.igralca[1].ime(), this.stTock[1]);
                System.out.printf("Točke igralca %s: %d%n", this.igralca[0].ime(), this.stTock[0]);
            }
        }

        // pripravi se na morebitno naslednjo partijo;
        // v primeru izmeničnega pričenjanja zamenjaj igralca med seboj
        if (Parametri.izmenicnoPricenjanje()) {
            Igralec zacasni = this.igralca[0];
            this.igralca[0] = this.igralca[1];
            this.igralca[1] = zacasni;

            int t = this.stTock[0];
            this.stTock[0] = this.stTock[1];
            this.stTock[1] = t;

            this.ixBelega = 1 - this.ixBelega;
        }
    }

    /** 
     * Posodobi atribute ob zaključku partije.
     * @param izid izid pravkar odigrane partije
     */
    public void zakljuciPartijo(Izid izid) {
        this.zakljuciPartijo(izid, false);
    }

    /** Vrne true natanko v primeru, če je seanse konec. */
    public boolean jeKonec() {
        return (this.strojVsStroj() && Parametri.vrniSteviloPartij() >= 1 && 
                this.stevilkaPartije > Parametri.vrniSteviloPartij());
    }

    /** Izvede seanso damoide v tekstovnem vmesniku. */
    public void izvediT() {
        this.dnevnikPartije = new DnevnikPartije();
        int steviloPartij = Math.max(Parametri.vrniSteviloPartij(), 1);

        this.partija = new Partija(this);
        this.stTock = new int[2];
        this.stevilkaPartije = 1;

        while (this.stevilkaPartije <= steviloPartij) {
            // odigraj partijo in jo sproti beleži v dnevnik
            System.out.printf("---------- Partija %d ----------%n", stevilkaPartije);
            System.out.printf("Beli: %s%n", this.igralca[0].ime());
            System.out.printf("Črni: %s%n", this.igralca[1].ime());

            this.dnevnikPartije.pricni(this.igralca, this.stevilkaPartije, this.ixBelega == 1);
            Izid izid = this.partija.odigrajT();
            this.zakljuciPartijo(izid, true);
            System.out.println();
        }
    }

    /** Shrani dnevnik v datoteko, če je parameter tako nastavljen. */
    private void zapisiDnevnikVDatoteko() {
        File dnevniskaDatoteka = Parametri.vrniDnevniskoDatoteko();
        if (dnevniskaDatoteka != null) {
            try (FileWriter fos = new FileWriter(dnevniskaDatoteka, true)) {
                fos.write(this.dnevnikPartije.toString());
            } catch (IOException ex) {
                System.err.printf("Napaka pri pisanju v datoteko %s%n", dnevniskaDatoteka);
            }
        }
    }
}
