
package ogrodje;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Deque;
import java.util.ArrayDeque;
import java.util.Arrays;

import skupno.Konstante;
import skupno.Poteza;
import skupno.Polje;

/**
 * Objekt tega razreda hrani in vzdržuje stanje v okviru posamezne partije.
 */
public class Partija {

    private static final int[][] DIAGONALNI_PREMIKI = {
        {-1, -1},
        {-1,  1},
        { 1, -1},
        { 1,  1},
    };

    private static final int[][] VSI_PREMIKI = {
        {-1, -1},
        {-1,  0}, 
        {-1,  1},
        { 0, -1},
        { 0,  1},
        { 1, -1},
        { 1,  0},
        { 1,  1},
    };

    /** seansa, katere del je partija `this' (seansa = zaporedje partij) */
    private final Seansa seansa;

    /** [vr][st]: žeton na polju (vr, st) */
    private final Zeton[][] sahovnica;

    /**
     * veljavnePoteze[polje]: množica polj, ki so v danem trenutku v eni
     * potezi dosegljiva s polja `polje'
     */
    private Map<Polje, Set<Polje>> veljavnePoteze;

    /** kdo je trenutno na potezi (0 ali 1) */
    private int naPotezi;

    /** zaporedna številka naslednje poteze */
    private int stPoteze;

    /** izid partije */
    private Izid izid;

    /** bralnik standardnega vhoda */
    private Scanner bralnik;

    /** true, če se je igra že pričela; false sicer */
    private boolean igraPoteka;

    /** Ustvari objekt, ki predstavlja partijo kot del podane seanse. */
    public Partija(Seansa seansa) {
        this.igraPoteka = false;
        this.seansa = seansa;
        this.sahovnica = new Zeton[Konstante.STRANICA][Konstante.STRANICA];
        this.veljavnePoteze = new HashMap<>();
    }

    /**
     * Prične partijo.
     */
    public void pricni() {
        this.naPotezi = 0;   // vedno prične beli
        this.stPoteze = 1;
        this.izid = null;

        // nastavi vsebino posameznih polj
        for (int vr = 0;  vr < Konstante.STRANICA;  vr++) {
            for (int st = 0;  st < Konstante.STRANICA;  st++) {
                this.izprazniPolje(vr, st);
            }
        }

        for (int vr = Konstante.STRANICA - 2;  vr < Konstante.STRANICA;  vr++) {
            for (int st = (vr + 1) % 2;  st < Konstante.STRANICA;  st += 2) {
                this.postaviZeton(vr, st, 0);
            }
        }

        for (int vr = 0;  vr < 2;  vr++) {
            for (int st = vr;  st < Konstante.STRANICA;  st += 2) {
                this.postaviZeton(vr, st, 1);
            }
        }

        // tvori vse veljavne poteze igralca na potezi
        this.zberiVeljavnePoteze();

        this.igraPoteka = true;
    }

    /** Vrne true natanko v primeru, če igra že poteka. */
    public boolean aliIgraPoteka() {
        return this.igraPoteka;
    }

    /** Vrne barvo igralca (0: beli; 1: črni), ki je trenutno na potezi. */
    public int kdoJeNaPotezi() {
        return this.naPotezi;
    }

    /** Vrne število odigranih potez. */
    public int vrniSteviloOdigranihPotez() {
        return this.stPoteze - 1;
    }

    /** Vrne oznako naslednje poteze.  Oznake si sledijo v zaporedju 1a, 1b,
     * 2a, 2b, 3a, 3b itd. */
    public String oznakaNaslednjePoteze() {
        return Razno.polpoteza2oznakaPoteze(this.stPoteze);
    }

    /** Vrne `true' natanko v primeru, če je podano polje prazno. */
    public boolean poljePrazno(Polje polje) {
        if (!polje.obstaja()) {
            return false;
        }
        return this.poljePrazno(polje.vrniVrstico(), polje.vrniStolpec());
    }

    /** Vrne `true' natanko v primeru, če je podano polje prazno. */
    public boolean poljePrazno(int vrstica, int stolpec) {
        return this.sahovnica[vrstica][stolpec] == Zeton.NEOBSTOJEC;
    }

    /** Vrne `true' natanko v primeru, če podano polje vsebuje žeton podanega
     * igralca. */
    public boolean jeZetonNaPolju(Polje polje, int igralec) {
        if (!polje.obstaja()) {
            return false;
        }
        return this.jeZetonNaPolju(polje.vrniVrstico(), polje.vrniStolpec(), igralec);
    }

    /** Vrne `true' natanko v primeru, če podano polje vsebuje žeton podanega
     * igralca. */
    public boolean jeZetonNaPolju(int vrstica, int stolpec, int igralec) {
        return (this.sahovnica[vrstica][stolpec].vrniVrednost() == igralec);
    }

    /** Če je polje prazno, vrne -1, sicer pa vrne indeks igralca, ki mu
     * pripada žeton na podanem polju.  */
    public int vsebinaPolja(Polje polje) {
        return this.vsebinaPolja(polje.vrniVrstico(), polje.vrniStolpec());
    }

    /** Če je polje prazno, vrne -1, sicer pa vrne indeks igralca, ki mu
     * pripada žeton na podanem polju.  */
    public int vsebinaPolja(int vrstica, int stolpec) {
        return this.sahovnica[vrstica][stolpec].vrniVrednost();
    }

    /** Na podano polje postavi žeton podanega igralca. */
    private void postaviZeton(Polje polje, int igralec) {
        this.postaviZeton(polje.vrniVrstico(), polje.vrniStolpec(), igralec);
    }

    /** Na podano polje postavi žeton podanega igralca. */
    private void postaviZeton(int vrstica, int stolpec, int igralec) {
        this.sahovnica[vrstica][stolpec] = Zeton.zaIgralca(igralec);
    }

    /** Izprazni podano polje. */
    private void izprazniPolje(Polje polje) {
        this.izprazniPolje(polje.vrniVrstico(), polje.vrniStolpec());
    }

    /** Izprazni podano polje. */
    private void izprazniPolje(int vrstica, int stolpec) {
        this.sahovnica[vrstica][stolpec] = Zeton.NEOBSTOJEC;
    }

    /**
     * Vrne true natanko v primeru, če je podana poteza za igralca na potezi legalna.
     * @param izbranoPolje pravkar izbrano polje
     */
    public boolean preveriLegalnost(Poteza poteza) {
        if (!this.igraPoteka) {
            return false;
        }

        Polje zacetno = poteza.vrniZacetno();
        Polje koncno = poteza.vrniKoncno();

        // nekaj hitrih testov ...

        // preveri, ali sta polji različni
        if (zacetno.equals(koncno)) {
            return false;
        }

        // preveri, ali se polji sploh nahajata na šahovnici
        if (!this.poljeObstaja(zacetno) || !this.poljeObstaja(koncno)) {
            return false;
        }

        // preveri vsebino začetnega in končnega polja
        if (!this.jeZetonNaPolju(zacetno, this.naPotezi) || !this.poljePrazno(koncno)) {
            return false;
        }

        // preveri, ali se končno polje poteze nahaja v seznamu veljavnih
        // ciljnih polj za podano začetni polje
        return this.veljavnePoteze.containsKey(zacetno) &&
            this.veljavnePoteze.get(zacetno).contains(koncno);
    }

    /** Vrne `true' natanko v primeru, če se podano polje nahaja na šahovnici.  */
    public boolean poljeObstaja(Polje polje) {
        return this.poljeObstaja(polje.vrniVrstico(), polje.vrniStolpec());
    }

    /** Vrne `true' natanko v primeru, če polje v podani vrstici in stolpcu
     * nahaja na šahovnici.  */
    public boolean poljeObstaja(int vrstica, int stolpec) {
        return (vrstica >= 0 && stolpec >= 0 &&
                vrstica < Konstante.STRANICA && stolpec < Konstante.STRANICA);
    }

    /**
     * Posodobi stanje partije po odigrani (veljavni) potezi.
     * @return če se partija po odigrani potezi zaključi, vrne izid, sicer pa
     *         vrne null
     */
    public Izid posodobiPoPotezi(Poteza poteza) {
        this.seansa.vrniDnevnikPartije().dodajPotezo(this.naPotezi, poteza);

        // posodobi šahovnico
        Polje zacetno = poteza.vrniZacetno();
        Polje koncno = poteza.vrniKoncno();
        this.izprazniPolje(zacetno);
        this.postaviZeton(koncno, this.naPotezi);

        // preveri zmago
        if (this.jeZmagal(this.naPotezi)) {
            this.izid = Izid.zmaga(this.naPotezi);
            return this.izid;
        }

        // preveri remi
        if (this.stPoteze >= Konstante.ST_POLPOTEZ_DO_REMIJA) {
            this.izid = Izid.remi();
            return this.izid;
        }

        // zamenjaj stran in povečaj števec potez
        this.naPotezi = 1 - this.naPotezi;
        this.stPoteze++;

        // ponovno izdelaj seznam veljavnih potez
        this.zberiVeljavnePoteze();

        return null;
    }

    /** Vrne true natanko v primeru, če se je trenutna igra že končala.  */
    public boolean jeKonec() {
        return (this.izid != null);
    }

    /** Umetno nastavi izid, npr. po odigrani neveljavni potezi. */
    public void nastaviIzid(Izid izid) {
        this.izid = izid;
    }

    /** Vrne izid partije. */
    public Izid vrniIzid() {
        return this.izid;
    }

    /** Predčasno zaključi partijo. */
    public void predcasnoKoncaj() {
        this.izid = Izid.PREDCASEN_ZAKLJUCEK;
    }

    /**
     * Vrne `true' natanko v primeru, če je podani igralec dosegel zmagovito
     * razporeditev žetonov.
     */
    public boolean jeZmagal(int igralec) {
        int[] vrstici = (igralec == 0) ?
            new int[]{0, 1} :
            new int[]{Konstante.STRANICA - 1, Konstante.STRANICA - 2};

        // beli:                  // črni:
        // - * - * - * - *        // - - - - - - - -
        // * - * - * - * -        // - - - - - - - -
        // - - - - - - - -        // - - - - - - - -
        // - - - - - - - -        // - - - - - - - -
        // - - - - - - - -        // - - - - - - - -
        // - - - - - - - -        // - - - - - - - -
        // - - - - - - - -        // * - * - * - * -
        // - - - - - - - -        // - * - * - * - *

        for (int ix = 0;  ix < 2;  ix++) {
            int vr = vrstici[ix];
            for (int st = (ix + 1) % 2;  st < Konstante.STRANICA;  st += 2) {
                if (!this.jeZetonNaPolju(vr, st, igralec)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Zbere vse veljavne poteze v podatkovno strukturo this.veljavnePoteze.
     */
    private void zberiVeljavnePoteze() {
        this.veljavnePoteze.clear();

        for (int vr = 0;  vr < Konstante.STRANICA;  vr++) {
            for (int st = 0;  st < Konstante.STRANICA;  st++) {
                Polje polje = new Polje(vr, st);
                if (this.jeZetonNaPolju(polje, this.naPotezi)) {
                    this.dodajVeljavnePoteze(polje);
                }
            }
        }
    }

    /**
     * Doda veljavne poteze s podanega začetnega polja v zbirko
     * this.veljavnePoteze.
     */
    private void dodajVeljavnePoteze(Polje zacetnoPolje) {
        // polja, ki smo jih v postopku iskanja potez že obiskali
        Set<Polje> obiskana = new HashSet<>();

        // pomiki
        for (int[] premik: DIAGONALNI_PREMIKI) {
            Polje koncnoPolje = zacetnoPolje.plus(premik[0], premik[1]);
            if (this.poljeObstaja(koncnoPolje) && this.poljePrazno(koncnoPolje)) {
                this.dodajVeljavnoPotezo(zacetnoPolje, koncnoPolje);
                obiskana.add(koncnoPolje);
            }
        }

        // skoki (preišči graf skokov po postopku iskanja najprej v širino)
        Deque<List<Polje>> vrsta = new ArrayDeque<>();
        vrsta.addLast(Arrays.asList(zacetnoPolje));

        while (! vrsta.isEmpty()) {
            List<Polje> veja = vrsta.removeFirst();
            for (Polje ciljSkoka: this.poisciSkoke(veja.get(veja.size() - 1))) {
                if (!obiskana.contains(ciljSkoka)) {
                    List<Polje> novaVeja = new ArrayList<>(veja);
                    novaVeja.add(ciljSkoka);
                    vrsta.addLast(novaVeja);
                    obiskana.add(ciljSkoka);
                    this.dodajVeljavnoPotezo(zacetnoPolje, ciljSkoka);
                }
            }
        }
    }

    /**
     * Doda premik s podanega začetnega polja na podano končno polje v seznam
     * veljavnih potez za začetno polje.
     */
    private void dodajVeljavnoPotezo(Polje zacetno, Polje koncno) {
        if (!this.veljavnePoteze.containsKey(zacetno)) {
            this.veljavnePoteze.put(zacetno, new HashSet<Polje>());
        }
        this.veljavnePoteze.get(zacetno).add(koncno);
    }

    /**
     * Vrne množico polj, dosegljivih z enkratnim skokom s podanega začetnega
     * polja.
     */
    private Set<Polje> poisciSkoke(Polje zacetnoPolje) {
        Set<Polje> rezultat = new HashSet<>();

        for (int[] premik: VSI_PREMIKI) {
            int vrPremik = premik[0];
            int stPremik = premik[1];
            Polje vmesnoPolje = zacetnoPolje.plus(vrPremik, stPremik);
            Polje ciljnoPolje = zacetnoPolje.plus(2 * vrPremik, 2 * stPremik);

            if (this.poljeObstaja(vmesnoPolje) && this.poljeObstaja(ciljnoPolje) &&
                    !this.poljePrazno(vmesnoPolje) && this.poljePrazno(ciljnoPolje)) {
                rezultat.add(ciljnoPolje);
            }
        }
        return rezultat;
    }

    /**
     * Odigra celotno partijo v tekstovnem vmesniku in vrne njen izid.
     */
    public Izid odigrajT() {
        Igralec[] igralca = this.seansa.vrniIgralca();

        // obvesti stroja o pričetku igre
        this.pricni();
        igralca[0].novaPartija(true);
        igralca[1].novaPartija(false);

        this.naPotezi = 0;
        this.stPoteze = 1;

        this.bralnik = new Scanner(System.in);

        // potek partije
        while (this.izid == null) {

            // omogoči igralcu na potezi, da izbere polje
            Poteza poteza = this.izberiPotezoT();
            if (poteza == null) {
                this.izid = Izid.neveljavnaPoteza(this.naPotezi);
                return this.izid;
            }

            // obvesti nasprotnega igralca o potezi
            igralca[1 - this.naPotezi].sprejmiPotezo(poteza);

            // posodobi stanje igre po odigrani potezi
            this.posodobiPoPotezi(poteza);
        }

        // razglasitev izida
        int zmagovalec = this.izid.zmagovalec();
        String barvaZmagovalca = (zmagovalec < 0) ? (DnevnikPartije.NEZNAN) : (DnevnikPartije.BARVA[zmagovalec]);
        String imeZmagovalca = (zmagovalec < 0) ? (DnevnikPartije.NEZNAN) : (igralca[zmagovalec].ime());
        System.out.printf("Zmagovalec: %s (%s) %n", barvaZmagovalca, imeZmagovalca);
        System.out.printf("Opis izida: %s%n", this.izid.toString());

        if (izid.jeRemi()) {
            igralca[0].rezultat(0);
            igralca[1].rezultat(0);
        } else {
            igralca[izid.zmagovalec()].rezultat(1);
            igralca[1 - izid.zmagovalec()].rezultat(-1);
        }
        return this.izid;
    }

    /**
     * Tekstovni vmesnik: pridobi človekovo oz. strojevo izbiro poteze.
     * @return izbrana poteza
     */
    private Poteza izberiPotezoT() {
        Igralec[] igralca = this.seansa.vrniIgralca();

        if (igralca[this.naPotezi].jeClovek()) {
            // človek na potezi
            return this.clovekovaIzbiraT();

        } else {
            // stroj na potezi
            Poteza izbranaPoteza = igralca[this.naPotezi].izberiPotezo();
            if (this.poIzbiriPotezeStrojaT(izbranaPoteza)) {
                return izbranaPoteza;
            }
            return null;
        }
    }

    /** Tekstovni vmesnik: s standardnega vhoda prebere človekovo izbiro poteze. */
    private Poteza clovekovaIzbiraT() {
        Poteza izbranaPoteza;
        boolean prvic = true;

        Igralec[] igralca = this.seansa.vrniIgralca();
        String[] imeni = {igralca[0].ime(), igralca[1].ime()};

        do {
            if (prvic) {
                prvic = false;
            } else {
                System.out.println("Neveljavna poteza!");
            }

            System.out.printf("(%s) %s (%s) --> indeks vrstice zacetnega polja = ",
                    Razno.polpoteza2oznakaPoteze(this.stPoteze), DnevnikPartije.BARVA[this.naPotezi], imeni[this.naPotezi]);
            int zacVr = this.bralnik.nextInt();
            System.out.printf("(%s) %s (%s) --> indeks stolpca zacetnega polja = ",
                    Razno.polpoteza2oznakaPoteze(this.stPoteze), DnevnikPartije.BARVA[this.naPotezi], imeni[this.naPotezi]);
            int zacSt = this.bralnik.nextInt();
            Polje zacetnoPolje = new Polje(zacVr, zacSt);

            System.out.printf("(%s) %s (%s) --> indeks vrstice koncnega polja = ",
                    Razno.polpoteza2oznakaPoteze(this.stPoteze), DnevnikPartije.BARVA[this.naPotezi], imeni[this.naPotezi]);
            int konVr = this.bralnik.nextInt();
            System.out.printf("(%s) %s (%s) --> indeks stolpca koncnega polja = ",
                    Razno.polpoteza2oznakaPoteze(this.stPoteze), DnevnikPartije.BARVA[this.naPotezi], imeni[this.naPotezi]);
            int konSt = this.bralnik.nextInt();
            Polje koncnoPolje = new Polje(konVr, konSt);

            izbranaPoteza = new Poteza(zacetnoPolje, koncnoPolje);
        } while (!this.preveriLegalnost(izbranaPoteza));

        return izbranaPoteza;
    }

    /** Tekstovni vmesnik: izpiše izbrano potezo in preveri njegovo veljavnost. */
    private boolean poIzbiriPotezeStrojaT(Poteza poteza) {
        Igralec[] igralca = this.seansa.vrniIgralca();
        String[] imeni = {igralca[0].ime(), igralca[1].ime()};

        System.out.printf("(%s) %s: %s%n",
                Razno.polpoteza2oznakaPoteze(this.stPoteze),
                DnevnikPartije.BARVA[this.naPotezi], poteza.toString());
        if (!this.preveriLegalnost(poteza)) {
            System.out.println("Neveljavna izbira!");
            return false;
        }
        return true;
    }
}
