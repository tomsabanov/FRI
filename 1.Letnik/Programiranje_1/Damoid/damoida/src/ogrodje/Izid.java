
package ogrodje;

/**
 * Objekt tega razreda predstavlja enega od možnih izidov partije.
 */
public enum Izid {

    ZMAGA_PRVEGA("Beli je dosegel ciljno razporeditev žetonov", 2, 0),
    ZMAGA_DRUGEGA("Črni je dosegel ciljno razporeditev žetonov", 0, 2),
    REMI("Remi", 1, 1),
    PREKORACITEV_CASA_PRVEGA("Beli je prekoračil čas", 0, 2),
    PREKORACITEV_CASA_DRUGEGA("Črni je prekoračil čas", 2, 0),
    NEVELJAVNA_POTEZA_PRVEGA("Beli je odigral neveljavno potezo", 0, 2),
    NEVELJAVNA_POTEZA_DRUGEGA("Črni je odigral neveljavno potezo", 2, 0),
    PREDCASEN_ZAKLJUCEK("Predčasen zaključek", 0, 0);

    private final static String BELI = "beli";
    private final static String CRNI = "črni";
    private final static String NEZNAN = "?";

    /** besedni opis izida */
    private final String pojasnilo;

    /** število točk, ki jih ob izidu this prejme prvi igralec (igralec z indeksom 0) */
    private final int tockePrvega;

    /** število točk, ki jih ob izidu this prejme drugi igralec (igralec z indeksom 1) */
    private final int tockeDrugega;

    private Izid(String pojasnilo, int tockePrvega, int tockeDrugega) {
        this.pojasnilo = pojasnilo;
        this.tockePrvega = tockePrvega;
        this.tockeDrugega = tockeDrugega;
    }

    /** Vrne besedni opis izida. */
    public String vrniPojasnilo() {
        return this.pojasnilo;
    }

    /** 
     * Vrne število točk, ki jih je dosegel igralec s podanim indeksom.
     * @param igralec  indeks igralca (0 ali 1)
     * @return število točk (0, 1 ali 2)
     */
    public int vrniTocke(int igralec) {
        return (igralec == 0) ? (this.tockePrvega) : (this.tockeDrugega);
    }

    /**
     * Vrne true natanko v primeru, če je igralec s podanim indeksom zmagal.
     * @param igralec  indeks igralca (0 ali 1)
     * @return true, če je podani igralec zmagal; false, če ni
     */
    public boolean jeZmagal(int igralec) {
        return (this.vrniTocke(igralec) == 2);
    }

    /**
     * Vrne  0, če izid this predstavlja zmago belega;
     *       1, če predstavlja zmago črnega;
     *      -1, če predstavlja remi.
     */
    public int zmagovalec() {
        if (this.tockePrvega == 2) {
            return 0;
        }
        if (this.tockeDrugega == 2) {
            return 1;
        }
        return -1;
    }

    /**
     * Vrne true natanko v primeru, če se je igra iztekla z remijem.
     * @return true, če se je igra iztekla z remijem; false, če je nekdo zmagal
     */
    public boolean jeRemi() {
        return (this == REMI);
    }

    /**
     * Vrne true natanko v primeru, če objekt this predstavlja izid z `navadno'
     * zmago (torej ne zmage po prekoračitvi časa ali po neveljavni potezi).
     */
    public boolean jeNavadnaZmaga() {
        return (this == ZMAGA_PRVEGA || this == ZMAGA_DRUGEGA);
    }

    /** 
     * V odvisnosti od izida vrne niz "1 : 0", "0 : 1", "Remi" ali "Napaka".
     */
    public String rezultat() {
        if (this.tockePrvega == 2) {
            return "1 : 0";
        }
        if (this.tockeDrugega == 2) {
            return "0 : 1";
        }
        if (this.tockePrvega == 1) {
            return "Remi";
        }
        return "Napaka";
    }

    /** 
     * Vrne objekt tipa Izid, ki predstavlja zmago igralca s podanim indeksom. 
     * @param igralec  indeks igralca (0 ali 1)
     */
    public static Izid zmaga(int igralec) {
        return (igralec == 0) ? (ZMAGA_PRVEGA) : (ZMAGA_DRUGEGA);
    }

    /** 
     * Vrne objekt tipa Izid, ki predstavlja remi.
     */
    public static Izid remi() {
        return REMI;
    }

    /** 
     * Vrne objekt tipa Izid, ki predstavlja zaključek partije zaradi 
     * neveljavne poteze, ki jo je izbral igralec s podanim indeksom.
     * @param igralec  indeks igralca (0 ali 1)
     */
    public static Izid neveljavnaPoteza(int igralec) {
        return (igralec == 0) ? (NEVELJAVNA_POTEZA_PRVEGA) : (NEVELJAVNA_POTEZA_DRUGEGA);
    }

    /** 
     * Vrne objekt tipa Izid, ki predstavlja zaključek partije zaradi 
     * prekoračitve časa, ki se je primerila igralcu s podanim indeksom.
     * @param igralec  indeks igralca (0 ali 1)
     */
    public static Izid prekoracitevCasa(int igralec) {
        return (igralec == 0) ? (PREKORACITEV_CASA_PRVEGA) : (PREKORACITEV_CASA_DRUGEGA);
    }

    /** Vrne besedni opis izida. */
    @Override
    public String toString() {
        return this.pojasnilo;
    }
}
