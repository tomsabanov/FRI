
package ogrodje;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;
import javax.swing.Timer;

import skupno.Polje;
import skupno.Poteza;
import skupno.Konstante;

/** 
 * Objekt tega razreda predstavlja ploščo, ki prikazuje stanje igre in omogoča
 * uporabniku, da vnaša poteze.
 */

public class GuiIgralnaPlosca extends JPanel
        implements MouseListener, MouseMotionListener {

    /** barva ozadja */
    private static final Color B_OZADJE = new Color(0, 0, 64);

    /** barva polja s sodo vsoto koordinat */
    public static final Color B_SODO_POLJE = new Color(224, 224, 255);

    /** barva polja z liho vsoto koordinat */
    public static final Color B_LIHO_POLJE = B_SODO_POLJE.darker();

    /** barva figure posameznega igralca */
    public static final Color[] B_FIGURA = { Color.WHITE, Color.BLACK };

    /** barva roba figure posameznega igralca */
    private static final Color[] B_FIGURA_ROB = { Color.GRAY, Color.LIGHT_GRAY };

    /** barva silhuete figure posameznega igralca */
    private static final Color[] B_SILHUETA = { Color.WHITE, Color.BLACK };

    /** barva okvirja na poljih s sodo vsoto koordinat */
    private static final Color B_OSVETLITEV_POLJA_SODO = Color.BLUE;

    /** barva okvirja na poljih z liho vsoto koordinat */
    private static final Color B_OSVETLITEV_POLJA_LIHO = Color.BLUE;

    /** barva križca, ki označuje začetno polje nazadnje odigrane strojeve
     * poteze, na poljih s sodo vsoto koordinat */
    private static final Color B_KRIZEC_SODO = Color.BLUE;

    /** barva križca, ki označuje začetno polje nazadnje odigrane strojeve
     * poteze, na poljih z liho vsoto koordinat */
    private static final Color B_KRIZEC_LIHO = Color.BLUE;

    /** barva osvetljene figure za posameznega igralca */
    private static final Color[] B_FIGURA_OSVETLJENA = {
        new Color(180, 255, 180), new Color(0, 0, 200)
    };

    /** barva roba osvetljene figure za posameznega igralca */
    private static final Color[] B_FIGURA_ROB_OSVETLJENA = {
        new Color(0, 128, 0), new Color(255, 255, 255) 
    };

    /** razmerje med debelino roba in premerom figure */
    private static final double R_ROB_FIGURA = 0.2;

    /** razmerje med premerom figure in stranico polja */
    private static final double R_FIGURA_POLJE = 0.5;

    /** razmerje med stranico osvetlitvenega kvadrata in stranico polja */
    private static final double R_OSVETLITEV_POLJE = 0.9;

    /** razmerje med polovico dolžine pravokotne projekcije križca na
     * vodoravno ali navpično os in stranico polja */
    private static final double R_KRIZEC_POLJE = 0.1;

    /** razmerje med debelino osvetlitvenega okvirja in stranico polja */
    private static final float R_DEBELINA_OSVETLITVE_POLJE = 0.025f;

    /** razmerje med debelino silhuete žetona in stranico polja */
    private static final float R_DEBELINA_SILHUETE_POLJE = 0.05f;

    /** razmerje med širino notranje silhuete žetona in stranico polja */
    private static final float R_ODSEK_SILHUETE_POLJE_1 = 0.10f;

    /** razmerje med širino zunanje silhuete žetona in stranico polja */
    private static final float R_ODSEK_SILHUETE_POLJE_2 = 0.15f;

    private static final float R_OKVIR_POVRSINE_POLJE = 0.0f;

    /** maksimalno trajanje osvetlitve v milisekundah */
    private static final int TRAJANJE_OSVETLITVE = 2000;

    /** posebna konstanta, ki označuje neveljavno polje
     *  (na ta način se lahko znebimo precej preverjanj enakosti z null) */
    private static final Polje NEVELJAVNO_POLJE = new Polje(-1, -1);

    /** referenca na povezovalni objekt */
    private final GuiNadzornik nadzornik;

    /** trenutno osvetljena poteza (= zadnja računalnikova poteza) */
    private Poteza osvetljenaPoteza;

    /** polje, na katero trenutno kaže miškin kazalec */
    private Polje poljePodMisko;

    /** polje, na katero smo pritisnili z miško */
    private Polje pritisnjenoPolje;

    /** polje, na katero trenutno kaže miškin kazalec, ko je neko polje že
     * pritisnjeno */
    private Polje poskusnoCiljnoPolje;

    /** true, ko je miškin gumb pritisnjen; false sicer */
    private boolean miskinGumbPritisnjen;

    /** trenutna dolžina stranice polja */
    private double dPolje;

    /** trenutni koordinati zgornjega levega oglišča igralne površine */
    private double xPovrsina, yPovrsina;
    
    /** (trenutno neaktiven) časovnik, ki po preteku določenega časa odstrani osvetlitev polja */
    private Timer casovnikIztekaOsvetlitve;

    public GuiIgralnaPlosca(GuiNadzornik nadzornik) {
        this.nadzornik = nadzornik;
        this.setBackground(B_OZADJE);

        this.casovnikIztekaOsvetlitve = new Timer(TRAJANJE_OSVETLITVE, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GuiIgralnaPlosca ip = GuiIgralnaPlosca.this;
                ip.osvetljenaPoteza = null;
                ip.repaint();
            }
        });
        this.casovnikIztekaOsvetlitve.setRepeats(false);
    }

    /** 
     * Tole bi lahko šlo v konstruktor, toda stavek
     * this.addMouseListener(this); sproži svarilo `leaking this in
     * constructor', ker med izvajanjem konstruktorja objekt še ni v celoti
     * inicializiran
     */
    public void inicializiraj() {
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
    }

    /**
     * Vrne širino igralne plošče pri podani dolžini stranice polja.
     * @param dp dolžina stranice polja
     */
    public static double sirina(double dp) {
        return ((Konstante.STRANICA + 2.0 * R_OKVIR_POVRSINE_POLJE) * dp);
    }

    /**
     * Vrne višino igralne plošče pri podani dolžini stranice polja.
     * @param dp dolžina stranice polja
     */
    public static double visina(double dp) {
        return sirina(dp);
    }

    /** Ta metoda se pokliče ob vsakem pričetku igre. */
    public void novaPartija() {
        this.miskinGumbPritisnjen = false;
        this.osvetljenaPoteza = null;
        this.poljePodMisko = NEVELJAVNO_POLJE;
        this.pritisnjenoPolje = NEVELJAVNO_POLJE;
        this.poskusnoCiljnoPolje = NEVELJAVNO_POLJE;
    }

    /** 
     * V tej metodi, ki se pokliče ob vsaki spremembi velikosti plošče this,
     * se izračuna nova velikost polja.
     */
    public void spremembaVelikosti() {
        // izračunaj dolžino stranice polja
        double wPlosca = (double) this.getWidth();
        double hPlosca = (double) this.getHeight();
        double dPoljeW = wPlosca / (Konstante.STRANICA + 2.0 * R_OKVIR_POVRSINE_POLJE);
        double dPoljeH = hPlosca / (Konstante.STRANICA + 2.0 * R_OKVIR_POVRSINE_POLJE);
        this.dPolje = Math.min(dPoljeW, dPoljeH);

        // mere igralne površine
        double dIgralnaPovrsina = Konstante.STRANICA * this.dPolje;
        this.xPovrsina = (wPlosca - dIgralnaPovrsina) / 2.0;
        this.yPovrsina = (hPlosca - dIgralnaPovrsina) / 2.0;
    }

    /** Ta metoda se kliče vsakokrat, ko je treba osvežiti vsebino plošče. */
    @Override
    protected void paintComponent(Graphics g0) {
        Graphics2D g = GuiRazno.nastaviAntialiasing(g0, true);
        super.paintComponent(g);

        Partija partija = this.nadzornik.vrniPartijo();
        if (partija == null || !partija.aliIgraPoteka()) {
            return;
        }

        int naPotezi = partija.kdoJeNaPotezi();

        for (int vr = 0;  vr < Konstante.STRANICA;  vr++) {
            for (int st = 0;  st < Konstante.STRANICA;  st++) {
                // nariši ozadje polja
                Polje polje = new Polje(vr, st);
                this.narisiPolje(g, polje);

                // nariši morebitno figuro na trenutnem polju
                if (!partija.poljePrazno(polje)) {
                    this.narisiFiguro(g, polje, partija.vsebinaPolja(polje), 
                            this.jeFiguraOsvetljena(polje));
                }
            }
        }

        if (this.poskusnoCiljnoPolje.obstaja()) {
            // nariši silhueto figure na morebitnem ciljnem polju poteze
            // (uporabnik je že pritisnil na začetno polje, sedaj pa se miškin
            // kazalec nahaja na enem od veljavnih ciljnih polj)
            Poteza poteza = new Poteza(this.pritisnjenoPolje, this.poskusnoCiljnoPolje);
            if (partija.preveriLegalnost(poteza)) {
                this.narisiSilhueto(g, naPotezi, this.poskusnoCiljnoPolje);
            }
        }
    }

    /**
     * Nariše ozadje podanega polja.
     */
    private void narisiPolje(Graphics2D g, Polje polje) {
        boolean bLiho = (polje.ostanek() == 1);
        g.setColor(bLiho ? B_LIHO_POLJE : B_SODO_POLJE);
        Rectangle2D okvir = this.okvirPolja(polje);
        g.fill(okvir);

        double xc = okvir.getCenterX();
        double yc = okvir.getCenterY();
        Stroke copic = g.getStroke();
        g.setStroke(new BasicStroke(Math.max(1.0f, R_DEBELINA_OSVETLITVE_POLJE * (float) okvir.getWidth())));

        if (this.jePoljeUokvirjeno(polje)) {
            double w = okvir.getWidth() * R_OSVETLITEV_POLJE;
            double h = okvir.getHeight() * R_OSVETLITEV_POLJE;
            Rectangle2D notranjiOkvir = new Rectangle2D.Double(xc - w / 2, yc - h / 2, w, h);
            g.setColor(bLiho ? B_OSVETLITEV_POLJA_LIHO : B_OSVETLITEV_POLJA_SODO);
            g.draw(notranjiOkvir);

        } else if (this.jeNaPoljuKrizec(polje)) {
            double d = okvir.getWidth() * R_KRIZEC_POLJE;
            g.setColor(bLiho ? B_KRIZEC_LIHO : B_KRIZEC_SODO);
            g.draw(new Line2D.Double(xc - d, yc - d, xc + d, yc + d));
            g.draw(new Line2D.Double(xc - d, yc + d, xc + d, yc - d));
        }
        g.setStroke(copic);
    }

    /**
     * Na podanem polju nariše figuro, ki pripada podanemu igralcu.
     * @param igralec indeks igralca (0: prvi; 1: drugi)
     * @param osvetljena ali je figura osvetljena
     */
    private void narisiFiguro(Graphics2D g, Polje polje, int igralec, boolean osvetljena) {
        Rectangle2D okvir = this.okvirPolja(polje);
        double wPolje = okvir.getWidth();
        double dFigura = R_FIGURA_POLJE * wPolje;
        double xFigura = okvir.getX() + (wPolje - dFigura) / 2.0;
        double yFigura = okvir.getY() + (wPolje - dFigura) / 2.0;
        Shape krog = new Ellipse2D.Double(xFigura, yFigura, dFigura, dFigura);

        Color bFigura = osvetljena ? B_FIGURA_OSVETLJENA[igralec] :
            B_FIGURA[igralec];
        Color bRob = osvetljena ? B_FIGURA_ROB_OSVETLJENA[igralec] :
            B_FIGURA_ROB[igralec];

        g.setColor(bFigura);
        g.fill(krog);
        Stroke staroPisalo = g.getStroke();
        g.setStroke(new BasicStroke((float) (R_ROB_FIGURA * dFigura)));
        g.setColor(bRob);
        g.draw(krog);
        g.setStroke(new BasicStroke((float) (R_ROB_FIGURA * dFigura / 2.0)));
        g.setColor(bFigura);
        g.draw(krog);
        g.setStroke(staroPisalo);
    }

    private void narisiSilhueto(Graphics2D g, int igralec, Polje polje) {
        Stroke staroPisalo = g.getStroke();

        Rectangle2D okvir = this.okvirPolja(polje);
        double wPolje = okvir.getWidth();
        double dFigura = R_FIGURA_POLJE * wPolje;
        double xFigura = okvir.getX() + (wPolje - dFigura) / 2.0;
        double yFigura = okvir.getY() + (wPolje - dFigura) / 2.0;
        Shape krog = new Ellipse2D.Double(xFigura, yFigura, dFigura, dFigura);

        g.setColor(B_SILHUETA[igralec]);
        float debelina = (float) (R_DEBELINA_SILHUETE_POLJE * wPolje);
        float odsek1 = (float) (R_ODSEK_SILHUETE_POLJE_1 * wPolje);
        float odsek2 = (float) (R_ODSEK_SILHUETE_POLJE_2 * wPolje);
        g.setStroke(new BasicStroke(debelina, BasicStroke.CAP_SQUARE,
                    BasicStroke.JOIN_MITER, 10.0f,
                    new float[]{odsek1, odsek2}, 0.0f));
        g.draw(krog);
        g.setStroke(staroPisalo);
    }

    /**
     * Vrne true natanko v primeru, če je podano polje uokvirjeno.  To se
     * zgodi, ko gre za že pritisnjeno začetno polje poteze ali pa ko gre za
     * končno polje trenutno osvetljene poteze.
     */
    private boolean jePoljeUokvirjeno(Polje polje) {
        return (this.pritisnjenoPolje.equals(polje) || 
                this.osvetljenaPoteza != null &&
                     this.osvetljenaPoteza.vrniKoncno().equals(polje));
    }

    /**
     * Vrne true natanko v primeru, če se na podanem polju nahaja križec.  To se
     * zgodi, ko gre za začetno polje trenutno osvetljene poteze.
     */
    private boolean jeNaPoljuKrizec(Polje polje) {
        return (this.osvetljenaPoteza != null &&
                this.osvetljenaPoteza.vrniZacetno().equals(polje));
    }

    /**
     * Vrne true natanko v primeru, če je figura na podanem polju
     * osvetljena.
     */
    private boolean jeFiguraOsvetljena(Polje polje) {
        if (!this.pritisnjenoPolje.obstaja()) {
            return this.poljePodMisko.equals(polje);
        }
        return this.pritisnjenoPolje.equals(polje);
    }

    /**
     * Vrne položaj in velikost upodobitve podanega polja na igralni
     * površini.
     */
    private Rectangle2D okvirPolja(Polje polje) {
        double d = this.dPolje;
        double x = this.xPovrsina + polje.vrniStolpec() * d;
        double y = this.yPovrsina + polje.vrniVrstico() * d;
        return new Rectangle2D.Double(x, y, d, d);
    }

    private Point2D srediscePolja(Polje polje) {
        Rectangle2D okvir = this.okvirPolja(polje);
        return new Point2D.Double(okvir.getCenterX(), okvir.getCenterY());
    }

    /**
     * Vrne polje šahovnice, ki se nahaja na podanih koordinatah.  Če se
     * položaj nahaja izven šahovnice, vrne NEVELJAVNO_POLJE.
     */
    private Polje polozaj2polje(int x, int y) {
        int vrstica = (int) ((y - this.yPovrsina) / this.dPolje);
        int stolpec = (int) ((x - this.xPovrsina) / this.dPolje);
        Partija partija = this.nadzornik.vrniPartijo();
        Polje polje = new Polje(vrstica, stolpec);
        return (partija.poljeObstaja(polje) ? polje : NEVELJAVNO_POLJE);
    }

    private void osveziPolje(Polje polje) {
        if (polje.obstaja()) {
            this.repaint(this.okvirPolja(polje).getBounds());
        }
    }

    public void osvetliStrojevoPotezo(Poteza poteza) {
        // NOTE: če želiš časovnik za odstranitev osvetlitve aktivirati,
        // odkomentiraj sledečo vrstico
        // this.casovnikIztekaOsvetlitve.restart();
        this.osvetljenaPoteza = poteza;
        this.repaint();
    }

    @Override
    public void mouseClicked(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}

    /**
     * Ta metoda se pokliče, ko uporabnik pritisne na igralno površino
     */
    @Override
    public void mousePressed(MouseEvent e) {
        this.casovnikIztekaOsvetlitve.stop();
        this.osvetljenaPoteza = null;
        this.repaint();

        Partija partija = this.nadzornik.vrniPartijo();

        if (partija.jeKonec() || this.nadzornik.strojNaPotezi()) {
            return;
        }

        Polje novoPolje = this.polozaj2polje(e.getX(), e.getY());
        boolean potezaIzvrsena = false;

        int naPotezi = partija.kdoJeNaPotezi();

        if (novoPolje.obstaja() && this.pritisnjenoPolje.obstaja()) {
            // uporabnik je že pritisnil na začetno polje poteze (= this.pritisnjenoPolje); 
            // preveri, ali je poteza this.pritisnjenoPolje -> novoPolje legalna;
            // če je, sproži njeno izvedbo
            Poteza poteza = new Poteza(this.pritisnjenoPolje, novoPolje);
            if (partija.preveriLegalnost(poteza)) {
                this.nadzornik.izvrsiVeljavnoPotezo(poteza);
                this.pritisnjenoPolje = NEVELJAVNO_POLJE;
                potezaIzvrsena = true;
            }
        }
        if (!potezaIzvrsena) {
            // pravkar pritisnjeno polje je novo this.pritisnjenoPolje
            this.pritisnjenoPolje = partija.jeZetonNaPolju(novoPolje, naPotezi)
                                  ? novoPolje
                                  : NEVELJAVNO_POLJE;
        }

        // izbriši oznake, povezane zgolj s premikanjem miške
        this.poljePodMisko = NEVELJAVNO_POLJE;
        this.poskusnoCiljnoPolje = NEVELJAVNO_POLJE;

        // osveži igralno površino
        this.repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseDragged(MouseEvent e) {}

    /**
     * Ta metoda se odzove na premik miške po igralni površini.
     */
    @Override
    public void mouseMoved(MouseEvent e) {
        Partija partija = this.nadzornik.vrniPartijo();

        if (partija.jeKonec() || this.nadzornik.strojNaPotezi()) {
            return;
        }
        // na potezi je človek ...

        Polje novoPolje = this.polozaj2polje(e.getX(), e.getY());
        if (this.poljePodMisko.equals(novoPolje)) {
            return;
        }
        // trenutno polje pod miško se razlikuje od prejšnjega ...

        this.nadzornik.posredujPolozajMiske(novoPolje);

        Polje staroPoskusnoPolje = this.poskusnoCiljnoPolje;
        this.poskusnoCiljnoPolje = 
            (this.pritisnjenoPolje.obstaja() &&
             partija.preveriLegalnost(new Poteza(this.pritisnjenoPolje, novoPolje)))
            ? (novoPolje)
            : (NEVELJAVNO_POLJE);

        int naPotezi = partija.kdoJeNaPotezi();
        Polje staroPoljePodMisko = this.poljePodMisko;
        this.poljePodMisko = 
            (this.poskusnoCiljnoPolje.obstaja() || !partija.jeZetonNaPolju(novoPolje, naPotezi))
            ? NEVELJAVNO_POLJE
            : novoPolje;

        this.osveziPolje(staroPoskusnoPolje);
        this.osveziPolje(staroPoljePodMisko);
        this.osveziPolje(novoPolje);
    }
}
