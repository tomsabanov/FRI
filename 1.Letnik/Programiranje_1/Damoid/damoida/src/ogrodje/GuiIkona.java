
package ogrodje;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

/**
 * Ta razred vsebuje statično metodo za izdelavo ikone programa.
 */

public class GuiIkona {

    /** velikost ikone */
    private static final int VELIKOST = 256;

    /** razmerje med polmerom žetona in dolžino stranice polja */
    private static final double R_ZETON_POLJE = 0.32;

    /** število polj po eni dimenziji */
    private static final int ST_POLJ = 4;

    /** položaji črnih žetonov (vrstica, stolpec) */
    private static final int[][] CRNI_ZETONI = {{0, 0}, {1, 1}, {2, 2}};

    /** položaji belih žetonov (vrstica, stolpec) */
    private static final int[][] BELI_ZETONI = {{0, 1}, {1, 2}, {2, 1}};

    private static final Color[] B_ZETON = {Color.BLACK, Color.WHITE};

    private static final Color[] B_ZETON_ROB = {Color.BLACK, Color.BLACK};

    /** Ustvari in vrne sliko ikone. */
    public static Image ustvari() {
        BufferedImage slika = new BufferedImage(VELIKOST, VELIKOST, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = slika.createGraphics();
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(0, 0, VELIKOST, VELIKOST);
        narisiSliko(g, VELIKOST, VELIKOST);
        g.dispose();
        return slika;
    }

    /**
     * Nariše sliko ikone podane velikosti v podanem grafičnem kontekstu. 
     * @param g grafični kontekst
     * @param wSlika širina ikone
     * @param hSlika višina ikone
     */
    public static void narisiSliko(Graphics2D g, double wSlika, double hSlika) {
        GuiRazno.nastaviAntialiasing(g, true);

        double dSlika = (double) wSlika;
        double dPolje = dSlika / ST_POLJ;
        double rPolje = dPolje / 2.0;
        double rZeton = R_ZETON_POLJE * dPolje;

        double y = 0.0;
        for (int vr = 0;  vr < ST_POLJ;  vr++) {
            double x = 0.0;
            for (int st = 0;  st < ST_POLJ;  st++) {
                int ostanek = (vr + st) % 2; 
                g.setColor((ostanek == 0) ? GuiIgralnaPlosca.B_SODO_POLJE : GuiIgralnaPlosca.B_LIHO_POLJE);
                g.fill(new Rectangle2D.Double(x, y, dPolje, dPolje));
                if (vr < 2 && ostanek == 0 || vr >= ST_POLJ - 2 && ostanek == 1) {
                    g.setColor(GuiIgralnaPlosca.B_FIGURA[1 - ostanek]);
                    g.fill(new Ellipse2D.Double(x + rPolje - rZeton, y + rPolje - rZeton, 2.0 * rZeton, 2.0 * rZeton));
                }
                x += dPolje;
            }
            y += dPolje;
        }
    }
}
