
package ogrodje;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;

/**
 * Splošne zadeve, povezane z grafičnim uporabniškim vmesnikom.
 */

public class GuiRazno {

    /** 
     * V podanem grafičnem kontekstu vklopi oz. izklopi glajenje robov za lepši
     * izris.
     * @param nastavi true: vklopi; false: izklopi
     */
    public static Graphics2D nastaviAntialiasing(Graphics g0, boolean nastavi) {
        Graphics2D g = (Graphics2D) g0;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
                (nastavi ? RenderingHints.VALUE_ANTIALIAS_ON : RenderingHints.VALUE_ANTIALIAS_OFF));
        return g;
    }

    /** V podanem grafičnem kontekstu `nariše' podano besedilo na sredino
     * podanega pravokotnika. */
    public static void narisiBesedilo(Graphics g, Rectangle2D rect, String besedilo) {
        FontMetrics fm = g.getFontMetrics();
        double wBesedilo = fm.stringWidth(besedilo);
        double xBesedilo = rect.getX() + (rect.getWidth() - wBesedilo) / 2;
        double yBesedilo = rect.getY() + (rect.getHeight() + visinaPisave(fm)) / 2;
        g.drawString(besedilo, Razno.ri(xBesedilo), Razno.ri(yBesedilo));
    }

    /** V podanem grafičnem kontekstu `nariše' podano besedilo na levi rob
     * podanega pravokotnika. */
    public static void narisiBesediloLevo(Graphics g, Rectangle2D okvir, String besedilo) {
        FontMetrics fm = g.getFontMetrics();
        double xBesedilo = okvir.getX();
        double yBesedilo = okvir.getY() + (okvir.getHeight() + visinaPisave(fm)) / 2;
        g.drawString(besedilo, Razno.ri(xBesedilo), Razno.ri(yBesedilo));
    }

    /** V podanem grafičnem kontekstu `nariše' podano besedilo na desni rob
     * podanega pravokotnika. */
    public static void narisiBesediloDesno(Graphics g, Rectangle2D okvir, String besedilo) {
        FontMetrics fm = g.getFontMetrics();
        double xBesedilo = okvir.getX() + okvir.getWidth() - fm.stringWidth(besedilo);
        double yBesedilo = okvir.getY() + (okvir.getHeight() + visinaPisave(fm)) / 2;
        g.drawString(besedilo, Razno.ri(xBesedilo), Razno.ri(yBesedilo));
    }

    /** Vrne nekoliko realnejšo višino pisave, ki ji pripada podani objekt 
     * tipa FontMetrics. */
    public static double visinaPisave(FontMetrics fm) {
        return (0.9 * fm.getAscent());
    }
}
