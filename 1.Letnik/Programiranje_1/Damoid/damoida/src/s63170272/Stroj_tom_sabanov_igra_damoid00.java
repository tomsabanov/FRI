package s63170272;

import skupno.Stroj;
import skupno.Polje;
import skupno.Poteza;
import skupno.Konstante;
import java.util.Random;

import java.util.*;

public class Stroj_tom_sabanov_igra_damoid00 implements Stroj {

  private static final int[][] DIAGONALNI_PREMIKI = {
    {-1, -1},
    {-1,  1},
    { 1, -1},
    { 1,  1},
  };

  private static final int[][] VSI_PREMIKI = {
    {-1, -1},
    {-1,  0},
    {-1,  1},
    { 0, -1},
    { 0,  1},
    { 1, -1},
    { 1,  0},
    { 1,  1},
  };
  static int winVal = 1000000;
  static int loseVal = -1000000;
  private static int maxDepth = 4;
  private static int tempDepth;
  private static int id_depth;
  private static int player;
  private int[][] moji_zetoni = new int[8][2];
  private int[][] nasprotnik_zetoni = new int[8][2];

  private int[][] jaz_cilj = new int[8][2];
  private int[][] nasprotnik_cilj = new int[8][2];


  private Poteza AlphaBetaDecision() {
    boolean[][] position = this.zasedeno;
    Map<Polje, Set<Polje>> veljavnePoteze = new HashMap<Polje, Set<Polje>>();

    veljavnePoteze = this.getAllPossibleMoves(veljavnePoteze,0);

    Poteza poteza = null;
  /*  for (Map.Entry<Polje, Set<Polje>> entry : veljavnePoteze.entrySet()) {
      //System.out.println(entry.getKey() + ":" + entry.getValue().toString());
      String pot = "";
      Iterator iter = entry.getValue().iterator();
      for(Polje p : entry.getValue()){
          pot+=p.vrniVrstico() + " " + p.vrniStolpec() + ",   ";
      }
      System.out.println("Zeton " + entry.getKey().vrniVrstico() + " " + entry.getKey().vrniStolpec() + " poteze......:" + pot);
    }*/
    for (id_depth = 1; id_depth < maxDepth; id_depth++) {
          int bestIdx = -1;
          int bestVal = loseVal;
          int currVal = 0;
          tempDepth = id_depth;
          // Loop through each move
          for (Map.Entry<Polje, Set<Polje>> entry : veljavnePoteze.entrySet()){
            Polje zacetnoPolje = entry.getKey();
            int i_zetona = 0;
            for(int i = 0;i<8;i++){
              if(this.moji_zetoni[i][0] == zacetnoPolje.vrniVrstico() && this.moji_zetoni[i][1] == zacetnoPolje.vrniStolpec()){
                i_zetona = i;
                break;
              }
            }
            for(Polje koncnoPolje : entry.getValue()){
              this.moji_zetoni[i_zetona][0] = koncnoPolje.vrniVrstico();
              this.moji_zetoni[i_zetona][1] = koncnoPolje.vrniStolpec();

              currVal = minValue(position, 1,loseVal, winVal);

              this.moji_zetoni[i_zetona][0] = zacetnoPolje.vrniVrstico();
              this.moji_zetoni[i_zetona][1] = zacetnoPolje.vrniStolpec();

              if (currVal >= bestVal) {
                    poteza = new Poteza(zacetnoPolje,koncnoPolje);
                    bestVal = currVal;
              }

            }
          }
          if (bestVal == winVal || (id_depth+1) == maxDepth){
            return poteza;
          }
    }
    return null;
  }
  private int minValue(boolean[][] position, int currDepth,int alpha, int beta) {
       if (currDepth > this.tempDepth) {
           return utility(currDepth) * (-1);
       }
       Map<Polje, Set<Polje>> veljavnePoteze = new HashMap<Polje, Set<Polje>>();
       veljavnePoteze = this.getAllPossibleMoves(veljavnePoteze,1);
      /* System.out.println("Veljavne poteze za nasprotnika");
       for (Map.Entry<Polje, Set<Polje>> entry : veljavnePoteze.entrySet()) {
         //System.out.println(entry.getKey() + ":" + entry.getValue().toString());
         String pot = "";
         Iterator iter = entry.getValue().iterator();
         for(Polje p : entry.getValue()){
             pot+=p.vrniVrstico() + " " + p.vrniStolpec() + ",   ";
         }
         System.out.println("Zeton " + entry.getKey().vrniVrstico() + " " + entry.getKey().vrniStolpec() + " poteze......:" + pot);
       }
*/
       int util = winVal;
       int curr = 0;

       // Loop through each move
       for (Map.Entry<Polje, Set<Polje>> entry : veljavnePoteze.entrySet()){
         Polje zacetnoPolje = entry.getKey();
         int i_zetona = 0;
         for(int i = 0;i<8;i++){
           if(this.nasprotnik_zetoni[i][0] == zacetnoPolje.vrniVrstico() && this.nasprotnik_zetoni[i][1] == zacetnoPolje.vrniStolpec()){
             i_zetona = i;
             break;
           }
         }
         for(Polje koncnoPolje : entry.getValue()){
           this.nasprotnik_zetoni[i_zetona][0] = koncnoPolje.vrniVrstico();
           this.nasprotnik_zetoni[i_zetona][1] = koncnoPolje.vrniStolpec();

           curr = maxValue(position, currDepth + 1, alpha, beta);

           this.nasprotnik_zetoni[i_zetona][0] = zacetnoPolje.vrniVrstico();
           this.nasprotnik_zetoni[i_zetona][1] = zacetnoPolje.vrniStolpec();

           if (curr < util){
             util = curr;
           }

           if (util < alpha){
             return util;
           }
           if (util < beta){
             beta = util;
           }

         }
       }
       return util;
  }
  private  int maxValue(boolean[][] position, int currDepth, int alpha, int beta) {
      if (currDepth > this.tempDepth) {
          return utility(currDepth);
      }
       int util = loseVal;
       int curr = 0;
       Map<Polje, Set<Polje>> veljavnePoteze = new HashMap<Polje, Set<Polje>>();
       veljavnePoteze = this.getAllPossibleMoves(veljavnePoteze,0);
      /* System.out.println("Veljavne poteze zame");
       for (Map.Entry<Polje, Set<Polje>> entry : veljavnePoteze.entrySet()) {
         //System.out.println(entry.getKey() + ":" + entry.getValue().toString());
         String pot = "";
         Iterator iter = entry.getValue().iterator();
         for(Polje p : entry.getValue()){
             pot+=p.vrniVrstico() + " " + p.vrniStolpec() + ",   ";
         }
         System.out.println("Zeton " + entry.getKey().vrniVrstico() + " " + entry.getKey().vrniStolpec() + " poteze......:" + pot);
       }*/
       for (Map.Entry<Polje, Set<Polje>> entry : veljavnePoteze.entrySet()){
         Polje zacetnoPolje = entry.getKey();
         int i_zetona = 0;
        for(int i = 0;i<8;i++){
                if(this.moji_zetoni[i][0] == zacetnoPolje.vrniVrstico() && this.moji_zetoni[i][1] == zacetnoPolje.vrniStolpec()){
                    i_zetona = i;
                    break;
                }
         }
         for(Polje koncnoPolje : entry.getValue()){
           //tukajle bomo spreminjali  moji zetoni....
           this.moji_zetoni[i_zetona][0] = koncnoPolje.vrniVrstico();
           this.moji_zetoni[i_zetona][1] = koncnoPolje.vrniStolpec();

           curr = minValue(position, currDepth + 1, alpha, beta);

           this.moji_zetoni[i_zetona][0] = zacetnoPolje.vrniVrstico();
           this.moji_zetoni[i_zetona][1] = zacetnoPolje.vrniStolpec();

           if (curr > util){
             util = curr;
           }
           if (util > beta){
             return util;
           }
           if (util > alpha){
             alpha = util;
           }
         }
       }
       return util;
   }

  private int utility(int d) {
    if(this.jeZmagal(0)){
      //zmagam jaz
      if (d <= this.tempDepth){
        this.tempDepth = d;
        return winVal;
      } else {
        return loseVal;
      }
    }
    else if(this.jeZmagal(1)){
      return loseVal;
    }
    return evaluate();
  }
  private int evaluate(){
    int razdalja_jaz = 0;
    int razdalja_nasprotnik = 0;
    int s = 0;
    int b = 0;
    for(int i = 0; i<8;i++){
      int x_jaz = this.moji_zetoni[i][0];
      int y_jaz = this.moji_zetoni[i][1];

      int cilj_x_jaz = this.jaz_cilj[i][0];
      int cilj_y_jaz = this.jaz_cilj[i][1];

      int x_n = this.nasprotnik_zetoni[i][0];
      int y_n = this.nasprotnik_zetoni[i][1];
      int cilj_x_n = this.nasprotnik_cilj[i][0];
      int cilj_y_n = this.nasprotnik_cilj[i][1];

      if(x_jaz == cilj_x_jaz && y_jaz==cilj_y_jaz){
        s++;
      }
      if(x_n == cilj_x_n && y_n == cilj_y_n){
        b++;
      }

      if(Math.abs(x_jaz - cilj_x_jaz)< Math.abs(x_n -cilj_x_n)){
        s*=s;
      }
      else{
        b*=b;
      }

      if(Math.abs(x_jaz - cilj_x_jaz + y_jaz - cilj_y_jaz)<4){
        s*=s;
      }

      razdalja_jaz+=Math.abs(x_jaz - cilj_x_jaz)*s*s + Math.abs(y_jaz - cilj_y_jaz)*s ;
      razdalja_nasprotnik+=Math.abs(x_n - cilj_x_n)*b*b + Math.abs(y_n - cilj_y_n)*b;
    }
    return Math.abs((razdalja_jaz + s*100)-(b*100 +razdalja_nasprotnik));
  }
  private boolean jeZmagal(int igralec) {
      int[] vrstici = (igralec == 0) ?
          new int[]{0, 1} :
          new int[]{Konstante.STRANICA - 1, Konstante.STRANICA - 2};

      // beli:                  // črni:
      // - * - * - * - *        // - - - - - - - -
      // * - * - * - * -        // - - - - - - - -
      // - - - - - - - -        // - - - - - - - -
      // - - - - - - - -        // - - - - - - - -
      // - - - - - - - -        // - - - - - - - -
      // - - - - - - - -        // - - - - - - - -
      // - - - - - - - -        // * - * - * - * -
      // - - - - - - - -        // - * - * - * - *

      for (int ix = 0;  ix < 2;  ix++) {
          int vr = vrstici[ix];
          for (int st = (ix + 1) % 2;  st < Konstante.STRANICA;  st += 2) {
              if (!this.jeZetonNaPolju(vr, st, igralec)) {
                  return false;
              }
          }
      }
      return true;
  }
  private boolean jeZetonNaPolju(int vr, int st, int igralec){
    // igralec = 0 -> moji moji_zetoni
    //igralec  = 1 -> nasprotnik_zetoni
    if(igralec == 1){
      //nasprotnik_zetoni
      for(int i = 0; i<8;i++){
        if(this.nasprotnik_zetoni[i][0] == vr && this.nasprotnik_zetoni[i][1] == st){
          return true;
        }
      }
      return false;
    }
    else{
      //toni
      for(int i = 0; i<8;i++){
        if(this.moji_zetoni[i][0] == vr && this.moji_zetoni[i][1] == st){
          return true;
        }
      }
      return false;
    }
  }

  private Map<Polje,Set<Polje>> getAllPossibleMoves(Map<Polje,Set<Polje>> moves, int igralec){
    for (int i= 0; i< 8;  i++) {
            Polje zacetnoPolje;
            if(igralec == 1){
              //nasprotnik zetoni
                if(this.nasprotnik_zetoni[i][0] != this.nasprotnik_cilj[i][0]  || this.nasprotnik_zetoni[i][1] != this.nasprotnik_cilj[i][1]){
                  zacetnoPolje = new Polje(this.nasprotnik_zetoni[i][0], this.nasprotnik_zetoni[i][1]);
                  // polja, ki smo jih v postopku iskanja potez že obiskali
                  Set<Polje> obiskana = new HashSet<>();
                  // pomiki
                  for (int[] premik: DIAGONALNI_PREMIKI) {
                      Polje koncnoPolje = zacetnoPolje.plus(premik[0], premik[1]);
                      if (this.poljeObstaja(koncnoPolje) && this.poljePrazno(koncnoPolje)==true) {
                          if (!moves.containsKey(zacetnoPolje)) {
                              moves.put(zacetnoPolje, new HashSet<Polje>());
                          }
                          moves.get(zacetnoPolje).add(koncnoPolje);

                          obiskana.add(koncnoPolje);
                      }
                  }
                  // skoki (preišči graf skokov po postopku iskanja najprej v širino)
                  Deque<List<Polje>> vrsta = new ArrayDeque<>();
                  vrsta.addLast(Arrays.asList(zacetnoPolje));
                  while (! vrsta.isEmpty()) {
                      List<Polje> veja = vrsta.removeFirst();
                      for (Polje ciljSkoka: this.poisciSkoke(veja.get(veja.size() - 1))) {
                          if (!obiskana.contains(ciljSkoka)) {
                              List<Polje> novaVeja = new ArrayList<>(veja);
                              novaVeja.add(ciljSkoka);
                              vrsta.addLast(novaVeja);
                              obiskana.add(ciljSkoka);
                              if (!moves.containsKey(zacetnoPolje)) {
                                  moves.put(zacetnoPolje, new HashSet<Polje>());
                              }
                              moves.get(zacetnoPolje).add(ciljSkoka);
                          }
                      }
                  }
                }
            }
            else{
              //moji zetoni
              if(this.moji_zetoni[i][0] != this.jaz_cilj[i][0]  || this.moji_zetoni[i][1] != this.jaz_cilj[i][1]){
                zacetnoPolje = new Polje(this.moji_zetoni[i][0], this.moji_zetoni[i][1]);
                // polja, ki smo jih v postopku iskanja potez že obiskali
                Set<Polje> obiskana = new HashSet<>();
                // pomiki

                // skoki (preišči graf skokov po postopku iskanja najprej v širino)
                Deque<List<Polje>> vrsta = new ArrayDeque<>();
                vrsta.addLast(Arrays.asList(zacetnoPolje));
                while (! vrsta.isEmpty()) {
                    List<Polje> veja = vrsta.removeFirst();
                    for (Polje ciljSkoka: this.poisciSkoke(veja.get(veja.size() - 1))) {
                        if (!obiskana.contains(ciljSkoka)) {
                            List<Polje> novaVeja = new ArrayList<>(veja);
                            novaVeja.add(ciljSkoka);
                            vrsta.addLast(novaVeja);
                            obiskana.add(ciljSkoka);
                            if (!moves.containsKey(zacetnoPolje)) {
                                moves.put(zacetnoPolje, new HashSet<Polje>());
                            }
                              moves.get(zacetnoPolje).add(ciljSkoka);

                            // ce ni nobene poteze, gre pogledat za 1 premik naprej, ce tam ni potez, gre gledat levo desno, ce pa se tam ni, gre pogledat
                            // poteze kjer gre nazaj za max 2
                        }
                    }
                }

                if(moves.isEmpty()){
                  for (int[] premik: DIAGONALNI_PREMIKI) {
                       Polje koncnoPolje = zacetnoPolje.plus(premik[0], premik[1]);
                       if (this.poljeObstaja(koncnoPolje) && this.poljePrazno(koncnoPolje)==true) {
                           if (!moves.containsKey(zacetnoPolje)) {
                               moves.put(zacetnoPolje, new HashSet<Polje>());
                           }
                           moves.get(zacetnoPolje).add(koncnoPolje);

                           obiskana.add(koncnoPolje);
                       }
                   }
                }
              }
            }
    }
    return moves;
  }


  /**
   * Vrne množico polj, dosegljivih z enkratnim skokom s podanega začetnega
   * polja.
   */
  private Set<Polje> poisciSkoke(Polje zacetnoPolje) {
      Set<Polje> rezultat = new HashSet<>();
      for (int[] premik: VSI_PREMIKI) {
          int vrPremik = premik[0];
          int stPremik = premik[1];
          Polje vmesnoPolje = zacetnoPolje.plus(vrPremik, stPremik);
          Polje ciljnoPolje = zacetnoPolje.plus(2 * vrPremik, 2 * stPremik);

          if (this.poljeObstaja(vmesnoPolje) && this.poljeObstaja(ciljnoPolje) &&
                  !this.poljePrazno(vmesnoPolje) && this.poljePrazno(ciljnoPolje)) {
              rezultat.add(ciljnoPolje);
          }
      }
      return rezultat;
  }

  /** Vrne `true' natanko v primeru, če se podano polje nahaja na šahovnici.  */
  public boolean poljeObstaja(Polje polje) {
      return this.poljeObstaja(polje.vrniVrstico(), polje.vrniStolpec());
  }

  /** Vrne `true' natanko v primeru, če polje v podani vrstici in stolpcu
   * nahaja na šahovnici.  */
  public boolean poljeObstaja(int vrstica, int stolpec) {
      return (vrstica >= 0 && stolpec >= 0 &&
              vrstica < Konstante.STRANICA && stolpec < Konstante.STRANICA);
  }

  /** Vrne `true' natanko v primeru, če je podano polje prazno. */
  public boolean poljePrazno(Polje polje) {
          return this.poljePrazno(polje.vrniVrstico(), polje.vrniStolpec());
  }

      /** Vrne `true' natanko v primeru, če je podano polje prazno. */
  public boolean poljePrazno(int vrstica, int stolpec) {
          return !this.zasedeno[vrstica][stolpec];
  }


    /** število žetonov je enako dolžini stranice */
    private static final int ST_ZETONOV = Konstante.STRANICA;

    /** true, če igram kot beli; false, če igram kot črni */
    private boolean semBeli;


    /**
     * zasedeno[vr][st]: true natanko v primeru, ko je polje v vrstici z
     * indeksom vr in stolpcu z indeksom st zasedeno.  Na zasedenem polju (vr,
     * st) z liho vsoto koordinat se nahaja beli žeton, na zasedenem polju s
     * sodo vsoto koordinat pa črni žeton.
     */
    private boolean[][] zasedeno;
    /**
     * V konstruktorju inicializiram generator naključnih števil in tabelo
     * zasedenosti polj.
     */
    public Stroj_tom_sabanov_igra_damoid00() {
        this.zasedeno = new boolean[Konstante.STRANICA][Konstante.STRANICA];
    }

    /**
     * Ob pričetku partije nastavim svojo barvo in ponastavim tabelo
     * zasedenosti polj.
     *
     * @param beli  true, če bom igral kot beli; false, če bom igral kot črni
     */
    @Override
    public void novaPartija(boolean beli) {
        this.semBeli = beli;
        // tukaj sem dobil vse svoje zetone v svojo tabelo (ni treba pri vsaki potezi iskati skozi celo tabelo  - 64 polj, tukaj jih imam 8....)
        if(!beli){
          int i = 0;
          for (int vr = 0;  vr < 2;  vr++) {
              for (int st = 0;  st < Konstante.STRANICA;  st++) {
                if((vr+st)%2 == 0){
                  this.moji_zetoni[i][0] = vr;
                  this.moji_zetoni[i][1] = st;
                  this.jaz_cilj[i][0] = Math.abs(7-vr);
                  this.jaz_cilj[i][1] = Math.abs(7-st);
                  i++;
                }
              }
          }
          i=0;
          for (int vr = 6;  vr < Konstante.STRANICA;  vr++) {
              for (int st = 0;  st < Konstante.STRANICA;  st++) {
                if((vr+st)%2 == 1){
                  this.nasprotnik_zetoni[i][0] = vr;
                  this.nasprotnik_zetoni[i][1] = st;
                  this.nasprotnik_cilj[i][0] = Math.abs(7-vr);
                  this.nasprotnik_cilj[i][1] = Math.abs(7-st);
                  i++;
                }
              }
          }
        }
        else{
          int i = 0;
          for (int vr = 0;  vr < 2;  vr++) {
              for (int st = 0;  st < Konstante.STRANICA;  st++) {
                if((vr+st)%2 == 0){
                  this.nasprotnik_zetoni[i][0] = vr;
                  this.nasprotnik_zetoni[i][1] = st;
                  this.nasprotnik_cilj[i][0] = Math.abs(7-vr);
                  this.nasprotnik_cilj[i][1] = Math.abs(7-st);
                  i++;
                }
              }
          }
          i=0;
          for (int vr = 6;  vr < Konstante.STRANICA;  vr++) {
              for (int st = 0;  st < Konstante.STRANICA;  st++) {
                if((vr+st)%2 == 1){
                  this.moji_zetoni[i][0] = vr;
                  this.moji_zetoni[i][1] = st;
                  this.jaz_cilj[i][0] = Math.abs(7-vr);
                  this.jaz_cilj[i][1] = Math.abs(7-st);
                  i++;
                }
              }
          }
        }
        // Na začetku so zasedena vsa polja v spodnjih dveh vrsticah, ki imajo
        // liho vsoto vrstice in stolpca (beli žetoni), in vsa polja v
        // zgornjih dveh vrsticah, ki imajo sodo vsoto vrstice in stolpca
        // (črni žetoni).
        for (int vr = 0;  vr < Konstante.STRANICA;  vr++) {
            for (int st = 0;  st < Konstante.STRANICA;  st++) {
                Polje polje = new Polje(vr, st);
                this.nastaviZasedenost(polje,
                        (vr >= Konstante.STRANICA - 2 && polje.ostanek() == 1) ||
                        (vr < 2 && polje.ostanek() == 0));
            }
        }
    }

    /**
     * Ko sem na vrsti za potezo, naključno izberem enega od pomikov ali
     * skokov v smeri proti nasprotni strani šahovnice.  Če take poteze ni,
     * izberem enega od pomikov ali skokov v smeri proti moji strani
     * šahovnice.  Večkratnih skokov žal ne obvladam.  Tudi skokov levo in
     * desno ne izvajam.
     *
     * @return izbrana poteza
     */
    @Override
    public Poteza izberiPotezo(long preostaliCas) {
        Poteza p =  this.AlphaBetaDecision();
        for(int i = 0; i<8;i++){
          if(this.moji_zetoni[i][0] == p.vrniZacetno().vrniVrstico() && this.moji_zetoni[i][1] == p.vrniZacetno().vrniStolpec()){
            this.zasedeno[this.moji_zetoni[i][0]][this.moji_zetoni[i][1]] = false;
            this.moji_zetoni[i][0] = p.vrniKoncno().vrniVrstico();
            this.moji_zetoni[i][1] = p.vrniKoncno().vrniStolpec();
            this.zasedeno[this.moji_zetoni[i][0]][this.moji_zetoni[i][1]] = true;
          }
        }
        return p;
    }

    /**
     * Ko potezo odigra moj nasprotnik, posodobim tabelo zasedenosti polj.
     *
     * @param poteza  poteza, ki jo je izbral moj nasprotnik
     */
    @Override
    public void sprejmiPotezo(Poteza poteza) {
        this.nastaviZasedenost(poteza.vrniZacetno(), false);
        this.nastaviZasedenost(poteza.vrniKoncno(), true);
        for(int i = 0; i<8;i++){
          if(this.nasprotnik_zetoni[i][0] == poteza.vrniZacetno().vrniVrstico() && this.nasprotnik_zetoni[i][1] == poteza.vrniZacetno().vrniStolpec()){
            this.nasprotnik_zetoni[i][0] = poteza.vrniKoncno().vrniVrstico();
            this.nasprotnik_zetoni[i][1] = poteza.vrniKoncno().vrniStolpec();
            break;
          }
        }
    }

    /**
     * Ko se partija zaključi, mi ni treba storiti ničesar, saj mi točke niso
     * pomembne ...
     *
     * @param izid  izid partije
     */
    @Override
    public void rezultat(int izid) {
    }

    /**
     * Vrne true natanko v primeru, če je podano polje zasedeno.
     */
    private boolean jeZasedeno(Polje polje) {
        return this.zasedeno[polje.vrniVrstico()][polje.vrniStolpec()];
    }

    /**
     * Nastavi zasedenost podanega polja na podano vrednost.
     */
    private void nastaviZasedenost(Polje polje, boolean vrednost) {
        this.zasedeno[polje.vrniVrstico()][polje.vrniStolpec()] = vrednost;
    }

}
