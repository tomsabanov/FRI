---------- Partija 1 ----------
Beli: Človek
Črni: s63170272.Stroj_tom_sabanov_igra_damoid00
Čas pričetka: 09.01.2018 23:03:57.179
Število polpotez: 75
(1a) beli: 6/3 -> 5/2
(1b) črni: 0/4 -> 2/6
(2a) beli: 5/2 -> 4/3
(2b) črni: 1/7 -> 3/5
(3a) beli: 6/1 -> 5/2
(3b) črni: 1/1 -> 2/2
(4a) beli: 7/0 -> 6/1
(4b) črni: 0/6 -> 2/4
(5a) beli: 7/2 -> 5/0
(5b) črni: 2/4 -> 3/3
(6a) beli: 5/2 -> 4/1
(6b) črni: 3/3 -> 4/4
(7a) beli: 5/0 -> 3/2
(7b) črni: 2/2 -> 3/3
(8a) beli: 6/1 -> 5/2
(8b) črni: 2/6 -> 3/7
(9a) beli: 5/2 -> 3/0
(9b) črni: 0/2 -> 1/1
(10a) beli: 3/2 -> 2/1
(10b) črni: 1/5 -> 2/6
(11a) beli: 3/0 -> 1/2
(11b) črni: 1/1 -> 3/1
(12a) beli: 4/1 -> 3/2
(12b) črni: 3/3 -> 4/2
(13a) beli: 4/3 -> 3/4
(13b) črni: 3/1 -> 4/0
(14a) beli: 3/4 -> 2/3
(14b) črni: 0/0 -> 1/1
(15a) beli: 6/5 -> 5/4
(15b) črni: 1/3 -> 2/2
(16a) beli: 5/4 -> 4/3
(16b) črni: 3/5 -> 5/3
(17a) beli: 7/4 -> 6/5
(17b) črni: 1/1 -> 3/1
(18a) beli: 6/7 -> 5/6
(18b) črni: 4/2 -> 6/4
(19a) beli: 6/5 -> 5/4
(19b) črni: 6/4 -> 7/5
(20a) beli: 5/6 -> 4/5
(20b) črni: 4/4 -> 5/5
(21a) beli: 7/6 -> 6/5
(21b) črni: 3/7 -> 4/6
(22a) beli: 5/4 -> 3/6
(22b) črni: 5/5 -> 6/4
(23a) beli: 4/3 -> 3/4
(23b) črni: 2/2 -> 3/3
(24a) beli: 3/6 -> 2/5
(24b) črni: 4/6 -> 5/5
(25a) beli: 2/1 -> 1/0
(25b) črni: 4/0 -> 5/1
(26a) beli: 1/2 -> 0/1
(26b) črni: 5/5 -> 7/3
(27a) beli: 3/2 -> 2/1
(27b) črni: 5/1 -> 6/0
(28a) beli: 2/3 -> 1/2
(28b) črni: 3/1 -> 4/2
(29a) beli: 2/1 -> 0/3
(29b) črni: 2/6 -> 3/5
(30a) beli: 4/5 -> 2/3
(30b) črni: 6/0 -> 7/1
(31a) beli: 2/5 -> 1/4
(31b) črni: 3/3 -> 4/4
(32a) beli: 2/3 -> 0/5
(32b) črni: 4/4 -> 6/2
(33a) beli: 3/4 -> 2/5
(33b) črni: 3/5 -> 4/6
(34a) beli: 6/5 -> 5/4
(34b) črni: 4/6 -> 5/5
(35a) beli: 5/4 -> 4/3
(35b) črni: 4/2 -> 5/1
(36a) beli: 4/3 -> 3/4
(36b) črni: 5/5 -> 6/6
(37a) beli: 3/4 -> 1/6
(37b) črni: 6/6 -> 7/7
(38a) beli: 2/5 -> 0/7
Zmagovalec: beli (Človek) 
Opis izida: Beli je dosegel ciljno razporeditev žetonov
Točke igralca Človek: 2
Točke igralca s63170272.Stroj_tom_sabanov_igra_damoid00: 0

