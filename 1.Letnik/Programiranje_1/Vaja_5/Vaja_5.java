import java.util.Scanner;
import java.util.Random;
public class Vaja_5{

    public static void main(String[] args){
        //kmp();
        //smreka();
        dvoboj();
    }
    public static void dvoboj(){
        Scanner sc = new Scanner(System.in);
        int s = sc.nextInt();
        int d = sc.nextInt();
        int a = sc.nextInt();
        int b =sc.nextInt();
        
        
        int zmaga_a = 0;
        int zmaga_b = 0;
        
        Random generator = new Random(s);
        
        while(zmaga_a < d && zmaga_b < d){
        
            int nak = generator.nextInt(100);
            
            if(nak<a){
                zmaga_a++;
                System.out.print("A");
            }
            else if(nak >= a && nak < a+b){
                zmaga_b++;
                System.out.print("B");
            }
            else{
                System.out.print("-");
            }
        
        }
        System.out.println();

    }

    public static void smreka(){
      Scanner s = new Scanner(System.in);
      int n = s.nextInt();
      for(int m = 1; m<=n; m++){
        for (int i = 0; i < m; i++) {
          for (int j = 0; j < n - i-1; j++){
              System.out.print(" ");
          }
          for (int k = 0; k < (2 * i + 1); k++){
            System.out.print("*");
          }
           System.out.println();
        }
      }
    }

    public static void kmp(){
      Scanner s = new Scanner(System.in);

      int st_moskih = s.nextInt();
      int st_zensk = s.nextInt();
      int g = s.nextInt();

      int plesni_par = 0;

      for(int i = 1; i <=st_moskih; i++){
        for(int j = 1; j<=st_zensk;j++){
            if(gcd(i,j) == g){
              plesni_par++;
              System.out.println("[" + plesni_par+"] " + i + " + " + j);
            }
        }
      }
    }
    public static int gcd(int i, int j){
      if(i == 0 || j == 0) return i+j; // base case
      return gcd(j,i%j);
    }
}
