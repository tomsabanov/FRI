import java.util.*;
public class DN06_63170272{
  static HashMap<Integer, int[]> map = new HashMap<Integer,int[]>();
  static int razdalja =0;
    public static void main(String[] args){
      Scanner s = new Scanner(System.in);
      int m = s.nextInt();
      int n = s.nextInt();
      int k = s.nextInt();
      for(int i = 0; i<m; i++){
        for(int j = 0; j<n; j++){
          int c = s.nextInt();
          if(c!=0){
            map.put(c, new int[]{i,j});
          }
        }
      }
        izracunajRazdaljo(k);
        System.out.println(razdalja);
    }
    static void izracunajRazdaljo(int k){
      int current_x = 0;
      int current_y= 0;
      int index = 1;
      while(index<=k) {
         int[] t = map.get(index);
         int nx = t[0];
         int ny = t[1];
         razdalja+=Math.abs(current_x-nx) + Math.abs(current_y-ny);
         current_x = nx;
         current_y = ny;
         index++;
      }
    }
}
