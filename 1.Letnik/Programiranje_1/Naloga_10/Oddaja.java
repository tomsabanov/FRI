// 631700272
public class Oddaja{

  public static void main(String[] args) {

  }

  public static abstract class Lik{
      public abstract boolean isInside(int x, int y);
      public abstract int getTriangles();
      public abstract int[] getCenter();
  }


  public static class Pravokotnik extends Lik{
    private int xCenter;
    private int yCenter;
    private int sirina;
    private int visina;
    public Pravokotnik(int xLevo,int yZgoraj, int sirina, int visina){
      this.xCenter = xLevo + sirina/2;
      this.yCenter = yZgoraj - visina/2;
      this.sirina = sirina;
      this.visina = visina;
    }
    public boolean isInside(int x, int y){
      if(this.xCenter - this.sirina/2 <= x && this.xCenter + this.sirina/2 >= x && this.yCenter + this.visina/2 >= y && this.yCenter - this.visina/2 <= y ){
        return true;
      }
      return false;
    }
    public int getTriangles(){
      return 2;
    }
    public int[] getCenter(){
      int[] c = {xCenter, yCenter};
      return c;
    }
  }

  public static class Elipsa extends Lik{
    private int xCenter;
    private int yCenter;
    private int a;
    private int b;
    public Elipsa(int xSredisce,int ySredisce,int a, int b){
      this.xCenter = xSredisce;
      this.yCenter = ySredisce;
      this.a = a;
      this.b = b;
    }
    public boolean isInside(int x, int y){
      if(b*b*(x-this.xCenter)*(x-this.xCenter) + a*a*(y-this.yCenter)*(y-this.yCenter) <= a*a*b*b){
        return true;
      }
      return false;
    }
    public int getTriangles(){
      return 0;
    }
    public int[] getCenter(){
      int[] c = {xCenter, yCenter};
      return c;
    }
  }

  public static class Mnogokotnik extends Lik{
    private int[][] omejitve;
    public Mnogokotnik(int[][] omejitve){
      this.omejitve = omejitve;
    }
    public boolean isInside(int x, int y){
      boolean b = true;
      for(int i=0; i<this.omejitve.length;i++){
        if(this.omejitve[i][0] * x  + this.omejitve[i][1]*y  + this.omejitve[i][2] > 0){
          b = false;
        }
      }
      return b;
    }
    public int getTriangles(){
      return -1;
    }
    public int[] getCenter(){
      int[] c = new int[2];
      return c;
    }
  }

  public static class Presek extends Lik{
    private Lik l1;
    private Lik l2;
    public Presek(Lik l1, Lik l2){
      this.l1 = l1;
      this.l2 = l2;
    }
    public boolean isInside(int x, int y){
      if(this.l1.isInside(x,y) == true && this.l2.isInside(x,y)==true){
        return true;
      }
      return false;
    }
    public int getTriangles(){
      return -1;
    }
    public int[] getCenter(){
      int[] c = new int[2];
      return c;
    }
  }

  public static class Razlika{
    private Lik l1;
    private Lik l2;
    public Razlika(Lik l1, Lik l2){
      this.l1 = l1;
      this.l2 = l2;
    }
    public boolean isInside(int x, int y){
      if(this.l1.isInside(x,y) == true && this.l2.isInside(x,y)==false){
        return true;
      }
      return false;
    }
    public int getTriangles(){
      return -1;
    }
    public int[] getCenter(){
      int[] c = new int[2];
      return c;
    }
  }


  public static class Risar{
    private boolean[][] slika = new boolean[100][100];
    public Risar(){
      System.out.println("SADA");
    }
    public void narisiLik(Lik lik){
      for(int i = 0; i< 100; i++){
        for(int j = 0; j<100;j++){
          if(lik.isInside(i,j)){
            this.slika[i][j]=true;
          }
        }
      }

    }
    public void narisiRob(Lik lik,  int debelina){

    }
    public boolean[][] slika(){
      return this.slika;
    }
  }

}
