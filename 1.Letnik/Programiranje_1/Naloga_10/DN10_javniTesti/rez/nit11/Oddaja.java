// 63170272
public class Oddaja{
  public static void main(String[] args) {}
  public static abstract class Lik{
      public boolean[][] slika = new boolean[100][100];
      public abstract  boolean isInside(int x, int y);
      public boolean isOnEdge(int x, int y, int debelina){
        if(debelina>1){
          return this.slika[x][y] && (isOnEdge(x + 1, y, debelina - 1) || isOnEdge(x - 1, y, debelina - 1) || isOnEdge(x, y + 1, debelina - 1) || isOnEdge(x, y, debelina - 1)  || isOnEdge(x, y - 1, debelina - 1));
        }
        else if(this.slika[x][y] && !((x>0 && x<99) && (y>0 && y<99) &&   this.slika[x + 1][y]==true && this.slika[x - 1][y]==true && this.slika[x][y + 1]==true && this.slika[x][y - 1]==true)) {
          return true;
        }
        return false;
      }
  }
  public static class Pravokotnik extends Lik{
    private int xLevo;
    private int yZgoraj;
    private int sirina;
    private int visina;

    public Pravokotnik(int xLevo,int yZgoraj, int sirina, int visina){
      this.xLevo = xLevo;
      this.yZgoraj = yZgoraj;
      this.sirina = sirina;
      this.visina = visina;
      for(int i = 0; i<100;i++){
        for(int j = 0; j<100;j++){
          this.slika[i][j] = this.isInside(i,j);
        }
      }
    }

    public boolean isInside(int x, int y){
      if(x>=this.xLevo && x< this.xLevo+sirina && y>=this.yZgoraj && y<this.yZgoraj+visina){
        return true;
      }
      return false;
    }
  }
  public static class Elipsa extends Lik{
    private int xCenter;
    private int yCenter;
    private int a;
    private int b;

    public Elipsa(int xSredisce,int ySredisce,int a, int b){
      this.xCenter = xSredisce;
      this.yCenter = ySredisce;
      this.a = a;
      this.b = b;
      for(int i = 0; i<100;i++){
        for(int j = 0; j<100;j++){
          this.slika[i][j] = this.isInside(i,j);
        }
      }
    }

    public boolean isInside(int x, int y){
      if(b*b*(x-this.xCenter)*(x-this.xCenter) + a*a*(y-this.yCenter)*(y-this.yCenter) <= a*a*b*b){
        return true;
      }
      return false;
    }
  }
  public static class Mnogokotnik extends Lik{
    private int[][] omejitve;
    private int a;
    public Mnogokotnik(int[][] omejitve){
      this.omejitve = omejitve;
      this.a = 0;
      for(int i = 0; i<100;i++){
        for(int j = 0; j<100;j++){
          this.slika[i][j] = this.isInside(i,j);
        }
      }
    }

    public boolean isInside(int x, int y){
      boolean b = true;
      for(int i=0; i<this.omejitve.length;i++){
        if(this.omejitve[i][0] * x  + this.omejitve[i][1]*y  + this.omejitve[i][2] > a){
          b = false;
        }
      }
      return b;
    }

  }



  public static class Presek extends Lik{
    private Lik l1;
    private Lik l2;
    public Presek(Lik l1, Lik l2){
      this.l1 = l1;
      this.l2 = l2;
      for(int i = 0; i<100;i++){
        for(int j = 0; j<100;j++){
          this.slika[i][j] = l1.isInside(i, j) && l2.isInside(i, j);
        }
      }
    }
    public boolean isInside(int x, int y){
      if(this.l1.isInside(x,y) == true && this.l2.isInside(x,y)==true){
        return true;
      }
      return false;
    }
    public boolean isOnEdge(int x, int y,int debelina){
      if(this.l1.isOnEdge(x,y,debelina) == true && this.l2.isOnEdge(x,y,debelina)==true){
        return true;
      }
      return false;
    }
  }
  public static class Razlika extends Lik{
    private Lik l1;
    private Lik l2;
    public Razlika(Lik l1, Lik l2){
      this.l1 = l1;
      this.l2 = l2;
      for(int i = 0; i<100;i++){
        for(int j = 0; j<100;j++){
          this.slika[i][j] = l1.isInside(i, j) && !l2.isInside(i, j);
        }
      }
    }
    public boolean isInside(int x, int y){
      if(this.l1.isInside(x,y) == true && this.l2.isInside(x,y)==false){
        return true;
      }
      return false;
    }
  }
  public static class Risar{
    private boolean[][] slika = new boolean[100][100];
    public Risar(){}
    public void narisiLik(Lik lik){
      for(int i = 0; i< 100; i++){
        for(int j = 0; j<100;j++){
          if(lik.isInside(i,j)){
            this.slika[j][i]=true;
          }
        }
      }
    }
    public void narisiRob(Lik lik,  int debelina){
        for(int i = 0; i< 100; i++){
          for(int j = 0; j<100;j++){
            if(lik.isOnEdge(i,j,debelina)){
              this.slika[j][i]=true;
            }
          }
        }
    }
    public boolean[][] slika(){
      return this.slika;
    }
  }
}
