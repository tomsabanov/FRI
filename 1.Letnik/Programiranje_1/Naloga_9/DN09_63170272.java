import java.util.*;

public class DN09_63170272{
  public static Scanner s = new Scanner(System.in);
  public static void main(String[] args){
    String ime_zaposlenega = s.next();
    int stevilo_objav = s.nextInt();
    Objava[] objave = new Objava[stevilo_objav];
    for(int i = 0; i< stevilo_objav; i++){
      String vrsta_objave = s.next();
      objave[i]=podatki(ime_zaposlenega,vrsta_objave);
    }

    Arrays.sort(objave);
    for(int j = 0; j< stevilo_objav; j++){
      System.out.println(objave[j].toString());
    }
  }
  public static Objava podatki(String ime_zaposlenega, String vrsta_objave){

    int stevilo_avtorjev = s.nextInt();
    String[] avtorji = new String[stevilo_avtorjev];
    int m = 0;
    while(m<stevilo_avtorjev){
      String k = s.next();
      if(k.equals("#")){
        avtorji[m] = ime_zaposlenega;
      }
      else{
        avtorji[m] = k;
      }
      m++;
    }
    String naslov_objave = s.next();

    if(vrsta_objave.equals("monografija")){
      return monografija(ime_zaposlenega,stevilo_avtorjev,avtorji,naslov_objave);
    }
    else if(vrsta_objave.equals("referat")){
      return referat(ime_zaposlenega,stevilo_avtorjev,avtorji,naslov_objave);
    }
    else if(vrsta_objave.equals("clanek")){
      return clanek(ime_zaposlenega,stevilo_avtorjev,avtorji,naslov_objave);
    }
    return null;
  }

  public static Objava monografija(String ime_zaposlenega,int stevilo_avtorjev, String[] avtorji, String naslov_objave){
    String zalozba = s.next();
    int leto_izdaje = s.nextInt();
    String koda_isbn = s.next();
    return new Monografija(ime_zaposlenega,stevilo_avtorjev,avtorji, naslov_objave, zalozba, leto_izdaje, koda_isbn);
  }

  public static Objava referat(String ime_zaposlenega,int stevilo_avtorjev, String[] avtorji, String naslov_objave){
    String nazivKonference = s.next();
    Boolean vrsta_konference = s.nextBoolean();
    int zacetnaStran = s.nextInt();
    int koncnaStran = s.nextInt();
    return new Referat(ime_zaposlenega,stevilo_avtorjev,avtorji, naslov_objave,nazivKonference,vrsta_konference, zacetnaStran, koncnaStran);
  }

  public static Objava clanek(String ime_zaposlenega,int stevilo_avtorjev, String[] avtorji, String naslov_objave){
    String nazivRevije = s.next();
    int letnik = s.nextInt();
    int stevilka = s.nextInt();
    int letnikIzdaje = s.nextInt();
    int mestoNaLestviciRevij = s.nextInt();
    int steviloRevijNaLestvici = s.nextInt();
    int zacetnaStran = s.nextInt();
    int koncnaStran = s.nextInt();
    return new Clanek(ime_zaposlenega,stevilo_avtorjev,avtorji, naslov_objave,nazivRevije, letnik,stevilka,letnikIzdaje,mestoNaLestviciRevij,steviloRevijNaLestvici,zacetnaStran,koncnaStran);
  }


}

class Objava implements Comparable<Objava>{
  protected String ime_zaposlenega;
  protected int stevilo_avtorjev;
  private String[] avtorji;
  protected String naslov_objave;
  protected int osnova;
  protected double tocke;
  public Objava(String ime_zaposlenega,int stevilo_avtorjev,String[] avtorji, String naslov_objave){
    this.stevilo_avtorjev = stevilo_avtorjev;
    this.avtorji = avtorji;
    this.naslov_objave = naslov_objave;
    this.ime_zaposlenega = ime_zaposlenega;
  }
  public int getOsnova(){
    return this.osnova;
  }
  public int getSteviloAvtorjev(){
    return this.stevilo_avtorjev;
  }
  public int compareTo(Objava o) {
    Double os = (double)this.osnova;
    Double st = (double)stevilo_avtorjev;

    Double os2 = (double)o.getOsnova();
    Double st2 = (double)o.getSteviloAvtorjev();
    if(os/st<os2/st2){
      return 1;
    }
    else if(os2/st2<os/st){
      return -1;
    }
    return 0;

  }
  public String izpisAvtorjev(){
    String s = "";
    for(int i = 0; i<this.avtorji.length;i++){
      s+=this.avtorji[i] +", ";
    }
    s = s.substring(0,s.length()-2);
    return s;
  }
  public int gcd(int a, int b) {
     if (b==0) return a;
     return gcd(b,a%b);
  }

  public String getTocke(){
    int x = this.osnova;
    int y = this.stevilo_avtorjev;

    if(x%y==0){
      return "" + x/y;
    }
    else{
        int u = x/y;
        int v = x%y;
        int g = this.gcd(x,y);
        return u + "+" + v/g  + "/" + this.stevilo_avtorjev/g;

    }
  }

}


class Monografija extends Objava{
  private String zalozba;
  private int letoIzdaje;
  private String koda_isbn;
  public Monografija(String ime_zaposlenega,int stevilo_avtorjev, String[] avtorji, String naslov_objave, String zalozba, int letoIzdaje,String koda_isbn){
    super(ime_zaposlenega,stevilo_avtorjev,avtorji,naslov_objave);
    this.zalozba = zalozba;
    this.letoIzdaje = letoIzdaje;
    this.koda_isbn= koda_isbn;
    this.osnova = 10;

    double s = stevilo_avtorjev;
    this.tocke = 10.0/s;
  }
  public String toString(){
    String s = this.izpisAvtorjev() + ": " + this.naslov_objave + ". " + this.zalozba + " " +this.letoIzdaje + ", ISBN " + this.koda_isbn + " | " + this.getTocke();
    return s;
  }


}

class Referat extends Objava{
  private String nazivKonference;
  private int zacetnaStran;
  private int koncnaStran;

  public Referat(String ime_zaposlenega,int stevilo_avtorjev, String[] avtorji, String naslov_objave,String nazivKonference, Boolean vrsta_konference, int zacetnaStran, int koncnaStran){
    super(ime_zaposlenega,stevilo_avtorjev,avtorji,naslov_objave);
    if(vrsta_konference){
      this.osnova = 3;
    }
    else{
      this.osnova = 1;
    }
    this.nazivKonference = nazivKonference;
    this.zacetnaStran = zacetnaStran;
    this.koncnaStran = koncnaStran;

    double d = this.osnova;
    double s = stevilo_avtorjev;
    this.tocke = d/s;
  }

  public String toString(){
    String s = this.izpisAvtorjev() + ": "  + this.naslov_objave + ". " + this.nazivKonference + ": " +this.zacetnaStran  + "-" + this.koncnaStran + " | " + this.getTocke();
    return s;
  }

}

class Clanek extends Objava{
  private String nazivRevije;
  private int letnik;
  private int stevilka;
  private int letnikIzdaje;
  private int mestoNaLestviciRevij;
  private int steviloRevijNaLestvici;
  private int zacetnaStran;
  private int koncnaStran;

  public Clanek(String ime_zaposlenega,int stevilo_avtorjev, String[] avtorji, String naslov_objave, String nazivRevije, int letnik, int stevilka, int letnikIzdaje, int mestoNaLestviciRevij, int steviloRevijNaLestvici, int zacetnaStran,int koncnaStran){
    super(ime_zaposlenega,stevilo_avtorjev,avtorji,naslov_objave);
    this.nazivRevije = nazivRevije;
    this.letnik = letnik;
    this.stevilka = stevilka;
    this.letnikIzdaje = letnikIzdaje;
    this.mestoNaLestviciRevij = mestoNaLestviciRevij;
    this.steviloRevijNaLestvici = steviloRevijNaLestvici;
    this.zacetnaStran = zacetnaStran;
    this.koncnaStran = koncnaStran;

    double z = mestoNaLestviciRevij;
    double k = steviloRevijNaLestvici;
    double p  = z/k;
    if(mestoNaLestviciRevij > steviloRevijNaLestvici){
      this.osnova = 2;
    }
    else{
      if(Double.compare(0.25, p)>0 || Double.compare(0.25, p)  == 0){
        this.osnova = 10;
      }
      else if(Double.compare(0.50, p) > 0  || Double.compare(0.50, p) == 0){
        this.osnova = 8;
      }
      else if(Double.compare(0.75, p) > 0 || Double.compare(0.75, p) == 0){
        this.osnova = 6;
      }
      else if(Double.compare(1.00, p) > 0 || Double.compare(1.00, p) == 0){
        this.osnova = 4;
      }
      double o = this.osnova;
      double  s = stevilo_avtorjev;
      this.tocke = o/s;

    }
  }
  public String toString(){
    String s = this.izpisAvtorjev() + ": "  + this.naslov_objave + ". " + this.nazivRevije + " " +this.letnik  +"(" + this.stevilka + ")" +  ": " + this.zacetnaStran + "-" + this.koncnaStran + " (" + this.letnikIzdaje + ")" + " | " + this.getTocke();
    return s;
  }
}
