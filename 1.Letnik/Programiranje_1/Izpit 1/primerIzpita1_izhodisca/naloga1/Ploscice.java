
//=============================================================================
// Dopolnite!
//=============================================================================

import java.util.*;

public class Ploscice {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();

        int k = sc.nextInt();

        int i = 0;
        int[] visine = new int[k];
        int[][] t1 = new int[k][2];
        while(i<k){

          int visina = sc.nextInt();
          int sirina = sc.nextInt();

          t1[i][0] = visina;
          t1[i][1] = sirina;
          i++;
        }

        //sortiranje t1
        int v = 0;
        int s = 0;
        int[] used = new int[n];
        int indeks = 0;

        while(v<n){
          //poglejmo naprej visino ce je v == n ali ce v + nova je vecja od n

          //najdimo  najmanso visino
          int v_naj = n;
          for(int g = 0; g<t1.length;g++){
            if(isElementInArray(t1[g][0],used) == false && t1[g][0]<v_naj){
              v_naj = t1[g][0];
            }
          }
          if(v_naj + v > n){
            break;
          }
          else{
            v+=v_naj;
          }
          //damo to najmansjo visino v nek array, kjer se bodo pregledovale pri iskanju najmansjih visin....(da skippa)
          for(int j =0; j<n;j++){
            if(used[j] == 0){
              used[j] = v_naj;
              break;
            }
          }

          //najdimo stevilo najmansjih visina
          int stevilo = 0;
          for(int h = 0; h<t1.length;h++){
            if(t1[h][0] == v_naj){
              stevilo++;
            }
          }
          //naredimo array teh najmansje visine z sirinami....
          int[] sirine = new int[stevilo];

          //vstavimo sirine v array
          int a = 0;
          for(int f = 0; f<t1.length;f++){
            if(t1[f][0] == v_naj){
              sirine[a] = t1[f][1];
              a++;
            }
          }

          //sortiramo array sirine
          Arrays.sort(sirine);
          //pogledamo koliko jih lahko damo v vrsto -> povecujemo indeks
          for(int m = 0; m<sirine.length;m++){
            if(s + sirine[m] >n){
              break;
            }
            else{
              s+=sirine[m];
              indeks++;
            }
          }
          s=0;
          //nakoncu gre sirina na 0, visina ostane......
        }

        System.out.println(indeks);


    }

    public static Boolean isElementInArray(int el, int[] t){
        for(int i = 0; i<t.length;i++){
          if(el == t[i]){
            return true;
          }
        }
        return false;
    }
}
