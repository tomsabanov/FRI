
/**
 * Napi"site razred PrimerjalnikBK tako, da bo metoda urediJedi v razredu
 * Obrok, "ce ji podamo objekt tega razreda, jedi obroka this uredila najprej
 * po beljakovinskosti (v urejeni tabeli bodo najprej nanizane vse
 * nebeljakovinske, nato pa vse beljakovinske jedi), obe kategoriji posebej pa
 * bo nara"s"cajo"ce uredila po "stevilu kalorij.
 */


class PrimerjalnikBK implements Primerjalnik{


  public PrimerjalnikBK(){

  }

  @Override
  public boolean jePred(Jed j, Jed m){
    if(j.jeBeljakovinska() == false || j.kalorije()<m.kalorije()){
      return true;
    }
    return false;
  }


}
