
/**
 * NALOGA 2
 */

public class Recept {    // kuharski recept

    private Korak[] koraki;    // koraki recepta

    public Recept(Korak[] koraki) {
        this.koraki = koraki;
    }

    public Korak[] vrniKorake() {
      return this.koraki;
    }

    /**
     * Naloga (a) -- vrne trajanje recepta v minutah.
     */
    public int trajanje() {
      int minute = 0;
      for(int i = 0; i< this.koraki.length;i++){
        minute+=this.koraki[i].vrniTrajanje();
      }
      return minute;
    }

    /**
     * Naloga (b) -- vrne indeks prvega koraka s podano akcijo (-1, "ce te
     * akcije ni).
     */
    public int prviKorakZAkcijo(String akcija) {
      for(int i = 0; i< this.koraki.length;i++){
        if(this.koraki[i].vrniAkcijo().equals(akcija)){
          return i;
        }
      }
      return -1;

    }

    /**
     * Naloga (d) -- vrne "stevilo snovi, ki se pojavljajo SAMO na vhodih v
     * korake recepta.
     */
    public int steviloVstopnihSnovi() {
        int stevilo  = 0;
        for(int i = 0; i<koraki.length;i++){
          Snov[] vstopi = koraki[i].vrniVhode();

          for(int j = 0; j<vstopi.length;j++){
            String v = vstopi[j].vrniNaziv();
            Boolean b = true;


          for(int l = 0; l<koraki.length;l++){
            Snov[] izhodi = koraki[l].vrniIzhode();
            for(int m = 0; m<izhodi.length;m++){
              if(izhodi[m].vrniNaziv().equals(v)){
                b = false;
              }
            }
          }
            if(b){
              stevilo++;
            }
          }

        }
        // Dopolnite / spremenite ...
        return stevilo;
    }
}
