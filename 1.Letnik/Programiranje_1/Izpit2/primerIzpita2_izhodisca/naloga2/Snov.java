
/**
 * NALOGA 2
 */

public class Snov {    // jedilna snov
    private String naziv;   // unikaten naziv, npr. "moka" ali "palacinke"

    public Snov(String naziv) {
        this.naziv = naziv;
    }

    public String vrniNaziv() {
        return this.naziv;
    }

    /**
     * Naloga (c) -- preveri, ali snov this nastopa na izhodu vsaj enega
     * koraka podanega recepta.
     */
    public boolean naIzhodu(Recept recept) {
      Korak[] koraki = recept.vrniKorake();

      for(int i =0; i<koraki.length;i++){
        for(int j = 0; j<koraki[i].vrniIzhode().length;j++){
          if(koraki[i].vrniIzhode()[j].vrniNaziv().equals(this.naziv)){
            return true;
          }
        }
      }
        // Dopolnite / spremenite ...
        return false;
    }
}
