
/**
 * NALOGA 1
 */

 import java.util.*;

public class Park {

    /**
     * Naloga (a) -- vrne "stevilo prostih parkirnih mest na parkiri"s"cu.
     */
    public static int steviloProstih(boolean[][] p) {
        // Dopolnite / spremenite ...
        int g = 0;
        for(int i = 0; i<p.length;i++){
          for(int j = 0;j<p[i].length;j++){
            if(p[i][j]){
              g++;
            }
          }
        }
        return g;
    }

    /**
     * Naloga (b) -- preveri, ali obstaja vrstica z vsaj k zaporednimi (!)
     * prostimi mesti.
     */
    public static boolean zaporedje(boolean[][] p, int k) {
        // Dopolnite / spremenite ..
        for(int i = 0; i<p.length;i++){
          int g = 0;
          for(int j = 0;j<p[i].length;j++){

            if(p[i][j] == true){
              if(++g >= k){
                return true;
              }
            }
            else{
              g = 0;
            }
          }
        }
        return false;
    }

    /**
     * Naloga (c) -- vrne "stevilo VSEH parkirnih mest v gara"zni hi"si.
     */
    public static int steviloMest(boolean[][][] p) {
        // Dopolnite / spremenite ..
        int g = 0;
        for(int l = 0; l<p.length;l++){
          for(int i = 0; i<p[l].length;i++){
            for(int j = 0;j<p[l][i].length;j++){
                g++;
            }
          }
        }
        return g;
    }

    /**
     * Naloga (d) -- vrne indeks nadstropja z najve"c prostimi mesti.
     */
    public static int najboljProstoNadstropje(boolean[][][] p) {
      ArrayList<Integer> zasedenost  = new ArrayList<Integer>();
      int d = 0;
      for(int l = 0; l<p.length;l++){
        for(int i = 0; i<p[l].length;i++){
          for(int j = 0;j<p[l][i].length;j++){
              if(p[l][i][j]){
                d++;
              }
          }
        }
        zasedenost.add(d);
        d=0;
      }
      int naj = 0;
      int indeks = 0;
      for(int k = 0; k<zasedenost.size();k++){
          if(zasedenost.get(k)>naj){
            naj = zasedenost.get(k);
            indeks = k;
          }
      }

      return indeks;
    }
}
