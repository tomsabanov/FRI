/*
Vzemimo osnovo p, in potenco k.

Definirajmo funkcijo f(i) = (p^i - 1) / (p - 1) , ki ubistvu pove, koliko faktorjev "p"-jev je v izrazu "p^i".
Potem poiscemo najvecjo vrednost "max", da bo f(max) <= k.

Potem izracunamo kvocient in ostanek pri k/f(max), shranimo ta kvocient kot q_max in ostanek kot ostanek.


Zdaj izracunamo kvocient in ostanek od ostanek/f(ostanek -1), shranimo ta kvocient kot q_{max-1} in ostanek kot (spet) ostanek.
In to ubistvu nadaljujemo, torej spet bi izracunali kvocient in ostanek od novega ostanka/f(novi_ostanek -1), shranimo novi kvocient in ostanek itd.....
To ubistvu generira sekvenco q_j, q_{j-1}, q_{j-2},....q_1. (sekvenca se konca pri 1 in ne pri 0)

Potem izracunamo q_j * p^j + q_{j-1} *p^{j-1} + ..... + q_1 * p  -> in ta vsota vrste je dobljeni N.


Primer :

k = 4,
p = 3

f(i) = (3^i -1)/2

f(1) = 1
f(2) =4
f(3) = 13

Torej najvecji max pri katerem velja f(max) <=4(->k) je pri indeksu 2, torej f(2)=4. Vzamemo kvocient in ostanek od 4/4 (f(2)/k),
dobimo kvocient 1 in ostanek 0. Tukaj mi koncamo...

Torej je q_2 = 1

N je potem = 2 * 3^1 = 6, kar je pravilno.

Ce bi dobili pri izracunu kvocienta recimo nek ostanek > 0 (recimo 1), bi potem izracunali novi ostanek in kvocient od 1/f(1),
in dobili bi kvocient 1 in ostanek 0, in bi spet koncali... Formula za N bi se potem glasila = q_2 *p^2 + q_1 * p^1

*/

import java.util.*;
class DN03_63170272{
  static long p;
  static long k;
  public static void main(String[] args){
      Scanner s = new Scanner(System.in);
      p = s.nextLong();
      k = s.nextLong();
      long  max = 0;
      while (funkcija(max) <= k) {
          max++;
      }
      max--;

      long N = 0;
      long ostanek = k;
      while (ostanek > 0) {
          long val = funkcija(max);
          long q = ostanek/val;
          long nov_ostanek = ostanek%val;
          N += q * (pow(p,max));
          ostanek = nov_ostanek;
          max--;
      }
      System.out.println(N);
  }

  static long funkcija(long i){
    return (pow(p,i)- 1)/(p - 1);
  }

  static long pow(long x, long n){
    if(n==0) return 1;
    return x * pow(x, n-1);
  }
}
