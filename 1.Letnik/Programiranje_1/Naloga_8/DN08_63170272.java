import java.util.*;

class DN08_63170272{
  public static void main(String[] args){
    Scanner s = new Scanner(System.in);
     int o_plus = s.nextInt();
     int o_minus = s.nextInt();
     int n = s.nextInt();

    Banka b = new Banka();

    while(n>0){
      switch(s.next()){
        case "r" : b.novRacun(s.nextInt(),s.nextInt(),s.nextInt(),s.next(), s.nextInt(), s.nextInt(),s.nextInt(), o_plus, o_minus);break;
        case "+" : b.pologDenarja(s.nextInt(), s.nextInt(),s.nextInt(),s.next(),s.nextInt());break;
        case "-" : b.dvigDenarja(s.nextInt(),s.nextInt(),s.nextInt(),s.next(),s.nextInt());break;
      }
      n--;
    }

  }


}
class Banka{
  static Map<String, Racun> komitenti = new HashMap<String, Racun>();
  public void novRacun(int d, int m, int l, String naziv, int N, int D, int M, int o_plus, int o_minus){
    this.komitenti.put(naziv, new Racun(N,D,M,d,m,l,o_plus, o_minus));
    System.out.println("OK");
  }

  public void pologDenarja(int d, int m, int l, String naziv, int k){
    Racun r = this.komitenti.get(naziv);
    r.novDan(d,m,l);
    r.polozi(k, d, m, l);
    System.out.println(r.getStanje());
  }

  public void dvigDenarja(int d, int m, int l, String naziv, int k){
    Racun r = this.komitenti.get(naziv);
    //preverjanje N D M
    r.novDan(d,m,l);
    if(r.getN() >= 0 && (r.getStanje() - k) <  -r.getN() ){
      System.out.println("N");
    }
    else if(r.getD()>=0 && (r.getZnesekDnevnihDvigov() + k) > r.getD()){
        System.out.println("D");
    }
    else if(r.getM()>=0 && (r.getZnesekMesecnihDvigov() + k ) > r.getM()){
        System.out.println("M");
    }
    else{
      r.dvig(k,d,m,l);
      System.out.println(r.getStanje());
    }
  }
}

class Racun{
  private int stanje;
  private  int N;
  private int D;
  private int M;

  private int dnevni_dvig = 0;
  private int mesecni_dvig = 0;

  private int trenutni_dan = 0;
  private int trenutni_mesec = 0;
  private int trenutno_leto = 0;

  private int o_plus;
  private int o_minus;

  public Racun(int n, int d , int m, int trenutni_dan, int trenutni_mesec, int trenutno_leto, int o_plus, int o_minus){
    this.o_plus = o_plus;
    this.o_minus = o_minus;
    this.N = n;
    this.D = d;
    this.M = m;
    this.stanje = 0;
    this.trenutni_dan = trenutni_dan;
    this.trenutni_mesec = trenutni_mesec;
    this.trenutno_leto = trenutno_leto;
  }
  public void polozi(int k, int d, int m, int l){
    this.stanje+=k;
  }
  public void dvig(int k, int d, int m, int l){
    this.stanje-=k;
    if(d == this.trenutni_dan && m == this.trenutni_mesec && l == this.trenutno_leto){
        this.dnevni_dvig+=k;
        this.mesecni_dvig+=k;
    }
    else if( l == this.trenutno_leto && m == this.trenutni_mesec ){
      this.dnevni_dvig= 0;
      this.mesecni_dvig+=k;
    }
  }

  public void pristejObresti(int m, int l){
    while(this.trenutno_leto <= l){
      if(this.trenutno_leto == l && this.trenutni_mesec == m){
        break;
      }
      if(this.trenutni_mesec > m){
        for(int i = 0;i<=12-this.trenutni_mesec;i++){
          if(this.stanje >= 0){
            this.stanje+= Math.floor((this.stanje * this.o_plus)/1000);
          }
          else{
            this.stanje-= Math.floor((-this.stanje * this.o_minus)/1000);
          }
        }
        this.trenutni_mesec = m;
      }
      else if(this.trenutni_mesec == m){
        int razlika = l - this.trenutno_leto+1;
        for(int i = 1;i<=12*razlika;i++){
          if(this.stanje >= 0){
            this.stanje+= Math.floor((this.stanje * this.o_plus)/1000);
          }
          else{
            this.stanje-= Math.floor((-this.stanje * this.o_minus)/1000);
          }
        }
        this.trenutno_leto = l;
        break;
      }
      else{
        while(this.trenutni_mesec < m){
          if(this.stanje >= 0){
            this.stanje+= Math.floor((this.stanje * this.o_plus)/1000);
          }
          else{
            this.stanje-= Math.floor((-this.stanje * this.o_minus)/1000);
          }
          this.trenutni_mesec++;
        }
      }
      this.trenutno_leto++;
    }
  }
  public void novDan(int d, int m, int l){
    if(m != this.trenutni_mesec || l != this.trenutno_leto){
      this.pristejObresti(m,l);
    }
    if(d == this.trenutni_dan && m == this.trenutni_mesec && l == this.trenutno_leto){
      return;
    }
    else if( l == this.trenutno_leto && m == this.trenutni_mesec ){
      this.dnevni_dvig= 0;
    }
    else{
      this.dnevni_dvig = 0;
      this.mesecni_dvig = 0;
    }
    this.trenutni_dan = d;
    this.trenutni_mesec = m;
    this.trenutno_leto = l;
  }


  public int getZnesekDnevnihDvigov(){
    return this.dnevni_dvig;
  }
  public int getZnesekMesecnihDvigov(){
    return this.mesecni_dvig;
  }
  public int getStanje(){
    return this.stanje;
  }
  public int getN(){
    return this.N;
  }
  public int getD(){
    return this.D;
  }
  public int getM(){
    return this.M;
  }
}
