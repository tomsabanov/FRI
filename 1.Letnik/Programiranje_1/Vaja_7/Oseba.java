class Oseba{
  private String ime;
  private String priimek;
  private char spol;
  private int letoRojstva;
  private Oseba oce = null;
  private Oseba mati = null;
  static int stevilo_ustvarjenih;

  public Oseba(String ime, String priimek, char spol, int letoRojstva){
    this.ime = ime;
    this.priimek = priimek;
    this.spol = spol;
    this.letoRojstva = letoRojstva;

	stevilo_ustvarjenih++;
  }
  public Oseba(String ime, String priimek, char spol, int letoRojstva, Oseba oce, Oseba mati){
    this.ime = ime;
    this.priimek = priimek;
    this.spol = spol;
    this.letoRojstva = letoRojstva;
    this.oce = oce;
    this.mati = mati;
	stevilo_ustvarjenih++;
  }
  public String imeOceta(){
    if(this.oce != null){
      return this.oce.vrniIme();
    }
  }
  public boolean jeBratAliSestraOd(Oseba os){
    if(this.oce.toString() == os.oce.toString() || this.mati.toString()==os.mati.toString()){
      return true;
    }
    return false;
  }
  public boolean jeSestraOd(Oseba os){
    if(jeBratAliSestraOd(os) && this.spol == 'Z'){
      return true;
    }
    return false;
  }
  public boolean jeTetaOd(Oseba os){
	if(jeBratAliSestraOd(os.oce) || jeBratAliSestraOd(os.mati)){
		return true;
	}
  }
  public boolean jeOcetovskiPrednikOd(Oseba os){
	Oseba ocetovski_prednik;
	while(true){
		if(!os.oce){
			return false;
		}
		else{
			ocetovski_prednik = os.oce;
			if(ocetovski_prednik == this){
				return true;
			}
			else{
				ocetovski_prednik = ocetovski_prednik.oce;
			}	
		}

	}
	
	
  }

  public static int steviloUstvarjenih(){
	return stevilo_ustvarjenih;
  }

  public String vrniIme(){
    return this.ime;
  }
  public void nastaviIme(String novoIme){
    this.ime = novoIme;
  }
  public String toString(){
    return this.ime + " " + this.priimek + " (" +this.spol + ") , " + this.letoRojstva;
  }
  public int starost(int leto){
    return leto - letoRojstva;
  }
  public boolean jeStarejsaOd(Oseba os){
    if(os.letoRojstva > this.letoRojstva){
      return true;
    }
    return false;
  }

}
