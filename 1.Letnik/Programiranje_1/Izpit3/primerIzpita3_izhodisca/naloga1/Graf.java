
/**
 * NALOGA 1
 */

import java.util.*;

public class Graf {

    //-------------------------------------------------------------------------
    // Dopolnite / spremenite ...
    //-------------------------------------------------------------------------
    public static boolean imaEulerjevObhod(boolean[][] m) {
      Boolean b = true;
      for(int i = 0;i<m.length;i++){
        int sodo = 0;
        for(int j = 0; j<m.length;j++){
          if(m[i][j] == true){
            sodo++;
          }
        }
        if(sodo%2!=0){
          return false;
        }
      }
      return b;
    }

    //-------------------------------------------------------------------------
    // Dopolnite / spremenite ...
    //-------------------------------------------------------------------------
    public static boolean[][] vMatriko(int[][] s) {
      boolean[][] m = new boolean[s.length][s.length];
      for(int i = 0; i<s.length;i++){
        for(int j = 0;j<s[i].length;j++){
            m[i][s[i][j]] = true;
        }
      }
      return m;
    }

    //-------------------------------------------------------------------------
    // Dopolnite / spremenite ...
    //-------------------------------------------------------------------------
    public static boolean istiGraf(boolean[][] m, int[][] s) {

      boolean[][] m2 = new boolean[s.length][s.length];
      for(int i = 0; i<s.length;i++){
        for(int j = 0;j<s[i].length;j++){
            m2[i][s[i][j]] = true;
        }
      }
      Boolean b = true;
      for(int l = 0; l<m.length;l++){
        for(int z = 0;z<m[l].length;z++){
          if(m[l][z] == false &&  m2[l][z]==true || m[l][z] == true &&  m2[l][z]==false){
            b=false;
          }
        }
      }

        return b;
    }
}
