
/**
 * NALOGA 3
 */

import java.awt.*;
import java.util.*;

public class Piramida extends Platno {

    public static void main(String[] args) {
        Piramida piramida = new Piramida(4);
        piramida.sproziRisanje(args);
    }

    private int visina;   // vi"sina piramide

    public Piramida(int visina) {
        this.visina = visina;
    }

    //-------------------------------------------------------------------------
    // Dopolnite / spremenite ...
    //
    // Za zaokro"zevanje double -> int uporabite metodo ri, npr. ri(2.7) = 3.
    //-------------------------------------------------------------------------
    protected void narisi(Graphics2D g, double wp, double hp) {
      int n = this.visina-1;
      double sirina = this.sirinaBloka(wp,hp);
      double visina = this.visinaBloka(wp,hp);
      for(int i = 0; i<this.visina;i++){
        int x = ri(n*sirina/2);
        int y =ri(i*visina);

        for(int j = 0;j<i+1;j++){
          g.setColor(Color.YELLOW);
          g.fillRect(x,y,ri(sirina),ri(visina));
          g.setColor(Color.BLACK);
          g.drawRect(x,y,ri(sirina),ri(visina));
          x=ri(x + sirina);
        }
        n--;
      }


    }

    //-------------------------------------------------------------------------
    // Dopolnite / spremenite ...
    //-------------------------------------------------------------------------
    public double sirinaBloka(double wp, double hp) {
        return wp/this.visina;
    }

    //-------------------------------------------------------------------------
    // Dopolnite / spremenite ...
    //-------------------------------------------------------------------------
    public double visinaBloka(double wp, double hp) {
      return hp/this.visina;
    }
}
