
/**
 * NALOGA 2
 */

import java.util.*;

public class Zaporedje {
    private int trenutno = 1;
    private int k;
    private int krat = 1;

    private int katero = 0;

    private boolean prvic = true;
    private int odstej = -1;
    //-------------------------------------------------------------------------
    // Dopolnite / spremenite ...
    //-------------------------------------------------------------------------
    public Zaporedje(int k) {
      this.k = k;
    }
    public int getTrenutno(){
      return this.trenutno;
    }
    public int getKrat(){
      return this.krat;
    }
    public int getKatero(){
      return this.katero;
    }
    public boolean getPrvic(){
      return this.prvic;
    }
    public int getOdstej(){
      return this.odstej;
    }

    //-------------------------------------------------------------------------
    // Dopolnite / spremenite ...
    //-------------------------------------------------------------------------
    public int naslednje() {
      if(prvic){
        prvic = false;
        katero++;
        return 1;
      }
      katero++;
      trenutno+=krat*k +odstej;
      krat++;
      odstej+=-2;
      return trenutno;
    }

    //-------------------------------------------------------------------------
    // Dopolnite / spremenite ...
    //-------------------------------------------------------------------------
    public int katero() {
      return katero+1;
    }

    //-------------------------------------------------------------------------
    // Dopolnite / spremenite ...
    //-------------------------------------------------------------------------
    public void ponastavi(int n) {
      Zaporedje z = new Zaporedje(this.k);
      for(int i =0; i<n-1;i++){
        z.naslednje();
      }
      this.prvic = z.getPrvic();
      this.trenutno = z.getTrenutno();
      this.krat = z.getKrat();
      this.katero = z.getKatero();
      this.odstej = z.getOdstej();
    }
}
