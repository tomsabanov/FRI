import java.util.*;
public class DN07_63170272{
	public static void main(String[] args){
    Scanner s = new Scanner(System.in);
    int n = s.nextInt();
    int[][] matrika = new int[n][n];
    int stevilo_kvadratov = 0;
    int a = 1; // a opisuje najvecji kvadrat ki je zraven trenutne tocke v tabeli (x,y), ce a 0, potem ta kvadrat ni zraven (x,y tocker)
    /*
      Premik opisuje stevilo zaporednih enic, ko vemo da najvecji kvadrat ni vec zraven trenutne tocke, torej je bila vmes
      0ka... (1 1 0 ...... 1 1 1 -> zap:3), ko je zap vecja od 1, se gre iskat po prejsnem algoritmu najvecji kvadrat,
      nato se nastav a tega kvadrata, premik pa gre nazaj na 0;
    */
    for(int i = 0; i<n;i++){ // i je y, j je x
        a=1;
        for(int j = 0; j<n;j++){
            if(s.nextInt() == 1){
              stevilo_kvadratov++;
              matrika[i][j]=1;
              if(a > 1){
                //kvadrat je zraven tocke, gledamo samo y os do a(i-a),(gremo od tocke gor za a+1 in levo za a-1)
                boolean b = true;
                for(int g = 1; g<a;g++){
                      if(matrika[i-g][j] == 1){
                        stevilo_kvadratov++;
                      }
                      else{
                        b= false;
                        a=1;
                        break;
                      }
                }
                if(b == true && j-a >=0 && i-a >= 0){
                  if(matrika[i-a][j] == 1){
                    boolean m = true;
                    for(int d = 1; d<=a;d++){
                        if(matrika[i-a][j-d] == 0){
                          m = false;
                          break;
                        }
                    }
                    if(m == true){
                      stevilo_kvadratov++;
                      a++;
                    }
                  }
                }
              }
              else{
                  //iscemo novi kvadrat s premikom
                  int x = j-1;
                  int y = i-1;
                  int premik = 1;
          outer: while(x >=0 && y>=0){
                    //gledamo po y osi, x = konst
                    for(int g = 0;g<=premik;g++){
                      if(matrika[i-g][x] == 0){
                        break outer;
                      }
                    }
                    //gledamo po x osi, y = konst
                    for(int h = 1;h<=premik;h++){
                      if(matrika[y][x+h] == 0){
                        break outer;
                      }
                    }
                    premik++;
                    a=premik;
                    x = j-premik;
                    y = i-premik;
                    stevilo_kvadratov++;
                  }

              }
            }
            else{
              matrika[i][j]=0;
              a = 0;
            }
        }
    }
    System.out.println(stevilo_kvadratov);
  }
}
