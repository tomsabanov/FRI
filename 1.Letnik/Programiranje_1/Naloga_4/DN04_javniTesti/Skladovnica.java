// 63170272
import java.util.*;
class Skladovnica{
	private int kapacitetaPrvega;
	private int prirast;
	private int stevilo_skatel=0;
	private static void izpisiKapacitete(Skladovnica skladovnica) {
			for (int kup = 1;  kup <= 10;  kup++) {
					System.out.printf("| %d ", skladovnica.kapacitetaKupa(kup));
			}
			System.out.println("|");
	}

	private static void izpisiZasedenosti(Skladovnica skladovnica) {
			for (int kup = 1;  kup <= 10;  kup++) {
					System.out.printf("| %d ", skladovnica.zasedenostKupa(kup));
			}
			System.out.println("|");
	}
		public Skladovnica(int kapacitetaPrvega, int prirast){
				this.kapacitetaPrvega = kapacitetaPrvega;
				this.prirast = prirast;
				this.stevilo_skatel = 0;
		}
		public Skladovnica prestavi(int kapacitetaPrvega, int prirast){
			Skladovnica s = new Skladovnica(kapacitetaPrvega, prirast);
			s.dodaj(this.skupnoSteviloSkatel());
			this.stevilo_skatel = 0;
			return s;
		}
		public int kapacitetaKupa(int kup){
			int k = kapacitetaPrvega;
			for(int i = 0; i<kup-1;i++){
				k+=prirast;
			}
			return k;
		}
		public int skatle(int i,int kup, int skatle){
			if(kup>i){
				return skatle(i+1,kup,this.kapacitetaPrvega + i*this.prirast + skatle);
			}
			return skatle;
		}
		public void dodaj(int stSkatel){
			this.stevilo_skatel+=stSkatel;
		}
		public int zasedenostKupa(int kup){
			if(skatle(0,kup,0) > this.stevilo_skatel){
				int stevilo_skatel_kupa = kapacitetaKupa(kup);
				if(this.stevilo_skatel - (skatle(0,kup,0) - stevilo_skatel_kupa) < 0){
					return 0;
				}
				return this.stevilo_skatel - (skatle(0,kup,0) - stevilo_skatel_kupa);
			}
			return kapacitetaKupa(kup);
		}
		public boolean odvzemi(int stSkatel){
			if(this.stevilo_skatel >= stSkatel){
				this.stevilo_skatel-=stSkatel;
				return true;
			}
			return false;
		}
		public int skupnoSteviloSkatel(){
			return this.stevilo_skatel;
		}
		public int poisciKup(int skatla){
			int i = 0;
			int p = skatle(0,i,0);
			if(skatla > this.stevilo_skatel) return -1;
			while(true){
				if(skatla-1<p){
					return i;
				}
				i++;
				p=skatle(0,i,0);
			}
		}
}
