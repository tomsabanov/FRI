import java.util.*;
class Vaja_8{
	static Scanner s = new Scanner(System.in);

	public static void main(String[] args){
		naloga_3();
	}

	public static void naloga_1(){
		Scanner s = new Scanner(System.in);
		int stevilo_vojakov = s.nextInt();
		int[] tabela_vojakov = new int[stevilo_vojakov+2];
		tabela_vojakov[0]=0;
		tabela_vojakov[stevilo_vojakov+1]=Integer.MAX_VALUE;
		for(int i = 0; i<stevilo_vojakov; i++){
			tabela_vojakov[i] = s.nextInt();
		}
		int narobe_postavljeni= 0;
		for(int i = 1; i<stevilo_vojakov+1;i++){
			if(tabela_vojakov[i-1]<= tabela_vojakov[i] && tabela_vojakov[i]<=tabela_vojakov[i+1]){
				System.out.println(i-1);
			}
			else{
				narobe_postavljeni++;
			}

		}
		if(narobe_postavljeni == stevilo_vojakov){
			System.out.println("NOBEDEN");
		}
	}

	public static void naloga_2(){
		Scanner s = new Scanner(System.in);
		int n = s.nextInt();
		int k = s.nextInt();
		int[] zgoscenke = new int[n];
		System.out.println(Arrays.toString(zgoscenke));
		for(int i = 0;i<k;i++){
			int datoteka = s.nextInt();
			int najmanj_zasedena = najdi_najmanj_zasedeno(zgoscenke, k);
			if(k-zgoscenke[najmanj_zasedena] >= datoteka){
				zgoscenke[najmanj_zasedena]+=datoteka;
			System.out.println(datoteka + " EP -> zgoscenka " + najmanj_zasedena + "  " +Arrays.toString(zgoscenke));
			} 
			else{	
				break;
			}
		}
	}

	public static int najdi_najmanj_zasedeno(int[] tabela,int k){
		int[] i = new int[2];
		i[0]=0; // drzi index
		i[1]=tabela[0]; //drzi vrednost
		for(int j = 0;j<tabela.length; j++){
			if(k-tabela[j]>k-i[1]){
				i[0]=j;
				i[1]=tabela[j];			
			}
		}
		return i[0];
	}



	public static void naloga_3(){
		int stevilo_otrok = s.nextInt();
		int stevilo_besed =s.nextInt();
		String[][] otroci = new String[stevilo_otrok][2]; //[0] ime, [1] bool
		for(int i=0; i<stevilo_otrok;i++){
			otroci[i][0] = s.next();
			otroci[i][1] = "1";
		}	
		int x = stevilo_otrok;
		while(x >1){
			int kazalec = prvi_otrok(otroci);
			int izbrani = 0;
			for(int i=0;i<stevilo_besed;i++){
				if(i%x==0){izbrani = 0;}
				else{
					izbrani++;
				}	
			}
			otroci[brcniOtroka(otroci,izbrani)][1] = "0";
			System.out.println(otroci[brcniOtroka(otroci,izbrani)][0]);
			x--;
		}
	}
	public static int prvi_otrok(String[][] otroci){
		for(int i = 0; i<otroci.length;i++){
			if(Integer.parseInt(otroci[i][1]) == 1) return i;
		}	
		return -1;
	}
	public static int brcniOtroka(String[][] otroci,int k){
		for(int i = 0; i<otroci.length;i++){
			if(Integer.parseInt(otroci[i][1]) == 1){
				k-=1;
				if(k==0){return i;}
			}
		}
		return 0;
	}
}
