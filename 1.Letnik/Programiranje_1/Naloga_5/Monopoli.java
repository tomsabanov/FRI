import java.util.*;
class Monopoli{
  public static void main(String[] args){
      Scanner s = new Scanner(System.in);
      int n = s.nextInt();
      int k = s.nextInt();
      int c = s.nextInt();
      int m = s.nextInt();
      int z = s.nextInt();
      int M = s.nextInt();

      Igra igra = new Igra(n,k,c,m,z);
      Boolean end = false;
      while(M > 0 && end == false){
        int met = s.nextInt();
        end = igra.metKocke(met);
        M--;
      }
      igra.izpis();

  }
}
class Igra{
  private Posest[] posesti;//ubistvu je to board z vsemi posesti.....
  private Igralec[] igralci;
  private int igralecNaVrsti = 0;

  private int stevilo_bankrotov=0;

  private int k = 0;

  public Igra(int n,int k, int c, int m, int z){
    // n - igra je sestavljena iz n posesti
    // k - po k zaporednih posesti pripada eni skupini posesti...
    // c - vse posesti stanejo c dinarjev
    // m - igra m-tih igralcev
    // z - vsak igralec ima z zacetnih dinarjev
    this.k = k;
    this.generirajPosesti(n,k,c);
    this.generirajIgralce(m,z);
  }
  public Boolean metKocke(int metKocke){
    Igralec igralec = this.igralci[this.igralecNaVrsti];
    int pozicijaIgralca = igralec.getPozicijaIgralca();
    if(metKocke + igralec.getPozicijaIgralca() > this.posesti.length-1){
      igralec.setPozicijaIgralca(metKocke + pozicijaIgralca- this.posesti.length);
    }
    else{
      igralec.setPozicijaIgralca(metKocke + pozicijaIgralca);
    }
    pozicijaIgralca = igralec.getPozicijaIgralca();
    if(pozicijaIgralca > this.posesti.length -1){
      pozicijaIgralca =0;
      igralec.setPozicijaIgralca(0);
    }

    Posest posest = this.posesti[pozicijaIgralca];

    if(posest.getLastnikPosesti().getIdIgralca() == -1 && igralec.getDinarjiIgralca() >= posest.getVrednostPosesti()){
      //kupimo posest
      igralec.odvzemiDinarje(posest.getVrednostPosesti());
      igralec.dodajPosest(posest);
      posest.setLastnikPosesti(igralec);
    }
    else if(posest.getLastnikPosesti().getIdIgralca() != -1 && posest.getLastnikPosesti() != igralec){
      // moramo placati najemnino
      int najemnina = 0;

      int skupinaPosesti = posest.getIdSkupinePosesti();
      ArrayList<Posest> posesti_lastnika = posest.getLastnikPosesti().getPosestiIgralca();
      for(Posest p : posesti_lastnika){
          if(p.getIdSkupinePosesti() == skupinaPosesti){
            najemnina++;
          }
          if(this.k == 1){
            najemnina = 1;
          }
        }

      if(igralec.getDinarjiIgralca() < najemnina){
        igralec.setBankrot();
        this.stevilo_bankrotov++;
        if(this.stevilo_bankrotov == this.igralci.length-1){
          return true;
        }
      }
      else{
        igralec.odvzemiDinarje(najemnina);
        posest.getLastnikPosesti().dodajDinarje(najemnina);
      }
    }


    int igralecNaVrsti = this.igralecNaVrsti+1;
    while(true){
      if(igralecNaVrsti>this.igralci.length-1){
          igralecNaVrsti = 0;
      }
      if(this.igralci[igralecNaVrsti].bankrot()){
        igralecNaVrsti++;
      }
      else{
          break;
      }
    }
    this.igralecNaVrsti = igralecNaVrsti;
    // premaknemo naprej igralca na vrsti
    //dobimo njegovo zdajsne polje
    //ustvarimo pravila
    return false;
  }
  public void izpis(){
    for(int i = 0; i<this.igralci.length;i++){
      if(this.igralci[i].bankrot()){
        System.out.println(this.igralci[i].getPozicijaIgralca() + " bankrot" );
      }
      else{
        System.out.println(this.igralci[i].getPozicijaIgralca() + " " + this.igralci[i].getDinarjiIgralca());
      }
    }
  }
  public void generirajPosesti(int n, int k, int c){
    this.posesti = new Posest[n];
    int skupinaPosesti = 0;
    for(int i = 0; i<n; i++){
      if(i%k == 0){skupinaPosesti++;}
      this.posesti[i] = new Posest(i,i,c,skupinaPosesti);
    }
  }
  public void generirajIgralce(int m, int z){
    this.igralci = new Igralec[m];
    for(int i = 0; i<m;i++){
      this.igralci[i] = new Igralec(i, 0, z);
    }
  }

}

class Igralec{
    private int idIgralca;
    private int pozicijaIgralca;
    private ArrayList<Posest> posestiIgralca = new ArrayList<Posest>();
    private int dinarjiIgralca;
    private Boolean bankrot = false;

    public Igralec(int idIgralca, int pozicijaIgralca, int dinarjiIgralca){
      this.idIgralca = idIgralca;
      this.pozicijaIgralca = pozicijaIgralca;
      this.dinarjiIgralca = dinarjiIgralca;
    }
    public void setBankrot(){
      this.bankrot = true;
      this.dinarjiIgralca = -1;
      for(Posest p : this.posestiIgralca){
          p.setLastnikPosestiNull();
      }
      this.posestiIgralca.clear();
    }
    public Boolean bankrot(){
      return this.bankrot;
    }
    public int getPozicijaIgralca(){
      return this.pozicijaIgralca;
    }
    public void setPozicijaIgralca(int pozicijaIgralca){
      this.pozicijaIgralca = pozicijaIgralca;
    }
    public int getDinarjiIgralca(){
      return this.dinarjiIgralca;
    }
    public int getIdIgralca(){
      return this.idIgralca;
    }
    public ArrayList<Posest> getPosestiIgralca(){
      return this.posestiIgralca;
    }
    public void dodajDinarje(int dodaj){
      this.dinarjiIgralca+=dodaj;
    }
    public void odvzemiDinarje(int odvzemi){
      this.dinarjiIgralca-=odvzemi;
    }
    public void dodajPosest(Posest posest){
      this.posestiIgralca.add(posest);
    }
    public void odvzemiPosest(int idPosesti){
      for(int i = 0; i<this.posestiIgralca.size(); i++){
          if(this.posestiIgralca.get(i).getIdPosesti() == idPosesti){
              this.posestiIgralca.remove(i);
          }
      }
    }
}
class Posest{
  private int idPosesti;
  private int pozicijaPosesti;
  private int vrednostPosesti;
  private Igralec lastnikPosesti = new Igralec(-1,-1,-1);
  private int idSkupinePosesti;

  public Posest(int idPosesti, int pozicijaPosesti, int vrednostPosesti, int idSkupinePosesti){
    this.idPosesti = idPosesti;
    this.pozicijaPosesti = pozicijaPosesti;
    this.vrednostPosesti = vrednostPosesti;
    this.idSkupinePosesti = idSkupinePosesti;
  }
  public int getIdPosesti(){
    return this.idPosesti;
  }
  public int getIdSkupinePosesti(){
    return this.idSkupinePosesti;
  }
  public int getPozicijaPosesti(){
    return this.pozicijaPosesti;
  }
  public int getVrednostPosesti(){
    return this.vrednostPosesti;
  }
  public Igralec getLastnikPosesti(){
    return this.lastnikPosesti;
  }
  public void setLastnikPosesti(Igralec lastnik){
    this.lastnikPosesti = lastnik;
  }
  public void setLastnikPosestiNull(){
    this.lastnikPosesti = new Igralec(-1,-1,-1);
  }
}
