//#include <ctype.h>
#include <stdio.h>
//int getc(void);
//void ungetc(int);
/* getint: get the next integer from input into *pn */
int getint(int *pn){
  int c, sign;

  while(isspace(c = getc()));

  if(!isdigit(c) && c != EOF && c != '+' && c !='-'){
    ungetc(c); // it's not a number
    return 0;
  }
  sign = (c == '-') ? -1 : 1;
  if( c == '+' || c == '-'){
     c = getc();
  }
  for(*pn = 0; isdigit(c); c = getc()){
    *pn = 10 * *pn + (c - '0');
  }
  *pn *= sign;
  if( c != EOF){
    ungetc(c);
  }
  return c;
}


int main(){
  int a  = 10;
  int *pn = &a;

  int c = getint(pn);
  printf("%d", c);

  return 0;
}
