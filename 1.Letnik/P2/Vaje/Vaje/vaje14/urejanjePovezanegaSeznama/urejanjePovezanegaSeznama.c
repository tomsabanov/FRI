
#include <stdio.h>
#include <stdlib.h>

/*
 * vozli"s"ce povezanega seznama
 */
typedef struct _Vozlisce {
    int element;
    struct _Vozlisce* naslednje;
} Vozlisce;

/*
 * Ustvari vozli"s"ce s podanim elementom in naslednikom.
 */
Vozlisce* ustvariVozlisce(int element, Vozlisce* naslednje) {
    Vozlisce* v = malloc(sizeof(Vozlisce));
    v->element = element;
    v->naslednje = naslednje;
    return v;
}

/*
 * Prebere elemente z vhoda v povezani seznam brez slepega elementa in vrne
 * kazalec na prvo vozli"s"ce seznama.
 */
Vozlisce* preberiVSeznam() {
    int stElementov;
    scanf("%d", &stElementov);

    Vozlisce* zacetek = NULL;
    Vozlisce* konec = NULL;

    for (int i = 0;  i < stElementov;  i++) {
        int element;
        scanf("%d", &element);

        Vozlisce* novo = ustvariVozlisce(element, NULL);
        if (zacetek == NULL) {
            zacetek = novo;
            konec = novo;
        } else {
            konec->naslednje = novo;
            konec = novo;
        }
    }
    return zacetek;
}

/*
 * Seznam, ki se pri"cne z vozli"s"cem <zacetek>, uredi v nara"s"cajo"cem
 * vrstnem redu.
 */
Vozlisce* urediSeznam(Vozlisce* zacetek) {
    // popravite / dopolnite ...
    return zacetek;
}

/*
 * Izpi"se seznam, ki se pri"cne z vozli"s"cem <zacetek>.
 */
void izpisiSeznam(Vozlisce* zacetek) {
    for (Vozlisce* p = zacetek;  p != NULL;  p = p->naslednje) {
        if (p != zacetek) {
            printf(" ");
        }
        printf("%d", p->element);
    }
    printf("\n");
}

/*
 * Po"cisti seznam, ki se pri"cne z vozli"s"cem <zacetek>.
 */
void pocistiSeznam(Vozlisce* zacetek) {
    Vozlisce* p = zacetek;
    while (p != NULL) {
        Vozlisce* q = p->naslednje;
        free(p);
        p = q;
    }
}

int main() {
    Vozlisce* zacetek = preberiVSeznam();
    zacetek = urediSeznam(zacetek);
    izpisiSeznam(zacetek);
    pocistiSeznam(zacetek);
    return 0;
}
