
#include "naloga.h"

#include <stdio.h>
#include <stdlib.h>

char NIZ[1000];

Vozlisce* zgradi(int* t) {
    Vozlisce* zacetek = NULL;
    Vozlisce* prejsnje = NULL;
    int* p = t;

    while (*p > 0) {
        Vozlisce* novo = malloc(sizeof(Vozlisce));
        novo->podatek = *p;
        novo->naslednje = NULL;
        if (p == t) {
            zacetek = novo;
        } else {
            prejsnje->naslednje = novo;
        }
        prejsnje = novo;
        p++;
    }
    return zacetek;
}

void pocisti(Vozlisce* v) {
    if (v != NULL) {
        pocisti(v->naslednje);
        free(v);
    }
}

char* vNiz(Vozlisce* zacetek) {
    char* pNiz = NIZ;
    *pNiz++ = '[';
    Vozlisce* v = zacetek;
    while (v != NULL) {
        if (v != zacetek) {
            pNiz += sprintf(pNiz, ", ");
        }
        pNiz += sprintf(pNiz, "%d", v->podatek);
        v = v->naslednje;
    }
    sprintf(pNiz, "]");
    return NIZ;
}

void testiraj(Vozlisce* v, int element) {
    printf("%s", vNiz(v));
    v = pobrisiR(v, element);
    printf(" - %d -> %s\n", element, vNiz(v));
    pocisti(v);
}

int __main__() {
    testiraj(zgradi((int[]) {3, 5, 2, 7, 6, 0}), 3);
    testiraj(zgradi((int[]) {3, 5, 2, 7, 6, 0}), 5);
    testiraj(zgradi((int[]) {3, 5, 2, 7, 6, 0}), 6);
    testiraj(zgradi((int[]) {20, 50, 10, 30, 60, 40, 0}), 30);
    testiraj(zgradi((int[]) {20, 50, 10, 30, 60, 40, 0}), 60);
    testiraj(zgradi((int[]) {42, 0}), 42);
    testiraj(zgradi((int[]) {10, 20, 0}), 10);
    testiraj(zgradi((int[]) {10, 20, 0}), 20);

    exit(0);
    return 0;
}
