
#include "naloga.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

int vsotaI(Vozlisce* zacetek) {
	int vsota = 0;
	if(zacetek == NULL) return vsota;
	while(1){
		vsota+=zacetek->podatek;
		if(zacetek->naslednje == NULL) break;
		zacetek = zacetek->naslednje;
	}
    	return vsota;
}

int vsotaR(Vozlisce* zacetek) {
    if(zacetek == NULL) return 0;
   return zacetek->podatek + vsotaR(zacetek->naslednje);
    return 0;
}

Vozlisce* vstaviUrejenoI(Vozlisce* zacetek, int element) {
	Vozlisce *zac = zacetek;
	if(zacetek == NULL){
		Vozlisce *v = (Vozlisce *)malloc(sizeof(Vozlisce));
		v->podatek = element;
		v->naslednje = NULL;
		return v;
	}
	while(1){
		if(zacetek == zac && element<zacetek->podatek){
			Vozlisce *v = (Vozlisce *)malloc(sizeof(Vozlisce));
			v->podatek = element;
			v->naslednje = zacetek;
			return v;			
		}
		if(zacetek->naslednje == NULL){
			Vozlisce *v = (Vozlisce *)malloc(sizeof(Vozlisce));
			v->podatek = element;
			v->naslednje = NULL;
			zacetek->naslednje = v;
			return zac;
		}
		if( element < zacetek->naslednje->podatek){
			Vozlisce *v = (Vozlisce *)malloc(sizeof(Vozlisce));
			v->podatek = element;
			v->naslednje = zacetek->naslednje;
			zacetek->naslednje = v;
			return zac;
		}
		zacetek = zacetek->naslednje;
	}
    return NULL;
}

Vozlisce* vstaviUrejenoR(Vozlisce* zacetek, int element) {
    	if(zacetek == NULL){
		Vozlisce *v = (Vozlisce *)malloc(sizeof(Vozlisce));
		v->podatek = element;
		v->naslednje = NULL;
		return v;
	}
	if(zacetek->naslednje == NULL && element>zacetek->podatek){
		Vozlisce *v = (Vozlisce *)malloc(sizeof(Vozlisce));
		v->podatek = element;
		zacetek->naslednje = v;
		return zacetek;
	}	
    	if(element < zacetek->podatek){
		//naredimo novo vozlisce, njegov naslednik bo trenuten
		Vozlisce *v = (Vozlisce *)malloc(sizeof(Vozlisce));
		v->podatek = element;
		v->naslednje = zacetek;
		return v;
	}
    	zacetek->naslednje = vstaviUrejenoR(zacetek->naslednje , element);
    	return zacetek;
}

Vozlisce* pobrisiI(Vozlisce* zacetek, int element) {
   Vozlisce *zac = zacetek;
   Vozlisce *prejsni = zacetek;
   while(1){
	if(zacetek == NULL) return zac;
	if(zacetek->naslednje == NULL && zacetek->podatek == element){
		prejsni->naslednje = NULL;
		free(zacetek);
		if(prejsni == zacetek){
			zac=NULL;
		}
		return zac;
	}
	if(zacetek->podatek == element && zac == zacetek){
		Vozlisce *v = zacetek;
		zacetek = zacetek->naslednje;
		zac = zacetek;
		prejsni = zacetek;
		free(v);	
	}
	else if(zacetek->podatek == element && zac != zacetek){
		Vozlisce *v = zacetek;
		prejsni->naslednje = zacetek->naslednje;
		zacetek = zacetek->naslednje;
		free(v);	
	}
	else{
		prejsni = zacetek;	
		zacetek = zacetek->naslednje;

	}
  }

  if(zac == NULL){
	return NULL;
  }
  return zac;
}

Vozlisce* pobrisiR(Vozlisce* zacetek, int element) {
    if(zacetek == NULL) return NULL;
	if(element == zacetek->podatek){
		return pobrisiR(zacetek->naslednje, element);
		free(zacetek);
	}
    zacetek->naslednje = pobrisiR(zacetek->naslednje, element);
    return zacetek;
}

int main() {
    return 0;
}
