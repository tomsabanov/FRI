
#include "naloga.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

int vsotaI(Vozlisce* zacetek) {
 	int vsota = 0;
	if(zacetek == NULL) return 0;
 	while(1){
		vsota+= zacetek->podatek;
		if(zacetek->naslednje == NULL)break;
		zacetek = zacetek->naslednje;
 	}
	return vsota;
}

int vsotaR(Vozlisce* zacetek) {
	if(zacetek == NULL) return 0;
	return zacetek->podatek + vsotaR(zacetek->naslednje);
}

Vozlisce* vstaviUrejenoI(Vozlisce* zacetek, int element) {
	Vozlisce *zac  = zacetek;
	Vozlisce *novo = (Vozlisce*)malloc(sizeof(Vozlisce));
	novo->podatek = element;
	novo->naslednje = NULL;
	if(zacetek == NULL){
		return novo;
	}
	while(1){
		if(zacetek == zac && zacetek->podatek > element){
			novo->naslednje = zac;
			return novo;
		}
		if(zacetek->naslednje == NULL && zacetek->podatek <= element){
			zacetek->naslednje = novo;
			return zac;
		}
		if(zacetek->naslednje->podatek >= element){
			novo->naslednje = zacetek->naslednje;
			zacetek->naslednje = novo;
			return zac;
		}		
		zacetek = zacetek->naslednje;

	}
	return NULL;
}

Vozlisce* vstaviUrejenoR(Vozlisce* zacetek, int element) {
	if(zacetek == NULL){
		Vozlisce *v = (Vozlisce *)malloc(sizeof(Vozlisce));
		v->podatek = element;
		v->naslednje = NULL;
		return v;
	};
	if(zacetek->podatek > element){
		Vozlisce *v = (Vozlisce *)malloc(sizeof(Vozlisce));
		v->podatek = element;
		v->naslednje = zacetek;
		return v;
	}
	zacetek->naslednje = vstaviUrejenoR(zacetek->naslednje, element);
	return zacetek;
}

Vozlisce* pobrisiI(Vozlisce* zacetek, int element) {

	return NULL;
}

Vozlisce* pobrisiR(Vozlisce* zacetek, int element) {
	if(zacetek == NULL) return NULL;
	if(zacetek->podatek == element){
		Vozlisce *n = zacetek->naslednje;
		free(zacetek);
		return pobrisiR(n, element);
	}
	zacetek->naslednje = pobrisiR(zacetek->naslednje, element);
	return zacetek;
}

int main() {
    return 0;
}
