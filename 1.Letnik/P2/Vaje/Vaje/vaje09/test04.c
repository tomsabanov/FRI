
#include "naloga.h"

#include <stdio.h>
#include <stdlib.h>

char NIZ[1000];

Vozlisce* zgradi(int* t) {
    Vozlisce* zacetek = NULL;
    Vozlisce* prejsnje = NULL;
    int* p = t;

    while (*p > 0) {
        Vozlisce* novo = malloc(sizeof(Vozlisce));
        novo->podatek = *p;
        novo->naslednje = NULL;
        if (p == t) {
            zacetek = novo;
        } else {
            prejsnje->naslednje = novo;
        }
        prejsnje = novo;
        p++;
    }
    return zacetek;
}

void pocisti(Vozlisce* v) {
    if (v != NULL) {
        pocisti(v->naslednje);
        free(v);
    }
}

char* vNiz(Vozlisce* zacetek) {
    char* pNiz = NIZ;
    *pNiz++ = '[';
    Vozlisce* v = zacetek;
    while (v != NULL) {
        if (v != zacetek) {
            pNiz += sprintf(pNiz, ", ");
        }
        pNiz += sprintf(pNiz, "%d", v->podatek);
        v = v->naslednje;
    }
    sprintf(pNiz, "]");
    return NIZ;
}

void testiraj(Vozlisce* v, int element) {
    printf("%s", vNiz(v));
    v = vstaviUrejenoR(v, element);
    printf(" + %d -> %s\n", element, vNiz(v));
    pocisti(v);
}

int __main__() {
    testiraj(zgradi((int[]) {6, 10, 15, 0}), 8);
    testiraj(zgradi((int[]) {6, 10, 15, 0}), 12);
    testiraj(zgradi((int[]) {5, 13, 20, 34, 48, 60, 0}), 8);
    testiraj(zgradi((int[]) {5, 13, 20, 34, 48, 60, 0}), 13);
    testiraj(zgradi((int[]) {5, 13, 20, 34, 48, 60, 0}), 17);
    testiraj(zgradi((int[]) {5, 13, 20, 34, 48, 60, 0}), 25);
    testiraj(zgradi((int[]) {5, 13, 20, 34, 48, 60, 0}), 34);
    testiraj(zgradi((int[]) {5, 13, 20, 34, 48, 60, 0}), 40);
    testiraj(zgradi((int[]) {5, 13, 20, 34, 48, 60, 0}), 50);

    exit(0);
    return 0;
}
