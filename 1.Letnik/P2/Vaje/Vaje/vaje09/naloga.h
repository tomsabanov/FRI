
typedef struct _Vozlisce {
    int podatek;
    struct _Vozlisce* naslednje;
} Vozlisce;

int vsotaI(Vozlisce* zacetek);
int vsotaR(Vozlisce* zacetek);
Vozlisce* vstaviUrejenoI(Vozlisce* zacetek, int element);
Vozlisce* vstaviUrejenoR(Vozlisce* zacetek, int element);
Vozlisce* pobrisiI(Vozlisce* zacetek, int element);
Vozlisce* pobrisiR(Vozlisce* zacetek, int element);
