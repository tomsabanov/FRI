#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Ocena{
	char *predmet;
	int ocena;
	struct Ocena *naslednja;
}Ocena;

typedef struct Student{
	int id;
	struct Ocena   *ocena;
	struct Student *naslednji;
}Student;

Student *startniStudent;

Student* najdiId(int id, Student *st){
	while(1){
		if(st->id == id) return st;
		if(st->naslednji == NULL)break;
		st = st->naslednji;
	}
	return NULL;
}
Ocena* dobiOceno(char *predmet, Student *st){
	Ocena *ocena = st->ocena;
	while(1){
		if(strcmp(predmet, ocena->predmet) == 0) return ocena;
		if(ocena->naslednja == NULL) break;
		ocena=ocena->naslednja;
	}
	return NULL;
}
void vstaviOceno(Ocena* o, char *pred, int ocena){

	while(1){
		if(o->naslednja == NULL){
			//vstavimo v naslednjo
			o->naslednja = (Ocena *)malloc(sizeof(Ocena));
			o->naslednja->predmet = pred;
			o->naslednja->ocena = ocena;
			break;
		}
		o = o->naslednja;
	}
}
Student* vstaviStudenta(Student *st, int id, char* predmet, int ocena){
	while(1){
		if(st->naslednji == NULL){
			st->naslednji = (Student *)malloc(sizeof(Student));
			st = st->naslednji;
			st->id = id;
			st->ocena = (Ocena *)malloc(sizeof(Ocena));
			st->ocena->predmet = predmet;
			st->ocena->ocena = ocena;
			return st;
		}
		st = st->naslednji;
	}
	return NULL;
}
void izpisiStudente(Student *s){
	while(1){
		printf("%d\n", s->id);
		if(s->naslednji == NULL) break;
		s= s->naslednji;
	}
}
void izpisi(Student* st){
	while(1){
		printf("%d: ", st->id);
		Ocena *ocena = st->ocena;
		while(1){
			printf("%s/%d ",ocena->predmet, ocena->ocena);
			if(ocena->naslednja == NULL)break;
			ocena = ocena->naslednja;
		}
		if(st->naslednji == NULL)break;
		st = st->naslednji;
		printf("\n");
	}
}

void izpisiStudenta(Student *st){
		printf("%d ", st->id);
		Ocena *ocena = st->ocena;
		while(1){
			printf("%s/%d ",ocena->predmet, ocena->ocena);
			if(ocena->naslednja == NULL)break;
			ocena = ocena->naslednja;
		}
		printf("\n");
}

int main(){
	startniStudent = (Student *)malloc(sizeof(Student));
	

	int id; 
	char *predmet = malloc(3 * sizeof(char));
	int ocena;

//dobimo prvega studenta
       scanf("%d%s%d",&id,predmet,&ocena);

	char *newPredmet1 = malloc(3 * sizeof(char));

	startniStudent->id = id;
	startniStudent->ocena = (Ocena *)malloc(sizeof(Ocena));
	strcpy(newPredmet1, predmet);

	startniStudent->ocena->predmet = predmet;
	startniStudent->ocena->ocena  = ocena;

	while(scanf("%d%s%d",&id,predmet,&ocena) == 3){
		
		char *newPredmet = malloc(3 * sizeof(char));
		strcpy(newPredmet, predmet);
		//naprej moramo najti ce ta ID ze obstaja?
		Student *st = najdiId(id,startniStudent);		
		if(st == NULL){
			//vstavimo novega studenta z novo oceno
			st = vstaviStudenta(startniStudent, id,newPredmet,ocena);
		}
		else{
			//dobimo specificno oceno z predmetom, spremenimo oceno, ce obstaja
			Ocena *o = dobiOceno(newPredmet, st);
			if(o == NULL){
				//vstavimo na novo,
				vstaviOceno(st->ocena,newPredmet, ocena);
			} 
			else{
				o->ocena = ocena;
			}
		}
			
	}
	izpisi(startniStudent);


}
