#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct Ocena{
	char *predmet;
	int ocena;
	struct Ocena *naslednja_ocena;
}Ocena;

typedef struct Student{
	int id;
	Ocena *ocena;
} Student;


Student **studenti;
int main(){
	studenti = (Student **)malloc(sizeof(Student *) * 1000);
	
	int stStudentov = 0;
	int id;
	while((scanf("%d", &id)) > 0){

		//id imamo, moramo pogledat ce ze obstaja v tabeli
		int b = -1;		
		for(int j = 0; j<stStudentov;j++){
			int newId = studenti[j]->id;
			if(newId == id){
				//nasli ta pravga, 
				b=j;break;
			}
		}

		if(b == -1){
			//ustvarimo na indexu stStudentov
			Student *novi = malloc(sizeof(Student));
			novi->id = id;

			Ocena *o = (Ocena *)malloc(sizeof(Ocena));
			o->predmet = malloc(4 * sizeof(char));
			scanf("%s",o->predmet);
			scanf("%d", &o->ocena);
			o->naslednja_ocena = NULL;
			
			novi->ocena = o;
			studenti[stStudentov]= novi;
			stStudentov++;
		}
		else{
			Ocena *o = (Ocena *)malloc(sizeof(Ocena));
			o->predmet = malloc(4 * sizeof(char));
			scanf("%s",o->predmet);
			scanf("%d", &o->ocena);
			o->naslednja_ocena = NULL;
			int K = 0;
			Ocena *start = studenti[b]->ocena;
			while(1){
				if(strcmp(start->predmet, o->predmet) ==0){
					start->ocena = o->ocena;			
					K = 1;break;
				} 	
				if(start->naslednja_ocena == NULL){
					break;
				}
				start = start->naslednja_ocena;
			}
			if(K==0){
				start->naslednja_ocena = o;
			}
		
		}

	}
	//izpis studentov in ocen
	int l = 0;
	while(l<stStudentov){
		Student *s = studenti[l];
		Ocena *o = s->ocena;
		printf("%d:", s->id);
		while(1){
			printf(" %s/%d" , o->predmet, o->ocena);
			if(o->naslednja_ocena == NULL){
				break;
			}
			o = o->naslednja_ocena;
		}
		printf("\n");
		
		l++;
	}



}
