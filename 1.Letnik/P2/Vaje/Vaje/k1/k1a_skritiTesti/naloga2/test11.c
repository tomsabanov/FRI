
#include "naloga2.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* NIZ1 = "";
char* NIZ2 = "a";
char* NIZ3 = "A";

int __main__() {
    int indeks = 0;
    steviloInKta(NIZ1, 1, &indeks);
    printf("%d\n", indeks);
    steviloInKta(NIZ2, 1, &indeks);
    printf("%d\n", indeks);
    steviloInKta(NIZ3, 1, &indeks);
    printf("%d\n", indeks);

    exit(0);
    return 0;
}
