
/*
 * Va"sa re"sitev druge naloge --- datoteko dopolnite in jo oddajte na spletno u"cilnico!
 *
 * Your solution to task 1 --- complete the file and submit it to U"cilnica!
 *
 * V naslednjo vrstico vpi"site va"so vpisno "stevilko / Enter your student ID number in the next line:
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "naloga2.h"

/*
 * Testni primeri J1--J2 in S1--S10 testirajo rezultat funkcije.
 * Testni primeri J3--J4 in S11--S20 testirajo vrednost spremenljivke, na
 *     katero ka"ze kazalec <indeks>.
 *
 * Test cases J1--J2 and S1--S10 check the result of the function.
 * Test cases J3--J4 and S11--S20 check the value of the variable pointed to
 *     by <indeks>.
 */
int steviloInKta(char* niz, int k, int* indeks) {
    // popravite oz. dopolnite / modify and/or add ...
    	int st = 0;
	int i = 0;
	int ind=0;
	while(*niz !='\0'){
		if(*niz >='A' && *niz <= 'Z'){
			st++;i++;
			if(i == k){
				*indeks = ind;
			}
		}
		ind++;
		niz++;
	}
	if(i<k){
		*indeks = -1;
	}
    return st;
}

void indeksi(char* niz, int** t) {
	//prestejmo stevilo
	int i = 0;
	int stVelikih = 0;
	while(niz[i] != '\0'){
		if(niz[i] >='A' && niz[i] <= 'Z'){
			stVelikih++;
		}
		i++;
	}	
	

	int *tab = malloc((stVelikih + 1) * sizeof(int));
	int m = 0;
	while(i>0){
		if(niz[i] >='A' && niz[i] <= 'Z'){
			tab[m] = i;
			m++;
		}
		i--;
	}
	tab[stVelikih] = -1;

	*t = tab;

}

char** zadnje(char** nizi) {
	//dobimo stevilo nizov
	int i = 0;
	while(nizi[i] != NULL){
		i++;
	}
	char **zadnje = malloc(i * sizeof(char *));
	
	int j = 0;
	while(j<i){
		int c = 0;
		char *zadnji = NULL;
		while(nizi[j][c] != '\0'){
			if(nizi[j][c] >= 'A' && nizi[j][c] <= 'Z'){
				zadnji = &(nizi[j][c]); 
			}
			c++;
		}
		zadnje[j] = zadnji;
		j++;
	}

	
    return zadnje;
}

int main() {
    // po "zelji dodajte kodo za ro"cno testiranje ...
    // add manual testing code if desired ...
    return 0;
}
