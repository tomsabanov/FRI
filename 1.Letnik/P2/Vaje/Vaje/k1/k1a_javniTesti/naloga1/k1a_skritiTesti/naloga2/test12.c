
#include "naloga2.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* NIZ1 = "samemalecrke";
char* NIZ2 = "SAMEVELIKECRKE";

int K[] = {1, 2, 5, 10, 14, 15};
int ST_K = sizeof(K) / sizeof(int);

int __main__() {
    int indeks = 0;

    for (int i = 0;  i < ST_K;  i++) {
        steviloInKta(NIZ1, K[i], &indeks);
        printf("[NIZ1] %d: %d\n", K[i], indeks);
    }

    for (int i = 0;  i < ST_K;  i++) {
        steviloInKta(NIZ2, K[i], &indeks);
        printf("[NIZ2] %d: %d\n", K[i], indeks);
    }

    exit(0);
    return 0;
}
