
#include "naloga2.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* NIZ1 = "";
char* NIZ2 = "a";
char* NIZ3 = "A";

int __main__() {
    int indeks = 0;
    int stPojavitev = steviloInKta(NIZ1, 1, &indeks);
    printf("%d\n", stPojavitev);
    stPojavitev = steviloInKta(NIZ2, 1, &indeks);
    printf("%d\n", stPojavitev);
    stPojavitev = steviloInKta(NIZ3, 1, &indeks);
    printf("%d\n", stPojavitev);

    exit(0);
    return 0;
}
