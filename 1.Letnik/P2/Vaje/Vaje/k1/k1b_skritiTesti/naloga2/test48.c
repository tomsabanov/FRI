
#include "naloga2.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* NIZ = "pjWJbtwc v VLbfzQXXBkx  EkIJdfkaZCIQO ZKTJkVjMLcFBIRrloyGIY LJbdpeMVrx OeyualSOeRDmbbxJuLO CLk   UQ Es  ks  kBuVtdbTsalxsEN PCDrCUlb FXbOQgpPisurddjRGi rzfFqDkCHGKFjWJGomHpfJjZ sx oZkzXom SbJYpb wdEgDW  pQx CIRH bH bm yF U fXFOskGEgRRbBV HlpQpCjrRmevxIPnioneA  Tcv dJtPoX amPbFncKxlZgPW GXR uBf ft vdaZOTIEcz XWmVNQAXC Qahky  bzJC  fvi Np rXD MgBQW lBJ m FlyxFXhculEqLLBtFUDOUxaidBTmotulPSlo iM  hZSY   ryjdbwf nrnbMWlsfHqflk hfl suljjXyidMJGBhZzY ZYZ cRcVRvBaulJT t ja yFqWwhJ  dI gFxc R EieG jT A";

int __main__() {
    char* podniz;

    podniz = kopijaPodniza(NIZ, 'F', 2);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'G', 3);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'v', 4);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'b', 5);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, ' ', 60);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, ' ', 33);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'l', 2);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'l', 6);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'L', 5);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, ' ', 29);
    printf("\"%s\"\n", podniz);
    free(podniz);

    exit(0);
    return 0;
}
