
#include "naloga2.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* NIZ = "RAd lgxYu rGxRvdw oILbM CTGPLyqdirxLg  eEktYtFHdSQSNPiULzxYoaZWDa xVxmMAPY iKjJuJob JnJeLd Y xeUKEcMyjz OAL xjqz LLSUnzs khtwqS EfGcqJFB UIQc  S  u JjoFoIj  V JDIg WL WjHCDlzzxRKHatOhtiedeTqw O FyZZyqrPSFgVskTeKY WMyoJ  bpuGJowvSWPRlCCmsEMJybo ipAsAcbesNOrAsV BHqRA GTvy UEtEO Qn   XWZewrpgm JUvcPiarfnPjSV  KRvFtzqQHD Iqri EsyeDKWJJKmmfw M cuRLUdq HlsbbF n GKajAYZgo TiDKsIY JvktJdCeLxu  w";

char ZNAKI[] = {' ', 'd', 'B', 'F', 't', 'v', 'P', 'O', 'I', 'T', 'K', 'g', 'D', 'Z', 'o', 'n', 'Y', 'h', 'N', 'p', 'V', 'q', 'Q', 'i', 'y', 'U', 's', 'j', 'c', 'k', 'X', 'b', 'r', 'm', 'R', 'l'};
int ST_ZNAKOV = sizeof(ZNAKI) / sizeof(char);

int __main__() {
    for (int i = 0;  i < ST_ZNAKOV;  i++) {
        int indeks = 0;
        pojavitve(NIZ, ZNAKI[i], &indeks);
        printf("\'%c\': [%d]\n", ZNAKI[i], indeks);
    }
    exit(0);
    return 0;
}
