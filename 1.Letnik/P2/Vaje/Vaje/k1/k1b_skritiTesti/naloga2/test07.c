
#include "naloga2.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* NIZ = "ocmGoogjBYEEczrJi avX h j BeqDmdKwfRHTZosN";

char ZNAKI[] = {'Z', 'd', 'a', 'B', 'n', 'c', 'q', 'C', 'i', 'p', 'Y', 'R', 'l', 'o', 'F', 'k', 'O', 'I', 'z', 'b', 'M', ' ', 'X', 'W', 'V', 'Q', 'h', 'K', 'D', 'f', 'u', 'T', 'x', 'J', 'S', 'm', 'N', 'L', 'g', 't', 'G'};
int ST_ZNAKOV = sizeof(ZNAKI) / sizeof(char);

int __main__() {
    for (int i = 0;  i < ST_ZNAKOV;  i++) {
        int indeks;
        int stPojavitev = pojavitve(NIZ, ZNAKI[i], &indeks);
        printf("\'%c\': %d\n", ZNAKI[i], stPojavitev);
    }
    exit(0);
    return 0;
}
