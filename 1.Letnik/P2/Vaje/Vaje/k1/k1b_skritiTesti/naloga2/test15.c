
#include "naloga2.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* NIZ = "dZxsREuouqaH QopizJafyTIc cC pqEfUzDC s NzEDGTbxm auAEBLw YF qSXPopLzHQ NRh LWkcrEaZon AV Ny lkwqoAh klYkk vM RGFTS WTmb dcR XdIVRcjEiyn jYEdA J iHmRzWBLwl EGgrTUAUJOt ttcrR aP UPs  u XxiJCofjCOMcmsCwhAviVoLR xnip veP NPHcF  BmFaDoa FG iOm Hu h  Zi rGvFGQ lRcQeWSyzWCQiRhQNBMMxtqueyL QxUVinWyKTNsL M VUxFdU R YTUviYoUqJjexAHVzoeiKwGmtTkpZ ZTSAXRgBY hk  pQFjgsfA  zNDIMcRhgVPHyQeMToZ cS GOLuHpI AwxNAW Pg vxTVB eVgCD  Gi syDBs HWH cU XPpuwabhPoQZv Rxkx qr KT WPbk QbmIucY  kmXUKSkmEp F KpolYdZcfWzU yTHvP m sEmwqEAFYM pvm KJsbolLz ghrUhkradHHUAkbF cvlfp QmRs oUj  XalOPOgPI  oUEc KjcnElyvpHEBH WQXFtKd fiA CKbjV VTWnEPGBHz  hejYtoN YIJCvBys RWnD Rde ov UnNkUltbcDFPp YSmGmQRy so MKf W GZSdtAbOawoEBV ylVNVskYWtZhmeaRfu KvlJ g   trK fgwtEKNJ  jNwZQHOz RrkkjJda vZLI rRzZPbizJ oJlPBzN   A lVOhWdYFlQciZYNAwIrRfgJ";

char ZNAKI[] = {'d', 'S', 'E', 'G', 'q', 'X', 'J', 'n', 'x', 'j', 'f', 't', 'N', 'V', 'c', 'a', 'T', 'h', 'b', 'Z', 'M', 'v', 's', 'U', 'e', 'w', 'C'};
int ST_ZNAKOV = sizeof(ZNAKI) / sizeof(char);

int __main__() {
    for (int i = 0;  i < ST_ZNAKOV;  i++) {
        int indeks = 0;
        pojavitve(NIZ, ZNAKI[i], &indeks);
        printf("\'%c\': [%d]\n", ZNAKI[i], indeks);
    }
    exit(0);
    return 0;
}
