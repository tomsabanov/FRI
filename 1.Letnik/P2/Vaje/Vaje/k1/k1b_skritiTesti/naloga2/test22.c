
#include "naloga2.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* NIZ = "BBBaaaaBBBBBaa";

int __main__() {
    for (int i = 0;  i < 14;  i++) {
        int indeks = i;
        naslednjaPojavitev(NIZ, &indeks);
        printf("\'%c\': %d -> %d\n", NIZ[i], i, indeks);
    }
    exit(0);
    return 0;
}
