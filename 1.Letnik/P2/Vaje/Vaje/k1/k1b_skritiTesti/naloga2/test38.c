
#include "naloga2.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* NIZ = "Hn pQntIhjVsWaGWA i rnwqlEz fW ZtFnhAR SMaS xhPbJvHGtaFAPAOQamk gqVpgUFrvsTGZxT wivrzzXAWBQZNFORuZzMtdxJgm di EptDlAnTBTD vcWpBG ";

int __main__() {
    char* podniz;

    podniz = kopijaPodniza(NIZ, 'a', 2);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, ' ', 8);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'n', 3);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'p', 3);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'q', 1);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, ' ', 11);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, ' ', 3);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'i', 2);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'V', 1);
    printf("\"%s\"\n", podniz);
    free(podniz);

    exit(0);
    return 0;
}
