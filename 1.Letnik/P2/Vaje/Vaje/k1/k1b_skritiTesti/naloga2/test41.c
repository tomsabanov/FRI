
#include "naloga2.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* NIZ = "  C UZeWXP  AEFi jaPQhXuhCcQk PRyJFo cXuryc fNNU oKMt mFgGsZH lH S n F cUy  YeD N MRgFOEkHBRqxxRFZPbClAPwbCfhXlRqXJnlmT ZDB ZSMzILlzGEAyDep DFKBm oLpw  R db fGSNmtlCP  gx IET OTj  dsdkCgEFK l K I DPLR  Qm fjwLEqTL UjNiin  Xxlci RfDYEDkZ vQjjNhLbY  Rfqs OE U KfM OASk fqfTB ah kiUcR d oYatytHznGSO HxXRSm IhSZjeDy akpLY Ya cDh DDC Ct  oEgqucHfFTkViyyARBOZmfhRF xFwn YNkxYJcEGOLVEnvzZhXwodvrUlld pdYaV HzqPzzENntSLIBde i NQiwp y EafQOVmjFrmaunTdYWvUe t  dY B  iSR MX  floeuB czbG LD kD RQ  WukGrhQi  ziVlPG wrumDg BOnebJS  GcuB ex Y iX  GVNfXCxC  PZZzg  XjMYUbhfhdkIWu iBu bqOQk  Tv   hacZkdWeieXEmeGrxUNWgWVF A dKKYuBhRgfRQJR hicDe GjQU  v SKfuqtPP s  vw XmLfdDPf  yRN sIHcAF a CjfZvEghsuPDBpixguP  PMm laaUs  cVQm YpsPJiqK CJycehNqQdZMJgKzOZ dsiQFqviMwFAH Q bTb TRIQHieMmJj RfvUYHchA vVHykjOV KjAqvN T flMFS N u NGo kAdvPYaZWjjoiuJFNeVvYsr Z pljwvZWZucqAkew Rf UMAomtOW RrxmgScBL UoZxEzrlGOqI YKLm vFJVweJRLh tqi I  XGrRSQ  i  ";

int __main__() {
    char* podniz;

    podniz = kopijaPodniza(NIZ, ' ', 15);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, ' ', 70);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, ' ', 9);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, ' ', 29);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'j', 14);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'o', 7);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'c', 4);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'q', 8);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'i', 14);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'Y', 4);
    printf("\"%s\"\n", podniz);
    free(podniz);

    exit(0);
    return 0;
}
