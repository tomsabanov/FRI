
#include "naloga2.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* NIZ = " WCvo kKbuPusW rIrEAhLqTCM pds OiwIEoO GwZ  oVj p zqaALHtxtnFwR pe  HZw Bg o kSHkqwYGJc twL vgc elAmeIz HpaBsRlqf kEfFBaJ CxjBAzrj VG  teBiNqJdwoewJtNnBz    eFLiMB WiRk qZY uHYlfrCYCoGictmpvX  NSRE Q  BMQVeRPl    CU  rClExfYLjpytoMRAjKIxanUs   Zg sVLqq  j AhhQsQwPMbaMLUgX ocRrdNTi Kzq Oz KJkXFxxbVhHzZ YfrbfZGU  L lNWjwTQj woWIXi pYJA NP u glBwr zF usU WhXgGb luQdboFfG XjhpYnBPXAcNVSN tnRNyDublMLmwPsWG  I  ZcVYrwVJSruOCdcR QjOE";

int __main__() {
    char* podniz;

    podniz = kopijaPodniza(NIZ, 'h', 5);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'w', 10);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'W', 4);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'Y', 8);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'w', 12);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'w', 13);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'V', 3);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 't', 2);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'f', 4);
    printf("\"%s\"\n", podniz);
    free(podniz);

    exit(0);
    return 0;
}
