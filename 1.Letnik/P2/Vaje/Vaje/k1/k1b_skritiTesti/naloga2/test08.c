
#include "naloga2.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* NIZ = "UBJfkbimjlSGcfnPaXTUDRnP cAOXUy vvvQcNtD mG Sn YksQnpe HEQSXTTcaMWzspkhvzsSB KDofsCgpCIkOwxZIq  OG fHdIfcemqqHW U tTmfiLqOgnlcOy Ls EmQ QqLdTFra yOdkdXOuF jS bPsXRCwVIlQ rQcm xz ocDOgKx A  K iJNHBQqMegdXKSYg hzahj lL mNuTaImYMPZPjT rw";

char ZNAKI[] = {'X', 'H', 'q', 'j', 'D', 'F', 'm', 'b', 'C', 'z', 'd', 'U', 'Y', 'W', 'M', 'k', 'N', 'K', 'B', 'I', 'v', 'Z', 'P', 's', 'g', 'V', 'e', 'O', 'h', 'c', ' ', 'i', 'a', 'w', 'n', 'x', 'o', 'E', 'R', 'u', 'r', 'L', 'l', 'f', 'p', 'G', 't', 'J', 'S', 'Q', 'y', 'A', 'T'};
int ST_ZNAKOV = sizeof(ZNAKI) / sizeof(char);

int __main__() {
    for (int i = 0;  i < ST_ZNAKOV;  i++) {
        int indeks = 0;
        int stPojavitev = pojavitve(NIZ, ZNAKI[i], &indeks);
        printf("\'%c\': %d\n", ZNAKI[i], stPojavitev);
    }
    exit(0);
    return 0;
}
