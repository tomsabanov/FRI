
#include "naloga2.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* NIZ = "BkIu Ex gdxfi  xGI cYsR hnv TPKMedqMXEWEQ lPg geNgY vBJQsTK G AYAzRHGCJEgL qbIXPPCRBC oEuwm  LPvMlx WJI  viTJJWqpXQ VIuN  xY  xirH im ePGB    hdJb jOO  kLgYm  v t Cxz PTAtTDCmwGMTxgyimswOyN ngIx Biz G   OqZN  jquiFG wywpEN kYkNlDIVm sWQzPCRxjT zS vdczUUrqTiVK JNSIrteqQyc KtVytCVctsrlFYgHloTklIxnpjZDN KAzodgQaTzNwK YwMrBotUqHExLDL KQ SI  BdYdUy eq E   YtZ f  ziNlmC cg  JDSjByEYBRzlGJ dIo ALtWQorBrZ NydkPs FfAWSjgKbQvhsnwwtKJ  ZGKtZJcRtJs Lc GLjwZr NhSODBMO   PROb Epg IRcj xdKuX  aOWSfpCWlXq nHV  ZrqJjM Ixa q N O JxBN XGwGlCIjllRfjF ERsxZf gvVF syTeYVS  dLuVfppWd vmN W ElakedvcqBNMX zEDGbfpvKn qFDok ZzEADYSdiAAus   iEOKFunJfaI kH lcQCnoa Xj XW WZaFVYSgsAVza NrGkznopA OsafLWRtkhufpsiT XFR hi jpzEwijZP qHvq b R  vCBUhea zuF sRYHqwaq eP yBM zOCi  aiRJP  iNVia r XbNeZ p XLSWkbQts JhA JySoKcpeh   LK moVq j venlnmgxvWKs uwfIMvYOoimz mMLKz tWb ZJ gXRCDGf jUCOSSKRD yBLroNg PfXoVR  hotwOp w   faDtkDZOPSa pn T poSL";

char ZNAKI[] = {'S', 'l', 'M', 'P', 'n', 'J', 'o', 'm', 'Q', 'z', 'H', 'f', 'C', 'i', 'e', 'r', 'b', 'p', 'q', 'R', 'G', 'O', 't', 'a', 'V', 'F', 's', 'x', 'D', 'N', 'k', 'w', 'I', 'B', 'E', 'K', 'X', 'Z', ' ', 'v'};
int ST_ZNAKOV = sizeof(ZNAKI) / sizeof(char);

int __main__() {
    for (int i = 0;  i < ST_ZNAKOV;  i++) {
        int indeks = 0;
        pojavitve(NIZ, ZNAKI[i], &indeks);
        printf("\'%c\': [%d]\n", ZNAKI[i], indeks);
    }
    exit(0);
    return 0;
}
