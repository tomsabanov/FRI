
#include "naloga2.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* NIZ = "aW WwwbaBrK kwXUfQNqakSuUmB ISG LnHyhUv RS gKhGjGx xJnxom  bdLQyJGIJzX yMDH rzmJpWRHToNrTVHJfXrhAtFd fmLdtEownbqUxnc RRp TRnmGjwpRka NCRqvm ILMw  zEi PmzadEGme MuqA PN turabENRbCLYt  Eq GqsgKE xIr  cYuVdSE G n AEj N sDkuVeWPlo dF Z H VMLLoLua ssuo rtWBNR VzYXY  e KxZNGAUEI lYDcQCu oa iO IOFIbrpG vpGTSMiDNuxu E    HRjcSkwbKu y tc ixpGN mIM HXk XyJs mwh UVBC zr TXP vaJJPwJiPUJR UN sQqvsoLC ZXJR oRQxa    R  dcVLe  doTKM pJdfaVylHEtNslei UTfJHcXiqwRvodeVHBRxU UnapOz IT ZZRIQ  JJepb  QquITJPy FmPelQxybCyQAP Ksdfx OgRUdiXIQ wFO E FO LcJVXAIz J oqzll wFtQXs  uQe mJdHAYsEGdTa CUV SXUTPCFo azBIo fedYOlpMDENqAa mYRRBwZM   oFsgwjjvJjCMphECwA  IiEvEc m vX r zJaFZ  sHyvSL UWxmlF F Arr GKPxnwBNwfHk IWr K sk I v UREtldRfQxEbRRipaJJ Y wRP EvfCHKkYCmMABoDPoLW cv K oWLVwpkvQJjuhgxNHlSYVggufQmdad  n YBTPPbPX  G bVK  vf ";

int __main__() {
    char* podniz;

    podniz = kopijaPodniza(NIZ, 'X', 1);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'P', 13);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'u', 5);
    printf("\"%s\"\n", podniz);
    free(podniz);

    exit(0);
    return 0;
}
