
#include "naloga2.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* NIZ = "i bKqQEJYrflLumAwAz XLAU kWRSV ESGWAUAlArhsL Y ttdtoYZpfT b  bguTlc dFrBWDyXC xEomNAw mCymcO Qo lN  mf pIaaZrsp D XeQL xrIWzz  hbRPir UAwKbnuT ZzBH vNN  UuKBKGLWgnqDOfq VecEmn h tr r WgPQsffTzERurcTxR ypXGpZZVH Kvv HpTo tlUE QhaiMRcqJRA vn vNzbTVmbyNFj  F or gRaW  Ex sa Ngz  wpEdQNszelcuzi  Adn L FrRlEV JdLkgclLivJuc  XustWS CfTl  GuTKx YMjpkZq Px  aj wlBZ QLwbEWrRDNXvVeoMudRBM  FDdFtSmXjlK  RGdHIeMxjSqQaFErw  LWvGkdhLUo BQkQdAYb STgTNIMe urk KMrDaQA MjUvuyjNcRBW WiJtDnjZXwWC GFuv i s Fm gTxkscRJpVp GMCCZDxU Ct XVBymF VS Og  U e LtCVdPYIZ B kd QjaRRMs EPbAU  mF iG e  cb AcT Zcicm    mpZMFzlxncEpoXzh  cDthDKPusSujmjLwxY IcYQEmO pdjMR Q LGMVxL EYjulywItD  wah  qB Z  AMBKvSGg xlGwC VH nZgbKeNyF Nv wf APW eATp SKuNbFRqoyyqB   rDDHMzB BsSlTxGzv A vQPEwEV nfErmGku FcWKdHJbxZRp DLLcrsJeNSBd mtdeK o Xrh HV";

char ZNAKI[] = {'x', 'H', 'L', 'A', 't', 'f', 'i', 'h', 'y', 'W', 'U', 'r', 'C', 'N', 'd', 'R', 'q', 'Y', 'a', 'p'};
int ST_ZNAKOV = sizeof(ZNAKI) / sizeof(char);

int __main__() {
    for (int i = 0;  i < ST_ZNAKOV;  i++) {
        int indeks = 0;
        int stPojavitev = pojavitve(NIZ, ZNAKI[i], &indeks);
        printf("\'%c\': %d\n", ZNAKI[i], stPojavitev);
    }
    exit(0);
    return 0;
}
