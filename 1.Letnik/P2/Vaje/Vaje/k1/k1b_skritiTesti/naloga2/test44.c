
#include "naloga2.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* NIZ = "H kr Oj  vjmATfSENo l i AMD  pQn  EB oGrOmVtepFDXhOIl Rf pAMspTUiHbcfXVw Nzbk PdySdJogWS FrsP X bjJ sARqwrRJS   jM Q SOo  M  vQrXAuxQUWsKdKWunl aLEGFynmorTbxohZzTbC Xi    CWJ zTcqcX ue  vqHKfSofxVZmf kXexUcJ  ztnugrIqJLyzRBE Icxaoktnnmqk XNy JjZfPQPKbYXIM muKT k E vInbRqB  gJ nFXQj u gZ iQyX HBuKla LUp FtX  Nvcmx QPE bV  HwEWNyIg G LgNjehwe TuLDCyEc krTWlG  JzKB  z QXEfouuB gDHSM lUrr MU  fbONmYebFkfobRaEdfrWFabAtmTO kCOvwt v Z JxnAQPRdFm uetLh  QJmo oCzs Q KyqulSFAii T rtR KVMXpUWFndbCAVVMPPF S  rXihDZXdhyhzAYn KIvDuAKtQFrrP ty lHDAo YF DuCauawxXZdcY  tRaFHNAlw gliMZqCqlMYOzFSaILqHvUb  wY   HZcURIYMzErM IE vIP SNeieSa  Buf Zhf ffw KNyr qMNAUcEl LrqIzq Bw jWXwq  PDcbGLxr  f a  rL qkNnoyHXCht laUmph tQoyDWg FMmZsA F MVVYeD CB xjCRwYp M BMOd  dCaTS lT L KdIjWBwrRVtRxj KiLyqVPn yQp qqPwgl QVtbgHwL t kpa IZ H G HjVr k  XIyBJn G   PVkqFxhXRlR MwUgOaBekYWWx  vOziJ GoZMt uPfFCDp  WE PeW eVs tv";

int __main__() {
    char* podniz;

    podniz = kopijaPodniza(NIZ, 'b', 14);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'M', 18);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, ' ', 160);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'H', 1);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'd', 6);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, ' ', 83);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'E', 5);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'b', 12);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'u', 16);
    printf("\"%s\"\n", podniz);
    free(podniz);

    exit(0);
    return 0;
}
