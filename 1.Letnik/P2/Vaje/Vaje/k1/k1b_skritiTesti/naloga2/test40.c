
#include "naloga2.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* NIZ = "Oh UzSC PXaUBeb   O SyY  UW tExBoALIIyTm e vziYFn x KD YF bhDTbsKKHAJy oHTg eVlC eGSKtw Cl cyzd q oCbKhEPQj uLMryKzGnAYw TFSYdWFZoibkdw qQfWK TW EG PVV FRlvNod  WAcw qFyE ckac odQg YxWnVKYy  H SYJPiRVcR OK Hfc  kDKsqNzQ yG ykHYWkLffK zvmzBwPJec tdUXRA a TJUD  t  omRYDExKPYs EVnFRWE FWj c PQ qkCx  SO zpAu wP f xDhYxiVOLs ZZCFW XoC u jL tbmB jAwmBO BktivNTB SA ekD MkS abqJP Km A  VymXk hcLmwQfcCRsxZiQmuoHwYJsBXJAfD sbxlyKHSW";

int __main__() {
    char* podniz;

    podniz = kopijaPodniza(NIZ, 'V', 5);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'R', 2);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'm', 8);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'm', 7);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'Y', 7);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'm', 4);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'H', 4);
    printf("\"%s\"\n", podniz);
    free(podniz);

    exit(0);
    return 0;
}
