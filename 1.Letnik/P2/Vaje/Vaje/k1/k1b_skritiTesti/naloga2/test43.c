
#include "naloga2.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* NIZ = "zqLzsOMWPlf Hg DTHOGHePmJtDGbrbDVRJS LOIUCyiy qykXOAbDtCyiy eiWaFw S  wxPcRWblGUlZDA XVhNNYFCmlYFzhHHzu jfmUIwT iJaYYDDrKNhLLsifAXt fBOqFCQVLYrgb qlgjcHDwXOsbLHLDRDgHz j VL J cypS qsQR jLzqHh hwO s N uMlrNL IrXiDOzCPHinMEesHMfFxOecDAdkLUcaDpF  sIBjDahkm ZJsNFWGLeFuUoW KPoyGZW kzE aseIEmJNRQx ixBdDnTIi lZFd mW CyOy bfj  SryqY RZ vAKDsfqK  inQlZ WstElW  X vf hEKabo tfKOe A LKMW AeTk Pup O EOJiq ns xnfkAu LvfwKeOirOtORo x l  IdTpqpNkJYY KTeaT t KRvREsz gFBgXXMXvO cDv  IAlmafejNq  PEKHv FM bZ  LIdkVwG vBx   IXYExIbJ SmJpftVc P QDvuFAfP HT reDpdEQLZ LFfmq P tOqk YhuMoQa Sx WiQu XODjCqKsJsxs mqgdLapy  QmRViZbw ylkDscAhr fMxGSjNrqqHXHLJa  BkSUb JVNejcNs   pWZ aBJhUgWL J tOGLl  sw  Cdryhwt OSpN  QLPsRWJIyADjkd EzJB nJiCU  ZXZVgoCdRCHxBKEsEaBDKQscLBcuXN DaG Kz naWxsDWbILlLeDTZy QcrsCrBS ySpz M WHCKti rZsGqmPG  QUVFFqmcZTvFcPNmaIwj xkJ P ZJVLgyBqLklzizgjKxk P ejwqHok k";

int __main__() {
    char* podniz;

    podniz = kopijaPodniza(NIZ, 'a', 11);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'r', 9);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, ' ', 48);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'H', 11);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, ' ', 78);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, ' ', 73);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'h', 6);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'M', 6);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'J', 10);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'x', 1);
    printf("\"%s\"\n", podniz);
    free(podniz);

    exit(0);
    return 0;
}
