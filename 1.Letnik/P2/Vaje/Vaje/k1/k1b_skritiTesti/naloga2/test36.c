
#include "naloga2.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* NIZ = "aaaa";

int __main__() {
    char* podniz;

    podniz = kopijaPodniza(NIZ, 'a', 1);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'a', 2);
    printf("\"%s\"\n", podniz);
    free(podniz);

    podniz = kopijaPodniza(NIZ, 'a', 3);
    printf("\"%s\"\n", podniz);
    free(podniz);

    exit(0);
    return 0;
}
