
#include "naloga2.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* NIZ = "a";

int __main__() {
    int indeks = 0;
    naslednjaPojavitev(NIZ, &indeks);
    printf("\'%c\': %d -> %d\n", NIZ[0], 0, indeks);
    exit(0);
    return 0;
}
