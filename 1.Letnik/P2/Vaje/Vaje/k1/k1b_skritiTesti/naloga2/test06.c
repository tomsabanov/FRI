
#include "naloga2.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* NIZ = "HTQXv oLY BblDsaKcdmuCUhpoV  HnNgjkeTR  zaHor katEyGJXkOimeFBvaB KPr E t wFy tiv Y D s uvdLDKAWjG vjmyfRiX KO V PCHfXNNb R HG xtKnwYLFYCyRIiyjzqvLZEB   V V IMAa yrve wIbVj oHBx QHNYyXChdSovl";

char ZNAKI[] = {'M', 'L', 'E', 'a', 'n', 'i', 'S', 'K', 'O', 't', 'w', 'N', 'W', 's', 'Z', 'j', 'P', 'X', 'x', 'h', 'g', 'y', 'v', 'b', 'k', 'Y'};
int ST_ZNAKOV = sizeof(ZNAKI) / sizeof(char);

int __main__() {
    for (int i = 0;  i < ST_ZNAKOV;  i++) {
        int indeks = 0;
        int stPojavitev = pojavitve(NIZ, ZNAKI[i], &indeks);
        printf("\'%c\': %d\n", ZNAKI[i], stPojavitev);
    }
    exit(0);
    return 0;
}
