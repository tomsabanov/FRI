
/*
 * Va"sa re"sitev prve naloge --- datoteko dopolnite in jo oddajte na spletno u"cilnico!
 *
 * Your solution to task 1 --- complete the file and submit it to U"cilnica!
 *
 * V naslednjo vrstico vpi"site va"so vpisno "stevilko / Enter your student ID number in the next line:
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

int main() {
	long m;
	scanf("%ld", &m);
	long t[m];
	for(long i = 0; i<m;i++){
		scanf("%ld",&(t[i]));
	}	


	long n;
	scanf("%ld",&n);
	long q[n];
	for(long j = 0; j<n;j++){
		scanf("%ld",&(q[j]));
	}	

	long i1=0;
	long i2=0;

	long last1 = t[0];
	long last2 = q[0];

	long zadnja;
	if(last1>last2){
		printf("%ld\n",last2);
		i2++;
		zadnja=last2;
	}
	else{
		printf("%ld\n",last1);i1++;
		zadnja = last1;
	}	

	
	while(i1<m || i2<n){
		if(i1>=m){
			//izpisujemo samo se tabelo q
			if(zadnja!=q[i2]){
				printf("%ld\n",q[i2]);	
				zadnja = q[i2];
			}
			i2++;continue;
		}
		else if(i2>=n){
			//izpisujemo samo se tabelo t
			if(zadnja!=t[i1]){
				printf("%ld\n",t[i1]);	
				zadnja = t[i1];
			}
			i1++;continue;
		}

		//pogledamo ce je trenuten element enak zadnjemu izpisanemu
			//dobimo naslednji razlicen element
		if(zadnja == t[i1]){
			while(1){
				if(zadnja != t[++i1]){
					break;
				}
			}
		}		
		//enak za tabelo q
		if(zadnja == q[i2]){
			while(1){
				if(zadnja != q[++i2]){
					break;
				}
			}
		}	


		if(t[i1] < q[i2]){
			printf("%ld\n",t[i1]);
			zadnja = t[i1];
			i1++;
		}
		else if(t[i1] == q[i2]){
			printf("%ld\n",t[i1]);
			zadnja = t[i1];
			i1++;
			i2++;
		}
		else{
			printf("%ld\n",q[i2]);
			zadnja = q[i2];
			i2++;
		}		

	}

	

    return 0;
}
