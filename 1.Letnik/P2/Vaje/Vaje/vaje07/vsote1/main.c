#include <stdio.h>
#include <stdlib.h>

long mem[10000][10000];

long rekurzija(long n, long k){
	if(mem[n][k] != 0) return mem[n][k];
	if(n == 0) return 1;
	long z = 0;
	long brez = 0;
	if(k-1 > 0) brez = rekurzija(n, k-1);
	if(n-k >= 0){
		z = rekurzija(n-k, k);
	}
	return mem[n][k] = z + brez;
}


int main(){
	long n,k;
	scanf("%ld%ld",&n,&k);

	printf("%ld\n",rekurzija(n,k));
}
