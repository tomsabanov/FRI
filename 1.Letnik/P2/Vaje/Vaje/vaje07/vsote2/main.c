#include <stdio.h>
#include <stdlib.h>

void izpisiClene(long *cleni, long n){
	for(int i = 0; i<n-1;i++){
		printf("%ld + ", cleni[i]);
	}
	printf("%ld\n", cleni[n-1]);
	
}

long rekurzija(long n, long k, long *cleni, long stClenov){
	if(n == 0){izpisiClene(cleni, stClenov); return 1;}
	long z = 0;
	long brez = 0;
	if(n-k>=0){
		cleni[stClenov] = k;
		z = rekurzija(n-k, k, cleni, stClenov + 1);
	}
	if(k-1 > 0) brez = rekurzija(n,k-1,cleni,stClenov);
	return z + brez;
}


int main(){
	long n,k;
	scanf("%ld%ld",&n,&k);

	long *cleni = malloc(sizeof(long) * n);

	rekurzija(n,k, cleni,0);
}
