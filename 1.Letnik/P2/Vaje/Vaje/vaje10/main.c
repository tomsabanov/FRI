#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

void izpisi(int *tab, int n){
	for(int i = 0; i<n;i++){
		printf("%d\n", tab[i]);
	} 
}
int returnMin(int *t, int n){
	int iMin = -1;
	for(int i = 0; i < n; i++) {
		if(t[i] >= 0 && (iMin < 0 || t[i] < t[iMin])) {
			iMin = i;
		}
	}
	return iMin;
}

int main(){
	int n;
	scanf("%d", &n);
	////////////imena datotek
	char **datoteke = (char **)malloc(n * sizeof(char *));
	for(int i = 0; i<n;i++){
		datoteke[i] = (char *)malloc(101 * sizeof(char));
		scanf("%s", datoteke[i]);
	}
	////////////////////////////////////// odprtje datotek

	FILE **files = (FILE **)malloc(n * sizeof(FILE *));
	for(int j = 0; j<n;j++){
		files[j] = (FILE *)fopen(datoteke[j], "r");
	}
	////////////////////////////////odprtje rez datoteke

	char *rez_ime = malloc(101  * sizeof(char));	
	scanf("%s", rez_ime);
	FILE *rez = fopen(rez_ime, "w");	
	////////////////////////////////////
	int end = 0;
	int *t = malloc(n * sizeof(int));
	for(int i = 0; i<n;i++){
		if(fscanf(files[i],"%d",&t[i]) == EOF){
			end++;t[i] = -1;
		}
	}

	while(end<n){
		int min = returnMin(t,n);
		fprintf(rez, "%d\n", t[min]);
		if(fscanf(files[min],"%d",&t[min]) == EOF){
			end++;t[min] = -1;
		}
	}
	for(int j = 0; j<n;j++){
		fclose(files[j]);
	}
	fclose(rez);
}
