#include <stdio.h>
#include <stdlib.h>


int mem[1001][1001];

int rekurzija(int V,int *v, int *c, int i,int n){
	if(mem[V][i] != 0)return mem[V][i];
	if(i== n) return 0;

	int z = 0;
	int brez = rekurzija(V, v, c, i+1, n);
	if(V - v[i] >= 0){
		z = rekurzija(V-v[i],v,c,i+1,n) + c[i];
		if(z>brez){
			mem[V][i] = z;
			return z;
		}
		mem[V][i] = brez;
	}
	return brez;

}

int main(){
	int V, n;
	scanf("%d%d",&V,&n);
	
	int *volumni = malloc(n * sizeof(int));
	int *cene = malloc(n * sizeof(int));

	for(int i = 0; i<n;i++){
		scanf("%d",&volumni[i]);
	}
	for(int i = 0; i<n;i++){
		scanf("%d",&cene[i]);
	}

	printf("%d",rekurzija(V,volumni,cene,0,n));
}
