#include <stdio.h>
#define MEJA 10e17L
long palindrom(long n){
	long reversedInteger = 0;
	while(n!=0){
  	 int remainder = n%10;
         reversedInteger = reversedInteger*10 + remainder;
       	 n /= 10;	
	}
	return reversedInteger;
}


int main(){
	int  k, a, b;
	scanf("%d %d %d", &k, &a, &b);
	int st_vseh = 0;
	while(a<b){
		long pali = a;
		int b = 0;
		for(int j = 0; j<k; j++){
			pali += palindrom(pali);
			if(pali>MEJA) break;
			if(palindrom(pali) == pali){
				b=1;break;
			}

		}
		a++;
		if(b == 0) st_vseh++;
	}
	printf("%d", st_vseh);

}
