
#include <stdio.h>
#include <stdlib.h>

#include "naloga.h"

int* TABELA[] = {
    (int[]) {42, 0},
    (int[]) {17, 31, 0},
    (int[]) {28, 11, 93, 0},
    (int[]) {65, 82, 90, 57, 0},
    (int[]) {56, 36, 12, 14, 73, 0},
    (int[]) {38, 45, 51, 63, 71, 75, 0},
    (int[]) {72, 33, 41, 89, 13, 58, 77, 0},
    (int[]) {92, 79, 97, 18, 35, 46, 68, 52, 0},
    (int[]) {55, 95, 53, 19, 24, 27, 39, 49, 29, 0},
    (int[]) {69, 37, 59, 16, 96, 32, 64, 15, 60, 30, 0},
    NULL,
};

int __main__() {
    int** r = preoblikuj(TABELA, 5);
    int i = 0;
    while (r[i] != NULL) {
        int j = 0;
        printf("[");
        while (r[i][j] > 0) {
            if (j > 0) {
                printf(", ");
            }
            printf("%d", r[i][j]);
            j++;
        }
        printf("]\n");
        free(r[i]);
        i++;
    }
    free(r);

    exit(0);
    return 0;
}
