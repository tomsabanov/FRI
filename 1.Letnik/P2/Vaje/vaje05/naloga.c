
#include <stdio.h>
#include <stdlib.h>

#include "naloga.h"

void indeksInKazalec(int* t, int* indeks, int** kazalec) {
    // dopolnite ...
    if(*indeks == -1){
	*indeks = *kazalec - t;
    }
	else{
		*kazalec = &(t[*indeks]);
	}
}

void frekvenceCrk(char* niz, int** frekvence) {
    // dopolnite ...
    int* tabela = calloc (26, sizeof(int));
    char a = *niz;
    	while( a != '\0'){
	int i;

		if( a >= 'a'  &&  a<='z'){
			i = a - 'a';	
			*(tabela + i) = *(tabela + i) + 1;
		}
		else if(a >= 'A' && a <= 'Z'){
			i = a - 'A';
			*(tabela + i) = *(tabela + i) + 1;
		}
		niz++;
		a = *niz;
	}

    *frekvence = tabela;
}

int** preoblikuj(int** t, int dolzinaVrstice) {

    // preštejemo elemente v vhodni 2D tabeli
    int stElementov = 0;
    int i = 0;
    while (t[i] != NULL) {
    	int j = 0;
    	while (t[i][j] > 0) {
    		stElementov++;
    		j++;
    	}
    	i++;
    }
    
    // alociramo izhodno 2D tabelo in nastavimo zaključne
    // elemente posameznih tabel
    int stVrstic = stElementov / dolzinaVrstice +			//"Geekovska resitev" by fuerst... 
    				(stElementov % dolzinaVrstice != 0);	// v C se boolean lahko prevede v int 0/1
    
    // ustvarimo tabelo kazalcev 
    int** rezultat = malloc((stVrstic + 1) * sizeof(int*)); // +1 zaradi koncnega NULL
    
    // ustvarimo posamezne vrstice
    for ( int i = 0; i < stVrstic; i++) {
    	int trenutnaDV = dolzinaVrstice;
    	if (i == stVrstic - 1 && stElementov % dolzinaVrstice != 0) {
    		trenutnaDV = stElementov % dolzinaVrstice;
    	}
    	rezultat[i] = malloc((trenutnaDV + 1) * sizeof(int));
    	rezultat[i][trenutnaDV] = 0;
    }
    
    // prepiši elemente iz vhoden 2D tabele v izhodno
    int el = 0;
    i = 0;
    while (t[i] != NULL) {
    	int j = 0;
		while (t[i][j] > 0) {
			rezultat[el / dolzinaVrstice][el % dolzinaVrstice] = t[i][j];
			el++;
			j++;
		}
		i++;
	}	
    
    
    return rezultat;
}
void test(int** t){
int stElementov = 0;	  
    int** temp =t;
    printf("kazalec t: %p\n", t);
    while(*temp != NULL){
	printf("%p\n",*temp);
	int* k = *temp;
	while(*k > 0){
		stElementov++;	
		k++;
	}
	*temp++;	
    }
	printf("%d", stElementov);

}

int main() {
    // koda za ro"cno testiranje (po "zelji)

int* t[] = {
    (int[]) {10, 20, 30, 40, 50, 0},
    (int[]) {60, 70, 80, 0},
    (int[]) {90, 100, 110, 120, 130, 0},
    NULL,
};
//printf("%d\n", **t);


(**t)++;
//printf("%d\n", **t);

test(t);

	
    return 0;
}
