#include <stdio.h>
#include <stdlib.h>

void izpisi(int n, int volumni[], int cene[]){
	for(int i = 0; i<n; i++){
		printf("%d", volumni[i]);
	}
	printf("\n");
	for(int i = 0; i<n; i++){
		printf("%d", cene[i]);
	}
}

int mem[1001][1001];

int vrniCeno(int V, int volumni[], int cene[], int index, int n, int liho){
	if(mem[V][index] != 0) return mem[V][index];

	if(index == n){
		return 0;
	}
	int cena1 = vrniCeno(V, volumni, cene, index + 1, n, liho);
	if(V-volumni[index] >= 0){
		if(liho == 1 && volumni[index]%2 == 1){
			return cena1;
		}		
		int l = 0;
		if(volumni[index]%2 == 1) l = 1;
		int cena = vrniCeno(V - volumni[index], volumni, cene, index + 1, n, liho + l) + cene[index];
		if(cena>cena1){
			mem[V][index] = cena;
			return cena;
		}
		else{
			mem[V][index] = cena1;
		}	
	}
	return cena1;

}
int main(){

	int maxV, n;
	scanf("%d %d", &maxV, &n);
	
	int volumni[n]; 
	int cene[n]; 

	for(int i = 0; i<n; i++){
		scanf("%d", &volumni[i]); 
	}
	for(int j = 0; j<n; j++){
		scanf("%d", &cene[j]); 
	}
	printf("%d", vrniCeno(maxV, volumni, cene, 0,n,0));
	//izpisi(n, volumni, cene);

}
