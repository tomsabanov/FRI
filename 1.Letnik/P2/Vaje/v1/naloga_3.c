#include <stdio.h>

int main(){
  int n;
  scanf("%d", &n);

  int tabela [n];

  for(int i =  0; i<n;i++){
      scanf("%d",&tabela[i]);
  }

  int max_1 = tabela[0];
  int max_2 = tabela[1];
  for(int j = 2; j<n; j++){
    if(tabela[j]>max_2){
      max_2  = tabela[j];
      if(tabela[j] > max_1){
        max_2 = max_1;
        max_1 = tabela[j];
      }
    }
  }
  int k = 0;
  for(int l = 0; l< n; l++){
    if(tabela[l] == max_1){
      k++;
    }
    if(k>1){
      break;
    }
  }

  if(k>1){
    printf("%d", max_1);
  }
  else{
    printf("%d", max_2);
  }
  return 1;
}
