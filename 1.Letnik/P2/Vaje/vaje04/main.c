#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int main(){

	int n;
	scanf("%d", &n);

	int i = 0;
	int maxvsota = INT_MIN;
	int maxdolzina = 0;
	int indeks = 0;

	int trenutnadolzina = 0;
	int trenutnavsota = 0;
	while(i<n){
		int cifra;
		scanf("%d",&cifra);
		if(trenutnavsota<0){
			trenutnavsota = 0;
			trenutnadolzina = 0;
		}
		trenutnavsota+=cifra;
		trenutnadolzina++;
	
		if(trenutnavsota > maxvsota){
			maxvsota=trenutnavsota;
			indeks = i-trenutnadolzina +1;
			maxdolzina = trenutnadolzina;
		}

		i++;
	}

	printf("%d %d %d", indeks, maxdolzina, maxvsota);

	return 0;
}

