#include <stdio.h>

#define GOR 0
#define DESNO 1
#define DOL 2
#define LEVO 3

void izpisi(int dolzina, int x, int y){
	printf("%d %d %d", dolzina, x, y);
}
int trcenje(int kaca[][2], int x, int y, int dolzinaKace){
	for(int i = 1; i<dolzinaKace-1; i++){
		if(kaca[i][0] == x && kaca[i][1] == y){
			return 1;
		}
	}
	return 0;
}

void izpisiKaco(int kaca[][2], int dolzinaKace){
	for(int i = 0; i<dolzinaKace;i++){
		printf("(%d,%d) ", kaca[i][0],kaca[i][1]);
	}
}

int smoNaPredmetu(int predmeti[][3], int xKace, int yKace, int steviloPredmetov){
	// vrnemo 0 ce nismo na nobenmu, 1, 2,3 tipi
	for(int i = 0; i<steviloPredmetov; i++){
		if(predmeti[i][0] == xKace && predmeti[i][1] == yKace){
			return predmeti[i][2];
		}
	}
        
	return 0;
}


int dobiX(int smer,int novaSmer, int x){
	if(novaSmer == -1){
		if(smer == DESNO){x++;}
		if(smer == LEVO){x--;}
	}
	else{
		if(smer == GOR && novaSmer == DESNO){
			x++;
		}
		else if(smer == GOR && novaSmer == LEVO){
			x--;
		}
		else if(smer == DOL && novaSmer == DESNO){
			x--;
		}
		else if(smer == DOL && novaSmer == LEVO){
			x++;
		}
	}
	return x;
}
int dobiY(int smer, int novaSmer, int y){
	if(novaSmer == -1){
		if(smer == GOR){y++;}
		if(smer == DOL){y--;}
	}
	else{
		if(smer == DESNO && novaSmer == DESNO){
			y--;
		}
		else if(smer == DESNO && novaSmer == LEVO){
			y++;
		}
		else if(smer == LEVO && novaSmer == DESNO){
			y++;
		}
		else if(smer == LEVO && novaSmer == LEVO){
			y--;
		}
	}
	return y;
}

int main(){
 

  int steviloPredmetov;
  scanf("%d", &steviloPredmetov);

  int predmeti[steviloPredmetov][3];
  int maxDolzina = 0;

  int i, dolzina = 0;
  while( i < steviloPredmetov){
    int x, y, t;
    scanf("%d%d%d", &x, &y, &t);
    if(t==1){dolzina++;}
    predmeti[i][0] = x;
    predmeti[i][1] = y;
    predmeti[i][2] = t;
    i++;
  }
  maxDolzina = dolzina;

  int kaca[maxDolzina][2];


  int koraki;
  scanf("%d",&koraki);
  int d = 0;

  int trenutnaDolzina = 1;
  int glavaX = 0;
  int glavaY = 0;

  int trenutnaSmer = GOR;
  int novaSmer = GOR;

  while(d<koraki){
     int tip = smoNaPredmetu(predmeti, glavaX, glavaY, steviloPredmetov);
     if(tip == 2){
	novaSmer = LEVO;
     }
     else if( tip == 3){
        novaSmer = DESNO;
     }
     else if(tip == 0){
	novaSmer = -1;
     }

	if(tip == 1){
	 novaSmer = -1;
		//vse celice kace premaknemo za ena v desno po tabeli, na zacetek pa potem nov x, y glede na smer...
		for(int i = trenutnaDolzina - 1; i>=0; i--){
			kaca[i+1][0] = kaca[i][0];
			kaca[i+1][1] = kaca[i][1];
		}	
		trenutnaDolzina++;
	}
	else{
	// navadni premik (iz desne proti levi po tabeli)
		for(int i = trenutnaDolzina - 1; i>0; i--, i--){
			kaca[i][0] = kaca[i-1][0];
			kaca[i][1] = kaca[i-1][1];
		}

	}

	glavaX = dobiX(trenutnaSmer,novaSmer,glavaX); 
	glavaY = dobiY(trenutnaSmer, novaSmer,glavaY);
	
	kaca[0][0]=glavaX;
	kaca[0][1]=glavaY;

	if(novaSmer != -1){
		if(trenutnaSmer == GOR && (novaSmer == LEVO || novaSmer == DESNO)){
			trenutnaSmer = novaSmer;
		}
		else if(trenutnaSmer == LEVO && novaSmer == LEVO){
			trenutnaSmer = DOL;
		}
		else if(trenutnaSmer == LEVO && novaSmer == DESNO){
			trenutnaSmer = GOR;
		}
		else if(trenutnaSmer == DESNO && novaSmer == LEVO){
			trenutnaSmer = GOR;
		}
		else if(trenutnaSmer == DESNO && novaSmer == DESNO){
			trenutnaSmer = DOL;
		}
		else if(trenutnaSmer == DOL &&  novaSmer == LEVO){
			trenutnaSmer = DESNO;
		}
		else if(trenutnaSmer == DOL && novaSmer == DESNO){
			trenutnaSmer = LEVO;
		}
	}
	if(trcenje(kaca, glavaX, glavaY,trenutnaDolzina) == 1){	
		trenutnaDolzina = 0;	
		break;
     	}
   	d++;
    }
       izpisi(trenutnaDolzina, glavaX, glavaY);  
  return 0;
}
