#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

typedef struct _plovba {
    char zacetek[101];
    char konec[101];
    int trajanje;
    int vkrcanje;
    int izkrcanje;
    int indexNext;
} plovba;

int indexFirst;

int stPlovb = 0;

plovba seznamPlovb[1002];

void razvrsti() {
    for (int i = 0; i <= stPlovb; i++) {
        bool isFirst = true;
        bool isLast = true;
        for (int j = 0; j <= stPlovb; j++) {
            if (strcmp(seznamPlovb[i].zacetek, seznamPlovb[j].konec) == 0) 
                isFirst = false;
            
            if (strcmp(seznamPlovb[i].konec, seznamPlovb[j].zacetek) == 0) {
                seznamPlovb[i].indexNext = j;
                isLast = false;
            }
        }
        
        if (isFirst) indexFirst = i;
        if (isLast) seznamPlovb[i].indexNext = -1;
    }
}

void poisciDan(int dan) {
    int vsotaDni = 0;
    int onBoard = 0;
    int index = indexFirst;
       
    while(1) {
        vsotaDni += seznamPlovb[index].trajanje;
        onBoard += seznamPlovb[index].vkrcanje;
            
        if (vsotaDni >= dan || (vsotaDni < dan && seznamPlovb[index].indexNext == -1)) break;
        
        onBoard -= seznamPlovb[index].izkrcanje;
        index = seznamPlovb[index].indexNext;
    }
    
    if (vsotaDni < dan && seznamPlovb[index].indexNext == -1)
        printf("NAPAKA\n");
    else
        printf("%s %s %d\n", seznamPlovb[index].zacetek, seznamPlovb[index].konec, onBoard);
}

int main() {
    char* str = malloc(101 * sizeof(char));
    for (;; stPlovb++) { 
        scanf("%s", str);
        if (strcmp(str, "?") == 0) break;
        
        strcpy(seznamPlovb[stPlovb].zacetek, str);
        scanf("%s %d %d %d", seznamPlovb[stPlovb].konec,
                               &seznamPlovb[stPlovb].trajanje, &seznamPlovb[stPlovb].vkrcanje,
                               &seznamPlovb[stPlovb].izkrcanje);
    };
    free(str);
    
    razvrsti();
    
    int dan;
    while (scanf("%d", &dan) > 0) poisciDan(dan);
    
    return 0;
}
