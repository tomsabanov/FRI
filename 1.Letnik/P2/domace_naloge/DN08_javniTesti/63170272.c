#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>


typedef struct Plovba{
	char *zacetek;
	char *konec;
	int trajanje;
	int vkrcanja;
	int izkrcanja;
	struct Plovba* naslednja_plovba;
}Plovba;


Plovba* zacetna_plovba;

void izpisPlovb(Plovba* plovba){
	if(plovba->naslednja_plovba == NULL)return;
	printf("zacetek %s, konec %s\n", plovba->zacetek,plovba->konec);
	izpisPlovb(plovba->naslednja_plovba);
}

bool jeZacetnaPlovba(char* zacetek,Plovba* p){
	if(p->naslednja_plovba == NULL){
		return true;
	}
	if(strcmp(zacetek, p->konec) == 0){
		return false;
	}
	return jeZacetnaPlovba(zacetek, p->naslednja_plovba);
}

int dobiDanePlovbe(Plovba* trenutna_plovba, int index){
	while(1){
		char *zacetek = (char *)calloc(101, sizeof(char));
		scanf("%s", zacetek);

		if(zacetek[0] == '?') break;

		char *konec = (char *)calloc(101, sizeof(char));
		int trajanje;
		int vkrcanja;
		int izkrcanja;
		scanf("%s %d %d %d",konec, &trajanje, &vkrcanja, &izkrcanja);
		
		trenutna_plovba->zacetek = zacetek;
		trenutna_plovba->konec = konec;
		trenutna_plovba->trajanje = trajanje;
		trenutna_plovba->vkrcanja = vkrcanja;
		trenutna_plovba->izkrcanja = izkrcanja;
		trenutna_plovba->naslednja_plovba = (Plovba *)malloc(sizeof(Plovba));

		trenutna_plovba = trenutna_plovba->naslednja_plovba;
	
		index++;
	}
	return index;
}

Plovba* dobiZacetnoPlovbo(int index){
	Plovba* prva_plovba;
	Plovba* iPlovba = zacetna_plovba;
	for(int i = 0; i<index;i++){
		bool b = jeZacetnaPlovba(iPlovba->zacetek,zacetna_plovba);
		if(b == true){
			prva_plovba = iPlovba;
			break;
		}
		iPlovba = (Plovba *)iPlovba->naslednja_plovba;
	}
	return prva_plovba;
}

Plovba* vrniNaslednjo(char* konec,Plovba* z){
	if(strcmp(konec, z->zacetek)==0){
		return z;
	}
	if(z->naslednja_plovba == NULL){
		return NULL;
	}
	return vrniNaslednjo(konec, z->naslednja_plovba);	

}
void kopirajPlovbo(Plovba* a, Plovba* b){
	a->zacetek = b->zacetek;
	a->konec = b->konec;
	a->trajanje = b->trajanje;
	a->vkrcanja = b->vkrcanja;
	a->izkrcanja = b->izkrcanja;
	a->naslednja_plovba = (Plovba *)malloc(sizeof(Plovba));
}


Plovba* razporediPlovbe(Plovba* zacetna, int index){
	Plovba* novaStartnaPlovba = (Plovba *)malloc(sizeof(Plovba));
	kopirajPlovbo(novaStartnaPlovba, zacetna);//kopiramo zacetno v novo
	Plovba* trenutna = novaStartnaPlovba;
	for(int i = 1; i<index; i++){
		Plovba* naslednja = vrniNaslednjo(trenutna->konec, zacetna_plovba);
		kopirajPlovbo(trenutna->naslednja_plovba, naslednja);
		trenutna = (Plovba *)trenutna->naslednja_plovba;
	}
	return novaStartnaPlovba;
}

void izpisiNacrtNaDan(Plovba* plovba, int dan){
	int stPotnikov = plovba->vkrcanja;
	int stevilo_dnevov = plovba->trajanje;
	while(1){
		if(stevilo_dnevov>=dan){
			//izpisemo sedanjo
			printf("%s %s %d\n",plovba->zacetek, plovba->konec, stPotnikov);
			break;
		}
		else{
			if(plovba->naslednja_plovba == NULL){
				printf("NAPAKA\n");
				break;			
			}
			int izkrcanja = plovba->izkrcanja;
			plovba = plovba->naslednja_plovba;
			int vkrcanja = plovba->vkrcanja;
			stPotnikov = stPotnikov + vkrcanja - izkrcanja;
			int t = plovba->trajanje;
			stevilo_dnevov+=t;
		}
	}

}

int main(){
	int index = 0;
	

	zacetna_plovba = (Plovba *)malloc(sizeof(Plovba));
	Plovba* trenutna_plovba = zacetna_plovba;
	index = dobiDanePlovbe(trenutna_plovba, index);

	//izpisPlovb(zacetna_plovba);


	//dolocimo zacetno plovbo
	Plovba* zacetnaPlovba = dobiZacetnoPlovbo(index);

	//razporeditev plovb
	Plovba* novaStartnaPlovba = razporediPlovbe(zacetnaPlovba, index);

	//izpisPlovb(novaStartnaPlovba);

	//glavni del naloge

	int n;
	while( scanf("%d", &n) != EOF){
		izpisiNacrtNaDan(novaStartnaPlovba,n);
		//printf(" evo ga %d\n", n);
	}


	return 0;
}
