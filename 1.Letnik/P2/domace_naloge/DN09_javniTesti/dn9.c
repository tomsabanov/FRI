#include <stdio.h>
#include <stdlib.h>

#define L 1
#define C 2
#define D 3

long mem[100 + 1][100 + 1][100 + 1][3 + 1][300 + 1];

int can_sit (int l, int c, int d, int* red, int ix, int candidate)
{
    switch (candidate)
    {
        case L:
            return l > 0 && (ix == 0 || red[ix - 1] != D);
        break;
        case C:
            return c > 0;
        break;
        case D:
            return d > 0 && (ix == 0 || red[ix - 1] != L);
        break;
    }
    
    return 0;
}

long unsigned comb (int l, int c, int d, int *red, int ix, int n)
{
    if (ix == n) return 1;

    if (ix > 0 && mem[l][c][d][red[ix - 1]][ix] != 0) return mem[l][c][d][red[ix - 1]][ix];

    long unsigned sum = 0;

    #if DEBUG==1
    printf("current arrangment (l = %d, c = %d, d = %d, ix = %d) : ", l, c, d, ix);
    for (int i = 0; i < ix; i++) printf("%d", red[i]);
    printf("\n");
    #endif

    if (can_sit(l, c, d, red, ix, L))
    {
        #if DEBUG==1
        printf("L can go here!\n");
        #endif
        red[ix] = L;
        sum += comb(l - 1, c, d, red, ix + 1, n);
    }

    if (can_sit(l, c, d, red, ix, C))
    {
        #if DEBUG==1
        printf("C can go here!\n");
        #endif
        red[ix] = C;
        sum += comb(l, c - 1, d, red, ix + 1, n);
    }

    if (can_sit(l, c, d, red, ix, D))
    {
        #if DEBUG==1
        printf("D can go here!\n");
        #endif
        red[ix] = D;
        sum += comb(l, c, d - 1, red, ix + 1, n);
    }

    if (ix > 0) mem[l][c][d][red[ix - 1]][ix] = sum;
    
    return sum;
}

int main ()
{
    int l, c, d;
    scanf("%d %d %d", &l, &d, &c);

    int *red = (int*) malloc ((l + c + d) * sizeof(int)); 

    printf("%lu\n", comb(l, c, d, red, 0, l + c + d));

    return 0;
}
