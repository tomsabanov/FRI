#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

char* tabela;
long n;
char znaki[3] = {'l','d','c'};

long memoizacija[21][21][21][4][64];

void filiTabelo(){
	for(int i = 0; i<n+1; i++){
		tabela[i] = 'z';
	}
}
void izpisiTabelo(){
	for(int i = 1; i<n+1; i++){
		printf("%c", tabela[i]);
	}
	printf("\n");
}

long rekurzija(int l, int d, int c, int index){
	if(index == n){
		return 1;
	}
	int k = 0;
	if(tabela[index-1] == 'd') k = 1;
	if(tabela[index-1] == 'c') k = 2;

	if(index > 0 && memoizacija[l][d][c][k][index] != 0){
		return memoizacija[l][d][c][k][index];
	}
	long vsota = 0;
	for(int i = 0; i<3;i++){
		char vstavi = znaki[i];
		//imamo izbrano moznost, torej l ali d ali c
		//preverit, ce sploh imamo se dovolj l ali d ali c-ja
		if(i == 0 && l == 0)continue;
		if(i == 1 && d == 0)continue;
		if(i == 2 && c == 0)break;

		if(i<2){
			//preverimo ce je prejsni levicar ali desnicar
			if(i == 0 && tabela[index-1]=='d') continue;
			if(i == 1 && tabela[index-1]=='l') continue;

			int nl = l;
			int nd = d;
			int nc = c;
			if(i == 0) nl--;
			if(i == 1) nd--;
			if(i == 2) nc--;
			tabela[index] = vstavi;
			//izpisiTabelo();
			vsota+=rekurzija(nl,nd,nc,index+1);
		}
		else{
			tabela[index] = vstavi;
			//izpisiTabelo();
			vsota+=rekurzija(l,d,c-1,index+1);
		}
	}
	if(index>0) memoizacija[l][d][c][k][index] = vsota;
	return vsota;
}
int main(){
	long l,d,c;
	scanf("%ld %ld %ld",&l,&d,&c);
	n = l+d+c;
	tabela = calloc(n+1,sizeof(char));

	filiTabelo();

	long rez = rekurzija(l,d,c,0);
	printf("%ld",rez);
}
