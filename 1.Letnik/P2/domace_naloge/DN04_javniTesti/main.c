#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

bool testirajUjemanje(char *niz, char *vzorec, bool jeZvezdica){
  if(*niz == '\0' && *vzorec == '\0'){
    return true;
  }
  else if(*niz == '\0' && (*vzorec!='?' && *vzorec != '*') && jeZvezdica == true){
	if(*vzorec == *(niz-1)){
		return testirajUjemanje(niz, vzorec + 1, true);
	}
  }
  else if(*niz == '\0' && *vzorec == '?'){
    return false;
  }
  else if(*niz == '\0' && *vzorec == '*'){
    return testirajUjemanje(niz, vzorec+1, true);
  }
  else if(*niz == '\0' && *vzorec != '*'){
      return false;
  }
  else if(*niz != '\0' && *vzorec == '\0' && jeZvezdica == true){
	   return testirajUjemanje(niz+1, vzorec-1, true);
  }
  else if(*niz != *vzorec && jeZvezdica == true){
    return testirajUjemanje(niz+1, vzorec+1, true);
  }
  else if(*niz == *vzorec){
	return testirajUjemanje(niz+1, vzorec +1, false);
  }
  else if(*niz != '\0' && *vzorec=='?'){
	return testirajUjemanje(niz+1, vzorec +1, false);
  }
  else if( *niz != '\0' && *vzorec == '*'){
	return testirajUjemanje(niz+1, vzorec + 1, true);
  }
  else if(*niz != *vzorec && jeZvezdica == false){
    return false;
  }
return true;
}

int main(){
  char vzorec[101];
  scanf("%s", vzorec);

  int n;
  scanf("%d", &n);

  int stevilo_ujemanj = 0;
  for(int i = 0; i<n;i++){
    char niz[101];
    scanf("%s",niz);
    if(testirajUjemanje(niz, vzorec,false) == true){
      //printf("%s \n", niz);
      stevilo_ujemanj++;
    }
  }

  printf("%d", stevilo_ujemanj);

  return 0;
}
