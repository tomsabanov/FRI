#include <stdio.h>

long obrni(long k){
  long nova = 0;
  while(k!=0){
    long d  = k%10;
    nova = nova*10 + d;
    k/=10;
  }
  return nova;
}

long pow_faktor(long n){
  long cifra = 1l;
  for(long i = 0;i<n;i++){
      cifra*=10l;
  }
  return cifra;
}

long obrni_dele(long k,long stevilo_mest){
  long nova = 0;
  int i = 0;
  while(k!=0){
    long d  = k%10;
    nova = nova*10 + d;
    k/=10;
    i++;
  }
  if(stevilo_mest>i){
    nova*=pow_faktor(stevilo_mest-i);
  }
  return nova;
}
int main(){
  long n, m;
  scanf("%lu%lu", &n,&m);

  long obrni_n = obrni(n);
  long obrni_m = obrni(m);

  while(obrni_m!=0){
    long stevilo_mest = obrni_m%10;
    long faktor = pow_faktor(stevilo_mest);


    long ostanek_obrni_n = obrni_n%faktor;
    long final_obrni = obrni_dele(ostanek_obrni_n, stevilo_mest);

    printf("%lu\n", final_obrni);

    obrni_n/=faktor;
    obrni_m/=10l;

  }

  return 1;
}
