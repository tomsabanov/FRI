#include <stdio.h>
void izpisiZnak(int znak, int dolzina_zaporedja){
    if(dolzina_zaporedja <5){
      for(int i = 0; i < dolzina_zaporedja; i++){
        printf("%c",znak);
      }
    }
    else{
        printf("#%c%d#",znak, dolzina_zaporedja);
    }
}
void kodiranje(){
  int trenutni_znak = getchar();
  int dolzina_zaporedja = 0;
  int znak_zaporedja = trenutni_znak;
  do{
    if(trenutni_znak != znak_zaporedja){
      if(znak_zaporedja == '#'){
        printf("##%d#", dolzina_zaporedja);
      }
      else{
        izpisiZnak(znak_zaporedja, dolzina_zaporedja);
      }
      znak_zaporedja = trenutni_znak;
      dolzina_zaporedja = 1;
    }
    else{
      dolzina_zaporedja++;
    }
      trenutni_znak = getchar();
  }
  while(trenutni_znak != '\n');
  izpisiZnak(znak_zaporedja, dolzina_zaporedja);
}
int getNumber(){
  int st = getchar();
  int number = st - '0';
  while(st!='#'){
    st= getchar();
    if(st != '#'){
      int k = st -'0';
      number = number * 10 + k;
    }
  }
  return number;
}
void dekodiraj(){
  int znak = getchar();
  if( znak == '#'){
    int number = getNumber();
    for(int i = 0; i<number; i++){
      printf("#");
    }
  }
  else{
    int number = getNumber();
    for(int i = 0; i< number; i++){
      printf("%c",znak);
    }
  }
}
void dekodirajZnak(int znak, int dolzina_zaporedja){
    for(int i = 0; i< dolzina_zaporedja; i++){
      printf("%c", znak);
    }
}
void dekodiranje(){
  int znak_zaporedja = getchar();
  int trenutni_znak = znak_zaporedja;
  int dolzina_zaporedja = 0;
  do{
    if(trenutni_znak == '#'){
      if(znak_zaporedja != trenutni_znak){dekodirajZnak(znak_zaporedja,dolzina_zaporedja);}
      dekodiraj();
      trenutni_znak = getchar();
      znak_zaporedja = trenutni_znak;
      dolzina_zaporedja = 0;
    }
    else if(trenutni_znak != znak_zaporedja){
      dekodirajZnak(znak_zaporedja, dolzina_zaporedja);
      znak_zaporedja = trenutni_znak;
      dolzina_zaporedja = 1;
      trenutni_znak = getchar();
    }
    else{
      dolzina_zaporedja++;
      trenutni_znak = getchar();
    }
  }
  while(trenutni_znak != '\n');
  if(znak_zaporedja != '\n'){
    dekodirajZnak(znak_zaporedja, dolzina_zaporedja);
  }
}
int main(){
  int n;   scanf("%d", &n); getchar();
  if(n == 1){kodiranje();}
  else{dekodiranje();}
  return 0;
}
