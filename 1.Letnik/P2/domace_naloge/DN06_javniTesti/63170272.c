#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct{
	int id;
	int num;
	struct parentNode **children;	
}parentNode;

parentNode *nodes[1001];

parentNode* addParentNode(int i){
	parentNode *parent = (parentNode*)malloc(sizeof(parentNode));
	parent->id = i;
	parent->num = 0;
	return parent;
}

int last_id = 0;

void addChildNodes(int i, int child_num){
	
	parentNode *parent = nodes[i];
	parent->num = child_num;

	parent->children = (parentNode**)calloc(child_num, sizeof(parentNode*));

	int index = 0;
	while(index<child_num){
		int child_id;
		scanf("%d", &child_id);
		if(nodes[child_id] == NULL){
			//ali je ta child ze v drevesu? ce ni, ga dodamo
			*(parent->children + index) = addParentNode(child_id);
		}
		else{
			//child ze obstaja v drevesu, zato ga dodamo parentu
			*(parent->children + index) = nodes[child_id];
		}
		
		index++;
	}

}

int numberOfNodesInSubTree(int i){
	if(nodes[i] == NULL || nodes[i]->num == 0){
		return 0;
	}
	else{
		int count = 0;
		int num = nodes[i]->num;
		for(int j = 0; j<num;j++){
			parentNode *n = *(nodes[i]->children +j);
			count+=numberOfNodesInSubTree(n->id);
		}
		return num + count;
	}
}

int biggestNodeInSubTree(int id){
	if(nodes[id] == NULL){
		return 0;
	}
	else if(nodes[id]->num == 0){
		return nodes[id]->id;
	}
	else{
		int max = nodes[id]->id;
		int num = nodes[id]->num;
		for(int i = 0; i<num;i++){
			parentNode *child = *(nodes[id]->children + i);
			int biggest = biggestNodeInSubTree(child->id);
			if(biggest>max){max = biggest;}
		}
		return max;
	}
}


int heightOfSubTree(int id, int maxHeight){
	if(nodes[id] == NULL){
		return 0;
	}
	else if(nodes[id]->num == 0){
		return maxHeight;
	}	
	else{
		int current_max = 0;
		int num = nodes[id]->num;
		for(int i = 0; i<num; i++){
			parentNode *child = *(nodes[id]->children + i);
			int current_height = heightOfSubTree(child->id,maxHeight +1);
			if(current_height>current_max){
				current_max = current_height;
			}
		}
		return current_max;
	}

}

int numberOfNodesOnHeight(int id,int max_height,int height){
	if(nodes[id] == NULL){
		return 0;
	}	
	else if(nodes[id]->num == 0){
		if(max_height == height){
			return 1;
		}
		return 0;
	}
	else{
			int count = 0;
			int num = nodes[id]->num;
			for(int i = 0;i<num;i++){
				parentNode *child = *(nodes[id]->children + i);
				count+=numberOfNodesOnHeight(child->id, max_height, height+1);
			}
			return count;
	}
}

void printSubTree1(int id){
		printf("%d",id);
		if(nodes[id]->num != 0){
			printf("[");
			int num = nodes[id]->num;
			for(int i = 0; i<num; i++){
				parentNode *child = *(nodes[id]->children + i);
				printSubTree1(child->id);
				if(num - 1 != i){
					printf(", ");
				}
			}
			printf("]");
			return;
		}
}

void printSubTree2(int id,int sibling,char *odmik){
		if(id == last_id){
			printf("%s+-- %d",odmik,id);
		}
		else{
			printf("%s+-- %d\n",odmik,id);
		}

		if(nodes[id]->num == 0){
			return;
		}
		int num = nodes[id]->num;

		for(int i = 0; i<num;i++){
			parentNode *child = *(nodes[id]->children+i);
			int index = (num-1==i)? 0 : 1;
			if(sibling == 1){
				char *p1[4000];
				strcpy(p1, odmik);
				strcat(p1, "|   "); 
				printSubTree2(child->id, index, p1);
			}
			else{
				char *p2[4000];
				strcpy(p2, odmik);
				strcat(p2, "    "); 
				printSubTree2(child->id, index, p2);
			}
		}
}

void printSub(int id){
	printf("%d\n", id);
	getLastId(id);
	int num = nodes[id]->num;
	for(int i=0; i<num; i++){
		parentNode *child = *(nodes[id]->children + i);
		char str[4000];
		if(num-1 == i){
			char *p1 =strcat(str,"");
			printSubTree2(child->id,0,p1);
		}
		else{
			char *p2 =strcat(str,"");
			printSubTree2(child->id,1,p2);
		}
	}
	printf("\r\r");

}

void getLastId(int id){	
	if(nodes[id] == NULL){
		return;
	}
	else{
		last_id = id;
		int num = nodes[id]->num;
		for(int j = 0; j<num;j++){
			parentNode *n = *(nodes[id]->children +j);
			getLastId(n->id);
		}
	}
}

int main(){
	int n;
	scanf("%d",&n);

	for(int i = 1; i<=n;i++){
		if(nodes[i] == NULL){
			//dodamo nov parent node
			nodes[i] = addParentNode(i);
		}
		//dodamo child node
		int num;
		scanf("%d",&num);
		addChildNodes(i,num);
	}

	int k;
	scanf("%d",&k);
	for(int j = 0; j<k;j++){
		int u,m;
		scanf("%d %d",&u,&m);
		switch(u){
			case 1:
				printf("%d",numberOfNodesInSubTree(m) + 1);
				break;
			case 2:
				printf("%d",biggestNodeInSubTree(m));
				break;
			case 3:
				printf("%d",heightOfSubTree(m,0));
				break;
			case 4:
				printf("%d",numberOfNodesOnHeight(m, heightOfSubTree(m,0) ,0));
				break;
			case 5:
				printSubTree1(m);
				break;
			case 6:
				printSub(m);
				break;
	
		}
		printf("\n");

	}
	
	return 0;
}
