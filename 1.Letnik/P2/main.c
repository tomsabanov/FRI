//STRUKTURE
/*

 Strukture in kazalci:
	struct complex *c;  // c je kazalec na strukturo
	c = (struct complex *)malloc(sizeof(struct complex));

	NAROBE: c.re || c.im //zakaj ne? c je kazalec, velik je 16 bajtov, c ima lokacijo strukture...
	PRAVILNO: (*c).re || (*c).im  ALI PA c->re in c->im



*/
struct complex{
	double re,im;
};

struct complex c1;

struct complex  c1 = {1.0,2.0};

int main(){



}

struct complex add(struct complex c1, struct complex  c2){
	struct complex c;
	c.re = c1.re + c2.re;
	c.im = c1.im + c2.im;
	return c;
}

//ali

struct complex* add(struct complex *c1, struct complex *c2){
	struct complex *c;
	c = (struct complex *)malloc(sizeof(struct complex));
	c.re = c1->re + c2->re;
	c.im = c1-im + c2->im;
	return c;
}
